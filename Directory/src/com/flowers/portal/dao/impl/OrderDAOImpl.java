package com.flowers.portal.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.flowers.portal.dao.OrderDAO;
import com.flowers.server.entity.OrderProduct;
import com.flowers.server.entity.ProcessedOrder;
import com.flowers.server.entity.Rating;
import com.flowers.server.entity.Route4MeIntake;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.User;
import com.flowers.server.entity.Userrole;
import com.flowers.server.search.SearchResultSet;

@Transactional
@Repository("orderDAO")
public class OrderDAOImpl implements OrderDAO {
	
    @Autowired private HibernateTemplate hibernateTemplate;

    @SuppressWarnings("unchecked")
	@Override
	public Shop getShopByShopCode(String shopCode) {

    	Shop result = new Shop();
    	Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
    	
    	List<Shop> results =  session.createQuery("from Shop s where shopCode = :shopCode").setParameter("shopCode", shopCode).list();
    	
    	if (results.size()>0){
			result = results.get(0);
		}
    	
    	return result;
	}
    
    @SuppressWarnings("unchecked")
	@Override
   	public List<Shop> getShopsByShopName(String shopName) {

		List<Shop> results = new ArrayList<Shop>();
		Session session = hibernateTemplate.getSessionFactory().getCurrentSession(); 
       	results =  session.createQuery("from Shop s where shopName = :shopName").setParameter("shopName", shopName).list();
       	
       	
       	return results;
   	}
    
    @SuppressWarnings("unchecked")
	@Override
	public List<Rating> getRatingsByShopId(long shopId){
    	
		List<Rating> results = new ArrayList<Rating>();
		Session session = hibernateTemplate.getSessionFactory().getCurrentSession(); 
		results  = session.createQuery("from Rating r where reviewed_shopid = :shopId").setParameter("shopId", shopId).list();
		
		return results;

    }
    
    @SuppressWarnings("unchecked")
	@Override
	public List<Rating> getRatingsByShopCode(String shopcode){
    	
		List<Rating> results = new ArrayList<Rating>();
		Session session = hibernateTemplate.getSessionFactory().getCurrentSession(); 
		results  = session.createQuery("from Rating r where reviewed_shopcode = :shopCode").setParameter("shopCode", shopcode).list();
		
		return results;

    }


	
	@SuppressWarnings("unchecked")
	@Override
	public User getUserByUserName(String userName) {
		
		User result = new User();
		Session session = hibernateTemplate.getSessionFactory().getCurrentSession(); 
		List<User> dbUser = session.createQuery("from User where userName = :userName").setParameter("userName", userName).list();
		if (dbUser.size()>0){
			result = dbUser.get(0);
		}
		
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Userrole> getUserrolesByUserId(String userId) {
		
		Session session = hibernateTemplate.getSessionFactory().getCurrentSession(); 
		List<Userrole> results = session.createQuery("from Userrole u where userByUserId = :userId").setParameter("userId", userId).list();
		
		return results;
	}

	@Override
	public void setLikeDislike(String shopCode, String userName, long shopId,
			boolean liked) {

	}

	@Override
	public void commitSearchLog(SearchResultSet<Document> result) {}

	@Override
	public void commitSearchPageLog(long queryId, int pageNumber,
			List<Document> page) {}

	@Override
	public void commitClickLog(String shopCode) {}

	@Override
	public void processLikeDislike(SearchResultSet<Document> results){}

	@Override
	public void saveUser(User user) {

		hibernateTemplate.saveOrUpdate(user);

	}

	@Override
	public void saveOrder(ProcessedOrder order) {

		hibernateTemplate.saveOrUpdate(order);

	}
	
	@Override
	public void saveRating(Rating rating) {

		hibernateTemplate.saveOrUpdate(rating);

	}
	
	@Override
	public void saveRoute4Me(Route4MeIntake route4Me) {
		hibernateTemplate.saveOrUpdate(route4Me);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Route4MeIntake> getRoute4MeByUnprocessed() {
		
		List<Route4MeIntake> resultsList = new ArrayList<Route4MeIntake>();
		
		Session session = hibernateTemplate.getSessionFactory().getCurrentSession(); 
		List<Route4MeIntake> results = session.createQuery("from Route4MeIntake r where bloomlinkProcessed = 0 and attempts < 5").list();
		
		resultsList.addAll(results);
		
		results = session.createQuery("from Route4MeIntake r where route4MeProcessed = 0 and bloomlinkProcessed = 1 and attempts < 5").list();
		
		resultsList.addAll(results);
		
		return resultsList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Route4MeIntake> getRoute4MeByEmail(String email) {
		
		List<Route4MeIntake> resultsList = new ArrayList<Route4MeIntake>();
		
		Session session = hibernateTemplate.getSessionFactory().getCurrentSession(); 
		List<Route4MeIntake> results = session.createQuery("from Route4MeIntake r where email = :email and bloomlinkProcessed = 1 and route4MeProcessed = 1 ").setParameter("email", email).list();
		
		resultsList.addAll(results);
		
		return resultsList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String getExistingSecretPasswordByShopCode(String shopCode) {
		
		String result = "";
		
		Session session = hibernateTemplate.getSessionFactory().getCurrentSession(); 
		List<Route4MeIntake> results = session.createQuery("from Route4MeIntake r where shopCode = :shopCode").setParameter("shopCode", shopCode).list();
		
		if(results != null && results.size() > 0)
			result = results.get(0).getSecretPassword();
		
		return result;
	}
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}
	public SessionFactory getSessionFactory() {
		return hibernateTemplate.getSessionFactory();
	}

	@Override
	public void updateRatingAbuse(long ratingId) {
		
		Rating rating = (Rating) hibernateTemplate.getSessionFactory().getCurrentSession().createQuery("from Rating r where rating_id = :ratingId").setParameter("ratingId", ratingId).getSingleResult();
		rating.setAbuse(rating.isAbuse() + 1);
		hibernateTemplate.update(rating);
	}

	/**
	 *
	 */
	@Override
	public void saveOrderProduct(OrderProduct orderProduct) {
		hibernateTemplate.saveOrUpdate(orderProduct);
		
	}

	

}
