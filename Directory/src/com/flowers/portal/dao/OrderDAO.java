package com.flowers.portal.dao;

import java.util.List;

import org.apache.lucene.document.Document;
import com.flowers.server.entity.OrderProduct;
import com.flowers.server.entity.ProcessedOrder;
import com.flowers.server.entity.Rating;
import com.flowers.server.entity.Route4MeIntake;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.User;
import com.flowers.server.entity.Userrole;
import com.flowers.server.search.SearchResultSet;

public interface OrderDAO {
	
	User getUserByUserName(String userName);

	List<Userrole> getUserrolesByUserId(String userId);
	
	void setLikeDislike( String shopCode, String userName, long shopId, boolean liked);
	
	void commitSearchLog(SearchResultSet<Document> result);
	
	void commitSearchPageLog(long queryId, int pageNumber, List<Document> page);
	
	void commitClickLog(String shopCode);
	
	void processLikeDislike(SearchResultSet<Document> results) throws Exception;
	
	void saveUser(User user);
	
	void saveOrder(ProcessedOrder order);
	
	void saveOrderProduct(OrderProduct orderProduct);
	
	List<Rating> getRatingsByShopId(long shopId);
	
	List<Rating> getRatingsByShopCode(String shopCode);

	void saveRating(Rating rating); 
	
	Shop getShopByShopCode(String shopCode);
	
	List<Shop> getShopsByShopName(String shopName);
	
	void updateRatingAbuse(long ratingId);

	void saveRoute4Me(Route4MeIntake route4Me);

	List<Route4MeIntake> getRoute4MeByUnprocessed();

	List<Route4MeIntake> getRoute4MeByEmail(String email);

	String getExistingSecretPasswordByShopCode(String shopCode);

}
