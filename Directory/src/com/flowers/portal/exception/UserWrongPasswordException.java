/**
 * 
 */
package com.flowers.portal.exception;

/**
 * @author Danil svirchtchev
 *
 */
public class UserWrongPasswordException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public UserWrongPasswordException() {
		super("Invalid user name or password.  Please try again");
	}
}
