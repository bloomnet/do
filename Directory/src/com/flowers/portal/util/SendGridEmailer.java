package com.flowers.portal.util;

import com.sendgrid.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class SendGridEmailer {
	

   public SendGridEmailer(String body, int group) throws IOException {
	   
	    Properties properties = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream("/opt/apps/properties/directory.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
	   try {
		   properties.load(input);
		} catch (IOException e) {
			e.printStackTrace();
		}
	   
	   if(properties.getProperty("emailIsActive").equals("true")) {
		   String primaryTo = properties.getProperty("emailPrimary");
		   String emailTo = null;
		   if(group == 1)
			   emailTo = properties.getProperty("emailList1");
		   else if(group == 2)
			   emailTo = properties.getProperty("emailList2");
		   else if(group ==3)
			   emailTo = properties.getProperty("emailList3");
		   String[] emailList = emailTo.split(",");
		   String subject = null;
		   if(group == 1)
			   subject = properties.getProperty("emailSubject1");
		   else if(group == 2)
			   subject = properties.getProperty("emailSubject2");
		   else if(group ==3)
			   subject = properties.getProperty("emailSubject3");
		   
		   
	       Email from = new Email("Directory@bloomnet.net");
	       Email to = new Email(primaryTo);
	       
	       Personalization p = new Personalization();
	       for(int ii=0; ii<emailList.length; ++ii)
	    	   p.addTo(new Email(emailList[ii]));
	
	       Content content = new Content("text/html", body);
	
	       Mail mail = new Mail(from, subject, to, content);
	       mail.addPersonalization(p);

	       SendGrid sg = new SendGrid(properties.getProperty("apikey"));
	       Request request = new Request();
	
	       request.setMethod(Method.POST);
	       request.setEndpoint("mail/send");
	       request.setBody(mail.build());
	
	       sg.api(request);
	   }

   }
}