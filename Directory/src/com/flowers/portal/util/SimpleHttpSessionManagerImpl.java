/*
 * SessionManager.java
 *
 * Created on October 07, 2011
 */

package com.flowers.portal.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

//import com.bloomnet.bom.common.entity.Bomorder;
//import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.flowers.portal.service.SessionManager;

/**
 * Manages setting, getting and removal of application
 * object(s) in a HTTP session.
 *
 * @author Danil Svirchtchev
 *
 */
public class SimpleHttpSessionManagerImpl implements SessionManager {
    
    // Define a static logger variable
    static Logger logger = Logger.getLogger( SimpleHttpSessionManagerImpl.class );
    
    public static final String APPLICATION_USER = "USER";
    public static final String ORDER     = "BLOOMNET_ORDER";
    public static final String SEARCH_RESULT    = "SEARCHRESULT";
    public static final String SELECTED_DATE     = "SELECTEDDATE";
    public static final String SELECTED_ENDDATE     = "SELECTEDENDDATE";



    
    //TODO: Fix incoming and outgoing types 
    /* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.utils.SessionManager#getApplicationUser(javax.servlet.http.HttpServletRequest)
	 */
    @Override
	public Object getApplicationUser( HttpServletRequest request ) {
        
        return ( Object ) request.getSession(true).getAttribute( APPLICATION_USER );
    }
    /* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.utils.SessionManager#setApplicationUser(javax.servlet.http.HttpServletRequest, java.lang.Object)
	 */
    @Override
	public void setApplicationUser( HttpServletRequest request, Object applicationUser ) {
        
        request.getSession(true).setAttribute( APPLICATION_USER, applicationUser );
    }
    public void removeApplicationUser( HttpServletRequest request ) {
        
        request.getSession().removeAttribute( APPLICATION_USER );
    }
   
   
    
    //TODO: Fix incoming and outgoing types 
    /* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.utils.SessionManager#getMasterOrder(javax.servlet.http.HttpServletRequest)
	 */
    @Override
	public Object getOrder( HttpServletRequest request ) {
        
        return ( Object ) request.getSession(false).getAttribute(ORDER );
    }
    /* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.utils.SessionManager#setMasterOrder(javax.servlet.http.HttpServletRequest, java.lang.Object)
	 */
    @Override
	public void setOrder( HttpServletRequest request, Object masterOrder ) {
        
        request.getSession(false).setAttribute( ORDER , masterOrder );
    }
    /* (non-Javadoc)
     * @see com.bloomnet.bom.mvc.service.SessionManager#removeMasterOrder(javax.servlet.http.HttpServletRequest)
     */
    @Override
    public void removeOrder( HttpServletRequest request ) {
        
        request.getSession().removeAttribute( ORDER );
    }
    
  
    

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.SessionManager#getSearchResults(javax.servlet.http.HttpServletRequest)
	 */
/*	@Override
	@SuppressWarnings("unchecked")
	public List<WebAppOrder> getSearchResults( HttpServletRequest request ) {
		return (List<WebAppOrder>) request.getSession().getAttribute( SEARCH_RESULT );
	}
	 (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.SessionManager#setSearchResults(javax.servlet.http.HttpServletRequest, java.util.List)
	 
	@Override
	public void setSearchResults( HttpServletRequest request, List<? extends Bomorder> searchResults ) {
		request.getSession().setAttribute( SEARCH_RESULT , searchResults );
	}
    public void removeSearchResults( HttpServletRequest request ) {
        
        request.getSession().removeAttribute( SEARCH_RESULT );
    }
*/    
    
	
    @Override
	public Object getSelectedDate( HttpServletRequest request ) {
        
        return ( Object ) request.getSession(false).getAttribute( SELECTED_DATE );
    }
    /* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.utils.SessionManager#setMasterOrder(javax.servlet.http.HttpServletRequest, java.lang.Object)
	 */
    @Override
	public void setSelectedDate( HttpServletRequest request, Object masterOrder ) {
        
        request.getSession(false).setAttribute( SELECTED_DATE , masterOrder );
    }
    /* (non-Javadoc)
     * @see com.bloomnet.bom.mvc.service.SessionManager#removeMasterOrder(javax.servlet.http.HttpServletRequest)
     */
    @Override
    public void removeSelectedDate( HttpServletRequest request ) {
        
        request.getSession().removeAttribute( SELECTED_DATE );
    }
    @Override
	public Object getSelectedEndDate( HttpServletRequest request ) {
        
        return ( Object ) request.getSession(false).getAttribute( SELECTED_ENDDATE );
    }
    /* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.utils.SessionManager#setMasterOrder(javax.servlet.http.HttpServletRequest, java.lang.Object)
	 */
    @Override
	public void setSelectedEndDate( HttpServletRequest request, Object masterOrder ) {
        
        request.getSession(false).setAttribute( SELECTED_ENDDATE , masterOrder );
    }
    /* (non-Javadoc)
     * @see com.bloomnet.bom.mvc.service.SessionManager#removeMasterOrder(javax.servlet.http.HttpServletRequest)
     */
    @Override
    public void removeSelectedEndDate( HttpServletRequest request ) {
        
        request.getSession().removeAttribute( SELECTED_ENDDATE );
    }
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.utils.SessionManager#deleteAllApplicationObjectsFromSession(javax.servlet.http.HttpServletRequest)
	 */
    @Override
	public void deleteAllApplicationObjectsFromSession( HttpServletRequest request ) {
        
        Class<SimpleHttpSessionManagerImpl> c = SimpleHttpSessionManagerImpl.class;
        
        @SuppressWarnings("rawtypes")
		Class[]  parameterTypes = new Class[]  { HttpServletRequest.class };
        Object[] arguments      = new Object[] { request };
        
        Method[] methods = c.getMethods();
        
        try {
            
            for ( int i = 0; i < methods.length; i++ ) {
                
                if ( methods[i].getName().startsWith("remove") ) {
                    
                    Method removeMethod = c.getMethod( methods[i].getName(), parameterTypes );
                    removeMethod.invoke ( new SimpleHttpSessionManagerImpl(), arguments );
                }
            }
            
        } 
        catch ( NoSuchMethodException e ) { 
            
            logger.error( e ); 
        } 
        catch ( IllegalAccessException e ) { 
            
            logger.error( e ); 
        } 
        catch ( InvocationTargetException e ) { 
            
            logger.error( e ); 
        }
    }
}
