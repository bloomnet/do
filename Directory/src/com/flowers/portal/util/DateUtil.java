package com.flowers.portal.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	private static SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
	private static SimpleDateFormat xmlFormat = new SimpleDateFormat("yyyyMMddhhmmss");
	
	@SuppressWarnings("deprecation")
	public static String getCurrentDate(long l) {
		Date date = new Date(l);
		if(date.getHours() > 14) date.setDate(date.getDate()+1);
		return toString(date);
	}
	public static Date toDate(String dateStr) {
		try { 
			return format.parse(dateStr);
		}
		catch (Exception e) {
			return null;  
		}
	}
	public static String formatDateString(String in) {
		in = in.replaceAll("-","/");
		String[] temp = in.split("/");
		String finalDate = "";
		for(int ii=0; ii<temp.length; ++ii){
			if(temp[ii].length() == 1) finalDate += "0"+temp[ii];
			else finalDate += temp[ii];
		}
		if(finalDate.length() == 8) return finalDate.substring(0,2)+"/"+finalDate.substring(2,4)+"/"+finalDate.substring(4);
		return finalDate;
	}
	public static String toString(Date date) {
		return format.format(date);
	}
	
	public static String toXmlFormatString(Date date) {
		return xmlFormat.format(date);
	}
}
