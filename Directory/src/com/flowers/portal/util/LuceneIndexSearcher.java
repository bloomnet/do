package com.flowers.portal.util;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TopFieldDocs;
import org.apache.lucene.search.TotalHitCountCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

public class LuceneIndexSearcher {
	private String indexPath;
	private IndexSearcher searcher;
	private Sort sort;
	private int pageSize;
	private int page;
	private int total;
	
	public LuceneIndexSearcher(String indexPath) {
		this.indexPath = indexPath;
	}
	
	public Document document(Query query) throws IOException {
		IndexSearcher searcher = getSearcher();
		TopDocs topDocs = searcher.search(query, 1);
		return searcher.doc(topDocs.scoreDocs[0].doc);
	}
	public List<Document> documents(Query query) throws IOException {
		List<Document> results = new ArrayList<Document>();
		IndexSearcher searcher = getSearcher();
		TotalHitCountCollector collector = new TotalHitCountCollector();
		searcher.search(query, collector);
		total = collector.getTotalHits();
		if(collector.getTotalHits() > 0) {
			int start = (page*pageSize)-pageSize;
			int end = pageSize != 0 ? start+pageSize : total;	
			if(sort != null) {
				TopFieldDocs topDocs = searcher.search(query, total, sort);
				for(int i=start; i < end; i++) {
					results.add(searcher.doc(topDocs.scoreDocs[i].doc));
				}
			} else {
				TopDocs topDocs = searcher.search(query, total);
				for(int i=start; i < end; i++) {
					results.add(searcher.doc(topDocs.scoreDocs[i].doc));
				}
			}
		}
		return results;
	}
	
	protected IndexSearcher getSearcher() throws IOException {
		if(searcher == null || searcher.getIndexReader() == null) {
			Directory dir = FSDirectory.open(Paths.get(indexPath));
			IndexReader reader = DirectoryReader.open(dir);
			searcher = new IndexSearcher(reader);
		} else if(!((DirectoryReader) searcher.getIndexReader()).isCurrent()) {
				searcher.getIndexReader().close();
				Directory dir = FSDirectory.open(Paths.get(indexPath));
				IndexReader reader = DirectoryReader.open(dir);
				searcher = new IndexSearcher(reader);
		}
		return searcher;
	}

	public Sort getSort() {
		return sort;
	}

	public void setSort(Sort sort) {
		this.sort = sort;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
	
}
