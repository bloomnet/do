 package com.flowers.portal.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.lucene.document.Document;

import com.flowers.server.responseobjects.JSONCitiesResponse;
import com.flowers.server.responseobjects.JSONCitiesZipResponse;
import com.flowers.server.responseobjects.JSONCountriesResponse;
import com.flowers.server.responseobjects.JSONFacilitiesResponse;
import com.flowers.server.responseobjects.JSONFacilityListingResponse;
import com.flowers.server.responseobjects.JSONLandingAdsResponse;
import com.flowers.server.responseobjects.JSONListingResponse;
import com.flowers.server.responseobjects.JSONResultsAdsResponse;
import com.flowers.server.responseobjects.JSONStatesResponse;

public class ConvertLuceneToJSON {
	
	
	public static JSONListingResponse convertListing(Document d){
				
		JSONListingResponse json = new JSONListingResponse();
		
		json.setShopAddress1(d.get("address1"));
		json.setShopAddress2(d.get("address2"));
		json.setListingAdSize(d.get("ad_size"));
		json.setListingCity(d.get("city"));
		json.setShopContact(d.get("contact"));
		json.setListingDate(d.get("date"));
		json.setShopDeliveryFee(d.get("delivery"));
		json.setListingDeliveryCharge(d.get("delivery_charge"));
		json.setListingEntryCode(d.get("entry_code"));
		if(d.get("facilities") != null)
			json.setFacilitiesServiced(d.get("facilities").replaceAll(" ", ","));
		json.setShopFax(d.get("fax"));
		json.setListingFreeAd(d.get("free_ad"));
		json.setListingHighlightColor(d.get("highlight_color"));
		json.setListingImageCount(d.get("image_count"));
		json.setShopLatitude(d.get("latitude"));
		json.setShopLatitude(d.get("longitude"));
		json.setListingType(d.get("listing_type"));
		json.setListingMinimum(d.get("minimum"));
		String productMins = "";
		String[] tempMins = d.get("minimums").replaceAll("H", "").replaceAll("\\.0", "\\.00").split(" ");
		if(!tempMins[0].equals("0.00")) productMins += "Arrangements: $" + tempMins[0] + ",";
		if(!tempMins[4].equals("0.00")) productMins += "Balloons: $" + tempMins[4]+ ",";
		if(!tempMins[2].equals("0.00")) productMins += "Blooms: $" + tempMins[1]+ ",";
		if(!tempMins[5].equals("0.00")) productMins += "Candy: $" + tempMins[5]+ ",";
		if(!tempMins[3].equals("0.00")) productMins += "Dozen Roses: $" + tempMins[2]+ ",";
		if(!tempMins[6].equals("0.00")) productMins += "Dried/Silk Arrangements: $" + tempMins[6]+ ",";
		if(!tempMins[7].equals("0.00")) productMins += "Fruit: $" + tempMins[7]+ ",";
		if(!tempMins[3].equals("0.00")) productMins += "Funeral Arrangements: $" + tempMins[3]+ ",";
		if(!tempMins[8].equals("0.00")) productMins += "Gourmet Foods: $" + tempMins[8]+ ",";
		if(productMins.endsWith(",")) productMins = productMins.substring(0,productMins.length() - 1);
		if(productMins.equals("")) productMins = null;
		json.setShopProductMinimums(productMins);
		json.setShopMinimumOrder(d.get("min_order"));
		json.setShopPhone(d.get("phone"));
		json.setShopCity(d.get("shopcity"));
		json.setShopCode(d.get("shopcode"));
		json.setShopName(d.get("shopname"));
		json.setShopState(d.get("shopstate"));
		json.setListingState(d.get("state"));
		json.setListingStateCode(d.get("state_code"));
		json.setShopZip(d.get("zip"));
		json.setShopWebsite(d.get("website"));
		if(d.get("logo") != null && !d.get("logo").equals(""))
			json.setShopLogo(d.get("logo").replaceAll(" ", "\\%20"));
		else
			json.setShopLogo(d.get("logo"));
		json.setListingBid(d.get("bid"));
		
		String featuredListing = d.get("featured_listing");
		if(featuredListing != null && !featuredListing.equals("") && featuredListing.equalsIgnoreCase("true")){
			json.setListingCitySponsorAd(d.get("featured_listing"));
		}
		
		String preferredListing = d.get("preferred_listing");
		if(preferredListing != null && !preferredListing.equals("") && preferredListing.equalsIgnoreCase("true")){
			json.setListingCityPreferredAd(preferredListing);
		}
		
		String socialMediaString = "";
		if(d.get("facebook") != null && !d.get("facebook").equals("")) socialMediaString += "Facebook: " + d.get("facebook") + ",";
		if(d.get("googleplus") != null && !d.get("googleplus").equals("")) socialMediaString += "GooglePlus: " + d.get("googleplus") + ",";
		if(d.get("twitter") != null && !d.get("twitter").equals("")) socialMediaString += "Twitter: " + d.get("twitter") + ",";
		if(d.get("linkedin") != null && !d.get("linkedin").equals("")) socialMediaString += "LinkedIn: " + d.get("linkedin") + ",";
		if(d.get("instagram") != null && !d.get("instagram").equals("")) socialMediaString += "Instagram: " + d.get("instagram") + ",";
		if(d.get("pinterest") != null && !d.get("pinterest").equals("")) socialMediaString += "Pinterest: " + d.get("pinterest") + ",";
		if(d.get("yelp") != null && !d.get("yelp").equals("")) socialMediaString += "Yelp: " + d.get("yelp") + ",";
		if(socialMediaString.endsWith(",")) socialMediaString = socialMediaString.substring(0,socialMediaString.length() - 1);
		if(socialMediaString.equals("")) socialMediaString = null;
		json.setSocialMedia(socialMediaString);
		if(d.get("consumer_image") != null && ! d.get("consumer_image").equals(""))
			json.setConsumerImage(d.get("consumer_image").replaceAll(" ", "\\%20"));
		else
			json.setConsumerImage("RandomAd" + String.valueOf(ThreadLocalRandom.current().nextInt(1, 5)) + ".png");
		String localProducts = "";
		if(d.get("local_products") != null){
			String[] localProductsTotalString = d.get("local_products").split(",");
			for(int ii=0; ii<localProductsTotalString.length; ++ii){
				String[] localProductsString = localProductsTotalString[ii].split("--");
				if(localProductsString.length == 3)
					localProducts += "ProductName: " + localProductsString[0] + ",ProductPrice: $" + localProductsString[1].replaceAll("\\.0","\\.00") + ",ProductImage: " + localProductsString[2] + ";";			
			}
		}
		if(localProducts.endsWith(";")) localProducts = localProducts.substring(0,localProducts.length() - 1);
		if(localProducts.equals("")) localProducts = null;
		json.setShopCustomProducts(localProducts);
		
		
		return json;
	}
	
	public static JSONFacilityListingResponse convertFacilityListing(Document d){
		
		JSONFacilityListingResponse json = new JSONFacilityListingResponse();
		
		json.setShopAddress1(d.get("address1"));
		json.setShopAddress2(d.get("address2"));
		json.setListingAdSize(d.get("ad_size"));
		json.setListingCity(d.get("city"));
		json.setShopContact(d.get("contact"));
		json.setListingDate(d.get("date"));
		json.setShopDeliveryFee(d.get("delivery"));
		json.setListingDeliveryCharge(d.get("delivery_charge"));
		json.setListingEntryCode(d.get("entry_code"));
		if(d.get("facilities") != null)
			json.setFacilitiesServiced(d.get("facilities").replaceAll(" ", ","));
		json.setShopFax(d.get("fax"));
		json.setListingFreeAd(d.get("free_ad"));
		json.setListingHighlightColor(d.get("highlight_color"));
		json.setListingImageCount(d.get("image_count"));
		json.setShopLatitude(d.get("latitude"));
		json.setShopLatitude(d.get("longitude"));
		json.setListingType(d.get("listing_type"));
		json.setListingMinimum(d.get("minimum"));
		String productMins = "";
		String[] tempMins = d.get("minimums").replaceAll("H", "").replaceAll("\\.0", "\\.00").split(" ");
		if(!tempMins[0].equals("0.00")) productMins += "Arrangements: $" + tempMins[0] + ",";
		if(!tempMins[4].equals("0.00")) productMins += "Balloons: $" + tempMins[4]+ ",";
		if(!tempMins[2].equals("0.00")) productMins += "Blooms: $" + tempMins[1]+ ",";
		if(!tempMins[5].equals("0.00")) productMins += "Candy: $" + tempMins[5]+ ",";
		if(!tempMins[3].equals("0.00")) productMins += "Dozen Roses: $" + tempMins[2]+ ",";
		if(!tempMins[6].equals("0.00")) productMins += "Dried/Silk Arrangements: $" + tempMins[6]+ ",";
		if(!tempMins[7].equals("0.00")) productMins += "Fruit: $" + tempMins[7]+ ",";
		if(!tempMins[3].equals("0.00")) productMins += "Funeral Arrangements: $" + tempMins[3]+ ",";
		if(!tempMins[8].equals("0.00")) productMins += "Gourmet Foods: $" + tempMins[8]+ ",";
		if(productMins.endsWith(",")) productMins = productMins.substring(0,productMins.length() - 1);
		if(productMins.equals("")) productMins = null;
		json.setShopProductMinimums(productMins);
		json.setShopMinimumOrder(d.get("min_order"));
		json.setShopPhone(d.get("phone"));
		json.setShopCity(d.get("shopcity"));
		json.setShopCode(d.get("shopcode"));
		json.setShopName(d.get("shopname"));
		json.setShopState(d.get("shopstate"));
		json.setListingState(d.get("state"));
		json.setListingStateCode(d.get("state_code"));
		json.setShopZip(d.get("zip"));
		json.setShopWebsite(d.get("website"));
		if(d.get("logo") != null && !d.get("logo").equals(""))
			json.setShopLogo(d.get("logo").replaceAll(" ", "\\%20"));
		else
			json.setShopLogo(d.get("logo"));
		json.setListingBid(d.get("bid"));
		
		String featuredListing = d.get("featured_listing");
		if(featuredListing != null && !featuredListing.equals("") && featuredListing.equalsIgnoreCase("true")){
			json.setListingCitySponsorAd(d.get("featured_listing"));
		}
		
		String preferredListing = d.get("preferred_listing");
		if(preferredListing != null && !preferredListing.equals("") && preferredListing.equalsIgnoreCase("true")){
			json.setListingCityPreferredAd(preferredListing);
		}
		
		String socialMediaString = "";
		if(d.get("facebook") != null && !d.get("facebook").equals("")) socialMediaString += "Facebook: " + d.get("facebook") + ",";
		if(d.get("googleplus") != null && !d.get("googleplus").equals("")) socialMediaString += "GooglePlus: " + d.get("googleplus") + ",";
		if(d.get("twitter") != null && !d.get("twitter").equals("")) socialMediaString += "Twitter: " + d.get("twitter") + ",";
		if(d.get("linkedin") != null && !d.get("linkedin").equals("")) socialMediaString += "LinkedIn: " + d.get("linkedin") + ",";
		if(d.get("instagram") != null && !d.get("instagram").equals("")) socialMediaString += "Instagram: " + d.get("instagram") + ",";
		if(d.get("pinterest") != null && !d.get("pinterest").equals("")) socialMediaString += "Pinterest: " + d.get("pinterest") + ",";
		if(d.get("yelp") != null && !d.get("yelp").equals("")) socialMediaString += "Yelp: " + d.get("yelp") + ",";
		if(socialMediaString.endsWith(",")) socialMediaString = socialMediaString.substring(0,socialMediaString.length() - 1);
		if(socialMediaString.equals("")) socialMediaString = null;
		json.setSocialMedia(socialMediaString);
		if(d.get("consumer_image") != null && ! d.get("consumer_image").equals(""))
			json.setConsumerImage(d.get("consumer_image").replaceAll(" ", "\\%20"));
		else
			json.setConsumerImage("RandomAd" + String.valueOf(ThreadLocalRandom.current().nextInt(1, 5)) + ".png");
		String localProducts = "";
		if(d.get("local_products") != null){
			String[] localProductsTotalString = d.get("local_products").split(",");
			for(int ii=0; ii<localProductsTotalString.length; ++ii){
				String[] localProductsString = localProductsTotalString[ii].split("--");
				if(localProductsString.length == 3)
					localProducts += "ProductName: " + localProductsString[0] + ",ProductPrice: $" + localProductsString[1].replaceAll("\\.0","\\.00") + ",ProductImage: " + localProductsString[2] + ";";			
			}
		}
		if(localProducts.endsWith(";")) localProducts = localProducts.substring(0,localProducts.length() - 1);
		if(localProducts.equals("")) localProducts = null;
		json.setShopCustomProducts(localProducts);
		
		
		return json;
	}
	
	public static JSONFacilitiesResponse convertFacilities(Document d){
		
		JSONFacilitiesResponse json = new JSONFacilitiesResponse();
		
		json.setFacilityAddress1(d.get("address1"));
		json.setFacilityAddress2(d.get("address2"));
		json.setFacilityCity(d.get("city"));
		json.setFacilityState(d.get("state"));
		json.setFacilityZip(d.get("zip"));
		json.setFacilityID(d.get("id"));
		json.setFacilityName(d.get("name"));
		json.setFacilityPhone(d.get("phone"));
		json.setFacilityType(d.get("type"));
		
		return json;
	}
	
	public static JSONStatesResponse convertStates(Document d){
		
		JSONStatesResponse json = new JSONStatesResponse();
		
		json.setStateName(d.get("name"));
		json.setStateShortName(d.get("short_name"));
		
		return json;
	}
	
	public static JSONLandingAdsResponse convertLandingAds(Document d){
		
		JSONLandingAdsResponse json = new JSONLandingAdsResponse();
		
		if(d.get("file_name") != null && !d.get("file_name").equals(""))
			json.setFileName(d.get("file_name").replaceAll(" ", "\\%20"));
		else
			json.setFileName(d.get("file_name"));
		json.setWebsite(d.get("website"));
		json.setAdShape(d.get("adShape").toUpperCase());
		
		return json;
	}
	
	public static JSONResultsAdsResponse convertResultsAds(Document d){
		
		JSONResultsAdsResponse json = new JSONResultsAdsResponse();
		
		if(d.get("file_name") != null && !d.get("file_name").equals(""))
			json.setFileName(d.get("file_name").replaceAll(" ", "\\%20"));
		else
			json.setFileName(d.get("file_name"));
		json.setWebsite(d.get("website"));
		
		return json;
	}
	
	public static JSONCitiesResponse convertCities(Document d){
		
		JSONCitiesResponse json = new JSONCitiesResponse();
		
		json.setCityName(d.get("name").toUpperCase());
		
		return json;
	}
	
	public static JSONCitiesZipResponse convertZipCities(Document d){
		
		JSONCitiesZipResponse json = new JSONCitiesZipResponse();

		json.setCityName(d.get("name"));
		json.setStateName(d.get("state.name"));
		json.setStateShortName(d.get("state.short_name"));
		
		return json;
	}
	
	public static JSONCountriesResponse convertCountries(Document d){
		
		JSONCountriesResponse json = new JSONCountriesResponse();

		json.setCountryName(d.get("country.name"));
		json.setCountryShortName(d.get("country.short_name"));
		
		return json;
	}
}
