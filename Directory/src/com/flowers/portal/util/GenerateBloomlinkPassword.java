package com.flowers.portal.util;

import java.security.SecureRandom;

public class GenerateBloomlinkPassword {
	
    public String generateBloomlinkPassword(int length) {
    	
    	
    	String allowedOpeningChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        String allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        String allowedEndingChars = "0123456789";


        SecureRandom random = new SecureRandom();
        StringBuilder password = new StringBuilder();
        
        int randomIndex = random.nextInt(allowedOpeningChars.length());
        password.append(allowedOpeningChars.charAt(randomIndex));

        for (int i = 1; i < length-1; i++) {
            randomIndex = random.nextInt(allowedChars.length());
            password.append(allowedChars.charAt(randomIndex));
        }
        randomIndex = random.nextInt(allowedEndingChars.length());
        password.append(allowedEndingChars.charAt(randomIndex));
        
        return password.toString();
    }
}
