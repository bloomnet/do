package com.flowers.portal.util;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;

public class MyHttpClient{ 
	
	public CloseableHttpClient getNewHttpClient(boolean production) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException {
		if(production){
			
			SSLContext context = SSLContexts.createDefault();
			HttpClientBuilder builder = HttpClientBuilder.create();
		    SSLConnectionSocketFactory sslConnectionFactory = new SSLConnectionSocketFactory(context);
		    builder.setSSLSocketFactory(sslConnectionFactory);
	
		    Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
		            .register("https", sslConnectionFactory)
		            .build();
		    HttpClientConnectionManager ccm = new BasicHttpClientConnectionManager(registry);
		    builder.setConnectionManager(ccm);
	
		    return builder.build();
		}else{
			TrustManager[] trustAllCerts = new TrustManager[]{
	    		    new X509TrustManager() {
	    		        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
	    		            return null;
	    		        }
	    		        public void checkClientTrusted(
	    		            java.security.cert.X509Certificate[] certs, String authType) {
	    		        }
	    		        public void checkServerTrusted(
	    		            java.security.cert.X509Certificate[] certs, String authType) {
	    		        }
	    		    }
	    		};

			SSLContext context = SSLContext.getInstance("TLSv1.2");
			context.init(null, trustAllCerts, new java.security.SecureRandom());
		    HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
			HttpClientBuilder builder = HttpClientBuilder.create();
		    SSLConnectionSocketFactory sslConnectionFactory = new SSLConnectionSocketFactory(context, new NoopHostnameVerifier());
		    builder.setSSLSocketFactory(sslConnectionFactory);

		    Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
		            .register("https", sslConnectionFactory)
		            .build();

		    HttpClientConnectionManager ccm = new BasicHttpClientConnectionManager(registry);
		    builder.setConnectionManager(ccm);
		    return builder.build();
		}
	}
}


