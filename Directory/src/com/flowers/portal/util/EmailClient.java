package com.flowers.portal.util;

import com.sendgrid.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class EmailClient {
	

   public EmailClient(String body, String toEMail) throws IOException {
	   
	    Properties properties = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream("/opt/apps/properties/directory.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
	   try {
		   properties.load(input);
		} catch (IOException e) {
			e.printStackTrace();
		}
	   
	   if(properties.getProperty("emailIsActive").equals("true")) {
		   
		   
	       Email from = new Email("bloomnetdriver@bloomnet.net");
	       Email to = new Email(toEMail);
	       	
	       Content content = new Content("text/html", body);
	
	       Mail mail = new Mail(from, "Welcome to BloomNet Driver!", to, content);

	       SendGrid sg = new SendGrid(properties.getProperty("apikey"));
	       Request request = new Request();
	
	       request.setMethod(Method.POST);
	       request.setEndpoint("mail/send");
	       request.setBody(mail.build());
	
	       sg.api(request);
	   }

   }
}