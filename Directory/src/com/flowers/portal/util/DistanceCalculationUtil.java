package com.flowers.portal.util;


/**
 * <p>
 * This routine calculates the distance between two points (given the
 * latitude/longitude of those points).
 * </p>
 * <p>
 * This code is based on Haversine formula, used to calculate great-circle 
 * distance between two points on a sphere from their latitude and
 * longitude.
 * </p>
 * <p>
 * Ref: http://en.wikipedia.org/wiki/Haversine_formula
 * </p>
 */
public class DistanceCalculationUtil {
	/**
	 * Result type for distance calculation 
	 */
	public static enum CalculationType {KILOMETERS, MILES, NAUTICAL_MILES}
	/**
	 * Earth radius measured in kilometers. Ref:
	 * http://en.wikipedia.org/wiki/Earth_radius
	 */
	public static int EARTH_RADIUS_KM = 6371;
	
	/**
	 * 
	 * Given two latitude and longitude coordinates, calculate distance between
	 * two points.
	 * 
	 * @param lat1 Latidude from point one
	 * @param lon1 Longitude from point one 
	 * @param lat2 Latitude from point two
	 * @param lon2 Longitude from point two
	 * @param calculationType Result type measurement for distance calculation
	 * @return
	 */
	public double calculate(double lat1, double lon1, double lat2, double lon2, CalculationType calculationType){
		double dLat = Math.toRadians(lat2-lat1);  
		double dLon = Math.toRadians(lon2-lon1);  
		double a = Math.sin(dLat/2) * Math.sin(dLat/2) +  
		              Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *  
		              Math.sin(dLon/2) * Math.sin(dLon/2);  
		double c = 2 * Math.asin(Math.sqrt(a));
		//distance in kilometers
		double distance = EARTH_RADIUS_KM * c;
		if(CalculationType.MILES.equals(calculationType)){
			//convert distance to miles
			distance = distance * 0.62137119;
		}else if (CalculationType.NAUTICAL_MILES.equals(calculationType)){
			//convert distance to nautical miles
			distance = distance * 0.5399568;
		}
		return distance;
	}
}
