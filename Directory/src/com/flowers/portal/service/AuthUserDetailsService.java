package com.flowers.portal.service;

import com.flowers.portal.exception.UserNotFoundException;
import com.flowers.portal.exception.UserWrongPasswordException;
import com.flowers.portal.exception.WorkingShopNotFoundException;
import com.flowers.server.entity.User;


/**
 * A custom service for authenticating users against database.
 * 
 * @author Mark Silver
 */
public interface AuthUserDetailsService {
    
	/**
	 * Retrieves a user record containing the user's credentials and access.
	 *
	 * @param user User entity
	 * @return User if found, null if not
	 * @throws Exception User exception
	 */
	 User authenticate(User formUser) throws 			 UserNotFoundException,
															 UserWrongPasswordException,
															 WorkingShopNotFoundException;

	String getShopName(String userName, String password, String shopCode);
	
	boolean validateBloomLinkCredentials(String userName, String password, String shopCode);
	
}
