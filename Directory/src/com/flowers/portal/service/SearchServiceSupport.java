package com.flowers.portal.service;

import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.SortField;

import com.flowers.server.search.SearchQuery;
import com.flowers.server.search.SearchResultSet;

public interface SearchServiceSupport extends ServiceSupport {

	public static final Double MAX_DISTANCE = 50000.00;

	public abstract Document document(Class<?> clazz, String id);

	@SuppressWarnings("rawtypes")
	public abstract List<Document> documents(Class clazz, Query query, SortField sort);
/*
	@SuppressWarnings("rawtypes")
	public abstract <T> SearchResultSet<T> objects(Class clazz, SearchQuery query);

	@SuppressWarnings("rawtypes")
	public abstract <T> SearchResultSet<T> maps(Class clazz, SearchQuery query);
*/
	public abstract Document document(Class<?> clazz, Query query);

	@SuppressWarnings("rawtypes")
	public abstract <T> SearchResultSet<T> documents(Class clazz, SearchQuery query);

	@SuppressWarnings("rawtypes")
	public abstract <T> SearchResultSet<T> listings(Class clazz, SearchQuery query);

	@SuppressWarnings("rawtypes")
	public abstract List<Document> documents(Class clazz, String[] ids);

	@SuppressWarnings("rawtypes" )
	public abstract List<Document> documentsByParent(Class clazz, String[] ids);
/*
	public abstract void index(Class<?> clazz, List<Long> ids);

	public abstract void index(Class<?> clazz, Long id);

	public abstract void index(Object obj);

	public abstract void index(Class<?> clazz, int page);

	public abstract void purge(Class<?> clazz);
*/
}