package com.flowers.portal.service;

import com.flowers.portal.vo.Order;
import com.flowers.portal.vo.RestResponse;
import com.flowers.server.entity.User;


/**
 * Bloomnet service for orders.
 * @author Fabio Cabral
 */
public interface OrderService {
	
	/**
	 * Sends (submits) an order. This services will confirm an order.
	 * @param order @see com.flowers.server.entity.Order
	 */
	public RestResponse sendOrder(User user, Order order);
	
	public void processRoute4MeIntake();
}
