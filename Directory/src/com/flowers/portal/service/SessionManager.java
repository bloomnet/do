package com.flowers.portal.service;

import javax.servlet.http.HttpServletRequest;


public interface SessionManager {

	Object getApplicationUser(HttpServletRequest request);
	void setApplicationUser(HttpServletRequest request, Object applicationUser);
	Object getOrder(HttpServletRequest request);
	void setOrder(HttpServletRequest request,Object masterOrder);
	void removeOrder(HttpServletRequest request);
	
	/**
	 * Removes all of the object wich the application might have had kept in
	 * session.  This method is usefull for the sign out user action. This method
	 * checks for all methods starting with "remove".
	 *
	 * @param request
	 */
	void deleteAllApplicationObjectsFromSession(HttpServletRequest request);
	
	
	void removeSelectedDate(HttpServletRequest request);
	void setSelectedDate(HttpServletRequest request, Object masterOrder);
	Object getSelectedDate(HttpServletRequest request);
	Object getSelectedEndDate(HttpServletRequest request);
	void setSelectedEndDate(HttpServletRequest request, Object masterOrder);
	void removeSelectedEndDate(HttpServletRequest request);
}