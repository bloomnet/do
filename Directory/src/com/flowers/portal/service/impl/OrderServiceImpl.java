package com.flowers.portal.service.impl;
import java.io.IOException;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.text.CharacterIterator;
import java.text.DecimalFormat;
import java.text.StringCharacterIterator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.flowers.portal.dao.OrderDAO;
import com.flowers.portal.service.OrderService;
import com.flowers.portal.util.DateUtil;
import com.flowers.portal.util.EmailClient;
import com.flowers.portal.util.MyHttpClient;
import com.flowers.portal.util.SendGridEmailer;
import com.flowers.portal.util.WebServiceUtil;
import com.flowers.portal.vo.DescribedProduct;
import com.flowers.portal.vo.Order;
import com.flowers.portal.vo.RestResponse;
import com.flowers.server.entity.OrderProduct;
import com.flowers.server.entity.ProcessedOrder;
import com.flowers.server.entity.Route4MeIntake;
import com.flowers.server.entity.User;


@Transactional(propagation = Propagation.SUPPORTS)
public class OrderServiceImpl extends LuceneSearchServiceSupport implements OrderService, ApplicationContextAware {
	protected final Log logger = LogFactory.getLog(getClass());
	@Autowired private Properties directoryProperties;
    @Autowired private OrderDAO orderDAO;


	
	private Map<String, String> errorMessages = new HashMap<String, String>();
	
	
	@Override
	public void setApplicationContext(ApplicationContext arg0) throws BeansException {	}
	
	@Override
	public RestResponse sendOrder(User user, Order order) {
		try {
			order.setCaptureDate(new Date());			
			String requestParameters = "func=postmessages&data=" + createXmlData(user, order);			
			String url = directoryProperties.getProperty("address");//PropertiesUtil.getInstance().getProperty("address");
			String result = WebServiceUtil.sendGetRequest(url, requestParameters);
			RestResponse response = new RestResponse(result);
			recordOrder(order, response);
			if(response.getErrors().size() > 0) {
				for(com.flowers.portal.vo.Error error : response.getErrors()) {
					String friendly = errorMessages.get(String.valueOf(error.getDetailedErrorCode()));
					if(friendly != null && friendly.length() > 0)
						error.setErrorMessage(friendly);
				}
			} else if(response.getGlobalErrors().size() > 0) {
				for(com.flowers.portal.vo.Error error : response.getGlobalErrors()) {
					String friendly = errorMessages.get(String.valueOf(error.getDetailedErrorCode()));
					if(friendly != null && friendly.length() > 0)
						error.setErrorMessage(friendly);
				}
			}
			return response;
		}
		catch (Exception ex) {
			logger.error(ex.getClass().getName() + ex.getMessage());
			RestResponse restResponse = new RestResponse(new com.flowers.portal.vo.Error("Exception during order submission. No order submitted."));
			recordOrder(order, restResponse);
			return restResponse;
		}
	}
	@Transactional
	protected void recordOrder(Order placedOrder, RestResponse response) {
		ProcessedOrder order = new com.flowers.server.entity.ProcessedOrder();
		order.setDeliveryDate(placedOrder.getDeliveryDate());
		order.setSendingShop(placedOrder.getSendingShop().getShopCode());
		order.setFulfillingShop(placedOrder.getFulfillingShop().getShopCode());
		order.setReceivingShop(placedOrder.getReceivingShop().getShopCode());
		order.setCreatedDate(new java.sql.Date(new Date().getTime()));
		order.setPrice(placedOrder.getGrandTotal().getAmount().doubleValue());
		if(placedOrder.getRecipient() != null) {
			order.setRecipientName(placedOrder.getRecipient().getFirstName()+" "+placedOrder.getRecipient().getLastName());
			String address = placedOrder.getRecipient().getAddress1()+" "+placedOrder.getRecipient().getCity()+", "+placedOrder.getRecipient().getPostalCode();
			if(address.length() > 100) 
				order.setRecipientAddress(address.substring(0, 100));
			else
				order.setRecipientAddress(address);
		}
		if(response.getErrors().size() > 0) {
			StringBuffer buff = new StringBuffer();
			for(com.flowers.portal.vo.Error error : response.getErrors()) {
				buff.append(error.getErrorMessage());
			}
			if(buff.length() > 1020) order.setMessage(buff.toString().substring(0, 1020));
			else order.setMessage(buff.toString());
		} else if(response.getGlobalErrors().size() > 0) {
			StringBuffer buff = new StringBuffer();
			for(com.flowers.portal.vo.Error error : response.getGlobalErrors()) {
				buff.append(error.getErrorMessage());
			}
			if(buff.length() > 1020) order.setMessage(buff.toString().substring(0, 1020));
			else order.setMessage(buff.toString());
		}
		order.setTrackingNumber(String.valueOf(response.getBmtOrderNumber()));
		order.setTimestamp(System.currentTimeMillis());
		orderDAO.saveOrder(order);
		List<DescribedProduct> products = placedOrder.getDescribedProducts();
		for(DescribedProduct product : products){
			OrderProduct orderProduct = new OrderProduct();
			orderProduct.setPrice(product.getPrice().getAmount().doubleValue());
			orderProduct.setProductCode(product.getCode());
			orderProduct.setQuantity(product.getQuantity());
			orderProduct.setDescription(product.getDescription());
			orderProduct.setTrackingNumber(String.valueOf(response.getBmtOrderNumber()));
			orderDAO.saveOrderProduct(orderProduct);
		}
	}
	
	public void processRoute4MeIntake() {
		
		List<Route4MeIntake> items = orderDAO.getRoute4MeByUnprocessed();
		
		for(Route4MeIntake item : items) {
			Integer attempts = item.getAttempts();
			
			Boolean failedBloomlink = false;
			
			if(item.getBloomlinkProcessed() == 0) {
				
				Boolean success = false;
				
				PostMethod myPost = new PostMethod(directoryProperties.getProperty("address"));
				
				String xmlString = "<memberDirectoryInterface><searchShopRequest><security><username>"+directoryProperties.getProperty("r4MeBlkUser")+"</username><password>"+directoryProperties.getProperty("r4MeBlkPass")+"</password><shopCode>"+directoryProperties.getProperty("r4MeBlkShopCode")+"</shopCode></security><memberDirectorySearchOptions><updateShopCredential><targetShopCode>"+item.getShopCode()+"</targetShopCode><shopPassword>"+item.getSecretPassword()+"</shopPassword></updateShopCredential></memberDirectorySearchOptions></searchShopRequest></memberDirectoryInterface>";
				
				myPost.addParameter("func", "getMemberDirectory");
				myPost.addParameter("data", xmlString);
				
				HttpClient client = new HttpClient();
			
				try {
					client.executeMethod(myPost);
					String response = myPost.getResponseBodyAsString();
					if(response != null && response.contains("Credentials updated successfully")) {
						success = true;
					}
				}catch(Exception ee){
					ee.printStackTrace();
				}
				
				if(success) {
					item.setBloomlinkProcessed(1);
				}else {
					
					failedBloomlink = true;
					attempts = attempts + 1;
					item.setAttempts(attempts);
					
					if(attempts == 5) {
						try {
							new SendGridEmailer("There was an error processing an account for shop code: " + item.getShopCode(), 1);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
				
				orderDAO.saveRoute4Me(item);
				
			}
			if(item.getRoute4MeProcessed() == 0 && !failedBloomlink) {
				
				String contact = item.getContact();
				String firstName = contact;
				String lastName = contact;
				
				if(contact.split(" ").length > 1) {
					firstName = contact.split(" ")[0];
					lastName = contact.split(" ")[1];			
				}
					
				
				Boolean success = false;
				
				MyHttpClient client = new MyHttpClient();
				CloseableHttpClient httpclient = null;
				try {
					httpclient = client.getNewHttpClient(true);
				} catch (KeyManagementException e2) {
					e2.printStackTrace();
				} catch (NoSuchAlgorithmException e2) {
					e2.printStackTrace();
				} catch (KeyStoreException e2) {
					e2.printStackTrace();
				}
				
				HttpPost httppost = null;
				HttpPut httpput = null;
				HttpEntity httpEnt = null;
				HttpResponse response = null;
				HttpEntity resEntity = null;
				String responseJson = "";
				
				if(item.getMemberId() == null || item.getMemberId().equals("")) {
					
					httppost = new HttpPost("https://api.route4me.com/actions/register_action.php?api_key="+directoryProperties.getProperty("r4MeKey")+"&plan=enterprise_plan&organization=bloomnet");
					httpEnt = MultipartEntityBuilder.create()
					    		.addPart("strIndustry", new StringBody("Gifting", ContentType.TEXT_PLAIN))
					    		.addPart("strFirstName", new StringBody(firstName, ContentType.TEXT_PLAIN))
					    		.addPart("strLastName", new StringBody(lastName, ContentType.TEXT_PLAIN))
					    		.addPart("strEmail", new StringBody(item.getEmail(), ContentType.TEXT_PLAIN))
					    		.addPart("format", new StringBody("json", ContentType.TEXT_PLAIN))
					    		.addPart("chkTerms", new StringBody("1", ContentType.TEXT_PLAIN))
					    		.addPart("device_type", new StringBody("web", ContentType.TEXT_PLAIN))
					    		.addPart("strPassword_1", new StringBody(item.getPassword(), ContentType.TEXT_PLAIN))
					    		.addPart("strPassword_2", new StringBody(item.getPassword(), ContentType.TEXT_PLAIN))
					    		.addPart("activation_key", new StringBody(directoryProperties.getProperty("r4MeKey"), ContentType.TEXT_PLAIN))
					    		.build();
					httppost.setEntity(httpEnt);
					try {
						response = httpclient.execute(httppost);
						resEntity = response.getEntity();
						responseJson = EntityUtils.toString(resEntity);
						EntityUtils.consume(resEntity);
						
						if(responseJson != null) {
							
							String status = "";
							
							try{status = responseJson.split("\"status\":")[1].split(",")[0];}catch(Exception ee){}
							
							if(status.equals("true")) {
								
								status = "";
									
								String memberId = responseJson.split("\"member_id\":")[1].split(",")[0];
								String apiKey = responseJson.split("\"api_key\":\"")[1].split("\",")[0];
								item.setMemberId(memberId);
								item.setApiKey(apiKey);
								String json = "{\"member_id\":\""+memberId+"\",\"timezone\":\""+item.getTimezone()+"\"}";
								httpput = new HttpPut("https://api.route4me.com/api.v4/user.php?api_key="+apiKey);
								httpEnt = new StringEntity(json, ContentType.APPLICATION_JSON);
								httpput.setEntity(httpEnt);
							    response = httpclient.execute(httpput);
							    resEntity = response.getEntity();
								responseJson = EntityUtils.toString(resEntity);
								EntityUtils.consume(resEntity);
								
								try {status = responseJson.split("\"member_id\":\"")[1].split("\",")[0];}catch(Exception ee){}
								
								if(status.equals(memberId)) {
									Boolean result = submitCredentialsToR4Me(item, httppost, httpEnt, response, resEntity, httpclient);
									if(result == true) success = true;
								}
							}								
						}
					} catch (ClientProtocolException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}finally {
						try {
							httpclient.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}else {
					try {
						String json = "{\"member_id\":\""+item.getMemberId()+"\",\"timezone\":\""+item.getTimezone()+"\"}";
						httpput = new HttpPut("https://api.route4me.com/api.v4/user.php?api_key="+item.getApiKey());
						httpEnt = new StringEntity(json, ContentType.APPLICATION_JSON);
						httpput.setEntity(httpEnt);
					    response = httpclient.execute(httpput);
					    resEntity = response.getEntity();
						responseJson = EntityUtils.toString(resEntity);
						EntityUtils.consume(resEntity);
						
						String status = "";
						
						try {status = responseJson.split("\"member_id\":\"")[1].split("\",")[0];}catch(Exception ee){}
						
						if(status.equals(item.getMemberId())){	
							Boolean result = submitCredentialsToR4Me(item, httppost, httpEnt, response, resEntity, httpclient);
							if(result == true) success = true;
						}
						
					} catch (ClientProtocolException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}finally {
						try {
							httpclient.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
				
				if(success) {
					item.setRoute4MeProcessed(1);
					try {
						new EmailClient("<!DOCTYPE html><html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\"><head><!--[if gte mso 9]><xml>            <o:OfficeDocumentSettings>                <o:AllowPNG />                <o:PixelsPerInch>96</o:PixelsPerInch>            </o:OfficeDocumentSettings>        </xml><![endif]--><title>Welcome to BloomNet Driver!</title><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" /><meta name=\"format-detection\" content=\"telephone=no, date=no, address=no, email=no, url=no\"><meta name=\"x-apple-disable-message-reformatting\"><meta name=\"color-scheme\" content=\"light dark\"><meta name=\"supported-color-schemes\" content=\"light dark\"><style type=\"text/css\">:root {  color-scheme: light dark;  supported-color-schemes: light dark;}a:hover {  text-decoration: none !important;}a[x-apple-data-detectors] {  color: inherit !important;  text-decoration: none !important;}a[href^=\"tel\"]:hover {  text-decoration: none !important;}a img {  border: none;}p {  margin: 0;  padding: 0;  margin-bottom: 0;}h1, h2, h3, h4, h5, h6 {  margin: 0;  padding: 0;  margin-bottom: 0;}table td {  mso-line-height-rule: exactly;}table {  border-collapse: collapse;}#emailBody #emailWrapper [x-apple-data-detectors] {  color: inherit !important;  text-decoration: inherit !important;}</style></head><body id=\"emailBody\" class=\"body\" xml:lang=\"en\" style=\"background:#EFEFEF; margin:0; padding:0; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;\"><div role=\"article\" aria-roledescription=\"email\" aria-label=\"email name\" lang=\"en\" dir=\"ltr\" style=\"font-size:medium; font-size:max(16px, 1rem)\">     <div style=\"display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family:sans-serif;\">Your shop account has been successfully created!</div>    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" bgcolor=\"#EFEFEF\" class=\"wrapper\" role=\"presentation\">    <tr>      <td align=\"center\" valign=\"top\" width=\"100%\"><table width=\"600\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"wrapper\" role=\"presentation\" bgcolor=\"#ffffff\" style=\"margin-top: 30px;\">          <tr>            <td width=\"600\" align=\"center\" valign=\"top\" style=\"color:#333333; font-family: Arial, sans-serif; font-size: 16px; padding-top: 45px; padding-bottom: 15px; border-top: 5px solid #000000;\"><img src=\"https://www.bloomnet.net/wp-content/uploads/2023/01/logo_bloomnet_no_tag.png\" border=\"0\" style=\"border:0; display:block; width: 175px\" alt=\"BloomNet Driver Logo\"></td>          </tr>          <tr>            <td width=\"600\" align=\"left\" valign=\"top\" style=\"color:#333333; font-family: Arial, sans-serif; font-size: 16px; line-height: 22px; padding-top: 30px; padding-right: 40px; padding-bottom: 9px; padding-left: 40px;\"><p style=\"padding-bottom: 24px;\">Hello "+item.getContact()+", </p>              <p style=\"padding-bottom: 18px;\">Thank you for signing up for BloomNet Driver!  Your shop account has been successfully created and is ready to use.</p>            </td>          </tr>          <tr>            <td width=\"600\" align=\"left\" valign=\"top\" style=\"color:#333333; font-family: Arial, sans-serif; font-size: 16px; line-height: 22px;  padding-right: 40px; padding-bottom: 30px; padding-left: 40px;\">               <p style=\"padding-bottom: 18px;\"><strong>To get started:</strong></p>              <ol style=\"margin:0; margin-left: 25px; padding:0; font-family: Arial, sans-serif; color:#495055; font-size:16px; line-height:22px;\" align=\"left\" type=\"1\">                <li style=\"padding-bottom: 20px;\"> Login to your <a href=\"https://route4me.com/login\">management dashboard.</a> </li>                <li style=\"padding-bottom: 20px;\"> <a href=\"https://support.route4me.com/add-and-manage-users/#create-users\" target=\"_blank\">Follow these instructions</a> to add drivers to your account. </li>                <li style=\"padding-bottom: 12px;\"> Download the BloomNet Driver App:                  <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"margin-top: 12px;\">                    <tr>                      <td align=\"left\" valign=\"top\" style=\"color:#333333; font-family: Arial, sans-serif; font-size: 16px; line-height: 22px;  padding-right: 20px;\"><a href=\"https://apps.apple.com/us/app/bloomnet-driver/id6738212747\" target=\"_blank\"> <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/Download_on_the_App_Store_Badge.svg/2560px-Download_on_the_App_Store_Badge.svg.png\" border=\"0\" style=\"border:0; display:block; height: 40px;\" alt=\"Download on the App Store\"> </a></td>                      <td align=\"left\" valign=\"top\" style=\"color:#333333; font-family: Arial, sans-serif; font-size: 16px; line-height: 22px\"> <a href=\"https://play.google.com/store/apps/details?id=com.mybloomnet.routeoptimizer&pli=1\" target=\"_blank\"> <img src=\"https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Google_Play_Store_badge_EN.svg/2560px-Google_Play_Store_badge_EN.svg.png\" border=\"0\" style=\"border:0; display:block; height:40px;\" alt=\"Get it on the Google Play Store\"> </a></td>                    </tr>                  </table>                </li>              </ol></td>          </tr>          <tr>        <td width=\"600\" align=\"left\" valign=\"top\" style=\"color:#333333; font-family: Arial, sans-serif; font-size: 16px; line-height: 22px;  padding-right: 40px; padding-bottom: 30px; padding-left: 40px;\"><p style=\"padding-bottom: 18px;\">Your drivers will receive separate login credentials once you add them to your account.  If you need any assistance setting up your account or adding drivers, please contact our support team at <a href=\"mailto:customerservice@bloomnet.net?subject=BloomNet%20Driver:%20Help%20Onboarding%20New%20Drivers\">customerservice@bloomnet.net.</a></p>              <p style=\"padding-bottom: 36px;\">Thank you for using this App! Drivers capturing proof-of-delivery (POD) photos may help protect you when orders are cancelled. Be sure drivers know all the best practices – log into BloomLink, and go to Utilities &raquo Standard &amp; Guidelines &raquo BloomNet – Photo Delivery Best Practices. </p>              <p style=\"padding-bottom: 18px;\">Sincerely, <br />                The BloomNet Driver App Team </p></td>     </tr>   </table></td>    </tr>  </table></div></body></html>", item.getEmail());
					} catch (IOException e) {
						e.printStackTrace();
					}
				}else {
					
					attempts = attempts + 1;
					item.setAttempts(attempts);
					
					if(attempts == 5) {
						try {
							new SendGridEmailer("There was an error processing an account for shop code: " + item.getShopCode(), 1);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
				
				orderDAO.saveRoute4Me(item);
				
			}
		}
	}
	
	private Boolean submitCredentialsToR4Me(Route4MeIntake item, HttpPost httppost, HttpEntity httpEnt, HttpResponse response, HttpEntity resEntity, CloseableHttpClient httpclient){
		
		String userName = item.getShopCode().substring(0,4);
		
		String json = "{\"vendor_id\":\""+directoryProperties.getProperty("r4MeVendorID")+"\",\"name\":\"Bloomlink "+userName+"\",\"username\":\""+userName+"R4Me\",\"password\":\""+item.getSecretPassword()+"\",\"shop_code\":\""+item.getShopCode()+"\",\"base_url\":\""+directoryProperties.getProperty("r4MeAddress")+"\"}";
		
		httppost = new HttpPost("https://wh.route4me.com/modules/api/v5.0/orders-import/connections");
		httpEnt = new StringEntity(json, ContentType.APPLICATION_JSON);
		
		httppost.addHeader("Content-Type","application/json");
		httppost.addHeader("Accept","application/json");
		httppost.addHeader("Authorization","Bearer "+item.getApiKey());
		
		httppost.setEntity(httpEnt);
		
		Boolean status = false;
		
		try {
			response = httpclient.execute(httppost);
			resEntity = response.getEntity();
			String responseJson = EntityUtils.toString(resEntity);
			EntityUtils.consume(resEntity);
			
			try{
				if(responseJson.contains("connection_id")) status = true;
				else System.out.println("Error submitting credentials to Route4Me: " + responseJson);
				}catch(Exception ee){
					ee.printStackTrace();
				}
			
		}catch(Exception ee) {
			ee.printStackTrace();
		}
		
		return status;
	}
	
	private String createXmlData(User user, Order order) {
		try {
			Element rootElement = new Element("foreignSystemInterface");
			rootElement.addNamespaceDeclaration(Namespace.getNamespace("xsi","http://www.w3.org/2001/XMLSchema-instance"));
			
			rootElement.setAttribute("noNamespaceSchemaLocation", 
					"C:\\logistics_services\\components\\fsicore\\xsd\\client\\ForeignSystemInterface.xsd", 
					Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance"));
			
			//security
			Element securityElement = new Element("security");
			securityElement.addContent(new Element("username").setText(user.getUserName()));
			securityElement.addContent(new Element("password").setText(user.getPassword()));
			securityElement.addContent(new Element("shopCode").setText(user.getShopCode()));
			rootElement.addContent(securityElement);
			
			//errors
			rootElement.addContent(new Element("errors"));
			
			//messages
			Element messagesElement = new Element("messagesOnOrder");
			messagesElement.addContent(new Element("messageCount").setText("1"));
			
			//message
			Element messageElement = new Element("messageOrder");
			messageElement.addContent(new Element("messageType").setText(Order.MESSAGE_TYPE));
			messageElement.addContent(new Element("sendingShopCode").setText(order.getSendingShop().getShopCode()));
			messageElement.addContent(new Element("receivingShopCode").setText(order.getReceivingShop().getShopCode()));
			messageElement.addContent(new Element("fulfillingShopCode").setText(order.getFulfillingShop().getShopCode()));
			messageElement.addContent(new Element("systemType").setText(Order.SYSTEM_TYPE));
			messageElement.addContent(new Element("containsPerishables").setText(order.getContainsPerishables()));
			
			//identifiers
			Element identifiersElement = new Element("identifiers");
			Element generalIdentifiersElement = new Element("generalIdentifiers");
			generalIdentifiersElement.addContent(new Element("bmtOrderNumber"));
			generalIdentifiersElement.addContent(new Element("bmtSeqNumberOfOrder"));
			generalIdentifiersElement.addContent(new Element("bmtSeqNumberOfMessage"));
			generalIdentifiersElement.addContent(new Element("externalShopOrderNumber").setText("10004"));
			generalIdentifiersElement.addContent(new Element("externalShopMessageNumber"));
			identifiersElement.addContent(generalIdentifiersElement);
			messageElement.addContent(identifiersElement);
			
			messageElement.addContent(new Element("messageCreateTimestamp").setText(DateUtil.toXmlFormatString(new Date())));
			
			//orderDetails
			Element orderDetailsElement = new Element("orderDetails");
			orderDetailsElement.addContent(new Element("orderNumber"));
			orderDetailsElement.addContent(new Element("occasionCode").setText(Long.toString(order.getOcasionId())));
			orderDetailsElement.addContent(new Element("totalCostOfMerchandise").setText(order.getGrandTotal().getAmount().toPlainString()));
			
			DecimalFormat decimalFormat = new DecimalFormat();
			decimalFormat.setDecimalSeparatorAlwaysShown(false);
			
			//orderProductsInfo
			Element orderProductsInfoElement = new Element("orderProductsInfo");
			for (DescribedProduct describedProduct: order.getDescribedProducts()) {
				Element orderProductsInfoDetailsElement = new Element("orderProductInfoDetails");
				orderProductsInfoDetailsElement.addContent(new Element("units").setText(decimalFormat.format(describedProduct.getQuantity()))); //Double.toString(describedProduct.getQuantity())));
				orderProductsInfoDetailsElement.addContent(new Element("costOfSingleProduct").setText(describedProduct.getPrice().getAmount().toPlainString()));
				String productDescription = describedProduct.getDescription();
				String dropshipShopCode = directoryProperties.getProperty("dropshipShopCode");
				if(order != null && order.getReceivingShop().getShopCode().equals(dropshipShopCode))
					productDescription = "Dropship Product Code: " + describedProduct.getCode();
				orderProductsInfoDetailsElement.addContent(new Element("productDescription").setText(productDescription));
				orderProductsInfoDetailsElement.addContent(new Element("productCode").setText(describedProduct.getCode()));
				orderProductsInfoElement.addContent(orderProductsInfoDetailsElement);
			}	
			orderDetailsElement.addContent(orderProductsInfoElement);
			
			orderDetailsElement.addContent(new Element("orderCardMessage").setText(order.getCardMessage()));
			orderDetailsElement.addContent(new Element("orderType").setText("1"));
			orderDetailsElement.addContent(new Element("orderTypeMessage").setText("Directory Online Order"));
			
			messageElement.addContent(orderDetailsElement);

			messageElement.addContent(new Element("orderCaptureDate").setText(DateUtil.toXmlFormatString(order.getCaptureDate())));
			
			//deliveryDetails
			Element deliveryDetailsElement = new Element("deliveryDetails");
			deliveryDetailsElement.addContent(new Element("deliveryDate").setText(order.getDeliveryDate()));
			deliveryDetailsElement.addContent(new Element("specialInstruction").setText(order.getSpecialInstruction()));
			messageElement.addContent(deliveryDetailsElement);
			
			//recipient
			Element recipientElement = new Element("recipient");
			recipientElement.addContent(new Element("recipientFirstName").setText(order.getRecipient().getFirstName()));
			recipientElement.addContent(new Element("recipientLastName").setText(order.getRecipient().getLastName()));
			
			//facility
			if(order.getFacilityName() != null && !order.getFacilityName().equals("") && order.getFacilityName().length() > 60)
				recipientElement.addContent(new Element("recipientAttention").setText(order.getFacilityName().substring(0, 60)));
			else
				recipientElement.addContent(new Element("recipientAttention").setText(order.getFacilityName()));
			recipientElement.addContent(new Element("recipientAddress1").setText(order.getRecipient().getAddress1()));
			recipientElement.addContent(new Element("recipientAddress2").setText(order.getRecipient().getAddress2()));
			if(order.getRecipient().getCity() != null && !order.getRecipient().getCity().equals("")){
				recipientElement.addContent(new Element("recipientCity").setText(order.getRecipient().getCity()));
			}else{
				recipientElement.addContent(new Element("recipientCity").setText(""));
			}
			if(order.getRecipient().getState() != null && !order.getRecipient().getState().equals("")){
				String country = order.getRecipient().getCountryCode();
				if(country.equals("US") || country.equals("USA") || country.equals("CAN") || country.equals("PUR")){
					recipientElement.addContent(new Element("recipientState").setText(order.getRecipient().getState()));
				}else{
					recipientElement.addContent(new Element("recipientState").setText("OS"));
				}
			}else{
				recipientElement.addContent(new Element("recipientState").setText(""));
			}
			if(order.getRecipient().getPostalCode() != null && !order.getRecipient().getPostalCode().equals("")){
				if(order.getRecipient().getPostalCode().length() > 10)
					recipientElement.addContent(new Element("recipientZipCode").setText(order.getRecipient().getPostalCode().substring(0,10)));
				else
					recipientElement.addContent(new Element("recipientZipCode").setText(order.getRecipient().getPostalCode()));
			}
			recipientElement.addContent(new Element("recipientCountryCode").setText(order.getRecipient().getCountryCode()));
			recipientElement.addContent(new Element("recipientPhoneNumber").setText(formatPhone(order.getRecipient().getPhoneNumber().replaceAll(" ", ""))));
			
			messageElement.addContent(recipientElement);
			
			messageElement.addContent(new Element("wireServiceCode").setText(Order.WIRE_SERVICE_CODE));
			
			//shippingDetails
			Element shippingDetailsElement = new Element("shippingDetails");
			shippingDetailsElement.addContent(new Element("trackingNumber"));
			shippingDetailsElement.addContent(new Element("shipperCode"));
			shippingDetailsElement.addContent(new Element("shippingMethod"));
			shippingDetailsElement.addContent(new Element("shippingDate"));
			messageElement.addContent(shippingDetailsElement);
			
			messagesElement.addContent(messageElement);
			rootElement.addContent(messagesElement);			
			
			Document doc = new Document(rootElement);
			
			XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
			
			logger.info("XML: " + outputter.outputString(doc));

			//logger.info("XML encoded: " + URLEncoder.encode(outputter.outputString(doc), "UTF-8"));
			
			String encoded = URLEncoder.encode(outputter.outputString(doc), "UTF-8"); 
			
			return encoded;
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	
	private String formatPhone(String phone){
		
		final StringBuilder result = new StringBuilder();
	    final StringCharacterIterator iterator = new StringCharacterIterator(phone);
	    
	    char character =  iterator.current();
	    
	    while (character != CharacterIterator.DONE ){
	      if (character == '(' || character == ')' || character == '-') {
	      }else {
	        result.append(character);
	      }
	      character = iterator.next();
	    }

	    return result.toString();
	}
		
}
