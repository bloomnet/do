package com.flowers.portal.service.impl;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import com.flowers.portal.service.SearchServiceSupport;
import com.flowers.portal.util.DistanceCalculationUtil;
import com.flowers.portal.util.LuceneIndexSearcher;
import com.flowers.server.entity.MinimumPrice;
import com.flowers.server.search.BidComparator;
import com.flowers.server.search.SearchQuery;
import com.flowers.server.search.SearchResultSet;


public class LuceneSearchServiceSupport implements SearchServiceSupport {
	protected final Log log = LogFactory.getLog(getClass());
	private DistanceCalculationUtil calculator = new DistanceCalculationUtil();
	private Map<String, LuceneIndexSearcher> searchers = new HashMap<String, LuceneIndexSearcher>();
	private String indexPath;
	
	public static final Double MAX_DISTANCE = 5000.00;
	
	private static List<String> fieldExclusions = new ArrayList<String>();
	static {
		fieldExclusions.add("_hibernate_class");
	}
	
	@SuppressWarnings({ "rawtypes" })
	public Document document(Class clazz, String id) {
		try {
			MultiFieldQueryParser parser = new MultiFieldQueryParser(new String[]{"id"}, new StandardAnalyzer());
			String queryStr = "id:"+id;
			Query query = parser.parse(queryStr);
			LuceneIndexSearcher searcher =  getSearcher(clazz.getName());
			return searcher.document(query);
		} catch(Exception e) {
			log.debug("no entity found id:"+id);
		}
		return null;
	}
	@SuppressWarnings({ "rawtypes" })
	public List<Document> documents(Class clazz, Query query, SortField sort) {
		try {
			LuceneIndexSearcher searcher =  getSearcher(clazz.getName());
			if(sort != null) searcher.setSort(new Sort(sort));
			return searcher.documents(query);
		} catch(Exception e) {
			log.debug("no entity found for query");
		}
		return null;
	}
	@SuppressWarnings("rawtypes")
	public Document document(Class clazz, Query query) {
		try {
			LuceneIndexSearcher searcher =  getSearcher(clazz.getName());
			return searcher.document(query);
		} catch(Exception e) {
			log.debug("no entity found for query");
		}
		return null;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T> SearchResultSet<T> documents(Class clazz, SearchQuery query) {
		try {
			long start = System.currentTimeMillis();
			LuceneIndexSearcher searcher = getSearcher(clazz.getName());
			searcher.setSort(query.getLuceneSort());
			searcher.setPage(query.getPage());
			searcher.setPageSize(query.getPageSize());
			List<Document> results1 = searcher.documents(query.getLuceneQuery());
			SearchResultSet results = new SearchResultSet();
			for(Object o : results1) {
				results.getResults().add((T)o);
			}
			results.setResultSize(searcher.getTotal());
			long end = System.currentTimeMillis();
			long time = (end-start);
			results.setTime(time);
			return results;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T> SearchResultSet<T> listings(Class clazz, SearchQuery query) {
		try {
			long start = System.currentTimeMillis();
			LuceneIndexSearcher searcher = getSearcher(clazz.getName());
			searcher.setSort(query.getLuceneSort());
			searcher.setPageSize(query.getPageSize());
			List<Document> results1 = searcher.documents(query.getLuceneQuery());
			SearchResultSet results = new SearchResultSet(query);
			List<String> shopCodes = new ArrayList<String>();
			for(int i=0; i < results1.size(); i++) {
				Document doc = results1.get(i);					
				if(query.getDestinationLatitude() != null && query.getDestinationLongitude() != null && doc.get("latitude") != null && doc.get("longitude") != null) {
					Double latititude = Double.valueOf(doc.get("latitude"));
					Double longitude = Double.valueOf(doc.get("longitude"));
					double distance = calculator.calculate(query.getDestinationLatitude(), query.getDestinationLongitude(), latititude, longitude, DistanceCalculationUtil.CalculationType.MILES);
					if(distance <= MAX_DISTANCE) {
						doc.add(new TextField("distance", String.valueOf((int)distance), Field.Store.YES));
					} else continue;
				}
				
				Date today = new Date();

				String listingEndDate = doc.get("listing_end_date");
				
				/** 
				 * We check if the shopCode is already included, if it's a duplicate then remove it.
				 */
				if(!shopCodes.contains(doc.get("shopcode"))) {						
					if(query.getMinimums().size() > 0) {
						boolean holiday = inHolidayPeriod();					
						String minimums = doc.get("minimums");
						if(minimums != null) {
							String[] mins = minimums.split(" ");
							for(MinimumPrice price : query.getMinimums()) {
								String min = mins[price.getId().intValue()];
								Double minDouble = null;
								if(min != null && !min.equals("")) {
									if(min.endsWith("H")) {
										if(holiday)	
											minDouble = Double.valueOf(min.substring(0, min.length()-1));
									} else
										minDouble = Double.valueOf(min);
									if(minDouble == null || minDouble <= price.getMinimumPrice()) {
										shopCodes.add(doc.get("shopcode"));
										results.getResults().add((T)doc);
									}
								}	
							}
						}
					} else if(listingEndDate == null || listingEndDate.equals("") || Long.valueOf(listingEndDate) > today.getTime()){
						shopCodes.add(doc.get("shopcode"));
						results.getResults().add((T)doc);
					}
				}
			}
			//Collections.sort(results.getResults(), new DistanceSortComparator());
			List<Document> list = results.getResults();
			List<Document> finalResults;
			if(query.getIsConsumerDirectory() != null && query.getIsConsumerDirectory() == true)
				finalResults = rotation(list, 55);
			else
				finalResults = rotation(list, 0);
			results.setResults(finalResults);
			results.setResultSize(searcher.getTotal());
			long end = System.currentTimeMillis();
			long time = (end-start);
			results.setTime(time);
			return results;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private List<Document> rotation(List<Document> list, int alternateRotation) throws ParseException{
		
		//New Rotation Logic for Consumer Directory
		if(alternateRotation == 55){
			
			List<Document> cityFeaturedPreferredListings = new ArrayList<Document>();
			List<Document> cityFeaturedListings = new ArrayList<Document>();
			List<Document> cityPreferredListings = new ArrayList<Document>();
			List<Document> residentialListings = new ArrayList<Document>();
			List<Document> extraListings = new ArrayList<Document>();
			List<Document> extraListingsFinal = new ArrayList<Document>();
			List<Document> priorityListingsFinal = new ArrayList<Document>();
			List<Document> finalResults = new ArrayList<Document>();
			
			Date today = new Date();

			for(Document listing : list){
				
				String featuredListing = listing.get("featured_listing");
				String preferredListing = listing.get("preferred_listing");
				String listingEndDate = listing.get("listing_end_date");
				if(listingEndDate == null || listingEndDate.equals("") || Long.valueOf(listingEndDate) > today.getTime()){
					if(featuredListing != null &&  featuredListing.equalsIgnoreCase("true") && preferredListing != null &&  preferredListing.equalsIgnoreCase("true")) {
						cityFeaturedPreferredListings.add(listing);
					}else if(featuredListing != null &&  featuredListing.equalsIgnoreCase("true")){
						cityFeaturedListings.add(listing);
					}else if(preferredListing != null &&  preferredListing.equalsIgnoreCase("true")){
						cityPreferredListings.add(listing);
					}else if(listing.get("city").equals(listing.get("shopcity")) && listing.get("state_code").equals(listing.get("shopstate")))
						residentialListings.add(listing);
					else
						extraListings.add(listing);
				}
			}
			
			if(cityFeaturedPreferredListings.size() > 0){
				Collections.shuffle(cityFeaturedPreferredListings);
				finalResults.addAll(cityFeaturedPreferredListings);
			}
			if(cityFeaturedListings.size() > 0){
				Collections.shuffle(cityFeaturedListings);
				finalResults.addAll(cityFeaturedListings);
			}
			
			if(cityPreferredListings.size() > 0){
				Collections.shuffle(cityPreferredListings);
				finalResults.addAll(cityPreferredListings);
			}
			
			if(residentialListings.size() > 0){
				Collections.shuffle(residentialListings);
				priorityListingsFinal.addAll(residentialListings);
				finalResults.addAll(priorityListingsFinal);
			}
			if(extraListings.size() > 0){
				Collections.shuffle(extraListings);
				extraListingsFinal.addAll(extraListings);
				finalResults.addAll(extraListingsFinal);
			}
			
			return finalResults;
			
		}else{
			
			List<Document> gAds = new ArrayList<Document>();
			List<Document> fAds = new ArrayList<Document>();
			List<Document> eAds = new ArrayList<Document>();
			List<Document> dAds = new ArrayList<Document>();
			List<Document> cAds = new ArrayList<Document>();
			List<Document> bAds = new ArrayList<Document>();
			List<Document> residentialListings = new ArrayList<Document>();
			List<Document> extraListings = new ArrayList<Document>();
			List<Document> extraListingsFinal = new ArrayList<Document>();
			List<Document> priorityListingsFinal = new ArrayList<Document>();
			List<Document> finalResults = new ArrayList<Document>();
			Date today = new Date();
			for(Document listing : list){
				String listingEndDate = listing.get("listing_end_date");
				if(listingEndDate == null || listingEndDate.equals("") || Long.valueOf(listingEndDate) > today.getTime()){
					if(listing.get("ad_size") != null && !listing.get("ad_size").equals("") &&
						(listing.get("ad_start_date") == null || listing.get("ad_start_date").equals("") || Long.valueOf(listing.get("ad_start_date")) <= today.getTime()) && 
						(listing.get("ad_end_date") == null || listing.get("ad_end_date").equals("") || Long.valueOf(listing.get("ad_end_date")) >= today.getTime())){
							
							if(listing.get("ad_size").equals("G"))
								gAds.add(listing);
							else if(listing.get("ad_size").equals("F"))
								fAds.add(listing);
							else if(listing.get("ad_size").equals("E"))
								eAds.add(listing);
							else if(listing.get("ad_size").equals("D"))
								dAds.add(listing);
							else if(listing.get("ad_size").equals("C"))
								cAds.add(listing);
							else if(listing.get("ad_size").equals("B"))
								bAds.add(listing);
							
					}else if(listing.get("city").equals(listing.get("shopcity")) && listing.get("state_code").equals(listing.get("shopstate")))
						residentialListings.add(listing);
					else
						extraListings.add(listing);
				}
			}
			
			if(gAds.size() > 0){
				Collections.shuffle(gAds);
				finalResults.addAll(gAds);
			}
			if(fAds.size() > 0){
				Collections.shuffle(fAds);
				finalResults.addAll(fAds);
	
			}
			if(eAds.size() > 0){
				Collections.shuffle(eAds);
				finalResults.addAll(eAds);
			}
			if(dAds.size() > 0){
				Collections.shuffle(dAds);
				finalResults.addAll(dAds);
			}
			if(cAds.size() > 0){
				Collections.shuffle(cAds);
				finalResults.addAll(cAds);
			}
			if(bAds.size() > 0){
				Collections.shuffle(bAds);
				finalResults.addAll(bAds);
			}
			if(residentialListings.size() > 0){
				Collections.shuffle(residentialListings);
				priorityListingsFinal.addAll(residentialListings);
				finalResults.addAll(priorityListingsFinal);
			}
			if(extraListings.size() > 0){
				Collections.shuffle(extraListings);
				extraListingsFinal.addAll(extraListings);
				finalResults.addAll(extraListingsFinal);
			}
			
			Collections.sort(finalResults, new BidComparator());
			return finalResults;
		}
	}
	
	@SuppressWarnings("rawtypes")
	public <T> SearchResultSet<T> facilities(Class clazz, SearchQuery query) {
		
		return new SearchResultSet<T>();
	}
	@SuppressWarnings({ "rawtypes"})
	public List<Document> documents(Class clazz, String[] ids) {
		try {
			MultiFieldQueryParser parser = new MultiFieldQueryParser(new String[]{"id"}, new StandardAnalyzer());
			StringBuffer query = new StringBuffer();
			for(int i=0; i < ids.length; i++) {
				if(!ids[i].equals("")) query.append(" id:"+ids[i]);
			}
			Query q = parser.parse(query.toString().trim());
			LuceneIndexSearcher searcher = getSearcher(clazz.getName());
			List<Document> results = searcher.documents(q);
			return results;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings({ "rawtypes"})
	public List<Document> documentsByParent(Class clazz, String[] ids) {
		try {
			MultiFieldQueryParser parser = new MultiFieldQueryParser(new String[]{"parent_id"}, new StandardAnalyzer());
			StringBuffer query = new StringBuffer();
			for(int i=0; i < ids.length; i++) {
				if(!ids[i].equals("")) query.append(" parent_id:"+ids[i]);
			}
			Query q = parser.parse(query.toString().trim());
			LuceneIndexSearcher searcher = getSearcher(clazz.getName());
			List<Document> results = searcher.documents(q);
			return results;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T> SearchResultSet<T> objects(Class clazz, SearchQuery query) {
		try {
			long start = System.currentTimeMillis();
			LuceneIndexSearcher searcher = getSearcher(clazz.getName());
			searcher.setSort(query.getLuceneSort());
			searcher.setPage(query.getPage());
			searcher.setPageSize(query.getPageSize());
			List<Document> results1 = searcher.documents(query.getLuceneQuery());
			SearchResultSet results = new SearchResultSet();
			for(Object o : results1) {
				results.getResults().add((T)o);
			}
			results.setResultSize(searcher.getTotal());
			long end = System.currentTimeMillis();
			long time = (end-start);
			results.setTime(time);
			return results;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T> SearchResultSet<T> maps(Class clazz, SearchQuery query) {
		try {
			long start = System.currentTimeMillis();
			LuceneIndexSearcher searcher = getSearcher(clazz.getName());
			searcher.setSort(query.getLuceneSort());
			searcher.setPage(query.getPage());
			searcher.setPageSize(query.getPageSize());
			List<Document> results1 = searcher.documents(query.getLuceneQuery());
			SearchResultSet results = new SearchResultSet();
			for(Document doc : results1) {
				Map<String, Object> map = new HashMap<String, Object>();
				for(Object obj : doc.getFields()) {
					if(obj instanceof Field) {
						Field field = (Field)obj;
						if(!fieldExclusions.contains(field.name())) map.put(field.name(), field.stringValue());
					}
				}
				results.getResults().add(map);
			}
			results.setResultSize(searcher.getTotal());
			results.setPageNumber(query.getPage());
			if(query.getPageSize() == 0 || results.getResultSize() < query.getPageSize()) results.setPageCount(1);
			else {
				double ratio = (double)results.getResultSize() / query.getPageSize();
				results.setPageCount((int)(Math.ceil(ratio)));
			}
			results.setQueryExplanation(query.getLuceneQuery().toString());
			long end = System.currentTimeMillis();
			long time = (end-start);
			results.setTime(time);
			return results;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	protected LuceneIndexSearcher getSearcher(String className) throws IOException {
		LuceneIndexSearcher searcher = searchers.get(className);
		if(searcher == null) {
			searcher = new LuceneIndexSearcher(indexPath+className);
			searchers.put(className, searcher);
		}
		return searcher;
	}
	protected boolean inHolidayPeriod() {
		return false;
	}
	protected Document cloneDocument(Document doc) {
		Document document = new Document();
		for(Object fieldObj : doc.getFields()) {
			Field field = (Field)fieldObj;
			if(!field.name().equals("ad_size") && !field.name().equals("bid")) {
				document.add(field);
			}
		}
		return document;
	}
	public String getIndexPath() {
		return indexPath;
	}
	public void setIndexPath(String indexPath) {
		this.indexPath = indexPath;
	}
	
}
