package com.flowers.portal.service.impl;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.TermQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flowers.portal.service.OrderService;
import com.flowers.portal.service.PortalService;
import com.flowers.portal.service.SearchService;
import com.flowers.portal.vo.Order;
import com.flowers.portal.vo.RestResponse;
import com.flowers.server.entity.Facility;
import com.flowers.server.entity.FacilityListing;
import com.flowers.server.entity.Listing;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.State;
import com.flowers.server.entity.User;
import com.flowers.server.search.SearchQuery;
import com.flowers.server.search.SearchQueryType;
import com.flowers.server.search.SearchResultSet;

@Service("PortalService")
public class PortalServiceImpl extends LuceneSearchServiceSupport implements PortalService {
	@Autowired private SearchService search;
	@Autowired private OrderService orderService;
	
	public RestResponse order(User user, Order order) {
		return orderService.sendOrder(user, order);
	}
	
	public String getStatesOptions(String choice, String countryId) {
		StringBuffer buff = new StringBuffer();
		SearchQuery searchQuery = new SearchQuery(SearchQueryType.STATEBYCOUNTRY, SearchQuery.RETURN_TYPE_DOCUMENT);
		searchQuery.setCountryId(countryId);
		SearchResultSet<Document> states = search.search(searchQuery);
		if(states != null) {
			for(Document doc : states.getResults()) {
				String id = doc.get("id");
				String shortName = doc.get("short_name");
				String name = doc.get("name");
				if(shortName.equals(choice)) buff.append("<option value="+id+" selected>"+name+"</option>");
				else buff.append("<option value="+id+">"+doc.get("name")+"</option>");
			}
		}
		return buff.toString();
	}
	
	public String getCountryOptions(String choice) {
		StringBuffer buff = new StringBuffer();
		SearchResultSet<Document> states = search.search(new SearchQuery(SearchQueryType.STATE, SearchQuery.RETURN_TYPE_DOCUMENT));
		Map<String,String> uniqueCountries = new HashMap<String, String>();
		if(states != null) {
			for(Document doc : states.getResults()) {
				String id = doc.get("country_id");
				String name = doc.get("country.name");
				if(uniqueCountries.get(id) == null && !name.equals("INTERNATIONAL")){
					if(id.equals(choice)) buff.append("<option value="+id+" selected>"+name+"</option>");
					else buff.append("<option value="+id+">"+doc.get("country.name")+"</option>");
					uniqueCountries.put(id, name);
				}
			}
		}
		return buff.toString();
	}
	
	public List<Document> getProducts() {
		SearchQuery query = new SearchQuery(SearchQueryType.CODIFIED_PRODUCT, SearchQuery.RETURN_TYPE_DOCUMENT);
		SearchResultSet<Document> docs = search.search(query);
		if(docs != null)	return docs.getResults();
		return new ArrayList<Document>(0);
	}
	public List<Document> getDropship() {
		SearchQuery query = new SearchQuery(SearchQueryType.DROPSHIP, SearchQuery.RETURN_TYPE_DOCUMENT);
		SearchResultSet<Document> docs = search.search(query);
		if(docs != null)	return docs.getResults();
		return new ArrayList<Document>(0);
	}
	public List<String> getBannerAds() {
		SearchQuery query = new SearchQuery(SearchQueryType.BANNERADS, SearchQuery.RETURN_TYPE_DOCUMENT);
		SearchResultSet<Document> docs = search.search(query);
		if(docs != null && docs.getResults() != null){
			List<String> results = new ArrayList<String>();
			for(Document doc : docs.getResults()){
				results.add(doc.get("shop_code") + "--" + doc.get("website"));
			}
			Collections.shuffle(results);
			return results;
		}
		return new ArrayList<String>(0);
	}
	public List<String> getSideAds() {
		SearchQuery query = new SearchQuery(SearchQueryType.SIDEADS, SearchQuery.RETURN_TYPE_DOCUMENT);
		SearchResultSet<Document> docs = search.search(query);
		if(docs != null && docs.getResults() != null){
			List<String> results = new ArrayList<String>();
			for(Document doc : docs.getResults()){
				results.add(doc.get("file_name"));
			}
			Collections.shuffle(results);
			return results;
		}
		return new ArrayList<String>(0);
	}
	public List<Document> getFruitBouquetProducts() {
		SearchQuery query = new SearchQuery(SearchQueryType.FRUITBOUQUETPRODUCTS, SearchQuery.RETURN_TYPE_DOCUMENT);
		SearchResultSet<Document> docs = search.search(query);
		if(docs != null)	return docs.getResults();
		return new ArrayList<Document>(0);
	}
	public List<Document> fetchCategories() {
		SearchQuery searchQuery = new SearchQuery(SearchQueryType.PRODUCT_CATEGORY, SearchQuery.RETURN_TYPE_DOCUMENT);
		SearchResultSet<Document> results = search.search(searchQuery);
		if(results != null) return results.getResults();
		return new ArrayList<Document>(0);
	}
	public Document getFacilityDocument(String id) {
		return search.document(Facility.class, id);
	}
	public Document getShopDocument(String id) {
		return search.document(Shop.class, new TermQuery(new Term("shop_code", id)));
	}
	public Document getListingDocument(String id) {
		return search.document(Listing.class, id);
	}
	public Document getFacilityListingDocument(String id) {
		return search.document(FacilityListing.class, id);
	}
	public Document getStateDocument(String id) {
		Document stateDoc = search.document(State.class, new TermQuery(new Term("name", id)));
		if(stateDoc == null)
			stateDoc = search.document(State.class, new TermQuery(new Term("short_name", id)));
		return stateDoc;
	}
	
}
