/**
 * 
 */
package com.flowers.portal.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Properties;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.flowers.portal.dao.OrderDAO;
import com.flowers.portal.exception.UserNotFoundException;
import com.flowers.portal.exception.UserWrongPasswordException;
import com.flowers.portal.exception.WorkingShopNotFoundException;
import com.flowers.portal.service.AuthUserDetailsService;
import com.flowers.portal.util.DateUtil;
import com.flowers.server.entity.User;


/**
 * An implementation of the authentication service.
 * 
 * This implementation will find a user(s) in the DB by email and
 * validate this user(s) by a password.
 * 
 * On success the user object will be populated with a list of views
 * which match the user roles.
 * 
 * View access settings are loaded from a configuration file.
 * 
 * @author BloomNet Technologies
 */
@Service("authService")
@Transactional(propagation = Propagation.SUPPORTS)
public class DbUserauthenticator implements AuthUserDetailsService {
	
    // Define a static logger variable
    static Logger logger = Logger.getLogger( DbUserauthenticator.class );
	
	@Autowired protected Properties viewAccess;
	protected DocumentBuilderFactory factory;

	@Autowired protected Properties directoryProperties;
    @Autowired protected OrderDAO orderDAO;



	
	public DbUserauthenticator(){}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.AuthUserDetailsService#authenticate(org.springframework.security.core.userdetails.User)
	 */
	public User authenticate(User formUser) throws 				    UserNotFoundException,
																	    UserWrongPasswordException, 
																	    WorkingShopNotFoundException {

		String userName = formUser.getUserName();
		String password = formUser.getPassword();
		String shopCode = formUser.getShopCode();
		User result = new User();
		
		boolean isBlkValid = validateBloomLinkCredentials(userName, password, shopCode);
		
		if (isBlkValid){

		User dbUser = orderDAO.getUserByUserName(userName);
		
		if (dbUser == null) {
			
			throw new UserNotFoundException( formUser.getUserName() );
		}
		if (dbUser.getId()>0){
			result = validateUser( formUser, dbUser );
		}else{
				try {
					result = createUser( userName+"@bloomnet.com", userName, password, shopCode, userName, shopCode);
				} catch (Exception e) {
					e.printStackTrace();
				}
			
		}
			}else{
			throw new UserNotFoundException( formUser.getUserName() );
		}
		
		
		
		return result;
	}
	
	protected User validateUser( User formUser,
			User dbUser ) {

		if( formUser.getPassword().equals(dbUser.getPassword())){
			return dbUser;
		}
		else {

			dbUser.setPassword(formUser.getPassword());
			orderDAO.saveUser(dbUser);
			return dbUser;
		}
	}

	
	public boolean validateBloomLinkCredentials(String userName,
			String password, String shopCode) {
		
		if(userName != null && userName.length() > 0) {
			if(shopCode != null && shopCode.length() > 0 ) {
				try {
					StringBuffer params = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
					params.append("<memberDirectoryInterface>");
					params.append("<searchShopRequest>");
					params.append("<security>");
					params.append("<username>" + userName + "</username>");
					params.append("<password>" + password + "</password>");
					params.append("<shopCode>" + shopCode + "</shopCode>");
					params.append("</security>");
					params.append("<memberDirectorySearchOptions>");
					params.append("<searchByShopCode>");
					params.append("<shopCode>"+shopCode+"</shopCode>");
					params.append("</searchByShopCode>");
					params.append("</memberDirectorySearchOptions>");
					params.append("</searchShopRequest>");
					params.append("</memberDirectoryInterface>");
					String webServiceData = "func=getMemberDirectory&data=" + URLEncoder.encode(params.toString(), "UTF-8");
					String url = directoryProperties.getProperty("address");
					String result = sendGetRequest(url, webServiceData);
		
					if(authenticated(result)) {
					/*	User user = UserLocalServiceUtil.getUserByScreenName(companyId, screenName);
						if(!user.getOpenId().equals(shop_code[0]+"///"+password+"///"+username[0])) {
							UserLocalServiceUtil.updateOpenId(user.getUserId(), shop_code[0]+"///"+password+"///"+username[0]);
							UserLocalServiceUtil.updatePassword(user.getUserId(), password, password, false);
							}*/
						
						return true;
						} else {
						return false;
					}
			} catch (Exception nsue) {
				
					System.err.println("Cannot get connection to FSI: " + nsue);
				
			}
			} else {
				try {
					User user = orderDAO.getUserByUserName(userName);
					String encryptedPassword = PasswordAuthenticator.encrypt(password, user.getPassword());
					if(user.getPassword().equals(encryptedPassword)) {
						return true;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
	}
		return false;
	}
	
	@Override
	public String getShopName(String userName,
			String password, String shopCode) {
		
		String shop = "";

		if(userName != null && userName.length() > 0) {
			if(shopCode != null && shopCode.length() > 0 ) {
				try {
					StringBuffer params = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
					params.append("<memberDirectoryInterface>");
					params.append("<searchShopRequest>");
					params.append("<security>");
					params.append("<username>" + userName + "</username>");
					params.append("<password>" + password + "</password>");
					params.append("<shopCode>" + shopCode + "</shopCode>");
					params.append("</security>");
					params.append("<memberDirectorySearchOptions>");
					params.append("<searchByShopCode>");
					params.append("<shopCode>"+shopCode+"</shopCode>");
					params.append("</searchByShopCode>");
					params.append("</memberDirectorySearchOptions>");
					params.append("</searchShopRequest>");
					params.append("</memberDirectoryInterface>");
					String webServiceData = "func=getMemberDirectory&data=" + URLEncoder.encode(params.toString(), "UTF-8");
					String url = directoryProperties.getProperty("address");
					String result = sendGetRequest(url, webServiceData);
		
					if(authenticated(result)) {
						Document document = getDOMDocumentFromString(result);		
						Element root = document.getDocumentElement();
						 NodeList shopname = root.getElementsByTagName("name");
						if(shopname.getLength() == 1) {
							//
						Node shopNode = shopname.item(0);
						shop = shopNode.getTextContent();
						}
						
						} 
			} catch (Exception nsue) {
				
					System.err.println("Cannot get connection to FSI: " + nsue);
				
			}
			} 
	}
		return shop;
	}

				protected boolean authenticated(String response) throws IOException {
					Document document = getDOMDocumentFromString(response);		
					Element root = document.getDocumentElement();
					NodeList errorList = root.getElementsByTagName("errors");
					if(errorList.getLength() == 1) {
						Node errors = errorList.item(0);
						if(!errors.hasChildNodes())
							return true;
					}
					return false;
				}

				public org.w3c.dom.Document getDOMDocumentFromString(String content) {
					try {
						if(factory == null) factory = DocumentBuilderFactory.newInstance();
						factory.setValidating(false);
						org.w3c.dom.Document doc = factory.newDocumentBuilder().parse(new InputSource(new StringReader(content)));
						return doc;
					} catch (SAXException e) { e.printStackTrace();
					} catch (ParserConfigurationException e) { e.printStackTrace();
					} catch (IOException e) { e.printStackTrace(); }
					return null;
				}
				public static String sendGetRequest(String endpoint, String requestParameters) {
					String result = null;
					if (endpoint.startsWith("http://")) {
						// Send a GET request to the servlet
						try {
							// Send data
							String urlStr = endpoint;
							if (requestParameters != null && requestParameters.length() > 0) {
								urlStr += "?" + requestParameters;
							}
							
							URL url = new URL(urlStr);
							URLConnection conn = url.openConnection();

							// Get the response
							BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
							StringBuffer sb = new StringBuffer();
							String line;
							while ((line = rd.readLine()) != null) {
								sb.append(line);
							}
							rd.close();
							result = sb.toString();
						}
						catch (Exception e) {
							e.printStackTrace();
						}
					}
					return result;
				}
				
				protected User createUser( String email, String userName, String password, String shopCode, String firstName, String lastName) throws Exception {
					
					
					User user  = new User();
					user.setFirstName(firstName);
					user.setLastName(lastName);
					user.setUserName(userName);
					user.setShopCode(shopCode);
					user.setPassword(password);
					user.setEmail(email);
					String createdDate = DateUtil.toXmlFormatString(new Date());
					user.setCreatedDate(createdDate );
					user.setCreatedUser_id(1);
					user.setModifiedUser_id(1);
					user.setViewPreference(2);

					
					orderDAO.saveUser(user);
					
					return user;

				
				}

	/**
	 * Validates a WebAppUser.
	 * 
	 * @param user
	 * @return
	 * @throws Exception
	 */
	
}
