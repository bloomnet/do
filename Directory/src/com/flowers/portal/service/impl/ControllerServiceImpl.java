package com.flowers.portal.service.impl;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.lucene.document.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.flowers.portal.dao.OrderDAO;
import com.flowers.portal.service.ControllerService;
import com.flowers.portal.service.PortalService;
import com.flowers.portal.util.Money;
import com.flowers.portal.vo.DescribedProduct;
import com.flowers.portal.vo.Order;
import com.flowers.portal.vo.Recipient;
import com.flowers.server.entity.Address;
import com.flowers.server.entity.City;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.State;
import com.flowers.server.entity.User;
import com.flowers.server.entity.Userrole;

@Service("controllerService")
@Transactional(propagation = Propagation.SUPPORTS)
public class ControllerServiceImpl implements ControllerService {
	
    @Autowired private PortalService portal;
    @Autowired private OrderDAO orderDAO;
    @Autowired private Properties directoryProperties;


	@Override
	public void populateSummaryParameters(HttpServletRequest request, Order order) {
		if(order.getRecipient() != null) {
			List<DescribedProduct> describedProducts = new ArrayList<DescribedProduct>();
			List<DescribedProduct> allProducts = order.getDescribedProducts();
			Money subTotal = new Money(new BigDecimal(0));
			//subTotal.setScale(DECIMAL_PLACES);
			int quantity = 0;
			for(DescribedProduct product : allProducts) {
				describedProducts.add(product);
				for(int i=0; i < product.getQuantity(); i++) {
					Money price = product.getPrice();
					Money newSubTotal = subTotal.plus(price);
					subTotal = newSubTotal;
					quantity++;
				}
			}
			request.setAttribute("describedProducts", describedProducts);
			order.setSubTotal(subTotal);
			order.setQuantity(quantity);
			if(order.getDeliveryCharge() != null) {
				order.setGrandTotal(subTotal.plus(order.getDeliveryCharge()));
			} else {
				order.setGrandTotal(subTotal);
				order.setDeliveryCharge(new Money(new BigDecimal(0)));
			}
		}	
		
	}


	@Override
	public Order processOrderParameters(HttpServletRequest httprequest, boolean dropship) {
		List<Document> products = portal.getProducts();
		Order order = (Order)httprequest.getSession(true).getAttribute("BLOOMNET_ORDER");
		if(order == null) {
			order = new Order();
			httprequest.getSession(true).setAttribute("BLOOMNET_ORDER", order);
		}
		if(dropship){
			String deliveryDate = httprequest.getParameter("deliveryDate");
			order.setDeliveryDate(deliveryDate);
			String dropshipShopCode = directoryProperties.getProperty("dropshipShopCode");
			Document shopDoc = portal.getShopDocument(dropshipShopCode);
			if(shopDoc != null) {
				Shop shop = new Shop(shopDoc.get("shop_code"), shopDoc.get("shop_name"));
				Address address = new Address();
				if(shopDoc.get("address1") != null && !shopDoc.get("address1").equals("")) address.setStreetAddress1(shopDoc.get("address1"));
				if(shopDoc.get("address2") != null && !shopDoc.get("address2").equals("")) address.setStreetAddress2(shopDoc.get("address2"));	
				if(shopDoc.get("telephone_number") != null && !shopDoc.get("telephone_number").equals("")) shop.setTelephoneNumber(shopDoc.get("telephone_number"));
				if(shopDoc.get("toll_free_number") != null && !shopDoc.get("toll_free_number").equals("")) shop.setTollFreeNumber(shopDoc.get("toll_free_number"));
				if(shopDoc.get("contact_person") != null && !shopDoc.get("contact_person").equals("")) shop.setContactPerson(shopDoc.get("contact_person"));
				if(shopDoc.get("zip") != null && !shopDoc.get("zip").equals("")) {
					String zip = shopDoc.get("zip");
					if(zip.length() == 4) address.setPostalCode("0"+zip);
					else address.setPostalCode(zip);
				}
				if(shopDoc.get("city_id") != null && !shopDoc.get("city_id").equals("")) {
					//String country_id = shopDoc.get("country_id");
					//String country_name = shopDoc.get("country");
					String state_id = shopDoc.get("state_id");
					String state_name = shopDoc.get("state");
					String state_code = shopDoc.get("state_code");
					String city_id = shopDoc.get("city_id");
					String city_name = shopDoc.get("city");
					//Country country = new Country(Long.valueOf(country_id), country_name);
					State state = new State(Long.valueOf(state_id), null, state_name, state_code);
					City city = new City(Long.valueOf(city_id), state, city_name);
					address.setCity(city);
				}
				shop.setAddress(address);
				order.setFulfillingShop(shop);
			}
		}
		// TODO request.setAttribute("order", order);
		String facilityId = httprequest.getParameter("selectedFacilityId");
		String orderNameFirst = httprequest.getParameter("orderNameFirst");
		String orderNameLast = httprequest.getParameter("orderNameLast");
		String addressIptPhone = httprequest.getParameter("orderAddressIptPhone");
		String addressIptStreet1 = httprequest.getParameter("orderAddressIptStreet1");
		String addressIptStreet2 = httprequest.getParameter("orderAddressIptStreet2");
		String acity = httprequest.getParameter("orderSelCity");
		String containsPerishables = httprequest.getParameter("containsPerishables");
		
		String addressIptCity = null;
		String addressSelState = null;
		if (acity!=null){
		 addressIptCity = acity.replaceAll("\\+"," ");
		}
		String sstate = httprequest.getParameter("orderSelState");
		if (sstate!=null){
		 addressSelState = sstate.replaceAll("\\+"," ");
		}
		

		String addressIptPostal = httprequest.getParameter("orderZip");
		
		String addressSelOccasion = httprequest.getParameter("orderAddressSelOccasion");
		
		String orderCardMsgTxt = httprequest.getParameter("orderCardMsgTxt");
		String specialInstructionsTxt = httprequest.getParameter("orderSpecialInstructionsTxt");
		
		String additionalCities = httprequest.getParameter("extraCities");
		String previousCity = "";
		String previousState = "";
		String previousCountry = "";
		try{
			previousCity = order.getRecipient().getCity();
			previousState =  order.getRecipient().getState();
			previousCountry = order.getRecipient().getCountryCode();
		}catch(Exception ee){
		}
		
		boolean cityFlag = false;
		boolean stateFlag = false;
		boolean countryFlag = false;
		
		if(addressSelOccasion != null && !addressSelOccasion.equals("")) {
			if(addressSelOccasion.equals("Anniversary")) order.setOcasionId(7L);
			else if(addressSelOccasion.equals("Birthday")) order.setOcasionId(3L);
			else if(addressSelOccasion.replaceAll("\\+", " ").equals("Business Gift")) order.setOcasionId(4L);
			else if(addressSelOccasion.equals("Funeral/Memorial")) order.setOcasionId(1L);
			else if(addressSelOccasion.equals("Holiday")) order.setOcasionId(5L);
			else if(addressSelOccasion.replaceAll("\\+", " ").equals("Illness / Get Well")) order.setOcasionId(2L);
			else if(addressSelOccasion.replaceAll("\\+", " ").equals("Maternity / Birth")) order.setOcasionId(6L);
			else if(addressSelOccasion.equals("Other")) order.setOcasionId(8L);
		} else order.setOcasionId(8L);
		if(orderCardMsgTxt != null && !orderCardMsgTxt.equals("") && !orderCardMsgTxt.replaceAll("\\+", " ").equals("Card message")) order.setCardMessage(orderCardMsgTxt.replaceAll("\\+", " "));
		if(specialInstructionsTxt != null && !specialInstructionsTxt.equals("") && !specialInstructionsTxt.replaceAll("\\+", " ").equals("Special Instructions")) order.setSpecialInstruction(specialInstructionsTxt.replaceAll("\\+", " "));
		if(orderNameFirst != null && !orderNameFirst.equals("")) {
			if(order.getRecipient() == null) {
				order.setRecipient(new Recipient());
			}
			order.getRecipient().setFirstName(orderNameFirst.replaceAll("\\+", " "));
		}
		if(orderNameLast != null && !orderNameLast.equals("")) order.getRecipient().setLastName(orderNameLast.replaceAll("\\+", " "));
		if(addressIptPhone != null && !addressIptPhone.equals("")) order.getRecipient().setPhoneNumber(addressIptPhone.replaceAll("\\+", " "));
		
		if(facilityId != null && !facilityId.equals("") && !facilityId.equals("[object Object]")) {
			order.setFacilityId(facilityId);
			Document facility = portal.getFacilityDocument(facilityId);
			if (order.getRecipient()==null){
				order.setRecipient(new Recipient());
			}
			if(facility.get("name") != null && !facility.get("name").equals("")) order.setFacilityName(facility.get("name").replaceAll("\\+", " "));
			if(facility.get("address1") != null && !facility.get("address1").equals("")) order.getRecipient().setAddress1(facility.get("address1").replaceAll("\\+", " "));
			if(facility.get("address2") != null && !facility.get("address2").equals("")) order.getRecipient().setAddress2(facility.get("address2").replaceAll("\\+", " "));			 		 
			if(facility.get("zip") != null && !facility.get("zip").equals("")) {
				String facilityZip = facility.get("zip");
				if(facilityZip.length() == 5) order.getRecipient().setPostalCode(facilityZip.replaceAll("\\+", " "));
				else if(facilityZip.length() > 5) order.getRecipient().setPostalCode(facilityZip.replaceAll("\\+", " "));
			}
			if(facility.get("city_id") != null && !facility.get("city_id").equals("")) {
				String country_code = facility.get("country");
				if(country_code.equals("US")) country_code = "USA";
				else if(country_code.equals("CA")) country_code = "CAN";
				else if(country_code.equals("PR")) country_code = "PUR";
				String state_code = facility.get("state_code");
				String city_name = facility.get("city");
				order.getRecipient().setCity(city_name.replaceAll("\\+", " "));
				order.getRecipient().setState(state_code.replaceAll("\\+", " "));
				order.getRecipient().setCountryCode(country_code.replaceAll("\\+", " "));
			}
		} else {			
			if(addressIptStreet1 != null && !addressIptStreet1.equals("") && !addressIptStreet1.replaceAll("\\+", " ").equals("Street Address 1")) order.getRecipient().setAddress1(addressIptStreet1.replaceAll("\\+", " "));
			if(addressIptStreet2 != null && !addressIptStreet2.equals("") && !addressIptStreet2.replaceAll("\\+", " ").equals("Street Address 2")) order.getRecipient().setAddress2(addressIptStreet2.replaceAll("\\+", " "));
			if(addressIptPostal != null && !addressIptPostal.equals("")) {
				order.getRecipient().setPostalCode(addressIptPostal.replaceAll("\\+", " "));
			}
			if(addressIptCity != null && !addressIptCity.equals("")){
				if(previousCity != null && addressIptCity.equals(previousCity)) cityFlag = true;
				order.getRecipient().setCity(addressIptCity.replaceAll("\\+", " "));
			}
			if(addressSelState != null && !addressSelState.equals("")) {				
				Document state = portal.getStateDocument(addressSelState);
				if(state != null) {
					if(previousState != null && state.get("short_name").equals(previousState)) stateFlag = true;
					if(previousCountry != null && state.get("country.name").equals(previousCountry)) countryFlag = true;
					order.getRecipient().setState(state.get("short_name").replaceAll("\\+", " "));
					order.getRecipient().setCountryCode(state.get("country.short_name").replaceAll("\\+", " "));
					order.getRecipient().setCountryId(state.get("country_id"));
				}
			}
			if(!cityFlag || !stateFlag || !countryFlag){ 
				order.getRecipient().setAdditionalCities("");
				if(addressIptPostal != null && !addressIptPostal.equals("") && additionalCities != null && !additionalCities.equals("")) 
					order.getRecipient().setAdditionalCities(additionalCities.replaceAll("\\+", " "));
			}
		}
		order.getDescribedProducts().clear();
		for(Document prod : products) {
			String id = prod.get("id");
			if(id != null) {
				String parm = httprequest.getParameter(id);
				if(parm != null) {
					String desc = prod.get("name");
					String price = httprequest.getParameter("productPrice"+id);
					String code = prod.get("code");
					String quantityParm = httprequest.getParameter("productQuantity"+id);					
					if(quantityParm != null && price != null) { 
						DescribedProduct product = new DescribedProduct(Integer.valueOf(quantityParm), desc, new Money(Double.valueOf(price)));
						product.setId(Long.valueOf(id));
						if(!code.replaceAll("\\+", " ").equals("Product Code")) product.setCode(code.replaceAll("\\+", " "));
						else product.setCode("");
						product.setCodified(true);
						order.getDescribedProducts().add(product);
					}
				}
			}
		}
		String productId = httprequest.getParameter("productId");
		if(productId != null && !productId.equals("") && !productId.equals("1")){
			order.setProductId(productId);
		}
		String[] prodCodes = httprequest.getParameterValues("productCode");
		String[] prodQuants = httprequest.getParameterValues("productQuantity");
		String[] prodDescs = httprequest.getParameterValues("productDescription");
		String[] prodPrices = httprequest.getParameterValues("productPrice");
		if(prodQuants != null) {
			for(int i=0; i < prodQuants.length; i++) {
				if(prodDescs.length > i && prodPrices.length > i && !prodQuants[i].equals("Quantity") && !prodDescs[i].equals("Product Description") && !prodPrices[i].equals("Price")) {
					try {
						Integer quant = Integer.valueOf(prodQuants[i].trim());
						String prodDesc = prodDescs[i].replaceAll("\\+", " ");
						try{
							prodDesc = prodDesc.split("<br")[0];
						}catch(Exception ee){}
						DescribedProduct product = new DescribedProduct(quant, prodDesc, new Money(Double.valueOf(prodPrices[i].replace(",", ""))));
						product.setCodified(false);
						if(!prodCodes[i].replaceAll("\\+", " ").equals("Product Code")) product.setCode(prodCodes[i].replaceAll("\\+", " "));
						else product.setCode("");
						order.getDescribedProducts().add(product);
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		order.setContainsPerishables(containsPerishables);
		httprequest.setAttribute("order", order);


		return order;
	}

	@Override
	public Order processSearchParameters(HttpServletRequest request) {
		Order order = (Order)request.getSession(true).getAttribute("BLOOMNET_ORDER");
		if(order == null) {
			order = new Order();
			request.getSession(true).setAttribute("BLOOMNET_ORDER", order);
		}
		request.setAttribute("order", order);
		
		String listingId = request.getParameter("selectedListing");
		if(listingId != null && !listingId.equals("")) {
			
			Document listing = null;
			
			if(order.getFacilityName() != null)
				listing = portal.getFacilityListingDocument(listingId);
			else
				listing = portal.getListingDocument(listingId);
			if(listing != null) {
				Document shopDoc = portal.getShopDocument(listing.get("shopcode"));
				if(shopDoc != null) {
					Shop shop = new Shop(shopDoc.get("shop_code"), shopDoc.get("shop_name"));
					Address address = new Address();
					if(shopDoc.get("address1") != null && !shopDoc.get("address1").equals("")) address.setStreetAddress1(shopDoc.get("address1"));
					if(shopDoc.get("address2") != null && !shopDoc.get("address2").equals("")) address.setStreetAddress2(shopDoc.get("address2"));	
					if(shopDoc.get("telephone_number") != null && !shopDoc.get("telephone_number").equals("")) shop.setTelephoneNumber(shopDoc.get("telephone_number"));
					if(shopDoc.get("toll_free_number") != null && !shopDoc.get("toll_free_number").equals("")) shop.setTollFreeNumber(shopDoc.get("toll_free_number"));
					if(shopDoc.get("contact_person") != null && !shopDoc.get("contact_person").equals("")) shop.setContactPerson(shopDoc.get("contact_person"));
					if(shopDoc.get("zip") != null && !shopDoc.get("zip").equals("")) {
						String zip = shopDoc.get("zip");
						if(zip.length() == 4) address.setPostalCode("0"+zip);
						else address.setPostalCode(zip);
					}
					if(shopDoc.get("city_id") != null && !shopDoc.get("city_id").equals("")) {
						//String country_id = shopDoc.get("country_id");
						//String country_name = shopDoc.get("country");
						String state_id = shopDoc.get("state_id");
						String state_name = shopDoc.get("state");
						String state_code = shopDoc.get("state_code");
						String city_id = shopDoc.get("city_id");
						String city_name = shopDoc.get("city");
						//Country country = new Country(Long.valueOf(country_id), country_name);
						State state = new State(Long.valueOf(state_id), null, state_name, state_code);
						City city = new City(Long.valueOf(city_id), state, city_name);
						address.setCity(city);
					}
					shop.setAddress(address);
					order.setFulfillingShop(shop);
				}
			}
		}
		String searchDate = request.getParameter("searchDeliveryDate");
		if(searchDate != null && !searchDate.equals("")) {
			order.setDeliveryDate(searchDate);
		}
		
		String productId = request.getParameter("productId");
		if(productId != null && !productId.equals("") && !productId.equals("1")){
			order.setProductId(productId);
		}
		
		return order;
	}
	
	
	@Override
	public void SaveUser(User user){
		
		orderDAO.saveUser(user);
		
	}
	
	@Override
	public User getUserByUserName(String userName) {

		User result = orderDAO.getUserByUserName(userName);
		
		return result;
	}

	@Override
	public List<Userrole> getUserrolesByUserId(String userId) {
		
		List<Userrole> result = orderDAO.getUserrolesByUserId(userId);
		
		return result;
	}
	
	@Override
	public String SHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest md;
		//System.err.println("text is " + text);
		//System.err.println("text length is " + text.length());
		md = MessageDigest.getInstance("SHA-1");
		//System.err.println("md is " + md);
		byte[] sha1hash = new byte[40];
		md.update(text.getBytes("iso-8859-1"), 0, text.length());
		sha1hash = md.digest();
		//System.err.println("sha1hash" + sha1hash);
		return convertToHex(sha1hash);
	}
	
	public static void main(){
		String text = "=)@s[!v(AA222000010/31/2017";
		MessageDigest md = null;
		//System.err.println("text is " + text);
		//System.err.println("text length is " + text.length());
		try {
			md = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.err.println("md is " + md);
		byte[] sha1hash = new byte[40];
		try {
			md.update(text.getBytes("iso-8859-1"), 0, text.length());
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sha1hash = md.digest();
	}
	
	private static String convertToHex(byte[] data) {
		StringBuffer buf = new StringBuffer();
		//System.err.println("Length is:" + data.length);
		for (int i = 0; i < data.length; i++) {
			int halfbyte = (data[i] >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				if ((0 <= halfbyte) && (halfbyte <= 9))
					buf.append((char) ('0' + halfbyte));
				else
					buf.append((char) ('a' + (halfbyte - 10)));
				halfbyte = data[i] & 0x0F;
			} while (two_halfs++ < 1);
		}
		//System.err.println("Print the Hash Buffer:" + buf.toString());
		return buf.toString();
	}

}
