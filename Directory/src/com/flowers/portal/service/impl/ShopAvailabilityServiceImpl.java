package com.flowers.portal.service.impl;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.springframework.stereotype.Service;

import com.flowers.portal.service.ShopAvailabilityService;
import com.flowers.portal.util.WebServiceUtil;

@Service("ShopAvailabilityService")
public class ShopAvailabilityServiceImpl implements ShopAvailabilityService {
	protected final Log logger = LogFactory.getLog(getClass());
	private LinkedList<String> status = new LinkedList<String>();
	private boolean tlo = false;
	Properties props = new Properties();

	//@Autowired private Properties directoryProperties;

		
	@Override
	public LinkedList<String> searchShopCodesByAvailabilityDate(String availabilityDate, 
			LinkedList<String> availabilityShopCodes) throws IOException, JDOMException {
		 
		FileInputStream fos = null; 

		try {
	
		     fos = new FileInputStream("/opt/apps/properties/directory.properties");
			 props.load(fos);
		        	
			String webServiceUrl = props.getProperty("address");
			String username = props.getProperty("username");
			String password = props.getProperty("password");
			String shopcode = props.getProperty("shopcode");
			
			
			StringBuilder params = new StringBuilder();
			params.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			params.append("<memberDirectoryInterface>");
			params.append("<searchShopRequest>");
			params.append("<security>");
			params.append("<username>"+username+"</username>");
			params.append("<password>"+password+"</password>");
			params.append("<shopCode>"+shopcode+"</shopCode>");
			params.append("</security>");
			params.append("<memberDirectorySearchOptions>");
			params.append("<searchAvailabilityByShopCodesAndDeliveryDate>");
			params.append("<deliveryDate>" + availabilityDate + "</deliveryDate>");
			params.append("<shopCodes>");
			for(int ii=0; ii<availabilityShopCodes.size(); ++ii){
				params.append("<code>"+ availabilityShopCodes.get(ii) +"</code>");
			}
			params.append("</shopCodes>");
			params.append("</searchAvailabilityByShopCodesAndDeliveryDate>");
			params.append("</memberDirectorySearchOptions>");
			params.append("</searchShopRequest>");
			params.append("</memberDirectoryInterface>");
			String webServiceData = "func=getMemberDirectory&data=" + URLEncoder.encode(params.toString(), "UTF-8");
			//logger.debug("searchShopCodesByAvailabilityDate.webServiceUrl=\n" + webServiceUrl);
			//logger.debug("searchShopCodesByAvailabilityDate.webServiceData=\n" + webServiceData);
			
			String result = WebServiceUtil.sendGetRequest(webServiceUrl,webServiceData);
			getStatus(result, availabilityShopCodes.size(),props);
		} catch(Exception e) {
			e.printStackTrace();	
			return new LinkedList<String>();
		}finally{
			fos.close();
		}
		return status;		
	}
	
	@Override
	public String searchShopCodesByAvailabilityDateCDO(String availabilityDate, 
			List<String> availabilityShopCodes) throws IOException, JDOMException {
		 
		FileInputStream fos = null; 
		String result = "";

		try {
	
		     fos = new FileInputStream("/opt/apps/properties/directory.properties");
			 props.load(fos);
		        	
			String webServiceUrl = props.getProperty("address");
			String username = props.getProperty("username");
			String password = props.getProperty("password");
			String shopcode = props.getProperty("shopcode");
			
			
			StringBuilder params = new StringBuilder();
			params.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			params.append("<memberDirectoryInterface>");
			params.append("<searchShopRequest>");
			params.append("<security>");
			params.append("<username>"+username+"</username>");
			params.append("<password>"+password+"</password>");
			params.append("<shopCode>"+shopcode+"</shopCode>");
			params.append("</security>");
			params.append("<memberDirectorySearchOptions>");
			params.append("<searchAvailabilityByShopCodesAndDeliveryDate>");
			params.append("<deliveryDate>" + availabilityDate + "</deliveryDate>");
			params.append("<shopCodes>");
			for(int ii=0; ii<availabilityShopCodes.size(); ++ii){
				params.append("<code>"+ availabilityShopCodes.get(ii) +"</code>");
			}
			params.append("</shopCodes>");
			params.append("</searchAvailabilityByShopCodesAndDeliveryDate>");
			params.append("</memberDirectorySearchOptions>");
			params.append("</searchShopRequest>");
			params.append("</memberDirectoryInterface>");
			String webServiceData = "func=getMemberDirectory&data=" + URLEncoder.encode(params.toString(), "UTF-8");
			
			result = WebServiceUtil.sendGetRequest(webServiceUrl,webServiceData);
			
		} catch(Exception e) {
			e.printStackTrace();	
		}finally{
			fos.close();
		}
		return result;		
	}
	
	@Override
	public String searchShopHoursByCityStateZip(String availabilityDate, 
			String city, String state) throws IOException, JDOMException {
		 
		FileInputStream fos = null; 
		String result = "";

		try {
	
		     fos = new FileInputStream("/opt/apps/properties/directory.properties");
			 props.load(fos);
		        	
			String webServiceUrl = props.getProperty("address");
			String username = props.getProperty("username");
			String password = props.getProperty("password");
			String shopcode = props.getProperty("shopcode");
			
			
			StringBuilder params = new StringBuilder();
			params.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			params.append("<memberDirectoryInterface>");
			params.append("<searchShopRequest>");
			params.append("<security>");
			params.append("<username>"+username+"</username>");
			params.append("<password>"+password+"</password>");
			params.append("<shopCode>"+shopcode+"</shopCode>");
			params.append("</security>");
			params.append("<memberDirectorySearchOptions>");
			params.append("<searchByAvailabilityDate>");
			params.append("<availabilityDate>" + availabilityDate + "</availabilityDate>");
			params.append("<zipCode/>");
			if(city != null && !city.equals("")) params.append("<city>" + city.toUpperCase() + "</city>");
			else params.append("<city/>");
			if(state != null && !state.equals("")) params.append("<state>" + state.toUpperCase() + "</state>");
			else params.append("<state/>");
			params.append("</searchByAvailabilityDate>");
			params.append("</memberDirectorySearchOptions>");
			params.append("</searchShopRequest>");
			params.append("</memberDirectoryInterface>");
			String webServiceData = "func=getMemberDirectory&data=" + URLEncoder.encode(params.toString(), "UTF-8");
			//logger.debug("searchShopCodesByAvailabilityDate.webServiceUrl=\n" + webServiceUrl);
			//logger.debug("searchShopCodesByAvailabilityDate.webServiceData=\n" + webServiceData);
			
			result = WebServiceUtil.sendGetRequest(webServiceUrl,webServiceData);
		} catch(Exception e) {
			e.printStackTrace();	
		}finally{
			fos.close();
		}
		return result;		
	}
	
	private void getStatus(String xmlData, int listLength, Properties props) throws IOException, JDOMException {
		if(xmlData != null) {
			SAXBuilder builder = new SAXBuilder();
			Document document = builder.build(new ByteArrayInputStream(xmlData.getBytes()));
			
			Element rootElement = document.getRootElement();
			Element responseElement = rootElement.getChild("searchShopResponse");
			if (responseElement != null) {
				Element availabilityElement = responseElement.getChild("shopAvailability");
				if(availabilityElement != null){
					@SuppressWarnings("unchecked")
					List<Element> availability = availabilityElement.getChildren("availability");
					for(int ii=0; ii<availability.size(); ++ii){
						if (availability.get(ii) != null) {
							Element statusElement = availability.get(ii).getChild("status");
							if(statusElement != null){
								if(statusElement.getValue().toString().equals("N")){
									
									isTlo(availability.get(ii).getChildText("shopCode").toString(), props);
									if(tlo){
										status.add("tlo");
									}else{
										status.add(statusElement.getValue().toString());
									}
								}else{
									status.add(statusElement.getValue().toString());
								}
							}else status.add("N");
						}else status.add("N");
				    }
				}else status.add("N");
			}
		}else{
			for(int ii=0; ii<listLength; ++ii){
				status.add("N");
			}
		}

	}
	
	public boolean isTlo(String shopCode, Properties props){
		try {
			this.tlo = false;
			String webServiceUrl = props.getProperty("address"); //PropertiesUtil.getInstance().getProperty("address");
				String username = props.getProperty("username");
				String password = props.getProperty("password");
				String shopcode = props.getProperty("shopcode");
			StringBuilder params = new StringBuilder();
			params.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			params.append("<memberDirectoryInterface>");
			params.append("<searchShopRequest>");
			params.append("<security>");
			params.append("<username>"+username+"</username>");
			params.append("<password>"+password+"</password>");
			params.append("<shopCode>"+shopcode+"</shopCode>");
			params.append("</security>");
			params.append("<memberDirectorySearchOptions>");
			params.append("<searchByShopCode>");
			params.append("<shopCode>"+shopCode+"</shopCode>");
			params.append("</searchByShopCode>");
			params.append("</memberDirectorySearchOptions>");
			params.append("</searchShopRequest>");
			params.append("</memberDirectoryInterface>");
			String webServiceData = "func=getMemberDirectory&data=" + URLEncoder.encode(params.toString(), "UTF-8");
			String result = WebServiceUtil.sendGetRequest(webServiceUrl,webServiceData);
			getTlo(result);
			if(tlo){
				return true;
			}else{
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} 
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<String> getShopOfferings(String shopCode)throws IOException, JDOMException{
		
		FileInputStream fos = null;
		ArrayList<String> floristOfferings  = new ArrayList<String>();


		try {
	
		     fos = new FileInputStream("/opt/apps/properties/directory.properties");
			 props.load(fos);
		        	
			String webServiceUrl = props.getProperty("address");
			String username = props.getProperty("username");
			String password = props.getProperty("password");
			String shopcode = props.getProperty("shopcode");
			
			
			StringBuilder params = new StringBuilder();
			params.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			params.append("<memberDirectoryInterface>");
			params.append("<searchShopRequest>");
			params.append("<security>");
			params.append("<username>"+username+"</username>");
			params.append("<password>"+password+"</password>");
			params.append("<shopCode>"+shopcode+"</shopCode>");
			params.append("</security>");
			params.append("<memberDirectorySearchOptions>");
			params.append("<searchByShopCode>");
			params.append("<shopCode>" + shopCode + "</shopCode>");
			params.append("<returnNonDeliveryDates>false</returnNonDeliveryDates>");
			params.append("<returnFulfilledZipCodes>false</returnFulfilledZipCodes>");
			params.append("<returnCommunicationCode>false</returnCommunicationCode>");
			params.append("</searchByShopCode>");
			params.append("</memberDirectorySearchOptions>");
			params.append("</searchShopRequest>");
			params.append("</memberDirectoryInterface>");
			String webServiceData = "func=getMemberDirectory&data=" + URLEncoder.encode(params.toString(), "UTF-8");
			
			String result = WebServiceUtil.sendGetRequest(webServiceUrl,webServiceData);
			//getStatus(result, 0,props);
			if(result != null) {
				SAXBuilder builder = new SAXBuilder();
				Document document = builder.build(new ByteArrayInputStream(result.getBytes()));
				
				Element rootElement = document.getRootElement();
				Element responseElement = rootElement.getChild("searchShopResponse");
				Element shopsElement = responseElement.getChild("shops");
				if(shopsElement != null){
					Element shopElement = shopsElement.getChild("shop");
					Element offeringTypes = shopElement.getChild("floristOfferingTypes");
					List<Element> offerings = offeringTypes.getChildren("offeringType");
					
					for(Element element : offerings){
						if(element != null && element.getValue() != null && !element.getValue().equals(""))
							if(element.getValue().equals("1"))
								floristOfferings.add("Florist to Florist");
							else if(element.getValue().equals("3"))
								floristOfferings.add("Fruit Bouquet");
					}
					
				}
	
				fos.close();
			}
		}catch(Exception ee){
				ee.printStackTrace();
		}
		return floristOfferings;				
	}
	
	@Override
	public ArrayList<String> searchByShopCode(String shopCode)throws IOException, JDOMException{
		
		FileInputStream fos = null;
		ArrayList<String> schedule  = new ArrayList<String>();


		try {
	
		     fos = new FileInputStream("/opt/apps/properties/directory.properties");
			 props.load(fos);
		        	
			String webServiceUrl = props.getProperty("address");
			String username = props.getProperty("username");
			String password = props.getProperty("password");
			String shopcode = props.getProperty("shopcode");
			
			
			StringBuilder params = new StringBuilder();
			params.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			params.append("<memberDirectoryInterface>");
			params.append("<searchShopRequest>");
			params.append("<security>");
			params.append("<username>"+username+"</username>");
			params.append("<password>"+password+"</password>");
			params.append("<shopCode>"+shopcode+"</shopCode>");
			params.append("</security>");
			params.append("<memberDirectorySearchOptions>");
			params.append("<searchByShopCode>");
			params.append("<shopCode>" + shopCode + "</shopCode>");
			params.append("<returnNonDeliveryDates>false</returnNonDeliveryDates>");
			params.append("<returnFulfilledZipCodes>false</returnFulfilledZipCodes>");
			params.append("<returnCommunicationCode>false</returnCommunicationCode>");
			params.append("</searchByShopCode>");
			params.append("</memberDirectorySearchOptions>");
			params.append("</searchShopRequest>");
			params.append("</memberDirectoryInterface>");
			String webServiceData = "func=getMemberDirectory&data=" + URLEncoder.encode(params.toString(), "UTF-8");
			//logger.debug("searchShopCodesByAvailabilityDate.webServiceUrl=\n" + webServiceUrl);
			//logger.debug("searchShopCodesByAvailabilityDate.webServiceData=\n" + webServiceData);
			
			String result = WebServiceUtil.sendGetRequest(webServiceUrl,webServiceData);
			//getStatus(result, 0,props);
			if(result != null) {
				SAXBuilder builder = new SAXBuilder();
				Document document = builder.build(new ByteArrayInputStream(result.getBytes()));
				
				Element rootElement = document.getRootElement();
				Element responseElement = rootElement.getChild("searchShopResponse");
				Element shopsElement = responseElement.getChild("shops");
				if(shopsElement != null){
					Element shopElement = shopsElement.getChild("shop");
					Element hoursElement = shopElement.getChild("operationalHours");
					Element scheduleElement = hoursElement.getChild("normalSchedule");
	
	
					if (responseElement != null) {
						//Element availabilityElement = responseElement.getChild("normalSchedule");
						Element sunday = scheduleElement.getChild("sunday");
						Element monday = scheduleElement.getChild("monday");
						Element tuesday = scheduleElement.getChild("tuesday");
						Element wednesday = scheduleElement.getChild("wednesday");
						Element thursday = scheduleElement.getChild("thursday");
						Element friday = scheduleElement.getChild("friday");
						Element saturday = scheduleElement.getChild("saturday");
						
						String timeSunday = "";
						String timeMonday = "";
						String timeTuesday = "";
						String timeWednesday = "";
						String timeThursday = "";
						String timeFriday = "";
						String timeSaturday = "";
						
						if(sunday.getValue() != null && !sunday.getValue().equals("")) timeSunday = sunday.getValue().replaceAll("AM0", "AM-");
						else timeSunday = "Closed";
						if(monday.getValue() != null && !monday.getValue().equals("")) timeMonday = monday.getValue().replaceAll("AM0", "AM-");
						else timeMonday = "Closed";
						if(tuesday.getValue() != null && !tuesday.getValue().equals("")) timeTuesday = tuesday.getValue().replaceAll("AM0", "AM-");
						else timeTuesday = "Closed";
						if(wednesday.getValue() != null && !wednesday.getValue().equals("")) timeWednesday = wednesday.getValue().replaceAll("AM0", "AM-");
						else timeWednesday = "Closed";
						if(thursday.getValue() != null && !thursday.getValue().equals("")) timeThursday = thursday.getValue().replaceAll("AM0", "AM-");
						else timeThursday = "Closed";
						if(friday.getValue() != null && !friday.getValue().equals("")) timeFriday = friday.getValue().replaceAll("AM0", "AM-");
						else timeFriday = "Closed";
						if(saturday.getValue() != null && !saturday.getValue().equals("")) timeSaturday = saturday.getValue().replaceAll("AM0", "AM-");
						else timeSaturday = "Closed";
						
						schedule.add("Sunday: "+ timeSunday);
						schedule.add("Monday: "+ timeMonday);
						schedule.add("Tuesday: "+ timeTuesday);
						schedule.add("Wednesday: "+ timeWednesday);
						schedule.add("Thursday: "+ timeThursday);
						schedule.add("Friday: "+ timeFriday);
						schedule.add("Saturday: "+ timeSaturday);
				//
					}
				}else{
					schedule.add("No Hours Are Currently Available For This Shop");
				}
			}
		} catch(Exception e) {
			e.printStackTrace();	
			return new ArrayList<String>();
		}finally{
			fos.close();
		}
		return schedule;				
	}
	
	@Override
	public String searchHoursByShopCode(String shopCode)throws IOException, JDOMException{
		
		FileInputStream fos = null;


		try {
	
		     fos = new FileInputStream("/opt/apps/properties/directory.properties");
			 props.load(fos);
		        	
			String webServiceUrl = props.getProperty("address");
			String username = props.getProperty("username");
			String password = props.getProperty("password");
			String shopcode = props.getProperty("shopcode");
			
			
			StringBuilder params = new StringBuilder();
			params.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			params.append("<memberDirectoryInterface>");
			params.append("<searchShopRequest>");
			params.append("<security>");
			params.append("<username>"+username+"</username>");
			params.append("<password>"+password+"</password>");
			params.append("<shopCode>"+shopcode+"</shopCode>");
			params.append("</security>");
			params.append("<memberDirectorySearchOptions>");
			params.append("<searchByShopCode>");
			params.append("<shopCode>" + shopCode + "</shopCode>");
			params.append("<returnNonDeliveryDates>false</returnNonDeliveryDates>");
			params.append("<returnFulfilledZipCodes>false</returnFulfilledZipCodes>");
			params.append("<returnCommunicationCode>false</returnCommunicationCode>");
			params.append("</searchByShopCode>");
			params.append("</memberDirectorySearchOptions>");
			params.append("</searchShopRequest>");
			params.append("</memberDirectoryInterface>");
			String webServiceData = "func=getMemberDirectory&data=" + URLEncoder.encode(params.toString(), "UTF-8");
			
			String result = WebServiceUtil.sendGetRequest(webServiceUrl,webServiceData);
			
			return result;

		} catch(Exception e) {
			e.printStackTrace();
			return "";
		}finally{
			fos.close();
		}				
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<String> searchZipCoverageByShopCode(String shopCode)throws IOException, JDOMException{
		
		FileInputStream fos = null;
		ArrayList<String> zipsCovered  = new ArrayList<String>();


		try {
	
		     fos = new FileInputStream("/opt/apps/properties/directory.properties");
			 props.load(fos);
		        	
			String webServiceUrl = props.getProperty("address");
			String username = props.getProperty("username");
			String password = props.getProperty("password");
			String shopcode = props.getProperty("shopcode");
			
			
			StringBuilder params = new StringBuilder();
			params.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			params.append("<memberDirectoryInterface>");
			params.append("<searchShopRequest>");
			params.append("<security>");
			params.append("<username>"+username+"</username>");
			params.append("<password>"+password+"</password>");
			params.append("<shopCode>"+shopcode+"</shopCode>");
			params.append("</security>");
			params.append("<memberDirectorySearchOptions>");
			params.append("<searchByShopCode>");
			params.append("<shopCode>" + shopCode + "</shopCode>");
			params.append("<returnNonDeliveryDates>false</returnNonDeliveryDates>");
			params.append("<returnFulfilledZipCodes>true</returnFulfilledZipCodes>");
			params.append("<returnCommunicationCode>false</returnCommunicationCode>");
			params.append("</searchByShopCode>");
			params.append("</memberDirectorySearchOptions>");
			params.append("</searchShopRequest>");
			params.append("</memberDirectoryInterface>");
			String webServiceData = "func=getMemberDirectory&data=" + URLEncoder.encode(params.toString(), "UTF-8");
			
			String result = WebServiceUtil.sendGetRequest(webServiceUrl,webServiceData);
			if(result != null) {
				SAXBuilder builder = new SAXBuilder();
				Document document = builder.build(new ByteArrayInputStream(result.getBytes()));
				
				Element rootElement = document.getRootElement();
				Element responseElement = rootElement.getChild("searchShopResponse");
				Element shopsElement = responseElement.getChild("shops");
				if(shopsElement != null){
					Element shopElement = shopsElement.getChild("shop");
					Element zipsElement = shopElement.getChild("servicedZips");
	
				if (responseElement != null && zipsElement != null) {
						List<Element> zips = (List<Element>) zipsElement.getChildren("zip");

						for(int ii=0; ii<zips.size(); ++ii){
							zipsCovered.add(zips.get(ii).getValue());
						}
					}
				}else{
					zipsCovered.add("No Coverage Is Currently Available For This Shop");
				}
			}
		} catch(Exception e) {
			e.printStackTrace();	
			return new ArrayList<String>();
		}finally{
			fos.close();
		}
		return zipsCovered;				
	}

	
	public void getTlo(String result) throws JDOMException, IOException{
		if(result != null) {
			SAXBuilder builder = new SAXBuilder();
			Document document = builder.build(new ByteArrayInputStream(result.getBytes()));
			
			Element rootElement = document.getRootElement();
			if(rootElement != null){
				Element searchResponseElement = rootElement.getChild("searchShopResponse");
				if(searchResponseElement != null){
					Element shopsElement = searchResponseElement.getChild("shops");
					if(shopsElement != null){
						Element shopElement = shopsElement.getChild("shop");
						if(shopElement != null){
							Element communicationTypeElement = shopElement.getChild("communicationCode");
							if(communicationTypeElement != null){
								if(communicationTypeElement.getValue().toString().equals("10")){
									this.tlo = true;
								}
							}
						}
					}
				}
			}
		}
	}
}
