package com.flowers.portal.service.impl;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.BooleanQuery.Builder;
import org.springframework.transaction.annotation.Transactional;

import com.flowers.portal.service.SearchService;
import com.flowers.server.entity.BannerAd;
import com.flowers.server.entity.City;
import com.flowers.server.entity.ConsumerLandingAd;
import com.flowers.server.entity.ConsumerResultsAd;
import com.flowers.server.entity.Dropship;
import com.flowers.server.entity.Facility;
import com.flowers.server.entity.FacilityListing;
import com.flowers.server.entity.FacilityType;
import com.flowers.server.entity.FruitBouquet;
import com.flowers.server.entity.Listing;
import com.flowers.server.entity.ProductCategory;
import com.flowers.server.entity.ProductCodification;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.SideAd;
import com.flowers.server.entity.State;
import com.flowers.server.entity.Zip;
import com.flowers.server.search.SearchQuery;
import com.flowers.server.search.SearchQueryType;
import com.flowers.server.search.SearchResultSet;

public class LuceneSearchServiceImpl extends LuceneSearchServiceSupport implements SearchService {
	
	protected <T,E> SearchResultSet<T> getSearchResultSet(Class<E> clazz, SearchQuery searchQuery){
		if(searchQuery.getReturnType() == SearchQuery.RETURN_TYPE_OBJECT) return objects(clazz, searchQuery);
		else if(searchQuery.getReturnType() == SearchQuery.RETURN_TYPE_MAP) return maps(clazz, searchQuery);
		else return documents(clazz, searchQuery);
	}

	@Override
	public <T> SearchResultSet<T> search(SearchQuery searchQuery) {
		switch (searchQuery.getType()) {
		case STATE:
			return findAllStates(searchQuery);
		case STATEBYCOUNTRY:
			return findStatesByCountry(searchQuery);
		case CITY:
			return findCityFromZip(searchQuery);
		case CITIES:
			return findCitiesFromState(searchQuery);
		case COUNTRIES:
			return findAllCountries(searchQuery);
		case CITIESSTATENAME:
			return findCitiesFromStateName(searchQuery);
		case MAJORCITY:
			return findAllMajorCities(searchQuery);
		case LISTING:
			return findListing(searchQuery);
		case LISTINGS:
			return findListings(searchQuery);
		case SHOP_DETAIL:
			return findShopById(searchQuery);
		case SHOP_PRODUCTS:
			return findShopCodifiedProducts(searchQuery);
		case FACILITY:
			return findFacilities(searchQuery);
		case FACILITY_ID:
			return findFacilityById(searchQuery);
		case FACILITY_TYPE:
			return findFacilityTypes(searchQuery);
		case CODIFIED_PRODUCT:
			return findAllCodifiedProducts(searchQuery);
		case CODIFIED_PRODUCT_DETAIL:
			return findCodifiedProductById(searchQuery);
		case CODIFIED_PRODUCT_DETAIL_BY_CODE:
			return findCodifiedProductByCode(searchQuery);
		case PRODUCT_CATEGORY:
			return findAllProductCategories(searchQuery);
		case PRODUCT_CATEGORY_DETAIL:
			return findProductCategory(searchQuery);
		case DROPSHIP:
			return findAllDropship(searchQuery);
		case FRUITBOUQUETPRODUCTS:
			return findAllFBQ(searchQuery);
		case BANNERADS:
			return findAllBannerAds(searchQuery);
		case SIDEADS:
			return findAllSideAds(searchQuery);
		case CONSUMERLANDINGADS:
			return findAllConsumerLandingAds(searchQuery);
		case CONSUMERRESULTSADS:
			return findConsumerResultsAds(searchQuery);
		default:
			return null;
		}
	}

	public List<Document> fetchFacilities(String[] ids) {		
		Builder query = new BooleanQuery.Builder();
		for(int i=0; i < ids.length; i++) {
			query.add(new TermQuery(new Term("id", ids[i])), BooleanClause.Occur.SHOULD);
		}
		return documents(Facility.class, query.build(), null);
	}
	/** Returns the map used by searchResults.jsp that looks up the codified products for each listing **/
	public Map<String, Document> fetchProducts() {
		Map<String, Document> products = new HashMap<String, Document>();
		SearchQuery searchQuery = new SearchQuery(SearchQueryType.CODIFIED_PRODUCT, SearchQuery.RETURN_TYPE_DOCUMENT);
		SearchResultSet<Document> results = search(searchQuery);
		for(Document doc : results.getResults()) {
			products.put(doc.get("id"), doc);
		}
		return products;
	}
	/** Returns the map used by searchResults.jsp that looks up the products categories for the minimum prices in each listing **/
	public Map<String, Document> fetchCategories() {
		Map<String, Document> categories = new HashMap<String, Document>();
		SearchQuery searchQuery = new SearchQuery(SearchQueryType.PRODUCT_CATEGORY, SearchQuery.RETURN_TYPE_DOCUMENT);
		SearchResultSet<Document> results = search(searchQuery);
		for(Document doc : results.getResults()) {
			categories.put("price"+doc.get("id"), doc);
		}
		return categories;
	}
	/**
	 * Returns a list containing all states.
	 * @return list of all states.
	 */
	protected <T> SearchResultSet<T> findAllStates(SearchQuery searchQuery) {
		MatchAllDocsQuery query = new MatchAllDocsQuery();
		searchQuery.setLuceneQuery(query);
		SortField[] sortFields = new SortField[2];
		sortFields[0] = new SortField("country.idcountry", SortField.Type.INT);
		sortFields[1] = new SortField("name", SortField.Type.STRING);
		Sort sort = new Sort(sortFields);
		searchQuery.setLuceneSort(sort);
		searchQuery.setPage(0);
		return getSearchResultSet(State.class, searchQuery);
	}
	
	/**
	 * Returns a list containing all states by country id.
	 * @return list of all states by country id.
	 */
	protected <T> SearchResultSet<T> findStatesByCountry(SearchQuery searchQuery) {
		Builder query = new BooleanQuery.Builder();
		query.add(new BooleanClause(new TermQuery(new Term("country_id", searchQuery.getCountryId())), BooleanClause.Occur.MUST));
		searchQuery.setLuceneQuery(query.build());
		Sort sort = new Sort(new SortField("name", SortField.Type.STRING));
		searchQuery.setLuceneSort(sort);
		searchQuery.setPage(0);
		return getSearchResultSet(State.class, searchQuery);
	}
	
	protected <T> SearchResultSet<T> findCityFromZip(SearchQuery searchQuery) {
		Builder query = new BooleanQuery.Builder();
		query.add(new BooleanClause(new TermQuery(new Term("zips", searchQuery.getZip())), BooleanClause.Occur.MUST));
		Builder subquery = new BooleanQuery.Builder();
		subquery.add(new BooleanClause(new TermQuery(new Term("city_type", "D")), BooleanClause.Occur.MUST));
		query.add(new BooleanClause(subquery.build(), BooleanClause.Occur.MUST));
		searchQuery.setLuceneQuery(query.build());
		Sort sort = new Sort(new SortField("cityName", SortField.Type.STRING));
		searchQuery.setLuceneSort(sort);
		searchQuery.setPage(0);
		SearchResultSet<T> results = getSearchResultSet(City.class, searchQuery);
		return results;
	}
	/**
	 * Returns a list of cities from a state using the SearchQuery object as criteria for the search.
	 * @param searchQuery @see com.flowers.server.search.SearchQuery
	 * @return list of cities
	 */
	protected <T> SearchResultSet<T> findCitiesFromState(SearchQuery searchQuery){
		Builder query = new BooleanQuery.Builder();
		query.add(new BooleanClause(new TermQuery(new Term("state_id", searchQuery.getState())), BooleanClause.Occur.MUST));
		query.add(new BooleanClause(new TermQuery(new Term("city_type", "D")), BooleanClause.Occur.MUST));
		searchQuery.setLuceneQuery(query.build());
		Sort sort = new Sort(new SortField("cityName", SortField.Type.STRING));
		searchQuery.setLuceneSort(sort);
		searchQuery.setPage(0);
		return getSearchResultSet(City.class, searchQuery);
	}
	
	protected <T> SearchResultSet<T> findCitiesFromStateName(SearchQuery searchQuery){
		Builder query = new BooleanQuery.Builder();
		query.add(new BooleanClause(new TermQuery(new Term("state_name", searchQuery.getState())), BooleanClause.Occur.MUST));
		Builder subquery = new BooleanQuery.Builder();
		subquery.add(new BooleanClause(new TermQuery(new Term("city_type", "D")), BooleanClause.Occur.MUST));
		query.add(new BooleanClause(subquery.build(), BooleanClause.Occur.MUST));
		searchQuery.setLuceneQuery(query.build());
		Sort sort = new Sort(new SortField("cityName", SortField.Type.STRING));
		searchQuery.setLuceneSort(sort);
		searchQuery.setPage(0);
		return getSearchResultSet(City.class, searchQuery);
	}
	
	/**
	 * Returns a list containing all major cities.
	 * @return list of all major cities
	 */
	protected <T> SearchResultSet<T> findAllMajorCities(SearchQuery searchQuery){
		Builder query = new BooleanQuery.Builder();
		query.add(new BooleanClause(new TermQuery(new Term("major_city", "true")), BooleanClause.Occur.MUST));
		query.add(new BooleanClause(new TermQuery(new Term("city_type", "N")), BooleanClause.Occur.MUST_NOT));
		
		searchQuery.setLuceneQuery(query.build());
		Sort sort = new Sort(new SortField("name", SortField.Type.STRING));
		searchQuery.setLuceneSort(sort);
		searchQuery.setPage(0);
		return getSearchResultSet(City.class, searchQuery);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected <T> SearchResultSet<T> findListing(SearchQuery searchQuery) {
		try {
			Document doc = document(Listing.class, new TermQuery(new Term("id", searchQuery.getListingId())));
			SearchResultSet results = new SearchResultSet();
			results.getResults().add(doc);
			return results;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * Returns a list of shops using the SearchQuery object as criteria for the search
	 * @param searchQuery @see com.flowers.server.search.SearchQuery
	 * @return list of shops
	 */
	@Transactional
	protected <T> SearchResultSet<T> findListings(SearchQuery searchQuery) {
		parseToLuceneQueryCity(searchQuery);
		SearchResultSet<T> results; 
		if(searchQuery.getFacility() != null || searchQuery.getPreviouslyFacility() != null) {
			results = listings(FacilityListing.class, searchQuery);
		}else{
			results = listings(Listing.class, searchQuery);
		}
		return results;
	}
	protected void parseToLuceneQueryCity(SearchQuery searchQuery) {
		Builder query = new BooleanQuery.Builder();
		if(searchQuery.getPhone() != null) query.add(new TermQuery(new Term("phone",String.valueOf(searchQuery.getPhone()))), BooleanClause.Occur.MUST);
		if(searchQuery.getShopCode() != null) query.add(new TermQuery(new Term("shopcode",String.valueOf(searchQuery.getShopCode()))), BooleanClause.Occur.MUST);
		if(searchQuery.getShopName() != null) query.add(new TermQuery(new Term("shopname",String.valueOf(searchQuery.getShopName()))), BooleanClause.Occur.MUST);
		/** For each ProductCodifcation we receive, we're going to add a filtering clause 
		 * that checks for the explicit product linkage or an "NA" which represents all products available **/
		if(searchQuery.getProductCodifications().size() > 0) {
			Builder bq1 = new BooleanQuery.Builder();
			for(ProductCodification product : searchQuery.getProductCodifications()) {
				bq1.add(new BooleanClause(new TermQuery(new Term("products",String.valueOf(product.getId()))), BooleanClause.Occur.MUST));
			}
			Builder bq2 = new BooleanQuery.Builder();
			bq2.add(new BooleanClause(bq1.build(), BooleanClause.Occur.SHOULD));
			bq2.add(new BooleanClause(new TermQuery(new Term("products","NA")), BooleanClause.Occur.SHOULD));
			query.add(new BooleanClause(bq2.build(), BooleanClause.Occur.MUST));
		}
		if(searchQuery.getFacility() != null) {
			query.add(new BooleanClause(new TermQuery(new Term("facilities",searchQuery.getFacility())), BooleanClause.Occur.MUST));
			if(searchQuery.getZip() != null) {
				query.add(new BooleanClause(new TermQuery(new Term("zips", String.valueOf(searchQuery.getZip()))), BooleanClause.Occur.SHOULD));
				query.add(new BooleanClause(new TermQuery(new Term("zip", String.valueOf(searchQuery.getZip()))), BooleanClause.Occur.SHOULD));
				searchQuery.setDestinationZip(searchQuery.getZip());
			}else{
				query.add(new BooleanClause(new TermQuery(new Term("city", searchQuery.getCity())), BooleanClause.Occur.MUST));
			}
		} else if(searchQuery.getCity() != null) {
			query.add(new BooleanClause(new TermQuery(new Term("city", searchQuery.getCity())), BooleanClause.Occur.MUST));
			query.add(new BooleanClause(new TermQuery(new Term("state", String.valueOf(searchQuery.getState()))), BooleanClause.Occur.MUST));
			Builder q = new BooleanQuery.Builder();
			q.add(new BooleanClause(new TermQuery(new Term("name", searchQuery.getCity())), BooleanClause.Occur.MUST));
			q.add(new BooleanClause(new TermQuery(new Term("state.name", searchQuery.getState())), BooleanClause.Occur.MUST));
			Document cityDoc = document(City.class, q.build());
			if(cityDoc != null && cityDoc.get("zips") != null) {
				//BooleanQuery bq = new BooleanQuery();
				String[] zips = cityDoc.get("zips").split(" ");
				/*
				if(zips.length == 1) {
					query.add(new BooleanClause(new TermQuery(new Term("zips", zips[0])), BooleanClause.Occur.MUST));
				} else {
					for(int i=0; i < zips.length; i++) {
						bq.add(new BooleanClause(new TermQuery(new Term("zips", zips[i])), BooleanClause.Occur.SHOULD));
					}
					query.add(new BooleanClause(bq, BooleanClause.Occur.MUST));
				}
				*/
				if(searchQuery.getZip() != null && !searchQuery.getZip().equals("")) 
					searchQuery.setDestinationZip(searchQuery.getZip());
				else if(zips.length > 0) searchQuery.setDestinationZip(zips[0]);
			}
		} else if(searchQuery.getState() != null) {
			query.add(new TermQuery(new Term("state",String.valueOf(searchQuery.getState()))), BooleanClause.Occur.MUST);
		}
		List<SortField> comparators = new ArrayList<SortField>();
		if(searchQuery.getDestinationZip() != null) {
			try {
				Document zipDoc = document(Zip.class, new TermQuery(new Term("value", searchQuery.getDestinationZip())));
				if(zipDoc != null && zipDoc.get("latitude") != null && zipDoc.get("longitude") != null) {
					searchQuery.setDestinationLongitude(Double.valueOf(zipDoc.get("longitude")));
					searchQuery.setDestinationLatitude(Double.valueOf(zipDoc.get("latitude")));
					//DistanceComparatorSource comparator = new DistanceComparatorSource(Double.parseDouble(zipDoc.get("latitude")), Double.parseDouble(zipDoc.get("longitude")));
					//SortField distanceSortField = new SortField("latitude", comparator);
					//comparators.add(distanceSortField);
				}
			} catch(Exception e) {}
		}
		SortField adSizeSortField = new SortField("ad_size", SortField.Type.STRING);
		comparators.add(adSizeSortField);
		searchQuery.setLuceneSort(new Sort(comparators.toArray(new SortField[comparators.size()])));				
		searchQuery.setLuceneQuery(query.build());
	}
	protected void parseToLuceneQueryZip(SearchQuery searchQuery) {
		Builder query = new BooleanQuery.Builder();
		if(searchQuery.getPhone() != null) query.add(new TermQuery(new Term("phone",String.valueOf(searchQuery.getPhone()))), BooleanClause.Occur.MUST);
		if(searchQuery.getShopCode() != null) query.add(new TermQuery(new Term("shopcode",String.valueOf(searchQuery.getShopCode()))), BooleanClause.Occur.MUST);
		if(searchQuery.getShopName() != null) query.add(new TermQuery(new Term("shopname",String.valueOf(searchQuery.getShopName()))), BooleanClause.Occur.MUST);
		/** For each ProductCodifcation we receive, we're going to add a filtering clause 
		 * that checks for the explicit product linkage or an "NA" which represents all products available **/
		if(searchQuery.getProductCodifications().size() > 0) {
			Builder bq1 = new BooleanQuery.Builder();
			for(ProductCodification product : searchQuery.getProductCodifications()) {
				bq1.add(new BooleanClause(new TermQuery(new Term("products",String.valueOf(product.getId()))), BooleanClause.Occur.MUST));
			}
			Builder bq2 = new BooleanQuery.Builder();
			bq2.add(new BooleanClause(bq1.build(), BooleanClause.Occur.SHOULD));
			bq2.add(new BooleanClause(new TermQuery(new Term("products","NA")), BooleanClause.Occur.SHOULD));
			query.add(new BooleanClause(bq2.build(), BooleanClause.Occur.MUST));
		}
		if(searchQuery.getFacility() != null) {
			query.add(new BooleanClause(new TermQuery(new Term("facilities",searchQuery.getFacility())), BooleanClause.Occur.MUST));
			if(searchQuery.getZip() != null) {
				query.add(new BooleanClause(new TermQuery(new Term("zips", String.valueOf(searchQuery.getZip()))), BooleanClause.Occur.SHOULD));
				query.add(new BooleanClause(new TermQuery(new Term("zip", String.valueOf(searchQuery.getZip()))), BooleanClause.Occur.SHOULD));
				searchQuery.setDestinationZip(searchQuery.getZip());
			}
		} else if(searchQuery.getZip() != null) {
			/**We use the zips associated with the city of the listing as well as the shop since they may not overlap **/
			Builder bq = new BooleanQuery.Builder();
			bq.add(new TermQuery(new Term("zips", String.valueOf(searchQuery.getZip()))), BooleanClause.Occur.SHOULD);
			bq.add(new TermQuery(new Term("zip", String.valueOf(searchQuery.getZip()))), BooleanClause.Occur.SHOULD);
			query.add(new BooleanClause(bq.build(), BooleanClause.Occur.MUST));
			searchQuery.setDestinationZip(searchQuery.getZip());
		} else if(searchQuery.getState() != null) {
			query.add(new TermQuery(new Term("state",String.valueOf(searchQuery.getState()))), BooleanClause.Occur.MUST);
		}
		List<SortField> comparators = new ArrayList<SortField>();
		if(searchQuery.getDestinationZip() != null) {
			try {
				Document zipDoc = document(Zip.class, new TermQuery(new Term("value", searchQuery.getDestinationZip())));
				if(zipDoc != null && zipDoc.get("latitude") != null && zipDoc.get("longitude") != null) {
					searchQuery.setDestinationLongitude(Double.valueOf(zipDoc.get("longitude")));
					searchQuery.setDestinationLatitude(Double.valueOf(zipDoc.get("latitude")));
					//DistanceComparatorSource comparator = new DistanceComparatorSource(Double.parseDouble(zipDoc.get("latitude")), Double.parseDouble(zipDoc.get("longitude")));
					//SortField distanceSortField = new SortField("latitude", comparator);
					//comparators.add(distanceSortField);
				}
			} catch(Exception e) {}
		}
		SortField adSizeSortField = new SortField("ad_size", SortField.Type.STRING);
		comparators.add(adSizeSortField);
		searchQuery.setLuceneSort(new Sort(comparators.toArray(new SortField[comparators.size()])));				
		searchQuery.setLuceneQuery(query.build());
	}
	/**
	 * Returns a shop using the id as criteria for the search
	 * @param searchQuery @see com.flowers.server.search.SearchQuery
	 * @return a shop
	 */
	protected <T> SearchResultSet<T> findShopById(SearchQuery searchQuery){
		TermQuery query = new TermQuery(new Term("shop_code", searchQuery.getShopCode()));
		searchQuery.setLuceneQuery(query);
		searchQuery.setPage(0);
		return getSearchResultSet(Shop.class, searchQuery);
	}
	
	/**
	 * Returns a list of codified products from a specified shop using the SearchQuery object as criteria for the search.
	 * @param searchQuery @see com.flowers.server.search.SearchQuery
	 * @return list of product categories
	 */
	protected <T> SearchResultSet<T> findShopCodifiedProducts(SearchQuery searchQuery){
		TermQuery query = new TermQuery(new Term("shopcodes", searchQuery.getShopCode().toLowerCase()));
		searchQuery.setLuceneQuery(query);
		Sort sort = new Sort(new SortField("name", SortField.Type.STRING));
		searchQuery.setLuceneSort(sort);
		searchQuery.setPage(0);
		return getSearchResultSet(ProductCodification.class, searchQuery);
		
	}
	/**
	 * Returns one product category using the SearchQuery object as criteria for the search.
	 * @param searchQuery @see com.flowers.server.search.SearchQuery
	 * @return list of product categories
	 */
	protected <T> SearchResultSet<T> findProductCategory(SearchQuery searchQuery){
		TermQuery query = new TermQuery(new Term("id", String.valueOf(searchQuery.getProductCategoryId())));
		searchQuery.setLuceneQuery(query);
		Sort sort = new Sort(new SortField("name", SortField.Type.STRING));
		searchQuery.setLuceneSort(sort);
		searchQuery.setPage(0);
		return getSearchResultSet(ProductCategory.class, searchQuery);
	}
	
	/**
	 * Returns a list of all product categories using the SearchQuery object as criteria for the search.
	 * @param searchQuery @see com.flowers.server.search.SearchQuery
	 * @return list of product categories
	 */
	protected <T> SearchResultSet<T> findAllProductCategories(SearchQuery searchQuery){
		MatchAllDocsQuery query = new MatchAllDocsQuery();
		searchQuery.setLuceneQuery(query);
		Sort sort = new Sort(new SortField("name", SortField.Type.STRING));
		searchQuery.setLuceneSort(sort);
		searchQuery.setPage(0);
		return getSearchResultSet(ProductCategory.class, searchQuery);
	}
	
	/**
	 * Returns a list of facilities using the SearchQuery object as criteria for the search.
	 * @param searchQuery @see com.flowers.server.search.SearchQuery
	 * @return list of facilities
	 */
	@SuppressWarnings("unchecked")
	protected <T> SearchResultSet<T> findFacilities(SearchQuery searchQuery){
		SearchResultSet<T> results = null;
		Builder query = new BooleanQuery.Builder();
		if(searchQuery.getFacilityTypeId() != null) query.add(new TermQuery(new Term("typeid", searchQuery.getFacilityTypeId())), BooleanClause.Occur.MUST);
		//if(searchQuery.getShopCode() != null) query.add(new TermQuery(new Term("shopcodes", searchQuery.getShopCode().toLowerCase())), BooleanClause.Occur.MUST);
		if(searchQuery.getCity() != null) query.add(new TermQuery(new Term("city_id", searchQuery.getCity())), BooleanClause.Occur.MUST);
		if(searchQuery.getState() != null) query.add(new TermQuery(new Term("state_id", searchQuery.getState())), BooleanClause.Occur.MUST);
		//query.add(new TermQuery(new Term("hasshopcodes", "true")), BooleanClause.Occur.MUST);
		searchQuery.setLuceneQuery(query.build());
		Sort sort = new Sort(new SortField("name", SortField.Type.STRING));
		searchQuery.setLuceneSort(sort);
		searchQuery.setPage(0);
		results = getSearchResultSet(Facility.class, searchQuery);
		
		if(results.getResultSize() > 0) {
			for(T result : results.getResults()) {
				Map<String, String> facility = (Map<String, String>)result;
				String zip = (String)facility.get("zip");
				if(zip != null && zip.length() < 5) facility.put("zip", "0"+zip);
			}
		}else{	
			query = new BooleanQuery.Builder();
			if(searchQuery.getFacilityTypeId() != null) query.add(new TermQuery(new Term("typeid", searchQuery.getFacilityTypeId())), BooleanClause.Occur.MUST);
			if(searchQuery.getCity() != null) query.add(new TermQuery(new Term("city", (searchQuery.getCity()))), BooleanClause.Occur.MUST);
			if(searchQuery.getState() != null) query.add(new TermQuery(new Term("state", searchQuery.getState())), BooleanClause.Occur.MUST);
			searchQuery.setLuceneQuery(query.build());
			searchQuery.setLuceneSort(sort);
			searchQuery.setPage(0);
			results = getSearchResultSet(Facility.class, searchQuery);
		
		}
		
		return results;
		/*
		}else if(searchQuery.getCity() != null) { //in case no facilities found, ignore city and search facilities from the whole state
			query = new BooleanQuery();
			if(searchQuery.getFacilityTypeId() != null) query.add(new TermQuery(new Term("typeid", searchQuery.getFacilityTypeId())), BooleanClause.Occur.MUST);
			if(searchQuery.getShopCode() != null) query.add(new TermQuery(new Term("shopcodes", searchQuery.getShopCode().toLowerCase())), BooleanClause.Occur.MUST);
			if(searchQuery.getState() != null) query.add(new TermQuery(new Term("state_id", searchQuery.getState())), BooleanClause.Occur.MUST);
			searchQuery.setLuceneQuery(query.build());
			results = getSearchResultSet(Facility.class, searchQuery);
		}
		return results;
		*/
	}
	
	/**
	 * Returns a list of facilities using the SearchQuery object as criteria for the search.
	 * @param searchQuery @see com.flowers.server.search.SearchQuery
	 * @return list of facilities
	 */
	protected <T> SearchResultSet<T> findFacilityById(SearchQuery searchQuery){
		SearchResultSet<T> results = null;
		Builder query = new BooleanQuery.Builder();
		if(searchQuery.getFacility() != null) query.add(new TermQuery(new Term("id", searchQuery.getFacility())), BooleanClause.Occur.MUST);
		searchQuery.setLuceneQuery(query.build());
		Sort sort = new Sort(new SortField("name", SortField.Type.STRING));
		searchQuery.setLuceneSort(sort);
		searchQuery.setPage(0);
		results = getSearchResultSet(Facility.class, searchQuery);
		
		return results;
	}
	
	/**
	 * Returns a list of facility types using the SearchQuery object as criteria for the search.
	 * @param searchQuery @see com.flowers.server.search.SearchQuery
	 * @return list of facility types
	 */
	@SuppressWarnings({ "unchecked" })
	protected <T> SearchResultSet<T> findFacilityTypes(SearchQuery searchQuery){
		Builder q = new BooleanQuery.Builder();
		if(searchQuery.getCity() != null) 
			q.add(new TermQuery(new Term("city_id", searchQuery.getCity())), BooleanClause.Occur.MUST);
		if(searchQuery.getState() != null) 
			q.add(new TermQuery(new Term("state_id", searchQuery.getState())), BooleanClause.Occur.MUST);
		Sort s = new Sort(new SortField("type", SortField.Type.STRING));
		searchQuery.setLuceneQuery(q.build());
		searchQuery.setLuceneSort(s);
		SearchResultSet<T> searchResultSet = documents(Facility.class, searchQuery);
		/*
		if (searchResultSet.getResultSize() == 0){ //if not found any facility type for a specified city, look for all facility types on the state
			BooleanQuery q2 = new BooleanQuery();
			q2.add(new TermQuery(new Term("state_id", searchQuery.getState())), BooleanClause.Occur.MUST);
			searchQuery.setLuceneQuery(q2);
			searchResultSet = documents(Facility.class, searchQuery);
		}
		*/
		Collection<T> list = searchResultSet.getResults();
		Set<T> facilityTypeList = new LinkedHashSet<T>();
		for (T o:list){
			Document doc = (Document)o;
			FacilityType facilityType = new FacilityType();
			facilityType.setId(Long.parseLong(doc.get("typeid")));
			facilityType.setName(doc.get("type"));
			if(facilityType.getId() == 9 || facilityType.getId() == 8 || facilityType.getId() == 4)
				facilityTypeList.add((T)facilityType);
		}
		searchResultSet.setResults(new ArrayList<T>(facilityTypeList));
		searchResultSet.setResultSize(facilityTypeList.size());
		return searchResultSet;
	}
	
	/**
	 * Returns a list of codified products from a specified shop using the SearchQuery object as criteria for the search. 
	 * @param searchQuery @see com.flowers.server.search.SearchQuery
	 * @return list of codified products
	 */
	protected <T> SearchResultSet<T> findAllCodifiedProducts(SearchQuery searchQuery){
		MatchAllDocsQuery query = new MatchAllDocsQuery();
		searchQuery.setLuceneQuery(query);
		Sort sort = new Sort(new SortField("name", SortField.Type.STRING));
		searchQuery.setLuceneSort(sort);
		searchQuery.setPage(0);
		return getSearchResultSet(ProductCodification.class, searchQuery);
	}
	
	protected <T> SearchResultSet<T> findAllCountries(SearchQuery searchQuery){
		MatchAllDocsQuery query = new MatchAllDocsQuery();
		searchQuery.setLuceneQuery(query);
		Sort sort = new Sort(new SortField("country.name", SortField.Type.STRING));
		searchQuery.setLuceneSort(sort);
		searchQuery.setPage(0);
		return getSearchResultSet(State.class, searchQuery);
	}
	
	/**
	 * Returns a codified product using the id as criteria for the search. 
	 * @param searchQuery @see com.flowers.server.search.SearchQuery
	 * @return a codified product
	 */
	protected <T> SearchResultSet<T> findCodifiedProductById(SearchQuery searchQuery){
		TermQuery query = new TermQuery(new Term("id", String.valueOf(searchQuery.getCodifiedProductId())));
		searchQuery.setLuceneQuery(query);
		Sort sort = new Sort(new SortField("name", SortField.Type.STRING));
		searchQuery.setLuceneSort(sort);
		searchQuery.setPage(0);
		return getSearchResultSet(ProductCodification.class, searchQuery);
	}	
	
	/**
	 * Returns a codified product using the code as criteria for the search. 
	 * @param searchQuery @see com.flowers.server.search.SearchQuery
	 * @return a codified product
	 */
	protected <T> SearchResultSet<T> findCodifiedProductByCode(SearchQuery searchQuery){
		TermQuery query = new TermQuery(new Term("code", String.valueOf(searchQuery.getProductCode())));
		searchQuery.setLuceneQuery(query);
		Sort sort = new Sort(new SortField("name", SortField.Type.STRING));
		searchQuery.setLuceneSort(sort);
		searchQuery.setPage(0);
		return getSearchResultSet(ProductCodification.class, searchQuery);
	}
	
	/**
	 * Returns a list containing all Dropship items.
	 * @return list of all Dropship items.
	 */
	protected <T> SearchResultSet<T> findAllDropship(SearchQuery searchQuery) {
		MatchAllDocsQuery query = new MatchAllDocsQuery();
		searchQuery.setLuceneQuery(query);
		SortField[] sortFields = new SortField[1];
		sortFields[0] = new SortField("product_code", SortField.Type.STRING);
		Sort sort = new Sort(sortFields);
		searchQuery.setLuceneSort(sort);
		searchQuery.setPage(0);
		return getSearchResultSet(Dropship.class, searchQuery);
	}
	
	protected <T> SearchResultSet<T> findAllBannerAds(SearchQuery searchQuery) {
		MatchAllDocsQuery query = new MatchAllDocsQuery();
		searchQuery.setLuceneQuery(query);
		SortField[] sortFields = new SortField[1];
		sortFields[0] = new SortField("shop_code", SortField.Type.STRING);
		Sort sort = new Sort(sortFields);
		searchQuery.setLuceneSort(sort);
		searchQuery.setPage(0);
		return getSearchResultSet(BannerAd.class, searchQuery);
	}
	
	protected <T> SearchResultSet<T> findAllSideAds(SearchQuery searchQuery) {
		MatchAllDocsQuery query = new MatchAllDocsQuery();
		searchQuery.setLuceneQuery(query);
		SortField[] sortFields = new SortField[1];
		sortFields[0] = new SortField("shop_code", SortField.Type.STRING);
		Sort sort = new Sort(sortFields);
		searchQuery.setLuceneSort(sort);
		searchQuery.setPage(0);
		return getSearchResultSet(SideAd.class, searchQuery);
	}
	
	protected <T> SearchResultSet<T> findAllConsumerLandingAds(SearchQuery searchQuery) {
		MatchAllDocsQuery query = new MatchAllDocsQuery();
		searchQuery.setLuceneQuery(query);
		SortField[] sortFields = new SortField[1];
		sortFields[0] = new SortField("file_name", SortField.Type.STRING);
		Sort sort = new Sort(sortFields);
		searchQuery.setLuceneSort(sort);
		searchQuery.setPage(0);
		return getSearchResultSet(ConsumerLandingAd.class, searchQuery);
	}
	
	protected <T> SearchResultSet<T> findConsumerResultsAds(SearchQuery searchQuery) {
		Builder query = new BooleanQuery.Builder();
		TermQuery q1 = new TermQuery(new Term("city", searchQuery.getCity().toUpperCase()));
		TermQuery q2 = new TermQuery(new Term("state", searchQuery.getState().toUpperCase()));
		query.add(new BooleanClause(q1, BooleanClause.Occur.MUST));
		query.add(new BooleanClause(q2, BooleanClause.Occur.MUST));
		searchQuery.setLuceneQuery(query.build());
		return getSearchResultSet(ConsumerResultsAd.class, searchQuery);
	}
	
	protected <T> SearchResultSet<T> findAllFBQ(SearchQuery searchQuery) {
		MatchAllDocsQuery query = new MatchAllDocsQuery();
		searchQuery.setLuceneQuery(query);
		SortField[] sortFields = new SortField[1];
		sortFields[0] = new SortField("product_code", SortField.Type.STRING);
		Sort sort = new Sort(sortFields);
		searchQuery.setLuceneSort(sort);
		searchQuery.setPage(0);
		return getSearchResultSet(FruitBouquet.class, searchQuery);
	}
	
}