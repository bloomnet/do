package com.flowers.portal.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.jdom.JDOMException;

public interface ShopAvailabilityService {
	
	LinkedList<String> searchShopCodesByAvailabilityDate(String availabilityDate, LinkedList<String> availabilityShopCode) throws IOException, JDOMException;
	
	ArrayList<String> searchByShopCode(String shopCode)throws IOException, JDOMException;

	ArrayList<String> getShopOfferings(String shopCode) throws IOException, JDOMException;

	ArrayList<String> searchZipCoverageByShopCode(String shopCode) throws IOException, JDOMException;

	String searchShopHoursByCityStateZip(String availabilityDate, String city, String state) throws IOException, JDOMException;

	String searchHoursByShopCode(String shopCode) throws IOException, JDOMException;

	String searchShopCodesByAvailabilityDateCDO(String availabilityDate, List<String> availabilityShopCodes) throws IOException, JDOMException;
}
