/*
 * Copyright (C) 2009 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.flowers.portal.service;



public interface ServiceSupport {
	public static final int BATCH_SIZE = 100;
	/*
	void refresh(Object o);
	void persist(Object o) throws PersistenceException;
	void merge(Object o) throws PersistenceException;
	void remove(Object o) throws PersistenceException;
	@SuppressWarnings("rawtypes")
	Object find(Class clazz, Object id) throws LookupException;
	@SuppressWarnings("rawtypes")
	List findAll(String name) throws LookupException;
	Object findSingleResult(String query);
	List<Object> findResults(String query, int limit);
	List<Object> findResults(String query, Map<String, Object> params);
	@SuppressWarnings("rawtypes")
	List findPaged(Class clazz, int page);
	*/
}
