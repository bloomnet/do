package com.flowers.portal.service;
import java.util.List;

import org.apache.lucene.document.Document;

import com.flowers.portal.vo.Order;
import com.flowers.portal.vo.RestResponse;
import com.flowers.server.entity.User;



public interface PortalService {

	String getStatesOptions(String choice, String countryId);
	String getCountryOptions(String choice);
	List<Document> getProducts();
	List<Document> getDropship();
	List<Document> getFruitBouquetProducts();
	List<Document> fetchCategories();
	List<String> getBannerAds();
	List<String> getSideAds();
	
	Document getFacilityDocument(String id);
	Document getShopDocument(String id);
	Document getListingDocument(String id);
	Document getFacilityListingDocument(String id);
	Document getStateDocument(String id);
	
	RestResponse order(User user, Order order);

}
