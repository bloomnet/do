package com.flowers.portal.service;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.flowers.portal.vo.Order;
import com.flowers.server.entity.User;
import com.flowers.server.entity.Userrole;

public interface ControllerService {
	
	void populateSummaryParameters(HttpServletRequest request, Order order);
	
	Order processOrderParameters(HttpServletRequest httprequest, boolean dropship);
	
	Order processSearchParameters(HttpServletRequest request);
	
	User getUserByUserName(String userName);

	List<Userrole> getUserrolesByUserId(String userId);

	String SHA1(String text) throws NoSuchAlgorithmException,
			UnsupportedEncodingException;

	void SaveUser(User user);

}
