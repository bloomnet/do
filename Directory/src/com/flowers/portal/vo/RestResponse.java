package com.flowers.portal.vo;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.flowers.portal.util.DateUtil;

public class RestResponse implements Serializable {
	private static final long serialVersionUID = 8583269451262434290L;
	private int status;
	
	private int messageCount;
	private int messageType;
	private String sendingShopCode;
	private String receivingShopCode;
	private String fulfillingShopCode;
	private String systemType;
	
	//identifiers - generalIdentifiers
	private long bmtOrderNumber;
	private long bmtSeqNumberOfOrder;
	private long bmtSetNumberOfMessage;
	private long externalShopOrderNumber;
	private long externalShopMessageNumber;
	
	private Date messageCreateTimestamp;
	
	private List<Error> errors = new ArrayList<Error>();
	private List<Error> globalErrors;
	
	public RestResponse() {}
	
	public RestResponse(Error error) {
		errors.add(error);
	}	
	public RestResponse(String xmlData) throws IOException, JDOMException {
		SAXBuilder builder = new SAXBuilder();
		Document document = builder.build(new ByteArrayInputStream(xmlData.getBytes()));
		
		Element rootElement = document.getRootElement();
		
		Element messagesOnOrderElement = rootElement.getChild("messagesOnOrder");
		if (messagesOnOrderElement != null) {
			Element messageCountElement = messagesOnOrderElement.getChild("messageCount");
			if (messageCountElement != null) {
				setMessageCount(Integer.parseInt(messageCountElement.getText()));
			}
		
			Element messageAckbElement = messagesOnOrderElement.getChild("messageAckb");
			if (messageAckbElement != null) {
				Element messageTypeElement = messageAckbElement.getChild("messageType");
				if (messageTypeElement != null && !messageTypeElement.getText().equals("")) {
					setMessageType(Integer.parseInt(messageTypeElement.getText()));
				}
				Element sendingShopCodeElement = messageAckbElement.getChild("sendingShopCode");
				if (sendingShopCodeElement != null && !sendingShopCodeElement.getText().equals("")) {
					setSendingShopCode(sendingShopCodeElement.getText());
				}
				Element receivingShopCodeElement = messageAckbElement.getChild("receivingShopCode");
				if (receivingShopCodeElement != null && !receivingShopCodeElement.getText().equals("")) {
					setReceivingShopCode(receivingShopCodeElement.getText());
				}
				Element fulfillingShopCodeElement = messageAckbElement.getChild("fulfillingShopCode");
				if (fulfillingShopCodeElement != null && !fulfillingShopCodeElement.getText().equals("")) {
					setFulfillingShopCode(fulfillingShopCodeElement.getText());
				}
				Element systemTypeElement = messageAckbElement.getChild("systemType");
				if (systemTypeElement != null && !systemTypeElement.getText().equals("")) {
					setSystemType(systemTypeElement.getText());
				}
				
				Element identifiersElement = messageAckbElement.getChild("identifiers");
				if (identifiersElement != null) {
					Element generalIdentifiersElement = identifiersElement.getChild("generalIdentifiers");
					if (generalIdentifiersElement != null) {
						Element bmtOrderNumberElement = generalIdentifiersElement.getChild("bmtOrderNumber");
						if (bmtOrderNumberElement != null && !bmtOrderNumberElement.getText().equals("")) {
							setBmtOrderNumber(Long.parseLong(bmtOrderNumberElement.getText()));
						}
						Element bmtSeqNumberOfOrderElement = generalIdentifiersElement.getChild("bmtSeqNumberOfOrder");
						if (bmtSeqNumberOfOrderElement != null && !bmtSeqNumberOfOrderElement.getText().equals("")) {
							setBmtSeqNumberOfOrder(Long.parseLong(bmtSeqNumberOfOrderElement.getText()));
						}
						Element bmtSeqNumberOfMessageElement = generalIdentifiersElement.getChild("bmtSeqNumberOfMessage");
						if (bmtSeqNumberOfMessageElement != null && !bmtSeqNumberOfMessageElement.getText().equals("")) {
							setBmtSetNumberOfMessage(Long.parseLong(bmtSeqNumberOfMessageElement.getText()));
						}
						Element externalShopOrderNumberElement = generalIdentifiersElement.getChild("externalShopOrderNumber");
						if (externalShopOrderNumberElement != null && !externalShopOrderNumberElement.getText().equals("")) {
							setExternalShopOrderNumber(Long.parseLong(externalShopOrderNumberElement.getText()));
						}
						Element externalShopMessageNumberElement = generalIdentifiersElement.getChild("externalShopMessageNumber");
						if (externalShopMessageNumberElement != null && !externalShopMessageNumberElement.getText().equals("")) {
							setExternalShopMessageNumber(Long.parseLong(externalShopMessageNumberElement.getText()));
						}
					}
				}
				
				Element messageCreateTimestampElement = messageAckbElement.getChild("messageCreateTimestamp");
				if (messageCreateTimestampElement != null && !messageCreateTimestampElement.getText().equals("")) {
					setMessageCreateTimestamp(DateUtil.toDate(messageCreateTimestampElement.getText()));
				}
				
				Element errorsElement = messageAckbElement.getChild("errors");
				@SuppressWarnings("rawtypes")
				List errorList = errorsElement.getChildren("error");
				if (errorList != null) {
					List<Error> errors = new ArrayList<Error>();
					for (int i=0; i<errorList.size(); i++) {
						Element errorElement = (Element)errorList.get(i);
						if (errorElement != null) {
							Error error = new Error();
							Element errorCodeElement = errorElement.getChild("errorCode");
							if (errorCodeElement != null && !errorCodeElement.getText().equals("")) {
								error.setErrorCode(Long.parseLong(errorCodeElement.getText()));
							}
							Element detailedErrorCodeElement = errorElement.getChild("detailedErrorCode");
							if (detailedErrorCodeElement != null && !detailedErrorCodeElement.getText().equals("")) {
								error.setDetailedErrorCode(Long.parseLong(detailedErrorCodeElement.getText()));
							}
							Element errorMessageElement = errorElement.getChild("errorMessage");
							if (errorMessageElement != null && !errorMessageElement.getText().equals("")) {
								error.setErrorMessage(errorMessageElement.getText());
							}
							Element errorSourceElement = errorElement.getChild("errorSource");
							if (errorSourceElement != null && !errorSourceElement.getText().equals("")) {
								error.setErrorSource(errorSourceElement.getText());
							}
							errors.add(error);
						}
					}
					setErrors(errors);
				}
			}
		}
		
		Element globalErrorsElement = rootElement.getChild("errors");
		if (globalErrorsElement != null) {
			@SuppressWarnings("rawtypes")
			List globalErrorList = globalErrorsElement.getChildren("error");
			if (globalErrorList != null) {
				List<Error> errors = new ArrayList<Error>();
				for (int i=0; i<globalErrorList.size(); i++) {
					Element errorElement = (Element)globalErrorList.get(i);
					if (errorElement != null) {
						Error error = new Error();
						Element errorCodeElement = errorElement.getChild("errorCode");
						if (errorCodeElement != null && !errorCodeElement.getText().equals("")) {
							error.setErrorCode(Long.parseLong(errorCodeElement.getText()));
						}
						Element detailedErrorCodeElement = errorElement.getChild("detailedErrorCode");
						if (detailedErrorCodeElement != null && !detailedErrorCodeElement.getText().equals("")) {
							error.setDetailedErrorCode(Long.parseLong(detailedErrorCodeElement.getText()));
						}
						Element errorMessageElement = errorElement.getChild("errorMessage");
						if (errorMessageElement != null && !errorMessageElement.getText().equals("")) {
							error.setErrorMessage(errorMessageElement.getText());
						}
						Element errorSourceElement = errorElement.getChild("errorSource");
						if (errorSourceElement != null && !errorSourceElement.getText().equals("")) {
							error.setErrorSource(errorSourceElement.getText());
						}
						errors.add(error);
					}
				}
				setGlobalErrors(errors);
			}
		}
		if(getErrors().size() == 0 && getGlobalErrors().size() == 0 && getBmtOrderNumber() > 0) setStatus(1);
		else setStatus(0);
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public int getMessageType() {
		return messageType;
	}
	
	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}
	
	public String getSendingShopCode() {
		return sendingShopCode;
	}
	
	public void setSendingShopCode(String sendingShopCode) {
		this.sendingShopCode = sendingShopCode;
	}
	
	public String getReceivingShopCode() {
		return receivingShopCode;
	}
	
	public void setReceivingShopCode(String receivingShopCode) {
		this.receivingShopCode = receivingShopCode;
	}
	
	public String getFulfillingShopCode() {
		return fulfillingShopCode;
	}
	
	public void setFulfillingShopCode(String fulfillingShopCode) {
		this.fulfillingShopCode = fulfillingShopCode;
	}
	
	public String getSystemType() {
		return systemType;
	}
	
	public void setSystemType(String systemType) {
		this.systemType = systemType;
	}
	
	public long getBmtOrderNumber() {
		return bmtOrderNumber;
	}
	
	public void setBmtOrderNumber(long bmtOrderNumber) {
		this.bmtOrderNumber = bmtOrderNumber;
	}
	
	public long getBmtSeqNumberOfOrder() {
		return bmtSeqNumberOfOrder;
	}
	
	public void setBmtSeqNumberOfOrder(long bmtSeqNumberOfOrder) {
		this.bmtSeqNumberOfOrder = bmtSeqNumberOfOrder;
	}
	
	public long getBmtSetNumberOfMessage() {
		return bmtSetNumberOfMessage;
	}
	
	public void setBmtSetNumberOfMessage(long bmtSetNumberOfMessage) {
		this.bmtSetNumberOfMessage = bmtSetNumberOfMessage;
	}
	
	public long getExternalShopOrderNumber() {
		return externalShopOrderNumber;
	}
	
	public void setExternalShopOrderNumber(long externalShopOrderNumber) {
		this.externalShopOrderNumber = externalShopOrderNumber;
	}
	
	public long getExternalShopMessageNumber() {
		return externalShopMessageNumber;
	}
	
	public void setExternalShopMessageNumber(long externalShopMessageNumber) {
		this.externalShopMessageNumber = externalShopMessageNumber;
	}
	
	public Date getMessageCreateTimestamp() {
		return messageCreateTimestamp;
	}
	
	public void setMessageCreateTimestamp(Date messageCreateTimestamp) {
		this.messageCreateTimestamp = messageCreateTimestamp;
	}

	public void setMessageCount(int messageCount) {
		this.messageCount = messageCount;
	}

	public int getMessageCount() {
		return messageCount;
	}

	public void setErrors(List<Error> errors) {
		this.errors = errors;
	}

	public List<Error> getErrors() {
		return errors;
	}

	public void setGlobalErrors(List<Error> globalErrors) {
		this.globalErrors = globalErrors;
	}

	public List<Error> getGlobalErrors() {
		return globalErrors;
	}

	@Override
	public String toString() {
		return "RestResponse [status=" + status + ", messageCount=" + messageCount + ", messageType=" + messageType + ", sendingShopCode=" + sendingShopCode + ", receivingShopCode=" + receivingShopCode + ", fulfillingShopCode=" + fulfillingShopCode + ", systemType=" + systemType
				+ ", bmtOrderNumber=" + bmtOrderNumber + ", bmtSeqNumberOfOrder=" + bmtSeqNumberOfOrder + ", bmtSetNumberOfMessage=" + bmtSetNumberOfMessage + ", externalShopOrderNumber=" + externalShopOrderNumber + ", externalShopMessageNumber=" + externalShopMessageNumber
				+ ", messageCreateTimestamp=" + messageCreateTimestamp + ", errors=" + errors + ", globalErrors=" + globalErrors + "]";
	}
}
