package com.flowers.portal.vo;

import java.io.Serializable;

public interface FsiSecurityElementInterface extends Serializable{

	/**
	 * @return the username
	 */
	public String getUsername();

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username);

	/**
	 * @return the shopcode
	 */
	public String getShopcode();

	/**
	 * @param shopcode the shopcode to set
	 */
	public void setShopcode(String shopcode);

	/**
	 * @return the password
	 */
	public String getPassword();

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password);

}