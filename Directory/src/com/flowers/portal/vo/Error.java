package com.flowers.portal.vo;

public class Error {
	private long errorCode;
	private long detailedErrorCode;
	private String errorMessage;
	private String errorSource;
	
	public Error() {}
	public Error(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public void setErrorCode(long errorCode) {
		this.errorCode = errorCode;
	}
	
	public long getErrorCode() {
		return errorCode;
	}
	
	public void setDetailedErrorCode(long detailedErrorCode) {
		this.detailedErrorCode = detailedErrorCode;
	}
	
	public long getDetailedErrorCode() {
		return detailedErrorCode;
	}
	
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public void setErrorSource(String errorSource) {
		this.errorSource = errorSource;
	}
	
	public String getErrorSource() {
		return errorSource;
	}

	@Override
	public String toString() {
		return "Error [errorCode=" + errorCode + ", detailedErrorCode=" + detailedErrorCode + ", errorMessage=" + errorMessage + ", errorSource=" + errorSource + "]";
	}
	
	
}
