package com.flowers.portal.vo;

import java.io.Serializable;


public class Recipient implements Serializable {
	private static final long serialVersionUID = 3868521306789118490L;
	private String firstName;
	private String lastName;
	private String attention;
	private String address1;
	private String address2;
	private String postalCode;
	private String city;
	private String additionalCities;
	private String state;
	private String countryCode;
	private String countryId;
	private String phoneNumber;
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setAttention(String attention) {
		this.attention = attention;
	}
	
	public String getAttention() {
		return attention;
	}
	
	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setAdditionalCities(String additionalCities) {
		this.additionalCities = additionalCities;
	}

	public String getAdditionalCities() {
		return additionalCities;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

}
