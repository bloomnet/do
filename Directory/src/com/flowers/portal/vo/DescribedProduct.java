package com.flowers.portal.vo;

import java.io.Serializable;

import com.flowers.portal.util.Money;

public class DescribedProduct implements Serializable {
	private static final long serialVersionUID = 5746820763752656297L;
	private Long id;
	private Integer quantity;
	private String description;
	private Money price;
	private String code;
	private boolean isCodified = false;
	
	public DescribedProduct(int quantity, String description, Money price) {
		this.quantity = quantity;
		this.description = description;
		this.price = price;
	}
	public DescribedProduct(int quantity, String description, Money price, String code) {
		this.quantity = quantity;
		this.description = description;
		this.price = price;
		this.code = code;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setPrice(Money price) {
		this.price = price;
	}
	
	public Money getPrice() {
		return price;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String productCode) {
		this.code = productCode;
	}
	public boolean isCodified() {
		return isCodified;
	}
	public void setCodified(boolean isCodified) {
		this.isCodified = isCodified;
	}
	
}
