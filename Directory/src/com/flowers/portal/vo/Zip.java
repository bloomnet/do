package com.flowers.portal.vo;

import java.io.Serializable;

public class Zip implements Serializable {
	private static final long serialVersionUID = -5001719408137299206L;
	private String zipCode;

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getZipCode() {
		return zipCode;
	}
}
