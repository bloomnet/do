package com.flowers.portal.vo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.flowers.portal.util.Money;
import com.flowers.server.entity.Shop;


public class Order implements Serializable{
	private static final long serialVersionUID = 931996160281452222L;
	public static final String MESSAGE_TYPE = "0";
	public static final String SYSTEM_TYPE = "GENERAL";
	public static final String WIRE_SERVICE_CODE = "BMT";
	
	private Shop sendingShop;
	private Shop receivingShop;
	private Shop fulfillingShop;
	
	private String cardMessage;
	private Date captureDate;
	
	private String deliveryDate;
	private String specialInstruction;
	
	private Long ocasionId;
	private String facilityId;
	
	private Recipient recipient;
	
	private String trackingNumber;
	private String shipperCode;
	private String shippingMethod;
	private Date shippingDate;
	private String containsPerishables;
	
	private String productId;
	
	private List<DescribedProduct> describedProducts = new ArrayList<DescribedProduct>();
		
	//private long userId; 
	
//	private String cardMessage;
//	private String specialInstructions;
//	private Date deliveryDate;
	//private long myRatingId;
	//private String myComments;
	//private List<ProductCodification> products = new ArrayList<ProductCodification>();
	private String facility;
	
	private Money deliveryCharge;
	private Money subTotal;
	private Money grandTotal;
	private int quantity;
	
	
	public void setSendingShop(Shop sendingShop) {
		this.sendingShop = sendingShop;
	}
	
	public Shop getSendingShop() {
		return sendingShop;
	}
	
	public void setReceivingShop(Shop receivingShop) {
		this.receivingShop = receivingShop;
	}
	
	public Shop getReceivingShop() {
		return receivingShop;
	}
	
	public void setFulfillingShop(Shop fulfillingShop) {
		this.fulfillingShop = fulfillingShop;
	}
	
	public Shop getFulfillingShop() {
		return fulfillingShop;
	}
	
	public void setRecipient(Recipient recipient) {
		this.recipient = recipient;
	}
	
	public Recipient getRecipient() {
		return recipient;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setShipperCode(String shipperCode) {
		this.shipperCode = shipperCode;
	}

	public String getShipperCode() {
		return shipperCode;
	}

	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public String getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingDate(Date shippingDate) {
		this.shippingDate = shippingDate;
	}

	public Date getShippingDate() {
		return shippingDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setSpecialInstruction(String specialInstruction) {
		this.specialInstruction = specialInstruction;
	}

	public String getSpecialInstruction() {
		return specialInstruction;
	}

	public void setCaptureDate(Date captureDate) {
		this.captureDate = captureDate;
	}

	public Date getCaptureDate() {
		return captureDate;
	}

	public void setCardMessage(String cardMessage) {
		this.cardMessage = cardMessage;
	}

	public String getCardMessage() {
		return cardMessage;
	}

	public void setOcasionId(Long ocasionId) {
		this.ocasionId = ocasionId;
	}

	public Long getOcasionId() {
		return ocasionId;
	}
	
	public String getFacilityId() {
		return facilityId;
	}

	public void setFacilityId(String facilityId) {
		this.facilityId = facilityId;
	}

	public void setDescribedProducts(List<DescribedProduct> describedProducts) {
		this.describedProducts = describedProducts;
	}

	public List<DescribedProduct> getDescribedProducts() {
		return describedProducts;
	}

	public String getFacilityName() {
		return facility;
	}

	public void setFacilityName(String facility) {
		this.facility = facility;
	}

	public Money getDeliveryCharge() {
		return deliveryCharge;
	}

	public void setDeliveryCharge(Money deliveryCharge) {
		this.deliveryCharge = deliveryCharge;
	}

	public Money getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Money subTotal) {
		this.subTotal = subTotal;
	}

	public Money getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(Money grandTotal) {
		this.grandTotal = grandTotal;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getContainsPerishables() {
		return containsPerishables;
	}

	public void setContainsPerishables(String containsPerishables) {
		this.containsPerishables = containsPerishables;
	}
	
}
