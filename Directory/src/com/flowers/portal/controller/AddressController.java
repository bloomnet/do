package com.flowers.portal.controller;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.flowers.portal.service.SearchService;
import com.flowers.portal.vo.Zip;
import com.flowers.server.entity.Address;
import com.flowers.server.entity.City;
import com.flowers.server.entity.State;
import com.flowers.server.search.SearchQuery;
import com.flowers.server.search.SearchQueryType;
import com.flowers.server.search.SearchResultSet;

/**
 * Bloomnet controller for address. This is a controller for a RESTful Spring MVC web service that
 * returns on JSON format.
 * @author Andre Moura
 */
@Controller
public class AddressController {
	protected final Log logger = LogFactory.getLog(getClass());
	@Autowired private SearchService search;
	/**
	 * Returns a JSON array containing all states.
	 * @return JSON array with all states
	 */
	@RequestMapping(value="state", method=RequestMethod.GET)
	public @ResponseBody SearchResultSet<State> getAllStates() {
		String message = "REST: /state"; 
		logger.info(message);
		SearchQuery query = new SearchQuery(SearchQueryType.STATE, SearchQuery.RETURN_TYPE_OBJECT);
		return search.search(query);
	}
	
	/**
	 * Returns a JSON array with the city of a given state {stateId}
	 * @param stateId state that will be criteria for the search of cities
	 * @return JSON array with cities
	 */
	@RequestMapping(value="state/{stateId}/city", method=RequestMethod.GET)
	public @ResponseBody SearchResultSet<City> getCities(@PathVariable String stateId) {
		String message = String.format("REST: /state/%s/city",stateId); 
		logger.info(message);
		SearchQuery query = new SearchQuery(SearchQueryType.CITIES, SearchQuery.RETURN_TYPE_MAP);
		query.setState(stateId);
		return search.search(query);
	}
	
	/**
	 * Returns a JSON array with the city of a given zip {zipId}
	 * @param stateId state that will be criteria for the search of cities
	 * @return JSON array with cities
	 */
	@RequestMapping(value="zip/{zip}/city", method=RequestMethod.GET)
	public @ResponseBody SearchResultSet<City> getCityByZip(@PathVariable String zip) {
		zip = zip.toLowerCase().replaceAll(" ", "");
		String message = String.format("REST: /zip/%s/city",zip); 
		logger.info(message);
		SearchQuery query = new SearchQuery(SearchQueryType.CITY, SearchQuery.RETURN_TYPE_MAP);
		query.setZip(zip);
		return search.search(query);
	}
	
	/**
	 * Returns a JSON array with the city of a given zip {zipId}
	 * @param stateId state that will be criteria for the search of cities
	 * @return JSON array with cities
	 */
	@RequestMapping(value="state/{countryId}", method=RequestMethod.GET)
	public @ResponseBody SearchResultSet<State> getStatesByCountryId(@PathVariable String countryId) {
		countryId = countryId.replaceAll(" ", "");
		String message = String.format("REST: /state/%s",countryId); 
		logger.info(message);
		SearchQuery query = new SearchQuery(SearchQueryType.STATEBYCOUNTRY, SearchQuery.RETURN_TYPE_MAP);
		query.setCountryId(countryId);
		return search.search(query);
	}
	
	/**
	 * Returns a JSON array with all major cities
	 * @return JSON array with all major cities
	 */
	@RequestMapping(value="majorcity", method=RequestMethod.GET)
	public @ResponseBody SearchResultSet<City> getMajorCities() {
		String message = "REST: /majorcity";
		logger.info(message);
		SearchQuery query = new SearchQuery(SearchQueryType.MAJORCITY, SearchQuery.RETURN_TYPE_MAP);
		return search.search(query);
	}
	
	/**
	 * Returns a JSON array with the address of a recipient. The parameters first name and last name
	 * are not required on the declaration to avoid 404 error but they are required for the search
	 * of the recipient. First name and last name should be on the query string.<br/>
	 * <b>e.g. /recipient/address?firstname=andre&lastname=moura</b> 
	 * @param request an HTTPServletRequest instance
	 * @param firstName informed on the query string.
	 * @param lastName informed on the query string.
	 * @return JSON representation of the address.
	 */
	@RequestMapping(value="recipient/address", method=RequestMethod.GET)
	public @ResponseBody Address getRecipientAddress(HttpServletRequest request,
			@RequestParam(value="firstname", required=false) String firstName,
			@RequestParam(value="lastname", required=false) String lastName) {
		String message = "REST: /recipient/address?" + request.getQueryString(); 
		logger.info(message);

		State state = new State();
		state.setId(3);
		state.setName("Nevada");
		
		City city = new City();
		city.setState(state);
		city.setName("Las Vegas");
		
		Address address = new Address();
		address.setCity(city);
		address.setStreetAddress1("Strip, 1000");
		address.setStreetAddress2("Room 1");
		address.setPostalCode("30987");
		
		return address;
	}
	
	/**
	 * Returns the zip code of a given address. The parameters address1, city, state are not required
	 * on the declaration to avoid 404 error but they are required for the search of the zip. All 
	 * parameters should be on the query string.<br/>
	 * <b>e.g. /zip?address1=fifth%20avenue&address2=1500&city=new%20york&state=ny</b>
	 * @param request
	 * @param address1
	 * @param address2
	 * @param city
	 * @param state
	 * @return
	 */
	@RequestMapping(value="zip", method=RequestMethod.GET)
	public @ResponseBody Zip getZipByAddress(HttpServletRequest request,
			@RequestParam(value="address1", required=false) String address1,
			@RequestParam(value="address2", required=false) String address2,
			@RequestParam(value="city", required=false) String city,
			@RequestParam(value="state", required=false) String state) {

		String message = "REST:/ zip?" + request.getQueryString();
		logger.info(message);
		
		Zip zip = new Zip();
		zip.setZipCode("30987");
		
		return zip;
	}
}
