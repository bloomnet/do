/**
 * 
 */
package com.flowers.portal.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.flowers.portal.service.AuthUserDetailsService;
import com.flowers.portal.service.SessionManager;





/**
 * Controller for "signing out" of the application.
 *
 * @author Mark Silver
 */
@Controller
@RequestMapping("/signout.htm")
public class SignOutController {
	
    // Define a static logger variable
    static Logger logger = Logger.getLogger( SignOutController.class );
    
    @Autowired private SessionManager sessionManager;
    
	@RequestMapping(method = RequestMethod.GET)
	public String handleRequest( HttpServletRequest request,
								 ModelMap model ) {
		boolean isR4Me = false;
		if(request.getSession(true).getAttribute("isR4Me") != null && request.getSession(true).getAttribute("isR4Me").equals("true"))
			isR4Me = true;
        sessionManager.deleteAllApplicationObjectsFromSession(request);
        request.getSession().invalidate();
        //if(isR4Me) request.getSession(true).setAttribute("isR4Me","true");
        if(isR4Me) return "redirect:https://directory.bloomnet.net";
        else return "redirect:signin.htm";
	}
}
