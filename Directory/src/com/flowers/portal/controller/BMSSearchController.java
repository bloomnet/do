package com.flowers.portal.controller;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.flowers.portal.service.ControllerService;
import com.flowers.portal.service.PortalService;
import com.flowers.portal.service.SessionManager;
import com.flowers.portal.util.DateUtil;
import com.flowers.server.entity.LoginLog;
import com.flowers.server.entity.User;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.security.auth.AutoLoginException;

@Controller
public class BMSSearchController {
	
	Properties props = new Properties();
    @Autowired private SessionManager sessionManager;
    @Autowired private PortalService portal;
	@Autowired private Properties directoryProperties;
	
	public static final String HTTP_HEADER_SHOP_CODE = "rfx_shopcode";
	public static final String HTTP_HEADER_DELIVERY_DATE = "rfx_deliverydate";
	public static final String HTTP_HEADER_HASH = "hash";
	
    @Autowired private ControllerService controllerService;
    @Autowired private HibernateTemplate hibernateTemplate;

    
	private static Logger _log = Logger.getLogger(BMSSearchController.class);
	

	
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value="bms_search.htm")
	public String login(HttpServletRequest req, HttpServletResponse res,
			  ModelMap model) throws AutoLoginException {
		
		String shopCode = null;
        String view = req.getParameter("view");
        if(view != null && view.equals("bms")) {
        	} else {
        	try {
        		String salt = directoryProperties.getProperty("auth.salt");
        
        		shopCode = req.getParameter(HTTP_HEADER_SHOP_CODE);
        		String deliveryDate = req.getParameter(HTTP_HEADER_DELIVERY_DATE);
        		String hash = req.getParameter(HTTP_HEADER_HASH);
        		String zip = req.getParameter("rfx_zipcode");
				String city = req.getParameter("rfx_city");
				String state = req.getParameter("rfx_state");
				String callback = req.getParameter("callback");
        		if(deliveryDate == null || hash == null) return null;
        		String computed = controllerService.SHA1(salt+shopCode+deliveryDate);
        		if(!computed.equals(hash)){
        			_log.info("Hashed secret invalid.");
        			return null;
        		}
        		if(shopCode == null || shopCode.length() < 1) {
        			_log.error("Did not receive shop code from external system");
        			return null;
        		}
        		if(Validator.isNotNull(shopCode)) {
        			try {
        				
        				try{
	       					LoginLog log = new LoginLog(shopCode);
	       					hibernateTemplate.getSessionFactory().getCurrentSession().saveOrUpdate(log);
       					}catch(Exception ee){
       						//fail silently and continue
       					}
        				
        				User user = controllerService.getUserByUserName(shopCode.substring(0, 4));
	       				sessionManager.setApplicationUser( req, user );
	       				String nextView="bmsSearch";
	       				req.setAttribute("categories", portal.fetchCategories());
	       				req.setAttribute("products", portal.getProducts());
	       				req.setAttribute("countries", portal.getCountryOptions(directoryProperties.getProperty("USACountryId")));
	       				req.setAttribute("bannerAds", portal.getBannerAds());
	       				req.setAttribute("sideAds", portal.getSideAds());
	       				String formatDate = DateUtil.formatDateString(deliveryDate);
	    				req.getSession(true).setAttribute("callback", callback);
	       				if(deliveryDate != null && deliveryDate.length() > 0) req.setAttribute("currentDate", formatDate);
	    				if(zip != null && zip.length() > 0) req.setAttribute("currentZip", zip);
	    				if(city != null && city.length() > 0) req.setAttribute("currentCity", city.toUpperCase());
	    				if(state != null && state.length() > 0)
	    					req.setAttribute("states", portal.getStatesOptions(state,directoryProperties.getProperty("USACountryId")));
	    				else
	    					req.setAttribute("states", portal.getStatesOptions("",directoryProperties.getProperty("USACountryId")));
	    				
	    				
	    				
	    		        return nextView;

        			} catch(Exception e) {
            	
            		_log.info(e);
            	}
            }
        	} catch (Exception e) {
            _log.error(e);
        	}
        	
        }
        return null;
        
	}
}
	