package com.flowers.portal.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

//import com.bloomnet.bom.common.entity.Uactivitytype;
//import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.flowers.portal.exception.UserWrongPasswordException;
import com.flowers.portal.exception.WorkingShopNotFoundException;
import com.flowers.portal.exception.UserNotFoundException;

import com.flowers.portal.service.AuthUserDetailsService;
import com.flowers.portal.service.SessionManager;
import com.flowers.portal.validators.SignInValidator;
import com.flowers.server.entity.LoginLog;
import com.flowers.server.entity.User;

/**
 * Controller for the Sign In screen.
 *
 * @author Danil Svirchtchev
 */
@Controller
@RequestMapping("/signin.htm")
public class SignInController {

    // Define a static logger variable
    static Logger logger = Logger.getLogger( SignInController.class );
    
    @Autowired private SessionManager sessionManager;
    @Autowired private SignInValidator signInValidator;
    @Autowired private AuthUserDetailsService authService;
    @Autowired private HibernateTemplate hibernateTemplate;

    @Transactional
    @RequestMapping(method = RequestMethod.POST)
    public String processSubmit( HttpServletRequest request,
    							 @ModelAttribute("command") User webAppUser, 
    		                     BindingResult result, 
    		                     SessionStatus status ) {
    	
    	String nextView = "signin";
    	
    	signInValidator.validateForm(webAppUser, result);
        
        if (result.hasErrors()) {
        	// do nothing
        } else {

        	try { 
        		
				 User authenticatedUser = authService.authenticate(webAppUser);		
				 sessionManager.setApplicationUser( request, authenticatedUser );
				 nextView = "redirect:contemporary.htm";
				 request.getSession(true).setAttribute("uname", authenticatedUser.getUserName());
				 request.getSession(true).setAttribute("captcha", authenticatedUser.getPassword());
				 
				 LoginLog log = new LoginLog(authenticatedUser.getUserName()+"0000");
				 hibernateTemplate.getSessionFactory().getCurrentSession().saveOrUpdate(log);
				 
			}catch (UserNotFoundException e) {
				result.reject("error.login.exception", new Object[] { e.getMessage() }, null );
				logger.info(e);
			} catch (UserWrongPasswordException e) {
				result.reject("error.login.exception", new Object[] { e.getMessage() }, null );
			} catch (WorkingShopNotFoundException e) {
				result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
				logger.error(e);
				e.printStackTrace();
	        }catch(Exception ee) {
	        	ee.printStackTrace();
	        }
        }
        
        return nextView;
    }
    
	@RequestMapping(method = RequestMethod.GET)
	public String handleRequest( HttpServletRequest request,
			                     Model model ) {
		
		String nextView = "signin";
		
		if( sessionManager.getApplicationUser(request) != null ) {
		    
			nextView = "redirect:contemporary.htm";			
		} 
		else {
		    
			//WebAppUser user = WebAppUser.getInstance();		
			model.addAttribute( "command", new User() );
		}
	
		return nextView;
	}
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}
	public SessionFactory getSessionFactory() {
		return hibernateTemplate.getSessionFactory();
	}

	
}
