package com.flowers.portal.controller;

import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.flowers.portal.service.AuthUserDetailsService;
import com.flowers.portal.service.ControllerService;
import com.flowers.portal.service.PortalService;
import com.flowers.portal.service.SessionManager;
import com.flowers.portal.util.DateUtil;
import com.flowers.portal.vo.Order;
import com.flowers.portal.vo.RestResponse;
import com.flowers.server.entity.LoginLog;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.User;
import com.flowers.server.search.SearchResultSet;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.security.auth.AutoLoginException;

@Controller
public class AutoLoginController{
	
	Properties props = new Properties();
    @Autowired private SessionManager sessionManager;
    @Autowired private PortalService portal;
	@Autowired private Properties directoryProperties;
	@Autowired private AuthUserDetailsService authService;
	
	public static final String HTTP_HEADER_SHOP_CODE = "rfx_shopcode";
	public static final String HTTP_HEADER_DELIVERY_DATE = "rfx_deliverydate";
	public static final String HTTP_HEADER_HASH = "hash";
	public static final String HTTP_HEADER_VIEW = "view";
	
    @Autowired private ControllerService controllerService;
    @Autowired private HibernateTemplate hibernateTemplate;

	private static Logger _log = Logger.getLogger(AutoLoginController.class);
	
	@Transactional
	@RequestMapping(method = RequestMethod.GET, value="guest.htm")
	public String login(HttpServletRequest req, HttpServletResponse res,
			  ModelMap model) throws AutoLoginException {
		
		String credentials = null;
        String shopCode = null;
        String view = req.getParameter(HTTP_HEADER_VIEW);
        if(view != null && view.equals("bms")) {
        	credentials = new String();
        } else {
        	if(view != null && view.equals("r4me")) {
				req.getSession(true).setAttribute("isR4Me", "true");
			}
        	try {
        		String salt = directoryProperties.getProperty("auth.salt");
        
        		shopCode = req.getParameter(HTTP_HEADER_SHOP_CODE);
        		String deliveryDate = req.getParameter(HTTP_HEADER_DELIVERY_DATE);
        		String hash = req.getParameter(HTTP_HEADER_HASH);
        		if(deliveryDate == null || hash == null) return null;
        		String computed = controllerService.SHA1(salt+shopCode+deliveryDate);
        		if(!computed.equals(hash)){
        			_log.info("Hashed secret invalid.");
        			return "redirect:/signin.htm";
        		}
        		if(shopCode == null || shopCode.length() < 1) {
        			_log.error("Did not receive shop code from external system");
        			return credentials;
        		}
        		if(Validator.isNotNull(shopCode)) {
        			credentials = new String();
        			try {
        				
        				User user = controllerService.getUserByUserName(shopCode.substring(0, 4));
        			
        			 if(user != null && user.getUserName() != null){	
	        			 _log.info(user.getId()+" found!");     				
	       				 
	       				boolean isBlkValid = authService.validateBloomLinkCredentials(user.getUserName(), user.getPassword(), shopCode);
	       				
	       				String nextView=null;
	       				
	       				if (isBlkValid){
	       					
	       					sessionManager.setApplicationUser( req, user );
	       					
	       					try{
		       					LoginLog log = new LoginLog(user.getShopCode());
		       					hibernateTemplate.getSessionFactory().getCurrentSession().saveOrUpdate(log);
	       					}catch(Exception ee){
	       						//fail silently and continue
	       					}

	       					nextView = "redirect:contemporary.htm";
		    				Order order = new Order();
		    				order.setDeliveryDate(DateUtil.formatDateString(deliveryDate));
		    				req.getSession(true).setAttribute("BLOOMNET_ORDER", order);
		    				
	       				}else{
	       					nextView = "redirect:/signin.htm";
	       					req.getSession(true).setAttribute("errorMessage", "Your password has been changed since the last time you logged in (or this is your first time logging in). Please sign in using your latest Bloomlink credentials before continuing." );
	       					
	       				}
	       				req.getSession(true).setAttribute("uname", user.getUserName());
	       				req.getSession(true).setAttribute("captcha", user.getPassword());
	    		        return nextView;
        			 }else{
        				 return "redirect:/signin.htm";
        			 }

        			} catch(Exception e) {
            	
            		_log.info(e);
            	}
            }
        	} catch (Exception e) {
            _log.error(e);
        	}
        	
        }
        return null;
        
	}

        	
        	
	
	
	public String processAutoLogin( HttpServletRequest request,HttpServletResponse response,
			                     ModelMap model ) throws IOException {
		
		String returnValue = null;
		
		@SuppressWarnings("rawtypes")
		Enumeration e = request.getParameterNames();
		while(e.hasMoreElements()){
			String param = (String) e.nextElement();
			}
		
		
			
				User  user  = (User) sessionManager.getApplicationUser(request);
				request.setAttribute("user_email", user.getEmail());
				request.setAttribute("user_name", user.getShopCode());
		
				
				String view = request.getParameter("view");
				
				
				
				if(view == null || view.equals("order")) {
					
					String date = null, formatDate = null;
					Order order = (Order)request.getSession(true).getAttribute("BLOOMNET_ORDER");
					try{
						date = request.getParameter("rfx_deliverydate");
						formatDate = DateUtil.formatDateString(date);
					}catch(Exception ee){}
					if(order != null) order.setFacilityId("");
					String states = null;
					if(order != null) {
						request.setAttribute("order", request.getSession(true).getAttribute("BLOOMNET_ORDER"));			
						if(order.getRecipient() != null){
							request.setAttribute("states", portal.getStatesOptions(order.getRecipient().getState(),order.getRecipient().getCountryId()));
							request.setAttribute("countries", portal.getCountryOptions(order.getRecipient().getCountryId()));
						}
					}
					if(order == null && date != null && date.length() > 0){
						order = new Order();
						order.setDeliveryDate(formatDate);
						request.getSession(true).setAttribute("BLOOMNET_ORDER", order);
					}
					if(states == null){
						states = portal.getStatesOptions("",directoryProperties.getProperty("USACountryId"));
						request.setAttribute("countries", portal.getCountryOptions(directoryProperties.getProperty("USACountryId")));
					}
					request.setAttribute("states", states);
					List<Document> products = portal.getProducts();
					request.setAttribute("products", products);
					returnValue="redirect:/traditional.htm";
				}				
				
											return returnValue;
				
			
			}
			
			@RequestMapping(value= "login", method = RequestMethod.POST)
			public String processAutoLogin( HttpServletRequest request,
					                     ModelMap model ) throws IOException {
				
				String returnValue = null;
				
		@SuppressWarnings("rawtypes")
		Enumeration e = request.getParameterNames();
		while(e.hasMoreElements()){
			String param = (String) e.nextElement();
			}
		
				Integer timestamp = (request.getParameter("timestamp") != null &&  request.getParameter("timestamp").length() > 0) ? 
					Integer.valueOf(request.getParameter("timestamp")) : 0;
			        
				User  user  = (User) sessionManager.getApplicationUser(request);
				request.setAttribute("user_email", user.getEmail());
				request.setAttribute("user_name", user.getShopCode());
				String view = request.getParameter("view");
				
				if(view == null) {
					String ppid = request.getParameter("p_p_id");
					if(ppid != null){
						view = request.getParameter("_"+ppid+"_view");
						try{
							String[] baseView = view.split("timestamp");
							view = baseView[0];
							if(baseView.length > 1){
								view = view.substring(0,view.length()-1);
							}
						}catch(Exception ee){}
					}
				}
				
				String from = request.getParameter("from");
				if(view == null || view.equals("order")) {
					
					String date = null, formatDate = null;
					Order order = (Order)request.getSession(true).getAttribute("BLOOMNET_ORDER");
					try{
						date = request.getParameter("rfx_deliverydate");
						formatDate = DateUtil.formatDateString(date);
					}catch(Exception ee){}
					if(order != null) order.setFacilityId("");
					String states = null;
					if(order != null) {
						request.setAttribute("order", request.getSession(true).getAttribute("BLOOMNET_ORDER"));			
						if(order.getRecipient() != null){
							request.setAttribute("states", portal.getStatesOptions(order.getRecipient().getState(),order.getRecipient().getCountryId()));
							request.setAttribute("countries", portal.getCountryOptions(order.getRecipient().getCountryId()));
						}
					}
					if(order == null && date != null && date.length() > 0){
						order = new Order();
						order.setDeliveryDate(formatDate);
						request.getSession(true).setAttribute("BLOOMNET_ORDER", order);
					}
					if(states == null){
						states = portal.getStatesOptions("",directoryProperties.getProperty("USACountryId"));
						request.setAttribute("countries", portal.getCountryOptions(directoryProperties.getProperty("USACountryId")));
					}
					request.setAttribute("states", states);
					List<Document> products = portal.getProducts();
					request.setAttribute("products", products);
					returnValue="order";
				} else if(view.equals("submit")) {
					Order order = (Order)request.getSession(true).getAttribute("BLOOMNET_ORDER");
					if(user != null) {
						new ObjectMapper();
						if(order != null) {
							if(order.getSendingShop() == null) {
								Shop sendingShop = new Shop();
								sendingShop.setShopCode(user.getShopCode());
								order.setSendingShop(sendingShop);
							}
							order.setReceivingShop(order.getFulfillingShop());
							if(order.getOcasionId() == null) order.setOcasionId(8L);
							RestResponse restResponse = portal.order(user, order);
							returnValue ="summary";
							
							if(restResponse.getErrors().size() == 0) {
								request.getSession(true).setAttribute("BLOOMNET_ORDER", null);
							}
							@SuppressWarnings("unchecked")
							List<SearchResultSet<Document>> searches = (List<SearchResultSet<Document>>)request.getSession().getAttribute("BLOOMNET_LOGS");
							if(searches != null && searches.size() > 0) {
								SearchResultSet<Document> search = searches.get(searches.size()-1);
								if(search != null) search.setOrderId(restResponse.getBmtOrderNumber());
							}	
							returnValue ="summary";
						} else {
							com.flowers.portal.vo.Error error = new com.flowers.portal.vo.Error("No order submitted.");
							new RestResponse(error);
							returnValue =view;
						}
					}
				} else if(view.equals("search") || view.equals("bms")) {
					request.setAttribute("states", portal.getStatesOptions("",directoryProperties.getProperty("USACountryId")));
					request.setAttribute("countries", portal.getCountryOptions(directoryProperties.getProperty("USACountryId")));
					List<Document> products = portal.getProducts();
					request.setAttribute("products", products);
					request.setAttribute("categories", portal.fetchCategories());
					if(view.equals("bms")) {
						String date = request.getParameter("rfx_deliverydate");
						String zip = request.getParameter("rfx_zipcode");
						String city = request.getParameter("rfx_city");
						String state = request.getParameter("rfx_state");
						String formatDate = DateUtil.formatDateString(date);
						if(date != null && date.length() > 0) request.setAttribute("currentDate", formatDate);
						if(zip != null && zip.length() > 0) request.setAttribute("currentZip", zip);
						if(city != null && city.length() > 0) request.setAttribute("currentCity", city.toUpperCase());
						if(state != null && state.length() > 0){
							request.setAttribute("states", portal.getStatesOptions(state,directoryProperties.getProperty("USACountryId")));
							request.setAttribute("countries", portal.getCountryOptions(directoryProperties.getProperty("USACountryId")));
						}
						returnValue="bmsSearch";
					} else {
						Order order = (Order)request.getSession(true).getAttribute("BLOOMNET_ORDER");
						if(order != null && order.getDeliveryDate() != null){
							request.setAttribute("currentDate", order.getDeliveryDate());
						}else{
							request.setAttribute("currentDate", DateUtil.getCurrentDate(timestamp));
						}
						returnValue="search";
					}
				} else if(view.equals("summary")) {
					Order order = null;			
					String commitments = request.getParameter("commitment");
					if(commitments != null && commitments.length() > 0) {
						order = (Order)request.getSession(true).getAttribute("BLOOMNET_ORDER");
						if(order != null) {
							request.setAttribute("order", request.getSession(true).getAttribute("BLOOMNET_ORDER"));
							order.setFulfillingShop(new Shop(directoryProperties.getProperty("commitmentShop"), "Commitment To Coverage"));
							controllerService.populateSummaryParameters(request, order);
						} else 	{
							order = new Order();
							order.setFulfillingShop(new Shop(directoryProperties.getProperty("commitmentShop"), "Commitment To Coverage"));
							request.getSession(true).setAttribute("BLOOMNET_ORDER", order);					
						}
						order.setDeliveryDate(commitments);
						if(order.getRecipient() != null) returnValue="summary";
						
						else {
							request.setAttribute("states", portal.getStatesOptions("",directoryProperties.getProperty("USACountryId")));
							request.setAttribute("countries", portal.getCountryOptions(directoryProperties.getProperty("USACountryId")));
							List<Document> products = portal.getProducts();
							request.setAttribute("products", products);
							returnValue="order";
						}
					} else {
						if(from != null && from.equals("order")) order = controllerService.processOrderParameters( request, false );
						else if(from != null && from.equals("search")) order = controllerService.processSearchParameters(request);
						if(order == null || order.getRecipient() == null) {
							request.setAttribute("states", portal.getStatesOptions("",directoryProperties.getProperty("USACountryId")));
							request.setAttribute("countries", portal.getCountryOptions(directoryProperties.getProperty("USACountryId")));
							List<Document> products = portal.getProducts();
							request.setAttribute("products", products);
							returnValue="order";
						} else if(order.getFulfillingShop() == null) {
							request.setAttribute("states", portal.getStatesOptions(order.getRecipient().getState(),order.getRecipient().getCountryId()));
							request.setAttribute("countries", portal.getCountryOptions(order.getRecipient().getCountryId()));
							List<Document> products = portal.getProducts();
							request.setAttribute("products", products);
							request.setAttribute("categories", portal.fetchCategories());
							if(order.getDeliveryDate() == null) request.setAttribute("currentDate", DateUtil.getCurrentDate(timestamp));
							else request.setAttribute("currentDate", order.getDeliveryDate());
							returnValue="search";
						} else {
							controllerService.populateSummaryParameters(request, order);
							returnValue="summary";
						}
					}
				} else if(view.equals("close")) { 
					request.getSession(true).removeAttribute("BLOOMNET_ORDER");
					List<Document> products = portal.getProducts();
					request.setAttribute("products", products);
					request.setAttribute("states", portal.getStatesOptions("",directoryProperties.getProperty("USACountryId")));
					request.setAttribute("countries", portal.getCountryOptions(directoryProperties.getProperty("USACountryId")));
					returnValue="order";
				} else if(view.equals("clear")) { 
					request.getSession(true).setAttribute("BLOOMNET_SEARCH_PARMS", "false");
				} else 
					returnValue=view;
		
				return returnValue;
		
	
	}
	
	
	
	
}
