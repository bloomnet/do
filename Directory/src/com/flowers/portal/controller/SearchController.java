package com.flowers.portal.controller;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.hibernate.SessionFactory;
import org.jdom.JDOMException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.flowers.portal.dao.OrderDAO;
import com.flowers.portal.service.AuthUserDetailsService;
import com.flowers.portal.service.PortalService;
import com.flowers.portal.service.SearchService;
import com.flowers.portal.service.impl.ShopAvailabilityServiceImpl;
import com.flowers.portal.util.ConvertLuceneToJSON;
import com.flowers.portal.util.DateUtil;
import com.flowers.portal.util.GenerateBloomlinkPassword;
import com.flowers.portal.vo.Order;
import com.flowers.portal.vo.Recipient;
import com.flowers.server.entity.Address;
import com.flowers.server.entity.MinimumPrice;
import com.flowers.server.entity.ProductCodification;
import com.flowers.server.entity.Rating;
import com.flowers.server.entity.Route4MeIntake;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.User;
import com.flowers.server.responseobjects.JSONStatesResponse;
import com.flowers.server.responseobjects.JSONCitiesResponse;
import com.flowers.server.responseobjects.JSONCitiesZipResponse;
import com.flowers.server.responseobjects.JSONCountriesResponse;
import com.flowers.server.responseobjects.JSONFacilitiesResponse;
import com.flowers.server.responseobjects.JSONFacilityListingResponse;
import com.flowers.server.responseobjects.JSONLandingAdsResponse;
import com.flowers.server.responseobjects.JSONListingResponse;
import com.flowers.server.responseobjects.JSONResultsAdsResponse;
import com.flowers.server.search.SearchQuery;
import com.flowers.server.search.SearchQueryType;
import com.flowers.server.search.SearchResultSet;
import com.qas.EncodingUtil;
import com.qas.newmedia.internet.ondemand.product.proweb.AddressLine;
import com.qas.newmedia.internet.ondemand.product.proweb.PicklistItem;
import com.qas.newmedia.internet.ondemand.product.proweb.PromptSet;
import com.qas.newmedia.internet.ondemand.product.proweb.QuickAddress;
import com.qas.newmedia.internet.ondemand.product.proweb.SearchResult;
import com.qas.ondemand_2011_03.QAAuthentication;

@Controller
public class SearchController {
	protected final Log logger = LogFactory.getLog(getClass());
	@Autowired private SearchService searchService;
    @Autowired private AuthUserDetailsService authService;
	@Autowired private Properties directoryProperties;
	@Autowired private PortalService portal;
	@Autowired private HibernateTemplate hibernateTemplate;
    @Autowired private OrderDAO orderDAO;
    String encryptedAPIKey = "77a2d7b6b069bc91d5053b551220b66d60c5d5ee446c397dd403749cc415c79d0312f39c13fd2a23760d8d182ac97a9959c4b7d9d7b2d1a6d62daf8d4cee6632";

    public static class ResultsXML {
        private String resultsXML;
        public void setResultsXML(String resultsXML){
        	this.resultsXML = resultsXML;
        }
        public String getResultsXML(){
        	return this.resultsXML;
        }
    }

	private Map<String, String> stateMap;
	private Map<String, String> reverseStateMap;
	
	private static List<String> fieldExclusions = new ArrayList<String>();
	static {
		fieldExclusions.add("_hibernate_class");
		fieldExclusions.add("zips");
	}
	
	@Transactional
	@SuppressWarnings("unchecked")
	@RequestMapping(value="nextContemporary", method=RequestMethod.GET)
	public String getNextSearchResultPageContemporary(HttpServletRequest request) {
		SearchResultSet<Document> resultSet = (SearchResultSet<Document>)request.getSession(true).getAttribute("searchResults");
		if(resultSet != null && resultSet.getPageNumber()+1 <= resultSet.getPageCount() - 1){
			resultSet.setPageNumber(resultSet.getPageNumber()+1);
			request.setAttribute("searchResults", resultSet);
			request.setAttribute("products", searchService.fetchProducts());
			request.setAttribute("categories", searchService.fetchCategories());
		}else{
			request.setAttribute("searchResults", resultSet);
			request.setAttribute("products", searchService.fetchProducts());
			request.setAttribute("categories", searchService.fetchCategories());
		}
		return "../pages/results";
	}
			
	@SuppressWarnings("unchecked")
	@RequestMapping(value="previousContemporary", method=RequestMethod.GET)
	public String getPreviousSearchResultPageContemporary(HttpServletRequest request) {
		SearchResultSet<Document> resultSet = (SearchResultSet<Document>)request.getSession(true).getAttribute("searchResults");
		if(resultSet != null && resultSet.getPageNumber()-1 >= 0){
			resultSet.setPageNumber(resultSet.getPageNumber()-1);
			request.setAttribute("searchResults", resultSet);
			request.setAttribute("products", searchService.fetchProducts());
			request.setAttribute("categories", searchService.fetchCategories());
		}else{
			request.setAttribute("searchResults", resultSet);
			request.setAttribute("products", searchService.fetchProducts());
			request.setAttribute("categories", searchService.fetchCategories());
		}
		return "../pages/results";
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="unavailable", method=RequestMethod.GET)
	public @ResponseBody SearchResultSet<Map<String, String>> getSearchResultAvailability(HttpServletRequest request) {
		SearchResultSet<Document> resultSet = (SearchResultSet<Document>)request.getSession(true).getAttribute("searchResults");
		if(resultSet != null) {
			SearchResultSet<Map<String,String>> results = new SearchResultSet<Map<String,String>>();
			return results;
		}
		return new SearchResultSet<Map<String,String>>();
	}
	@SuppressWarnings("unchecked")
	@RequestMapping(value="shops", method=RequestMethod.GET)
	public @ResponseBody SearchResultSet<Map<String, String>> getSearchResultMappingData(HttpServletRequest request) {
		SearchResultSet<Document> resultSet = (SearchResultSet<Document>)request.getSession().getAttribute("searchResults");
		SearchResultSet<Map<String, String>> results = new SearchResultSet<Map<String, String>>();
		if(resultSet != null) {	
			results.setDestination(resultSet.getDestination());
			for(Document doc : resultSet.getResults()) {
				Map<String, String> map = new HashMap<String, String>();
				for(Object obj : doc.getFields()) {
					if(obj instanceof Field) {
						Field field = (Field)obj;
						if(!fieldExclusions.contains(field.name()) && !field.name().startsWith("price")) {
							map.put(field.name(), field.stringValue());
						}
					}
				}
				results.getResults().add(map);
			}
		}
		return results;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value="/listing/{listingId}", method=RequestMethod.GET)
	public @ResponseBody SearchResultSet<Map<String, Object>> getShop(@PathVariable String listingId, HttpServletRequest request) {
		SearchResultSet<Map<String, Object>> resultSet = new SearchResultSet<Map<String, Object>>();
		String message = String.format("REST: /listing/%s", listingId);
		logger.info(message);				
		SearchQuery searchQuery1 = new SearchQuery(SearchQueryType.LISTING, SearchQuery.RETURN_TYPE_DOCUMENT);
		searchQuery1.setListingId(listingId);
		SearchResultSet<Document> results1 = (SearchResultSet<Document>)request.getSession(true).getAttribute("searchResults");
		if(results1 != null){
			Document listing = results1.getResultById(listingId);
			//listing.add(new Field("clicked", "true", Field.Store.YES, Field.Index.NOT_ANALYZED, Field.TermVector.NO));
			if(listing != null) {
				String shopCode = listing.get("shopcode");
				SearchQuery searchQuery2 = new SearchQuery(SearchQueryType.SHOP_DETAIL, SearchQuery.RETURN_TYPE_DOCUMENT);
				searchQuery2.setShopCode(shopCode);
				SearchResultSet<Document> results = searchService.search(searchQuery2);			
				if(results.getResultSize() == 1) {
					Map<String, Document> productsMap = searchService.fetchProducts();
					Map<String, Document> categoriesMap = searchService.fetchCategories();
					Document doc = results.getResult(0);
					Map<String, Object> map = new HashMap<String, Object>();
					List<Map<String,String>> minimums = new ArrayList<Map<String,String>>();
					List<Map<String,String>> facilities = new ArrayList<Map<String,String>>();
					List<Map<String,String>> products = new ArrayList<Map<String,String>>();
					for(Object obj : doc.getFields()) {
						if(obj instanceof Field) {
							Field field = (Field)obj;
							if(field.name().equals("products")) {
								String productsVal = field.stringValue();
								if(productsVal != null && productsVal.length() > 0) {
									String[] productsArray = productsVal.split(" ");
									for(int j=0; j < productsArray.length; j++) {
										Map<String,String> vals = new HashMap<String,String>();
										Document prod = productsMap.get(productsArray[j]);
										if(prod != null) {
											vals.put("name", prod.get("name"));
											vals.put("code", prod.get("code"));
											vals.put("bloomnet_url", prod.get("bloomnet_url"));
											products.add(vals);
										}
									}
								}
							} else if(field.name().equals("facilities")) {
								String facilitiesVal = field.stringValue();
								if(facilitiesVal != null && facilitiesVal.length() > 0) {
									String[] facArray = facilitiesVal.split(" ");
									List<Document> facilityList = searchService.fetchFacilities(facArray);
									for(Document doc2 : facilityList) {
										Map<String,String> vals = new HashMap<String,String>();
										vals.put("id", doc2.get("id"));
										vals.put("name", doc2.get("name"));
										vals.put("type", doc2.get("type"));
										vals.put("address1", doc2.get("address1"));
										vals.put("address2", doc2.get("address2"));
										vals.put("city", doc2.get("city"));
										if(doc2.get("state") != null){
											vals.put("state", doc2.get("state"));
											vals.put("state_code", doc2.get("state_code"));
										}
										vals.put("zip", doc2.get("zip"));
										facilities.add(vals);
									}
								}
							} else if(field.name().startsWith("price")) {
								String price = field.stringValue();
								if(price != null) {
									Document cat = categoriesMap.get(field.name());
									String catName = cat != null ? cat.get("name") : "";
									boolean seasonal = false;
									if(price.endsWith("H")) {
										seasonal = true;
										price = price.substring(0, price.length()-1);
									}
									if(price.endsWith("0")) price += "0";
									Map<String,String> vals = new HashMap<String,String>();
									vals.put("category",catName);
									vals.put("price",price);
									vals.put("seasonal",String.valueOf(seasonal));
									minimums.add(vals);
								}
							}else if(field.name().equals("id")){
								map.put("shopId", field.stringValue());
							}else if(!fieldExclusions.contains(field.name())) {
								map.put(field.name(), field.stringValue());
							}
						}
					}
					try{
						File dir = new File("/opt/apps/portal/tomcat-6.0.26/webapps/bloomnet-images/ads"); 
						@SuppressWarnings("deprecation")
						FileFilter fileFilter = new WildcardFileFilter(shopCode+"pic*"); 
						File[] files = dir.listFiles(fileFilter);
						map.put("images_count", files.length);
					}catch(Exception ee){
						ee.printStackTrace();
					}
					map.put("id", listingId);
					if(listing.get("distance") != null) map.put("distance", listing.get("distance"));
					if(listing.get("delivery_charge") != null) {
						NumberFormat formatter = new DecimalFormat("#0.00");
						String delivery = listing.get("delivery_charge");
						map.put("delivery_charge", formatter.format(Double.valueOf(delivery)));
					}
					map.put("custom_listing", listing.get("custom_listing"));
					String shop_code = listing.get("shopcode");
					String entry_code = listing.get("entry_code");
					String ad_size = listing.get("ad_size");
					if(shop_code != null && entry_code != null && ad_size != null)
						map.put("ad_url", shop_code+"-"+entry_code+ad_size+".png");
					else
						map.put("ad_url", "");
					map.put("minimums", minimums);
					map.put("facilities", facilities);
					map.put("products", products);
					resultSet.getResults().add(map);
				}			
			}
		}
		return resultSet;
	}
	
	@RequestMapping(value="/productcodification/{productCode}", method=RequestMethod.GET)
	public @ResponseBody SearchResultSet<Map<String, Object>> getProductDescription(@PathVariable String productCode, HttpServletRequest request) {
		SearchResultSet<Map<String, Object>> resultSet = new SearchResultSet<Map<String, Object>>();
		
		SearchQuery searchQuery = new SearchQuery(SearchQueryType.CODIFIED_PRODUCT_DETAIL_BY_CODE, SearchQuery.RETURN_TYPE_DOCUMENT);
		searchQuery.setProductCode(productCode.toLowerCase());
		
		SearchResultSet<Document> results = searchService.search(searchQuery);
		
		if(results != null){
			List<Document> resultList = results.getResults();
			
			Map<String, Object> map = new HashMap<String, Object>();
			
			for(int ii=0; ii<resultList.size(); ++ii){
				Document doc = resultList.get(ii);
				if(doc.get("code").toUpperCase().equals(productCode.toUpperCase())){
					String productName = doc.get("name");
					map.put("productName", productName);
				}
			}
			resultSet.getResults().add(map);
		}

		return resultSet;
	}
	
	@RequestMapping(value="verifyaddress", method=RequestMethod.GET)
	public @ResponseBody Object[] verifyAddress( HttpServletRequest request,
			@RequestParam(value="addressLine1", required=false) String addressLine1,
			@RequestParam(value="addressLine2", required=false) String addressLine2,
			@RequestParam(value="city", required=false) String city,
			@RequestParam(value="stateShort", required=false) String stateShort,
			@RequestParam(value="zip", required=false) String zip,
			@RequestParam(value="country", required=false) String country){
				
				System.setProperty("https.protocols", "TLSv1.2");
				
				List<String> finalCorrectedAddresses = new ArrayList<String>();
				
				String URL = directoryProperties.getProperty("avAddress");
			    String Username = directoryProperties.getProperty("avUsername");
			    String Password = directoryProperties.getProperty("avPassword");

			    //get search information from post
			    String action = EncodingUtil.decodeURIComponent("search");
			    String addLayout = EncodingUtil.decodeURIComponent("Database layout");
			    String searchstring = "";
			    
			    if(addressLine2 != null && !addressLine2.equals(""))
			    	searchstring = EncodingUtil.decodeURIComponent(addressLine1+" "+addressLine2+"|||@+"+city+"|"+stateShort+"|"+zip);
			    else
			    	searchstring = EncodingUtil.decodeURIComponent(addressLine1+"|||@+"+city+"|"+stateShort+"|"+zip);

				QAAuthentication authenticationInfo = new QAAuthentication();
				authenticationInfo.setUsername(Username);
				authenticationInfo.setPassword(Password);
				
				try
				{
					//setup qas engine
				    QuickAddress qAddress = new QuickAddress(URL, authenticationInfo);
				    qAddress.setEngineType(QuickAddress.VERIFICATION);
				    qAddress.setFlatten(true);
				    qAddress.setEngineIntensity(QuickAddress.CLOSE);
				    qAddress.setPromptSet("Default");
					
				    //search for address
				    if (action.equals("search"))
				    {
				        //send address to QAS
				        SearchResult result = qAddress.search("USA", searchstring, PromptSet.DEFAULT, addLayout);
				        
				        if ((result.getVerifyLevel() == "Verified") || (result.getVerifyLevel() == "InteractionRequired") ){
				        //for Verified and InteractionRequired addresses get the result
					        if(result.getPicklist() != null)
					        {
					            for( PicklistItem item : result.getPicklist().getItems())
					            {
					            	String addressCorrected = item.getPartialAddress().replaceAll(item.getPostcode(), "").trim();
					            	String[] addressCorrectedItems = addressCorrected.split(",");
					            	String correctedAddress1 = addressCorrectedItems[0];
					            	String[] correctedCityState = addressCorrectedItems[1].split(" ");
					            	String correctedCity = "";
					            	String correctedState = "";
					            	for(int ii=0; ii<correctedCityState.length; ++ii){
					            		if(ii == correctedCityState.length - 1)
					            			correctedState = correctedCityState[ii];
					            		else if(ii == 0)
					            			correctedCity = correctedCityState[ii];
					            		else
					            			correctedCity += " "+correctedCityState[ii];
					        
					            	}
					            	String correctedZip = item.getPostcode();
					            	String fullAddress = correctedAddress1+","+""+","+correctedCity+","+correctedState+","+correctedZip;
					            	if(!correctedAddress1.equalsIgnoreCase(addressLine1) || !correctedCity.equalsIgnoreCase(city) || !correctedZip.equals(zip))
					            		finalCorrectedAddresses.add(fullAddress);
					            }
					        }else if(result.getAddress() != null && result.getAddress().getAddressLines() != null){
					        	String correctedAddress = "";
					        	 for( AddressLine line : result.getAddress().getAddressLines())
						            {
						            	correctedAddress += line.getLine() + ",";
						            }
					        	 correctedAddress = correctedAddress.substring(0,correctedAddress.length() - 1).trim();
					        	 String[] correctedArray = correctedAddress.split(",");
					        	 String correctedAddress1 = correctedArray[0];
					        	 String correctedAddress2 = correctedArray[1];
					        	 String correctedCity = correctedArray[3];
					        	 String correctedState = correctedArray[4];
					        	 String correctedZip = correctedArray[5];
					        	 if(correctedZip.contains("-"))
					        		 correctedZip = correctedZip.split("-")[0];
					        	 String correctedAddressString = correctedAddress1 + "," + correctedAddress2 + "," + correctedCity + "," + correctedState + "," + correctedZip;
					        	 if(!correctedAddress1.equalsIgnoreCase(addressLine1) || !correctedAddress2.equalsIgnoreCase(addressLine2) || !correctedCity.equalsIgnoreCase(city) || !correctedZip.equals(zip))
					        		 finalCorrectedAddresses.add(correctedAddressString);
					        }
					    }	
				    }
				}
			    catch(Exception ex)
			    { 
			    	//System.out.println(ex.getMessage());
			    	return new ArrayList<String>().toArray();
			    }

		return finalCorrectedAddresses.toArray();
		
	}
	
	@RequestMapping(value="shopHours", method=RequestMethod.GET)
	public @ResponseBody Object[] getOperationalHours(HttpServletRequest request,
			@RequestParam(value="shopcode", required=false) String shopcode){
		
		ShopAvailabilityServiceImpl availabilityService = new ShopAvailabilityServiceImpl();
		ArrayList<String> hours = new ArrayList<String>();
		
		try {
			hours = (ArrayList<String>) availabilityService.searchByShopCode(shopcode);
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return hours.toArray();
		
	}
	
	@RequestMapping(value="shopZips", method=RequestMethod.GET)
	public @ResponseBody Object[] getZipCoverage(HttpServletRequest request,
			@RequestParam(value="shopcode", required=false) String shopcode){
		
		ShopAvailabilityServiceImpl availabilityService = new ShopAvailabilityServiceImpl();
		ArrayList<String> zipsCovered = new ArrayList<String>();
		try {
			zipsCovered = (ArrayList<String>) availabilityService.searchZipCoverageByShopCode(shopcode);
			Collections.sort(zipsCovered);
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return zipsCovered.toArray();
		
	}
	
	@RequestMapping(value="shopOfferings", method=RequestMethod.GET)
	public @ResponseBody Object[] getShopOfferings(HttpServletRequest request,
			@RequestParam(value="shopcode", required=false) String shopcode){
		
		ShopAvailabilityServiceImpl availabilityService = new ShopAvailabilityServiceImpl();
		ArrayList<String> offerings = new ArrayList<String>();
		
		try {
			offerings = (ArrayList<String>) availabilityService.getShopOfferings(shopcode);
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return offerings.toArray();
		
	}
	
	@RequestMapping(value="ratings", method=RequestMethod.GET)
	public @ResponseBody Object[] getShopRatings(HttpServletRequest request,
			@RequestParam(value="shopCode", required=false) String shopCode){
		
		List<Rating> shopCodes = orderDAO.getRatingsByShopCode(shopCode);
		Object[] results = shopCodes.toArray();
		
		return results;
		
	}
	
	@RequestMapping(value="abuse", method=RequestMethod.GET)
	public @ResponseBody Object[] reportAbuse(HttpServletRequest request,
			@RequestParam(value="reviewId", required=false) long reviewId){
		
		orderDAO.updateRatingAbuse(reviewId);
		
		return null;
		
	}
	
	@RequestMapping(value="saveReview", method=RequestMethod.GET)
	public @ResponseBody Object[] saveReview(HttpServletRequest request,
			@RequestParam(value="comment", required=false) String comment,
			@RequestParam(value="title", required=false) String title,
			@RequestParam(value="score", required=false) String score,
			@RequestParam(value="reviewedShop", required=false) String reviewedShop,
			@RequestParam(value="reviewedName", required=false) String reviewedName){
		
		User  user  = (User)request.getSession(true).getAttribute("USER");
		long userid = user.getId();
		String shopName = authService.getShopName(user.getUserName(),
				user.getPassword(), user.getShopCode());
		
		Rating rating = new Rating();
		rating.setComments(comment);
		rating.setDateReviewed(new Date());
		rating.setReviewed_shopcode(reviewedShop);
		rating.setReviewed_shopname(reviewedName);
		rating.setReviewer_shopname(shopName);
		rating.setReviewer_userid(userid);
		rating.setStars(Double.parseDouble(score));
		rating.setTitle(title);
		
		
		orderDAO.saveRating(rating);
		//List<Rating> shopCodes = orderDAO.getRatingsByShopCode(shopCode);
		//Object[] results = shopCodes.toArray();
		
		return null;
		
	}
	@Transactional
	protected void commitSearchLog(SearchResultSet<Document> result, User user) {

		com.flowers.server.entity.SearchQuery queryEntity = new com.flowers.server.entity.SearchQuery(result.getSearchQuery().getDate(), result.getSearchQuery().getZip(), result.getSearchQuery().getCity(), result.getSearchQuery().getState(), result.getSearchQuery().getPhone(), result.getSearchQuery().getShopCode(), result.getSearchQuery().getShopName(), result.getOrderId(), user.getShopCode());
		queryEntity.setFacility(result.getSearchQuery().getFacility());
		List<MinimumPrice> prices = result.getSearchQuery().getMinimums();
		if(prices.size() > 0) {
			StringBuffer buff = new StringBuffer();
			for(MinimumPrice price : prices)
				buff.append(price.getMinimumPrice()+" ");
			queryEntity.setMinimums(buff.toString().trim());
		}
		List<ProductCodification> prods = result.getSearchQuery().getProductCodifications();
		if(prods.size() > 0) {
			StringBuffer buff = new StringBuffer();
			for(ProductCodification prod : prods)
				buff.append(prod.getProductCodification()+" ");
			queryEntity.setProducts(buff.toString().trim());
		}
		queryEntity.setCount(result.getResultSize());
		hibernateTemplate.getSessionFactory().getCurrentSession().saveOrUpdate(queryEntity);
	}	
	
	protected void buildPaging(SearchResultSet<Document> resultSet) {
		List<Document> page = new ArrayList<Document>();
		for(int ii=1; ii <= resultSet.getResults().size(); ++ii) {
			Document result = resultSet.getResults().get(ii-1);
			if(ii%20 == 0){
				page.add(result);
				resultSet.getPages().add(page);
				page = new ArrayList<Document>();
			}else if(ii == resultSet.getResults().size()){
				page.add(result);
				resultSet.getPages().add(page);
			}else{
				page.add(result);
			}
				
		}
	}
	
	protected void populateStates() {
		 if(stateMap == null) {
			 stateMap = new HashMap<String, String>();
			 try {
				 SearchResultSet<Document> states = searchService.search(new SearchQuery(SearchQueryType.STATE, SearchQuery.RETURN_TYPE_DOCUMENT));
				 if(states != null) {
					for(Document state : states.getResults()) {
						stateMap.put(state.get("name"), state.get("short_name"));
					}
				}
			 } catch(Exception e) {
				 e.printStackTrace();
			 }
		 }
	}
	
	protected void populateStatesReverse() {
		 if(reverseStateMap == null) {
			 reverseStateMap = new HashMap<String, String>();
			 try {
				 SearchResultSet<Document> states = searchService.search(new SearchQuery(SearchQueryType.STATE, SearchQuery.RETURN_TYPE_DOCUMENT));
				 if(states != null) {
					for(Document state : states.getResults()) {
						reverseStateMap.put(state.get("short_name"), state.get("name"));
					}
				}
			 } catch(Exception e) {
				 e.printStackTrace();
			 }
		 }
	}
	@Transactional
	@RequestMapping(value="search", method=RequestMethod.POST)
	public String getContemporarySearchResultPage(HttpServletRequest request,
			@RequestParam(value="searchDeliveryDate", required=false) String date,
			@RequestParam(value="openSunday", required=false) boolean openSunday,
			@RequestParam(value="searchSelState", required=false) String state,
			@RequestParam(value="searchCityAc", required=false) String city,
			@RequestParam(value="searchAdditionalCities", required=false) String additionalCities,
			@RequestParam(value="majorCity", required=false) String majorCity,
			@RequestParam(value="searchZip", required=false) String zip,
			@RequestParam(value="searchPhone", required=false) String phone,
			@RequestParam(value="searchShopCode", required=false) String shopCode,
			@RequestParam(value="searchShopName", required=false) String shopName,
			@RequestParam(value="facility", required=false) String facility,
			@RequestParam(value="facilityData", required=false) String facilityData,
			@RequestParam(value="products", required=false) String products,
			@RequestParam(value="minimums", required=false) String minimums,
			@RequestParam(value="productId", required=false) String specialProductId,
			@RequestParam(value="fromBMS", required=false) String fromBMS){
		request.getSession(true).setAttribute("existingCoverageNoZips", "NO");
		request.getSession(true).setAttribute("fbSearch", "NO");
		Order order = (Order) request.getSession(true).getAttribute("BLOOMNET_ORDER");
		if(specialProductId != null && specialProductId.equals("1144"))  
			request.getSession(true).setAttribute("fbSearch", "YES");
		if(order != null){
			order.setProductId(specialProductId);
		}else{
			order = new Order();
			order.setProductId(specialProductId);
			order.setDeliveryDate(date);
			Recipient recipient = new Recipient();
			recipient.setCity(city.replaceAll("\\+", " "));
			Document stateDoc = portal.getStateDocument(state);
			if(stateDoc != null){
				recipient.setState(stateDoc.get("short_name").replaceAll("\\+", " "));
				recipient.setCountryId(stateDoc.get("country_id"));
				recipient.setCountryCode(stateDoc.get("country.short_name").replaceAll("\\+", " "));
			}
			if(zip != null && ! zip.equals(""))
				recipient.setPostalCode(zip.replaceAll("\\+", " "));
			order.setRecipient(recipient);
			request.getSession(true).setAttribute("BLOOMNET_ORDER",order);
		}

		populateStates();
		SearchQuery searchQuery = new SearchQuery(SearchQueryType.LISTINGS, SearchQuery.RETURN_TYPE_DOCUMENT);
		@SuppressWarnings("unused")
		String add_parms = (String)request.getSession(true).getAttribute("BLOOMNET_SEARCH_PARMS");
		
		Map<String, Document> productDocs = searchService.fetchProducts();
		request.getSession(true).setAttribute("productsMap", productDocs);
		Map<String, Document> categoryDocs = searchService.fetchCategories();
		request.getSession(true).setAttribute("categoriesMap", categoryDocs);
		if(facilityData != null) request.getSession(true).setAttribute("facilityData", facilityData);
		else request.getSession(true).setAttribute("facilityData", null);
		if (date != null && !date.isEmpty()) searchQuery.setDate(date);
		searchQuery.setOpenSunday(openSunday);
		
		if(zip != null && !zip.equals("") && !zip.equals("Zip / Postal Code") && (facility == null || facility.equals(""))) searchQuery.setZip(zip);
		if(city != null && !city.equals("")) searchQuery.setCity(city.toUpperCase());			
		if(state != null && !state.equals("")) {
			searchQuery.setStateCode(stateMap.get(state));
			searchQuery.setState(state.toUpperCase());
		}
		if(phone != null && !phone.equals("")) searchQuery.setPhone(phone);
		if(shopCode != null && !shopCode.equals("")) searchQuery.setShopCode(shopCode);
		if(shopName != null && !shopName.equals("")) searchQuery.setShopName(shopName);
		if(facility != null && !facility.equals("")) searchQuery.setFacility(facility);
		if(products != null && !products.equals("")) {
			List<ProductCodification> productCodifications = new ArrayList<ProductCodification>();
			String[] prodArr = products.substring(1).split("_");
			for(int i=0; i < prodArr.length; i++) {
				//Document doc = productDocs.get(prodArr[i]);
				productCodifications.add(new ProductCodification(Long.valueOf(prodArr[i])));
			}					
			searchQuery.setProductCodifications(productCodifications);
		}
		if(order != null && order.getProductId() != null && !order.getProductId().equals("") && !order.getProductId().equals("1")){
			List<ProductCodification> productCodifications = new ArrayList<ProductCodification>();
			String productId = order.getProductId();
			productCodifications.add(new ProductCodification(Long.valueOf(productId)));
			searchQuery.setProductCodifications(productCodifications);
		}
		if(minimums != null && !minimums.equals("")) {
			List<MinimumPrice> minimumPrices = new ArrayList<MinimumPrice>();
			String[] minArr = minimums.substring(1).split("_");
			for(int i=0; i < minArr.length; i++) {
				//Document doc = productDocs.get(prodArr[i]);
				minimumPrices.add(new MinimumPrice(Long.valueOf(minArr[i]), Double.valueOf(minArr[i+1])));
				i++;
			}				
			searchQuery.setMinimums(minimumPrices);
		}
		searchQuery.setPage(1);
		searchQuery.setReturnType(4);
		logger.info("SEARCH QUERY: " + searchQuery.toString());		
		SearchResultSet<Document> resultSet = searchService.search(searchQuery);
	
		if((searchQuery.getProductCodifications() == null || searchQuery.getProductCodifications().size() == 0) && resultSet != null){
			List<Document> filteredResults = new ArrayList<Document>();
			for(Document result : resultSet.getResults()) {
				if(result.get("products") == null || (result.get("products") != null && !result.get("products").contains("1145"))){
					filteredResults.add(result);
				}
			}
			resultSet.setResults(filteredResults);
		} //Uncomment this out to allow for Fruit Bouquet Only shops
		
		if(resultSet != null && resultSet.getResults().size() == 0){
			if(facility != null && !facility.equals("")){ 
				searchQuery.setFacility(null);
				searchQuery.setFacilityTypeId(null);
				searchQuery.setPreviouslyFacility("Y");
				resultSet = searchService.search(searchQuery);
				if(resultSet != null && resultSet.getResults().size() > 0)
					request.getSession(true).setAttribute("noSpecificResults", "We're sorry, your search criteria did not result in a match. However, the below professional florists were found that cover the area provided.");
			}
		}else{
			request.getSession(true).setAttribute("noSpecificResults", "");
		}
		
		
		if(resultSet != null && resultSet.getResultSize() != 0) {
			User user = (User)request.getSession().getAttribute("BLOOMNET_USER");
			if(user != null) resultSet.setUsername(user.getShopCode());
			
			resultSet.setPageNumber(0);
			logger.info("SEARCH RESULTSET: " + resultSet);
		
			Address destination = new Address();
			if(zip != null && !zip.equals("")) destination.setPostalCode(zip);
			if(city != null && !city.equals("")) {
				//destination.setCity(city);
			}
			resultSet.setDestination(destination);
			
			if(zip != null && !zip.equals("") && !zip.equals("Zip / Postal Code") && searchQuery.getFacility() == null){
				List<Document> newResults = new ArrayList<Document>();
				for(Document result : resultSet.getResults()) {
					//if(!result.get("listing_country").equals("USA"))
					newResults.add(result);
					/*if(result.get("zip_listings") != null) {
						String[] zipResults = result.get("zip_listings").split(" ");
						for(int i=0; i<zipResults.length; i++){
							if(zip.equals(zipResults[i]))
								newResults.add(result);
						}

					}*/ //uncomment this for zip search
				}
				if(newResults.size() > 0)
					resultSet.setResults(newResults);
				else
					request.getSession(true).setAttribute("existingCoverageNoZips", "YES");	//This is the logic that drives the page to display that there are no zip coverages but there are city ones, so click here to view them.
					request.setAttribute("searchResults", new SearchResultSet<Document>());
			}
			
			if((searchQuery.getProductCodifications() == null || searchQuery.getProductCodifications().size() == 0) && resultSet != null){
				List<Document> filteredResults = new ArrayList<Document>();
				for(Document result : resultSet.getResults()) {
					if(result.get("products") == null || (result.get("products") != null && !result.get("products").contains("1145"))){
						filteredResults.add(result);
					}
				}
				resultSet.setResults(filteredResults);
			} //Uncomment or comment this out to allow or disallow for Fruit Bouquet only shops.
			
			buildPaging(resultSet);
			user = (User)request.getSession().getAttribute("USER");
			commitSearchLog(resultSet, user);
			request.getSession(true).setAttribute("searchResults", resultSet);	
			request.setAttribute("searchResults", resultSet);
			
		}else{
			request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
			request.setAttribute("searchResults", new SearchResultSet<Document>());
		}
		if(fromBMS != null && fromBMS.equals("yes"))
			return "../pages/bmsresults";
		return "../pages/results";
	}
	
	@RequestMapping(value="/v1/SearchListingsByCityAndState")
	@Scope("request")
	public @ResponseBody List<JSONListingResponse> getListingsByCityAndState(HttpServletRequest request,
			@RequestParam(value="state", required=false) String state,
			@RequestParam(value="city", required=false) String city,
			@RequestParam(value="shopCode", required=false) String shopCode,
			@RequestParam(value="apiKey", required=false) String apiKey){
		
		List<JSONListingResponse> jsonList = new ArrayList<JSONListingResponse>();
		JSONListingResponse json = new JSONListingResponse();
		
		if(authorize(apiKey)){
		
			SearchQuery searchQuery = new SearchQuery(SearchQueryType.LISTINGS, SearchQuery.RETURN_TYPE_DOCUMENT);
			
			if(city != null && !city.equals("")) searchQuery.setCity(city.toUpperCase());
			else{
				json.setErrorMessage("No city was submitted. Please try again while submitting a valid city.");
				jsonList.add(json);
				return jsonList;
			}
			if(state != null && !state.equals("")) {
				populateStates();
				searchQuery.setStateCode(stateMap.get(state.toUpperCase()));
				searchQuery.setState(state.toUpperCase());
			}else{
				json.setErrorMessage("No state was submitted. Please try again while submitting a valid state.");
				jsonList.add(json);
				return jsonList;
			}
			 
			if(shopCode != null && !shopCode.equals(""))
				searchQuery.setShopCode(shopCode);
			
			searchQuery.setPage(1);
			searchQuery.setReturnType(4);
			searchQuery.setIsConsumerDirectory(true);
			
			SearchResultSet<Document> resultSet = searchService.search(searchQuery);
			
			if(resultSet != null && resultSet.getResultSize() != 0) {
				for(Document d : resultSet.getResults()){
					json = ConvertLuceneToJSON.convertListing(d);
					if(d.get("dont_show_cdo") == null || d.get("dont_show_cdo").equals("false")){
						jsonList.add(json);
					}
				}
				
			}else if(jsonList.size() == 0 || resultSet == null || resultSet.getResultSize() == 0){
				json.setErrorMessage("No results were found.");
				jsonList.add(json);
			}
			
		}else{
			json.setErrorMessage("Unauthorized attempt.");
			jsonList.add(json);
		}
		return jsonList;
	}
	
	@RequestMapping(value="/v1/SearchShopByShopCode")
	@Scope("request")
	public @ResponseBody List<JSONListingResponse> getShopByShopCode(HttpServletRequest request,
			@RequestParam(value="state", required=false) String state,
			@RequestParam(value="city", required=false) String city,
			@RequestParam(value="shopCode", required=false) String shopCode,
			@RequestParam(value="apiKey", required=false) String apiKey){
		
		List<JSONListingResponse> jsonList = new ArrayList<JSONListingResponse>();
		JSONListingResponse json = new JSONListingResponse();
		
		if(authorize(apiKey)){
		
			SearchQuery searchQuery = new SearchQuery(SearchQueryType.LISTINGS, SearchQuery.RETURN_TYPE_DOCUMENT);
			
			if(shopCode != null && !shopCode.equals("")) searchQuery.setShopCode(shopCode.toUpperCase());
			else{
				json.setErrorMessage("No shop code was submitted. Please try again while submitting a valid shop code.");
				jsonList.add(json);
				return jsonList;
			}
			
			searchQuery.setPage(1);
			searchQuery.setReturnType(4);
			searchQuery.setIsConsumerDirectory(true);
			
			SearchResultSet<Document> resultSet = searchService.search(searchQuery);
			
			if(resultSet != null && resultSet.getResultSize() != 0) {
				
				Document d = resultSet.getResult(0);
				json = ConvertLuceneToJSON.convertListing(d);
				jsonList.add(json);
				
			}else{
				json.setErrorMessage("No results were found.");
				jsonList.add(json);
			}
		}else{
			json.setErrorMessage("Unauthorized attempt.");
			jsonList.add(json);
		}
		return jsonList;
	}
	
	@RequestMapping(value="/v1/SearchFacilityListingsByCityAndState")
	@Scope("request")
	public @ResponseBody List<JSONFacilityListingResponse> getFacilityListingsByCityAndState(HttpServletRequest request,
			@RequestParam(value="state", required=false) String state,
			@RequestParam(value="city", required=false) String city,
			@RequestParam(value="shopCode", required=false) String shopCode,
			@RequestParam(value="facilityID", required=false) String facilityID,
			@RequestParam(value="apiKey", required=false) String apiKey){
		
		List<JSONFacilityListingResponse> jsonList = new ArrayList<JSONFacilityListingResponse>();
		JSONFacilityListingResponse json = new JSONFacilityListingResponse();
		
		if(authorize(apiKey)){
		
			SearchQuery searchQuery = new SearchQuery(SearchQueryType.LISTINGS, SearchQuery.RETURN_TYPE_DOCUMENT);
			
			if(facilityID != null && !facilityID.equals("")) searchQuery.setFacility(facilityID);
			else{
				json.setErrorMessage("No facility ID was submitted. Please try again while submitting a valid Facility ID.");
				jsonList.add(json);
				return jsonList;
			}
			
			if(city != null && !city.equals("")) searchQuery.setCity(city.toUpperCase());
			else{
				json.setErrorMessage("No city was submitted. Please try again while submitting a valid city.");
				jsonList.add(json);
				return jsonList;
			}
			if(state != null && !state.equals("")) {
				populateStates();
				searchQuery.setStateCode(stateMap.get(state.toUpperCase()));
				searchQuery.setState(state.toUpperCase());
			}else{
				json.setErrorMessage("No state was submitted. Please try again while submitting a valid state.");
				jsonList.add(json);
				return jsonList;
			}
			
			if(shopCode != null && !shopCode.equals(""))
				searchQuery.setShopCode(shopCode);
			
			searchQuery.setPage(1);
			searchQuery.setReturnType(4);
			searchQuery.setIsConsumerDirectory(true);
			
			SearchResultSet<Document> resultSet = searchService.search(searchQuery);
			
			if(resultSet != null && resultSet.getResultSize() != 0) {
				for(Document d : resultSet.getResults()){
					json = ConvertLuceneToJSON.convertFacilityListing(d);
					if(d.get("dont_show_cdo") == null || d.get("dont_show_cdo").equalsIgnoreCase("false")){
						jsonList.add(json);
					}
				}
				
			}else if(jsonList.size() == 0 || resultSet == null || resultSet.getResultSize() == 0){
				json.setErrorMessage("No results were found.");
				jsonList.add(json);
			}
			
		}else{
			json.setErrorMessage("Unauthorized attempt.");
			jsonList.add(json);
		}
		return jsonList;
	}
	
	@RequestMapping(value="/v1/SearchFacilitiesByCityAndState")
	@Scope("request")
	public @ResponseBody List<JSONFacilitiesResponse> getFacilitiesByCityAndState(HttpServletRequest request,
			@RequestParam(value="state", required=false) String state,
			@RequestParam(value="facilityTypeID", required=false) String facilityTypeID,
			@RequestParam(value="city", required=false) String city,
			@RequestParam(value="apiKey", required=false) String apiKey){
		
		List<JSONFacilitiesResponse> jsonList = new ArrayList<JSONFacilitiesResponse>();
		JSONFacilitiesResponse json = new JSONFacilitiesResponse();
		
		if(authorize(apiKey)){
		
			SearchQuery searchQuery = new SearchQuery(SearchQueryType.FACILITY, SearchQuery.RETURN_TYPE_DOCUMENT);
			
			if(city != null && !city.equals("")) searchQuery.setCity(city.toUpperCase());
			else{
				json.setErrorMessage("No city was submitted. Please try again while submitting a valid city.");
				jsonList.add(json);
				return jsonList;
			}
			
			if(state != null && !state.equals("")) searchQuery.setState(state.toUpperCase());
			else{
				json.setErrorMessage("No state was submitted. Please try again while submitting a valid state.");
				jsonList.add(json);
				return jsonList;
			}
			
			if(facilityTypeID != null && !facilityTypeID.equals("")) searchQuery.setFacilityTypeId(facilityTypeID);
			else{
				json.setErrorMessage("No facility type ID was submitted. Please try again while submitting a valid facility type ID.");
				jsonList.add(json);
				return jsonList;
			}
			
			SearchResultSet<Document> resultSet = searchService.search(searchQuery);
			
			if(resultSet != null && resultSet.getResultSize() != 0) {
				for(Document d : resultSet.getResults()){
					json = ConvertLuceneToJSON.convertFacilities(d);
					jsonList.add(json);
				}
				
			}else{
				json.setErrorMessage("No results were found.");
				jsonList.add(json);
			}
		}else{
			json.setErrorMessage("Unauthorized attempt.");
			jsonList.add(json);
		}
		return jsonList;
	}
	
	@RequestMapping(value="/v1/SearchFacilitiesByFacilityIDs")
	@Scope("request")
	public @ResponseBody List<JSONFacilitiesResponse> getFacilitiesByFacilityIDs(HttpServletRequest request,
			@RequestParam(value="facilityIDs", required=false) String facilityIDs,
			@RequestParam(value="apiKey", required=false) String apiKey){
		
		List<JSONFacilitiesResponse> jsonList = new ArrayList<JSONFacilitiesResponse>();
		JSONFacilitiesResponse json = new JSONFacilitiesResponse();
		
		if(authorize(apiKey)){
		
			SearchQuery searchQuery = new SearchQuery(SearchQueryType.FACILITY_ID, SearchQuery.RETURN_TYPE_DOCUMENT);
			
			if(facilityIDs != null && !facilityIDs.equals("")){
				
				String[] facilityIDList = facilityIDs.split(",");
				
				for(int ii=0; ii<facilityIDList.length; ++ii){
					
					searchQuery.setFacility(facilityIDList[ii]);
					
					SearchResultSet<Document> resultSet = searchService.search(searchQuery);
					
					if(resultSet != null && resultSet.getResultSize() != 0) {
						for(Document d : resultSet.getResults()){
							json = ConvertLuceneToJSON.convertFacilities(d);
							jsonList.add(json);
						}
						
					}
				}
			}
			else{
				json.setErrorMessage("No facility IDs were submitted. Please try again while submitting at least one valid facility ID.");
				jsonList.add(json);
				return jsonList;
			}
			
			if(jsonList.size() < 1){
				json.setErrorMessage("No results were found.");
				jsonList.add(json);
			}
		}else{
			json.setErrorMessage("Unauthorized attempt.");
			jsonList.add(json);
		}
		return jsonList;
	}
	
	@RequestMapping(value="/v1/SearchStatesByCountry")
	@Scope("request")
	public @ResponseBody List<JSONStatesResponse> getStatesByCountry(HttpServletRequest request,
			@RequestParam(value="country", required=false) String country,
			@RequestParam(value="apiKey", required=false) String apiKey){
		
		List<JSONStatesResponse> jsonList = new ArrayList<JSONStatesResponse>();
		JSONStatesResponse json = new JSONStatesResponse();
		
		if(authorize(apiKey)){
			
			
			if(country == null || country.equals("")){ 
				json.setErrorMessage("No country was submitted. Please try again while submitting a valid country.");
				jsonList.add(json);
				return jsonList;
			}
			
			String countryID = "";
			if(country.equalsIgnoreCase("USA") || country.equalsIgnoreCase("US") || country.equalsIgnoreCase("United States") || country.equalsIgnoreCase("United States of America"))
				countryID = "1";
			else if(country.equalsIgnoreCase("CAN") || country.equalsIgnoreCase("Canada"))
				countryID = "2";
			else if(country.equalsIgnoreCase("PR") || country.equalsIgnoreCase("PUR") || country.equalsIgnoreCase("Puerto Rico"))
				countryID = "3";
		
			SearchQuery searchQuery = new SearchQuery(SearchQueryType.STATEBYCOUNTRY, SearchQuery.RETURN_TYPE_DOCUMENT);
			searchQuery.setCountryId(countryID);
			
			SearchResultSet<Document> resultSet = searchService.search(searchQuery);
			
			if(resultSet != null && resultSet.getResultSize() != 0) {
				for(Document d : resultSet.getResults()){
					json = ConvertLuceneToJSON.convertStates(d);
					jsonList.add(json);
				}
				
			}else{
				json.setErrorMessage("No results were found.");
				jsonList.add(json);
			}
		}else{
			json.setErrorMessage("Unauthorized attempt.");
			jsonList.add(json);
		}
		return jsonList;
	}
	
	@RequestMapping(value="/v1/SearchCitiesByState")
	@Scope("request")
	public @ResponseBody List<JSONCitiesResponse> getCitiesByState(HttpServletRequest request,
			@RequestParam(value="state", required=false) String state,
			@RequestParam(value="apiKey", required=false) String apiKey){
		
		List<JSONCitiesResponse> jsonList = new ArrayList<JSONCitiesResponse>();
		JSONCitiesResponse json = new JSONCitiesResponse();
		
		if(authorize(apiKey)){
			
			if(state == null || state.equals("")){ 
				json.setErrorMessage("No state was submitted. Please try again while submitting a valid state.");
				jsonList.add(json);
				return jsonList;
			}
		
			SearchQuery searchQuery = new SearchQuery(SearchQueryType.CITIESSTATENAME, SearchQuery.RETURN_TYPE_DOCUMENT);
			searchQuery.setState(state.toUpperCase());
			
			SearchResultSet<Document> resultSet = searchService.search(searchQuery);
			
			if(resultSet != null && resultSet.getResultSize() != 0) {
				Map<String, String> citiesSeen = new HashMap<String, String>();
				for(Document d : resultSet.getResults()){
					if(citiesSeen.get(d.get("name").toUpperCase().trim()) == null){
						json = ConvertLuceneToJSON.convertCities(d);
						jsonList.add(json);
						citiesSeen.put(d.get("name").toUpperCase().trim(), "1");
					}
				}
				
			}else{
				json.setErrorMessage("No results were found.");
				jsonList.add(json);
			}
		}else{
			json.setErrorMessage("Unauthorized attempt.");
			jsonList.add(json);
		}
		return jsonList;
	}
	
	@RequestMapping(value="/v1/SearchCityAndStateByZip")
	@Scope("request")
	public @ResponseBody List<JSONCitiesZipResponse> getCityAndStateByZip(HttpServletRequest request,
			@RequestParam(value="zip", required=false) String zip,
			@RequestParam(value="apiKey", required=false) String apiKey){
		
		List<JSONCitiesZipResponse> jsonList = new ArrayList<JSONCitiesZipResponse>();
		JSONCitiesZipResponse json = new JSONCitiesZipResponse();
		
		if(authorize(apiKey)){
			
			if(zip == null || zip.equals("")){ 
				json.setErrorMessage("No zip code was submitted. Please try again while submitting a valid zip code.");
				jsonList.add(json);
				return jsonList;
			}
		
			SearchQuery searchQuery = new SearchQuery(SearchQueryType.CITY, SearchQuery.RETURN_TYPE_DOCUMENT);
			searchQuery.setZip(zip.toUpperCase());
			
			SearchResultSet<Document> resultSet = searchService.search(searchQuery);
			
			if(resultSet != null && resultSet.getResultSize() != 0) {
				for(Document d : resultSet.getResults()){
					json = ConvertLuceneToJSON.convertZipCities(d);
					jsonList.add(json);
				}
				
			}else{
				json.setErrorMessage("No results were found.");
				jsonList.add(json);
			}
		}else{
			json.setErrorMessage("Unauthorized attempt.");
			jsonList.add(json);
		}
		return jsonList;
	}
	
	@RequestMapping(value="/v1/SearchCountries")
	@Scope("request")
	public @ResponseBody List<JSONCountriesResponse> getCountries(HttpServletRequest request,
			@RequestParam(value="apiKey", required=false) String apiKey){
		
		List<JSONCountriesResponse> jsonList = new ArrayList<JSONCountriesResponse>();
		JSONCountriesResponse json = new JSONCountriesResponse();
		
		if(authorize(apiKey)){
		
			SearchQuery searchQuery = new SearchQuery(SearchQueryType.COUNTRIES, SearchQuery.RETURN_TYPE_DOCUMENT);
			
			SearchResultSet<Document> resultSet = searchService.search(searchQuery);
			
			if(resultSet != null && resultSet.getResultSize() != 0) {
				List<String> seenCountries = new ArrayList<String>();
				for(Document d : resultSet.getResults()){
					if(!seenCountries.contains(d.get("country.name"))){
						json = ConvertLuceneToJSON.convertCountries(d);
						jsonList.add(json);
						seenCountries.add(d.get("country.name"));
					}
				}
				
			}else{
				json.setErrorMessage("No results were found.");
				jsonList.add(json);
			}
		}else{
			json.setErrorMessage("Unauthorized attempt.");
			jsonList.add(json);
		}
		return jsonList;
	}
	
	@RequestMapping(value="/v1/SearchLandingAds")
	@Scope("request")
	public @ResponseBody List<JSONLandingAdsResponse> getLandingAds(HttpServletRequest request,
			@RequestParam(value="apiKey", required=false) String apiKey){
		
		List<JSONLandingAdsResponse> jsonList = new ArrayList<JSONLandingAdsResponse>();
		JSONLandingAdsResponse json = new JSONLandingAdsResponse();
		
		if(authorize(apiKey)){
		
			SearchQuery searchQuery = new SearchQuery(SearchQueryType.CONSUMERLANDINGADS, SearchQuery.RETURN_TYPE_DOCUMENT);
			
			SearchResultSet<Document> resultSet = searchService.search(searchQuery);
			
			if(resultSet != null && resultSet.getResultSize() != 0) { 
				for(Document d : resultSet.getResults()){
					json = ConvertLuceneToJSON.convertLandingAds(d);
					jsonList.add(json);
				}
				
			}else{
				json.setErrorMessage("No results were found.");
				jsonList.add(json);
			}
		}else{
			json.setErrorMessage("Unauthorized attempt.");
			jsonList.add(json);
		}
		return jsonList;
	}
	
	@RequestMapping(value="/v1/SearchResultsAds")
	@Scope("request")
	public @ResponseBody List<JSONResultsAdsResponse> getResultsAds(HttpServletRequest request,
			@RequestParam(value="apiKey", required=false) String apiKey,
			@RequestParam(value="city", required=false) String city,
			@RequestParam(value="state", required=false) String state){
		
		List<JSONResultsAdsResponse> jsonList = new ArrayList<JSONResultsAdsResponse>();
		JSONResultsAdsResponse json = new JSONResultsAdsResponse();
		
		if(authorize(apiKey)){
			
			if(city == null || city.equals("")){ 
				json.setErrorMessage("No city was submitted. Please try again while submitting a valid city.");
				jsonList.add(json);
				return jsonList;
			}
			
			if(state == null || state.equals("")){ 
				json.setErrorMessage("No state was submitted. Please try again while submitting a valid state.");
				jsonList.add(json);
				return jsonList;
			}
		
			SearchQuery searchQuery = new SearchQuery(SearchQueryType.CONSUMERRESULTSADS, SearchQuery.RETURN_TYPE_DOCUMENT);
			searchQuery.setCity(city);
			searchQuery.setState(state);
			
			SearchResultSet<Document> resultSet = searchService.search(searchQuery);
			
			if(resultSet != null && resultSet.getResultSize() != 0) { 
				for(Document d : resultSet.getResults()){
					json = ConvertLuceneToJSON.convertResultsAds(d);
					jsonList.add(json);
				}
				
			}else{
				FileInputStream fos;
				try {
					fos = new FileInputStream("/opt/apps/properties/directory.properties");
					Properties props = new Properties();
					props.load(fos);
					json.setFileName("ResultsAd.jpg");
					json.setWebsite(props.getProperty("results_site"));
					jsonList.add(json);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}else{
			json.setErrorMessage("Unauthorized attempt.");
			jsonList.add(json);
		}
		return jsonList;
	}
	
	@RequestMapping(value="/v1/GetBloomLinkMembershipHours")
	@Scope("request")
	public @ResponseBody String getBloomLinkMembershipHours(HttpServletRequest request,
			@RequestParam(value="apiKey", required=false) String apiKey,
			@RequestParam(value="city", required=false) final String city,
			@RequestParam(value="date", required=false) final String date,
			@RequestParam(value="state", required=false) final String state) throws IOException, JDOMException{
		
		final ResultsXML results = new ResultsXML();
				
		if(authorize(apiKey)){
			
			if(date == null || date.equals("")){
				return "No date was submitted. Please try again while submitting a valid date.";
			}
			if(city == null || city.equals("") || state == null || state.equals("")){ 
				return "No city/state combo was submitted. Please try again while submitting a valid city/state combo.";
			}
			
			final ShopAvailabilityServiceImpl availabilityService = new ShopAvailabilityServiceImpl();
	
			try {
				new Thread(new Runnable() {
				     @Override
				     public void run() {
				    	 try {
							results.setResultsXML(availabilityService.searchShopHoursByCityStateZip(date, city, state));
						} catch (IOException e) {
							e.printStackTrace();
						} catch (JDOMException e) {
							e.printStackTrace();
						}
				     }
				}).start();
				
				long start = System.currentTimeMillis();
				long end = start + 5000; // 5 seconds
				boolean cont = true;
				while (cont && System.currentTimeMillis() < end){
					System.clearProperty("dummy");
				    if(results.getResultsXML() != null && !results.getResultsXML().equals(""))
				    	cont = false;
				}
				
			} catch(Exception e){
				e.printStackTrace();
				results.setResultsXML("Bloomlink Error.");
			}
			
				
			List<String> shopCodesSearched = new ArrayList<String>();
			
			SearchQuery searchQuery = new SearchQuery(SearchQueryType.LISTINGS, SearchQuery.RETURN_TYPE_DOCUMENT);
			searchQuery.setCity(city.toUpperCase());
			populateStatesReverse();
			searchQuery.setStateCode(state.toUpperCase());
			searchQuery.setState(reverseStateMap.get(state.toUpperCase()).toUpperCase());
			searchQuery.setPage(1);
			searchQuery.setReturnType(4);
			
			SearchResultSet<Document> resultSet = searchService.search(searchQuery);
			
			String openingXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><memberDirectoryInterface><searchShopResponse><errors/><shops>";
			String closingXML = "</shops></searchShopResponse></memberDirectoryInterface>";
			
			String middleXML = "";
			
			if(results.getResultsXML() != null && !results.getResultsXML().equals("") && !results.getResultsXML().equals("Bloomlink Error.")){
				try{
					middleXML = results.getResultsXML().split("<shops>")[1].split("</shops>")[0];
				}catch(Exception ee){}
			}
			
			String memberDirectoryXMLString = "";
			
			StringBuilder sb = new StringBuilder();
			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader("/root/memberDirectory.xml"));
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
			String sCurrentLine = "";
	        try {
				while ((sCurrentLine = br.readLine()) != null) {
				    sb.append(sCurrentLine);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}finally{
				br.close();
			}
	        
			memberDirectoryXMLString = sb.toString();
			
			if(resultSet != null && resultSet.getResultSize() != 0) {
				for(Document d : resultSet.getResults()){
					String shopCode = d.get("shopcode");
					if(!shopCodesSearched.contains(shopCode)){
						shopCodesSearched.add(shopCode);
						if(!middleXML.contains(shopCode) && memberDirectoryXMLString.contains(shopCode)){
							String mdString = memberDirectoryXMLString.split(shopCode)[1].split("</shop>")[0];
							if(mdString.split("operationalHours").length > 1)
								middleXML += "<shop><shopCode>" + shopCode + mdString + "</shop>";
						}/*else if(!middleXML.contains(shopCode)){
							results.setResultsXML(availabilityService.searchHoursByShopCode(shopCode));
							if(results.getResultsXML() != null && !results.getResultsXML().equals("")){
								try{
									middleXML += results.getResultsXML().split("<shops>")[1].split("</shops>")[0];
								}catch(Exception ee){}
							}
						}*/ //Uncomment this out to allow for searching for individual shops that aren't found using the above logic of a searchAvailability call to Bloomlink and a search on the raw daily Member Directory dump.
						//The risk is that if Bloomlink ever goes down we'll have to wait for each of these responses to time out, OR add logic to only wait x amount of seconds for each call. Ideally the maximum amount of time we wait would only be 1 second
						// but even then a response CAN sometimes take longer than 1 second, plus if there's, say, 3 shops that aren't found, then we would be waiting a minimum of 3 extra seconds for the overall response which is not ideal. Thereby, use at your own risk.
					}
				}
				return openingXML + middleXML + closingXML;
			}
			
		}else{
			return "Unauthorized attempt.";
		}
		return results.getResultsXML();
	}
	
	@RequestMapping(value="/v1/GetBloomLinkMembershipHoursByShopCode")
	@Scope("request")
	public @ResponseBody String getBloomLinkMembershipHoursByShopCode(HttpServletRequest request,
			@RequestParam(value="apiKey", required=false) String apiKey,
			@RequestParam(value="shopCode", required=false) String shopCode){
		
		String resultXML = "";
				
		if(authorize(apiKey)){
			
			ShopAvailabilityServiceImpl availabilityService = new ShopAvailabilityServiceImpl();
			try {
				return  availabilityService.searchHoursByShopCode(shopCode);
			} catch (IOException e) {
				e.printStackTrace();
				resultXML = "Bloomlink Error.";
			} catch (JDOMException e) {
				e.printStackTrace();
				resultXML = "Bloomlink Error.";
			} catch(Exception e){
				e.printStackTrace();
				resultXML = "Bloomlink Error.";
			}
		}else{
			return "Unauthorized attempt.";
		}
		return resultXML;
	}
	
	@Transactional
	@RequestMapping(value="advancesearch", method=RequestMethod.GET)
	public String getAdvancedSearchResults(HttpServletRequest request,
	@RequestParam(value="shopcode", required=false) String shopcode,
	@RequestParam(value="shopphone", required=false) String shopphone,
	@RequestParam(value="shopname", required=false) String shopname,
	@RequestParam(value="deliverydate", required=false) String deliverydate,
	@RequestParam(value="fromBMS", required=false) String fromBMS){
		
		String nextPage = "";
		if(fromBMS != null && fromBMS.equals("yes"))
			nextPage = "../pages/bmsresults";
		else
			nextPage="../pages/results";
		request.getSession(true).setAttribute("existingCoverageNoZips", "NO");	
		request.getSession(true).setAttribute("noSpecificResults", "");
		populateStates();
		Map<String, Document> productDocs = searchService.fetchProducts();
		request.getSession(true).setAttribute("productsMap", productDocs);
		Map<String, Document> categoryDocs = searchService.fetchCategories();
		request.getSession(true).setAttribute("categoriesMap", categoryDocs);
		SearchResultSet<Document> resultSet = null;
		int index=0;

		
		SearchQuery searchQuery = new SearchQuery(SearchQueryType.LISTINGS, SearchQuery.RETURN_TYPE_DOCUMENT);
		String formatDate = "";
		if(deliverydate != null && !deliverydate.equals(""))
			formatDate = DateUtil.formatDateString(deliverydate);
		searchQuery.setDate(formatDate);
		if (!shopcode.equals("Shop Code")){
			shopcode = shopcode.toUpperCase();
			if(shopcode.length() == 4)shopcode += "0000";
			searchQuery.setShopCode(shopcode);
			resultSet = advanceSearch(request, searchQuery);

		}
		else if (!shopphone.equals("Phone number")){
			shopphone = shopphone.replaceAll(" ", "").replaceAll("\\+", "").replaceAll("[()]","").replaceAll("-","");
			if(shopphone.length() == 10)
				shopphone = shopphone.substring(0,3)+"-"+shopphone.substring(3,6)+"-"+shopphone.substring(6,10);
			searchQuery.setPhone(shopphone);
			resultSet = advanceSearch(request, searchQuery);

		}
		else if (!shopname.equals("Shop Name")){
			shopname = shopname.replaceAll("\\+"," ");
			List<Shop> shops = orderDAO.getShopsByShopName(shopname);
			for (Shop shop: shops){
			searchQuery.setShopCode(shop.getShopCode());
			SearchResultSet<Document> aResultSet = advanceSearch(request, searchQuery);
			if (index==0){
				resultSet = aResultSet;
			}
			else{
				for (Document result:aResultSet.getResults() ){
					resultSet.getResults().add(result);
				}
	
			}
			index++;
			}

		}
		
		if(resultSet != null && resultSet.getResults().size() > 0) {
			User user = (User)request.getSession().getAttribute("BLOOMNET_USER");
			if(user != null) resultSet.setUsername(user.getShopCode());
			resultSet.setPageNumber(0);
			buildPaging(resultSet);
			logger.info("SEARCH RESULTSET: " + resultSet);
		

			request.getSession(true).setAttribute("searchResults", resultSet);
			
			user = (User)request.getSession().getAttribute("USER");
			commitSearchLog(resultSet, user);
			
			
		}else{
			request.getSession(true).setAttribute("searchResults", resultSet);
		}

		
		return nextPage;
		
	}
	
	@RequestMapping(value="/uploadFile.htm", method=RequestMethod.POST)
	public String uploadFileHandler(HttpServletRequest request,
			@RequestParam("file") MultipartFile file) {
		if (!file.isEmpty()) {
			int count = 0;
			BufferedWriter writer = null;
			try {

				String endPoint = directoryProperties.getProperty("address");
				String myFunc = "postmessages";
				
				File dir = new File("/var/lib/tomcat/webapps/Directory/files");
				if (!dir.exists())
					dir.mkdirs();
				
				String sendingShopCode = request.getSession(true).getAttribute("uname")+"0000";
			    String receivingShopCode = "Z9980000";
			    String captcha = (String) request.getSession(true).getAttribute("captcha");
			    String userName = sendingShopCode.substring(0,4);
		         
		         BufferedReader br = new BufferedReader(new InputStreamReader(file.getInputStream()));
		         String line = "";
		         
		 		try {
		 			writer = new BufferedWriter(new FileWriter("/var/lib/tomcat/webapps/Directory/files/"+file.getOriginalFilename()));
		 		} catch (IOException e1) {
		 			e1.printStackTrace();
		 		}
		         
		         while((line = br.readLine()) != null) {
		            if(count==0) {
		            	count++;
		            	writer.write(line+",\"Order Number/Error\"\r\n");
		            	continue;
		            }
		            count++;
		            String[] lineItems = line.split("\",\"");
		            
		            String pattern = "yyyyMMddHHmmss";
			        DateFormat df = new SimpleDateFormat(pattern);
			        Date now = new Date();
			        
			        writer.write(line+",\"");
		            
				    String timeStamp = df.format(now);
				    String firstName = scrubBadData(lineItems[0].replaceAll("\"", ""));
				    String lastName = scrubBadData(lineItems[1]);
				    String address1 = scrubBadData(lineItems[2]);
				    String address2 = scrubBadData(lineItems[3]);
				    String city = scrubBadData(lineItems[4]);
				    String state = scrubBadData(lineItems[5]);
				    String zip = scrubBadData(lineItems[6]);
				    String country = scrubBadData(lineItems[7]);
				    String phone = scrubBadData(lineItems[8]);
				    String occasion = scrubBadData(lineItems[9]);
				    String cardMessage = scrubBadData(lineItems[10]);
				    String specialInstructions = scrubBadData(lineItems[11]);
				    String containsPerishables = scrubBadData(lineItems[12].toUpperCase());
				    String deliveryDate = scrubBadData(lineItems[13]);
				    String recipientAttention = scrubBadData(lineItems[14]);
				    String[] products = lineItems[15].split("\\[");
				    
				    String occasionCode = "";
				    if(occasion.equalsIgnoreCase("Funeral"))
				    	occasionCode = "1";
				    else if(occasion.equalsIgnoreCase("Illness"))
				    	occasionCode = "2";
				    else if(occasion.equalsIgnoreCase("Birthday"))
				    	occasionCode = "3";
				    else if(occasion.equalsIgnoreCase("BusinessGifts"))
				    	occasionCode = "4";
				    else if(occasion.equalsIgnoreCase("Holiday"))
				    	occasionCode = "5";
				    else if(occasion.equalsIgnoreCase("Maternity"))
				    	occasionCode = "6";
				    else if(occasion.equalsIgnoreCase("Anniversary"))
				    	occasionCode = "7";
				    else if(occasion.equalsIgnoreCase("Other"))
				    	occasionCode = "8";
	
					String myXMLString = "";		
					Double runningTotal = 0.00;
					String myXMLProductsString = "";
					
					for(int ii=1; ii<products.length; ++ii) {
						String[] productItems = products[ii].split("','");
						
						String units = scrubBadData(productItems[0].replaceAll("'", ""));
						String costOfProduct = scrubBadData(productItems[1]);
						String productDescription = scrubBadData(productItems[2]);
						String productCode = scrubBadData(productItems[3].replaceAll("'\\]\"","").replaceAll("'\\]","").replaceAll(",", ""));
						
						runningTotal += (Double.valueOf(units)*Double.valueOf(costOfProduct));
						
						myXMLProductsString += "<orderProductInfoDetails><units>"+units+"</units><costOfSingleProduct>"+costOfProduct+"</costOfSingleProduct><productDescription>"+productDescription+"</productDescription><productCode>"+productCode+"</productCode></orderProductInfoDetails>";
					}
					
					myXMLString = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><foreignSystemInterface><security><username>"+userName+"</username><password>"+captcha+"</password><shopCode>"+sendingShopCode+"</shopCode></security><errors /><messagesOnOrder><messageCount>1</messageCount><messageOrder><messageType>0</messageType><sendingShopCode>"+sendingShopCode+"</sendingShopCode><receivingShopCode>"+receivingShopCode+"</receivingShopCode><fulfillingShopCode>"+receivingShopCode+"</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber /><bmtSeqNumberOfOrder /><bmtSeqNumberOfMessage /><externalShopOrderNumber>10004</externalShopOrderNumber><externalShopMessageNumber /></generalIdentifiers></identifiers><messageCreateTimestamp>"+timeStamp+"</messageCreateTimestamp><orderDetails><orderNumber /><occasionCode>"+occasionCode+"</occasionCode><totalCostOfMerchandise>"+String.valueOf(runningTotal)+"</totalCostOfMerchandise><orderProductsInfo>" + myXMLProductsString + "</orderProductsInfo><orderCardMessage>"+cardMessage+"</orderCardMessage><orderType>1</orderType><orderTypeMessage>Directory Online Order</orderTypeMessage></orderDetails><orderCaptureDate>"+timeStamp+"</orderCaptureDate><deliveryDetails><deliveryDate>"+deliveryDate+"</deliveryDate><specialInstruction>"+specialInstructions+"</specialInstruction></deliveryDetails><recipient><recipientFirstName>"+firstName+"</recipientFirstName><recipientLastName>"+lastName+"</recipientLastName><recipientAttention>"+recipientAttention+"</recipientAttention><recipientAddress1>"+address1+"</recipientAddress1><recipientAddress2>"+address2+"</recipientAddress2><recipientCity>"+city+"</recipientCity><recipientState>"+state+"</recipientState><recipientZipCode>"+zip+"</recipientZipCode><recipientCountryCode>"+country+"</recipientCountryCode><recipientPhoneNumber>"+phone+"</recipientPhoneNumber></recipient><wireServiceCode>BMT</wireServiceCode><shippingDetails><trackingNumber /><shipperCode /><shippingMethod /><shippingDate /></shippingDetails><containsPerishables>"+containsPerishables+"</containsPerishables></messageOrder></messagesOnOrder></foreignSystemInterface>";
				    
				    PostMethod myPost = new PostMethod(endPoint);

				    //myXMLString = URLEncoder.encode(myXMLString, StandardCharsets.UTF_8.name()).replaceAll("\\+", "%20");
					
					myPost.addParameter("func", myFunc);
					myPost.addParameter("data", myXMLString);
					
					HttpClient myClient = new HttpClient();
				
					try {
						myClient.executeMethod(myPost);
						String response = myPost.getResponseBodyAsString();
						if(response.contains("bmtOrderNumber")) {
							String orderNumber = response.split("<bmtOrderNumber>")[1].split("</bmtOrderNumber>")[0];
							if(!orderNumber.equals("")){
								writer.write(orderNumber+"\"\r\n");
								writer.flush();
							}else {
								String errorMessage = response.split("<errorMessage>")[1].split("</errorMessage>")[0];
								writer.write(errorMessage + "\"\r\n");
								writer.flush();
							}
						}else {
							String errorMessage = response.split("<errorMessage>")[1].split("</errorMessage>")[0];
							writer.write(errorMessage + "\"\r\n");
							writer.flush();
						}
						
					}catch(Exception ee){}	
		            
		         }
		         br.close();
		         writer.flush();
		         writer.close();

				return "redirect:files/"+file.getOriginalFilename();
			} catch (Exception e) {
				if(count>1) {
					try {
						writer.write("An error occured parsing this line. Please ensure that all formatting requirements have been fulfilled and try again." + "\"\r\n");
						writer.flush();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					return "redirect:files/"+file.getOriginalFilename();
				}
				else
					return "redirect:files/fail.csv";
			}finally {
				if(writer != null)
					try {
						writer.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
		} else {
			return "redirect:files/fail.csv";
		}
	}
	
	private String scrubBadData(String input) {
		
		return input.replaceAll("&", "&amp;").replaceAll("<", "").replaceAll(">", "").replaceAll("\\+"," ");
	}


	private static String bytesToHex(byte[] bytes) {
	    StringBuilder hexStringBuilder = new StringBuilder();
	  
	    for (byte b : bytes) {
	        hexStringBuilder.append(String.format("%01X ", b));
	    }
	    return hexStringBuilder.toString();
	}
	
	private SearchResultSet<Document> advanceSearch(HttpServletRequest request, SearchQuery searchQuery) {

		searchQuery.setPage(1);
		searchQuery.setReturnType(4);
		logger.info("SEARCH QUERY: " + searchQuery.toString());		

		SearchResultSet<Document> resultSet = searchService.search(searchQuery);
		
		return resultSet;
		
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}
	public SessionFactory getSessionFactory() {
		return hibernateTemplate.getSessionFactory();
	}
	
	private boolean authorize(String apiKey){

    	MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-512");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		String salt = "h7M).hL9B6hjlScXz!a0Av$()77ScXzZ"; //hard coded here to prevent discovery in database or files system
		
		
		if(apiKey == null){
    		return false;
    	}else{
    		StringBuffer sb = new StringBuffer();
			md.update((apiKey+salt).getBytes());
			for (int xx=0; xx<10000; ++xx){
				 md.update(sb.toString().getBytes());
			     byte byteData[] = md.digest();
		        //convert the byte to hex format
		        sb = new StringBuffer();
		        for (int i = 0; i < byteData.length; i++) {
		        	sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		        }
			}
			
			if(sb.toString().equals(encryptedAPIKey)){
				return true;
			}else{
				return false;
			}
    	}
	}
	
	@RequestMapping(value="/route4MeIntake.htm", method=RequestMethod.POST)
	public String route4MeIntakeHandler(HttpServletRequest request,
			@RequestParam("email") String email,
			@RequestParam("password") String password,
			@RequestParam("passwordRepeated") String passwordRepeated,
			@RequestParam("timezone") String timezone,
			@RequestParam("shopName") String shopName,
			@RequestParam("contact") String contact) {
		
		if(email != null && !email.trim().equals("") && password != null && !password.trim().equals("") && passwordRepeated != null && !passwordRepeated.trim().equals("") && timezone != null && !timezone.trim().equals("")
				&& shopName != null && !shopName.trim().equals("") && contact != null && !contact.trim().equals("")) {
		
			if(request.getSession(true).getAttribute("alreadySubmitted") != null && request.getSession(true).getAttribute("alreadySubmitted").equals("true")) {
				request.getSession(true).setAttribute("failMessage", "This data has already been submitted. If you've forgotten your Route4Me password please reset it on the Route4Me Home Page.");
				return "route4MeForm";
			}
			
			if(password.equals(passwordRepeated)) {
				
				List<Route4MeIntake> existing = orderDAO.getRoute4MeByEmail(email);
				if(existing == null || existing.size() == 0) {
				
					User user = (User)request.getSession(true).getAttribute("USER");
					String secretPassword = orderDAO.getExistingSecretPasswordByShopCode(user.getShopCode()); 
					if(secretPassword == null || secretPassword.equals("")) secretPassword = new GenerateBloomlinkPassword().generateBloomlinkPassword(8);
					
					Route4MeIntake r4Me = new Route4MeIntake();
					r4Me.setBloomlinkProcessed(0);
					r4Me.setRoute4MeProcessed(0);
					r4Me.setEmail(email);
					r4Me.setPassword(password);
					r4Me.setContact(contact);
					r4Me.setTimezone(timezone);
					r4Me.setShopCode(user.getShopCode());
					r4Me.setSecretPassword(secretPassword);
					r4Me.setShopName(shopName);
					r4Me.setAttempts(0);
					r4Me.setCreatedDate(DateUtil.toXmlFormatString(new Date()));
					
					orderDAO.saveRoute4Me(r4Me);
					
					request.getSession(true).setAttribute("successMessage", "You have Successfully Submitted your information. Please wait two minutes before trying to access your new account.");
					request.getSession(true).setAttribute("isR4Me", null);
					request.getSession(true).setAttribute("alreadySubmitted", "true");
				}else {
					request.getSession(true).setAttribute("failMessage", "This data has already been submitted. If you've forgotten your Route4Me password please reset it on the Route4Me Home Page.");
					request.getSession(true).setAttribute("formEmail", email);
					request.getSession(true).setAttribute("formTimezone", timezone);
					request.getSession(true).setAttribute("formContact", contact);
					request.getSession(true).setAttribute("formShopName", shopName);
				}
			}else {
				request.getSession(true).setAttribute("failMessage", "Your entered password does not match the repeated password. Please try again.");
				request.getSession(true).setAttribute("formEmail", email);
				request.getSession(true).setAttribute("formTimezone", timezone);
				request.getSession(true).setAttribute("formContact", contact);
				request.getSession(true).setAttribute("formShopName", shopName);
			}
		}else {
			request.getSession(true).setAttribute("failMessage", "One or more fields were not filled out. All fields are required. Please try again.");
			request.getSession(true).setAttribute("formEmail", email);
			request.getSession(true).setAttribute("formTimezone", timezone);
			request.getSession(true).setAttribute("formContact", contact);
			request.getSession(true).setAttribute("formShopName", shopName);
		}
		
		return "route4MeForm";
		
	}
}
