package com.flowers.portal.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.lucene.document.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.flowers.portal.dao.OrderDAO;
import com.flowers.portal.service.ControllerService;
import com.flowers.portal.service.PortalService;
import com.flowers.portal.service.SessionManager;
import com.flowers.portal.util.DateUtil;
import com.flowers.portal.util.Money;
import com.flowers.portal.vo.DescribedProduct;
import com.flowers.portal.vo.Order;
import com.flowers.portal.vo.Recipient;
import com.flowers.portal.vo.RestResponse;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.User;
import com.flowers.server.search.SearchResultSet;

@Controller
@RequestMapping("/contemporary.htm")
public class ContemporaryDirectoryController {

	Properties props = new Properties();
	@Autowired private SessionManager sessionManager;
	@Autowired	private Properties directoryProperties;
	@Autowired	private PortalService portal;
	@Autowired	private ControllerService controllerService;
	@Autowired private OrderDAO orderDAO;
	
	
	@RequestMapping(value = "layout.xml", method = RequestMethod.GET)
	public String index(HttpServletRequest request,
			HttpServletResponse response, Model model) {
		
		response.setHeader("Cache-Control", "no-cache");
		return "";

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.GET)
	public String processGetRequest(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) throws IOException {

		String returnValue = null;

		Long timestamp = (request.getParameter("timestamp") != null && request
				.getParameter("timestamp").length() > 0) ? Long.valueOf(request
				.getParameter("timestamp")) : 0;

		User user = (User) sessionManager.getApplicationUser(request);
		if(user == null) {
			return "redirect:signin.htm";
		}
		request.setAttribute("user_email", user.getEmail());
		request.setAttribute("user_name", user.getShopCode());
		request.setAttribute("pref", user.getViewPreference());

		String view = request.getParameter("view");
		String from = request.getParameter("from");

		if(request.getSession(true).getAttribute("isR4Me") != null && request.getSession(true).getAttribute("isR4Me").equals("true"))
			view = "r4me";
		
		if (view == null) {

			returnValue = handleOrderView(request);
			
		}else if(view.equals("dropship")){
			Order order = (Order) request.getSession(true).getAttribute("BLOOMNET_ORDER");
			if(order != null && order.getRecipient() != null){
				request.setAttribute("order", order);
				request.setAttribute("states", portal.getStatesOptions(order.getRecipient().getState(),order.getRecipient().getCountryId()));
				request.setAttribute("bannerAds", portal.getBannerAds());
				request.setAttribute("sideAds", portal.getSideAds());
				request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
				request.setAttribute("countries", portal.getCountryOptions(order.getRecipient().getCountryId()));
			}else{
				request.setAttribute("states", portal.getStatesOptions("",directoryProperties.getProperty("USACountryId")));
				request.setAttribute("bannerAds", portal.getBannerAds());
				request.setAttribute("sideAds", portal.getSideAds());
				request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
				request.setAttribute("countries", portal.getCountryOptions(directoryProperties.getProperty("USACountryId")));
			}
			request.setAttribute("dropship", portal.getDropship());
			request.setAttribute("products", portal.getProducts());
			returnValue = "dropshipOrder";
		}else if (view.equals("summary")) {
			
			//This does not seem to be executed
			returnValue = handleSummaryView(request, from, timestamp);
			
		} else if (view.equals("searchResults")) {
			
			returnValue = handleSearchResultsView(request, from);
			
			if (from != null && (from.equals("search") || from.equals("bmsSearch"))) {
				SearchResultSet<Document> searchResults = (SearchResultSet<Document>) request
						.getSession(true).getAttribute("searchResults");
				String existingCoverageNoZips = (String) request.getSession(true).getAttribute("existingCoverageNoZips");
				request.setAttribute("existingCoverageNoZips", existingCoverageNoZips);
				Map<String, Document> productsMap = (Map<String, Document>) request
						.getSession(true).getAttribute("productsMap");
				Map<String, Document> catagoriesMap = (Map<String, Document>) request
						.getSession(true).getAttribute("categoriesMap");
				Order order = (Order) sessionManager.getOrder(request);
				
				int pageNumber = 0;
				List<Document> currPage = null;
				String searchQueryDate = "";
				if (searchResults != null) {
					pageNumber = searchResults.getPageNumber();
					currPage = (searchResults.getPages().size() > 0) ? searchResults
							.getPages().get(pageNumber) : null;
					searchResults.getMessage();
					searchQueryDate = searchResults.getSearchQuery().getDate();
					if (searchQueryDate != null) {
						if (order == null) {
							order = new Order();
						}
						if(order.getRecipient() != null){
							request.setAttribute("states", portal.getStatesOptions(order.getRecipient().getState(),order.getRecipient().getCountryId()));
							request.setAttribute("bannerAds", portal.getBannerAds());
							request.setAttribute("sideAds", portal.getSideAds());
							request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
							request.setAttribute("countries", portal.getCountryOptions(order.getRecipient().getCountryId()));
						}
						String city = searchResults.getSearchQuery().getCity();
						String state = searchResults.getSearchQuery().getStateCode();
						String zip = searchResults.getSearchQuery().getZip();
						String facility = (String) request.getSession(true).getAttribute("facilityData");
						if(facility != null && !facility.equals("")){
							try{
								String[] facilityParts = facility.replaceAll("\\+","").split("--");
								String facName = facilityParts[0];
								String address = facilityParts[1];
								String cityStateZip = facilityParts[2];
								String[] cityStateZipParts = cityStateZip.split(",");
								String facZip = cityStateZipParts[2];
								
								order.setFacilityName(facName);
								
								request.setAttribute("searchedAddress", address.trim());
								if(city != null && !city.equals("") && !city.equals("City"))
									request.setAttribute("searchedCity", city);
								request.setAttribute("searchedZip", facZip.trim());
								if(state != null && !state.equals("") && !state.equals("State/Province")){
									Document stateDoc = portal.getStateDocument(state);
									request.setAttribute("states", portal.getStatesOptions(state,stateDoc.get("country_id")));
									request.setAttribute("bannerAds", portal.getBannerAds());
									request.setAttribute("sideAds", portal.getSideAds());
									request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
									request.setAttribute("countries", portal.getCountryOptions(stateDoc.get("country_id")));
								}
								request.setAttribute("searchedState", state);
								try{
									request.setAttribute("searchedPhone", facilityParts[3].trim());
								}catch(Exception ee){}
							}catch(Exception ee){
							}
						}else{
							if(city != null && !city.equals("") && !city.equals("City"))
								request.setAttribute("searchedCity", city);
							if(state != null && !state.equals("") && !state.equals("State/Province")){
								Document stateDoc = portal.getStateDocument(state);
								request.setAttribute("states", portal.getStatesOptions(state,stateDoc.get("country_id")));
								request.setAttribute("bannerAds", portal.getBannerAds());
								request.setAttribute("sideAds", portal.getSideAds());
								request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
								request.setAttribute("countries", portal.getCountryOptions(stateDoc.get("country_id")));
							}
								request.setAttribute("searchedState", state);
							if(zip != null && !zip.equals("") && !zip.equals("Zip / Postal Code"))
								request.setAttribute("searchedZip", zip);

						}
						order.setDeliveryDate(searchQueryDate);
					}

				}
				if (currPage != null) {
					LinkedList<String> shopCodes = new LinkedList<String>();
					for (int ii = 0; ii < currPage.size(); ++ii) {
						Document result = currPage.get(ii);
						String listingId = result.get("shopcode") != null ? result
								.get("shopcode") : "";
						if (listingId != null && listingId != "")
							shopCodes.add(listingId);
					}

					request.setAttribute("searchResults", searchResults);
					request.setAttribute("productsMap", productsMap);
					request.setAttribute("catagoriesMap", catagoriesMap);
					request.getSession(true).setAttribute("BLOOMNET_ORDER",
							order);

				}
				request.setAttribute("order", order);
				request.getSession(true).setAttribute("BLOOMNET_ORDER", order);
				if ((searchResults == null)
						|| (searchResults.getResults().isEmpty())) {
					String fruitBouquetSearch = (String) request.getSession(true).getAttribute("fbSearch");
					request.getSession(true).setAttribute("fbSearch", fruitBouquetSearch);
					if(from.equals("bmsSearch"))
						returnValue = "bmsnoresults";
					else
						returnValue = "noresults";

				} else {
					if(from.equals("bmsSearch"))
						returnValue = "bmsresults";
					else
					returnValue = "results";

				}
			}

		} else if (view.equals("close")) {
			
			returnValue = handleCloseView(request);
			
		}else if (view.equals("backorder")) {
			Order order = (Order) request.getSession(true).getAttribute("BLOOMNET_ORDER");
			if(order != null && order.getRecipient() != null){
				request.setAttribute("order", order);
				request.setAttribute("states", portal.getStatesOptions(order.getRecipient().getState(),order.getRecipient().getCountryId()));
				request.setAttribute("bannerAds", portal.getBannerAds());
				request.setAttribute("sideAds", portal.getSideAds());
				request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
				request.setAttribute("countries", portal.getCountryOptions(order.getRecipient().getCountryId()));
				request.setAttribute("emptyorder", "emptyorder");
			}
			returnValue = "searchflorist";
			
		}else if (view.equals("backsearch")) {
			Order order = (Order) request.getSession(true).getAttribute("BLOOMNET_ORDER");
			if(order != null && order.getRecipient() != null){
				request.setAttribute("order", order);
				request.setAttribute("states", portal.getStatesOptions(order.getRecipient().getState(),order.getRecipient().getCountryId()));
				request.setAttribute("bannerAds", portal.getBannerAds());
				request.setAttribute("sideAds", portal.getSideAds());
				request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
				request.setAttribute("countries", portal.getCountryOptions(order.getRecipient().getCountryId()));
			}else{
				request.setAttribute("states", portal.getStatesOptions("",directoryProperties.getProperty("USACountryId")));
				request.setAttribute("bannerAds", portal.getBannerAds());
				request.setAttribute("sideAds", portal.getSideAds());
				request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
				request.setAttribute("countries", portal.getCountryOptions(directoryProperties.getProperty("USACountryId")));
			}
			if(order != null && order.getFulfillingShop() != null && order.getFulfillingShop().getShopCode() != null && !order.getFulfillingShop().getShopCode().equals("Z9980000"))
				returnValue = "results";
			else{
				String fruitBouquetSearch = (String) request.getSession(true).getAttribute("fbSearch");
				request.getSession(true).setAttribute("fbSearch", fruitBouquetSearch);
				returnValue = "noresults";
			}
						
		}else if (view.equals("modifysearch")) {
			Order order = (Order) request.getSession(true).getAttribute("BLOOMNET_ORDER");
			if(order != null)order.setFacilityName(null);
			if(order != null && order.getRecipient() != null){
				request.setAttribute("order", order);
				request.setAttribute("states", portal.getStatesOptions(order.getRecipient().getState(),order.getRecipient().getCountryId()));
				request.setAttribute("bannerAds", portal.getBannerAds());
				request.setAttribute("sideAds", portal.getSideAds());
				request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
				request.setAttribute("countries", portal.getCountryOptions(order.getRecipient().getCountryId()));
			}else{
				request.setAttribute("states", portal.getStatesOptions("",directoryProperties.getProperty("USACountryId")));
				request.setAttribute("bannerAds", portal.getBannerAds());
				request.setAttribute("sideAds", portal.getSideAds());
				request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
				request.setAttribute("countries", portal.getCountryOptions(directoryProperties.getProperty("USACountryId")));
			}
			request.getSession(true).removeAttribute("searchResults");
			request.getSession(true).removeAttribute("noSpecificResults");
			returnValue = "searchflorist";
			
		}else if(view.equals("backDropship")){
			Order order = (Order) request.getSession(true).getAttribute("BLOOMNET_ORDER");
			request.setAttribute("order", order);
			request.setAttribute("states", portal.getStatesOptions(order.getRecipient().getState(),order.getRecipient().getCountryId()));
			request.setAttribute("bannerAds", portal.getBannerAds());
			request.setAttribute("sideAds", portal.getSideAds());
			request.setAttribute("dropship", portal.getDropship());
			request.setAttribute("emptyorder", "dropship");
			returnValue = "dropshipOrder";
			
		}else if(view.equals("r4me")) {
			
			request.getSession().setAttribute("shopCode", user.getShopCode());
			Shop shop = orderDAO.getShopByShopCode(user.getShopCode());
			
			if(shop.getShopName() != null) request.getSession(true).setAttribute("formShopName", shop.getShopName());
			if(shop.getContactPerson() != null) request.getSession(true).setAttribute("formContact", shop.getContactPerson());
			
			returnValue = "route4MeForm";
		}

		return returnValue;
	}

	

	@RequestMapping(method = RequestMethod.POST)
	public String processPostRequest(HttpServletRequest request,
			HttpServletResponse response, Model model) throws IOException {

		String returnValue = null;

		Long timestamp = (request.getParameter("timestamp") != null && request
				.getParameter("timestamp").length() > 0) ? Long.valueOf(request
				.getParameter("timestamp")) : 0;

		User user = (User) sessionManager.getApplicationUser(request);
		if(user == null) {
			return "redirect:signin.htm";
		}
		request.setAttribute("user_email", user.getEmail());
		request.setAttribute("user_name", user.getShopCode());
		request.setAttribute("pref", user.getViewPreference());

		String view = request.getParameter("view");
		String from = request.getParameter("from");

		if (view == null || view.equals("order")) {
			
			returnValue = handleOrderView(request);
			
		} else if (view.equals("submit")) {
			
			returnValue = handleSubmitView(request, user);
			
		} else if (view.equals("summary")) {
			
			returnValue = handleSummaryView(request,from,timestamp);
			
		} else if (view.equals("close")) {
			
			returnValue = handleCloseView(request);
		}

		return returnValue;

	}

	private String handleCloseView(HttpServletRequest request) {
		
		String returnValue = new String();
		
		request.getSession(true).removeAttribute("BLOOMNET_ORDER");
		request.getSession(true).removeAttribute("searchResults");
		request.getSession(true).removeAttribute("noSpecificResults");
		List<Document> products = portal.getProducts();
		List<Document> dropship = portal.getDropship();
		request.setAttribute("products", products);
		request.setAttribute("dropship", dropship);
		request.setAttribute("states", portal.getStatesOptions("",directoryProperties.getProperty("USACountryId")));
		request.setAttribute("bannerAds", portal.getBannerAds());
		request.setAttribute("sideAds", portal.getSideAds());
		request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
		request.setAttribute("countries", portal.getCountryOptions(directoryProperties.getProperty("USACountryId")));
		returnValue = "searchflorist";
		
		return returnValue;
		
	}

	@SuppressWarnings("unchecked")
	private String handleSummaryView(HttpServletRequest request, String from, Long timestamp) {
		
		String returnValue = new String();
		
		Order order = null;
		String commitments = request.getParameter("commitment");
		if (commitments != null && commitments.length() > 0) {
		
			returnValue = handleCommitment(request);
			
		} else if(from != null && from.equals("dropshipOrder")){
			
			order = controllerService.processOrderParameters(request, true);
			controllerService.populateSummaryParameters(request, order);
			returnValue = "ordersummary";
			
		}else {
			
			if (from != null && from.equals("order")){
				order = controllerService.processOrderParameters(request, false);
				if(order.getRecipient().getPostalCode() != null && order.getRecipient().getPostalCode().equals("Zip / Postal Code")) order.getRecipient().setPostalCode("");
			}
			else if (from != null && from.equals("search")){
				order = controllerService.processSearchParameters(request);
			}
			else if (from != null && from.equals("facility")){
				order = controllerService.processSearchParameters(request);
				String searchDate = request
				.getParameter("deliverydate2");
		if (searchDate != null && !searchDate.equals("")) {
			order.setDeliveryDate(searchDate);
		}
				
			}
			if (order.getDeliveryDate() == null) {
				String searchDate = request
						.getParameter("searchDeliveryDate");
				if (searchDate != null && !searchDate.equals("")) {
					order.setDeliveryDate(searchDate);
				}
			}
				if ((order == null || order.getRecipient() == null)|| order.getRecipient().getFirstName() == null ||
					(order.getFacilityName() != null && order.getFulfillingShop() == null)) {
					if(order.getRecipient() == null || order.getRecipient().getFirstName() == null){
										request.setAttribute("emptyorder", "emptyorder");
									}
									if(order.getRecipient() == null || order.getRecipient().getState() == null){
										request.setAttribute("states", portal.getStatesOptions("",directoryProperties.getProperty("USACountryId")));
										request.setAttribute("bannerAds", portal.getBannerAds());
										request.setAttribute("sideAds", portal.getSideAds());
										request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
										request.setAttribute("countries", portal.getCountryOptions(directoryProperties.getProperty("USACountryId")));
									}
									else{
										Document stateDoc = portal.getStateDocument(order.getRecipient().getState());
										order.getRecipient().setCountryId(stateDoc.get("country_id"));
										request.setAttribute("states", portal.getStatesOptions(order.getRecipient().getState(),order.getRecipient().getCountryId()));
										request.setAttribute("bannerAds", portal.getBannerAds());
										request.setAttribute("sideAds", portal.getSideAds());
										request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
										request.setAttribute("countries", portal.getCountryOptions(order.getRecipient().getCountryId()));
									}
				List<Document> products = portal.getProducts();
				request.setAttribute("products", products);
				SearchResultSet<Document> searchResults = (SearchResultSet<Document>) request.getSession(true).getAttribute("searchResults");
				String existingCoverageNoZips = (String) request.getSession(true).getAttribute("existingCoverageNoZips");
				request.setAttribute("existingCoverageNoZips", existingCoverageNoZips);
				Map<String, Document> productsMap = (Map<String, Document>) request	.getSession(true).getAttribute("productsMap");
				Map<String, Document> catagoriesMap = (Map<String, Document>) request.getSession(true).getAttribute("categoriesMap");
				request.setAttribute("searchResults", searchResults);
				request.setAttribute("productsMap", productsMap);
				request.setAttribute("catagoriesMap", catagoriesMap);
				returnValue = "results";
			} else if (order.getFulfillingShop() == null) {
				request.setAttribute("states", portal.getStatesOptions(order.getRecipient().getState(),order.getRecipient().getCountryId()));
				request.setAttribute("bannerAds", portal.getBannerAds());
				request.setAttribute("sideAds", portal.getSideAds());
				request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
				request.setAttribute("countries", portal.getCountryOptions(order.getRecipient().getCountryId()));
				List<Document> products = portal.getProducts();
				request.setAttribute("products", products);
				request.setAttribute("categories", portal.fetchCategories());
				if (order.getDeliveryDate() == null)
					request.setAttribute("currentDate",
							DateUtil.getCurrentDate(timestamp));
				else
					request.setAttribute("currentDate",
							order.getDeliveryDate());
			
				request.setAttribute("order", request.getSession(true)
						.getAttribute("BLOOMNET_ORDER"));

				returnValue = "searchflorist";
			} else {
				controllerService.populateSummaryParameters(request, order);
				returnValue = "ordersummary";
			}
		}
		if(request.getParameter("productName") != null && request.getParameter("productPrice") != null){
			if(order.getDescribedProducts() != null){
				DescribedProduct dp = new DescribedProduct(1,request.getParameter("productName"),new Money(Double.valueOf(request.getParameter("productPrice"))));
				order.getDescribedProducts().add(dp);
			}else{
				List<DescribedProduct> dps = new ArrayList<DescribedProduct>();
				DescribedProduct dp = new DescribedProduct(1,request.getParameter("productName"),new Money(Double.valueOf(request.getParameter("productPrice"))));
				dps.add(dp);
				order.setDescribedProducts(dps);
			}
		}
		String facilityId = request.getParameter("facilityId");
		if(facilityId != null && !facilityId.equals("")){
			order.setFacilityId(facilityId);
			Document facility = portal.getFacilityDocument(facilityId);
			if(facility != null){
				if (order.getRecipient()==null){
					order.setRecipient(new Recipient());
				}
				if(facility.get("name") != null && !facility.get("name").equals("")) order.setFacilityName(facility.get("name").replaceAll("\\+", " "));
				if(facility.get("address1") != null && !facility.get("address1").equals("")) order.getRecipient().setAddress1(facility.get("address1").replaceAll("\\+", " "));
				if(facility.get("address2") != null && !facility.get("address2").equals("")) order.getRecipient().setAddress2(facility.get("address2").replaceAll("\\+", " "));			 		 
				if(facility.get("zip") != null && !facility.get("zip").equals("")) {
					String facilityZip = facility.get("zip");
					if(facilityZip.length() == 5) order.getRecipient().setPostalCode(facilityZip.replaceAll("\\+", " "));
					else if(facilityZip.length() > 5) order.getRecipient().setPostalCode(facilityZip.replaceAll("\\+", " "));
				}
				if(facility.get("phone") != null && ! facility.get("phone").equals("")) order.getRecipient().setPhoneNumber(facility.get("phone"));
				if(facility.get("city_id") != null && !facility.get("city_id").equals("")) {
					String country_code = facility.get("country");
					if(country_code.equals("US")) country_code = "USA";
					else if(country_code.equals("CA")) country_code = "CAN";
					else if(country_code.equals("PR")) country_code = "PUR";
					String state_code = facility.get("state_code");
					String city_name = facility.get("city");
					order.getRecipient().setCity(city_name.replaceAll("\\+", " "));
					order.getRecipient().setState(state_code.replaceAll("\\+", " "));
					order.getRecipient().setCountryCode(country_code.replaceAll("\\+", " "));
				}
			}
		}
		return returnValue;
	}
	



@SuppressWarnings("unchecked")
private String handleSearchResultsView(HttpServletRequest request, String from) {
		
		String returnValue = new String();
		request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
		
		if (from != null && from.equals("search")) {
			SearchResultSet<Document> searchResults = (SearchResultSet<Document>) request.getSession(true).getAttribute("searchResults");
			String existingCoverageNoZips = (String) request.getSession(true).getAttribute("existingCoverageNoZips");
			request.setAttribute("existingCoverageNoZips", existingCoverageNoZips);
			Map<String, Document> productsMap = (Map<String, Document>) request.getSession(true).getAttribute("productsMap");
			Map<String, Document> catagoriesMap = (Map<String, Document>) request.getSession(true).getAttribute("categoriesMap");
			Order order = (Order) sessionManager.getOrder(request);
			Document stateDoc = null;
			if(searchResults != null){
				if(searchResults.getSearchQuery().getState() != null)
					stateDoc = portal.getStateDocument(searchResults.getSearchQuery().getState());
				if(order != null && order.getRecipient() != null && stateDoc != null)
					order.getRecipient().setCountryId(stateDoc.get("country_id"));
				else if(order == null && stateDoc != null){
					order = new Order();
					Recipient recipient = new Recipient();
					recipient.setCountryId(stateDoc.get("country_id"));
					order.setRecipient(recipient);
				}else if(stateDoc != null){
					Recipient recipient = new Recipient();
					recipient.setCountryId(stateDoc.get("country_id"));
					order.setRecipient(recipient);
				}
			}

			int pageNumber = 0;
			List<Document> currPage = null;
			String searchQueryDate = "";
			if (searchResults != null) {
				pageNumber = searchResults.getPageNumber();
				currPage = (searchResults.getPages().size() > 0) ? searchResults
						.getPages().get(pageNumber) : null;
				searchQueryDate = searchResults.getSearchQuery().getDate();
				if (searchQueryDate != null) {
					if (order == null) {
						order = new Order();
						request.setAttribute("states", portal.getStatesOptions("",directoryProperties.getProperty("USACountryId")));
						request.setAttribute("bannerAds", portal.getBannerAds());
						request.setAttribute("sideAds", portal.getSideAds());
						request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
						request.setAttribute("countries", portal.getCountryOptions(directoryProperties.getProperty("USACountryId")));
					}else if(order.getRecipient() != null){
						request.setAttribute("states", portal.getStatesOptions(order.getRecipient().getState(),order.getRecipient().getCountryId()));
						request.setAttribute("bannerAds", portal.getBannerAds());
						request.setAttribute("sideAds", portal.getSideAds());
						request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
						request.setAttribute("countries", portal.getCountryOptions(order.getRecipient().getCountryId()));
					}else{
						request.setAttribute("states", portal.getStatesOptions("",directoryProperties.getProperty("USACountryId")));
						request.setAttribute("bannerAds", portal.getBannerAds());
						request.setAttribute("sideAds", portal.getSideAds());
						request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
						request.setAttribute("countries", portal.getCountryOptions(directoryProperties.getProperty("USACountryId")));
					}
						String city = searchResults.getSearchQuery().getCity();
											String state = searchResults.getSearchQuery().getStateCode();
											String zip = searchResults.getSearchQuery().getZip();
											String facility = (String) request.getSession(true).getAttribute("facilityData");
											if(facility != null && !facility.equals("")){
												try{
													String[] facilityParts = facility.split("--");
													String facName = facilityParts[0];
													String address = facilityParts[1];
													String cityStateZip = facilityParts[2];
													String[] cityStateZipParts = cityStateZip.split(",");
													String facZip = cityStateZipParts[2];
													
													order.setFacilityName(facName);
													
													request.setAttribute("searchedAddress", address.trim());
													if(city != null && !city.equals("") && !city.equals("City"))
														request.setAttribute("searchedCity", city);
													request.setAttribute("searchedZip", facZip.trim());
													if(state != null && !state.equals("") && !state.equals("State/Province")){
														request.setAttribute("states", portal.getStatesOptions(state,stateDoc.get("country_id")));
														request.setAttribute("bannerAds", portal.getBannerAds());
														request.setAttribute("sideAds", portal.getSideAds());
														request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
														request.setAttribute("countries", portal.getCountryOptions(stateDoc.get("country_id")));
													}
													request.setAttribute("searchedState", state);
													try{
														request.setAttribute("searchedPhone", facilityParts[3].trim());
													}catch(Exception ee){}
												}catch(Exception ee){
												}
											}else{
												if(city != null && !city.equals("") && !city.equals("City"))
													request.setAttribute("searchedCity", city);
												if(state != null && !state.equals("") && !state.equals("State/Province")){
													if(stateDoc == null)
														stateDoc = portal.getStateDocument(state);
													request.setAttribute("states", portal.getStatesOptions(state,stateDoc.get("country_id")));
													request.setAttribute("bannerAds", portal.getBannerAds());
													request.setAttribute("sideAds", portal.getSideAds());
													request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
													request.setAttribute("countries", portal.getCountryOptions(stateDoc.get("country_id")));
												}
												request.setAttribute("searchedState", state);
												if(zip != null && !zip.equals("") && !zip.equals("Zip / Postal Code"))
													request.setAttribute("searchedZip", zip);
					
						}
					order.setDeliveryDate(searchQueryDate);

				}

			}
			if (currPage != null) {
				LinkedList<String> shopCodes = new LinkedList<String>();
				for (int ii = 0; ii < currPage.size(); ++ii) {
					Document result = currPage.get(ii);
					String listingId = result.get("shopcode") != null ? result
							.get("shopcode") : "";
					if (listingId != null && listingId != "")
						shopCodes.add(listingId);
				}
			

				request.setAttribute("searchResults", searchResults);
				request.getSession(true).setAttribute("searchResults", searchResults);
				request.setAttribute("productsMap", productsMap);
				request.setAttribute("catagoriesMap", catagoriesMap);
				request.getSession(true).setAttribute("BLOOMNET_ORDER",
						order);

				

			}
			request.setAttribute("order", order);
			request.getSession(true).setAttribute("BLOOMNET_ORDER", order);
			if ((searchResults == null)
					|| (searchResults.getResults().isEmpty())) {
				String fruitBouquetSearch = (String) request.getSession(true).getAttribute("fbSearch");
				request.getSession(true).setAttribute("fbSearch", fruitBouquetSearch);
				if(order != null && order.getRecipient() != null && order.getRecipient().getState() != null){
					request.setAttribute("states", portal.getStatesOptions(order.getRecipient().getState(),order.getRecipient().getCountryId()));
					request.setAttribute("countries", portal.getCountryOptions(order.getRecipient().getCountryId()));
				}else{
					request.setAttribute("states", portal.getStatesOptions("",directoryProperties.getProperty("USACountryId")));
					request.setAttribute("countries", portal.getCountryOptions(directoryProperties.getProperty("USACountryId")));
				}
				request.setAttribute("bannerAds", portal.getBannerAds());
				request.setAttribute("sideAds", portal.getSideAds());
				request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
				if(from.equals("bmsSearch"))
					returnValue = "bmsnoresults";
				else
					returnValue = "noresults";

			} else {
				if(from.equals("bmsSearch"))
					returnValue = "bmsresults";
				else
				returnValue = "results";

			}
		}
		return returnValue;
		
	}

	@SuppressWarnings("unchecked")
	private String handleSubmitView(HttpServletRequest request, User user) {
		String returnValue = new String();
		
		Order order = (Order) request.getSession(true).getAttribute(
				"BLOOMNET_ORDER");
		
		if (user != null) {
			if (order != null) {
				if (order.getSendingShop() == null) {
					Shop sendingShop = new Shop();
					sendingShop.setShopCode(user.getShopCode());
					order.setSendingShop(sendingShop);
				}
				order.setReceivingShop(order.getFulfillingShop());
				if (order.getOcasionId() == null)
					order.setOcasionId(8L);
				RestResponse restResponse = portal.order(user, order);
				request.setAttribute("response", restResponse);
				request.setAttribute("order", order);
				
				List<SearchResultSet<Document>> searches = (List<SearchResultSet<Document>>) request.getSession().getAttribute("BLOOMNET_LOGS");
				if (searches != null && searches.size() > 0) {
					SearchResultSet<Document> search = searches
							.get(searches.size() - 1);
					if (search != null)
						search.setOrderId(restResponse.getBmtOrderNumber());
				}
				
				returnValue = "orderconfirmation";

			} else {
				new com.flowers.portal.vo.Error(
						"No order submitted.");
				returnValue = "orderconfirmation";

			}
		}
		return returnValue;
		
	}

	private String handleOrderView(HttpServletRequest request) {
		String returnValue = new String();

					String date = null, formatDate = null;
					
					Order order = (Order) sessionManager.getOrder(request);

					try {
						date = request.getParameter("rfx_deliverydate");
						formatDate = DateUtil.formatDateString(date);
					} catch (Exception ee) {
					}
					if (order != null)
						order.setFacilityId("");
					String states = null;
					if (order != null) {
						request.setAttribute("order", request.getSession(true)
								.getAttribute("BLOOMNET_ORDER"));
						if (order.getRecipient() != null){
							request.setAttribute("states", portal.getStatesOptions(order.getRecipient().getState(),order.getRecipient().getCountryId()));
							request.setAttribute("bannerAds", portal.getBannerAds());
							request.setAttribute("sideAds", portal.getSideAds());
							request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
							request.setAttribute("countries", portal.getCountryOptions(order.getRecipient().getCountryId()));
						}
					}
					if (order == null && date != null && date.length() > 0) {
						order = new Order();
						order.setDeliveryDate(formatDate);
						request.getSession(true).setAttribute("BLOOMNET_ORDER", order);
					}
					if (states == null){
						states = portal.getStatesOptions("",directoryProperties.getProperty("USACountryId"));
						request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
						request.setAttribute("countries", portal.getCountryOptions(directoryProperties.getProperty("USACountryId")));
					}
					request.setAttribute("states", states);
					request.setAttribute("bannerAds", portal.getBannerAds());
					request.setAttribute("sideAds", portal.getSideAds());
					
					List<Document> products = portal.getProducts();
					List<Document> dropship = portal.getDropship();
					
					request.setAttribute("products", products);
					request.setAttribute("dropship", dropship);
					
					 returnValue = "searchflorist";
					 
					 return returnValue;
	}
	
	private String handleCommitment(HttpServletRequest request) {
		
		String returnValue = new String();
			
		Order order = (Order) request.getSession(true).getAttribute(
				"BLOOMNET_ORDER");
		if (order != null) {
			request.setAttribute("order", request.getSession(true)
					.getAttribute("BLOOMNET_ORDER"));
			order.setFulfillingShop(new Shop(directoryProperties
					.getProperty("commitmentShop"),
					"Commitment To Coverage"));
			controllerService.populateSummaryParameters(request, order);
		} else {
			order = new Order();
			order.setFulfillingShop(new Shop(directoryProperties
					.getProperty("commitmentShop"),
					"Commitment To Coverage"));
			request.getSession(true).setAttribute("BLOOMNET_ORDER",
					order);
		}
		if (order.getRecipient() != null && order.getRecipient().getFirstName() != null)
			returnValue = "ordersummary";

		else {
			if(order.getRecipient() != null && order.getRecipient().getState() != null){
				request.setAttribute("states", portal.getStatesOptions(order.getRecipient().getState(),order.getRecipient().getCountryId()));
				request.setAttribute("bannerAds", portal.getBannerAds());
				request.setAttribute("sideAds", portal.getSideAds());
				request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
				request.setAttribute("countries", portal.getCountryOptions(order.getRecipient().getCountryId()));
			}else{
				request.setAttribute("states", portal.getStatesOptions("",directoryProperties.getProperty("USACountryId")));
				request.setAttribute("bannerAds", portal.getBannerAds());
				request.setAttribute("sideAds", portal.getSideAds());
				request.setAttribute("fruitBouquet", portal.getFruitBouquetProducts());
				request.setAttribute("countries", portal.getCountryOptions(directoryProperties.getProperty("USACountryId")));
			}
			List<Document> products = portal.getProducts();
			request.setAttribute("products", products);
			String fruitBouquetSearch = (String) request.getSession(true).getAttribute("fbSearch");
			request.getSession(true).setAttribute("fbSearch", fruitBouquetSearch);
			returnValue = "noresults";
			request.setAttribute("emptyorder", "emptyorder");
		}
		
		return returnValue;
		
		}		

}
