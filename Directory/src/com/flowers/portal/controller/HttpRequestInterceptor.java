package com.flowers.portal.controller;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;

import com.flowers.portal.service.ControllerService;
import com.flowers.portal.service.SessionManager;
import com.flowers.portal.vo.ApplicationView;
import com.flowers.server.entity.User;
import com.flowers.server.entity.Userrole;



/**
 * Intercepts HTTP requests to ensure user is signed in; it also closes the
 * Hibernate session for the current thread.
 *
 * @author Mark Silver
 */
public class HttpRequestInterceptor implements HandlerInterceptor  {
    
    
    // Define a static logger variable
    static final Logger logger = Logger.getLogger( HttpRequestInterceptor.class );
    
    
    private String         signInPage;
    private SessionManager sessionManager;
	@Autowired private Properties viewAccess;
    @Autowired private ControllerService controllerService;


    
    
    /**
     * Uses SessionManager to ensure user is logged in; if not, then
     * user is forwarded to the sign-in page.
     *
     * @see SimpleHttpSessionManagerImpl
     */
    public boolean preHandle( HttpServletRequest request, 
                              HttpServletResponse response, 
                              Object handler) throws Exception {
        
        final boolean result;
        
        User user = (User) sessionManager.getApplicationUser(request);
        
        if ( user == null ) {
            
            response.sendRedirect( signInPage );
            result = false;
        }
        else 
            result = true;
        
        return result;
    }
    

    @SuppressWarnings("unused")
	private boolean isViewAllowed(String requestURI, User user) {

    	@SuppressWarnings({ "unchecked", "rawtypes" })
		HashMap<String, String> propMap = new HashMap<String, String>((Map) viewAccess);
    	Set<Map.Entry<String, String>> propSet;
    	propSet = propMap.entrySet();

    	Set<ApplicationView> allowedViews = new HashSet<ApplicationView>();
    	Set<Userrole>        userRoles    = getUserrolesForUserId(user);
    	Set<ApplicationView> userViews = null;



    	for (Map.Entry<String, String> me : propSet) {

    		ApplicationView view = new ApplicationView();
    		view.setUrl(me.getKey());

    		String[] roleNames = me.getValue().split(",");

    		for( int i = 0; i < roleNames.length; i++ ) {

    			String definedRoleDescription = roleNames[i];

    			for(Userrole userRole : userRoles){

    				if(definedRoleDescription.equalsIgnoreCase(userRole.getRole().getDescription())){
    					allowedViews.add(view);
    				}
    			}
    		}
    	}

    	boolean b = false;

    	if ( userViews != null ) {

    		for ( ApplicationView view : userViews ) {

    			if ( requestURI.indexOf ( view.getUrl () ) != -1 ) {

    				b = true;
    			}
    		}
    	}

    	return b;


    }


	


	private Set<Userrole> getUserrolesForUserId(User user) {

		Set<Userrole> userRoles = new HashSet<Userrole>();
		long id = user.getId();
		
		List<Userrole> results = controllerService.getUserrolesByUserId(Long.toString(id));

		for (Userrole role: results){
			userRoles.add(role);
		}
		
		return userRoles;
	}


	///////////////////////////////////////////////////////////////////////////
    //
    // START SETTERS AND GETTERS FOR SPRING DEPENDENCY INJECTION
    //
    public String getSignInPage() {        
        return signInPage;
    }
    public void setSignInPage(String signInPage) {
        this.signInPage = signInPage;
    }
    
    public SessionManager getSessionManager() {
        return sessionManager;
    }
    public void setSessionManager(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }
}
