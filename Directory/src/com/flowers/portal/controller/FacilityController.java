package com.flowers.portal.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.flowers.server.entity.Facility;
import com.flowers.server.entity.FacilityType;
import com.flowers.server.search.SearchQuery;
import com.flowers.server.search.SearchQueryType;
import com.flowers.server.search.SearchResultSet;
import com.flowers.portal.service.SearchService;

/**
 * Bloomnet controller for facility. This is a controller for a RESTful Spring MVC web service that
 * returns on JSON format.
 * @author Andre Moura
 */
@Controller
public class FacilityController {
	protected final Log logger = LogFactory.getLog(getClass());
	@Autowired private SearchService search;
	/**
	 * Returns a JSON array containing all facility types. Accepts state and city parameters to
	 * be used as criteria for the search. These paratemeters should be informed in the query string.<br/>
	 * e.g. facility
	 * @param request
	 * @param state
	 * @param city
	 * @return JSON array with all facility types
	 */
	@RequestMapping(value="facilitytype", method=RequestMethod.GET)
	public @ResponseBody SearchResultSet<FacilityType> getFacilityTypes(HttpServletRequest request,
			@RequestParam(value="stateid", required=false) String stateId,
			@RequestParam(value="city", required=false) String city) {
		SearchQuery searchQuery = new SearchQuery(SearchQueryType.FACILITY_TYPE);
		if(city != null && !city.equals("")) searchQuery.setCity(city);
		if(stateId != null && !stateId.equals("")) searchQuery.setState(stateId);
		return search.search(searchQuery);
	}
	
	/**
	 * Returns a JSON array containing all facilities.
	 * @param facilityTypeId
	 * @return JSON array with all facilities
	 */
	@RequestMapping(value="facilitytype/{facilityTypeId}/facility", method=RequestMethod.GET)
	public @ResponseBody SearchResultSet<Map<String, Object>> getFacilities(HttpServletRequest request,@PathVariable String facilityTypeId,
			@RequestParam(value="stateid", required=false) String stateId,
			@RequestParam(value="city", required=false) String city) {
		SearchQuery searchQuery = new SearchQuery(SearchQueryType.FACILITY, SearchQuery.RETURN_TYPE_MAP);
		if(city != null && !city.equals("")) searchQuery.setCity(city);
		if(stateId != null && !stateId.equals("")) searchQuery.setState(stateId);
		if(facilityTypeId != null && !facilityTypeId.equals("")) searchQuery.setFacilityTypeId(facilityTypeId);
		return search.search(searchQuery);
	}
	
	/**
	 * Returns a JSON object representing a facility.
	 * @param facilityId
	 * @return JSON object representing a facility
	 */
	@RequestMapping(value="facility/{facilityId}", method=RequestMethod.GET)
	public @ResponseBody Facility getFacility(@PathVariable long facilityId) {
		
		FacilityType facilityType = new FacilityType(1, "Churches");
		
		Facility facility = new Facility();
		facility.setId(facilityId);
		facility.setName("Church 1");
		facility.setFacilityType(facilityType);
		
		return facility;
	}
}
