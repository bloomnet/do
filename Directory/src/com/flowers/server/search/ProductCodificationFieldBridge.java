package com.flowers.server.search;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.hibernate.search.bridge.FieldBridge;
import org.hibernate.search.bridge.LuceneOptions;

import com.flowers.server.entity.ProductCodification;

public class ProductCodificationFieldBridge implements FieldBridge {

	@Override
	public void set(String name, Object value, Document document, LuceneOptions luceneOptions) {
		StringField field = null;
		if(value instanceof ProductCodification) {
			ProductCodification productCodification = (ProductCodification)value;
			field = new StringField("tokenizedName", productCodification.getName(), Field.Store.YES);
			document.add(field);
		}
	}

}
