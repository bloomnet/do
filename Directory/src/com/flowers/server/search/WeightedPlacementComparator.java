package com.flowers.server.search;

import java.util.Comparator;

import org.apache.lucene.document.Document;

public class WeightedPlacementComparator implements Comparator<Document> {

	public int compare(Document doc1, Document doc2) {
		String weight1 = doc1.get("weightedPlacement");
		String weight2 = doc2.get("weightedPlacement");
		
		if(weight1 == null && weight2 == null) 
			return 0;
		else if(weight1 == null)
			return 1;
		else if(weight2 == null)
			return -1;
		else{
			Double weightDouble1 = Double.valueOf(weight1);
			Double weightDouble2 = Double.valueOf(weight2);
			return weightDouble2.compareTo(weightDouble1);
		}
	}
	
}
