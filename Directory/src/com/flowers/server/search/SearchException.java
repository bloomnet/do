package com.flowers.server.search;

public class SearchException extends Exception {
	private static final long serialVersionUID = -6410016089020370360L;

	
	public SearchException(final Throwable cause) {
		super(cause);
	}
	public SearchException(final String message, final Throwable cause) {
		super(message, cause);
	}
	public SearchException(final String message) {
		super(message);
	}
	
}
