/*
 * Copyright (C) 2011 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.flowers.server.search;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.hibernate.search.bridge.FieldBridge;
import org.hibernate.search.bridge.LuceneOptions;

import com.flowers.server.entity.Address;
import com.flowers.server.entity.City;
import com.flowers.server.entity.Listing;
import com.flowers.server.entity.MinimumPrice;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.ShopFacilityXref;
import com.flowers.server.entity.ShopProductCodificationXref;
import com.flowers.server.entity.State;
import com.flowers.server.entity.Zip;

public class ListingFieldBridge implements FieldBridge {

	public void set(String name, Object value, Document document, LuceneOptions luceneOptions) {
		StringField field = null;
		TextField field2 = null;
		if(value instanceof Listing) {
			Listing listing = (Listing)value;
			if(listing.getDelivery() != null) {
				field = new StringField("delivery_charge", String.valueOf(listing.getDelivery()), Field.Store.YES);
				document.add(field);
			}
			if(listing.getListingType() != null) {
				field = new StringField("listing_type", listing.getListingType().getName(), Field.Store.YES);
				document.add(field);
			}
			if(listing.getCustomListing() != null){
				field = new StringField("custom_listing", listing.getCustomListing(), Field.Store.YES);
				document.add(field);
			}
			if(listing.getAdSize() != null) {
				field = new StringField("ad_size", listing.getAdSize().getAdSize(), Field.Store.YES);
				document.add(field);
			}
			if(listing.getCity() != null) {
				City city = listing.getCity();
				field = new StringField("city_id", String.valueOf(city.getId()), Field.Store.YES);
				document.add(field);
				field = new StringField("city", String.valueOf(city.getName()), Field.Store.YES);
				document.add(field);
				State state = city.getState();
				if(state != null) {
					field = new StringField("state_id", String.valueOf(state.getId()), Field.Store.YES);
					document.add(field);
					field = new StringField("state", state.getName(), Field.Store.YES);
					document.add(field);
					field = new StringField("state_code", state.getShortName(), Field.Store.YES);
					document.add(field);
				}
				StringBuffer buff = new StringBuffer();
				for(Zip zip : listing.getCity().getZips()) {
					buff.append(zip.getZipCode()+" ");
				}
				if(buff.length() > 1) {
					field = new StringField("zips", buff.toString().trim(), Field.Store.YES);
					document.add(field);
				}
			}
			Shop shop = listing.getShop();
			if(shop != null) {
				if(shop.getLatitude() != null) {
					field = new StringField("latitude", String.valueOf(shop.getLatitude()), Field.Store.YES);
					document.add(field);
				}
				if(shop.getLongitude() != null) {
					field = new StringField("longitude", String.valueOf(shop.getLongitude()), Field.Store.YES);
					document.add(field);
				}
				if(shop.getMinimum() != null) {
					field = new StringField("minimum", String.valueOf(shop.getMinimum()), Field.Store.YES);
					document.add(field);
				}
				field = new StringField("open_sunday", String.valueOf(shop.getOpenSunday()), Field.Store.YES);
				document.add(field);
				field = new StringField("shopcode", shop.getShopCode(), Field.Store.YES);
				document.add(field);
				if(shop.getContactPerson() != null) {
					field = new StringField("contact", shop.getContactPerson(), Field.Store.YES);
					document.add(field);
				}
				if(shop.getTelephoneNumber() != null) {
					field = new StringField("phone", shop.getTelephoneNumber(), Field.Store.YES);
					document.add(field);
				}
				if(shop.getFaxNumber() != null) {
					field = new StringField("fax", shop.getFaxNumber(), Field.Store.YES);
					document.add(field);
				}
				if(shop.getAddress() != null) {
					if(shop.getAddress().getCity() != null) {
						field = new StringField("shopcity", shop.getAddress().getCity().getName(), Field.Store.YES);
						document.add(field);
					}
					if(shop.getAddress().getState() != null) {
						field = new StringField("shopstate", shop.getAddress().getState().getShortName(), Field.Store.YES);
						document.add(field);
					}
				}
				if(shop.getShopName() != null) {
					field = new StringField("shopname", shop.getShopName(), Field.Store.YES);
					document.add(field);
				}
				if(shop.getShopProductCodificationXrefs().size() > 0) {
					StringBuffer buff = new StringBuffer();
					for(ShopProductCodificationXref product : shop.getShopProductCodificationXrefs()) {
						buff.append(product.getProductCodification().getId()+" ");
					}
					if(buff.length() > 0)
						field = new StringField("products", buff.toString().trim(), Field.Store.YES);
					else field = new StringField("products", "NA", Field.Store.YES);
					document.add(field);
				} else {
					field = new StringField("products", "NA", Field.Store.YES);
					document.add(field);
				}
				if(shop.getShopFacilityXrefs().size() > 0) {
					StringBuffer buff = new StringBuffer();
					for(ShopFacilityXref facility : shop.getShopFacilityXrefs()) {
						buff.append(facility.getFacility().getId()+" ");
					}
					field = new StringField("facilities", buff.toString().trim(), Field.Store.YES);
					document.add(field);
				}
				/** 9 Product Categories, enter 0 for missing values **/
				MinimumPrice[] prices = new MinimumPrice[10];
				if(shop.getMinimumPrices().size() > 0) {
					for(MinimumPrice price : shop.getMinimumPrices()) {
						Long id = price.getProductCategory().getId();
						if(id != null) {
							prices[id.intValue()] = price;
						}
					}
				}
				StringBuffer buff = new StringBuffer();
				for(int i=1; i < prices.length; i++) {
					if(prices[i] == null) buff.append("0.0 ");
					else if(prices[i].getFluctuatePrice()) buff.append(prices[i].getMinimumPrice()+"H ");
					else buff.append(String.valueOf(prices[i].getMinimumPrice())+" ");
				}
				field2 = new TextField("minimums", buff.toString().trim(), Field.Store.YES);
				document.add(field2);
				
				Address address = shop.getAddress();
				if(address != null) {
					field = new StringField("zip", address.getPostalCode(), Field.Store.YES);
					document.add(field);
					if(address.getStreetAddress1() != null) {
						field = new StringField("address1", address.getStreetAddress1(), Field.Store.YES);
						document.add(field);
					}
					if(address.getStreetAddress2() != null) {
						field = new StringField("address2", address.getStreetAddress2(), Field.Store.YES);
						document.add(field);
					}
				}
				
				if(shop.getShopWebsite() != null){
					field2 = new TextField("website", shop.getShopWebsite(), Field.Store.YES);
					document.add(field2);
				}
				
				if(shop.getShopLogo() != null){
					field2 = new TextField("logo", shop.getShopLogo(), Field.Store.YES);
					document.add(field2);
				}
			}
		}
	}

}