package com.flowers.server.search;

import java.util.Comparator;

import org.apache.lucene.document.Document;

public class BidComparator implements Comparator<Document> {

	
	public int compare(Document doc1, Document doc2) {
		String bid1 = doc1.get("bid");
		String bid2 = doc2.get("bid");
		
		if(bid1 != null && (Double.valueOf(bid1) == 0.0 || Double.valueOf(bid1) == 0))
			bid1 = null;
		if(bid2 != null && (Double.valueOf(bid2) == 0.0 || Double.valueOf(bid2) == 0))
			bid2 = null;
		
		if(bid1 == null && bid2 == null) 
			return 0;
		else if(bid1 == null)
			return 1;
		else if(bid2 == null)
			return -1;
		else{
			Double bidDouble1 = Double.valueOf(bid1);
			Double bidDouble2 = Double.valueOf(bid2);
			return bidDouble2.compareTo(bidDouble1);
		}
	}

}
