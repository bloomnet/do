package com.flowers.server.search;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.hibernate.search.bridge.FieldBridge;
import org.hibernate.search.bridge.LuceneOptions;

import com.flowers.server.entity.State;

public class StateFieldBridge implements FieldBridge {

	@Override
	public void set(String name, Object value, Document document, LuceneOptions luceneOptions) {
		StringField field = null;
		TextField field2 = null;
		if(value instanceof State) {
			State state = (State)value;
			field = new StringField("tokenizedName", state.getName(), Field.Store.YES);
			document.add(field);
			field2 = new TextField("country_id", String.valueOf(state.getCountry().getId()), Field.Store.YES);
			document.add(field2);
		}
	}

}
