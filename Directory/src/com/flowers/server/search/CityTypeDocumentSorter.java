package com.flowers.server.search;
import java.util.Comparator;
import java.util.Map;

@SuppressWarnings("rawtypes")
public class CityTypeDocumentSorter implements Comparator<Map> {

	
	public int compare(Map doc1, Map doc2) {
		String type1 = (String)doc1.get("city_type");
		String name1 = (String)doc1.get("name");
		String type2 = (String)doc2.get("city_type");
		String name2 = (String)doc1.get("name");
		if(type1 == null) {
			if(type2 == null) {
				if(name1 != null && name2 != null) return name1.compareTo(name2);
			} else return -1;
		} else {
			if(type2 == null) return 1;
			if(type1.equals("D") && !type2.equals("D")) return 1;
			if(type2.equals("D") && !type1.equals("D")) return -1;
		}
		return 0;
	}
}