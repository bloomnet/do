package com.flowers.server.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "search_query")
public class SearchQuery {
	private long id;
	private String zip;
	private String city;
	private String state;
	private String phone;
	private String shopCode;
	private String shopName;
	private String delivery;
	private String facility;
	private String minimums;
	private String products;
	private long timestamp;
	private Long orderId;
	private Integer count;
	private String searchingShop;
	
	public SearchQuery() {}
	public SearchQuery(String delivery, String zip,String city,String state,String phone,String shopCode,String shopName, long orderId, String searchingShop) {
		this.delivery = delivery;
		this.zip = zip;
		this.city = city;
		this.state = state;
		this.phone = phone;
		this.shopCode = shopCode;
		this.shopName = shopName;
		this.timestamp = System.currentTimeMillis();
		this.orderId = orderId;
		this.searchingShop = searchingShop;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@Column(name = "zip")
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	@Column(name = "city")
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Column(name = "state")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Column(name = "phone")
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	@Column(name = "shop_code")
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	@Column(name = "shop_name")
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	@Column(name = "facility")
	public String getFacility() {
		return facility;
	}
	public void setFacility(String facility) {
		this.facility = facility;
	}
	@Column(name = "delivery")
	public String getDelivery() {
		return delivery;
	}
	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}
	@Column(name = "minimums")
	public String getMinimums() {
		return minimums;
	}
	public void setMinimums(String minimums) {
		this.minimums = minimums;
	}
	@Column(name = "products")
	public String getProducts() {
		return products;
	}
	public void setProducts(String products) {
		this.products = products;
	}
	@Column(name = "timestamp")
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	@Column(name = "order_id")
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	@Column(name = "count")
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	@Column(name = "searching_shop")
	public String getSearchingShop() {
		return searchingShop;
	}
	public void setSearchingShop(String searchingShop) {
		this.searchingShop = searchingShop;
	}
	
}
