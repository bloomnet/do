package com.flowers.server.entity;

// Generated Jan 31, 2011 3:27:19 PM by Hibernate Tools 3.3.0.GA

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * PhysicalFile generated by hbm2java
 */
@Entity
@Table(name = "physical_file")
public class PhysicalFile implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6332628967783873305L;
	private long id;
	private FileType fileType;
	private User userByUserCreatedId;
	private User userByUserModifiedId;
	private String fileName;
	private String fileSize;
	private Date dateCreated;
	private Date dateModified;
	private Set<ListingPhysicalFileXref> listingPhysicalFileXrefs = new HashSet<ListingPhysicalFileXref>(0);

	public PhysicalFile() {
	}

	public PhysicalFile(long id, FileType fileType, User userByUserCreatedId,
			User userByUserModifiedId) {
		this.id = id;
		this.fileType = fileType;
		this.userByUserCreatedId = userByUserCreatedId;
		this.userByUserModifiedId = userByUserModifiedId;
	}

	public PhysicalFile(long id, FileType fileType, User userByUserCreatedId,
			User userByUserModifiedId, String fileName, String fileSize,
			Date dateCreated, Date dateModified,
			Set<ListingPhysicalFileXref> listingPhysicalFileXrefs) {
		this.id = id;
		this.fileType = fileType;
		this.userByUserCreatedId = userByUserCreatedId;
		this.userByUserModifiedId = userByUserModifiedId;
		this.fileName = fileName;
		this.fileSize = fileSize;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
		this.listingPhysicalFileXrefs = listingPhysicalFileXrefs;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "file_type_id", nullable = false)
	public FileType getFileType() {
		return this.fileType;
	}

	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_created_id", nullable = false)
	public User getUserByUserCreatedId() {
		return this.userByUserCreatedId;
	}

	public void setUserByUserCreatedId(User userByUserCreatedId) {
		this.userByUserCreatedId = userByUserCreatedId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_modified_id", nullable = false)
	public User getUserByUserModifiedId() {
		return this.userByUserModifiedId;
	}

	public void setUserByUserModifiedId(User userByUserModifiedId) {
		this.userByUserModifiedId = userByUserModifiedId;
	}

	@Column(name = "file_name", length = 50)
	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Column(name = "file_size", length = 50)
	public String getFileSize() {
		return this.fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_created", length = 19)
	public Date getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_modified", length = 19)
	public Date getDateModified() {
		return this.dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "physicalFile")
	public Set<ListingPhysicalFileXref> getListingPhysicalFileXrefs() {
		return this.listingPhysicalFileXrefs;
	}

	public void setListingPhysicalFileXrefs(
			Set<ListingPhysicalFileXref> listingPhysicalFileXrefs) {
		this.listingPhysicalFileXrefs = listingPhysicalFileXrefs;
	}

}
