package com.flowers.server.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "consumer_landing_ad")
public class ConsumerLandingAd implements java.io.Serializable {
	private static final long serialVersionUID = -1174961730852856327L;
	private long id;
	private String fileName;
	private String website;

		
	public ConsumerLandingAd() {
	}

	public ConsumerLandingAd(long id, String fileName) {
		this.id = id;
		this.fileName = fileName;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public void setFileName(String fileName){
		this.fileName = fileName;
	}

	@Column(name = "file_name", length = 255)
	public String getFileName() {
		return this.fileName;
	}

	@Column(name = "website", length = 255)
	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}
	

}

