package com.flowers.server.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;
import org.hibernate.search.annotations.TermVector;

@Entity
@Indexed
@Table(name = "banner_ad")
public class BannerAd implements java.io.Serializable {
	private static final long serialVersionUID = -1174961730852856327L;
	private long id;
	private String shopCode;

		
	public BannerAd() {
	}

	public BannerAd(long id, String shopCode) {
		this.id = id;
		this.shopCode = shopCode;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public void setShopCode(String shopCode){
		this.shopCode = shopCode;
	}

	@Field(name="shop_code",store=Store.YES,index=Index.YES,termVector=TermVector.NO)
	@Column(name = "shopCode", length = 8)
	public String getShopCode() {
		return this.shopCode;
	}
	

}

