package com.flowers.server.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.search.annotations.ClassBridge;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;

import com.flowers.server.search.LikeDislikeFieldBridge;

@Entity
@Indexed
@ClassBridge(name="likeDislikeBridge",index=Index.YES,store=Store.YES,impl =LikeDislikeFieldBridge.class)
@Table(name = "like_dislike")
public class LikeDislike implements java.io.Serializable {
	private static final long serialVersionUID = 2450792959741759230L;
	private long id;
	private long shop_id;
	private String shopCode;
	private String username;
	private boolean liked;
	
	public LikeDislike() {}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public long getId() {
		return this.id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@Column(name = "shop_id")
	public long getShopId() {
		return shop_id;
	}
	public void setShopId(long shop_id) {
		this.shop_id = shop_id;
	}
	@Column(name = "shop_code")
	public String getShopCode() {
		return this.shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	@Column(name = "username")
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	@Column(name = "liked")
	public boolean isLiked() {
		return liked;
	}
	public void setLiked(boolean liked) {
		this.liked = liked;
	}
	
}
