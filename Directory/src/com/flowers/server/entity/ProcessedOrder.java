/*
 * Copyright (C) 2011 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.flowers.server.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;
import org.hibernate.search.annotations.TermVector;


@Entity
@Indexed
@Table(name = "processed_order")
public class ProcessedOrder {
	private long id;
	private String sendingShop;
	private String receivingShop;
	private String fulfillingShop;
	private String deliveryDate;
	private String recipientName;
	private String recipientAddress;
	private String trackingNumber;
	private String message;
	private long timestamp;
	private Double price;
	private Date createdDate;
	
	public ProcessedOrder() {}
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@Column(name = "sending_shop", length = 15)
	@Field(name="sending_shop",store=Store.YES,index=Index.YES,termVector=TermVector.NO)
	public String getSendingShop() {
		return sendingShop;
	}
	public void setSendingShop(String sendingShop) {
		this.sendingShop = sendingShop;
	}
	@Column(name = "receiving_shop", length = 15)
	@Field(name="receiving_shop",store=Store.YES,index=Index.YES,termVector=TermVector.NO)
	public String getReceivingShop() {
		return receivingShop;
	}
	public void setReceivingShop(String receivingShop) {
		this.receivingShop = receivingShop;
	}
	@Column(name = "fulfilling_shop", length = 15)
	@Field(name="fulfilling_shop",store=Store.YES,index=Index.YES,termVector=TermVector.NO)
	public String getFulfillingShop() {
		return fulfillingShop;
	}
	public void setFulfillingShop(String fulfillingShop) {
		this.fulfillingShop = fulfillingShop;
	}
	@Column(name = "delivery_date", length = 25)
	@Field(name="delivery_date",store=Store.YES,index=Index.YES,termVector=TermVector.NO)
	public String getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	@Column(name = "recipient_name", length = 100)
	@Field(name="recipient_name",store=Store.YES,index=Index.YES,termVector=TermVector.NO)
	public String getRecipientName() {
		return recipientName;
	}
	public void setRecipientName(String recipient) {
		this.recipientName = recipient;
	}
	@Column(name = "recipient_address", length = 100)
	@Field(name="recipient_address",store=Store.YES,index=Index.YES,termVector=TermVector.NO)
	public String getRecipientAddress() {
		return recipientAddress;
	}
	public void setRecipientAddress(String recipientAddress) {
		this.recipientAddress = recipientAddress;
	}
	@Column(name = "tracking_number", length = 35)
	@Field(name="tracking_number",store=Store.YES,index=Index.YES,termVector=TermVector.NO)
	public String getTrackingNumber() {
		return trackingNumber;
	}
	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}
	@Column(name = "message", length = 1024)
	@Field(name="message",store=Store.YES,index=Index.YES,termVector=TermVector.NO)
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Column(name = "timestamp", length = 25)
	@Field(name="timestamp",store=Store.YES,index=Index.YES,termVector=TermVector.NO)
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	@Column(name = "price")
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	@Column(name = "created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}	
}
