package com.flowers.server.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "route4me_intake")
public class Route4MeIntake {
	private long id;
	private String shopCode;
	private String shopName;
	private String contact;
	private String timezone;
	private String email;
	private String password;
	private String secretPassword;
	private Integer bloomlinkProcessed;
	private Integer route4MeProcessed;
	private Integer attempts;
	private String memberId;
	private String apiKey;
	private String createdDate;

	
	public Route4MeIntake() {}
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@Column(name = "shop_code", length = 8)
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	@Column(name = "shop_name", length = 200)
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	@Column(name = "contact", length = 200)
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	@Column(name = "timezone", length = 25)
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	@Column(name = "email", length = 200)
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Column(name = "password", length = 100)
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Column(name = "secret_password", length = 100)
	public String getSecretPassword() {
		return secretPassword;
	}
	public void setSecretPassword(String secretPassword) {
		this.secretPassword = secretPassword;
	}
	@Column(name = "bloomlink_processed", length = 1)
	public Integer getBloomlinkProcessed() {
		return bloomlinkProcessed;
	}
	public void setBloomlinkProcessed(Integer bloomlinkProcessed) {
		this.bloomlinkProcessed = bloomlinkProcessed;
	}
	@Column(name = "route4me_processed", length = 25)
	public Integer getRoute4MeProcessed() {
		return route4MeProcessed;
	}
	public void setRoute4MeProcessed(Integer route4MeProcessed) {
		this.route4MeProcessed = route4MeProcessed;
	}
	@Column(name = "attempts", length = 1)
	public Integer getAttempts() {
		return attempts;
	}
	public void setAttempts(Integer attempts) {
		this.attempts = attempts;
	}
	@Column(name = "member_id", length = 100)
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	@Column(name = "api_key", length = 100)
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	@Column(name = "created_date", length=100)
	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
}	
