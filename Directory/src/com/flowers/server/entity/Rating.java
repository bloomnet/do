package com.flowers.server.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.flowers.portal.util.DateUtil;

@Entity
@Table(name = "rating")
public class Rating {

	private long rating_id;
	private String reviewed_shopname;
	private long reviewer_userid;
	private String  reviewed_shopcode;
	private String  reviewer_shopname;
	private Date dateReviewed;
	private String title;
	private String comments;
	private double stars;
	private String response;
	private int helpful;
	private int abuse;
	
	public Rating() {}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "rating_id", unique = true, nullable = false)
	public long getRating_id() {
		return rating_id;
	}
	
	public void setRating_id(long rating_id) {
		this.rating_id = rating_id;
	}
	
	@Column(name = "reviewed_shop_name", nullable = false)
	public String getReviewed_shopname() {
		return reviewed_shopname;
	}
	
	public void setReviewed_shopname(String reviewed_shopname) {
		this.reviewed_shopname = reviewed_shopname;
	}
	
	@Column(name = "reviewer_user_id", nullable = false)
	public long getReviewer_userid() {
		return reviewer_userid;
	}
	
	public void setReviewer_userid(long reviewer_shopid) {
		this.reviewer_userid = reviewer_shopid;
	}
	
	@Column(name = "reviewed_shop_code", nullable = false)
	public String getReviewed_shopcode() {
		return reviewed_shopcode;
	}
	
	public void setReviewed_shopcode(String reviewed_shopcode) {
		this.reviewed_shopcode = reviewed_shopcode;
	}
	
	@Column(name = "reviewer_shop_name", nullable = false)
	public String getReviewer_shopname() {
		return reviewer_shopname;
	}
	
	public void setReviewer_shopname(String reviewer_shopname) {
		this.reviewer_shopname = reviewer_shopname;
	}
	
	@Column(name = "date_reviewed", nullable = false)
	public Date getDateReviewed() {
		return dateReviewed;
	}
	
	public void setDateReviewed(Date dateReviewed) {
		this.dateReviewed = dateReviewed;
	}
	
	@Column(name = "title", nullable = false)
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Column(name = "comments", nullable = false)
	public String getComments() {
		return comments;
	}
	
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	@Column(name = "stars", nullable = false)
	public double getStars() {
		return stars;
	}
	
	public void setStars(double stars) {
		this.stars = stars;
	}
	
	@Column(name = "response", nullable = true)
	public String getResponse() {
		return response;
	}
	
	public void setResponse(String response) {
		this.response = response;
	}
	
	@Column(name = "helpful")
	public int isHelpful() {
		return helpful;
	}
	
	public void setHelpful(int helpful) {
		this.helpful = helpful;
	}
	
	@Column(name = "abuse")
	public int isAbuse() {
		return abuse;
	}
	
	public void setAbuse(int abuse) {
		this.abuse = abuse;
	}
	
	@Override
	public String toString() {
		return "Rating [rating_id=" + rating_id + ", reviewed_shopname=" + reviewed_shopname + ", reviewer_userid=" + reviewer_userid + ", reviewed_shopcode=" + reviewed_shopcode + ", reviewer_shopname=" + reviewer_shopname + ", dateReviewed=" + DateUtil.toString(dateReviewed) + ", title=" + title
				+ ", comments=" + comments + ", stars=" + stars + ", response=" + response + ", helpful=" + helpful + ", abuse=" + abuse+ "]";
	}
	
}
