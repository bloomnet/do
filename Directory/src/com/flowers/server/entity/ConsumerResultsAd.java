package com.flowers.server.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "consumer_results_ad")
public class ConsumerResultsAd implements java.io.Serializable {
	private static final long serialVersionUID = -1174961730852856327L;
	private long id;
	private City city;
	private String fileName;
	private String website;

		
	public ConsumerResultsAd() {
	}

	public ConsumerResultsAd(long id, String fileName) {
		this.id = id;
		this.fileName = fileName;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public void setFileName(String fileName){
		this.fileName = fileName;
	}

	@Column(name = "file_name", length = 255)
	public String getFileName() {
		return this.fileName;
	}

	@Column(name = "website", length = 255)
	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "city_id")
	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}
	

}

