package com.flowers.server.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "search_log")
public class SearchLog {
	private long id;
	private long queryId;
	private String shopCode;
	private int pageNumber;
	private boolean clicked;
	
	public SearchLog() {}
	public SearchLog(long queryId, String shopCode, int pageNumber, boolean clicked) {
		this.queryId = queryId;
		this.shopCode = shopCode;
		this.pageNumber = pageNumber;
		this.clicked = clicked;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@Column(name = "shop_code")
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	@Column(name = "page_number")
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	@Column(name = "query_id")
	public long getQueryId() {
		return queryId;
	}
	public void setQueryId(long queryId) {
		this.queryId = queryId;
	}
	@Column(name = "clicked")
	public boolean isClicked() {
		return clicked;
	}
	public void setClicked(boolean clicked) {
		this.clicked = clicked;
	}	
}
