package com.flowers.server.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "side_ad")
public class SideAd implements java.io.Serializable {
	private static final long serialVersionUID = -1174961730852856327L;
	private long id;
	private String shopCode;
	private String fileName;

		
	public SideAd() {
	}

	public SideAd(long id, String shopCode) {
		this.id = id;
		this.shopCode = shopCode;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public void setShopCode(String shopCode){
		this.shopCode = shopCode;
	}
	
	@Column(name = "shopCode", length = 8)
	public String getShopCode() {
		return this.shopCode;
	}
	
	public void setFileName(String fileName){
		this.fileName = fileName;
	}

	@Column(name = "fileName")
	public String getFileName() {
		return this.fileName;
	}
	

}

