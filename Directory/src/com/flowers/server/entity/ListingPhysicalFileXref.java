package com.flowers.server.entity;

// Generated Jan 31, 2011 3:27:19 PM by Hibernate Tools 3.3.0.GA

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * ListingPhysicalFileXref generated by hbm2java
 */
@Entity
@Table(name = "listing_physical_file_xref")
public class ListingPhysicalFileXref implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4320197527608907836L;
	private long id;
	private PhysicalFile physicalFile;
	private Listing listing;

	public ListingPhysicalFileXref() {
	}

	public ListingPhysicalFileXref(long id, PhysicalFile physicalFile,
			Listing listing) {
		this.id = id;
		this.physicalFile = physicalFile;
		this.listing = listing;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "physical_file_id", nullable = false)
	public PhysicalFile getPhysicalFile() {
		return this.physicalFile;
	}

	public void setPhysicalFile(PhysicalFile physicalFile) {
		this.physicalFile = physicalFile;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "listing_id", nullable = false)
	public Listing getListing() {
		return this.listing;
	}

	public void setListing(Listing listing) {
		this.listing = listing;
	}

}
