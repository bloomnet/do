package com.flowers.server.responseobjects;

import com.fasterxml.jackson.annotation.JsonInclude;

/* (non-Javadoc)
 * This is the response provided by an attempt to retrieve listing data. This class
 * is automatically converted and provided to the end user as a JSON response by Jackson
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class JSONListingResponse extends JSONResponse {
		
	private String listingEntryCode;
	private String listingRotate;
	private String listingRevListing;
	private String listingCustomListing;
	private String listingRevAd;
	private String shopMinimumOrder;
	private String shopDeliveryFee;
	private String listingDate;
	private String listingFreeAd;
	private String listingTextColor;
	private String listingHighlightColor;
	private String listingVideoColor;
	private String listingImageCount;
	private String listingStatus;
	private String listingDeliveryCharge;
	private String listingType;
	private String listingAdSize;
	private String listingCity;
	private String listingState;
	private String listingStateCode;
	private String shopLatitude;
	private String shopLongitude;
	private String listingMinimum;
	private String shopOpenSunday;
	private String shopCode;
	private String shopContact;
	private String shopPhone;
	private String shopFax;
	private String shopCity;
	private String shopState;
	private String shopName;
	private String shopProducts;
	private String facilitiesServiced;
	private String shopProductMinimums;
	private String shopAddress1;
	private String shopAddress2;
	private String shopZip;
	private String shopWebsite;
	private String shopLogo;
	private String socialMedia;
	private String consumerImage;
	private String shopCustomProducts;
	private String listingBid;
	private String listingCitySponsorAd;
	private String listingCityPreferredAd;
	
	public String getListingEntryCode() {
		return listingEntryCode;
	}
	public void setListingEntryCode(String listingEntryCode) {
		this.listingEntryCode = listingEntryCode;
	}
	public String getListingRotate() {
		return listingRotate;
	}
	public void setListingRotate(String listingRotate) {
		this.listingRotate = listingRotate;
	}
	public String getListingRevListing() {
		return listingRevListing;
	}
	public void setListingRevListing(String listingRevListing) {
		this.listingRevListing = listingRevListing;
	}
	public String getListingCustomListing() {
		return listingCustomListing;
	}
	public void setListingCustomListing(String listingCustomListing) {
		this.listingCustomListing = listingCustomListing;
	}
	public String getListingRevAd() {
		return listingRevAd;
	}
	public void setListingRevAd(String listingRevAd) {
		this.listingRevAd = listingRevAd;
	}
	public String getShopMinimumOrder() {
		return shopMinimumOrder;
	}
	public void setShopMinimumOrder(String shopMinimumOrder) {
		this.shopMinimumOrder = shopMinimumOrder;
	}
	public String getShopDeliveryFee() {
		return shopDeliveryFee;
	}
	public void setShopDeliveryFee(String shopDeliveryFee) {
		this.shopDeliveryFee = shopDeliveryFee;
	}
	public String getListingDate() {
		return listingDate;
	}
	public void setListingDate(String listingDate) {
		this.listingDate = listingDate;
	}
	public String getListingFreeAd() {
		return listingFreeAd;
	}
	public void setListingFreeAd(String listingFreeAd) {
		this.listingFreeAd = listingFreeAd;
	}
	public String getListingTextColor() {
		return listingTextColor;
	}
	public void setListingTextColor(String listingTextColor) {
		this.listingTextColor = listingTextColor;
	}
	public String getListingHighlightColor() {
		return listingHighlightColor;
	}
	public void setListingHighlightColor(String listingHighlightColor) {
		this.listingHighlightColor = listingHighlightColor;
	}
	public String getListingVideoColor() {
		return listingVideoColor;
	}
	public void setListingVideoColor(String listingVideoColor) {
		this.listingVideoColor = listingVideoColor;
	}
	public String getListingImageCount() {
		return listingImageCount;
	}
	public void setListingImageCount(String listingImageCount) {
		this.listingImageCount = listingImageCount;
	}
	public String getListingStatus() {
		return listingStatus;
	}
	public void setListingStatus(String listingStatus) {
		this.listingStatus = listingStatus;
	}
	public String getListingDeliveryCharge() {
		return listingDeliveryCharge;
	}
	public void setListingDeliveryCharge(String listingDeliveryCharge) {
		this.listingDeliveryCharge = listingDeliveryCharge;
	}
	public String getListingType() {
		return listingType;
	}
	public void setListingType(String listingType) {
		this.listingType = listingType;
	}
	public String getListingAdSize() {
		return listingAdSize;
	}
	public void setListingAdSize(String listingAdSize) {
		this.listingAdSize = listingAdSize;
	}
	public String getListingCity() {
		return listingCity;
	}
	public void setListingCity(String listingCity) {
		this.listingCity = listingCity;
	}
	public String getListingStateCode() {
		return listingStateCode;
	}
	public void setListingStateCode(String listingStateCode) {
		this.listingStateCode = listingStateCode;
	}
	public String getListingState() {
		return listingState;
	}
	public void setListingState(String listingState) {
		this.listingState = listingState;
	}
	public String getShopLatitude() {
		return shopLatitude;
	}
	public void setShopLatitude(String shopLatitude) {
		this.shopLatitude = shopLatitude;
	}
	public String getShopLongitude() {
		return shopLongitude;
	}
	public void setShopLongitude(String shopLongitude) {
		this.shopLongitude = shopLongitude;
	}
	public String getListingMinimum() {
		return listingMinimum;
	}
	public void setListingMinimum(String listingMinimum) {
		this.listingMinimum = listingMinimum;
	}
	public String getShopOpenSunday() {
		return shopOpenSunday;
	}
	public void setShopOpenSunday(String shopOpenSunday) {
		this.shopOpenSunday = shopOpenSunday;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopContact() {
		return shopContact;
	}
	public void setShopContact(String shopContact) {
		this.shopContact = shopContact;
	}
	public String getShopPhone() {
		return shopPhone;
	}
	public void setShopPhone(String shopPhone) {
		this.shopPhone = shopPhone;
	}
	public String getShopFax() {
		return shopFax;
	}
	public void setShopFax(String shopFax) {
		this.shopFax = shopFax;
	}
	public String getShopCity() {
		return shopCity;
	}
	public void setShopCity(String shopCity) {
		this.shopCity = shopCity;
	}
	public String getShopState() {
		return shopState;
	}
	public void setShopState(String shopState) {
		this.shopState = shopState;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopProducts() {
		return shopProducts;
	}
	public void setShopProducts(String shopProducts) {
		this.shopProducts = shopProducts;
	}
	public String getFacilitiesServiced() {
		return facilitiesServiced;
	}
	public void setFacilitiesServiced(String facilitiesServiced) {
		this.facilitiesServiced = facilitiesServiced;
	}
	public String getShopProductMinimums() {
		return shopProductMinimums;
	}
	public void setShopProductMinimums(String shopProductMinimums) {
		this.shopProductMinimums = shopProductMinimums;
	}
	public String getShopAddress1() {
		return shopAddress1;
	}
	public void setShopAddress1(String shopAddress1) {
		this.shopAddress1 = shopAddress1;
	}
	public String getShopAddress2() {
		return shopAddress2;
	}
	public void setShopAddress2(String shopAddress2) {
		this.shopAddress2 = shopAddress2;
	}
	public String getShopZip() {
		return shopZip;
	}
	public void setShopZip(String shopZip) {
		this.shopZip = shopZip;
	}
	public String getShopWebsite() {
		return shopWebsite;
	}
	public void setShopWebsite(String shopWebsite) {
		this.shopWebsite = shopWebsite;
	}
	public String getShopLogo() {
		return shopLogo;
	}
	public void setShopLogo(String shopLogo) {
		this.shopLogo = shopLogo;
	}
	public String getSocialMedia() {
		return socialMedia;
	}
	public void setSocialMedia(String socialMedia) {
		this.socialMedia = socialMedia;
	}
	public String getConsumerImage() {
		return consumerImage;
	}
	public void setConsumerImage(String consumerImage) {
		this.consumerImage = consumerImage;
	}
	public String getShopCustomProducts() {
		return shopCustomProducts;
	}
	public void setShopCustomProducts(String shopCustomProducts) {
		this.shopCustomProducts = shopCustomProducts;
	}
	public String getListingBid() {
		return listingBid;
	}
	public void setListingBid(String bid) {
		this.listingBid = bid;
	}
	public String getListingCitySponsorAd() {
		return listingCitySponsorAd;
	}
	public void setListingCitySponsorAd(String listingCitySponsorAd) {
		this.listingCitySponsorAd = listingCitySponsorAd;
	}
	public String getListingCityPreferredAd() {
		return listingCityPreferredAd;
	}
	public void setListingCityPreferredAd(String listingCityPreferredAd) {
		this.listingCityPreferredAd = listingCityPreferredAd;
	}
	
}
