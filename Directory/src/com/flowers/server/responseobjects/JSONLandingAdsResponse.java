package com.flowers.server.responseobjects;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class JSONLandingAdsResponse extends JSONResponse {
	
	private String fileName;
	private String website;
	private String adShape;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getAdShape() {
		return adShape;
	}
	public void setAdShape(String adShape) {
		this.adShape = adShape;
	}
	
}
