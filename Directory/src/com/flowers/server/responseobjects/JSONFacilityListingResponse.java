package com.flowers.server.responseobjects;

import com.fasterxml.jackson.annotation.JsonInclude;

/* (non-Javadoc)
 * This is the response provided by an attempt to retrieve listing data. This class
 * is automatically converted and provided to the end user as a JSON response by Jackson
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class JSONFacilityListingResponse extends JSONListingResponse {
	
}
