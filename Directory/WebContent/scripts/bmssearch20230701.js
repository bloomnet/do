$(document).ready(function() {
	
	$(inlineLabel());
	
	$("#facilityDD")
	.change(function() {
		populateFacilitiesDD();
		
	});
	
	$("#facilitiesDD")
	.change(function() {
       var facilityId = $("#facilitiesDD").val();
       $("#selectedFacilityId").val(facilityId);
	});

	
	if($("#searchSelState").val() !== 'State/Province'){
		var selState= $("#searchSelState").val();
		$("#searchCityAc").empty();
		var select = $("#searchCityAc");
		var select2= $("#searchCity");
		buildCitiesAC(selState,select,select2);
	}
	
	
	$('.tooltip').tooltip();	

	$('.deliverydate').datepicker();	
	$('.minicalendar').click(function(){
		$(this).parent().find('.deliverydate').focus();
	});

	$('.inputwrapper').mousedown(function () {
		$(this).find("span").html("");
	});

	$('.styled-select').mousedown(function () {
		$(this).find("span").html("");
	});
	
	$("#searchCity")
	.change(function() {
		var facState = $("#searchSelStateFac option:selected").val();
		var facCity = $("#searchCity option:selected").val();
		
		populateFacilitiesDD();
	});
	
    $(openDialog()); 
	//$( "#choose-search-layout" ).dialog( "open" );

	$( "#advanced-search" ).click(function() {
		$( "#advanced-search-layout" ).dialog({
		  draggable: false,
		  resizable: false,
		  width: 500,
		  height: 240,
		  modal: true
		});
	});
	
	
	
	$( "#continue" )
	  .button()
	  .click(function() {
		  var $addressLine1 = $("#orderAddressIptStreet1").val();
		  var $addressLine2 = $("#orderAddressIptStreet2").val();
		  var $city = $("#addressIptCity :selected").text();
		  var $state = $("#addressSelState :selected").text();
		  var $zip = $("#orderZip").val();
		  if($addressLine1 === 'Street Address 1' || $city === 'City' || $state === 'State/Province'){			  
			  if(parseFloat($("#orderTotal").text()) > 0.00){
      			$("#orderForm").submit();
      			$("#errorMessage").html("Please fill in all required fields marked with a *");
      		}else{
      			$("#errorMessage").html("Please select or enter at least one product");
      		}
		  }else{			  
			  if($addressLine2 === 'undefined' || $addressLine2 === 'Street Address 2')
				  $addressLine2 = "";
			  if($zip === 'undefined' || $zip === 'Zip / Postal Code')
				  $zip = "";
			  
			  var $url = 'services/verifyaddress?addressLine1='+$addressLine1+'&addressLine2='+$addressLine2+'&city='+$city+'&stateShort='+$state+'&zip='+$zip;
			  
			  $.ajax({
			    	
			        type: 'GET',
			        url: $url,
			        dataType: 'json',
			        
			        success: function(data) {
			        	
			        	if(data.length > 0){
			        		
			        		$( "#address-verification" ).dialog( "open" );
			        		
			            	 for(var i = data.length-1; i >= 0; i--){
			            		 var addressString = data[i];
			            		 if(i == data.length-1)
			            			 $("#recommendedaddress").prepend($('<br />'));
			            		 $("#recommendedaddress").prepend($('<div id="address'+i+'" onclick="newAddress(address'+i+')"><a href="#">'+addressString.replace(',,',',')+'</a></div>'));	 
			            	 }
			            	 
			            	 $("#originalAddress").append($addressLine1+"<br />");
			            	 if($addressLine2 !== "")
			            		 $("#originalAddress").append($addressLine2+"<br />");
			            	 if($zip !== "")
			            	 	$("#originalAddress").append($city+","+$state+","+$zip);
			            	 else
			            		 $("#originalAddress").append($city+","+$state);
			        	}else{
			        		if(parseFloat($("#orderTotal").text()) > 0.00){
			        			$("#orderForm").submit();
			        			$("#errorMessage").html("Please fill in all required fields marked with a *");
			        		}else{
			        			$("#errorMessage").html("Please select or enter at least one product");
			        		}
			       	}
		            	 
			        	
			        },
			        
			        error: function(msg) {
			        	//Do Nothing
			        }
			        
			     });
		  }
		  //$( "#address-verification2" ).dialog( "open" );
	});
	
	$("#clearallfields")
	.click(function() {
		resetForm($("#orderForm"));
		$("#fbProductTable").hide();
		$("#productTable").show();
		$("#orderTotal").text("0.00");
		inlineLabel();
	});
	
	
	$( "#accordion" ).accordion({
        collapsible: true,
        active: true,
        heightStyle: "content"
       });
	
	$(".step1b #accordion").accordion( "option", "active", 1 );
	
	//Modal Close Buttons
    $("#dialogclose").click(function () {
		$(".dialog").dialog( "close" );
    });

	//Close Modal on Overlay Click
    $(".ui-widget-overlay").click(function () {
		$(".dialog").dialog( "close" );
    });
    
    $("#layouttoggle")
    .click(function() {
    	window.location.assign("contemporary.htm?view=close&timestamp="+ new Date().getTime());
    });
    
    $("#modifySearch")
    .click(function() {
    	window.location.assign("?view=modifysearch&timestamp="+ new Date().getTime());
    });
    
    $("#residentialSearchTab")
    .click(function() {
    	$("#rightside").hide();
    	$("#advanced-search-layout").hide();
    	$("#leftside").show();
    	if($("#searchSelCountry").val() !== "1"){
	    	$("#searchSelCountry")[0].selectedIndex = 0;
	    	$("#searchSelCountry").trigger('change');
    	}
    	$("#countrySelectDiv").hide();
    	$("#searchOr").show();
    	$("#residential").html("Residential/Business Search");
    	$("#form1aArea").html("<form id=\"form1a\" style=\"margin-top: -20px;\" >");
    	
    });
    
    $("#funeralHomeSearchTab")
    .click(function() {
    	$("#leftside").hide();
    	$("#advanced-search-layout").hide();
    	$("#rightside").show();
	$("#facilityDD")[0].selectedIndex = 1;
	$("#facility").html("Funeral Home Search");
	$("#facilityNameDD option:first").text("Funeral Homes");
	if($("#searchCity option:selected").text() !== "City"){ populateFacilitiesDD(); }
    });
    
    $("#nursingHomeSearchTab")
    .click(function() {
    	$("#leftside").hide();
    	$("#advanced-search-layout").hide();
    	$("#rightside").show();
	$("#facilityDD")[0].selectedIndex = 2;
	$("#facility").html("Nursing Home Search");
	$("#facilityNameDD option:first").text("Nursing Homes");
	if($("#searchCity option:selected").text() !== "City"){ populateFacilitiesDD(); }
    });
    
    $("#hospitalSearchTab")
    .click(function() {
    	$("#leftside").hide();
    	$("#advanced-search-layout").hide();
    	$("#rightside").show();
	$("#facilityDD")[0].selectedIndex = 3;
	$("#facility").html("Hospital Search");
	$("#facilityNameDD option:first").text("Hospitals");
	if($("#searchCity option:selected").text() !== "City"){ populateFacilitiesDD(); }
    });
    
    $("#advancedSearchTab")
    .click(function() {
    	$("#leftside").hide();
    	$("#rightside").hide();
    	$("#advanced-search-layout").show();
    });
    
    $("#internationalSearchTab")
    .click(function() {
    	$("#countrySelectDiv").show();
    	if($("#searchSelCountry").val() == "1"){
    		$("#searchSelCountry")[0].selectedIndex = 1;
    		$("#searchSelCountry").trigger('change');
    	}
    	$("#residential").html("International Search");
    	$("#rightside").hide();
    	$("#advanced-search-layout").hide();
    	$("#searchOr").hide();
    	$("#form1aArea").html("<form id=\"form1a\" style=\"margin-top: -5px;\" >");
    	$("#leftside").show();
    });
    
    if($("#searchDeliveryDate").val() === ""){
    	var today = new Date();
    	if (today.getHours() > 14) {		// is it after 2:00 PM?, then use "tomorrow"
			today.setDate(today.getDate() + 1);
		}
    	var dd = today.getDate();
    	var mm = today.getMonth()+1; //January is 0!

    	var yyyy = today.getFullYear();
    	if(dd<10){dd='0'+dd;} if(mm<10){mm='0'+mm;} today = mm+'/'+dd+'/'+yyyy;
    	
    	$("#searchDeliveryDate").val(today);
    }
    
    if($("#deliveryDate2").val() === ""){
    	var today = new Date();
    	if (today.getHours() > 14) {		// is it after 2:00 PM?, then use "tomorrow"
			today.setDate(today.getDate() + 1);
		}
    	var dd = today.getDate();
    	var mm = today.getMonth()+1; //January is 0!

    	var yyyy = today.getFullYear();
    	if(dd<10){dd='0'+dd;} if(mm<10){mm='0'+mm;} today = mm+'/'+dd+'/'+yyyy;
    	
    	$("#deliveryDate2").val(today);
    }
    
    if($("#advsearchDeliveryDate").val() === ""){
    	var today = new Date();
    	if (today.getHours() > 14) {		// is it after 2:00 PM?, then use "tomorrow"
			today.setDate(today.getDate() + 1);
		}
    	var dd = today.getDate();
    	var mm = today.getMonth()+1; //January is 0!

    	var yyyy = today.getFullYear();
    	if(dd<10){dd='0'+dd;} if(mm<10){mm='0'+mm;} today = mm+'/'+dd+'/'+yyyy;
    	
    	$("#advsearchDeliveryDate").val(today);
    }
    $(openEnterOrder());
    $("#countrySelectDiv").hide();
    
    var selCountry = $("#searchSelCountry");
	if(selCountry != undefined){
		selCountry = selCountry.val();
		if(selCountry !== "1"){
			$("#countrySelectDiv").show();
	    		$("#searchSelCountry").trigger('change');
	    		$("#residential").html("International Search");
	    		$("#rightside").hide();
	    		$("#advanced-search-layout").hide();
	    		$("#leftside").show();
		}
	}
	
	var productId= $("#ordProductId option:selected").text();
	if(productId === "Fruit Bouquet"){
		$("#fbProductTable").show();
		$("#productTable").hide();
	}else{
		$("#fbProductTable").hide();
		$("#productTable").show();
	}
	
	var jcarousel = $('.jcarousel');
	if(jcarousel !== undefined){
		 $('.jcarousel').jcarousel({
			 auto:7,
	     	wrap: 'circular'
	     });
	}
    
});

function examineZip() {
		var $searchZip=$("#searchZip").val();
		var finalCity;
		if ($searchZip.length >= 5){
			var $url =  nsBloomNet.ioServer['cityStateByZip'];
			$url += $searchZip + '/city?timetamp='+ new Date().getTime();
			
			$.getJSON( $url, function( data ) {
				$.each( data.results, function( i, val ) {
					var city = val.id;
					var state = val['state_id'];
 		        	if (state === '') {
 		        	}else{
 		        		//$("#searchCityAc option:contains(" + city + ")").attr('selected', 'selected');
 		        		//$("#searchSelState option:contains(" + state + ")").attr('selected', 'selected');
 		        		//$("#stateorprovince option:contains(" + state + ")").attr('selected', 'selected');
 		        		$("#searchSelState option[value='"+state+"']").attr('selected','selected');
 		        		$("#stateorprovince option[value='"+state+"']").attr('selected','selected');
 		        		finalCity = city;
 		        	}

				});
			}).done(function(){
				var selState= $("#searchSelState").val();
	    		var select = $("#searchCityAc");
	    		var select2= $("#city");
	    		var url =  nsBloomNet.ioServer['citiesByState'];
	    		var stateCode = selState;
	    		if (!stateCode) {
	    			return;
	    		}
	    		if (nsBloomNet.ioServer['servertype'] === 'server') {
	    			url += stateCode + '/city?timestamp='+ new Date().getTime();
	    		}else{
	    			url += '?timestamp='+ new Date().getTime();
	    		}

	    		$.ajax({

	    		        type: 'GET',
	    		        url: url,
	    		        dataType: 'json',

	    		        success: function(data) {
	    		        	buildCitiesACCont(data,select,select2);

	    		        },

	    		        error: function(msg) {
	    		        	// do nothing
	    		        }

	    		     }).done(function() {
	    		    	 $("#searchCityAc option[value='"+finalCity+"']").attr('selected','selected');
	 		    		$("#city option[value='"+finalCity+"']").attr('selected','selected');
	    		     });
		     });
		}
	}
	
function examineOrderZip() {
	var $searchZip=$("#orderZip").val();
	var finalCity;
	if ($searchZip.length >= 5 && $searchZip !== "Zip / Postal Code"){
		var $url =  nsBloomNet.ioServer['cityStateByZip'];
		$url += $searchZip + '/city?timetamp='+ new Date().getTime();
		
		$.getJSON( $url, function( data ) {
			$.each( data.results, function( i, val ) {
				var city = val.id;
				var state = val['state_id'];
				var cityname = val.name;
				var statename = val['state_name'];
		        	if (state === '') {
		        	}else{
		        		$("#orderSelCity").val(cityname);
		        		$("#orderSelState").val(statename);
		        		$("#searchZip").val($searchZip);
		        		$("#searchSelState option[value='"+state+"']").attr('selected','selected');
		        		$("#addressSelState option[value='"+state+"']").attr('selected','selected');
		        		finalCity = city;
		        	}

			});
		}).done(function(){
			var selState= $("#addressSelState").val();
    		var select = $("#addressIptCity");
    		var select2= $("#searchCityAc");
    		var url =  nsBloomNet.ioServer['citiesByState'];
    		var stateCode = selState;
    		if (!stateCode) {
    			return;
    		}
    		if (nsBloomNet.ioServer['servertype'] === 'server') {
    			url += stateCode + '/city?timestamp='+ new Date().getTime();
    		}else{
    			url += '?timestamp='+ new Date().getTime();
    		}

    		$.ajax({

    		        type: 'GET',
    		        url: url,
    		        dataType: 'json',

    		        success: function(data) {
    		        	buildCitiesACCont(data,select,select2);

    		        },

    		        error: function(msg) {
    		        	// do nothing
    		        }

    		     }).done(function() {
    		    	 $("#addressIptCity option[value='"+finalCity+"']").attr('selected','selected');
    		     });
	     });
	}
}

function examineFacZip() {
	var $searchZip=$("#zipcode").val();
	var finalCity;
	var finalState;
	if ($searchZip.length >= 5){
		var $url =  nsBloomNet.ioServer['cityStateByZip'];
		$url += $searchZip + '/city?timetamp='+ new Date().getTime();
		
		$.getJSON( $url, function( data ) {
			$.each( data.results, function( i, val ) {
				var city = val.id;
				var state = val['state_id'];
		        	if (state === '') {
		        	}else{
		        		//$("#searchCityAc option:contains(" + city + ")").attr('selected', 'selected');
		        		//$("#searchSelState option:contains(" + state + ")").attr('selected', 'selected');
		        		//$("#stateorprovince option:contains(" + state + ")").attr('selected', 'selected');
		        		$("#searchSelStateFac option[value='"+state+"']").attr('selected','selected');
		        		finalCity = city;
		        		finalState = state;
		        	}

			});
		}).done(function(){
			var selState= $("#searchSelStateFac").val();
    		var select = $("#searchCity");
    		var select2= $("#city");
    		var url =  nsBloomNet.ioServer['citiesByState'];
    		var stateCode = selState;
    		if (!stateCode) {
    			return;
    		}
    		if (nsBloomNet.ioServer['servertype'] === 'server') {
    			url += stateCode + '/city?timestamp='+ new Date().getTime();
    		}else{
    			url += '?timestamp='+ new Date().getTime();
    		}

    		$.ajax({

    		        type: 'GET',
    		        url: url,
    		        dataType: 'json',

    		        success: function(data) {
    		        	buildCitiesACCont(data,select,select2);

    		        },

    		        error: function(msg) {
    		        	// do nothing
    		        }

    		     }).done(function() {
    		    	 $("#searchCity option[value='"+finalCity+"']").attr('selected','selected');
 		    		$("#city option[value='"+finalCity+"']").attr('selected','selected');
 		    		populateFacilitiesDD();
 		    		
    		     });
	     });
	}
}

function populateFacility(oCityId,oStateId){
	var url = nsBloomNet.ioServer['facilitytypes'];
	

	
	if (nsBloomNet.ioServer['servertype'] === 'server') {
		url += 'stateid=' + oStateId + '&city='+ oCityId + '&cacheBuster='+ new Date();
	}
	$.ajax({

        type: 'GET',
        url: url,
        dataType: 'json',

        success: function(data) {
        	populateFaciltyTypeDDCont(data);

        },

        error: function(msg) {
        	// do nothing
        }

     });
}

function populateFaciltyTypeDDCont(data) {
	var oRecords = data.results;
	var oCount = oRecords.length;
	//if (oCount === 0) {
		//return;
	//}
	
	var oOptions = [];
	var oRecord;
	var select = $("#facilityDD");
	select.empty();
	select.append("<option value=''>Facility Type</option>");
	oOptions.push('<input type="text" class="focus" style="height: 0; border-width: 0; margin-top: -10px;" />');
	for (var oIdx=0; oIdx<oCount; oIdx++) {
		//oHtml = '';
		oRecord = oRecords[oIdx];
		/*oHtml = ('<div class="ddWidget_item mouseable clickable">' + oRecord['name'] + '</div>');
		oOptions.push(oHtml);
		oValues.push(oRecord['id']);*/
		select.append($('<option></option>').val(oRecord['id']).html(oRecord['name']));

	}
	
}

function populateFacilitiesDD() {

		var oFacilityType = $("#facilityDD option:selected").val();

		var cityId = $("#searchCity option:selected").val();
		var stateId = $("#searchSelStateFac option:selected").val();
		
		var url =  nsBloomNet.ioServer['facilitieslist'];
		
		if (nsBloomNet.ioServer['servertype'] === 'server') {
			url += oFacilityType + '/facility?stateid=' + stateId + '&city='+ cityId;
		}
		
		
		$.ajax({

	        type: 'GET',
	        url: url,
	        dataType: 'json',

	        success: function(data) {
	        	populateFacilitiesDDCont(data);

	        },

	        error: function(msg) {
	        	// do nothing
	        }

	     });
}

function populateFacilitiesDDCont(data) {
	var oRecords = data.results;
	var oCount = oRecords.length;
	//if (oCount === 0) {
		//return;
	//}
	var oOptions = [];
	var oHtml;
	var oRecord;
	var select = $("#facilitiesDD");
	select.find('option').not(':first').remove();
	oOptions.push('<input type="text" class="focus" style="height: 0; border-width: 0; margin-top: -10px;" />');
	for (var oIdx=0; oIdx<oCount; oIdx++) {
		
		oRecord = oRecords[oIdx];
		oHtml = oRecord['name'] + ' -- ';
		oHtml += oRecord['address1'] + ' -- ';
		oHtml += oRecord['city'] + ', ' + oRecord['state'] + ', ' + oRecord['zip'] + ' -- ';
		if(oRecord['phone'] !== "null")oHtml += oRecord['phone'];
		select.append($('<option></option>').val(oRecord['id']).html(oHtml));
	}
	
	if(oCount == 0){
		$("#errorMessageSearchFac").html("No facilities for this search type were found for that city/state/zip. Please try the Residential Search instead.");
	}else{
		$("#errorMessageSearchFac").html("");

	}
	
}


	function buildCitiesAC(selState,select,select2) {
		$("#searchforaflorist").attr("disabled", "disabled");
		var url =  nsBloomNet.ioServer['citiesByState'];
		var stateCode = selState;
		if (!stateCode) {
			return;
		}
		if (nsBloomNet.ioServer['servertype'] === 'server') {
			url += stateCode + '/city?timestamp='+ new Date().getTime();
		}else{
			url += '?timestamp='+ new Date().getTime();
		}

		$.ajax({

		        type: 'GET',
		        url: url,
		        dataType: 'json',

		        success: function(data) {
		        	buildCitiesACCont(data,select,select2);

		        },

		        error: function(msg) {
		        	// do nothing
		        }

		     }).done(function() {
		 		if($("#searchCity option:selected").text() !== "City"){
		 			populateFacilitiesDD();			 		}
		     });
	}

	function buildCitiesACCont(data,select,select2){
		var oRecords = data.results;
		var oCount = oRecords.length;
		if (oCount === 0) {
			return;
		}
	
		var oRecord;
		var oPreviousCity;
		var searchCity = $("#currentCity").val();
		var oHtml = '';
		var selCountry= $("#searchSelCountry").val();
		var addressCountry= $("#addressSelCountry").val();
		var match = 0;
	    if((selCountry != undefined && selCountry !== "1" && selCountry !== "2" && selCountry !== "3") || (selCountry == undefined && addressCountry != undefined && addressCountry !== "1" && addressCountry !== "2" && addressCountry !== "3")){
	    	for (var oIdx=0; oIdx<1; oIdx++) {
				oRecord = oRecords[oIdx];
				if(oRecord['name'] !== oPreviousCity){
						oHtml += '<option selected="selected" value="'+oRecord['id']+'">'+oRecord['name']+'</option>';
						match = 1;
				}
				oPreviousCity = oRecord['name'];
			}
	    }else{
	    	for (var oIdx=0; oIdx<oCount; oIdx++) {
				oRecord = oRecords[oIdx];
				if(oRecord['name'] !== oPreviousCity){
					if(searchCity === oRecord['name']){
						oHtml += '<option selected="selected" value="'+oRecord['id']+'">'+oRecord['name']+'</option>';
						match = 1;
					}else{
						oHtml += '<option value="'+oRecord['id']+'">'+oRecord['name']+'</option>';
					}
				}
				oPreviousCity = oRecord['name'];
			}
	    }
		if(match == 1){
			oHtml = '<option value="">City</option>' + oHtml;

		}else{
			oHtml = oHtml = '<option selected="selected" value="">City</option>' + oHtml;

		}
		$("#searchforaflorist").removeAttr("disabled");
		select.empty();
		select.append(oHtml);
		select2.empty();
		select2.append(oHtml);
	}
	
	function selectState(){
		
		var selState= $("#searchSelState").val();
		$("#searchCityAc").empty();
 		var select = $("#searchCityAc");
 		var select2= $("#city");
 		buildCitiesAC(selState,select,select2);
		
	}

	function changeCountry(){
	  var selCountry= $("#searchSelCountry").val();
	  $("#searchSelState").empty();
	  $("#orderSelCity").empty();
	  $("#orderSelCity").append('<option selected="selected" value="">City</option>');
	  if(selCountry !== "1" && selCountry !== "2" && selCountry !== "3"){
	      $("#searchZip").val("");
	  }else{
	      $("#searchZip").val("Zip / Postal Code");
	  }
	  selectCountry();
	}
	
	function selectCountry(){
		
		var selCountry= $("#searchSelCountry").val();
 		var select = $("#searchSelState");
 		
 		var url =  nsBloomNet.ioServer['citiesByState'];
		var countryId = selCountry;
		
		if (nsBloomNet.ioServer['servertype'] === 'server') {
			url += countryId + '?timestamp='+ new Date().getTime();
		}
		$.ajax({

		        type: 'GET',
		        url: url,
		        dataType: 'json',

		        success: function(data) {
		        	buildStates(data,select);

		        },

		        error: function(msg) {
		        	// do nothing
		        }

		     });

		
	}
	
	function buildStates(data,select){
		var oRecords = data.results;
		var oCount = oRecords.length;
		$("#searchCityAc").empty();
		$("#searchCityAc").append('<option selected="selected" value="">City</option>');
		var selCountry= $("#searchSelCountry").val();
		if (oCount === 0) {
			return;
		}
		var oRecord;
		var oHtml = '';
		if(selCountry === "1" || selCountry === "2" || selCountry === "3")
			oHtml = '<option selected="selected" value="">State/Province</option>';
		for (var oIdx=0; oIdx<oCount; oIdx++) {
			oRecord = oRecords[oIdx];
			if(selCountry !== "1" && selCountry !== "2" && selCountry !== "3" && oIdx == 0){
				oHtml += '<option value="'+oRecord['id']+'" selected="selected">'+oRecord['name']+'</option>';
			}else{
				oHtml += '<option value="'+oRecord['id']+'">'+oRecord['name']+'</option>';
			}
		}
		
		select.append(oHtml);
		if(selCountry !== "1" && selCountry !== "2" && selCountry !== "3"){
			$("#orderSelState").val($("#addressSelState option:selected").text());
			selectState();
			$("#searchZip").hide();
	 		$("#searchstateorprovinceselect").hide();
	 		$("#searchCitySelect").hide();
	 		$("#searchZip").val("");
	 		internationalDate();
		}else{
			$("#searchZip").show();
	 		$("#searchstateorprovinceselect").show();
	 		$("#searchCitySelect").show();
		}
	}
	
function selectFacState(){
		
		var selState= $("#searchSelStateFac").val();
		$("#searchCity").empty();
 		var select = $("#searchCity");
 		var select2= $("#city");
 		buildCitiesAC(selState,select,select2);
		
	}
	
	function searchflorist() {
		
		var TextHighlightCss = {
		        'background': '#FFFFAA',
		        'border': '1px solid #FFAD33'
		    };

		
		var $searchZip=$("#searchZip").val();
		var $searchCityAc=$("#searchCityAc option:selected").text();
		var $searchSelState=$("#searchSelState option:selected").text();
		var $productId=$("#productId option:selected").val();
		var $searchDeliveryDate=$("#searchDeliveryDate").val();
		var $searchAdditionalCities=$("#searchAdditionalCities").val();
		 if($searchCityAc === "City"){
			 $("#errorMessageSearch").html("Please enter a valid zip code or select a City/State");
			 $("#searchCityAc option:selected").addClass("error");
			 $("#searchSelState").addClass("error");

		 }else{
			 $("#errorMessageSearch").html("");
			 var $url = 'services/search?fromBMS=yes&searchDeliveryDate='+$searchDeliveryDate+'&searchZip='+$searchZip+'&searchSelState='+$searchSelState+'&searchCityAc='+$searchCityAc+'&searchAdditionalCities='+$searchAdditionalCities+'&productId='+$productId+'&timestamp='+ new Date().getTime();
			 $.ajax({
			    	
			        type: 'POST',
			        url: $url,
			        dataType: 'text',
			        
			        success: function(data) {
		            	 window.location.href = "contemporary.htm?view=searchResults&from=bmsSearch&timestamp="+ new Date().getTime();
			        	
			        },
			        
			        error: function(msg) {
			        	alert(msg);
			        }
			        
			     }); 
		 }
		 
	}
	
	function searchfacility() {
		
		var $searchZip=$("#zipcode").val();
		var $searchCityAc=$("#searchCity option:selected").text();
		var $searchSelState=$("#searchSelStateFac option:selected").text();
		var $searchDeliveryDate=$("#deliveryDate2").val();
		var $searchAdditionalCities=$("#searchAdditionalCities").val();
		var facilityId=$("#selectedFacilityId").val();
		var facilityData = $("#facilitiesDD option:selected").text().replace(/\#+/g, '%23');
		 if(facilityData === "Facilities" || facilityData === "Funeral Homes" || facilityData === "Nursing Homes" || facilityData === "Hospitals"){
			 $("#errorMessageSearchFac").html("Please Select a Facility");
		 }else{
			 
			 $("#errorMessageSearchFac").html("");
			 
			 $.ajax({
			    	
			        type: 'POST',
			        url: 'services/search?fromBMS=yes&searchDeliveryDate='+$searchDeliveryDate+'&searchZip='+$searchZip+'&searchSelState='+$searchSelState+'&searchCityAc='+$searchCityAc+'&searchAdditionalCities='+$searchAdditionalCities+'&timestamp='+ new Date().getTime()+'&facility='+ facilityId+'&facilityData='+ facilityData,
			        dataType: 'text',
			        
			        success: function(data) {
		            	 window.location.href = "contemporary.htm?view=searchResults&from=bmsSearch&timestamp="+ new Date().getTime();
			        	
			        },
			        
			        error: function(msg) {
			        	alert(msg);
			        }
			        
			     }); 
		 }
		 
	}
	

	
	function openDialog(){

		var pref= $("#pref").val();
		if (pref==0){
     	//alert(pref);
			$( "#choose-search-layout" ).dialog( "open" );
		}

		
	}
	
	
	
	function addOrderLine() {
		//var oLineTemplate = Y$.one('#newOrderLineTemplate').get('innerHTML');
		//var newInput = $("<input name='new_field' type='text'>");
		var numberProducts = parseInt($("#productLineCount").val());
		var newNumber = numberProducts + 1;
		var newInput = $("<div class='orderLine' id='orderLine"+newNumber+"'><input type='text' name='productCode' id='productCode'  title='Product Code'/>" +
				"<input type='text' name='productQuantity' id='productQuantity' class='productQuantity' title='Quantity' onkeypress='return onlyNumbers();' onchange='calcTotal();' />" +
				"<input type='text' name='productDescription' id='productDescription' class='productDescription' title='Product Description' /> " +
				"<input type='text' name='productPrice' id='productPrice' class='productPrice' title='Price' onkeypress='return onlyNumbers();' onchange='calcTotal();' />" +
				"<a class='clickable green' onclick='removeOrderLine(\"orderLine"+newNumber+"\")'>&nbsp;Delete</a>" +
				"<br/>" +
				"</div>");

		$('#productTable').append(newInput);
		
		$("#productLineCount").val(newNumber);


		
		//Y$.one('#productTable').append(oLineTemplate);
		/*var oProdTable = $('#productTable');
		var oProductLines = oProdTable.all('.orderLine');
		var oNewLine = oProductLines.item(oProductLines.size()-1);
		
		
		oNewLine.one('.productDescription').on('change',editProductLine);
		oNewLine.one('.productPrice').on('change',editPrice);
		oNewLine.one('.productQuantity').on('change',productQuantity);
		
		oNewLine.all('input.orderField').on('focus', _toggleInputHint);
		oNewLine.all('input.orderField').on('focus', onFocus);
		oNewLine.all('input.orderField').on('blur', _toggleInputHint);
		oNewLine.all('input.orderField').on('blur', onBlur);
//		Y$.all('input.submitable').setAttribute('disabled', 'disabled').addClass('disabled');
		checkLineCount();
		*/
		
		inlineLabel();

	}
	
	function removeOrderLine(lineID){
		$("#"+lineID).remove();
		calcTotal();
	}
	
	function deleteLine(){
		//var oLine = target.ancestor('div.orderLine');
		//oLine.remove();
		$('#productTable').children("a:last-child").remove();
	}
	
	function inlineLabel(){
		$('input[title]').each(function() {
			if($(this).val() === '') {
				$(this).val($(this).attr('title')); 
				}
			$(this).focus(function() {
				if($(this).val() === $(this).attr('title')) {
					$(this).val('').addClass('focused'); 
					}
				});
			$(this).blur(function() {
				if($(this).val() === '') {
					$(this).val($(this).attr('title')).removeClass('focused'); 
					}
				});
			});
		
		$('textarea[title]').each(function() {
			if($(this).val() === '') {
				$(this).val($(this).attr('title')); 
				}
			$(this).focus(function() {
				if($(this).val() === $(this).attr('title')) {
					$(this).val('').addClass('focused'); 
					}
				});
			$(this).blur(function() {
				if($(this).val() === '') {
					$(this).val($(this).attr('title')).removeClass('focused'); 
					}
				});
			});
		
	}
	

	function submitSearchOrder(){
		
		 $.ajax({
		    	
		        type: 'POST',
		        url: '?view=summary',
		        dataType: 'text',
		        
		        success: function(data) {
		        	
		        },
		        
		        error: function(msg) {
		        	// do nothing
		        }
		        
		     }); 
		
	}

	function newAddress(addressNumber){
		
		var $addressLine1 = "";
		var $addressLine2 = "";
		var $city = "";
		var $state = "";
		var $zip = "";
		
		if(addressNumber.textContent.split(",").length == 5){
			var $addressArray = addressNumber.textContent.split(",");
			$addressLine1 = $addressArray[0];
			$addressLine2 = $addressArray[1];
			$city = $addressArray[2];
			$state = $addressArray[3];
			$zip = $addressArray[4];
		}else{
			var $addressArray = addressNumber.textContent.split(",");
			$addressLine1 = $addressArray[0];
			$city = $addressArray[1];
			$state = $addressArray[2];
			$zip = $addressArray[3];
		}
			$("#orderAddressIptStreet1").val($addressLine1);
			$("#orderAddressIptStreet2").val($addressLine2);
			$("#addressIptCity :selected").text($city);
			$("#addressSelState :selected").text($state);
			$("#orderZip").val($zip);
			
			$( "#address-verification" ).dialog( "close" );
			$("#recommendedaddress").empty();
			$("#originalAddress").empty();
	}

	function resetForm($form) {
		   $form.find('input:text, input:password, input:file, select, textarea').val('');
		   $form.find('input:radio, input:checkbox')
		        .removeAttr('checked').removeAttr('selected');
	}

	function onlyPhoneNumbers(evt){
		var e = event || evt;
		var charCode = e.which || e.keyCode;

		if (charCode > 31 && (charCode < 48 || charCode > 57))
			if(charCode == 32 || charCode == 40 || charCode == 41 || charCode == 45 ) return true;
			else return false;

		return true;
	}

	function onlyNumbers(evt){
		var e = event || evt;
		var charCode = e.which || e.keyCode;

		if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		
		return true;
	}

	function formatPhoneNumber() {
		
		var oValue = $("#orderAddressIptPhone").val();
		var charsRe = /\(|\)|\-|\.|\ /g;
		var oStripped = oValue.replace(charsRe,'');
		var numRe = /(^-?\d\d*$)/;
		if(oValue.length ===10){
			var oPhone = '('+oStripped.substring(0,3)+')'+ ' ' + oStripped.substring(3,6) + '-' + oStripped.substring(6,10);
			$("#orderAddressIptPhone").val(oPhone);
		}
	}

	function  openEnterOrder(){
	    var emptyOrder= $("#emptyorder").val();
	    if (emptyOrder=="emptyorder"){
	    
	    	$("#accordion").accordion( "option", "active", 1);


	    }else if(emptyOrder == "dropship"){

	    	$("#accordion").accordion( "option", "active", 2);

	    }else{
	            $("#accordion").accordion( "option", "active", 0);
	    }

	    
	}

	function advancedSearch(){
		var shopcode=$("#code").val();
		var shopphone=$("#phone1").val();
		var shopname=$("#shop_name").val();
		var deliveryDate=$("#advsearchDeliveryDate").val();
		if(shopcode === "Shop Code" && shopphone === "Phone number" && shopname === "Shop Name"){
			$("#advSearchError").html("Please Enter a Shop Code or a Phone Number or a Shop Name");
		}else{
			//alert(shopcode + "/"+ shopphone + "/"+ shopname);
			 $.ajax({
			    	
			        type: 'GET',
			        url: 'services/advancesearch?fromBMS=yes&shopcode='+shopcode+'&shopphone='+shopphone+'&shopname='+shopname+'&deliverydate='+deliveryDate+'&timestamp='+ new Date().getTime(),
			        dataType: 'text',
			        
			        success: function(data) {
		            	 window.location.href = "contemporary.htm?view=searchResults&from=bmsSearch&timestamp="+ new Date().getTime();
			        	
			        },
			        
			        error: function(msg) {
		            	 //window.location.href = "?view=searchResults&from=search&timestamp="+ new Date().getTime();
			        }
			        
			     }); 
		}

		
	}

	function internationalDate(){
		var today = new Date();
		today.setHours(0, 0, 0, 0);
		var selectedDate = Date.parse($("#searchDeliveryDate").val());
		if(today.getTime() == selectedDate){
			var dd = today.getDate()+1;
	    	var mm = today.getMonth()+1; //January is 0!
	    	var yyyy = today.getFullYear();
	    	if(dd<10){dd='0'+dd;} if(mm<10){mm='0'+mm;} today = mm+'/'+dd+'/'+yyyy;
	    	$("#searchDeliveryDate").val(today);
		}
		
	}
	
	function showSearch(){
		$("#step-1a").show();
	}


