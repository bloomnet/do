function sleep(ms) {
	  return new Promise(function(resolve){ setTimeout(resolve, ms)});
}

async function addMarkers(addresses) {
	var addressLines = addresses.split("///");
	if(addressLines.length == 0){
		  addMarker(addresses);
	}else{
		  for(var ii=0; ii<addressLines.length; ii++){
			if(ii > 9){
				await sleep(1000);
			}
		  	var addressLine = addressLines[ii];
			addMarker(addressLine);
		  }	
	}
}

async function addMarker(addressLine){
	var geoCoder = new google.maps.Geocoder();
	var addressItems = addressLine.split("--");
	var shopInfo = addressItems[0];
	var address = addressItems[1];
	var shopItems = shopInfo.split(",");
	var shopCode = shopItems[0];
	var shopName = shopItems[1];
	var shopPhone = shopItems[2];
	var splitItems = address.split(",");
	var addressLine1 = "";
	var addressLine2 = "";
	var city = "";
	var state = "";
	var zip = "";
	if(splitItems.length == 5){
		addressLine1 = splitItems[0];
		addressLine2 = splitItems[1];
		city = splitItems[2];
		state = splitItems[3];
		zip = splitItems[4];
	}else{
		addressLine1 = splitItems[0];
		city = splitItems[1];
		state = splitItems[2];
		zip = splitItems[3];
	}
	var latLng;
	  var contentString = '<div style="width:175px; overflow:hidden;">'+
	  '<div id="bodyContent">'+
	  '<div style="font-weight:bold; color:#719d48;">'+shopName+'</div>'+
	  shopCode+'<br />'+
	  shopPhone+'<br />'+
	  addressLine1+'<br />'+
	  city+', '+state+', '+zip+
	  '</div>';
	  
	  var infowindow = new google.maps.InfoWindow({
		  content: contentString
		  });
	  
	geoCoder.geocode( { 'address': addressLine1+", "+city+", "+state+", "+zip}, function(results, status) {
	if (status == google.maps.GeocoderStatus.OK) {
		var lat = results[0]['geometry']['location'].lat();
		var lng = results[0]['geometry']['location'].lng();
		latLng = new google.maps.LatLng(lat,lng);
		var deliveryMarker = new google.maps.Marker({
			map: basicMap,
			position: latLng,
			title: (shopName)
		});
		google.maps.event.addListener(deliveryMarker, 'click', function() {
			infowindow.open(basicMap,deliveryMarker);
			if(previousInfoWindow != null){	
				previousInfoWindow.close();
			}
			previousInfoWindow = infowindow;
			});
	}else{
		alert(status);
	}	

					});
				
}