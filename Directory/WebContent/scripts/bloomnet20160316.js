// place "global" constants in a protected "namespace"
var nsBloomNet = {}; /* a "global namespace" for creating app specific glbal variables */
nsBloomNet.daySuffixes = [null, 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th', 'th', 'th', 'th', 'th', 'th', 'th', 'th', 'th', 'th','th', 'th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th', 'th', 'st'];
nsBloomNet.ioServer = buildUrlList();

function buildUrlList() {
	if ((location.href.indexOf('localhost') > -1 || location.href.indexOf('127.0.0.1') > -1 || location.href.indexOf('192.168.1') > -1 || location.href.indexOf('beanstalkapp') > -1) && location.href.indexOf(':8080') == -1) {
		var ioBase = 'local';
		var oUrl  = '';
		var oImageUrl = oUrl + 'resources/img/product_images/';
		var oHTMLUrl = oUrl;
		var oAdUrl = oUrl + 'resources/img/product_images/';
	} else {
		var ioBase = 'server';
		var oUrl  = '/Directory/';
		var oImageUrl = 'http://directory.bloomnet.net/bloomnet-images/product-images/';
		var oHTMLUrl = oUrl + 'services/';
		var oAdUrl = 'http://directory.bloomnet.net/bloomnet-images/ads/';
	}

		var ioTarget = {
		'local': {
			'servertype':'local',
			'data': oUrl+'data/',
			'productdetails': oUrl + 'data/productPopup.json',
			'productpanel': oHTMLUrl+'panelProductDetail.html',
			'productlist': oUrl + 'data/productlist.json',
			'productimage': oImageUrl,
			'shop': oUrl + 'data/shop.json',
			'listing': oUrl + 'data/listing.json',
			'shopproducts': oUrl + 'data/productlist.json',
			'shopminimums': oUrl + 'data/minimums.json',
			'contactuspanel': oHTMLUrl+'panelContactUs.html',
			'adcopylocation': oAdUrl,
			'facilitytypes': oUrl + 'data/facilitytypes.json',
			'facilitieslist': oUrl + 'data/facilities.json',
			'citiesByState': oUrl + 'data/cities.json',
			'search': oUrl + 'data/search.json',
			'next': oUrl + 'data/next.json',
			'previous': oUrl + 'data/previous.json',
			'unavailable': oUrl + 'data/unavailable.json'
		},
		'server': {
			'servertype':'server',
			'data': oUrl+'services/',
			'productdetails': oUrl+'services/productcodification/',
			'productpanel': oHTMLUrl+'panelProductDetail.html',
			'productlist': oUrl+'services/productcodification',
			'productimage': oImageUrl,
			'shop': oUrl+'services/shop/',
			'listing': oUrl+'services/listing/',
			'shops': oUrl+'services/shops/',
			'shopproducts': oUrl+'services/shop/',
			'shopminimums': oUrl+'services/shop/',
			'contactuspanel': oHTMLUrl+'panelContactUs.html',
			'legalpanel': oHTMLUrl+'panelLegal.html',
			'helppanel': oHTMLUrl+'panelHelp.html',
			'privacypanel': oHTMLUrl+'panelPrivacy.html',
			'adcopylocation': oAdUrl,
			'facilitytypes': oUrl+'services/facilitytype?',
			'facilitieslist': oUrl+'services/facilitytype/',
			'citiesByState': oUrl + 'services/state/',
			'cityStateByZip': oUrl+'services/zip/',
			'search': oUrl + 'services/search',
			'next': oUrl + 'services/next',
			'send': oUrl + 'services/send/',
			'submit': '',
			'close': oUrl + 'services/close/',
			'previous': oUrl + 'services/previous',
			'unavailable': oUrl + 'services/unavailable',
			'likeDislike': oUrl + 'services/likedislike/',
			'skipPaidAds': oUrl + 'services/skip'
		}
	}
	return ioTarget[ioBase];

	}