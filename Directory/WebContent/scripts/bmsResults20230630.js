var nsBloomNet = {};
var basicMap;
$(document).ready(function() {
	  $.fn.raty.defaults.path = 'img';
	$('.score-overall').raty({ readOnly: true,
		 hints: ['Do not Send', 'Below Average', 'Average', 'Above Average', 'Preferred Florist'],
		 noRatedMsg: "Not rated yet",
		  score: function() {
		    return $(this).attr('data-score');
		  }
		});
	 
	 $('#star-reviews').raty({hints: ['Do not Send', 'Below Average', 'Average', 'Above Average', 'Preferred Florist']});



	$(inlineLabel());

	$( "#write-a-review" ).dialog({
		autoOpen: false,
		draggable: false,
		resizable: false,
		width: 680,
		height: 410,
		modal: true
	});
	
	$( ".write-a-review" )
	  .button()
	  .click(function() {
	  $( "#reviews-and-recommendations" ).dialog( "close" );
	  $( "#write-a-review" ).dialog( "open" );
	});

	$( "#more-information" ).dialog({
		autoOpen: false,
		draggable: false,
		resizable: false,
		width: 1100,
		height: 500,
		modal: true
	});
	
	$( ".more-information" )
	  .button()
	  .click(function() {
	  $( "#more-information" ).dialog( "open" );
	});

	$( "#minimums" ).dialog({
		autoOpen: false,
		draggable: false,
		resizable: false,
		width: 400,
		height: 240,
		modal: true
	});

	$( "#product-codification" ).dialog({
		autoOpen: false,
		draggable: false,
		resizable: false,
		width: 440,
		height: 480,
		modal: true
	});
	
	$( "#facility-data" ).dialog({
		autoOpen: false,
		draggable: false,
		resizable: false,
		width: 1000,
		height: 480,
		modal: true
	});
	

	$( "#reviews-and-recommendations" ).dialog({
		autoOpen: false,
		draggable: false,
		resizable: false,
		width: 680,
		height: 480,
		modal: true
	});
	
	$( ".reviews-and-recommendations" )
	  .button()
	  .click(function() {
		  var shopCode=$(this).attr('shopCode');
		  var shopName=$(this).attr('shopName');
		  displayReviews(shopCode,shopName);
	 // $( "#reviews-and-recommendations" ).dialog( "open" );
	});
	
	$( "#showMap" )
	  .click(function() {
		  loadScript();
	});

	$( "#store-hours" ).dialog({
		autoOpen: false,
		draggable: false,
		resizable: false,
		width: 400,
		modal: true
	});	
	
	$( "#shop-zips" ).dialog({
		autoOpen: false,
		draggable: false,
		resizable: false,
		width: 400,
		modal: true
	});	

	$( "#display-ad" ).dialog({
		autoOpen: false,
		draggable: false,
		resizable: false,
		width: 820,
		modal: true
	});	

	$("#images").dialog({
		autoOpen: false,
		draggable: false,
		resizable: false,
		width: 670,
		height: 540,
		modal: true
	});

	$("#videos").dialog({
		autoOpen: false,
		draggable: false,
		resizable: false,
		width: 670,
		height: 540,
		modal: true
	});
	
	$("#googleMap").dialog({
		autoOpen: false,
		draggable: false,
		resizable: false,
		width: 670,
		height: 540,
		modal: true
	});
	
	$("#signout")
	.click(function() {
		window.location("signout.htm?timestamp="+ new Date().getTime());
	});
	
	//Modal Close Buttons
    $(".dialogclose").click(function () {
		$(".dialog").dialog( "close" );
    });
    
    $("#dialogclosereview").click(function () {
    	$("#star-reviews").raty();
    	$("#titleTxt").val("");
		$("#reviewTxt").empty();
		$("#reviewCharCount").html("1000");
		$(".dialog").dialog( "close" );
    });
    
    $("#pageNext")
    .click(function (){
    	getNextSearchPage();
    });
    
    $("#pagePrev")
    .click(function (){
    	getPreviousSearchPage();
    });

	//Close Modal on Overlay Click
    $(".ui-widget-overlay").click(function () {
		$(".dialog").dialog( "close" );
    });

    $('#myvideocarousel').jcarousel({
    	wrap: 'circular'
    });

    $("#myvideocarousel a").click(function(){
    	var title = $(this).attr("title");
    	$(".video").removeClass("playing");
		$(".video").hide();
    	$("#"+title).addClass("playing").show();
    });
    
    $("#modifySearch")
    .click(function() {
    	window.location.assign("contemporary.htm");
    });
    
    $( "#accordion" ).accordion({
        collapsible: true,
        active: true,
        heightStyle: "content"
       });
    
    $("#accordion").accordion( "option", "active", 0);
    
    $(window).on('beforeunload', function() {
    	$.ajax({
        	
            type: 'GET',
            url: "signout.htm",
            
            success: function() { 
            	//Do Nothing
            },
            
            error: function(msg) {
            	//Do Nothing
            }
            
         });
    });
    
	var jcarousel = $('.jcarousel');
	if(jcarousel !== undefined){
		 $('.jcarousel').jcarousel({
			 auto:7,
	     	wrap: 'circular'
	     });
	}
	     
});



function displayCodification(oArg){
		var textWrapper = $("#product-codification-content");
		textWrapper.empty();
	var products = oArg.split(',');
	products.sort();
	for(var i=0; i<products.length; i++){
		textWrapper.append("<p><div>"+products[i]+"</div></p> ");
	}
	$("#product-codification").dialog("open");
	
}

function displayFacData(oArg){
	var textWrapper = $("#facility-data-content");
	textWrapper.empty();
	var facs = oArg.split('|');
	facs.sort();
	for(var i=0; i<facs.length; i++){
		var vars = facs[i].split('----');
		var fac = vars[0];
		textWrapper.append("<p><div>"+fac+"</div></p> ");
	}
	$("#facility-data").dialog("open");

}

function displayMinimums(oArg){
	var textWrapper = $("#minimums-content");
	textWrapper.empty();
	var mins = oArg.split(',');
	mins.sort();
	for(var i=0; i<mins.length; i++){
		textWrapper.append("<p><div>"+mins[i]+"</div></p> ");
	}
	$("#minimums").dialog("open");

}

function displayAd(oArg){
	var textWrapper = $("#display-ad-content");
	textWrapper.empty();
	if(oArg.includes("B.")){
		textWrapper.append("<img width=\"205\" height=\"30%\" src=\""+oArg+"?timestamp="+new Date().getTime()+"\"></img>");
	}else{
		textWrapper.append("<img src=\""+oArg+"?timestamp="+new Date().getTime()+"\"></img>");
	}
	$("#display-ad").dialog("open");

}

function displayImages(oArg){
	var textWrapper = $("#images-content");
	textWrapper.empty();
	var images = oArg.split(',');
	for(var i=0; i<images.length; i++){
		var picNumber = i + 1;
		if(i == 0)
			textWrapper.append("<img src='"+images[i]+"' id='image"+picNumber+"' class='image playing'/>");
		else
			textWrapper.append("<img src='"+images[i]+"' id='image"+picNumber+"' class='image'/>");
	}
	var textWrapper2 = $("#myimagecarousel");
	textWrapper2.empty();
	for(var i=0; i<images.length; i++){
		var picNumber = i + 1;
		textWrapper2.append("<li><a title='image"+picNumber+"' onclick='carouselDisplay()'><img src='"+images[i]+"' class='thumb'/></a></li>");
	}
	
	$('#myimagecarousel').jcarousel({
    	wrap: 'circular'
    });

    $("#myimagecarousel a").click(function(){
    	var title = $(this).attr("title");
    	$(".image").removeClass("playing");
		$(".image").hide();
    	$("#"+title).addClass("playing").show();
    });  
	
	$("#images").dialog("open");

}

function displayCustomListing(oArg){
	var textWrapper = $("#more-information-content");
	textWrapper.empty();
	textWrapper.append(oArg);
	$("#more-information").dialog("open");

}
function sendFromListing(id) {
	var url = $("#callback").val();
	$.ajax({
    	
        type: 'GET',
        url: "signout.htm",
        
        success: function() { 
        	window.location.href = url+'#'+id;
        },
        
        error: function(msg) {
        	//Do Nothing
        }
        
     });
}

function displayReviews(shopCode,shopName){
	
	//alert("dispaly review for " + shopCode);
	$('#reviewedShop').val(shopCode);
	$('#reviewedName').val(shopName);

	
	
	$.ajax({
    	
        type: 'GET',
        url: 'services/ratings?shopCode='+shopCode+'&timestamp='+ new Date().getTime(),
        dataType: 'json',
        
        success: function(data) {
        	var textWrapper = $("#reviews-content");
        	textWrapper.empty();
        	var newInput = "<div class='clickable' onclick=\"writeReview('"+shopName.replace("'","&rsquo;")+"')\"><img src='images/write-a-review.png'></img></div>" +
        			"<div id='review-pane'>";
        	for(var i=0; i<data.length; i++){
        			var d = new Date(data[i].dateReviewed);
        			var fmtd = d.getMonth()+1+ "/"+ d.getDate()+ "/" + d.getFullYear();
        			var ratingId = parseInt(data[i].rating_id);
        		newInput += "<div class='review-entry'>" +
				"<p class='stars'><div class='displaystars' display-score="+data[i].stars+"></div><span class='green'>"+data[i].reviewer_shopname+"</span><span>&nbsp;&nbsp;"+fmtd+"</span></p>" +
        		"<div class='clear'></div>"+
				"<br/>"+
				"<h4>"+data[i].title+"</h4>"+
				"<p>"+data[i].comments+"</p>"+
				"<br/>"+
				"<p class='links'><div id='abuse"+ratingId+"'><a class='clickable purple' onclick=reportAbuse("+ratingId+")>Flag Abuse</a></div></p>"+
				//"<p class='links'><span>Was This Helpful?</span><a >Yes</a><a >No</a>|<a >Flag Abuse</a><a  class='right'>Add A Businesss Response</a></p>"+
				"<br/>"+
				"</div>";			
        	}
        	newInput += "</div>";
        	textWrapper.append(newInput);
        	textWrapper.find('.displaystars').raty({ readOnly: true,
        		 hints: ['Do not Send', 'Below Average', 'Average', 'Above Average', 'Preferred Florist'],
        		  score: function() {
        		    return $(this).attr('display-score');
        		  }
        		});


        	

        },
        
        error: function(msg) {
        	//Do Nothing
        }
        
     }).done(function(){
   	  $( "#reviews-and-recommendations" ).dialog( "open" );

     }); 

}

function reportAbuse(reviewId){
	
	$.ajax({
    	
        type: 'GET',
        url: 'services/abuse?reviewId='+reviewId+'&timestamp='+ new Date().getTime(),
        dataType: 'json',
        
        success: function(data) {
        	var textWrapper = $("#abuse"+reviewId);
        	textWrapper.html("This review has been reported. Thank you.");
        	
        },
        
        error: function(msg) {
        	//Do Nothing
        }
	});
}

function writeReview(shopName){
	
	 $( "#reviews-and-recommendations" ).dialog( "close" );
	 $("#write-a-review").dialog({title: "Write A Review For "+shopName});
	 $( "#write-a-review" ).dialog( "open" );

}

function saveReview(){

	var reviewTxt = $('#reviewTxt').val();
	var titleTxt = $('#titleTxt').val();
	var score = $('#star-reviews').raty('score');
	var reviewedShop = $('#reviewedShop').val();
	var reviewedName = $('#reviewedName').val();
	
	if(score != undefined && titleTxt !== "" && reviewTxt !== "" && titleTxt !== "Title" ){
		
		$("#reviewError").html("");
		
		 $.ajax({
		    	
		        type: 'GET',
		        url: 'services/saveReview?comment='+reviewTxt+'&title='+titleTxt+'&score='+score+'&reviewedShop='+reviewedShop+'&reviewedName='+reviewedName+'&timestamp='+ new Date().getTime(),
		        dataType: 'json',
		        
		        success: function(data) {
		       	 $( "#write-a-review" ).dialog( "close" );
		        },
		        
		        error: function(msg) {
		        	//Do Nothing
		        }
		        
		     }); 
	}else{
		$("#reviewError").html("Please Select A Number of Stars and Write a Title and Review");
	}
	 
}

function getOperationalHours(shopcode){
	
	 $.ajax({
	    	
	        type: 'GET',
	        url: 'services/shopHours?shopcode='+shopcode+'&timestamp='+ new Date().getTime(),
	        dataType: 'json',
	        
	        success: function(data) {
	        	var textWrapper = $("#store-hours-content");
	        	textWrapper.empty();
	        	for(var i=0; i<data.length; i++){
	        		textWrapper.append("<p>"+data[i]+"</p> ");
	        	}
	        	$("#store-hours").dialog("open");
	        	
	        },
	        
	        error: function(msg) {
	        	//Do Nothing
	        }
	        
	     }); 
	
}

function getZipsCovered(shopcode){
	
	 $.ajax({
	    	
	        type: 'GET',
	        url: 'services/shopZips?shopcode='+shopcode+'&timestamp='+ new Date().getTime(),
	        dataType: 'json',
	        
	        success: function(data) {
	        	var textWrapper = $("#shop-zips-content");
	        	textWrapper.empty();
	        	for(var i=0; i<data.length; i++){
	        		textWrapper.append(data[i]+"<br /> ");
	        	}
	        	$("#shop-zips").dialog("open");
	        	
	        },
	        
	        error: function(msg) {
	        	//Do Nothing
	        }
	        
	     }); 
	
}

function showMoreInfo(shopCode,shopName,address,customListing,minimums,marker,localProducts,id){
	var d = new Date();
	var n = d.getDay();
	
	 $.ajax({
	    	
	        type: 'GET',
	        url: 'services/shopHours?shopcode='+shopCode+'&timestamp='+ new Date().getTime(),
	        dataType: 'json',
	        
	        success: function(data) {
	        	var textWrapper = $("#moreInfoHours");
	        	textWrapper.empty();
	        	textWrapper.append("<p><h3><font color='purple'>"+shopName+"</font></h3></p>");
        		textWrapper.append("<p>Today's Hours: <br/>");
        		textWrapper.append(data[n].split(": ")[1]+"</p> ");
        		textWrapper.append("<br/> <a class=\"calltoaction clickable\" onclick=\"sendFromListing('"+id+"')\"> <img src=\"images/selectflorist.png\" /></a>");
        		
	        },
	        
	        error: function(msg) {
	        	//Do Nothing
	        }
	        
	     });
	 
	 	$.ajax({
	    	
	        type: 'GET',
	        url: 'services/shopOfferings?shopcode='+shopCode+'&timestamp='+ new Date().getTime(),
	        dataType: 'json',
	        
	        success: function(data) {
	        	var textWrapper = $("#offerings-content");
	        	textWrapper.empty();
	        	textWrapper.append("<p><h3><font color='green'>Florist Offerings:</font></h3></p>");
	        	for(var i=0; i<data.length; i++){
	        		textWrapper.append(data[i]+"<br/> ");
	        	}
     		
	        },
	        
	        error: function(msg) {
	        	//Do Nothing
	        }
	        
	     });
	    localAddress = address;
	    geocodeMarker = marker;
	 	var script = document.createElement('script');
	 	script.type = 'text/javascript';
	 	script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&client=gme-bloomnetinc&channel=Directory&callback=initializeMoreInfo';
	 	document.body.appendChild(script);
	 
	    var textWrapper = $("#more-information-content");
		textWrapper.empty();
		textWrapper.append("<p><h3><font color='green'>Custom Listing:<br/></font></h3></p>");
		textWrapper.append(customListing);
		$("#more-information").dialog("open");
		
		textWrapper = $("#minimums-more-content");
		textWrapper.empty();
		textWrapper.append("<p><h3><font color='green'>Minimums:<br/></font></h3></p>");
		var mins = minimums.split(',');
		for(var i=0; i<mins.length; i++){
			textWrapper.append(mins[i]+"<br/> ");
		}
		
		textWrapper = $("#local-products");
		textWrapper.empty();
		if(localProducts != undefined && localProducts !== "" && localProducts !== "null"){
			var products = localProducts.split(',');
			var x = 0;
			for(var i=0; i<products.length; i++){
				var productItems = products[i].split("--");
				var prodName = productItems[0];
				var prodPrice = productItems[1];
				var prodPic = productItems[2];
				if(x == 5){
					x = 0;
					textWrapper.append("<br/><br/><br/>");
				}
				if(x == 0)
					textWrapper.append("<div class='local-product clickable' onclick='selectProductAndShop(\""+id+"\",\""+prodName+"\",\""+prodPrice+"\")'><img class='local-product-image' src='http://directory.bloomnet.net/bloomnet-images/ads/"+prodPic+"'></img><br/>"+prodName+"<br/>"+prodPrice+"</div>");
				else
					textWrapper.append("<div style='margin-left: "+x*220+"px; margin-top: -150px;' class='local-product clickable' onclick='selectProductAndShop(\""+id+"\",\""+prodName+"\",\""+prodPrice+"\")'><img class='local-product-image' src='http://directory.bloomnet.net/bloomnet-images/ads/"+prodPic+"'></img><br/>"+prodName+"<br/>"+prodPrice+"</div>");
				x++;

			}
		}
	 
	
}

function getNextSearchPage(){
	
	 $.ajax({
	    	
	        type: 'GET',
	        url: 'services/nextContemporary?timestamp='+ new Date().getTime(),
	        dataType: 'text',
	        
	        success: function(data) {
	        	location.reload();
	        },
	        
	        error: function(msg) {
	        	//Do Nothing
	        }
	        
	     }); 
	
}

function getPreviousSearchPage(){
	
	 $.ajax({
	    	
	        type: 'GET',
	        url: 'services/previousContemporary?timestamp='+ new Date().getTime(),
	        dataType: 'text',
	        
	        success: function(data) {
	        	location.reload();
	        },
	        
	        error: function(msg) {
	        	//Do Nothing
	        }
	        
	     }); 
	
}

function deleteLine(){
	//var oLine = target.ancestor('div.orderLine');
	//oLine.remove();
	$('#productTable').children("a:last-child").remove();
}

function inlineLabel(){
	$('input[title]').each(function() {
		if($(this).val() === '') {
			$(this).val($(this).attr('title')); 
			}
		$(this).focus(function() {
			if($(this).val() === $(this).attr('title')) {
				$(this).val('').addClass('focused'); 
				}
			});
		$(this).blur(function() {
			if($(this).val() === '') {
				$(this).val($(this).attr('title')).removeClass('focused'); 
				}
			});
		});
}

function initialize() {
	
	var zipCode = $("#searchZipCode").val();
	var geoCoder = new google.maps.Geocoder();
	geoCoder.geocode( { 'address': zipCode}, function(results, status) {
	if (status == google.maps.GeocoderStatus.OK) {
		var lat = results[0]['geometry']['location'].lat();
		var lng = results[0]['geometry']['location'].lng();
		var centerLatLng = new google.maps.LatLng(lat,lng);
		var mapOptions = {
			zoom: 12,
			center: centerLatLng,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		  };
		  basicMap = new google.maps.Map(document.getElementById('map-canvas'),
			  mapOptions);
	}	
 
					});
	  var addresses = $("#mapPoints").val();
	  addMarkers(addresses);
	  $( "#googleMap" ).dialog( "open" );  
}

function initializeMoreInfo(){
	var geoCoder = new google.maps.Geocoder();
	geoCoder.geocode( { 'address': localAddress}, function(results, status) {
	if (status == google.maps.GeocoderStatus.OK) {
		var lat = results[0]['geometry']['location'].lat();
		var lng = results[0]['geometry']['location'].lng();
		var centerLatLng = new google.maps.LatLng(lat,lng);
		var mapOptions = {
			zoom: 14,
			center: centerLatLng,
		    disableDefaultUI: true,
		    zoomControl: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		  };
		  basicMap = new google.maps.Map(document.getElementById('map-canvas-more-info'),
			  mapOptions);
	}	
 
					});
	var addressLine = geocodeMarker;
	addMarkers(addressLine);
	google.maps.event.trigger(basicMap, 'resize');
}

function addMarkers(addresses) {
  var geoCoder = new google.maps.Geocoder();
  var addressLines = addresses.split("///");
  for(var ii=0; ii<addressLines.length; ii++){
  	var addressLine = addressLines[ii];
	addMarker(addressLine);
}

function addMarker(addressLine){
	var addressItems = addressLine.split("--");
	var shopInfo = addressItems[0];
	var address = addressItems[1];
	var shopItems = shopInfo.split(",");
	var shopCode = shopItems[0];
	var shopName = shopItems[1];
	var shopPhone = shopItems[2];
	var splitItems = address.split(",");
	var addressLine1 = "";
	var addressLine2 = "";
	var city = "";
	var state = "";
	var zip = "";
	if(splitItems.length == 5){
		addressLine1 = splitItems[0];
		addressLine2 = splitItems[1];
		city = splitItems[2];
		state = splitItems[3];
		zip = splitItems[4];
	}else{
		addressLine1 = splitItems[0];
		city = splitItems[1];
		state = splitItems[2];
		zip = splitItems[3];
	}
	var latLng;
	  var contentString = '<div style="width:175px; overflow:hidden;">'+
	  '<div id="bodyContent">'+
	  '<div style="font-weight:bold; color:#719d48;">'+shopName+'</div>'+
	  shopCode+'<br />'+
	  shopPhone+'<br />'+
	  addressLine1+'<br />'+
	  city+', '+state+', '+zip+
	  '</div>';
	  
	  var infowindow = new google.maps.InfoWindow({
		  content: contentString
		  });
	  
	geoCoder.geocode( { 'address': address}, function(results, status) {
	if (status == google.maps.GeocoderStatus.OK) {
		var lat = results[0]['geometry']['location'].lat();
		var lng = results[0]['geometry']['location'].lng();
		latLng = new google.maps.LatLng(lat,lng);
		var deliveryMarker = new google.maps.Marker({
			map: basicMap,
			position: latLng,
			title: (shopName)
		});
		google.maps.event.addListener(deliveryMarker, 'click', function() {
			infowindow.open(basicMap,deliveryMarker);
			});
	}	
 
					});
				
	}

}

function popitup(url,name) {
	newwindow=window.open(url,name);
	if (window.focus) {newwindow.focus()}
	return false;
}

function loadScript() {
	  var script = document.createElement('script');
	  script.type = 'text/javascript';
	  script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&client=gme-bloomnetinc&channel=Directory&callback=initialize';
	  document.body.appendChild(script);
	}

function confirmSelection(shopCode,id){
	if(id !== "0"){
		if(confirm('Please confirm your selection of shop code ' + shopCode + ' as the fulfilling shop for this order.')){
			sendFromListing(id);
		}
	}
}
