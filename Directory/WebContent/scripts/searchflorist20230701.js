function examineZip() {
		var $searchZip=$("#searchZip").val();
		var finalCity;
		if ($searchZip.length >= 5){
			var $url =  nsBloomNet.ioServer['cityStateByZip'];
			$url += $searchZip + '/city?timetamp='+ new Date().getTime();
			
			$.getJSON( $url, function( data ) {
				$.each( data.results, function( i, val ) {
					var city = val.id;
					var state = val['state_id'];
 		        	if (state === '') {
 		        	}else{
 		        		//$("#searchCityAc option:contains(" + city + ")").attr('selected', 'selected');
 		        		//$("#searchSelState option:contains(" + state + ")").attr('selected', 'selected');
 		        		//$("#stateorprovince option:contains(" + state + ")").attr('selected', 'selected');
 		        		$("#searchSelState option[value='"+state+"']").attr('selected','selected');
 		        		$("#stateorprovince option[value='"+state+"']").attr('selected','selected');
 		        		finalCity = city;
 		        	}

				});
			}).done(function(){
				var selState= $("#searchSelState").val();
	    		var select = $("#searchCityAc");
	    		var select2= $("#city");
	    		var url =  nsBloomNet.ioServer['citiesByState'];
	    		var stateCode = selState;
	    		if (!stateCode) {
	    			return;
	    		}
	    		if (nsBloomNet.ioServer['servertype'] === 'server') {
	    			url += stateCode + '/city?timestamp='+ new Date().getTime();
	    		}else{
	    			url += '?timestamp='+ new Date().getTime();
	    		}

	    		$.ajax({

	    		        type: 'GET',
	    		        url: url,
	    		        dataType: 'json',

	    		        success: function(data) {
	    		        	buildCitiesACCont(data,select,select2);

	    		        },

	    		        error: function(msg) {
	    		        	// do nothing
	    		        }

	    		     }).done(function() {
	    		    	 $("#searchCityAc option[value='"+finalCity+"']").attr('selected','selected');
	 		    		$("#city option[value='"+finalCity+"']").attr('selected','selected');
	    		     });
		     });
		}
	}
	
function examineOrderZip() {
	var $searchZip=$("#orderZip").val();
	var finalCity;
	if ($searchZip.length >= 5 && $searchZip !== "Zip / Postal Code"){
		var $url =  nsBloomNet.ioServer['cityStateByZip'];
		$url += $searchZip + '/city?timetamp='+ new Date().getTime();
		
		$.getJSON( $url, function( data ) {
			$.each( data.results, function( i, val ) {
				var city = val.id;
				var state = val['state_id'];
				var cityname = val.name;
				var statename = val['state_name'];
		        	if (state === '') {
		        	}else{
		        		$("#orderSelCity").val(cityname);
		        		$("#orderSelState").val(statename);
		        		$("#searchZip").val($searchZip);
		        		$("#searchSelState option[value='"+state+"']").attr('selected','selected');
		        		$("#addressSelState option[value='"+state+"']").attr('selected','selected');
		        		finalCity = city;
		        	}

			});
		}).done(function(){
			var selState= $("#addressSelState").val();
    		var select = $("#addressIptCity");
    		var select2= $("#searchCityAc");
    		var url =  nsBloomNet.ioServer['citiesByState'];
    		var stateCode = selState;
    		if (!stateCode) {
    			return;
    		}
    		if (nsBloomNet.ioServer['servertype'] === 'server') {
    			url += stateCode + '/city?timestamp='+ new Date().getTime();
    		}else{
    			url += '?timestamp='+ new Date().getTime();
    		}

    		$.ajax({

    		        type: 'GET',
    		        url: url,
    		        dataType: 'json',

    		        success: function(data) {
    		        	buildCitiesACCont(data,select,select2);

    		        },

    		        error: function(msg) {
    		        	// do nothing
    		        }

    		     }).done(function() {
    		    	 $("#addressIptCity option[value='"+finalCity+"']").attr('selected','selected');
    		     });
	     });
	}
}

function examineFacZip() {
	var $searchZip=$("#zipcode").val();
	var finalCity;
	var finalState;
	if ($searchZip.length >= 5){
		var $url =  nsBloomNet.ioServer['cityStateByZip'];
		$url += $searchZip + '/city?timetamp='+ new Date().getTime();
		
		$.getJSON( $url, function( data ) {
			$.each( data.results, function( i, val ) {
				var city = val.id;
				var state = val['state_id'];
		        	if (state === '') {
		        	}else{
		        		//$("#searchCityAc option:contains(" + city + ")").attr('selected', 'selected');
		        		//$("#searchSelState option:contains(" + state + ")").attr('selected', 'selected');
		        		//$("#stateorprovince option:contains(" + state + ")").attr('selected', 'selected');
		        		$("#searchSelStateFac option[value='"+state+"']").attr('selected','selected');
		        		finalCity = city;
		        		finalState = state;
		        	}

			});
		}).done(function(){
			var selState= $("#searchSelStateFac").val();
    		var select = $("#searchCity");
    		var select2= $("#city");
    		var url =  nsBloomNet.ioServer['citiesByState'];
    		var stateCode = selState;
    		if (!stateCode) {
    			return;
    		}
    		if (nsBloomNet.ioServer['servertype'] === 'server') {
    			url += stateCode + '/city?timestamp='+ new Date().getTime();
    		}else{
    			url += '?timestamp='+ new Date().getTime();
    		}

    		$.ajax({

    		        type: 'GET',
    		        url: url,
    		        dataType: 'json',

    		        success: function(data) {
    		        	buildCitiesACCont(data,select,select2);

    		        },

    		        error: function(msg) {
    		        	// do nothing
    		        }

    		     }).done(function() {
    		    	 $("#searchCity option[value='"+finalCity+"']").attr('selected','selected');
 		    		$("#city option[value='"+finalCity+"']").attr('selected','selected');
				populateFacilitiesDD();
 		    		
    		     });
	     });
	}
}

function populateFacility(oCityId,oStateId){
	var url = nsBloomNet.ioServer['facilitytypes'];
	

	
	if (nsBloomNet.ioServer['servertype'] === 'server') {
		url += 'stateid=' + oStateId + '&city='+ oCityId + '&cacheBuster='+ new Date();
	}
	$.ajax({

        type: 'GET',
        url: url,
        dataType: 'json',

        success: function(data) {
        	populateFaciltyTypeDDCont(data);

        },

        error: function(msg) {
        	// do nothing
        }

     });
}

function populateFaciltyTypeDDCont(data) {
	var oRecords = data.results;
	var oCount = oRecords.length;
	//if (oCount === 0) {
		//return;
	//}
	
	var oOptions = [];
	var oRecord;
	var select = $("#facilityDD");
	select.empty();
	select.append("<option value=''>Facility Type</option>");
	oOptions.push('<input type="text" class="focus" style="height: 0; border-width: 0; margin-top: -10px;" />');
	for (var oIdx=0; oIdx<oCount; oIdx++) {
		//oHtml = '';
		oRecord = oRecords[oIdx];
		/*oHtml = ('<div class="ddWidget_item mouseable clickable">' + oRecord['name'] + '</div>');
		oOptions.push(oHtml);
		oValues.push(oRecord['id']);*/
		select.append($('<option></option>').val(oRecord['id']).html(oRecord['name']));

	}
	
}

function populateFacilitiesDD() {

		var oFacilityType = $("#facilityDD option:selected").val();

		var cityId = $("#searchCity option:selected").val();
		var stateId = $("#searchSelStateFac option:selected").val();
		
		var url =  nsBloomNet.ioServer['facilitieslist'];
		
		if (nsBloomNet.ioServer['servertype'] === 'server') {
			url += oFacilityType + '/facility?stateid=' + stateId + '&city='+ cityId;
		}
		
		
		$.ajax({

	        type: 'GET',
	        url: url,
	        dataType: 'json',

	        success: function(data) {
	        	populateFacilitiesDDCont(data);

	        },

	        error: function(msg) {
	        	// do nothing
	        }

	     });
}

function populateFacilitiesDDCont(data) {
	var oRecords = data.results;
	var oCount = oRecords.length;
	//if (oCount === 0) {
		//return;
	//}
	var oOptions = [];
	var oHtml;
	var oRecord;
	var select = $("#facilitiesDD");
	select.find('option').not(':first').remove();
	oOptions.push('<input type="text" class="focus" style="height: 0; border-width: 0; margin-top: -10px;" />');
	for (var oIdx=0; oIdx<oCount; oIdx++) {
		
		oRecord = oRecords[oIdx];
		oHtml = oRecord['name'] + ' -- ';
		oHtml += oRecord['address1'] + ' -- ';
		oHtml += oRecord['city'] + ', ' + oRecord['state'] + ', ' + oRecord['zip'] + ' -- ';
		if(oRecord['phone'] !== "null")oHtml += oRecord['phone'];
		select.append($('<option></option>').val(oRecord['id']).html(oHtml));

	}
	
	if(oCount == 0){
		$("#errorMessageSearchFac").html("No facilities for this search type were found for that city/state/zip. Please try the Residential Search instead.");
	}else{
		$("#errorMessageSearchFac").html("");

	}
	
}


	function buildCitiesAC(selState,select,select2) {
		$("#searchforaflorist").attr("disabled", "disabled");
		var url =  nsBloomNet.ioServer['citiesByState'];
		var stateCode = selState;
		if (!stateCode) {
			return;
		}
		if (nsBloomNet.ioServer['servertype'] === 'server') {
			url += stateCode + '/city?timestamp='+ new Date().getTime();
		}else{
			url += '?timestamp='+ new Date().getTime();
		}

		$.ajax({

		        type: 'GET',
		        url: url,
		        dataType: 'json',

		        success: function(data) {
		        	buildCitiesACCont(data,select,select2);

		        },

		        error: function(msg) {
		        	// do nothing
		        }

		     }).done(function() {
		 		if($("#searchCity option:selected").text() !== "City"){
					populateFacilitiesDD();	
		 		}
		     });
	}

	function buildCitiesACCont(data,select,select2){
		var oRecords = data.results;
		var oCount = oRecords.length;
		if (oCount === 0) {
			return;
		}
	
		var oRecord;
		var oPreviousCity;
		var orderCity = $("#orderSelCity").val();
		var oHtml = '';
		var selCountry= $("#searchSelCountry").val();
		var addressCountry= $("#addressSelCountry").val();
		var match = 0;
	    if((selCountry != undefined && selCountry !== "1" && selCountry !== "2" && selCountry !== "3") || (selCountry == undefined && addressCountry != undefined && addressCountry !== "1" && addressCountry !== "2" && addressCountry !== "3")){
	    	for (var oIdx=0; oIdx<1; oIdx++) {
				oRecord = oRecords[oIdx];
				if(oRecord['name'] !== oPreviousCity){
						oHtml += '<option selected="selected" value="'+oRecord['id']+'">'+oRecord['name']+'</option>';
						match = 1;
				}
				oPreviousCity = oRecord['name'];
			}
	    }else{
	    	for (var oIdx=0; oIdx<oCount; oIdx++) {
				oRecord = oRecords[oIdx];
				if(oRecord['name'] !== oPreviousCity){
					if(orderCity === oRecord['name']){
						oHtml += '<option selected="selected" value="'+oRecord['id']+'">'+oRecord['name']+'</option>';
						match = 1;
					}else{
						oHtml += '<option value="'+oRecord['id']+'">'+oRecord['name']+'</option>';
					}
				}
				oPreviousCity = oRecord['name'];
			}
	    }
		if(match == 1){
			oHtml = '<option value="">City</option>' + oHtml;

		}else{
			oHtml = oHtml = '<option selected="selected" value="">City</option>' + oHtml;

		}
		$("#searchforaflorist").removeAttr("disabled");
		select.empty();
		select.append(oHtml);
		select2.empty();
		select2.append(oHtml);
	}
	
	function selectState(){
		
		var selState= $("#searchSelState").val();
		$("#searchCityAc").empty();
 		var select = $("#searchCityAc");
 		var select2= $("#city");
 		buildCitiesAC(selState,select,select2);
		
	}

	function changeCountry(){
	  var selCountry= $("#searchSelCountry").val();
	  $("#searchSelState").empty();
	  $("#orderSelCity").empty();
	  $("#orderSelCity").append('<option selected="selected" value="">City</option>');
	  if(selCountry !== "1" && selCountry !== "2" && selCountry !== "3"){
	      $("#searchZip").val("");
	  }else{
	      $("#searchZip").val("Zip / Postal Code");
	  }
	  selectCountry();
	}
	
	function selectCountry(){
		
		var selCountry= $("#searchSelCountry").val();
 		var select = $("#searchSelState");
 		
 		var url =  nsBloomNet.ioServer['citiesByState'];
		var countryId = selCountry;
		
		if (nsBloomNet.ioServer['servertype'] === 'server') {
			url += countryId + '?timestamp='+ new Date().getTime();
		}
		$.ajax({

		        type: 'GET',
		        url: url,
		        dataType: 'json',

		        success: function(data) {
		        	buildStates(data,select);

		        },

		        error: function(msg) {
		        	// do nothing
		        }

		     });

		
	}
	
	function buildStates(data,select){
		var oRecords = data.results;
		var oCount = oRecords.length;
		$("#searchCityAc").empty();
		$("#searchCityAc").append('<option selected="selected" value="">City</option>');
		var selCountry= $("#searchSelCountry").val();
		if (oCount === 0) {
			return;
		}
		var oRecord;
		var oHtml = '';
		if(selCountry === "1" || selCountry === "2" || selCountry === "3")
			oHtml = '<option selected="selected" value="">State/Province</option>';
		for (var oIdx=0; oIdx<oCount; oIdx++) {
			oRecord = oRecords[oIdx];
			if(selCountry !== "1" && selCountry !== "2" && selCountry !== "3" && oIdx == 0){
				oHtml += '<option value="'+oRecord['id']+'" selected="selected">'+oRecord['name']+'</option>';
			}else{
				oHtml += '<option value="'+oRecord['id']+'">'+oRecord['name']+'</option>';
			}
		}
		
		select.append(oHtml);
		if(selCountry !== "1" && selCountry !== "2" && selCountry !== "3"){
			$("#orderSelState").val($("#addressSelState option:selected").text());
			selectState();
			$("#searchZip").hide();
	 		$("#searchstateorprovinceselect").hide();
	 		$("#searchCitySelect").hide();
	 		$("#searchZip").val("");
	 		internationalDate();
		}else{
			$("#searchZip").show();
	 		$("#searchstateorprovinceselect").show();
	 		$("#searchCitySelect").show();
		}
	}
	
function selectFacState(){
		
		var selState= $("#searchSelStateFac").val();
		$("#searchCity").empty();
 		var select = $("#searchCity");
 		var select2= $("#city");
 		buildCitiesAC(selState,select,select2);
		
	}
	
	function searchflorist() {
		
		var TextHighlightCss = {
		        'background': '#FFFFAA',
		        'border': '1px solid #FFAD33'
		    };

		
		var $searchZip=$("#searchZip").val();
		var $searchCityAc=$("#searchCityAc option:selected").text();
		var $searchSelState=$("#searchSelState option:selected").text();
		var $productId=$("#productId option:selected").val();
		var $searchDeliveryDate=$("#searchDeliveryDate").val();
		var $searchAdditionalCities=$("#searchAdditionalCities").val();
		 if($searchCityAc === "City"){
			 $("#errorMessageSearch").html("Please enter a valid zip code or select a City/State");
			 $("#searchCityAc option:selected").addClass("error");
			 $("#searchSelState").addClass("error");

		 }else{
			 $("#errorMessageSearch").html("");
			 var $url = 'services/search?searchDeliveryDate='+$searchDeliveryDate+'&searchZip='+$searchZip+'&searchSelState='+$searchSelState+'&searchCityAc='+$searchCityAc+'&searchAdditionalCities='+$searchAdditionalCities+'&productId='+$productId+'&timestamp='+ new Date().getTime();
			 $.ajax({
			    	
			        type: 'POST',
			        url: $url,
			        dataType: 'text',
			        
			        success: function(data) {
		            	 window.location.href = "?view=searchResults&from=search&timestamp="+ new Date().getTime();
			        	
			        },
			        
			        error: function(msg) {
			        	alert(JSON.stringify(msg, null, 4));
			        }
			        
			     }); 
		 }
		 
	}
	
	function searchfacility() {
		
		var $searchZip=$("#zipcode").val();
		var $searchCityAc=$("#searchCity option:selected").text();
		var $searchSelState=$("#searchSelStateFac option:selected").text();
		var $searchDeliveryDate=$("#deliveryDate2").val();
		var $searchAdditionalCities=$("#searchAdditionalCities").val();
		var facilityId=$("#selectedFacilityId").val();
		var facilityData = $("#facilitiesDD option:selected").text().replace(/\#+/g, '%23');
		 if(facilityData === "Facilities" || facilityData === "Funeral Homes" || facilityData === "Nursing Homes" || facilityData === "Hospitals"){
			 $("#errorMessageSearchFac").html("Please Select a Facility");
		 }else{
			 
			 $("#errorMessageSearchFac").html("");
			 
			 $.ajax({
			    	
			        type: 'POST',
			        url: 'services/search?searchDeliveryDate='+$searchDeliveryDate+'&searchZip='+$searchZip+'&searchSelState='+$searchSelState+'&searchCityAc='+$searchCityAc+'&searchAdditionalCities='+$searchAdditionalCities+'&timestamp='+ new Date().getTime()+'&facility='+ facilityId+'&facilityData='+ facilityData,
			        dataType: 'text',
			        
			        success: function(data) {
		            	 window.location.href = "?view=searchResults&from=search&timestamp="+ new Date().getTime();
			        	
			        },
			        
			        error: function(msg) {
			        	alert(JSON.stringify(msg, null, 4));
			        }
			        
			     }); 
		 }
		 
	}
	

	
	function openDialog(){

		var pref= $("#pref").val();
		if (pref==0){
     	//alert(pref);
			$( "#choose-search-layout" ).dialog( "open" );
		}

		
	}
	
	
	
	function addOrderLine() {
		//var oLineTemplate = Y$.one('#newOrderLineTemplate').get('innerHTML');
		//var newInput = $("<input name='new_field' type='text'>");
		var numberProducts = parseInt($("#productLineCount").val());
		var newNumber = numberProducts + 1;
		var newInput = $("<div class='orderLine' id='orderLine"+newNumber+"'><input type='text' name='productCode' id='productCode'  title='Product Code'/>" +
				"<input type='text' name='productQuantity' id='productQuantity' class='productQuantity' title='Quantity' onkeypress='return onlyNumbers();' onchange='calcTotal();' />" +
				"<input type='text' name='productDescription' id='productDescription' class='productDescription' title='Product Description' /> " +
				"<input type='text' name='productPrice' id='productPrice' class='productPrice' title='Price' onkeypress='return onlyNumbers();' onchange='calcTotal();' />" +
				"<a class='clickable green' onclick='removeOrderLine(\"orderLine"+newNumber+"\")'>&nbsp;Delete</a>" +
				"<br/>" +
				"</div>");

		$('#productTable').append(newInput);
		
		$("#productLineCount").val(newNumber);


		
		//Y$.one('#productTable').append(oLineTemplate);
		/*var oProdTable = $('#productTable');
		var oProductLines = oProdTable.all('.orderLine');
		var oNewLine = oProductLines.item(oProductLines.size()-1);
		
		
		oNewLine.one('.productDescription').on('change',editProductLine);
		oNewLine.one('.productPrice').on('change',editPrice);
		oNewLine.one('.productQuantity').on('change',productQuantity);
		
		oNewLine.all('input.orderField').on('focus', _toggleInputHint);
		oNewLine.all('input.orderField').on('focus', onFocus);
		oNewLine.all('input.orderField').on('blur', _toggleInputHint);
		oNewLine.all('input.orderField').on('blur', onBlur);
//		Y$.all('input.submitable').setAttribute('disabled', 'disabled').addClass('disabled');
		checkLineCount();
		*/
		
		inlineLabel();

	}
	
	function removeOrderLine(lineID){
		$("#"+lineID).remove();
		calcTotal();
	}
	
	function deleteLine(){
		//var oLine = target.ancestor('div.orderLine');
		//oLine.remove();
		$('#productTable').children("a:last-child").remove();
	}
	
	function inlineLabel(){
		$('input[title]').each(function() {
			if($(this).val() === '') {
				$(this).val($(this).attr('title')); 
				}
			$(this).focus(function() {
				if($(this).val() === $(this).attr('title')) {
					$(this).val('').addClass('focused'); 
					}
				});
			$(this).blur(function() {
				if($(this).val() === '') {
					$(this).val($(this).attr('title')).removeClass('focused'); 
					}
				});
			});
		
		$('textarea[title]').each(function() {
			if($(this).val() === '') {
				$(this).val($(this).attr('title')); 
				}
			$(this).focus(function() {
				if($(this).val() === $(this).attr('title')) {
					$(this).val('').addClass('focused'); 
					}
				});
			$(this).blur(function() {
				if($(this).val() === '') {
					$(this).val($(this).attr('title')).removeClass('focused'); 
					}
				});
			});
	}
	
	function showSearch(){
		$("#step-1a").show();
	}