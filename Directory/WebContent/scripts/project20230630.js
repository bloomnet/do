$(document).ready(function() {
	
	jQuery.validator.addMethod("notEqual", function(value, element, param) {
		  return this.optional(element) || value != param;
		}, "Please specify a different (non-default) value");
	
	jQuery.validator.addMethod("notEqualSelect", function(text, element, param) {
		  return this.optional(element) || text != param;
		}, "Please specify a different (non-default) value");
	
	$("#orderForm").validate({
		
						//errorClass: "my-error-class",
						ignore: "#orderZip, :hidden",
						
						rules : {
							orderNameFirst : {
								required : true,
								notEqual : "First name"
							},
							orderNameLast : {
								required : true,
								notEqual : "Last name"
							},
							orderAddressIptStreet1 : {
								required : true,
								notEqual : "Street Address 1"
							},
							addressIptCity : {
								required : true,
								notEqualSelect : "City"
							},
							addressSelState : {
								required : true,
								notEqualSelect : "State/Province"
							},
							orderAddressIptPhone : {
								required : true,
								notEqual : "Phone number"
							},
							orderSelOcassion : {
								required : true,
								notEqual : "Occasion"
							},
							productQuantity : {
								required : true,
								notEqual : "Quantity"
							},
							productDescription : {
								required : true,
								notEqual : "Product Description"
							},
							productPrice : {
								required : true,
								notEqual : "Price"
							},
							containsPerishables : {
								required : true,
								notEqualSelect : ""
							}

		},
		messages: {
			orderNameFirst: "&nbsp;*",
			orderNameLast: "&nbsp;*",
			orderAddressIptStreet1: "&nbsp;*",
			addressIptCity: { required: "*" },
			addressSelState: { required: "*" },
			orderAddressIptPhone: "&nbsp;*",
			orderSelOcassion: "*",
			productQuantity: "*&nbsp;",
			productDescription: "*",
			productPrice: "&nbsp;*",
			orderZip: "*",
			containsPerishables: { required: "<br />&nbsp;*" }
		},
		highlight: function(element, errorClass) {
	    	$(element).removeClass(errorClass); //prevent class to be added to selects
	  	}
		
	});
	
	$(inlineLabel());
	
	$("#addressSelState")
	.change(function() {
		$("#orderSelState").val($("#addressSelState option:selected").text());
	});
	
	$("#addressIptCity")
	.change(function() {
		$("#orderSelCity").val($("#addressIptCity option:selected").text());
	});
	
	$("#orderAddressIptPhone")
	.keypress(function() {
		return onlyPhoneNumbers();
	})
	.change(function() {
		return formatPhoneNumber();
	});
	
	$("#facilityDD")
	.change(function() {
		populateFacilitiesDD();
		
	});
	
	
	$("#facilitiesDD")
	.change(function() {
       var facilityId = $("#facilitiesDD").val();
       $("#selectedFacilityId").val(facilityId);
	});

	
	if($("#searchSelState").val() !== 'State/Province'){
		var selState= $("#searchSelState").val();
		$("#searchCityAc").empty();
		var select = $("#searchCityAc");
		var select2= $("#searchCity");
		buildCitiesAC(selState,select,select2);
	}
	
	
	if($("#orderSelState").val() !== 'State/Province' && $("#orderSelState").val() !== ''){
		var selState= $("#addressSelState").val();
		$("#addressIptCity").empty();
		var select = $("#addressIptCity");
		var select2= $("");
		buildCitiesAC(selState,select,select2);

		
		if($("#addressIptCity").html() === ""){
			$("#addressIptCity").html("<option selected='selected'>City</option>");
		}
	
	}
	
	$('.tooltip').tooltip();	
	
    
   	$('#addressSelState').change(function() {
   		var selState= $("#addressSelState").val();
		$("#addressIptCity").empty();
		var select = $("#addressIptCity");
		var select2= $("");
		buildCitiesAC(selState,select,select2);
   	});
   	
   	$('#addressSelCountry').change(function() {
	  var selCountry= $("#addressSelCountry").val();
	  $("#addressSelState").empty();
	  $("#addressIptCity").empty();
	  if(selCountry !== "1" && selCountry !== "2" && selCountry !== "3"){
	      $("#orderZip").val("");
	  }else{
	      $("#orderZip").val("Zip / Postal Code");
		  $("#addressIptCity").append('<option selected="selected" value="">City</option>');
	  }
	  selectOrderCountry();
   	});

	$('.deliverydate').datepicker();	
	$('.minicalendar').click(function(){
		$(this).parent().find('.deliverydate').focus();
	});

	$('.inputwrapper').mousedown(function () {
		$(this).find("span").html("");
	});

	$('.styled-select').mousedown(function () {
		$(this).find("span").html("");
	});
	
	$("#searchCity")
	.change(function() {
		var facState = $("#searchSelStateFac option:selected").val();
		var facCity = $("#searchCity option:selected").val();
		populateFacilitiesDD();
		});
	
	$("#addressIptCity2")
	.blur(function() {
		var city = $("#addressIptCity2").val();
		$("#orderSelCity").val(city);
	});

	$( "#choose-search-layout" ).dialog({
		autoOpen: false,
		draggable: false,
		resizable: false,
		width: 500,
		height: 240,
		modal: true,
		buttons: {
			"Save": function() {
				//
				var $selected= $("input:radio[name='pageview']:checked").val();
				var $rememberselection= $("input:checkbox[name='rememberselection']:checked").val();
				
			//	$("#pref").val( $("input:radio[name='pageview']:checked").val());
				if ($selected=="contemporary"){
					$("#pref").val("2");
				}else if ($selected=="traditional"){
					$("#pref").val("1");
				}else{
					$selected = "2";
				}

		
				  $.ajax({
				    	
				        type: 'GET',
				        url: 'layout.xml?selected='+$selected+'&rememberselection='+$rememberselection+'&timestamp='+ new Date().getTime(),
				        dataType: 'xml',
				        
				        success: function(data) {
				        	//alert($selected);
				        	if ($selected=='traditional'){
			            	 window.location.href = $selected +'.htm';
				        	}
				        },
				        
				        error: function(msg) {
				        	// do nothing
				        }
				        
				     });   
				//
				// alert("Please wait...we are already processing your request...");
				$( this ).dialog( "close" );
			}
		}
	});
	
    $(openDialog()); 
	//$( "#choose-search-layout" ).dialog( "open" );

	$( "#advanced-search" ).click(function() {
		$( "#advanced-search-layout" ).dialog({
		  draggable: false,
		  resizable: false,
		  width: 500,
		  height: 240,
		  modal: true
		});
	});
	
	

	$( "#address-verification" ).dialog({
		autoOpen: false,
		draggable: false,
		resizable: false,
		width: 630,
		height: 450,
		modal: true
	});

	$( "#address-verification2" ).dialog({
		autoOpen: false,
		draggable: false,
		resizable: false,
		width: 630,
		height: 320,
		modal: true
	});
	
	$( "#continue" )
	  .button()
	  .click(function() {
		  var $addressLine1 = $("#orderAddressIptStreet1").val();
		  var $addressLine2 = $("#orderAddressIptStreet2").val();
		  var $city = $("#addressIptCity :selected").text();
		  var $state = $("#addressSelState :selected").text();
		  var $zip = $("#orderZip").val();
		  var $containsPerishables = $("#containsPerishables").val();
		  if($addressLine1 === 'Street Address 1' || $city === 'City' || $state === 'State/Province' || $containsPerishables === ''){			  
			  if(parseFloat($("#orderTotal").text()) > 0.00){
      			$("#orderForm").submit();
      			$("#errorMessage").html("Please fill in all required fields marked with a *");
      		}else{
      			$("#errorMessage").html("Please select or enter at least one product");
      		}
		  }else{			  
			  if($addressLine2 === 'undefined' || $addressLine2 === 'Street Address 2')
				  $addressLine2 = "";
			  if($zip === 'undefined' || $zip === 'Zip / Postal Code')
				  $zip = "";
			  
			  var $url = 'services/verifyaddress?addressLine1='+$addressLine1+'&addressLine2='+$addressLine2+'&city='+$city+'&stateShort='+$state+'&zip='+$zip;
			  
			  $.ajax({
			    	
			        type: 'GET',
			        url: $url,
			        dataType: 'json',
			        
			        success: function(data) {
			        	
			        	if(data.length > 0){
			        		
			        		$( "#address-verification" ).dialog( "open" );
			        		
			            	 for(var i = data.length-1; i >= 0; i--){
			            		 var addressString = data[i];
			            		 if(i == data.length-1)
			            			 $("#recommendedaddress").prepend($('<br />'));
			            		 $("#recommendedaddress").prepend($('<div id="address'+i+'" onclick="newAddress(address'+i+')"><a href="#">'+addressString.replace(',,',',')+'</a></div>'));	 
			            	 }
			            	 
			            	 $("#originalAddress").append($addressLine1+"<br />");
			            	 if($addressLine2 !== "")
			            		 $("#originalAddress").append($addressLine2+"<br />");
			            	 if($zip !== "")
			            	 	$("#originalAddress").append($city+","+$state+","+$zip);
			            	 else
			            		 $("#originalAddress").append($city+","+$state);
			        	}else{
			        		if(parseFloat($("#orderTotal").text()) > 0.00){
			        			$("#orderForm").submit();
			        			$("#errorMessage").html("Please fill in all required fields marked with a *");
			        		}else{
			        			$("#errorMessage").html("Please select or enter at least one product");
			        		}
			       	}
		            	 
			        	
			        },
			        
			        error: function(msg) {
			        	//Do Nothing
			        }
			        
			     });
		  }
		  //$( "#address-verification2" ).dialog( "open" );
	});
	
	$("#clearallfields")
	.click(function() {
		resetForm($("#orderForm"));
		$("#fbProductTable").hide();
		$("#productTable").show();
		$("#orderTotal").text("0.00");
		$("#plus").show();
		$("#perishableHeader").show();
		$("#perishables").show();
		$("#containsPerishables").val("");
		inlineLabel();
	});
	
	$( "#potential-matches" )
	  .button()
	  .click(function() {
	  $( "#address-verification2" ).dialog( "close" );
	  $( "#address-verification" ).dialog( "open" );
	});
	
	$("#editAddress")
	.click(function(){
		$( "#address-verification" ).dialog( "close" );
		$("#recommendedaddress").empty();
		$("#originalAddress").empty();
	});
	
	$("#closeAddress")
	.click(function(){
		$( "#address-verification" ).dialog( "close" );
		$("#recommendedaddress").empty();
		$("#originalAddress").empty();
	});
	
	$("#keepAddress")
	.click(function(){
		$( "#address-verification" ).dialog( "close" );
		$("#recommendedaddress").empty();
		$("#originalAddress").empty();
		if(parseFloat($("#orderTotal").text()) > 0.00){
			$("#orderForm").submit();
			$("#errorMessage").html("Please fill in all required fields marked with a *");
		}else{
			$("#errorMessage").html("Please select or enter at least one product");
		}
	});
	
	$( "#accordion" ).accordion({
        collapsible: true,
        active: true,
        heightStyle: "content"
       });
	
	$(".step1b #accordion").accordion( "option", "active", 1 );
	
	$("#orderCardMsgTxt")
	.keyup(function() {
		var text = $("#orderCardMsgTxt").val();
		var oCount = text.length;
		var diff = 200 - oCount;
		$("#msgCount").text(diff);
		if (oCount > 500) {
			$("#orderCardMsgTxt").val(text.substring(0, 500));
			$("#msgCount").text("0");
		}
	});
	
	$("#reviewTxt")
	.keyup(function() {
		var text = $("#reviewTxt").val();
		var oCount = text.length;
		var diff = 1000 - oCount;
		$("#reviewCharCount").text(diff);
		if (oCount > 1000) {
			$("#reviewTxt").val(text.substring(0, 1000));
			$("#reviewCharCount").text("0");
		}
	});
	
	$("#orderSpecialInstructionsTxt")
	.keyup(function() {
		var text = $("#orderSpecialInstructionsTxt").val();
		var oCount = text.length;
		var diff = 200 - oCount;
		$("#instCount").text(diff);
		if (oCount > 200) {
			$("#orderSpecialInstructionsTxt").val(text.substring(0, 200));
			$("#instCount").text("0");
		}
	});
	
	$(".orderLine")
	.each(function() {
		var $this = $(this);
		var quantity = $($this.attr('class')+" .productQuantity");
		var price = $($this.attr('class')+" .productPrice");
		quantity.change(function() {
			var orderTotal = quantity*price;
			$("#orderTotal").text(parseFloat(orderTotal, 10).toFixed(2));
		});
		price.change(function() {
			var orderTotal = quantity*price;
			$("#orderTotal").text(parseFloat(orderTotal, 10).toFixed(2));
		});
	});

	//Modal Close Buttons
    $("#dialogclose").click(function () {
		$(".dialog").dialog( "close" );
    });

	//Close Modal on Overlay Click
    $(".ui-widget-overlay").click(function () {
		$(".dialog").dialog( "close" );
    });
    
    $("#layouttoggle")
    .click(function() {
    	window.location.assign("contemporary.htm?view=close&timestamp="+ new Date().getTime());
    });
    
    $("#modifySearch")
    .click(function() {
    	window.location.assign("?view=modifysearch&timestamp="+ new Date().getTime());
    });
    
    $("#goDropship")
    .click(function() {
    	window.location.assign("?view=dropship&timestamp="+ new Date().getTime());
    });
    
    $("#residentialSearchTab")
    .click(function() {
    	$("#rightside").hide();
    	$("#advanced-search-layout").hide();
    	$("#leftside").show();
    	if($("#searchSelCountry").val() !== "1"){
	    	$("#searchSelCountry")[0].selectedIndex = 0;
	    	$("#searchSelCountry").trigger('change');
    	}
    	$("#countrySelectDiv").hide();
    	$("#searchOr").show();
    	$("#residential").html("Residential/Business Search");
    	$("#form1aArea").html("<form id=\"form1a\" style=\"margin-top: -20px;\" >");
    	
    });
    
    $("#funeralHomeSearchTab")
    .click(function() {
    	$("#leftside").hide();
    	$("#advanced-search-layout").hide();
    	$("#rightside").show();
	$("#facilityDD")[0].selectedIndex = 1;
	$("#facility").html("Funeral Home Search");
	$("#facilityNameDD option:first").text("Funeral Homes");
	if($("#searchCity option:selected").text() !== "City"){ populateFacilitiesDD(); }
    });
    
    $("#nursingHomeSearchTab")
    .click(function() {
    	$("#leftside").hide();
    	$("#advanced-search-layout").hide();
    	$("#rightside").show();
	$("#facilityDD")[0].selectedIndex = 2;
	$("#facility").html("Nursing Home Search");
	$("#facilityNameDD option:first").text("Nursing Homes");
	if($("#searchCity option:selected").text() !== "City"){ populateFacilitiesDD(); }
    });
    
    $("#hospitalSearchTab")
    .click(function() {
    	$("#leftside").hide();
    	$("#advanced-search-layout").hide();
    	$("#rightside").show();
	$("#facilityDD")[0].selectedIndex = 3;
	$("#facility").html("Hospital Search");
	$("#facilityNameDD option:first").text("Hospitals");
	if($("#searchCity option:selected").text() !== "City"){ populateFacilitiesDD(); }
    });
    
    $("#advancedSearchTab")
    .click(function() {
    	$("#leftside").hide();
    	$("#rightside").hide();
    	$("#advanced-search-layout").show();
    });
    
    $("#internationalSearchTab")
    .click(function() {
    	$("#countrySelectDiv").show();
    	if($("#searchSelCountry").val() == "1"){
    		$("#searchSelCountry")[0].selectedIndex = 1;
    		$("#searchSelCountry").trigger('change');
    	}
    	$("#residential").html("International Search");
    	$("#rightside").hide();
    	$("#advanced-search-layout").hide();
    	$("#searchOr").hide();
    	$("#form1aArea").html("<form id=\"form1a\" style=\"margin-top: -5px;\" >");
    	$("#leftside").show();
    });
    
    $(".fbOrderLine")
	.each(function() {
		var $this = $(this);
		var quantity = $($this.attr('class')+" .fbProductQuantity");
		var price = $($this.attr('class')+" .fbProductPrice");
		quantity.change(function() {
			var fbOrderTotal = quantity*price;
			$("#orderTotal").text(parseFloat(fbOrderTotal, 10).toFixed(2));
		});
		price.change(function() {
			var fbOrderTotal = quantity*price;
			$("#orderTotal").text(parseFloat(fbOrderTotal, 10).toFixed(2));
		});
	});
    
    $("#ordProductId")
    .change(function(){
    	var productId= $("#ordProductId option:selected").text();
    	if(productId === "Fruit Bouquet"){
    		$("#fbProductTable").show();
    		$("#productTable").hide();
    		$("#plus").hide();
    		$("#perishables").hide();
    		$("#perishableHeader").hide();
    		$("#containsPerishables").val("Y");
    	}else{
    		$("#fbProductTable").hide();
    		$("#productTable").show();
    		$("#plus").show();
    		$("#perishables").show();
    		$("#perishableHeader").show();
    		$("#containsPerishables").val("");
    	}
    });
    
    if($("#searchDeliveryDate").val() === ""){
    	var today = new Date();
    	if (today.getHours() > 14) {		// is it after 2:00 PM?, then use "tomorrow"
			today.setDate(today.getDate() + 1);
		}
    	var dd = today.getDate();
    	var mm = today.getMonth()+1; //January is 0!

    	var yyyy = today.getFullYear();
    	if(dd<10){dd='0'+dd;} if(mm<10){mm='0'+mm;} today = mm+'/'+dd+'/'+yyyy;
    	
    	$("#searchDeliveryDate").val(today);
    }
    
    if($("#deliveryDate2").val() === ""){
    	var today = new Date();
    	if (today.getHours() > 14) {		// is it after 2:00 PM?, then use "tomorrow"
			today.setDate(today.getDate() + 1);
		}
    	var dd = today.getDate();
    	var mm = today.getMonth()+1; //January is 0!

    	var yyyy = today.getFullYear();
    	if(dd<10){dd='0'+dd;} if(mm<10){mm='0'+mm;} today = mm+'/'+dd+'/'+yyyy;
    	
    	$("#deliveryDate2").val(today);
    }
    
    if($("#advsearchDeliveryDate").val() === ""){
    	var today = new Date();
    	if (today.getHours() > 14) {		// is it after 2:00 PM?, then use "tomorrow"
			today.setDate(today.getDate() + 1);
		}
    	var dd = today.getDate();
    	var mm = today.getMonth()+1; //January is 0!

    	var yyyy = today.getFullYear();
    	if(dd<10){dd='0'+dd;} if(mm<10){mm='0'+mm;} today = mm+'/'+dd+'/'+yyyy;
    	
    	$("#advsearchDeliveryDate").val(today);
    }
    $(openEnterOrder());
    $("#countrySelectDiv").hide();
    var selCountry= $("#addressSelCountry").val();
    if(selCountry !== "1" && selCountry !== "2" && selCountry !== "3"){
 		$("#orderFormZip").hide();
 		$("#stateorprovinceselect").hide();
 		$("#orderZip").val("");
 		$("#searchZip").hide();
 		$("#searchstateorprovinceselect").hide();
 		$("#searchZip").val("");
 		$("#orderZip").val("");
 		$("#addressCitySelect").hide();
 		$("#addressIptCity2").show();
 		$("#addressCitySelect").html('');
 		$("#searchCitySelect").hide();
 		internationalDate();
	}else{
		$("#orderFormZip").show();
 		$("#stateorprovinceselect").show();
 		$("#searchZip").show();
 		$("#searchstateorprovinceselect").show();
 		$("#addressIptCity2").hide();
 		$("#addressCitySelect").show();
 		$("#searchCitySelect").show();
	}
    
    var selCountry = $("#searchSelCountry");
	if(selCountry != undefined){
		selCountry = selCountry.val();
		if(selCountry !== "1"){
			$("#countrySelectDiv").show();
	    	$("#searchSelCountry").trigger('change');
	    	$("#residential").html("International Search");
	    	$("#rightside").hide();
	    	$("#advanced-search-layout").hide();
	    	$("#leftside").show();
		}
	}
	
	var productId= $("#ordProductId option:selected").text();
	if(productId === "Fruit Bouquet"){
		$("#fbProductTable").show();
		$("#productTable").hide();
		$("#plus").hide();
		$("#perishables").hide();
		$("#perishableHeader").hide();
		$("#containsPerishables").val("Y");
	}else{
		$("#fbProductTable").hide();
		$("#productTable").show();
		$("#plus").show();
		$("#perishables").show();
		$("#perishableHeader").show();
		$("#containsPerishables").val("");
	}
	
	calcTotal();
	
	var jcarousel = $('.jcarousel');
	if(jcarousel !== undefined){
		 $('.jcarousel').jcarousel({
			 auto:7,
			 scroll:1,
	     	 wrap: 'circular'
	     });
	}
    
});

function calcTotal(){
	
	var numberProducts = $("#productLineCount").val();
	
	var orderTotal = 0.00;
	
	for(var ii=1; ii<=numberProducts; ii++){
		
		var quantity = $("#orderLine"+ii).find('.productQuantity').val();
		var price = $("#orderLine"+ii).find('.productPrice').val();
		 var total = quantity*price;
		 if(!isNaN(total))
			 orderTotal = orderTotal + total;
	}
	
	$("#orderTotal").text(parseFloat(orderTotal, 10).toFixed(2));
	
	
	
}

function submitSearchOrder(){
	
	 $.ajax({
	    	
	        type: 'POST',
	        url: '?view=summary',
	        dataType: 'text',
	        
	        success: function(data) {
	        	
	        },
	        
	        error: function(msg) {
	        	// do nothing
	        }
	        
	     }); 
	
}

function newAddress(addressNumber){
	
	var $addressLine1 = "";
	var $addressLine2 = "";
	var $city = "";
	var $state = "";
	var $zip = "";
	
	if(addressNumber.textContent.split(",").length == 5){
		var $addressArray = addressNumber.textContent.split(",");
		$addressLine1 = $addressArray[0];
		$addressLine2 = $addressArray[1];
		$city = $addressArray[2];
		$state = $addressArray[3];
		$zip = $addressArray[4];
	}else{
		var $addressArray = addressNumber.textContent.split(",");
		$addressLine1 = $addressArray[0];
		$city = $addressArray[1];
		$state = $addressArray[2];
		$zip = $addressArray[3];
	}
		$("#orderAddressIptStreet1").val($addressLine1);
		$("#orderAddressIptStreet2").val($addressLine2);
		$("#addressIptCity :selected").text($city);
		$("#addressSelState :selected").text($state);
		$("#orderZip").val($zip);
		
		$( "#address-verification" ).dialog( "close" );
		$("#recommendedaddress").empty();
		$("#originalAddress").empty();
}

function resetForm($form) {
	   $form.find('input:text, input:password, input:file, select, textarea').val('');
	   $form.find('input:radio, input:checkbox')
	        .removeAttr('checked').removeAttr('selected');
}

function onlyPhoneNumbers(evt){
	var e = event || evt;
	var charCode = e.which || e.keyCode;

	if (charCode > 31 && (charCode < 48 || charCode > 57))
		if(charCode == 32 || charCode == 40 || charCode == 41 || charCode == 45 ) return true;
		else return false;

	return true;
}

function onlyNumbers(evt){
	var e = event || evt;
	var charCode = e.which || e.keyCode;

	if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	
	return true;
}

function formatPhoneNumber() {
	
	var oValue = $("#orderAddressIptPhone").val();
	var charsRe = /\(|\)|\-|\.|\ /g;
	var oStripped = oValue.replace(charsRe,'');
	var numRe = /(^-?\d\d*$)/;
	if(oValue.length ===10){
		var oPhone = '('+oStripped.substring(0,3)+')'+ ' ' + oStripped.substring(3,6) + '-' + oStripped.substring(6,10);
		$("#orderAddressIptPhone").val(oPhone);
	}
}

function  openEnterOrder(){
    var emptyOrder= $("#emptyorder").val();
    if (emptyOrder=="emptyorder"){
    
    	$("#accordion").accordion( "option", "active", 1);
    	$("#orderNoResultsDiv").show();


    }else if(emptyOrder == "dropship"){

    	$("#accordion").accordion( "option", "active", 2);

    }else{
            $("#accordion").accordion( "option", "active", 0);
    }

    
}

function advancedSearch(){
	var shopcode=$("#code").val();
	var shopphone=$("#phone1").val();
	var shopname=$("#shop_name").val();
	var deliveryDate=$("#advsearchDeliveryDate").val();
	if(shopcode === "Shop Code" && shopphone === "Phone number" && shopname === "Shop Name"){
		$("#advSearchError").html("Please Enter a Shop Code or a Phone Number or a Shop Name");
	}else{
		//alert(shopcode + "/"+ shopphone + "/"+ shopname);
		 $.ajax({
		    	
		        type: 'GET',
		        url: 'services/advancesearch?shopcode='+shopcode+'&shopphone='+shopphone+'&shopname='+shopname+'&deliverydate='+deliveryDate+'&timestamp='+ new Date().getTime(),
		        dataType: 'text',
		        
		        success: function(data) {
	            	 window.location.href = "?view=searchResults&from=search&timestamp="+ new Date().getTime();
		        	
		        },
		        
		        error: function(msg) {
	            	 //window.location.href = "?view=searchResults&from=search&timestamp="+ new Date().getTime();
		        }
		        
		     }); 
	}

	
}

function selectOrderCountry(){
	var selCountry= $("#addressSelCountry").val();
	var select = $("#addressSelState");
	var url =  nsBloomNet.ioServer['citiesByState'];
	var countryId = selCountry;
	
	if (nsBloomNet.ioServer['servertype'] === 'server') {
		url += countryId + '?timestamp='+ new Date().getTime();
	}
	$.ajax({

	        type: 'GET',
	        url: url,
	        dataType: 'json',

	        success: function(data) {
	        	buildOrderStates(data,select);

	        },

	        error: function(msg) {
	        	// do nothing
	        }

	     });
}

function buildOrderStates(data,select){
	var oRecords = data.results;
	var oCount = oRecords.length;
	var selCountry = $("#addressSelCountry");
	if(selCountry != undefined){
		selCountry = selCountry.val();
		if (oCount === 0) {
			return;
		}
		var oRecord;
		var oHtml = '';
		if(selCountry === "1" || selCountry === "2" || selCountry === "3")
			oHtml = '<option selected="selected" value="">State/Province</option>';
		for (var oIdx=0; oIdx<oCount; oIdx++) {
			oRecord = oRecords[oIdx];
			if(selCountry !== "1" && selCountry !== "2" && selCountry !== "3" && oIdx == 0){
				oHtml += '<option value="'+oRecord['id']+'" selected="selected">'+oRecord['name']+'</option>';
			}else{
				oHtml += '<option value="'+oRecord['id']+'">'+oRecord['name']+'</option>';
			}
		}
		
		select.append(oHtml);
		if(selCountry !== "1" && selCountry !== "2" && selCountry !== "3"){
			$("#orderSelState").val($("#addressSelState option:selected").text());
			var selState= $("#addressSelState").val();
	 		var select = $("#addressIptCity");
	 		var select2= $("");
	 		$("#orderFormZip").hide();
	 		$("#stateorprovinceselect").hide();
	 		$("#addressCitySelect").hide();
	 		$("#addressIptCity2").show();
	 		$("#addressCitySelect").html('');
	 		$("#orderZip").val("");
	 		buildCitiesAC(selState,select,select2);
		}else{
			$("#orderFormZip").show();
	 		$("#stateorprovinceselect").show();
	 		$("#addressIptCity2").hide();
	 		$("#addressCitySelect").show();
	 		$("#addressCitySelect").html('<select class="clickable" name=addressIptCity id="addressIptCity" title="City"><option selected="selected" value="">City</option></select>');
		}
	}
	
	var searchCountry = $("#addresssearchCountry");
	if(searchCountry != undefined){
		searchCountry = searchCountry.val();
		if (oCount === 0) {
			return;
		}
		var oRecord;
		var oHtml = '';
		if(searchCountry === "1" || searchCountry === "2" || searchCountry === "3")
			oHtml = '<option selected="selected" value="">State/Province</option>';
		for (var oIdx=0; oIdx<oCount; oIdx++) {
			oRecord = oRecords[oIdx];
			if(searchCountry !== "1" && searchCountry !== "2" && searchCountry !== "3" && oIdx == 0){
				oHtml += '<option value="'+oRecord['id']+'" selected="selected">'+oRecord['name']+'</option>';
			}else{
				oHtml += '<option value="'+oRecord['id']+'">'+oRecord['name']+'</option>';
			}
		}
	}
}

function internationalDate(){
	var today = new Date();
	today.setHours(0, 0, 0, 0);
	var selectedDate = Date.parse($("#searchDeliveryDate").val());
	if(today.getTime() == selectedDate){
		var dd = today.getDate()+1;
    	var mm = today.getMonth()+1; //January is 0!
    	var yyyy = today.getFullYear();
    	if(dd<10){dd='0'+dd;} if(mm<10){mm='0'+mm;} today = mm+'/'+dd+'/'+yyyy;
    	$("#searchDeliveryDate").val(today);
	}
}

function showCityListings(){
	$("#messagearea").hide();
	$("#searchcontentblock").show();
	$("#buttonarea").show();
}

function activateProduct(lineID, productCode, productDescription, productPrice, id){
	var quantity = $("#"+lineID).find(".fbProductQuantity").val();
	if(quantity !== 'undefined' && quantity !== 'QTY' && quantity !== '0' && quantity !== ''){
		$("#"+lineID).html('<span><img src="http://directory.bloomnet.net/bloomnet-images/dropship/'+productCode+'.png" class="productImage"/><br />'+productCode+'<br />$'+productPrice+'</span>&nbsp;' +
						'<input readonly type="hidden" name="productCode" id="fbProductCode" class="productCode" title="Product Code" value="'+productCode+'" />' +
						'<input type="hidden" readonly name="productDescription" id="fbProductDescription" title="Product Description" class="productDescription" value="Fruit Bouquet Product Code: '+productCode+': '+productDescription.split('<br')[0]+'" />' +
						'<span class="fbProdDescription">'+productDescription+'</span> &nbsp;' +
						'<input onchange="activateProduct(\'fbOrderLine'+id+'\',\''+productCode+'\',\''+productDescription+'\',\''+productPrice+'\',\''+id+'\')" type="text" name="productQuantity" id="fbProductQuantity" class="fbProductQuantity" title="QTY" value="'+quantity+'" onchange="fbCalcTotal()" onkeypress="return onlyNumbers();" />' +
						'<input readonly type="hidden" name="productPrice" id="fbProductPrice" class="productPrice" title="Price" value="'+productPrice+'" onchange="fbCalcTotal()" onkeypress="return onlyNumbers();" />');
	}else{
		$("#"+lineID).html('<span><img src="http://directory.bloomnet.net/bloomnet-images/dropship/'+productCode+'.png" class="productImage"/><br />'+productCode+'<br />$'+productPrice+'</span>&nbsp;' +
				'<input readonly type="hidden" name="dummyCode" id="fbProductCode" class="productCode" title="Product Code" value="'+productCode+'" />' +
				'<input type="hidden" readonly name="dummyDescription" id="fbProductDescription" title="Product Description" class="productDescription" value="Fruit Bouquet Product Code: '+productCode+': '+productDescription.split('<br')[0]+'" />' +
				'<span class="fbProdDescription">'+productDescription+'</span> &nbsp;' +
				'<input onchange="activateProduct(\'fbOrderLine'+id+'\',\''+productCode+'\',\''+productDescription+'\',\''+productPrice+'\',\''+id+'\')" type="text" name="dummyQuantity" id="fbProductQuantity" class="fbProductQuantity" title="QTY" onchange="fbCalcTotal()" onkeypress="return onlyNumbers();" />' +
				'<input readonly type="hidden" name="dummyPrice" id="fbProductPrice" class="productPrice" title="Price" value="'+productPrice+'" onchange="fbCalcTotal()" onkeypress="return onlyNumbers();" />');
	}
	fbCalcTotal();
	$(inlineLabel());
}

function fbCalcTotal(){
	
	var numberProducts = $("#fbProductLineCount").val();
	
	var fbOrderTotal = 0.00;
	
	for(var ii=1; ii<=numberProducts; ii++){
		
		var quantity = $("#fbOrderLine"+ii).find('.fbProductQuantity').val();
		var price = $("#fbOrderLine"+ii).find('.productPrice').val();
		 var total = quantity*price;
		 if(!isNaN(total))
			 fbOrderTotal = fbOrderTotal + total;
	}
	
	$("#orderTotal").text(parseFloat(fbOrderTotal, 10).toFixed(2));
	
	
	
}