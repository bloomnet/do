$(document).ready(function() {
	
	jQuery.validator.addMethod("notEqual", function(value, element, param) {
		  return this.optional(element) || value != param;
		}, "Please specify a different (non-default) value");
	
	jQuery.validator.addMethod("notEqualSelect", function(text, element, param) {
		  return this.optional(element) || text != param;
		}, "Please specify a different (non-default) value");
	
	$("#dropshipForm").validate({
		
				//errorClass: "my-error-class",

								rules : {
							orderNameFirst : {
								required : true,
								notEqual : "First name"
							},
							orderNameLast : {
								required : true,
								notEqual : "Last name"
							},
							orderAddressIptStreet1 : {
								required : true,
								notEqual : "Street Address 1"
							},
							addressIptCity : {
								required : true,
								notEqualSelect : "City"
							},
							addressSelState : {
								required : true,
								notEqualSelect : "State/Province"
							},
							orderAddressIptPhone : {
								required : true,
								notEqual : "Phone number"
							},
							orderSelOcassion : {
								required : true,
								notEqual : "Occasion"
							},
							productQuantity : {
								required : true,
								notEqual : "Quantity"
							},
							productDescription : {
								required : true,
								notEqual : "Product Description"
							},
							productPrice : {
								required : true,
								notEqual : "Price"
							}

		},
	messages: {
		orderNameFirst: "&nbsp;*",
		orderNameLast: "&nbsp;*",
		orderAddressIptStreet1: "&nbsp;*",
		addressIptCity: { required: "*" },
		addressSelState: { required: "*" },
		orderAddressIptPhone: "&nbsp;*",
		orderSelOcassion: "*",
		productQuantity: "*&nbsp;",
		productDescription: "*",
		productPrice: "&nbsp;*",
		orderZip: "*"
	}
		
	});
	
	$(inlineLabel());
	
	$("#dsAddressSelState")
	.change(function() {
		$("#dsOrderSelState").val($("#dsAddressSelState option:selected").text());
	});
	
	$("#dsAddressIptCity")
	.change(function() {
		$("#dsOrderSelCity").val($("#dsAddressIptCity option:selected").text());
	});
	
	$("#dsOrderAddressIptPhone")
	.keypress(function() {
		return onlyPhoneNumbers();
	})
	.change(function() {
		return dsFormatPhoneNumber();
	});
	
	
	if($("#dsOrderSelState").val() !== 'State/Province' && $("#dsOrderSelState").val() !== ''){
		var selState= $("#dsAddressSelState").val();
		$("#dsAddressIptCity").empty();
		var select = $("#dsAddressIptCity");
		dsBuildCitiesAC(selState,select);

		
		if($("#dsAddressIptCity").html() === ""){
			$("#dsAddressIptCity").html("<option selected='selected'>City</option>");
		}
	
	}
    
   	$('#dsAddressSelState').change(function() {
   		var selState= $("#dsAddressSelState").val();
		$("#dsAddressIptCity").empty();
		var select = $("#dsAddressIptCity");
		dsBuildCitiesAC(selState,select);
   	});	

	$( "#dsAddress-verification" ).dialog({
		autoOpen: false,
		draggable: false,
		resizable: false,
		width: 630,
		height: 450,
		modal: true
	});
	
	$( "#dsContinue" )
	  .button()
	  .click(function() {
		  var $addressLine1 = $("#dsOrderAddressIptStreet1").val();
		  var $addressLine2 = $("#dsOrderAddressIptStreet2").val();
		  var $city = $("#dsAddressIptCity :selected").text();
		  var $state = $("#dsAddressSelState :selected").text();
		  var $zip = $("#dsOrderZip").val();
		  if($addressLine1 === 'Street Address 1' || $city === 'City' || $state === 'State/Province'){			  
			if($("#dsOrderTotal").html() === "0.00"){
				$("#dsErrorMessage").html("Please select at least one product to continue.");
			}
			else{
				$("#dropshipForm").submit();
				$("#dsErrorMessage").html("Please fill in all required fields marked with a *");
			}
		  }else if($("#dsOrderTotal").html() === "0.00"){
				$("#dsErrorMessage").html("Please select at least one product to continue.");
		  }else{			  
			  if($addressLine2 === 'undefined' || $addressLine2 === 'Street Address 2')
				  $addressLine2 = "";
			  if($zip === 'undefined' || $zip === 'Zip / Postal Code')
				  $zip = "";
			  
			  var $url = 'services/verifyaddress?addressLine1='+$addressLine1+'&addressLine2='+$addressLine2+'&city='+$city+'&stateShort='+$state+'&zip='+$zip;
			  
			  $.ajax({
			    	
			        type: 'GET',
			        url: $url,
			        dataType: 'json',
			        
			        success: function(data) {
			        	
			        	if(data.length > 0){
			        		
			        		$( "#dsAddress-verification" ).dialog( "open" );
			        		
			            	 for(var i = data.length-1; i >= 0; i--){
			            		 var addressString = data[i];
			            		 if(i == data.length-1)
			            			 $("#dsRecommendedaddress").prepend($('<br />'));
			            		 $("#dsRecommendedaddress").prepend($('<div id="address'+i+'" onclick="dsNewAddress(address'+i+')"><a href="#">'+addressString.replace(',,',',')+'</a></div>'));	 
			            	 }
			            	 
			            	 $("#dsOriginalAddress").append($addressLine1+"<br />");
			            	 if($addressLine2 !== "")
			            		 $("#dsOriginalAddress").append($addressLine2+"<br />");
			            	 if($zip !== "")
			            	 	$("#dsOriginalAddress").append($city+","+$state+","+$zip);
			            	 else
			            		 $("#dsOriginalAddress").append($city+","+$state);
			        	}else{  
		    				$("#dropshipForm").submit();
		    				$("#dsErrorMessage").html("Please fill in all required fields marked with a *");
			        	}
		            	 
			        	
			        },
			        
			        error: function(msg) {
			        	//Do Nothing
			        }
			        
			     });
		  }
		  //$( "#address-verification2" ).dialog( "open" );
	});
	
	$("#dsClearallfields")
	.click(function() {
		dsResetForm($("#dropshipForm"));
		inlineLabel();
	});
	
	
	$("#dsEditAddress")
	.click(function(){
		$( "#dsAddress-verification" ).dialog( "close" );
		$("#dsRecommendedaddress").empty();
		$("#dsOriginalAddress").empty();
	});
	
	$("#dsCloseAddress")
	.click(function(){
		$( "#dsAddress-verification" ).dialog( "close" );
		$("#dsRecommendedaddress").empty();
		$("#dsOriginalAddress").empty();
	});
	
	$("#dsKeepAddress")
	.click(function(){
		$( "#dsAddress-verification" ).dialog( "close" );
		$("#dsRecommendedaddress").empty();
		$("#dsOriginalAddress").empty();
		$("#dropshipForm").submit(); 
		$("#dsErrorMessage").html("Please fill in all required fields marked with a *");
	});
	
	$("#dsOrderCardMsgTxt")
	.keyup(function() {
		var text = $("#dsOrderCardMsgTxt").val();
		var oCount = text.length;
		var diff = 200 - oCount;
		$("#dsMsgCount").text(diff);
		if (oCount > 200) {
			$("#dsOrderCardMsgTxt").val(text.substring(0, 200));
			$("#dsMsgCount").text("0");
		}
	});
	
	$("#dsOrderSpecialInstructionsTxt")
	.keyup(function() {
		var text = $("#dsOrderSpecialInstructionsTxt").val();
		var oCount = text.length;
		var diff = 200 - oCount;
		$("#dsInstCount").text(diff);
		if (oCount > 200) {
			$("#dsOrderSpecialInstructionsTxt").val(text.substring(0, 200));
			$("#dsInstCount").text("0");
		}
	});
	
	$(".dsOrderLine")
	.each(function() {
		var $this = $(this);
		var quantity = $($this.attr('class')+" .dsProductQuantity");
		var price = $($this.attr('class')+" .dsProductPrice");
		quantity.change(function() {
			var dsOrderTotal = quantity*price;
			$("#dsOrderTotal").text(parseFloat(dsOrderTotal, 10).toFixed(2));
		});
		price.change(function() {
			var dsOrderTotal = quantity*price;
			$("#dsOrderTotal").text(parseFloat(dsOrderTotal, 10).toFixed(2));
		});
	});
	
	$('.dsDeliverydate').datepicker({ beforeShowDay: $.datepicker.noWeekends });	
	$('.dsMinicalendar').click(function(){
		$(this).parent().find('.dsDeliverydate').focus();
	});
	
	$("#dropshipDeliveryDate")
	.change(function(){
		var today = new Date();
		if(today.getDay() == 5 && today.getHours() > 15){
			today.setDate(today.getDate() + 4);
		}else if(today.getDay() == 6 && today.getHours() > 15){
			today.setDate(today.getDate() + 4);
		}else if(today.getDay() == 6){
			today.setDate(today.getDate() + 3);
		}else if(today.getDay() == 0){
			today.setDate(today.getDate() + 3);
		}else if (today.getHours() > 15) {		// is it after 3:00 PM?, then use "tomorrow"
			today.setDate(today.getDate() + 3);
		}else{
			today.setDate(today.getDate() + 2);
		}

    	if(today.getDay() == 6)
    		today.setDate(today.getDate() + 2);
    	if(today.getDay() == 0)
    		today.setDate(today.getDate() + 1);
    	var dd = today.getDate();
    	var mm = today.getMonth()+1; //January is 0!

    	var yyyy = today.getFullYear();
    	if(dd<10){dd='0'+dd;} if(mm<10){mm='0'+mm;} today = mm+'/'+dd+'/'+yyyy;
    	
    	if($("#dropshipDeliveryDate").val() < today){
    		$("#dropshipDeliveryDate").val(today);
    		$("#dsDateError").html("The selected date is too early for delivery. The next available date has been selected by default.");
    	}else{
    		var selectedDate = $("#dropshipDeliveryDate").val();
    		var selectedDateObject = new Date(selectedDate);
    		var badDate = false;
    		if(selectedDateObject.getDay() == 6){
    			selectedDateObject.setDate(selectedDateObject.getDate() + 2);
    			badDate = true;
    		}
        	if(selectedDateObject.getDay() == 0){
        		selectedDateObject.setDate(selectedDateObject.getDate() + 1);
        		badDate = true;
        	}
        	if(badDate)
        		$("#dsDateError").html("The selected date is unavailable for delivery. The next available date after the selected date has been chosen.");
        	else
        		$("#dsDateError").html("");
        	var dd = selectedDateObject.getDate();
        	var mm = selectedDateObject.getMonth()+1; //January is 0!
        	var yyyy = selectedDateObject.getFullYear();
        	if(dd<10){dd='0'+dd;} if(mm<10){mm='0'+mm;} selectedDateObject = mm+'/'+dd+'/'+yyyy;
        	$("#dropshipDeliveryDate").val(selectedDateObject);
    	}
	});
	
	if($("#dropshipDeliveryDate").val() === ""){
    	var today = new Date();
    	if(today.getDay() == 5 && today.getHours() > 15){
			today.setDate(today.getDate() + 4);
		}else if(today.getDay() == 6 && today.getHours() > 15){
			today.setDate(today.getDate() + 4);
		}else if(today.getDay() == 6){
			today.setDate(today.getDate() + 3);
		}else if(today.getDay() == 0){
			today.setDate(today.getDate() + 3);
		}else if (today.getHours() > 15) {		// is it after 3:00 PM?, then use "tomorrow"
			today.setDate(today.getDate() + 3);
		}else{
			today.setDate(today.getDate() + 2);
		}
    	
    	if(today.getDay() == 6)
    		today.setDate(today.getDate() + 2);
    	if(today.getDay() == 0)
    		today.setDate(today.getDate() + 1);
    	var dd = today.getDate();
    	var mm = today.getMonth()+1; //January is 0!

    	var yyyy = today.getFullYear();
    	if(dd<10){dd='0'+dd;} if(mm<10){mm='0'+mm;} today = mm+'/'+dd+'/'+yyyy;
    	
    	$("#dropshipDeliveryDate").val(today);
    }else{
    	var today = new Date();
    	if(today.getDay() == 5 && today.getHours() > 15){
			today.setDate(today.getDate() + 4);
		}else if(today.getDay() == 6 && today.getHours() > 15){
			today.setDate(today.getDate() + 4);
		}else if(today.getDay() == 6){
			today.setDate(today.getDate() + 3);
		}else if(today.getDay() == 0){
			today.setDate(today.getDate() + 3);
		}else if (today.getHours() > 15) {		// is it after 3:00 PM?, then use "tomorrow"
			today.setDate(today.getDate() + 3);
		}else{
			today.setDate(today.getDate() + 2);
		}

    	if(today.getDay() == 6)
    		today.setDate(today.getDate() + 2);
    	if(today.getDay() == 0)
    		today.setDate(today.getDate() + 1);
    	var dd = today.getDate();
    	var mm = today.getMonth()+1; //January is 0!

    	var yyyy = today.getFullYear();
    	if(dd<10){dd='0'+dd;} if(mm<10){mm='0'+mm;} today = mm+'/'+dd+'/'+yyyy;
    	
    	if($("#dropshipDeliveryDate").val() < today)
    		$("#dropshipDeliveryDate").val(today);
    	
    }
	dsCalcTotal();
});

function dsCalcTotal(){
	
	var numberProducts = $("#dsProductLineCount").val();
	
	var dsOrderTotal = 0.00;
	
	for(var ii=1; ii<=numberProducts; ii++){
		
		var quantity = $("#dsOrderLine"+ii).find('.dsProductQuantity').val();
		var price = $("#dsOrderLine"+ii).find('.productPrice').val();
		 var total = quantity*price;
		 if(!isNaN(total))
			 dsOrderTotal = dsOrderTotal + total;
	}
	
	$("#dsOrderTotal").text(parseFloat(dsOrderTotal, 10).toFixed(2));
	
	
	
}


function dsNewAddress(addressNumber){
	
	var $addressLine1 = "";
	var $addressLine2 = "";
	var $city = "";
	var $state = "";
	var $zip = "";
	
	if(addressNumber.textContent.split(",").length == 5){
		var $addressArray = addressNumber.textContent.split(",");
		$addressLine1 = $addressArray[0];
		$addressLine2 = $addressArray[1];
		$city = $addressArray[2];
		$state = $addressArray[3];
		$zip = $addressArray[4];
	}else{
		var $addressArray = addressNumber.textContent.split(",");
		$addressLine1 = $addressArray[0];
		$city = $addressArray[1];
		$state = $addressArray[2];
		$zip = $addressArray[3];
	}
		$("#dsOrderAddressIptStreet1").val($addressLine1);
		$("#dsOrderAddressIptStreet2").val($addressLine2);
		$("#dsAddressIptCity :selected").text($city);
		$("#dsAddressSelState :selected").text($state);
		$("#dsOrderZip").val($zip);
		
		$( "#dsAddress-verification" ).dialog( "close" );
		$("#dsRecommendedaddress").empty();
		$("#dsOriginalAddress").empty();
}

function dsResetForm($form) {
	   $("#dsOrderNameFirst").val('');
	   $("#dsOrderNameLast").val('');
	   $("#dsOrderAddressIptStreet1").val('');
	   $("#dsOrderAddressIptStreet2").val('');
	   $("#dsOrderZip").val('');
	   $("#dsOrderAddressIptStreet1").val('');
	   $("#dsAddressSelState").val('');
	   $("#dsAddressIptCity").val('');
	   $("#dsOrderAddressIptPhone").val('');
	   $("#dsOrderSelOcassion").val('');
	   $("#dsOrderCardMsgTxt").val('');
	   $("#dsOrderSpecialInstructionsTxt").val('');
	   $(".dsProductQuantity").each(function(){
		   $(this).val('QTY');
	   });
	   $form.find('input:radio, input:checkbox')
	        .removeAttr('checked').removeAttr('selected');
	   dsCalcTotal();
}

function dsExamineOrderZip() {
	var $searchZip=$("#dsOrderZip").val();
	if ($searchZip.length >= 5 && $searchZip !== "Zip / Postal Code"){
		var $url =  nsBloomNet.ioServer['cityStateByZip'];
		$url += $searchZip + '/city?timetamp='+ new Date().getTime();
		
		$.getJSON( $url, function( data ) {
			$.each( data.results, function( i, val ) {
				var city = val.id;
				var state = val['state_id'];
				var cityname = val.name;
				var statename = val['state_name'];
		        	if (state === '') {
		        	}else{
		        		$("#dsOrderSelCity").val(cityname);
		        		$("#dsOrderSelState").val(statename);
		        		$("#dsAddressSelState option[value='"+state+"']").attr('selected','selected');
		        		finalCity = city;
		        	}

			});
		}).done(function(){
			var selState= $("#dsAddressSelState").val();
    		var select = $("#dsAddressIptCity");
    		var url =  nsBloomNet.ioServer['citiesByState'];
    		var stateCode = selState;
    		if (!stateCode) {
    			return;
    		}
    		if (nsBloomNet.ioServer['servertype'] === 'server') {
    			url += stateCode + '/city?timestamp='+ new Date().getTime();
    		}else{
    			url += '?timestamp='+ new Date().getTime();
    		}

    		$.ajax({

    		        type: 'GET',
    		        url: url,
    		        dataType: 'json',

    		        success: function(data) {
    		        	dsBuildCitiesACCont(data,select);

    		        },

    		        error: function(msg) {
    		        	// do nothing
    		        }

    		     });
	     });
	}
}

function dsBuildCitiesAC(selState,select) {
	var url =  nsBloomNet.ioServer['citiesByState'];
	var stateCode = selState;
	if (!stateCode) {
		return;
	}
	if (nsBloomNet.ioServer['servertype'] === 'server') {
		url += stateCode + '/city?timestamp='+ new Date().getTime();
	}else{
		url += '?timestamp='+ new Date().getTime();
	}

	$.ajax({

	        type: 'GET',
	        url: url,
	        dataType: 'json',

	        success: function(data) {
	        	dsBuildCitiesACCont(data,select);

	        },

	        error: function(msg) {
	        	// do nothing
	        }

	     });
}

function dsBuildCitiesACCont(data,select){
	var oRecords = data.results;
	var oCount = oRecords.length;
	if (oCount === 0) {
		return;
	}

	var oRecord;
	var oPreviousCity = "";
	var orderCity = $("#dsOrderSelCity").val();
	var oHtml = '<option selected="selected" value="">City</option>';
	for (var oIdx=0; oIdx<oCount; oIdx++) {
		oRecord = oRecords[oIdx];
		if(oRecord['name'] !== oPreviousCity){
			if(orderCity === oRecord['name']){
				oHtml += '<option selected="selected" value="'+oRecord['id']+'">'+oRecord['name']+'</option>';
			}else{
				oHtml += '<option value="'+oRecord['id']+'">'+oRecord['name']+'</option>';
			}
		}
		oPreviousCity = oRecord['name'];
	}
	
	select.append(oHtml);
}

function dsAddOrderLine() {
	//var oLineTemplate = Y$.one('#newOrderLineTemplate').get('innerHTML');
	//var newInput = $("<input name='new_field' type='text'>");
	var numberProducts = parseInt($("#dsProductLineCount").val());
	var newNumber = numberProducts + 1;
	var newInput = $("<div class='orderLine' id='dsOrderLine"+newNumber+"'><input type='text' name='productCode' id='dsProductCode' class='productCode'  title='Product Code'/>" +
			"<input type='text' name='productQuantity' id='dsProductQuantity' class='productQuantity' title='Quantity' onkeypress='return onlyNumbers();' onchange='dsCalcTotal();' />" +
			"<input type='text' name='productDescription' id='dsProductDescription' class='productDescription' title='Product Description' /> " +
			"<input type='text' name='productPrice' id='dsProductPrice' class='productPrice' title='Price' onkeypress='return onlyNumbers();' onchange='dsCalcTotal();' />" +
			"<a class='clickable green' onclick='dsRemoveOrderLine(\"dsOrderLine"+newNumber+"\")'>&nbsp;Delete</a>" +
			"<br/>" +
			"</div>");

	$('#dsProductTable').append(newInput);
	
	$("#dsProductLineCount").val(newNumber);


	
	//Y$.one('#productTable').append(oLineTemplate);
	/*var oProdTable = $('#productTable');
	var oProductLines = oProdTable.all('.orderLine');
	var oNewLine = oProductLines.item(oProductLines.size()-1);
	
	
	oNewLine.one('.productDescription').on('change',editProductLine);
	oNewLine.one('.productPrice').on('change',editPrice);
	oNewLine.one('.dsProductQuantity').on('change',productQuantity);
	
	oNewLine.all('input.orderField').on('focus', _toggleInputHint);
	oNewLine.all('input.orderField').on('focus', onFocus);
	oNewLine.all('input.orderField').on('blur', _toggleInputHint);
	oNewLine.all('input.orderField').on('blur', onBlur);
//	Y$.all('input.submitable').setAttribute('disabled', 'disabled').addClass('disabled');
	checkLineCount();
	*/
	
	inlineLabel();

}

function dsRemoveOrderLine(lineID){
	$("#"+lineID).remove();
	dsCalcTotal();
}

function dsFormatPhoneNumber() {
	
	var oValue = $("#dsOrderAddressIptPhone").val();
	var charsRe = /\(|\)|\-|\.|\ /g;
	var oStripped = oValue.replace(charsRe,'');
	if(oValue.length ===10){
		var oPhone = '('+oStripped.substring(0,3)+')'+ ' ' + oStripped.substring(3,6) + '-' + oStripped.substring(6,10);
		$("#dsOrderAddressIptPhone").val(oPhone);
	}
}

function activateProduct(lineID, productCode, productDescription, productPrice, id){
	var quantity = $("#"+lineID).find(".dsProductQuantity").val();
	if(quantity !== 'undefined' && quantity !== 'QTY' && quantity !== '0' && quantity !== ''){
		$("#"+lineID).html('<img src="http://directory.bloomnet.net/bloomnet-images/dropship/'+productCode+'.png" class="productImage" /> &nbsp;' +
						'<input readonly type="hidden" name="productCode" id="dsProductCode" class="productCode" title="Product Code" value="'+productCode+'" />' +
						'<input type="hidden" readonly name="productDescription" id="dsProductDescription" title="Product Description" class="productDescription" value="'+productDescription+'" />' +
						'<span class="dsProdDescription">'+productDescription+'</span> &nbsp;' +
						'<input onchange="activateProduct(\'dsOrderLine'+id+'\',\''+productCode+'\',\''+productDescription+'\',\''+productPrice+'\',\''+id+'\')" type="text" name="productQuantity" id="dsProductQuantity" class="dsProductQuantity" title="QTY" value="'+quantity+'" onchange="dsCalcTotal()" onkeypress="return onlyNumbers();" />' +
						'<input readonly type="hidden" name="productPrice" id="dsProductPrice" class="productPrice" title="Price" value="'+productPrice+'" onchange="dsCalcTotal()" onkeypress="return onlyNumbers();" />');
	}else{
		$("#"+lineID).html('<img src="http://directory.bloomnet.net/bloomnet-images/dropship/'+productCode+'.png" class="productImage" /> &nbsp;' +
				'<input readonly type="hidden" name="dummyCode" id="dsProductCode" class="productCode" title="Product Code" value="'+productCode+'" />' +
				'<input type="hidden" readonly name="dummyDescription" id="dsProductDescription" title="Product Description" class="productDescription" value="'+productDescription+'" />' +
				'<span class="dsProdDescription">'+productDescription+'</span> &nbsp;' +
				'<input onchange="activateProduct(\'dsOrderLine'+id+'\',\''+productCode+'\',\''+productDescription+'\',\''+productPrice+'\',\''+id+'\')" type="text" name="dummyQuantity" id="dsProductQuantity" class="dsProductQuantity" title="QTY" onchange="dsCalcTotal()" onkeypress="return onlyNumbers();" />' +
				'<input readonly type="hidden" name="dummyPrice" id="dsProductPrice" class="productPrice" title="Price" value="'+productPrice+'" onchange="dsCalcTotal()" onkeypress="return onlyNumbers();" />');
	}
	dsCalcTotal();
	$(inlineLabel());
}


