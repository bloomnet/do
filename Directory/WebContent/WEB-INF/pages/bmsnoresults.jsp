<%@ page import="com.flowers.portal.vo.Order" %>
<%@ page import="java.util.Date" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>Bloomnet - Directory Online</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<%long today = new Date().getTime();%>
	<link href="styles/jquery-ui-1.9.0.custom.min.css?timestamp=<%=today %>" rel="stylesheet" />	
	<link rel="stylesheet" type="text/css" href="styles/reset.css?timestamp=<%=today %>" media="screen"/>
	<jsp:include page="common/mainCss.jsp" />
	
<!--[if IE 7]>
        <link rel="stylesheet" type="text/css" href="styles/ie7.css" />
<![endif]-->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-XG7CP6XY9J"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
  		function gtag(){dataLayer.push(arguments);}
  		gtag('js', new Date());
  		gtag('config', 'G-XG7CP6XY9J');
	</script>

	<script type="text/javascript" src="scripts/jquery-1.8.2.min.js?timestamp=<%=today %>"></script>
	<script type="text/javascript" src="scripts/jquery-ui-1.9.0.custom.min.js?timestamp=<%=today %>"></script>	
	<script type="text/javascript" src="scripts/jquery.jcarousel.min.js?timestamp=<%=today %>"></script>	
	<jsp:include page="common/bmsResultsJs.jsp" />
	<jsp:include page="common/bloomnetJs.jsp" />	
	<script type="text/javascript" src="scripts/jquery.raty.min.js?timestamp=<%=today %>"></script>

	
	<!-- Hotjar Tracking Code for directory.bloomnet.net -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:1037407,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>

</head>
<body id="results">
	<div id="container">
			<div id="header">
			<div class="left">
				<a id="logo"><img src="images/logo.png" alt="Bloomnet Logo" title="Bloomnet Logo"/></a>
			</div>
			<div class="right">
				<br/>
				<div class="text">					
					<span id="questions">Questions? Call 800-256-6663</span>
				</div>
			</div>
			<div class="clear"></div>
		</div>
					
			<div id="content">
			
				<div id="accordion">
				<h2>Search Results</h2>
				<%if(request.getSession(true).getAttribute("fbSearch") != null && request.getSession(true).getAttribute("fbSearch").equals("YES")){ %>
					<div style="margin-top: -40px;">
					<div id="searchcontentblock">
						<div id="nofloristfound"> 
							<p><strong>No Florist Was Found &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></p><br/>
							<br />
							<p>We're sorry, we currently do not have a fruit fulfiller in your target area. Thank you!</p><br/>
							<form name="search-form" id="search-form" method="post" action="?view=summary&commitment=yes&timestamp=<%=today %>"  >
								<input type="hidden" id="emptyOrder" name="emptyOrder" value="${emptyOrder}">
								</form>
							<br/>
						</div>
					</div>
					</div>
				<%}else{ %>
				
				<div style="margin-top: -40px;">
					<div id="searchcontentblock">
						<div id="nofloristfound"> 
							<p><strong>No Florist Was Found &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></p><br/>
							<br />
							<p>We're sorry, your search criteria did not result in a match. To send this order to a fellow BloomNet professional florist, please contact our BloomNet Customer Service Team at 1-800-BloomNet (1-800-256-6663) or click "Send order via Commitment To Coverage" below so that we may place the order for you. Thank you!</p><br/>
							<form name="search-form" id="search-form" method="post" action="?view=summary&commitment=yes&timestamp=<%=today %>"  >
								<input type="hidden" id="emptyOrder" name="emptyOrder" value="${emptyOrder}">
								<input id="callback" style="display:none" type="text" value="<%= request.getSession(true).getAttribute("callback") %>" />
								<div class="clickable" onclick="sendFromListing('Z9980000')"><img src="images/sendorderviacommitmenttocoverage.png"/></div>
								</form>
							<br/>
						</div>
					</div>
					</div>
				<%} %>
				
			</div>		 	
			<jsp:include page="common/fillerads.jsp" />
			<div class="clear"></div>

		</div>
		</div>
	<jsp:include page="common/footerContemp.jsp" />
</body>
</html>