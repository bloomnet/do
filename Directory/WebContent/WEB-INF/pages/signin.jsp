<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page import="java.util.Date" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
  <%long today = new Date().getTime(); %>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-XG7CP6XY9J"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
  		function gtag(){dataLayer.push(arguments);}
  		gtag('js', new Date());
  		gtag('config', 'G-XG7CP6XY9J');
	</script>
	<%if(request.getSession(true).getAttribute("isR4Me") != null && request.getSession(true).getAttribute("isR4Me").equals("true")){ %>
		<title>Welcome - BloomNet Driver</title>
	<%}else{ %>
    	<title>Welcome - Directory Online</title>
    <%} %>
		<meta name="description" content="" />
		<link rel="stylesheet" type="text/css" href="css/login20241118.css?timestamp=<%=today %>" /> 
	<style>
		.error {
			color: red;
		}
	</style>
	
		
	<!-- Hotjar Tracking Code for directory.bloomnet.net -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:1037407,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>
 		
	</head>
   
   <body class=" controls-visible guest-community signed-out public-page"> 
   <div id="login">
   
<form:form method="post"> 
  <div class="aui-fieldset-content "> <input class="aui-field-input aui-field-input-hidden"  id="_58_username" name="_58_username"    type="hidden" value=""   /> 
  <div id="container"> 
  <div <%if(request.getSession(true).getAttribute("isR4Me") != null && request.getSession(true).getAttribute("isR4Me").equals("true")){ %> id="login_containerR4Me" <%}else{ %> id="login_container" <%} %>> 
  <table id="login_fields"> 
  <tr> 
  <td><div class="login_field"> <span class="aui-field aui-field-text"> 
  <span class="aui-field-content">   <label class="aui-field-label" for="_58_shop_code"> Shop Code</label> 
  </span></span> 
  </div>
  </td> 
  <td><div class="login_field"> <span class="aui-field aui-field-text"> 
  <span class="aui-field-content"> <label class="aui-field-label" for="_58_login"> Username	</label></span></span> 
  </div>
  </td> 
  <td><div class="login_field"> <span class="aui-field aui-field-text"> 
  <span class="aui-field-content"> <label class="aui-field-label" for="_58_password"> Password </label></span></span> 
  </div>
  </td> 
  <td><span id="_58_passwordCapsLockSpan" style="display: none;">Caps Lock is on.</span></td> <!-- --> 
  </tr> 
  <tr> 
  <td><div class="login_field"><span class="aui-field aui-field-text">
  <form:input path="shopCode" type="text" name="shopCode" value="" class="aui-field-input aui-field-input-text" onfocus="if (this.value=='shopCode'){this.value='';this.style.color='#000'}" onblur="if (this.value==''){this.value='shopCode';this.style.color='#c7c6c6'}" /> </span> 
  </div>
  </td> 
  <td><div class="login_field"><span class="aui-field aui-field-text"> 
  <span class='aui-field-element '> <form:input path="userName" type="text" name="username" value="" class="aui-field-input aui-field-input-text" onfocus="if (this.value=='username'){this.value='';this.style.color='#000'}" onblur="if (this.value==''){this.value='username';this.style.color='#c7c6c6'}" /> </span>
  </span></div>
  </td> 
  <td> <div class="login_field"><span class="aui-field aui-field-text">
  <span class='aui-field-element '><form:input path="password" type="password" name="password" value="" class="aui-field-input aui-field-input-text" onfocus="if (this.value=='password'){this.value='';this.style.color='#000'}" onblur="if (this.value==''){this.value='password';this.style.color='#c7c6c6'}" /> </span>  
  </span></div>
  </td> 
  <td><span id="_58_passwordCapsLockSpan" style="display: none;">Caps Lock is on.</span></td> <!-- --> 
  </tr> 
  <tr>
  <td><div class="login_field"><input class="login_signin" type="submit" value="" onclick="populateLoginValue();" /></div></td></tr> <tr> <td colspan="3"> </td> 
  </tr> 
  </table> 
  </div> </div>
   </div>  
   </form:form> 
   </div>
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<div align="center">
		<table align="center">
	            <tr> 
	              <td height="40" colspan="2" align="center">
	                <font face="Arial" size="2" color="red">
	                <%if(request.getSession(true).getAttribute("errorMessage") != null){ %>
	                	<%= request.getSession(true).getAttribute("errorMessage") %>
	                <%
	                request.getSession(true).setAttribute("errorMessage", null);
	                } %>
				<spring:bind path="command.*">
				  <c:if test="${status.errors.errorCount > 0}">
				    <c:forEach var="error" items="${status.errors.allErrors}">
				      <spring:message message="${error}"></spring:message><br>
				    </c:forEach>
				  </c:if>
				</spring:bind>
		      </font>
	              </td>
	            </tr> 
	          </table>
	</div>
  </body>
</html>