<%@page import="java.util.Date"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ page import="com.flowers.portal.vo.Order" %>
<%@ page session="false" %>
<%@ page import="java.util.Date" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<%long today = new Date().getTime(); %>
	<title>Bloomnet - Directory Online</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<link href="styles/jquery-ui-1.9.0.custom.min.css?timestamp=<%=today %>" rel="stylesheet"/>	
	<link rel="stylesheet" type="text/css" href="styles/reset.css?timestamp=<%=today %>" media="screen"/>
	<jsp:include page="common/mainCss.jsp" />
	
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-XG7CP6XY9J"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
  		function gtag(){dataLayer.push(arguments);}
  		gtag('js', new Date());
  		gtag('config', 'G-XG7CP6XY9J');
	</script>

	<script type="text/javascript" src="scripts/jquery-1.8.2.min.js?timestamp=<%=today %>"></script>
	<script type="text/javascript" src="scripts/jquery-ui-1.9.0.custom.min.js?timestamp=<%=today %>"></script>	
	<jsp:include page="common/orderSummaryJs.jsp" />	
    <jsp:include page="common/bloomnetJs.jsp" />
    
    	
	<!-- Hotjar Tracking Code for directory.bloomnet.net -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:1037407,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>
	
</head>
<body id="confirmation">
	<div id="container">
		
		<jsp:include page="common/head.jsp" />
				
		<div id="content">
			<div id="contentblock">
							<form name="order-form" id="order-form"  method="post" action="?view=submit&timestamp=<%=today %>"  >
			
				<h2>Order Summary</h2>

				<h3 class="green">Recipient Information</h3>
				<hr/>
				<% Order order = (Order)request.getAttribute("order"); %>
				<div class="innercontent">
					<div id="customerinfo" class="left">
						<h4>Deliver to:</h4>
						<%if(order.getFacilityName() != null){ %><span id="cFacName"> <c:out value="${order.facilityName}" /> </span> <br /> <%} %>
						<span id="cname"><c:out value="${order.recipient.firstName}" /> <c:out value="${order.recipient.lastName}" /></span><br/>
						<span id="caddress1"><c:out value="${order.recipient.address1}" /></span><br/>
						<%if(order.getRecipient().getAddress2() != null){ %><span><c:out value="${order.recipient.address2}" /></span><br/><%} %>
						<%if(order.getRecipient().getCountryId() != null && !order.getRecipient().getCountryId().equals("1") && !order.getRecipient().getCountryId().equals("2") && !order.getRecipient().getCountryId().equals("3")){ %>
							<span id="caddress2"><c:out value="${order.recipient.city}" />, <c:out value="${order.recipient.countryCode}" /></span><br/>
						<%}else{ %>
							<span id="caddress2"><c:out value="${order.recipient.city}" />, <c:out value="${order.recipient.state}" /> <c:out value="${order.recipient.postalCode}" /></span><br/>
						<%} %>
						<span id="cphone1"><c:out value="${order.recipient.phoneNumber}" /></span><br/>
						<%if(!order.getFulfillingShop().getShopCode().equals("Z9990000")){ %><a href="?view=backorder&timestamp=<%=today %>">Edit Address</a>
						<%}else{ %>
							<a href="?view=backDropship&timestamp=<%=today %>">Edit Address</a>
						<%} %>
					</div>
					<div id="floristinfo" class="right">
						<h4>Delivery Date: <c:out value="${order.deliveryDate}" /></h4>
						<h4>Filling Florist Information:</h4>
						<h4 id="fname"><c:out value="${order.fulfillingShop.shopName}" /> &nbsp;(<c:out value="${order.fulfillingShop.shopCode}" />)</h4>
						<c:if test="${order.fulfillingShop.address != null}">
							<c:out value="${order.fulfillingShop.address.streetAddress1}" /> <br />
							<%if(order.getRecipient().getCountryId() != null && !order.getRecipient().getCountryId().equals("1") && !order.getRecipient().getCountryId().equals("2") && !order.getRecipient().getCountryId().equals("3")){ %>
								<span id="faddress2"><c:out value="${order.fulfillingShop.address.postalCode}" /></span><br/>
							<%}else{ %>
								<span id="faddress2"><c:out value="${order.fulfillingShop.address.city.name}" />, <c:out value="${order.fulfillingShop.address.city.state.shortName}" />, <c:out value="${order.fulfillingShop.address.postalCode}" /></span><br/>
							<%} %>
						</c:if>
						<c:if test="${order.fulfillingShop.tollFreeNumber != null}">
						<span id="fphone1"><c:out value="${order.fulfillingShop.tollFreeNumber}" /></span><br/>
						</c:if>
						<span id="fphone2"><c:out value="${order.fulfillingShop.telephoneNumber}" /></span><br/>
						<%if(!order.getFulfillingShop().getShopCode().equals("Z9990000")){ %><span id="femployeename"><c:out value="${order.fulfillingShop.contactPerson}" /></span><br/>
						<a href="?view=backsearch&timestamp=<%=today %>">Select a Different Florist</a>
						<br />
						<a href="?view=modifysearch&timestamp=<%=today %>">Edit Date</a>
						<%}else{ %>
						<a href="?view=backDropship&timestamp=<%=today %>">Edit Date</a>
						<%} %>
 					</div>
					<div class="clear"></div>
				</div>
				<br/>
				<br/>
					<h3 class="green">Products</h3>
					<hr/>
						<table id="products">
							<th id="productname">Product Name</th>
							<th id="quantity">Quantity</th>
							<th id="price">Price</th>
							
							<%int count = 0;%>
									<c:forEach items="${order.describedProducts}" var="product" varStatus="loopStatus">
									<%
										String countValue = String.valueOf(count);	
									%>
									<tr>
   										<td>
											<c:out value="${product.description}" />
										</td>
										<td>
											<c:out value="${product.quantity}" />
										</td>
										<td>
										<c:out value="${product.price}" />
										</td>
  									</tr>
  									<%count++; %>
									</c:forEach>
							
						</table>
					<br/>
					<div class="total">Total Amount Due: <c:out value="${order.grandTotal}" /></div>
				<br/>
					<h3 class="green">Messages &amp; Instructions</h3>
					<hr/>
					<div class="innercontent">					
						<div id="additionalinfo">
							<span id="cardmessage">Card Message:</span>
							<p><c:out value="${order.cardMessage}" /></p>
							<br/>
							<span id="specialinstructions">Special Instructions:</span>
							<p><c:out value="${order.specialInstruction}" /></p>
							<br/>
							<%if(!order.getFulfillingShop().getShopCode().equals("Z9990000")){ %><a href="?view=backorder&timestamp=<%=today %>">Edit Card Message or Special Instructions</a><br/>
							<%}else{ %>
								<a href="?view=backDropship&timestamp=<%=today %>">Edit Card Message or Special Instructions</a><br/>
							<%} %>				
						</div>
					</div>
					<br/>
					
					<a href="#" id="sendorder" class="left" onclick="submitOrderOnly()"><img src="images/sendorder.png"/></a>
					<a href="#" id="closebutton" class="left" onclick="window.location.href = 'contemporary.htm?view=close&timestamp=<%=today %>';"><img src="images/closebutton.png" /></a>
					<div class="clear"></div>
					</form>
			</div>
				
			<jsp:include page="common/fillerads.jsp" />
			<div class="clear"></div>
		</div>
	</div>
	<jsp:include page="common/footerContemp.jsp" />
</body>
</html>