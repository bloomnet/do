<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ page import="java.util.List" %>
<%@ page import="org.apache.lucene.document.Document" %>
<%@ page import="com.flowers.server.entity.Facility" %>
<%@ page import="com.flowers.portal.vo.Order" %>
<%@ page import="com.flowers.portal.vo.DescribedProduct" %>
<%@ page import="java.io.FileInputStream" %>
<%@ page import="java.util.Properties" %>
<%@ page import="java.util.Date" %>
<%@ page session="false" %>

<%
	Order order = (Order)request.getAttribute("order");
	String uname = (String)request.getSession(true).getAttribute("uname");
	String captcha = (String)request.getSession(true).getAttribute("captcha");
	if (order != null){
	String instructions  = order.getSpecialInstruction();
	}
	Facility facility = (Facility)request.getAttribute("facility");
	List<Document> categories = (List<Document>)request.getAttribute("categories");
	long today = new Date().getTime();
	%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>Bloomnet - Directory Online</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<link href="styles/jquery-ui-1.9.0.custom.min.css?timestamp=<%=today %>" rel="stylesheet" />	
	<link rel="stylesheet" type="text/css" href="styles/reset.css?timestamp=<%=today %>" media="screen"/>
	<jsp:include page="common/mainCss.jsp" />
<!--[if IE 7]>
        <link rel="stylesheet" type="text/css" href="styles/ie7.css" />
<![endif]-->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-XG7CP6XY9J"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
  		function gtag(){dataLayer.push(arguments);}
  		gtag('js', new Date());
  		gtag('config', 'G-XG7CP6XY9J');
	</script>

	<script type="text/javascript" src="scripts/jquery-1.8.2.min.js?timestamp=<%=today %>"></script>
	<script type="text/javascript" src="scripts/jquery-ui-1.9.0.custom.min.js?timestamp=<%=today %>"></script>	
	<script type="text/javascript" src="scripts/jquery.jcarousel.min.js?timestamp=<%=today %>"></script>
	<jsp:include page="common/bloomnetJs.jsp" />
	
	<!-- Hotjar Tracking Code for directory.bloomnet.net -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:1037407,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>
	
</head>
<body id="form">
	<div id="container">
		<div id="header">
			<div class="left">
				<a id="logo"><img src="images/logo.png" alt="Bloomnet Logo" title="Bloomnet Logo"/></a>
			</div>
			<div class="right">
				<a id="signout" href="<%= request.getContextPath() %>/signout.htm?timestamp=<%=today %>">Sign Out</a>
				<br/>
				<div class="text">					
					<span id="questions">Questions? Call 800-256-6663</span>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<%List<String> bannerAds = (List<String>)request.getAttribute("bannerAds"); 
		if(bannerAds != null && bannerAds.size() > 1){
		%>
			<div id="bannerAds" style="width: 930px; height: 150px; margin-bottom:-20px; overflow: hidden;" class="jcarousel-wrapper">
	                <div data-jcarousel="true">
	                    <ul class="jcarousel" style="left: 0px; top: 0px;">
	                    <% 
	                    int limit = 0;
	                    if(bannerAds.size() >= 10)
	                    	limit = 10;
	                    else
	                    	limit = bannerAds.size();
	                    for(int ii=0; ii<limit; ++ii){
	                    	String bannerAd = bannerAds.get(ii).split("--")[0];
	                    	String website = bannerAds.get(ii).split("--")[1];
	                    	if(website != null && !website.equals("") && !website.equalsIgnoreCase("null")){
	                    	%>
	                    		<li><a href="<%=website %>"><img width="930px" height="150px" alt="" src="https://directory.bloomnet.net/bloomnet-images/ads/<%=bannerAd%>-BANNER.jpg" /></a></li>
	                    	<%}else{ %>
	                    		<li><img width="930px" height="150px" alt="" src="https://directory.bloomnet.net/bloomnet-images/ads/<%=bannerAd%>-BANNER.jpg" /></li>
	                    <%	  }
	                    } %>
	                    </ul>
	                </div>
	            
	            </div>
	    <%}else if(bannerAds != null && bannerAds.size() == 1){
	    	
	    	String bannerAd = bannerAds.get(0).split("--")[0];
	        String website = bannerAds.get(0).split("--")[1];
	        if(website != null && !website.equals("") && !website.equalsIgnoreCase("null")){
	         %>
	    	<div id="bannerAds" style="width: 930px; height: 150px; margin-bottom:-20px; overflow: hidden;" >
	                    <ul style="left: 0px; top: 0px;">
	                    	<li><a href="<%=website %>"><img width="930px" height="150px" alt="" src="https://directory.bloomnet.net/bloomnet-images/ads/<%=bannerAd%>-BANNER.jpg" /></a></li>
	                    </ul>
	            </div>
	    	<%}else{ %>
	             <div id="bannerAds" style="width: 930px; height: 150px; margin-bottom:-20px; overflow: hidden;" >
	                    <ul style="left: 0px; top: 0px;">
	                    	<li><img width="930px" height="150px" alt="" src="https://directory.bloomnet.net/bloomnet-images/ads/<%=bannerAd%>-BANNER.jpg" /></li>
	                    </ul>
	            </div>
	        <%}
	       } %>
		
				
		<div id="content">
				<div id="accordion">
					<!--First Accordion-->
					<h2>Search for a Florist</h2>
					<div id="mainSearchDiv" style="overflow: hidden;">
						<h4 class="green"><span class="red"></span>Please Select a Search Type to Continue:</h4> <br />
						<div onclick="showSearch()" style="position: relative; top: -158px; left: -200px; height: 100px;"><div id="residentialSearchTab" class="clickable" title="Residential/Business"><br /><br /><br /><br /><span style="margin-left:-20px;">Residential/Business</span></div> <div id="funeralHomeSearchTab" class="clickable" title="Funeral Home Search"><br /><br /><br /><br /><span>Funeral&nbsp;Homes</span></div><div id="nursingHomeSearchTab" class="clickable" title="Nusring Home Search"><br /><br /><br /><br /><span>Nursing&nbsp;Homes</span></div><div id="hospitalSearchTab" class="clickable" title="Hospital Search"><br /><br /><br /><br /><span style="margin-left:14px;">Hospitals</span></div><div id="advancedSearchTab" class="clickable" title="Advanced"><br /><br /><br /><br /><span style="margin-left:5px;">Advanced</span></div><div id="internationalSearchTab" class="clickable" title="International"><br /><br /><br /><br /><span style="margin-left:-1px;">International</span></div></div>
						<div id="step-1a" style="display: none; position:relative; top: -90px; margin-bottom:-90px; overflow: hidden;">
							<div class="left" id="leftside" style="display:none;">
								<h3 id="residential">Residential/Business Search</h3>
								<br></br>
								<input type="hidden" id="from" name="from" value="search" tabindex="1"/>
								<input type="hidden" id="pref" name="pref" value="${pref}" />
								<div id="form1aArea">
									<form id="form1a" style="margin-top: -20px;" >
								</div>
									<div class="styled-select country" id="countrySelectDiv">
										<select name="searchSelCountry"  id="searchSelCountry" onchange="changeCountry()">
												<c:out value="${countries}" escapeXml="false"/>
										</select>
									</div>
									<br />
									<input type="text" name="searchZip" id="searchZip" title="Zip / Postal Code" autocomplete=off  onkeyup="examineZip()" value="<c:out value="${order.recipient.postalCode}" />"/>
									<div title="To narrow down your search results try using the zip code search"><img src="images/smallor.png" style="width: 30px; margin-top: 5px; margin-left: 115px;" id="searchOr" /></div> 
									<input type="hidden" id="searchAdditionalCities" name="searchAdditionalCities" value=" "  />
									<div class="styled-select stateorprovinceselect" id="searchstateorprovinceselect">
										<select name="searchSelState"  id="searchSelState" onchange="selectState()">
										<option selected="selected">State/Province</option>
												<c:out value="${states}" escapeXml="false"/>
										</select>
									</div>
									<div class="clear"></div>
									<div class="styled-select cityselect" id="searchCitySelect">
										<select name="searchCityAc" class="clickable" id="searchCityAc" title="City">
											<option selected="selected">City</option>
										</select>
									</div>
									<br/>
									<h3>Delivery Date</h3>
									<div><input type="text" name=searchDeliveryDate  <% if(order != null && order.getDeliveryDate() != null){ %>value="<c:out value="${order.deliveryDate}" />" <%}else{ %>value="${currentDate}"<%} %> class="deliverydate" id="searchDeliveryDate"   /><img src="images/minicalendar.png" class="minicalendar clickable" /></div>
									<br></br>
									<h3>Product Type</h3>
									<div class="styled-select occasionselect">
										<select class="clickable" id="productId" name="productId"  title="Product Type" >
											<option value="1" <%if(order == null || order.getProductId() == null || order.getProductId().equals("1")){ %>selected="selected"<%} %>>Floral</option>
											<option value="1144" <% if(order != null && order.getProductId() != null && order.getProductId().equals("1144")){ %> selected="selected" <%} %>>Fruit Bouquet</option>
										</select>
			
									</div>
									
									<div id="errorMessageSearch" style="color: red; width: 300px;"></div>
									<input type="button" name="submit" class="searchforafloristFac" id="searchforaflorist" value="" onclick="searchflorist()" />
									</form>
							</div>

							<div class="left" id="rightside" style="display: none;">
								<h3 id="facility">Facility Search</h3>
								<br />
								<form id="form1b">
							  <input type="hidden" id="from" name="from" value="facility" tabindex="1"/>
							  <input type="hidden" id="from2" name="from2" value="facility" />
							  <input type="hidden" id="selectedFacilityId" name="selectedFacilityId" value='<c:out value="${order.facilityId}" />' tabindex="1" />
							  
							  <br />

									<div class="left">
												<input type="text" name="zipcode" id="zipcode" onkeyup="examineFacZip()" title="Zip / Postal Code" value="<c:out value="${order.recipient.postalCode}" />"></input>
									</div>
									
									<div title="To narrow down your search results try using the zip code search"><img src="images/smallor.png" style="width: 30px; margin-top: 5px; margin-left: 115px;" id="searchOr" /></div> 

									<div class="styled-select facilitiesselectfacilitiesselect">
													<select class="clickable" name="searchSelStateFac" id="searchSelStateFac" onchange="selectFacState()">
														<option disabled="disabled" selected="selected" value="">State/Province</option>
															<c:out value="${states}" escapeXml="false"/>
													</select>
									</div>
									<div class="styled-select cityselect">
										<select class="clickable" name="searchCity" id="searchCity">
											<option selected="selected" value="">City</option>	
										</select>
									</div>

							<div class="styled-select facilityselect" style="display: none;">
								<select class="clickable" name="facilityDD" id="facilityDD">
									<option value="">Facility Type</option>
									<option value="4">FUNERAL HOME</option>
									<option value="8">Nursing Home</option>
									<option value="9">Hospital</option>

								</select>
							</div>
							<div class="styled-select facilitiesselect" id="facilityNameDD">
								<select class="clickable" name="facilitiesDD" id="facilitiesDD">
									<option selected="selected">Facilities</option>
								</select>
							</div>


									<h3>Delivery Date</h3>
									<input type="text" id="deliveryDate2" name="deliverydate2" class="deliverydate clickable" <% if(order != null && order.getDeliveryDate() != null){ %>value="<c:out value="${order.deliveryDate}" />" <%}else{ %>value="${currentDate}"<%} %> /><img src="images/minicalendar.png" class="minicalendar clickable" />
									<br></br>
									<div id="errorMessageSearchFac" style="color: red; width: 580px; margin-left: -170px; text-align: center;"></div>
									<div class="clear"></div>
									<input type="button" name="facSubmit" class="searchforaflorist" value="" onclick="searchfacility()" style="margin-top: 30px;" />
								</form>
							</div>
							
			<div class="left" id="advanced-search-layout" style="display: none;">
				<h3 id="advanced">Advanced Search</h3>
				<br></br>
                <div class="advancedFormat">
	                <form action="#">
						<input type="text" name="code" id="code" title="Shop Code"/>
						<div style="padding:2.5px;"></div>
	                	<input type="text"  style="width: 165px" name="phone1" id="phone1" title="Phone number" onkeypress="return onlyPhoneNumbers();"/>
	                	<div style="padding:4px;"></div>
	                	<input type="text" name="shop_name" id="shop_name" title="Shop Name" /><br /><br />
	                	<div style="padding:1px;"></div>
	                	<h3>Delivery Date</h3>
	                	<input type="text" name=advsearchDeliveryDate class="deliverydate"  <% if(order != null && order.getDeliveryDate() != null){ %>value="<c:out value="${order.deliveryDate}" />" <%}else{ %>value="${currentDate}"<%} %> class="deliverydate" id="advsearchDeliveryDate"   /><img src="images/minicalendar.png" class="minicalendar clickable" style="float:none;" /> 
	                <br />
	                <div id="advSearchError" style="color:red; margin-top:5px;"></div>
	                <input type="button" name="advsubmit" class="searchforaflorist" id="searchflorist" value="" onclick="advancedSearch()" />&nbsp;&nbsp;
	                </form>
                </div>
			</div>	

						</div>
						<%
						FileInputStream fos;
						Properties props = new Properties();
						try {
							fos = new FileInputStream("/opt/apps/properties/directory.properties");
							props.load(fos);
						} catch (Exception e) {
							e.printStackTrace();
						}
						if(props.getProperty("bulk").equals("all") || props.getProperty("bulk").contains(uname)){ 
						%>
						<div>
							<br /> <br />
							<form method="post" action="uploadFile.htm" enctype="multipart/form-data">
								<input type="file" name="file" /><br /> 
								<input type="submit" value="Process Bulk Orders CSV" />
							</form>	
						</div>
						<%} %>
					</div>
                    <!--Second Accordion-->

					<h2 id="mainOrderH2" style="margin-top:0px; display: none;">
                    	
					<div class="enteranordercompleted">
						<a href="#">Enter an Order</a> 
					</div>
				</h2>
				<div id="mainOrderDiv">
				   <jsp:include page="common/ordr.jsp" />
				
				</div>
                    <!-- end of Second Accordion -->  
                <!--Third Accordion-->
				<h2 id="goDropship" style="margin-top:0px; display: none;">
					<div class="dropshiporder">
						<img src="images/smallor.png" class="or" /> 
						<a href="#">Enter a Dropship Order</a> 
					</div>
				</h2>
              <!-- end of Third Accordion -->
									
			</div>

			<div id="choose-search-layout" title="Choose Search Layout" class="dialog">
				<input type="radio" name="pageview" value="traditional" >Traditional Layout</input><br/><br/>
				<input type="radio" name="pageview" value="contemporary" checked>Contemporary Layout </input><span class="green"><strong><em>NEW!</em></strong></span><br/><br/>
				<input type="checkbox" name="rememberselection" value="true" >Remember my selection</input>
			</div>
				
			

			<jsp:include page="common/landingSideAds.jsp" />

			<div class="clear"></div>
		</div>
		
	</div>
	<jsp:include page="common/footerContemp.jsp" />
</body>
</html>