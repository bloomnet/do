<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@page import="com.flowers.portal.vo.Error" %>
<%@page import="com.flowers.portal.vo.RestResponse" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.flowers.portal.vo.Order" %>
<%@ page session="false" %>
<%@ page import="java.util.Date" %>
<%
RestResponse restResponse = (RestResponse)request.getAttribute("response");
long bmtOrderNumber = 0;
List<Error> errors = restResponse.getErrors();
if (restResponse!=null){
	 bmtOrderNumber = restResponse.getBmtOrderNumber();
}
if(bmtOrderNumber == 0 && errors.size() == 0){
	Error unexpectedError = new Error();
	unexpectedError.setErrorMessage("An unexpected error has occured. Please click on the sign out button on the top right and re-sign in and try again. If this problem persists, pelase contact BloomNet Customer Service at 1-866-256-6663.");
	errors.add(unexpectedError);
}
long today = new Date().getTime();
 %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>Bloomnet - Directory Online</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<link href="styles/jquery-ui-1.9.0.custom.min.css?timestamp=<%=today %>" rel="stylesheet"/>	
	<link rel="stylesheet" type="text/css" href="styles/reset.css?timestamp=<%=today %>" media="screen"/>
	<jsp:include page="common/mainCss.jsp" />

	<script async src="https://www.googletagmanager.com/gtag/js?id=G-XG7CP6XY9J"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
  		function gtag(){dataLayer.push(arguments);}
  		gtag('js', new Date());
  		gtag('config', 'G-XG7CP6XY9J');
	</script>

	<script type="text/javascript" src="scripts/jquery-1.8.2.min.js?timestamp=<%=today %>"></script>
	<script type="text/javascript" src="scripts/jquery-ui-1.9.0.custom.min.js?timestamp=<%=today %>"></script>	
	<jsp:include page="common/orderSummaryJs.jsp" />
	
		
	<!-- Hotjar Tracking Code for directory.bloomnet.net -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:1037407,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>
	
</head>
<body id="confirmation">
	<div id="container">
		
		<jsp:include page="common/head.jsp" />
				
		<div id="content">
			<div id="contentblock">
				<%
					if (errors.size() == 0) {
				%>
				<h2 id="confirmation2">Your Order Has Been Sent!</h2>
				<div class="ordernumber" id="orderNumber">
					<strong>Order Number:</strong>
					<%=bmtOrderNumber%>
				</div>
				<%
					} else {
				%>
				<h2 id="confirmation2">No order submitted</h2>
				<div class="ordernumber">
					<%
						for (Error error : errors) {
								String message = error.getErrorMessage();
					%>
					<%=message%>
					<%
						}
					%>
				</div>
				<%
					}
				%>

				<h3 class="green">Recipient Information</h3>
				<hr />
				<% Order order = (Order)request.getAttribute("order"); %>
				<div class="innercontent">
					<div id="customerinfo" class="left">
						<h4>Deliver to:</h4>
						<%if(order.getFacilityName() != null){ %><span id="cFacName"> <c:out value="${order.facilityName}" /> </span> <br /> <%} %>
						<span id="cname"><c:out value="${order.recipient.firstName}" /> <c:out value="${order.recipient.lastName}" /></span><br/>
						<span id="caddress1"><c:out value="${order.recipient.address1}" /></span><br/>
						<%if(order.getRecipient().getAddress2() != null){ %><span><c:out value="${order.recipient.address2}" /></span><br/><%} %>
						<%if(order.getRecipient().getCountryId() != null && !order.getRecipient().getCountryId().equals("1") && !order.getRecipient().getCountryId().equals("2") && !order.getRecipient().getCountryId().equals("3")){ %>
							<span id="caddress2"><c:out value="${order.recipient.city}" />, <c:out value="${order.recipient.countryCode}" /></span><br/>
						<%}else{ %>
							<span id="caddress2"><c:out value="${order.recipient.city}" />, <c:out value="${order.recipient.state}" /> <c:out value="${order.recipient.postalCode}" /></span><br/>
						<%} %>
						<span id="cphone1"><c:out value="${order.recipient.phoneNumber}" /></span><br/>
					</div>
					<div id="floristinfo" class="right">
						<h4>Delivery Date: <c:out value="${order.deliveryDate}" /></h4>
						<h4>Filling Florist Information:</h4>
						<h4 id="fname"><c:out value="${order.fulfillingShop.shopName}" /> &nbsp;(<c:out value="${order.fulfillingShop.shopCode}" />)</h4>
						<c:if test="${order.fulfillingShop.address != null}">
							<c:out value="${order.fulfillingShop.address.streetAddress1}" /> <br />
							<%if(order.getRecipient().getCountryId() != null && !order.getRecipient().getCountryId().equals("1") && !order.getRecipient().getCountryId().equals("2") && !order.getRecipient().getCountryId().equals("3")){ %>
								<span id="faddress2"><c:out value="${order.fulfillingShop.address.postalCode}" /></span><br/>
							<%}else{ %>
								<span id="faddress2"><c:out value="${order.fulfillingShop.address.city.name}" />, <c:out value="${order.fulfillingShop.address.city.state.shortName}" />, <c:out value="${order.fulfillingShop.address.postalCode}" /></span><br/>
							<%} %>
						</c:if>
						<c:if test="${order.fulfillingShop.tollFreeNumber != null}">
						<span id="fphone1"><c:out value="${order.fulfillingShop.tollFreeNumber}" /></span><br/>
						</c:if>
						<span id="fphone2"><c:out value="${order.fulfillingShop.telephoneNumber}" /></span><br/>
						<span id="femployeename"><c:out value="${order.fulfillingShop.contactPerson}" /></span><br/>
					</div>
					<div class="clear"></div>
				</div>
				<br /> <br />
				<h3 class="green">Products</h3>
				<hr />
				<table id="products">
							<th id="productname">Product Name</th>
							<th id="quantity">Quantity</th>
							<th id="price">Price</th>
							
							<%int count = 0;%>
									<c:forEach items="${order.describedProducts}" var="product" varStatus="loopStatus">
									<%
										String countValue = String.valueOf(count);	
									%>
									<tr>
   										<td>
											<div id="prodDescription<%=countValue%>"><c:out value="${product.description}" /></div>
										</td>
										<td>
											<div id="prodQuantity<%=countValue%>"><c:out value="${product.quantity}" /></div>
											<div id="prodCode<%=countValue%>" style="display: none;"><c:out value="${product.code}" /></div>
										</td>
										<td>
											<div id="prodPrice<%=countValue%>"><c:out value="${product.price}" /></div>
										</td>
  									</tr>
  									<%count++; %>
									</c:forEach>
							
						</table>
				<br />
					<div class="total">Total Amount Due: <c:out value="${order.grandTotal}" /></div>
				<br />
				<h3 class="green">Messages &amp; Instructions</h3>
				<hr />
				<div class="innercontent">
					<div id="additionalinfo">
							<span id="cardmessage">Card Message:</span>
							<p><c:out value="${order.cardMessage}" /></p>
							<br/>
							<span id="specialinstructions">Special Instructions:</span>
							<p><c:out value="${order.specialInstruction}" /></p>
							<br/>
						</div>
				</div>
				<div id="buttons">
					<br /> <a href="#" id="printconfirmation" class="left" onclick="printWindow();"><img
						src="images/printconfirmation.png" />
					</a> <a href="#" id="closebutton" class="left" onclick="window.location.href = 'contemporary.htm?view=close&timestamp=<%=today %>';"><img
						src="images/closebutton.png" />
					</a>
				</div>
				<div class="clear"></div>
			</div>

			<jsp:include page="common/fillerads.jsp" />
			<div class="clear"></div>
		</div>
	</div>
	<jsp:include page="common/footerContemp.jsp" />
	<%
		if (errors.size() == 0) {
	%>
			<div id="fulfillingShopCode" style="display: none;"><c:out value="${order.fulfillingShop.shopCode}" /></div>
			<div id="dptPrice" style="display: none;"><c:out value="${order.grandTotal}" /></div>
			<div id="floristCityStateZip" style="display: none;"><c:out value="${order.fulfillingShop.address.city.name}" />,<c:out value="${order.fulfillingShop.address.city.state.shortName}" />,<c:out value="${order.fulfillingShop.address.postalCode}" /></div>
			<div id="productCount" style="display: none;"><%=count%></div>
			<div id="orderNumber" style="display: none;"><%=bmtOrderNumber%></div>
			<script type="text/javascript">
					function replaceAll(txt, replace, with_this) {  
						return txt.replace(new RegExp(replace, 'g'),with_this);

					}
					var oMsg = document.getElementById("orderNumber").innerHTML.toLowerCase().split("</strong>")[1].trim();
					var productArray = new Array("BABY HANGING ELEPHANT VASE","BEER MUG OF BLOOMS","BIRTHDAY FLOWER CAKE","CANVAS PRINTED BAG","CELEBRATING MOM BOOK",
						"CRAZY FOR YOU BALLOON BOUQUET","CUPCAKE IN BLOOM","EVERYDAY PLUSH TEDDY BEAR","GREEN TIN PLANTER","HARRY LONDON CHOCOLATES","IT'S YOUR SUNNY DAY","LENOX� MEADOW CRYSTAL VASE",
						"LIME MARGARITA BOUQUET","LOVE MONKEY WITH CANDY","MAKE LEMONADE","MINI MARGARITA BOUQUET","PRESENT BOX","PURPLE PASSION GLASS VASE",
						"RECTANGULAR GLASS VASE","RED RIBBED VASE","STRAWBERRY FLORAL MARGARITA","WHITE CERAMIC PITCHER");
	
					var oFulfillingShopCode = document.getElementById("fulfillingShopCode").innerHTML;
					var oTotalPrice = document.getElementById("dptPrice").innerHTML;
					oTotalPrice = oTotalPrice.substring(1,oTotalPrice.length);
					oTotalPrice = replaceAll(oTotalPrice,",","");
					var oCityStateZip = document.getElementById("floristCityStateZip").innerHTML.split(",");
					var oRecipientCity = oCityStateZip[0];
					var oRecipientState = oCityStateZip[1];
					var oRecipientZip = oCityStateZip[2];
					var oRecipientCountry = "";
					var count = document.getElementById("productCount").innerHTML;
					if(oRecipientZip != null && oRecipientZip !== "") oRecipientCountry = "USA";
					else oRecipientCountry = "International";	

					var products = [];
    				for(var ii=0; ii<count; ++ii){
    					var oProductDescription = document.getElementById("prodDescription"+ii).innerHTML.toUpperCase();
    					var oProductCode = document.getElementById("prodCode"+ii).innerHTML;
    					var oProductPrice = document.getElementById("prodPrice"+ii).innerHTML;
    					oProductPrice = oProductPrice.substring(1,oProductPrice.length);
    					oProductPrice = replaceAll(oProductPrice,",","");
    					var oProductQuantity = document.getElementById("prodQuantity"+ii).innerHTML;
    					if(oProductCode != null && oProductCode != '' && contains(oProductDescription,productArray)){
    						oProductDescription = document.getElementById("prodDescription"+ii).innerHTML;
    					}else{
    						oProductCode = '000000';
    						oProductDescription = "Designer's Choice";
    					}

    					var obj = {};
    				    obj.item_id = oProductCode;
    				    obj.item_name= oProductDescription;
    				    obj.affiliation= 'Directory Online';
    				    obj.item_brand= 'BloomNet'; 
    				    obj.item_category= 'BloomNet Services'; 
    				    obj.price= parseFloat(oProductPrice); 
    				    obj.quantity= parseInt(oProductQuantity); 
    				    products.push(obj);
    				} 
    				
    				gtag("event", "purchase", {
    			        transaction_id: oMsg,
    			        value: parseFloat(oTotalPrice),
    			        currency: "USD",
    			        items: products
    			    });
    				
	</script>
	<%
		}
	%>
</body>
</html>