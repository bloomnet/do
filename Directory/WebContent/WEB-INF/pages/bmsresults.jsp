<%@page import="com.flowers.server.entity.SearchQuery"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Random" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="org.hibernate.mapping.Collection" %>
<%@ page import="java.lang.StringBuffer" %>
<%@ page import="org.apache.lucene.document.Document" %>
<%@ page import="org.apache.lucene.document.Field" %>
<%@ page import="com.flowers.server.search.SearchResultSet" %>
<%@ page import="com.flowers.portal.service.impl.ShopAvailabilityServiceImpl" %>
<%@ page import="com.flowers.portal.util.DateUtil" %>
<%@ page import="com.flowers.server.entity.User" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.io.File" %>
<%@ page import="java.io.FileFilter" %>
<%@page import="org.apache.commons.io.filefilter.WildcardFileFilter" %>
<%@ page import="com.flowers.portal.vo.Order" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Comparator" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.net.URL" %>
<%@ page import="java.net.HttpURLConnection" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>Bloomnet - Directory Online</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<%long today = new Date().getTime();%>
	<link href="styles/jquery-ui-1.9.0.custom.min.css?timestamp=<%=today %>" rel="stylesheet" />	
	<link rel="stylesheet" type="text/css" href="styles/reset.css?timestamp=<%=today %>" media="screen"/>
	<jsp:include page="common/mainCss.jsp" />
	
<!--[if IE 7]>
        <link rel="stylesheet" type="text/css" href="styles/ie7.css" />
<![endif]-->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-XG7CP6XY9J"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
  		function gtag(){dataLayer.push(arguments);}
  		gtag('js', new Date());
  		gtag('config', 'G-XG7CP6XY9J');
	</script>

	<script type="text/javascript" src="scripts/jquery-1.8.2.min.js?timestamp=<%=today %>"></script>
	<script type="text/javascript" src="scripts/jquery-ui-1.9.0.custom.min.js?timestamp=<%=today %>"></script>	
	<script type="text/javascript" src="scripts/jquery.jcarousel.min.js?timestamp=<%=today %>"></script>	
	<jsp:include page="common/bmsResultsJs.jsp" />
	<jsp:include page="common/bloomnetJs.jsp" />
	<script type="text/javascript" src="scripts/jquery.raty.min.js?timestamp=<%=today %>"></script>

	
	<!-- Hotjar Tracking Code for directory.bloomnet.net -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:1037407,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>

</head>
<%String existingCoverageNoZips = (String) request.getSession(true).getAttribute("existingCoverageNoZips");
String noSpecificResultsString = (String) request.getSession(true).getAttribute("noSpecificResults");
 %>
<body id="results">
	<div id="container">
		<div id="header">
			<div class="left">
				<a id="logo"><img src="images/logo.png" alt="Bloomnet Logo" title="Bloomnet Logo"/></a>
			</div>
			<div class="right">
				<br/>
				<div class="text">					
					<span id="questions">Questions? Call 800-256-6663</span>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		
		<div id="bannerAdsPlaceholder" style="width: 930px; height: 150px; margin-bottom:-20px; overflow: hidden; display: none;"> </div>
					
			<div id="content">
	
				<div id="accordion">
				<h2>Search Results</h2>
<%
boolean odds = true;
if((new Random().nextInt(2) + 1) == 2)
	odds = false;
List<String> sideAds = new ArrayList<String>();
List<String> topAdsIds = new ArrayList<String>();
List<String> topAds = new ArrayList<String>();
Order order = (Order)request.getAttribute("order");
String selectedShopCode = "";
if(order != null && order.getFulfillingShop() != null){
	selectedShopCode = order.getFulfillingShop().getShopCode();
}
SearchResultSet<Document> searchResults = (SearchResultSet<Document>)request.getAttribute("searchResults");
if(searchResults == null)  searchResults = (SearchResultSet<Document>)request.getSession(true).getAttribute("searchResults");

if(searchResults != null && searchResults.getResultSize() > 0){		
	boolean hasResults = false;
	boolean hasNext = false;
	boolean hasPrev = false;
	int totalResults = 0;
	int totalPages = 0;
	int pageNumber = 0;
	List<Document> currPage = null;
	boolean lastPage = false;
	String message = "";
	String searchQueryDate = "";
	List<String> qualityCareFlorists = new ArrayList<String>();
	String[] qualityFlorists = new String[]{"H7620000","E2290000","B2260000","E4400000",
			"W6710000","J2900000","F8960000","D8330000","S4570000","F2420000","F3650000",
			"B1150000","M3370000","B6130000","D1770000","D6510000","D2460000","L0990000",
			"B2520000","G5590000","B7040000","N0800000","W4570000","G4300000","P5850000",
			"H8100000","H3010000","Y6220000","D2720000","S4250000","K9230000","G8000000",
			"Y2150000","B3920000","J4290000","B7510000","G1550000","K2100000","D9640000",
			"P2860000","X5800000","H7920000","P5810000","N1360000","H1400000","T0590000",
			"G8760000","J7660000","B5340000","H7080000","D4310000","B1580000","D9250000",
			"H3030000","S1000000","G3610000","K3750000","J6920000","Y9120000","R8750000",
			"P5610000","B3370000","B3510000","U5970000"};
	for(int ii=0; ii<qualityFlorists.length; ++ii){
		qualityCareFlorists.add(qualityFlorists[ii]);
	}
	List<String> fruitBouquetFlorists = new ArrayList<String>();
	String[] fruitFlorists = new String[]{"70700000","X7800000","R3370000","23000000","51900000",
			"R5610000","53400000","37300000","71500000","37500000","Y3260000","G7590000",
			"70600000","73600000","25100000","50800000","37700000","37600000","20800000",
			"74900000","70800000","E5450000","E5460000","50100000","S6590000","70200000",
			"31600000","X1740000","46200000","J0450000","H4770000","H1040000","H1800000",
			"H4790000","H1850000","H1700000","H1830000","H1090000","J6480000","H1310000",
			"G5290000","M5230000"};
	for(int ii=0; ii<fruitFlorists.length; ++ii){
		fruitBouquetFlorists.add(fruitFlorists[ii]);
	}
	
	if(searchResults != null){
		hasResults = searchResults.getResultSize() > 0;
		hasNext = searchResults.getPageNumber()+1 < searchResults.getPageCount();
		hasPrev = searchResults.getPageNumber()+1 > 1;
		totalResults = searchResults.getResultSize();
		totalPages = searchResults.getPageCount();
		pageNumber = searchResults.getPageNumber();
		currPage = (searchResults.getPages().size() > 0) ? searchResults.getPages().get(pageNumber) : null;
		lastPage = searchResults.getPageCount() == pageNumber+1;
		message = searchResults.getMessage();
		searchQueryDate = searchResults.getSearchQuery().getDate();
	}
	boolean paidAds = (currPage != null) ? currPage.get(0).get("ad_size") != null : false;
	Map<String, Document> productsMap = (Map<String, Document>)request.getAttribute("productsMap");
	Map<String, Document> catagoriesMap = (Map<String, Document>)request.getAttribute("catagoriesMap");
	StringBuffer buff = new StringBuffer();
	if(searchResults == null || !hasResults) {
		buff.append("empty");
	} else {
		if(hasResults) buff.append("hasResults");
		if(hasNext) buff.append(" hasNext");
		if(hasPrev) buff.append(" hasPrev");
		if(paidAds) buff.append(" paidAds");
		if(!paidAds) buff.append(" listings");
	}
	
	%>
				<div id="mainResultsDiv">
				<input id="callback" style="display:none" type="text" value="<%= request.getSession(true).getAttribute("callback") %>" />
				<%if(existingCoverageNoZips.equals("YES") && (noSpecificResultsString == null || noSpecificResultsString.length() <= 0)){ %>					
						<div id="messagearea"> 
							<p><strong>No Florist Was Found &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></p><br/>
							<br />
							<p>Unfortunately there are no BloomNet professional florists that met your exact zip code search. Please make one of the two following choices.</p><br/>
							<form name="search-form" id="search-form" method="post" action="?view=summary&commitment=yes&timestamp=<%=today %>"  >
								<input type="hidden" id="emptyOrder" name="emptyOrder" value="${emptyOrder}">
								<div class="clickable" onclick="showCityListings()"><img src="images/btn-view-other-florists.png"/></div>
								<div class="clickable" onclick="$('#search-form').submit();"><img src="images/sendorderviacommitmenttocoverage.png"/></div>
								</form>
							<br/>
						</div>
					<div id="searchcontentblock" style="display:none;">
				<%}else{ %>
					<div id="searchcontentblock">
				<%} %>
					   <form name="search-form" id="search-form" method="post" action="?view=summary&timestamp=<%=today %>"  >
					   		<input type="hidden" id="reviewedShop" name="reviewedShop" value=""></input>
					   		<input type="hidden" id="reviewedName" name="reviewedName" value=""></input>
							<input type="hidden" id="viewDirective" name="commitment" value=""></input>
							<input type="hidden" id="from" name="from" value="search" tabindex="1"></input>
							<input type="hidden" id="selectedListing" name="selectedListing" tabindex="1"></input>
							<input id="facilityId" type="hidden" tabindex="1" value="" name="facilityId"></input>
							<input id="from-order" type="hidden" tabindex="1" value="no" name="from-order"></input>
							<%String searchArea = ""; 
							if(searchResults.getSearchQuery().getZip() != null) searchArea = searchResults.getSearchQuery().getZip().length() > 7 ? searchResults.getSearchQuery().getCity()+","+searchResults.getSearchQuery().getStateCode() : searchResults.getSearchQuery().getZip(); 
							else searchArea = searchResults.getSearchQuery().getCity()+","+searchResults.getSearchQuery().getStateCode();
							%>
							<input type="hidden" id="searchZipCode" value="<%=searchArea%>" />
							<input type="hidden" id="emptyorder" name="emptyOrder" value="${emptyorder}">
					<p id="searchresults">
						<strong>Page <span id="count" class="purple"><%=pageNumber+1 %></span> of <span id="total" class="purple"><%=totalPages %></span> With <span id="results" class="purple"><%=totalResults%></span> Results</strong> | 
					</p>
					<br />
					<div style="color:red"><c:out value="${noSpecificResults}" escapeXml="false"/></div>
					<p><div id="showMap"> <a class="clickable"><img src="images/viewonmap.png" alt="View on Map" title="" id="viewonmap"/></a></div></p>
					<br/>
					<%
					String displayText = "";
					if(order !=null && order.getFacilityName() != null && (noSpecificResultsString == null || noSpecificResultsString.length() <= 0)){
						displayText = order.getFacilityName();
					}else if(order != null && order.getRecipient() != null && order.getRecipient().getCountryId() != null && !order.getRecipient().getCountryId().equals("1") && !order.getRecipient().getCountryId().equals("2") && !order.getRecipient().getCountryId().equals("3")){
						displayText = searchResults.getSearchQuery().getStateCode();
					}else if(searchResults.getSearchQuery().getZip() != null && (existingCoverageNoZips == null || !existingCoverageNoZips.equals("YES")) && order != null && order.getRecipient() != null && order.getRecipient().getCountryId() != null && order.getRecipient().getCountryId().equals("1")){
						displayText = searchResults.getSearchQuery().getCity() + ", " + searchResults.getSearchQuery().getStateCode();
					}else{
						if (searchResults.getSearchQuery().getCity()!=null){
							displayText = searchResults.getSearchQuery().getCity() + ", " + searchResults.getSearchQuery().getStateCode();
						}
					}
					%>		
					<h3 id="listingheading">Professional Florists Serving <%=displayText %></h3>
					
					<%	
									String mapPoints = "";
									LinkedList<String> shopCodes = new LinkedList<String>();
									for(int ii=0; ii<searchResults.getResultSize(); ++ii){
										Document result = searchResults.getResult(ii);
										String listingId = result.get("shopcode") != null ? result.get("shopcode") : "";
										if(listingId != null && listingId != "") shopCodes.add(listingId);
									}
									ShopAvailabilityServiceImpl availability = new ShopAvailabilityServiceImpl();
									LinkedList<String> shopsAvailable = availability.searchShopCodesByAvailabilityDate(DateUtil.formatDateString(searchQueryDate), shopCodes);
									int xx=0;
									for(Document result : searchResults.getResults()){
										String id = result.get("id") != null ? result.get("id") : "";
										String shopCode = result.get("shopcode");
										String addressLine1 = result.get("address1");
										if(addressLine1 != null) addressLine1 = addressLine1.replaceAll("'","&rsquo;");
										String addressLine2 = result.get("address2");
										if(addressLine2 != null) addressLine2 = addressLine2.replaceAll("'","&rsquo;");
										String city = result.get("shopcity");
										if(city != null) city = city.replaceAll("'","&rsquo;");
										String state = result.get("shopstate");
										String zip = result.get("zip");
										String shopName = result.get("shopname");
										if(shopName != null) shopName = shopName.replaceAll("'","&rsquo;");
										String shopPhone = result.get("phone");
										String finalAddress = "";
										if(addressLine2 != null && !addressLine2.equals("")){
											finalAddress = addressLine1+","+addressLine2+","+city+","+state+","+zip;
										}else{
											finalAddress = addressLine1+","+city+","+state+","+zip;
										}
										mapPoints += shopCode+","+shopName+","+shopPhone+"--"+finalAddress+"///";
										
										String listingName = result.get("shopname") != null ? result.get("shopname") : "";
										String listingId = result.get("shopcode") != null ? result.get("shopcode") : "";
										String phone = result.get("phone") != null ? result.get("phone") : "";
										String address1 = result.get("address1") != null ? result.get("address1") : "";
										String address2 = result.get("address2") != null ? result.get("address2") : "";
										String searchedCity = result.get("city") != null ? result.get("city") : "";	
										String moreInfo = "";
										String customListing = result.get("custom_listing");
										if(customListing != null){
											moreInfo = customListing.replaceAll("\"","&quot").replaceAll("'","&rsquo;");			
										}
										String minimumPrices = "";
										String minimums = result.get("minimums");
										if(minimums != null && !minimums.equals("")){
											String[] mins = minimums.split(" ");
											boolean hasMins = false;
											for(int j=0; j < mins.length; j++) {
												if(!mins[j].equals("0.0")) 
													hasMins = true;
											}
											if(hasMins && catagoriesMap != null) {
												for(int j=0; j < catagoriesMap.size(); j++) {
													if(mins[j] != null && !mins[j].equals("0.0")) {
														Document cat = catagoriesMap.get("price"+(j+1));
														String catName = cat != null ? cat.get("name") : "";
														String price = mins[j];
														boolean fluctuate = false;
														if(price != null) {
															if(price.endsWith("H")){
																price = price.substring(0, price.length()-1);
																fluctuate = true;
															}
															if(price.endsWith("0")) price += "0";
															if(fluctuate){
																if(j == catagoriesMap.size() - 1)
																	minimumPrices += catName+" $"+price+"H";
																else
																	minimumPrices += catName+" $"+price+"H,";
															}else{
																if(j == catagoriesMap.size() - 1)
																	minimumPrices += catName+" $"+price;
																else
																	minimumPrices += catName+" $"+price+",";
															}
														}																	
													}
												}
											}
										}
										
										String localProducts = result.get("local_products");
										
										String doMoreInfo = result.get("more_info");
										String sideAdBid = result.get("side_ad_bid");
										
										if(sideAdBid == null || sideAdBid.equals("")){
											sideAdBid = "0";
										}
										
										String safeName = listingName.replaceAll("'","&rsquo;");
										String finalAddress2 = "";
										if(address2 != null && !address2.equals("")){
											finalAddress2 = address1+","+address2+","+city+","+state+","+zip;
										}else{
											finalAddress2 = address1+","+city+","+state+","+zip;
										}
										
										String showMoreInfoString = "\""+shopCode +"\",\""+safeName +"\",\""+address1+"," + city + "," + state + "," + zip +"\",\""+moreInfo +"\",\""+minimumPrices +"\",\""+shopCode+","+safeName+","+phone+"--"+finalAddress2 +"\",\""+localProducts +"\",\""+id +"\"";

										String sideAdEntryNumber = result.get("side_ad_entry_number");
										String bannerStartDate = (result.get("banner_start_date") != null && result.get("banner_start_date").length() > 0) ? result.get("banner_start_date") : "";
										String bannerEndDate = (result.get("banner_end_date") != null && result.get("banner_end_date").length() > 0) ? result.get("banner_end_date") : "";
										boolean validBannerAd = false;
										if(sideAdEntryNumber != null && ! sideAdEntryNumber.equals("") &&
												(bannerStartDate == null || bannerStartDate.equals("") || Long.valueOf(bannerStartDate) <= today) && 
												(bannerEndDate == null || bannerEndDate.equals("") || Long.valueOf(bannerEndDate) >= today))
											validBannerAd = true;
										if(sideAdEntryNumber != null && validBannerAd){
											String tempId = id;
											if(shopsAvailable.get(xx) != null && !shopsAvailable.get(xx).equals("Y"))
												tempId = "0";
											if(sideAdEntryNumber.startsWith("GIF")){
												sideAds.add(shopCode+"-"+sideAdEntryNumber+"-BAN.gif"+"----" + showMoreInfoString +"----"+ String.valueOf(doMoreInfo) +"----"+ tempId + "----" + sideAdBid);
											}else{
												sideAds.add(shopCode+"-"+sideAdEntryNumber+"-BAN.png"+"----" + showMoreInfoString +"----"+ String.valueOf(doMoreInfo) +"----"+ tempId + "----" + sideAdBid);
											}
										}
										
										String topAdEntryNumber = result.get("top_ad_entry_number");
										String topAdStartDate = (result.get("top_ad_start_date") != null && result.get("top_ad_start_date").length() > 0) ? result.get("top_ad_start_date") : "";
										String topAdEndDate = (result.get("top_ad_end_date") != null && result.get("top_ad_end_date").length() > 0) ? result.get("top_ad_end_date") : "";
										boolean validTopAd = false;
										if(topAdEntryNumber != null && ! topAdEntryNumber.equals("") &&
												(topAdStartDate == null || topAdStartDate.equals("") || Long.valueOf(topAdStartDate) <= today) && 
												(topAdEndDate == null || topAdEndDate.equals("") || Long.valueOf(topAdEndDate) >= today)){
											
											URL url;
											if(topAdEntryNumber.startsWith("GIF")){
												url = new URL("https://directory.bloomnet.net/bloomnet-images/ads/"+shopCode+"-"+topAdEntryNumber+"-TOP.gif");
											}else{
												url = new URL("https://directory.bloomnet.net/bloomnet-images/ads/"+shopCode+"-"+topAdEntryNumber+"-TOP.png");
											}
											HttpURLConnection con = (HttpURLConnection) url.openConnection();
											int code = con.getResponseCode();
											if(code != 404)
												validTopAd = true;
										}
										if(topAdEntryNumber != null && validTopAd){
											String tempId = id;
											if(shopsAvailable.get(xx) != null && !shopsAvailable.get(xx).equals("Y"))
												tempId = "0";
											if(topAdEntryNumber.startsWith("GIF")){
												topAds.add(shopCode+"-"+topAdEntryNumber+"-TOP.gif"+"----" + showMoreInfoString +"----"+ doMoreInfo);
											}else{
												topAds.add(shopCode+"-"+topAdEntryNumber+"-TOP.png"+"----" + showMoreInfoString +"----"+ doMoreInfo);
											}
											topAdsIds.add(tempId);
										}
										xx++;
									}
									mapPoints = mapPoints.substring(0, mapPoints.length()-4);
									%>
									<input id="mapPoints" type="hidden" value="<%=mapPoints %>"/>
									<%
									Map<String,Boolean> shopsSeen = new HashMap<String,Boolean>();
									if(currPage != null) {
										for(int i=0; i < currPage.size(); i++) {
											Document result = currPage.get(i);
											String shopCode = result.get("shopcode") != null ? result.get("shopcode") : "";
											if(shopsSeen.get(shopCode) == null){
												shopsSeen.put(shopCode,true);
												String id = result.get("id") != null ? result.get("id") : "";
												String listingName = result.get("shopname") != null ? result.get("shopname") : "";
												String listingId = result.get("shopcode") != null ? result.get("shopcode") : "";
												String phone = result.get("phone") != null ? result.get("phone") : "";
												String phone800 = result.get("phone800") != null ? result.get("phone800") : "";
												String fax = result.get("fax") != null ? result.get("fax") : "";
												String address1 = result.get("address1") != null ? result.get("address1") : "";
												String address2 = result.get("address2") != null ? result.get("address2") : "";
												String city = result.get("shopcity") != null ? result.get("shopcity") : "";
												String searchedCity = result.get("city") != null ? result.get("city") : "";
												String state = result.get("shopstate") != null ? result.get("shopstate") : "";
												String zip = result.get("zip") != null ? result.get("zip") : "";
												if(zip != null) if(zip.length() == 4) zip = "0"+zip;
												String contact = result.get("contact") != null ? result.get("contact") : "";
												String products = result.get("products") != null ? result.get("products") : "";
												String distance = result.get("distance") != null ? result.get("distance") : null;
												String entryCode = result.get("entry_code") != null ? result.get("entry_code") : "";
												String delivery = result.get("delivery_charge") != null ? result.get("delivery_charge") : "";
												String adSize = (result.get("ad_size") != null && result.get("ad_size").length() > 0) ? result.get("ad_size") : "";
												String adStartDate = (result.get("ad_start_date") != null && result.get("ad_start_date").length() > 0) ? result.get("ad_start_date") : "";
												String adEndDate = (result.get("ad_end_date") != null && result.get("ad_end_date").length() > 0) ? result.get("ad_end_date") : "";				
												boolean validAd = false;
												String highlight = result.get("highlight_color");
												String textColor = result.get("text_color");
												String videoColor = result.get("video_color");
												String filler = result.get("filler");
												String customListing = result.get("custom_listing");
												if(customListing != null)
													customListing = customListing.replaceAll("\r","").replaceAll("\n","");
												String facebook = result.get("facebook");
												String twitter = result.get("twitter");
												String linkedin = result.get("linkedin");
												String googleplus = result.get("googleplus");
												String instagram = result.get("instagram");
												String pinterest = result.get("pinterest");
												String score = result.get("score");
												String numReviews = result.get("numReviews");
												String facData = result.get("facdata");
												String localProducts = result.get("local_products");
												String doMoreInfo = result.get("more_info");
												String bid = result.get("bid");
												String sideAdBid = result.get("side_ad_bid");
												String seasonalImages = result.get("seasonal_images");
												String spring = result.get("spring");
												String summer = result.get("summer");
												String fall = result.get("fall");
												String winter = result.get("winter");
												String vday = result.get("vday");
												String mday = result.get("mday");
												
												boolean available = false;
												boolean tlo = false;
												if(adSize != null && ! adSize.equals("") &&
													(adStartDate == null || adStartDate.equals("") || Long.valueOf(adStartDate) <= today) && 
													(adEndDate == null || adEndDate.equals("") || Long.valueOf(adEndDate) >= today))
													validAd = true;
												if(shopsAvailable != null && i< shopsAvailable.size() && shopsAvailable.get(i) != null){
													available = shopsAvailable.get(i).equals("Y");
													tlo = shopsAvailable.get(i).equals("tlo");
												}	
												String minimumPrices = "";
												String minimums = result.get("minimums");
												if(minimums != null && !minimums.equals("")){
													String[] mins = minimums.split(" ");
													boolean hasMins = false;
													for(int j=0; j < mins.length; j++) {
														if(!mins[j].equals("0.0")) 
															hasMins = true;
													}
													if(hasMins && catagoriesMap != null) {
														for(int j=0; j < catagoriesMap.size(); j++) {
															if(mins[j] != null && !mins[j].equals("0.0")) {
																Document cat = catagoriesMap.get("price"+(j+1));
																String catName = cat != null ? cat.get("name") : "";
																String price = mins[j];
																boolean fluctuate = false;
																if(price != null) {
																	if(price.endsWith("H")){
																		price = price.substring(0, price.length()-1);
																		fluctuate = true;
																	}
																	if(price.endsWith("0")) price += "0";
																	if(fluctuate){
																		if(j == catagoriesMap.size() - 1)
																			minimumPrices += catName+" $"+price+"H";
																		else
																			minimumPrices += catName+" $"+price+"H,";
																	}else{
																		if(j == catagoriesMap.size() - 1)
																			minimumPrices += catName+" $"+price;
																		else
																			minimumPrices += catName+" $"+price+",";
																	}
																}																	
															}
														}
													}
												}
												String shopImages = "";
												try{
													File dir = new File("/var/lib/tomcat/webapps/bloomnet-images/ads"); 
													FileFilter fileFilter = new WildcardFileFilter(shopCode+"pic*"); 
													File[] files = dir.listFiles(fileFilter);
													if(files != null){
														for(int ii=1; ii<=files.length; ++ii){
															if(ii==files.length)
																shopImages += "https://directory.bloomnet.net/bloomnet-images/ads/"+shopCode+"pic-"+ii+".jpg";
															else
																shopImages += "https://directory.bloomnet.net/bloomnet-images/ads/"+shopCode+"pic-"+ii+".jpg,";
														}
															
													}
														
												}catch(Exception ee){
													ee.printStackTrace();
												}
												
												String moreInfo = "";
												if(customListing != null){
													moreInfo = customListing.replaceAll("\"","&quot").replaceAll("'","&rsquo;");			
												}
													%>
													
													<% Date date= new Date();
												Calendar cal = Calendar.getInstance();
												cal.setTime(date);
												int month = cal.get(Calendar.MONTH); 
												%>
												<div class="<%if(bid != null && !bid.equals("") && Double.valueOf(bid) > 0.0){%>listing priority<%}else if(adSize.length()>0 && validAd){ %>listing preferred<%}else if ((listingId!=null)&&((!listingId.isEmpty()))){ %>listing<%}
												if(selectedShopCode.equals(shopCode)){ %> selected<% }
												if (seasonalImages.equalsIgnoreCase("True")){
													odds = true; 
													if (month == 11 || month ==0){ %>" style="background-image: url(https://directory.bloomnet.net/bloomnet-images/ads/<%=winter %>);<%}
													else if(month == 1){%>" style="background-image: url(https://directory.bloomnet.net/bloomnet-images/ads/<%=vday %>);<%}
													else if(month == 2 || month == 3){%>" style="background-image: url(https://directory.bloomnet.net/bloomnet-images/ads/<%=spring %>);<%}
													else if(month == 4){%>" style="background-image: url(https://directory.bloomnet.net/bloomnet-images/ads/<%=mday %>);<%}
													else if(month == 5 || month == 6 || month == 7){%>" style="background-image: url(https://directory.bloomnet.net/bloomnet-images/ads/<%=summer %>);<%}
													else if(month == 8 || month == 9 || month == 10){%>" style="background-image: url(https://directory.bloomnet.net/bloomnet-images/ads/<%=fall %>);<%}
												 } %> background-repeat:no-repeat;background-position: center;background-size: cover;">										<div class="left">
													<h3><%=listingName%> <br/><%=listingId %></h3>
													<p><%=address1 %></p>
													<%if(order != null && order.getRecipient() != null && order.getRecipient().getCountryId() != null && !order.getRecipient().getCountryId().equals("1") && !order.getRecipient().getCountryId().equals("2") && !order.getRecipient().getCountryId().equals("3")){ %>
														<p><%=zip %></p>
													<%}else{ %>
														<p><%=city %>, <%=state %>, <%=zip %></p>
													<%} %>
													<p><%=phone800 %></p>
													<p><%=phone %></p>
													<p><%=contact %></p>
													<%if(!delivery.equals("")){ 
															 if(delivery.endsWith(".0"))delivery += "0";%>
														<p><strong class="green">Delivery:</strong> $<%=delivery %></p>
													<%} %>
												</div>
												<div class="clear"></div>
												<%if(adSize.length()>0 && validAd){ %><div class="clickable sponsorlogo" onclick="displayAd('https://directory.bloomnet.net/bloomnet-images/ads/<%=shopCode %>-<%=entryCode %><%=adSize %>.png?timestamp=<%=today %>')"><img src="https://directory.bloomnet.net/bloomnet-images/ads/<%=shopCode %>-<%=entryCode %><%=adSize %>.png?timestamp=<%=today %>" /></div><br/><%} %>
												<%
												String availableHtml = "";
												if(available)
													availableHtml = "<a class=\"calltoaction clickable\" onclick=\"sendFromListing('"+shopCode+"' ,this)\"> <img src=\"images/selectflorist.png\" /></a>";
												else if(tlo)
													availableHtml = "<a class=\"calltoaction\"> <img src=\"images/PleaseCall.png\" /></a>";
												else
													availableHtml = "<a class=\"calltoaction\"> <img src=\"images/unavailable.png\" /></a>";
												%>
												<%=availableHtml %>
											
												<% boolean priorityBool = false;
												if(bid != null && !bid.equals("") && Double.valueOf(bid) > 0.0){
													priorityBool = true;
													%>
													
													<%if((phone.equals("") || phone800.equals("")) && adSize.equals("")){ %>
														<br />
													<%} %>
													
													<%if(!phone.equals("") && !phone800.equals("") && seasonalImages.equalsIgnoreCase("True")){ %>
														<br />
													<%} %>
													
												<div style="width: 35px; height: 35px; position: absolute; margin-left: -5px; margin-top: -2px;"><img style="max-width: 100%; max-height: 100%;" src="images/BNLogo.png" /></div>																						<%} %>
												
												<div class="arrows" <%if(priorityBool){ %>style="margin-left: 40px;"<%} %>>
													<%if(adSize.length() > 0 && validAd){ %><a class="clickable green" onclick="displayAd('https://directory.bloomnet.net/bloomnet-images/ads/<%=shopCode %>-<%=entryCode %><%=adSize %>.png')">Display Ad &gt;</a> &nbsp;<%} %>
													<%if(facData != null && !facData.equals("")){ %><a class="clickable green" onclick="displayFacData('<%=facData.replaceAll("'","&quot;").replaceAll("\"","") %>')">Facilities &gt;</a> &nbsp;<%} %>
													<%if(!minimumPrices.equals("")){ %><a class="clickable green" onclick="displayMinimums('<%=minimumPrices %>')">Minimums &gt;</a> &nbsp;<%} %>
													<a class="clickable green" onclick="getOperationalHours('<%=shopCode %>')">Hours &gt;</a> &nbsp;
													<!-- <a class="clickable green" onclick="getZipsCovered('<%=shopCode %>')">Zips Covered &gt;</a> &nbsp; -->
													<!-- <a class="clickable green">Video &gt;</a> &nbsp; -->
													<%if(!shopImages.equals("")){ %><a class="clickable green" onclick="displayImages('<%=shopImages %>')">Images &gt;</a> &nbsp;<%} %>
													<%String safeName = listingName.replaceAll("'","&rsquo;"); %>
													<%String finalAddress = "";
													if(address2 != null && !address2.equals("")){
														finalAddress = address1+","+address2+","+city+","+state+","+zip;
													}else{
														finalAddress = address1+","+city+","+state+","+zip;
													}
													
													String showMoreInfoString = "\""+shopCode +"\",\""+safeName +"\",\""+address1+"," + city + "," + state + "," + zip +"\",\""+moreInfo +"\",\""+minimumPrices +"\",\""+shopCode+","+safeName+","+phone+"--"+finalAddress +"\",\""+localProducts +"\",\""+shopCode +"\"";
													
													%>
													<%if(doMoreInfo != null && doMoreInfo.equalsIgnoreCase("True")){ %> <a class="clickable" color="purple" onclick='showMoreInfo(<%=showMoreInfoString %>)' ><font color="purple"> More Info > </font> </a><%} %>

												</div>
												<%if(facebook!=null || twitter!=null || linkedin!=null ||googleplus!=null){ %>
													<div id="social" style="margin-top: 10px; margin-left: -7px;">
												<%if(facebook != null){ %><a href="https://<%=facebook %>" onclick="return popitup('https://<%=facebook %>','facebook');"><img src="images/facebook.png"></img></a><a href="https://<%=facebook %>" onclick="return popitup('https://<%=facebook %>','facebook');">Facebook</a><%} %>
														<%if(twitter != null){ %><a href="https://<%=twitter %>" onclick="return popitup('https://<%=twitter %>','twitter');"><img src="images/twitter.png"></img></a><a href="https://<%=twitter %>" onclick="return popitup('https://<%=twitter %>','twitter');">Twitter</a><%} %>
														<%if(linkedin != null){ %><a href="https://<%=linkedin %>" onclick="return popitup('https://<%=linkedin %>','linkedin');"><img src="images/linkedin.png"></img></a><a href="https://<%=linkedin %>" onclick="return popitup('https://<%=linkedin %>','linkedin');">LinkedIn</a><%} %>
														<%if(googleplus != null){ %><a href="https://<%=googleplus %>" onclick="return popitup('https://<%=googleplus %>','googleplus');"><img src="images/googleplus.png"></img></a><a href="https://<%=googleplus %>" onclick="return popitup('https://<%=googleplus %>','googleplus');">Google+</a><%} %>
														<%if(instagram != null){ %><a href="https://<%=instagram %>" onclick="return popitup('https://<%=instagram %>','instagram');"><img src="images/instagram.png"></img></a><a href="https://<%=instagram %>" onclick="return popitup('https://<%=instagram %>','instagram');">Instagram</a><%} %>
														<%if(pinterest != null){ %><a href="https://<%=pinterest %>" onclick="return popitup('https://<%=pinterest %>','pinterest');"><img src="images/pinterest.png"></img></a><a href="https://<%=pinterest %>" onclick="return popitup('https://<%=pinterest %>','pinterest');">Pinterest</a><%} %>
													</div>
												<%} %>
											</div>
										<%	}
										}
									}
					%>
				
	
										</form>
	
					</div>
					<div id ="buttonarea" <%if(existingCoverageNoZips.equals("YES")){ %> style="display:none;"<%} %>>
						<table>
						<tr>
							<%if(hasPrev == true){ %><td><button id="pagePrev" class="clickable pagePrev">&nbsp;</button></td><%} %>
							<%if(hasNext == true){ %><td><button id="pageNext" class="clickable pageNext">&nbsp;</button></td><%} %>
						</tr>
						</table>
						<br />
						</div>
					</div>
	<%} %>
				
			</div>



			<!--  
			<div class="enteranorder">
				<div class="enteranorderwrapper">
					<img src="images/smallor.png" class="or">
					<a href="#">Enter an Order</a>
					<img src="images/check.png" class="check">
				</div>
					<img src="images/arrowdown.png" class="right" id="arrow">
			</div>
	-->
			<%if(sideAds != null && sideAds.size() > 0){ 
				Collections.sort(sideAds, new Comparator<String>() {
				    public int compare(String str1, String str2) {
				        String substr1 = str1.split("----")[4];
				        String substr2 = str2.split("----")[4];

				        return Double.valueOf(substr2).compareTo(Double.valueOf(substr1));
				    }
				});
			%>
				<div id="sponsorblock">
					<%for(int ii=0; ii<sideAds.size(); ++ii){
						String[] vars = sideAds.get(ii).split("----");
                    	String sideAd = vars[0];
                    	String moreInfoString = vars[1];
                    	String doMoreInfo = vars[2]; %>

						<div class="sponsor">
							<a class="clickable" onclick='<%if(doMoreInfo != null && doMoreInfo.equalsIgnoreCase("True")){ %>showMoreInfo(<%=moreInfoString %>);<%}else{ %>confirmSelection("<%=sideAd.split("-")[0] %>","<%=sideAd.split("-")[0] %>");<%} %> );'>
								<img src="https://directory.bloomnet.net/bloomnet-images/ads/<%=sideAd %>" />
							</a>
						</div>	
					<%} %>		
				</div>
			<%}else{ %>
				<jsp:include page="common/fillerads.jsp" />
			<%} 
			
			if(topAds != null && topAds.size() > 0){
				
				%>
					<script type="text/javascript">$("#bannerAdsPlaceholder").show();</script>
	                <div id="bannerAds" class="jcarousel-wrapper" style="width: 930px; height: 150px; margin-bottom:-20px; overflow: hidden; position: absolute; top:61px;">
		                <div data-jcarousel="true">
		                <%if(topAds.size() == 1){ %>
	                    	<ul style="left: 0px; top: 0px;">
	                    <%}else{ %>
	                    	<ul class="jcarousel" style="left: 0px; top: 0px;">
	                    <%} %>
		                    <% 
		                    for(int ii=0; ii<topAds.size(); ++ii){ 
		                    	String[] vars = topAds.get(ii).split("----");
		                    	String topAd = vars[0];
		                    	String moreInfoString = vars[1];
		                    	String doMoreInfo = vars[2]; %>
		                    	<%if(doMoreInfo != null && doMoreInfo.equalsIgnoreCase("True")){ %>
		                    		<li class="clickable" onclick='showMoreInfo(<%=moreInfoString %>)' ><img width="930px" height="150px" alt="" src="https://directory.bloomnet.net/bloomnet-images/ads/<%=topAd%>" /></li>
		                    	<%}else{ %>
		                    		<li class="clickable" onclick="confirmSelection('<%=topAd.split("-")[0] %>','<%=topAd.split("-")[0] %>')"><img width="930px" height="150px" alt="" src="https://directory.bloomnet.net/bloomnet-images/ads/<%=topAd%>" /></li>
		                    <%}
		                    } %>
		                    </ul>
		                </div>
		            </div>
	  
			<%}	%>	
				
			
			<div class="clear"></div>

		<!-- Write A Review Modal -->
 		<div id="write-a-review" title="Write A Review For This Florist" class="dialog">
			<a class="dialogclose clickable" id="dialogclosereview"><img src="images/close.png" /></a>
			<p><img src="images/yellow-dot.png"/>This Business Deserves A Star Rating Of <div id="star-reviews" ></div></p>
			<p><img src="images/yellow-dot.png"/>Please enter a title for your review: </p>
			<p><input type="text" name="titleTxt" id="titleTxt"  title="Title" maxlength="50"/></p>
			<p><img src="images/yellow-dot.png"/>If I Told My Best Friend About This Place, I'd Say:</p>
			<textarea id="reviewTxt" name="reviewTxt"></textarea> <div><span id="reviewCharCount">1000</span> Characters Remaining.</div>
			<br /> <br />
			<!-- <p><img src="images/yellow-dot.png"/>Also Add My Review To My Facebook Page (optional)</p> -->
			<div id="submit-review" class="clickable" onclick="saveReview()"><img src="images/submit-review.png"/></div>
			<br /> <br />
			<div id="reviewError" style="color:red;"></div>
		</div>

 			<!-- More Information Modal -->
  		<div id="more-information" title="More Information" class="dialog" >
			<a class="dialogclose clickable"><img src="images/close.png" /></a>	
			<div id="moreInfoHours" ></div>	
			<div id="map-canvas-more-info"></div>	
			<div id="offerings-content"></div>
			<div id="minimums-more-content"></div>
			<div id="more-information-content"></div>
			<div id="local-products"></div>
		</div>

 		<!-- Store Hours Modal -->
   		<div id="store-hours" title="Store Hours" class="dialog">
			<a class="dialogclose clickable"><img src="images/close.png" /></a>
			<div id="store-hours-content">
			</div>
		</div>
		
		<!-- Zip Coverage Modal -->
   		<div id="shop-zips" title="Zip Codes Covered" class="dialog">
			<a class="dialogclose clickable"><img src="images/close.png" /></a>
			<div id="shop-zips-content">
			</div>
		</div>
		

 			<!-- Minimums -->
  		<div id="minimums" title="Minimums" class="dialog">
  			<a class="dialogclose clickable"><img src="images/close.png" /></a>
  			<div id="minimums-content" class="text-wrapper">

			</div>
		</div>


 		<!-- Product Codification -->
 		<div id="product-codification" title="Product Codification" class="dialog">
			<a class="dialogclose clickable"><img src="images/close.png" /></a>
			<div id="product-codification-content" class="text-wrapper">
			</div>
		</div>
		
		<!-- Facility Data -->
 		<div id="facility-data" title="Facilities Served" class="dialog">
			<a class="dialogclose clickable"><img src="images/close.png" /></a>
			<div id="facility-data-content" class="text-wrapper">
			</div>
		</div>


 			<!-- Display Ad -->
    		<div id="display-ad" title="Display Ad" class="dialog">
			<a class="dialogclose clickable"><img src="images/close.png" /></a>
			<div id="display-ad-content"></div>
		</div>


 			<!-- Reviews and Recommendations -->
  		<div id="reviews-and-recommendations" title="Reviews &amp; Recommendations" class="dialog">
			<a class="dialogclose clickable"><img src="images/close.png" /></a>
			<div id="reviews-content">
			</div>
		</div>

    	<div id="images" title="Images" class="dialog">
			<a class="dialogclose clickable"><img src="images/close.png" /></a>
			<div id="images-content">
   			</div>

			<div class="thumbs">		
   				<ul id="myimagecarousel" class="jcarousel-skin">
		     	</ul>
	    	</div>
		</div>
		
		<div id="googleMap" title="Google Map" class="dialog">
			<a class="dialogclose clickable"><img src="images/close.png" /></a>
			<div id="map-canvas">
   			</div>
		</div>
 

	<!-- div id="videos" title="Videos" class="dialog">
			<a class="dialogclose clickable"><img src="images/close.png" /></a>
			<div id="video1" class="video playing">
				<iframe width="640" height="360" src="http://www.youtube.com/embed/sUavLDwbsV4?rel=0" frameborder="0" allowfullscreen></iframe>
			</div>
			<div id="video2" class="video">
				<iframe width="640" height="360" src="http://www.youtube.com/embed/YtEJ17RLhPY?rel=0" frameborder="0" allowfullscreen></iframe>
			</div>
			<div id="video3" class="video">
				<iframe width="640" height="360" src="http://www.youtube.com/embed/PCReZ1UOjfA?rel=0" frameborder="0" allowfullscreen></iframe>
			</div>
			<div id="video4" class="video">
				<iframe width="640" height="360" src="http://www.youtube.com/embed/ZzD9O1k2NA8?rel=0" frameborder="0" allowfullscreen></iframe>
			</div>
			<div id="video5" class="video">
				<iframe width="640" height="360" src="http://www.youtube.com/embed/fIbCSGsVinU?rel=0" frameborder="0" allowfullscreen></iframe>
			</div> 

			<div class="thumbs">  			
   				<ul id="myvideocarousel" class="jcarousel-skin">
  					<li><a title="video1"><img src="images/videos-thumb1.png" class="thumb"/></a></li>
					<li><a title="video2"><img src="images/videos-thumb2.png" class="thumb"/></a></li>
					<li><a title="video3"><img src="images/videos-thumb3.png" class="thumb"/></a></li>
					<li><a title="video4"><img src="images/videos-thumb2.png" class="thumb"/></a></li>
  					<li><a title="video5"><img src="images/videos-thumb3.png" class="thumb"/></a></li>
		     	</ul>
	    	</div>
		</div> -->
		</div>
	</div>
	<jsp:include page="common/footerContemp.jsp" />
</body>
</html>