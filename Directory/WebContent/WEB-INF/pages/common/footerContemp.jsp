<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.GregorianCalendar" %>
<div id="footerwrapper">
	<div id="footer">
		&copy; 2011 - <%=new GregorianCalendar().get(Calendar.YEAR) %> BloomNet, Inc. All rights reserved. Authorized users only.
		<a href="mailto:customerservice@bloomnet.net?Subject=Directory%20Online%20Inquiry" class="right">Contact us</a>
	</div>
</div>