<%@ page import="java.util.Random" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Date" %>

<%
long today = new Date().getTime();
ArrayList<String> fillerCategories = new ArrayList<String>();
fillerCategories.add("DVFloraAd");
fillerCategories.add("FSSAd");
fillerCategories.add("worldflowers");
fillerCategories.add("napcoAd");
fillerCategories.add("FruitBouquetAd");

ArrayList<String> adUrls = new ArrayList<String>();
adUrls.add("http://www.dvflora.com");
adUrls.add("http://www.fss.com");
adUrls.add("http://www.worldflowers.com");
adUrls.add("http://www.napcoimports.com");
adUrls.add("http://screencast.com/t/yo1OVgxcn");

List<String> fillerAds = new ArrayList<String>();
List<String> fillerUrls = new ArrayList<String>();

for(int ii=0; ii<3; ++ii){
	Random generator = new Random();
	int idx = generator.nextInt(fillerCategories.size());
	String category = fillerCategories.get(idx);
	String categoryUrl = adUrls.get(idx);
	
	fillerAds.add("https://directory.bloomnet.net/bloomnet-images/ads/"+category+".png?timestamp="+today);
	fillerCategories.remove(idx);
	fillerUrls.add(categoryUrl);
	adUrls.remove(idx);
}
%>
	<div id="sponsorblock">

	<div class="sponsor">
		<a href="<%=fillerUrls.get(0) %>"><img src="<%=fillerAds.get(0) %>" /></a>
	</div>
	<div class="sponsor">
		<a href="<%=fillerUrls.get(1) %>"><img src="<%=fillerAds.get(1) %>" /></a>
	</div>
	<div class="sponsor">
		<a href="<%=fillerUrls.get(2) %>"><img src="<%=fillerAds.get(2) %>" /></a>
		</div>				
	</div>