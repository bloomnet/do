<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ page import="java.util.List" %>
<%@ page import="org.apache.lucene.document.Document" %>
<%@ page import="com.flowers.server.entity.Facility" %>
<%@ page import="com.flowers.portal.vo.Recipient" %>
<%@ page import="com.flowers.portal.vo.Order" %>
<%@ page import="com.flowers.portal.vo.DescribedProduct" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page session="false" %>

<%
long today = new Date().getTime();
Order order = (Order)request.getAttribute("order");
List<Document> fruitBouquetProducts = (List<Document>) request.getAttribute("fruitBouquet");
Facility facility = (Facility)request.getAttribute("facility");
if(request.getAttribute("searchedCity") != null){
	if(order.getRecipient() == null){
		order.setRecipient(new Recipient());
	}
	if(order.getRecipient().getFirstName() == null){
		order.getRecipient().setCity((String)request.getAttribute("searchedCity"));
		if(request.getAttribute("searchedZip") != null)
			order.getRecipient().setPostalCode((String)request.getAttribute("searchedZip"));
		if(request.getAttribute("searchedAddress") != null)
			order.getRecipient().setAddress1((String)request.getAttribute("searchedAddress"));
		if(request.getAttribute("searchedPhone") != null)
			order.getRecipient().setPhoneNumber((String)request.getAttribute("searchedPhone"));
		if(request.getAttribute("searchedState") != null)
			order.getRecipient().setState((String)request.getAttribute("searchedState"));
	}
}
//List<Document> categories = (List<Document>)request.getAttribute("categories");
%>	
	<jsp:include page="projectJs.jsp" />
	<jsp:include page="searchFloristJs.jsp" />	
	<script type="text/javascript" src="scripts/jquery.validate.js?timestamp=<%=today %>"></script>	
<div>
	<h4 class="green">Recipient Information</h4>
	<form id="orderForm" action="?view=summary&timestamp=<%=today %>" method="post" class="cmxform">
	<input type="hidden" id="productLineCount" value="1"></input>
	<input type="hidden" id="from" name="from" value="order" tabindex="1"/>
	<input type="hidden" id="orderSelCity" name="orderSelCity" value="<%if(order != null && order.getRecipient() != null){%><%=order.getRecipient().getCity()%><%} %>" />
	<input type="hidden" id="orderSelState" name="orderSelState" value="<% if(order != null && order.getRecipient() != null){%><%=order.getRecipient().getState()%><%} %>" >
	<input type="hidden" id="emptyorder" name="emptyOrder" value="${emptyorder}">
	<%if (fruitBouquetProducts != null){%>
		<input type="hidden" id="fbProductLineCount" value="<%=fruitBouquetProducts.size() %>"></input>
	<%} %>
		<div class="inputwrapper">  <input type="text" name="orderNameFirst" id="orderNameFirst" value="<c:out value="${order.recipient.firstName}" />" class="orderField addressField facilityField changeable" title="First name" maxlength="50"  /></div>
		<div class="inputwrapper">  <input type="text" name="orderNameLast" id="orderNameLast" value="<c:out value="${order.recipient.lastName}" />" title="Last name" maxlength="50"/></div><br/>


		<div class="inputwrapper"><input type="text" name="orderAddressIptStreet1" id="orderAddressIptStreet1" value="<c:out value="${order.recipient.address1}"/>" title="Street Address 1" maxlength="200" class=my-error-class/></div>
		<input type="text" name="orderAddressIptStreet2" id="orderAddressIptStreet2" value="<c:out value="${order.recipient.address2}" />" title="Street Address 2" maxlength="100"/>

		<div class="clear"></div>
		<div class="styled-select countrySelect">
			<select class="clickable" id="addressSelCountry" title="Country" style="width: 250px;">
							<c:out value="${countries}" escapeXml="false"/>
					</select>
		</div>
		<div class="clear"></div>
		
		<div class="orderFormZip green" id="orderFormZip"><input type="text" name="orderZip" id="orderZip" onkeyup="examineOrderZip()" value="<c:out value="${order.recipient.postalCode}" />" title="Zip / Postal Code" maxlength="8"/>OR&nbsp;&nbsp;</div>
		<div class="styled-select stateorprovinceselect" id="stateorprovinceselect">
			<select class="clickable" name="addresse" id="addressSelState" title="State" style="width: 175px;">
						<option selected="selected" value="">State/Province</option>
							<c:out value="${states}" escapeXml="false"/>
					</select>
		</div>
		<div class="styled-select cityselect" id="addressCitySelect">
			<select class="clickable" name=addressIptCity id="addressIptCity" title="City" style="width: 175px;">
				<option selected="selected" value="">City</option>
			</select>
		</div>
		<input style="display:none;" type="text" id="addressIptCity2" title="City" name="addressIptCity" value="<% if(order != null && order.getRecipient() != null && order.getRecipient().getFirstName() != null && order.getRecipient().getCity() != null){%><%=order.getRecipient().getCity()%><%} %>" >
		<div class="clear"></div>
		<div>
			<div id="orderAddressPhone" class="inputwrapper">
				<input type="text" style="width: 165px" id="orderAddressIptPhone" name="orderAddressIptPhone" value="<c:out value="${order.recipient.phoneNumber}" />" class="phoneNumber" title="Phone number"  maxlength="15"></input>
				<span id="phoneNumberEg">&nbsp; &nbsp; Please include area code.</span>
			</div>
		</div>
		
		<div class="styled-select occasionselect">
		
					<select class="clickable" id="orderSelOcassion" name="orderAddressSelOccasion"  title="Occasion" style="width: 175px;" >
						<option <%if(order == null || order.getOcasionId() == null){ %>selected="selected"<%} %>>Occasion</option>
						<option<% if(order != null && order.getOcasionId() != null && order.getOcasionId() == 7L){ %> selected="selected" <%} %>>Anniversary</option>
						<option<% if(order != null && order.getOcasionId() != null && order.getOcasionId() == 3L){ %> selected="selected" <%} %>>Birthday</option>
						<option<% if(order != null && order.getOcasionId() != null && order.getOcasionId() == 4L){ %> selected="selected" <%} %>>Business Gift</option>
						<option<% if(order != null && order.getOcasionId() != null && order.getOcasionId() == 1L){ %> selected="selected" <%} %>>Funeral/Memorial</option>
						<option<% if(order != null && order.getOcasionId() != null && order.getOcasionId() == 5L){ %> selected="selected" <%} %>>Holiday</option>
						<option<% if(order != null && order.getOcasionId() != null && order.getOcasionId() == 2L){ %> selected="selected" <%} %>>Illness / Get Well</option>
						<option<% if(order != null && order.getOcasionId() != null && order.getOcasionId() == 6L){ %> selected="selected" <%} %>>Maternity / Birth</option>
						<option<% if(order != null && order.getOcasionId() != null && order.getOcasionId() == 8L){ %> selected="selected" <%} %>>Other</option>
					</select>
			
		</div>
		<br /><br /><br />
		<h4 class="green">Product Type</h4>
		<div class="styled-select occasionselect" style="margin-top: 5px;">
					<select class="clickable" id="ordProductId" name="productId"  title="Product Type" style="width: 175px;" >
						<option value="1" <%if(order == null || order.getProductId() == null || order.getProductId().equals("1")){ %>selected="selected"<%} %>>Floral</option>
						<option value="1144" <% if(order != null && order.getProductId() != null && order.getProductId().equals("1144")){ %> selected="selected" <%} %>>Fruit Bouquet</option>
					</select>
			
		</div>
	<br/><br/><br/>
		<div id="perishableHeader"><h4 class="green">Contains Perishable Items</h4></div>
		<div class="inputwrapper" id="perishables">
					<select class="clickable styled-select occasionselect" id="containsPerishables" name="containsPerishables"  title="Contains Perishable Items" style="width: 80px; -webkit-appearance: none; text-indent: 1px; text-overflow: '';" >
						<option value=""></option>
						<option value="Y" <%if(order != null && order.getContainsPerishables()!= null && order.getContainsPerishables().equals("Y")){ %>selected="selected"<%} %>>Yes</option>
						<option value="N" <% if(order != null && order.getContainsPerishables()!= null && order.getContainsPerishables().equals("N")){ %> selected="selected" <%} %>>No</option>
					</select>
			
		</div>
	<br/><br/><br/>
		
		<h4 class="green">Products</h4>
		<div id="productTable">
		<%
		boolean hasProducts = false;
		if(order != null && order.getDescribedProducts() != null) {
			for(DescribedProduct prod : order.getDescribedProducts()) {
				if(!prod.isCodified()) {
					hasProducts = true;
					String prodCode = prod.getCode() != null ? prod.getCode() : "";
					String prodName = prod.getDescription() != null ? prod.getDescription() : "";
					String prodPrice = prod.getPrice() != null ? prod.getPrice().toString().substring(1) : "";
					String prodQuant = String.valueOf(prod.getQuantity());
		%>
		<div class="orderLine" id="orderLine1">
		<input type="text" name="productCode" id="productCode"  title="Product Code" value="<%=prodCode %>" /><input type="text" name="productQuantity" id="productQuantity" class="productQuantity" title="Quantity" value="<%=prodQuant %>" onchange="calcTotal()" onkeypress="return onlyNumbers();" /><input type="text" name="productDescription" id="productDescription" title="Product Description" value="<%=prodName %>" /> <input type="text" name="productPrice" id="productPrice" class="productPrice" title="Price" value="<%=prodPrice %>" onchange="calcTotal()" onkeypress="return onlyNumbers();" /><br/>
		</div>
		<%
		}}}
		if(!hasProducts) {
			
		%>
			<div class="orderLine" id="orderLine1">
		<input type="text" name="productCode" id="productCode"  title="Product Code"  /><input type="text" name="productQuantity" id="productQuantity" class="productQuantity" title="Quantity" onchange="calcTotal()" onkeypress="return onlyNumbers();" /><input type="text" name="productDescription" id="productDescription" title="Product Description"  /> <input type="text" name="productPrice" id="productPrice" class="productPrice" title="Price" onchange="calcTotal()" onkeypress="return onlyNumbers();" /><br/>
		</div>
		<%
		}
		%>
		</div>
		
		<div id="fbProductTable" style="display: none;">
		<%
		Map<String,String> existingChosenProducts = new HashMap<String,String>();
		if(order != null) {
			for(DescribedProduct prod : order.getDescribedProducts()) {
				if(!prod.isCodified()) {
					hasProducts = true;
					String prodCode = prod.getCode() != null ? prod.getCode() : "";
					String prodName = prod.getDescription() != null ? prod.getDescription() : "";
					String prodPrice = prod.getPrice() != null ? prod.getPrice().toString().substring(1) : "";
					String prodQuant = String.valueOf(prod.getQuantity());
					existingChosenProducts.put(prodCode,prodQuant);
				}
			}
		}
		int ii = 0;
		if(fruitBouquetProducts != null){
			for(Document d: fruitBouquetProducts){
				String prodCode =d.get("product_code");
				String prodDescription = d.get("product_description").replaceAll("\"","&quot").replaceAll("'","&rsquo;");
				String prodPrice = d.get("price");
				++ii;
				%><hr><%
				if(existingChosenProducts.get(prodCode) != null){
					String quantity = existingChosenProducts.get(prodCode);
					%>
					<br />
					<div id="fbOrderLine<%=ii %>">
							<span><img src="https://directory.bloomnet.net/bloomnet-images/dropship/<%=prodCode %>.png" class="productImage"/><br /><%=prodCode %><br />$<%=prodPrice %></span>
							<input readonly type="hidden" name="productCode" id="fbProductCode" class="productCode" title="Product Code" value="<%=prodCode %>" />
							<input type="hidden" readonly name="productDescription" id="fbProductDescription" title="Product Description" class="productDescription" value="Fruit Bouquet Product Code: <%=prodCode %>: <%=prodDescription %>" />
							<span class="fbProdDescription"><%=prodDescription %></span>
							<input readonly type="hidden" name="productPrice" id="fbProductPrice" class="productPrice" title="Price" value="<%=String.valueOf(Double.valueOf(prodPrice)) %>" onchange="fbCalcTotal()" onkeypress="return onlyNumbers();" />
							<input onchange="activateProduct('fbOrderLine<%=ii %>','<%=prodCode %>','<%=prodDescription.replaceAll("'", "\\'") %>','<%=String.valueOf(Double.valueOf(prodPrice)) %>','<%=ii %>')" type="text" name="productQuantity" id="fbProductQuantity" class="fbProductQuantity" value="<%=quantity %>" onkeypress="return onlyNumbers();" />
					</div>
					<br />
					<% String totalPrice = String.valueOf(Double.valueOf(prodPrice)); %>
					<br />
				<%
				}else{
			%>
				<br />
				<div id="fbOrderLine<%=ii %>">
						<span><img src="https://directory.bloomnet.net/bloomnet-images/dropship/<%=prodCode %>.png" class="productImage"/><br /><%=prodCode %><br />$<%=prodPrice %></span>
						<input readonly type="hidden" name="dummyCode" id="fbProductCode" class="productCode" title="Product Code" value="<%=prodCode %>" />
						<input type="hidden" readonly name="dummyDescription" id="fbProductDescription" title="Product Description" class="productDescription" value="Fruit Bouquet Product Code: <%=prodCode %>: <%=prodDescription %>" />
						<span class="fbProdDescription"><%=prodDescription %></span>
						<input readonly type="hidden" name="dummyPrice" id="fbProductPrice" class="productPrice" title="Price" value="<%=String.valueOf(Double.valueOf(prodPrice)) %>" onchange="fbCalcTotal()" onkeypress="return onlyNumbers();" />
						<input onchange="activateProduct('fbOrderLine<%=ii %>','<%=prodCode %>','<%=prodDescription.replaceAll("'", "\\'") %>','<%=String.valueOf(Double.valueOf(prodPrice)) %>','<%=ii %>')" type="text" name="dummyQuantity" id="fbProductQuantity" class="fbProductQuantity" title="QTY" onkeypress="return onlyNumbers();" />
				</div>
				<br />
				<br />
			<%
				}
				%><hr><%
			}
		}
		%>
		</div>
		
		<a class="clickable" id="plus" onclick="addOrderLine()"><img src="images/plus.png" class="clickable"/>Add Another Product</a><br/><br/>
		
		<hr/>
			<span id="ordertotalWrapper"><span class="green">Order total </span><span style="float: right" id="orderTotal">0.00</span><span style="float: right">$</span></span><br/>
		<hr/><br/>
		
		<div class="textareacontainer">
			<textarea id="orderCardMsgTxt" name="orderCardMsgTxt" class="cardmessage left" title="Card message"><c:out value="${order.cardMessage}" /></textarea>
			<span class="left label"><span id="msgCount">200</span> Characters Remaining</span>
			<div class="clear"></div>
		</div>
		
		<div class="textareacontainer">							
			<textarea id="orderSpecialInstructionsTxt" name="orderSpecialInstructionsTxt" class="specialinstructions left" title="Special Instructions"><c:out value="${order.specialInstruction}" /></textarea>
			<span class="left label"><span id="instCount">200</span> Characters Remaining</span>
			<div class="clear"></div>
		</div>
		<div id="errorMessage" class="container" style="color: red;">
		</div>
		
		<input type="button" name="continue" id="continue" value=""/>
		<input type="button" name="clear" id="clearallfields" value=""/>				
	</form>					
</div>

<div id="address-verification" title="Address Verification" class="dialog">
	<div id=closeAddress><a class="dialogclose clickable"><img src="images/close.png" /></a></div>
	<p class="graybg">According To The USPS&reg; The Address You Entered Is Invalid </p>

	<strong>Select a Recommended Address:</strong>
	<div id="recommendedaddress" class="recommendedaddresses" ></div>

	<hr/>

	<div class="modal-footer">
		<div class="buttons">
			<div id="keepAddress"><a href="#"><img src="images/keeporiginal.png"/></a></div>
			<div id="editAddress"><a href="#"><img src="images/editthisaddress.png"/></a></div>	
		</div>
		<p><strong>Keep or Edit Original Address:</strong></p><br />
		<div id="originalAddress"></div>
	</div>
</div>