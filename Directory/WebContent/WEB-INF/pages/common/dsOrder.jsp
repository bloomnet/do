<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ page import="java.util.List" %>
<%@ page import="org.apache.lucene.document.Document" %>
<%@ page import="com.flowers.server.entity.Facility" %>
<%@ page import="com.flowers.portal.vo.Recipient" %>
<%@ page import="com.flowers.portal.vo.Order" %>
<%@ page import="com.flowers.portal.vo.DescribedProduct" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page session="false" %>

<%
long today = new Date().getTime();
Order order = (Order)request.getAttribute("order");
Facility facility = (Facility)request.getAttribute("facility");
List<Document> dropshipProducts = (List<Document>) request.getAttribute("dropship");
Double shippingCost = 14.99;
if(request.getAttribute("searchedCity") != null){
	if(order.getRecipient() == null)
		order.setRecipient(new Recipient());
	order.getRecipient().setCity((String)request.getAttribute("searchedCity"));
	if(request.getAttribute("searchedZip") != null)
		order.getRecipient().setPostalCode((String)request.getAttribute("searchedZip"));
	if(request.getAttribute("searchedAddress") != null)
		order.getRecipient().setAddress1((String)request.getAttribute("searchedAddress"));
	if(request.getAttribute("searchedPhone") != null)
		order.getRecipient().setPhoneNumber((String)request.getAttribute("searchedPhone"));
	if(request.getAttribute("searchedState") != null)
		order.getRecipient().setState((String)request.getAttribute("searchedState"));
}
//List<Document> categories = (List<Document>)request.getAttribute("categories");
%>	

<jsp:include page="dropshipJs.jsp" />
	
<div>

	<h4 class="green">Recipient Information</h4>
	<form id="dropshipForm" action="?view=summary&timestamp=<%=today %>" method="post" class="cmxform">
	<div>Delivery Date: <input readonly type="text" name=deliveryDate style="float:none;"  <% if(order != null && order.getDeliveryDate() != null){ %>value="<c:out value="${order.deliveryDate}" />" <%}else{ %>value="${currentDate}"<%} %> class="dsDeliverydate" id="dropshipDeliveryDate" /><img src="images/minicalendar.png" class="dsMinicalendar clickable" style="float:none;" />
	</div>
	<div id="dsDateError" style="color:red;"></div>
	<input type="hidden" id="dsProductLineCount" value="<%=dropshipProducts.size() %>"></input>
	<input type="hidden" id="dsFrom" name="from" value="dropshipOrder" tabindex="1"/>
	<input type="hidden" id="dsOrderSelCity" name="orderSelCity" value="<%if(order != null && order.getRecipient() != null){%><%=order.getRecipient().getCity()%><%} %>" />
	<input type="hidden" id="dsOrderSelState" name="orderSelState" value="<% if(order != null && order.getRecipient() != null){%><%=order.getRecipient().getState()%><%} %>" >
	<input type="hidden" id="dsEmptyorder" name="emptyOrder" value="${emptyorder}">
		<div class="inputwrapper">  <input type="text" name="orderNameFirst" id="dsOrderNameFirst" value="<c:out value="${order.recipient.firstName}" />" class="orderField addressField facilityField changeable" title="First name" maxlength="50"  /></div>
		<div class="inputwrapper">  <input type="text" name="orderNameLast" id="dsOrderNameLast" value="<c:out value="${order.recipient.lastName}" />" title="Last name" maxlength="50"/></div><br/>


		<div class="inputwrapper"><input type="text" name="orderAddressIptStreet1" id="dsOrderAddressIptStreet1" value="<c:out value="${order.recipient.address1}"/>" title="Street Address 1" maxlength="200" class=my-error-class/></div>
		<input type="text" name="orderAddressIptStreet2" id="dsOrderAddressIptStreet2" value="<c:out value="${order.recipient.address2}" />" title="Street Address 2" maxlength="100"/>

		<div class="clear"></div>
		
		<div class="orderFormZip green"><input type="text" name="orderZip" class="orderZip" id="dsOrderZip" onkeyup="dsExamineOrderZip()" value="<c:out value="${order.recipient.postalCode}" />" title="Zip / Postal Code" maxlength="8"/>OR&nbsp;&nbsp;</div>
		<div class="styled-select stateorprovinceselect">
			<select class="clickable" name="addresse" id="dsAddressSelState" title="State">
						<option selected="selected" value="">State/Province</option>
							<c:out value="${states}" escapeXml="false"/>
					</select>
		</div>
		<div class="styled-select cityselect">
			<select class="clickable" name=addressIptCity id="dsAddressIptCity" title="City">
				<option selected="selected" value="">City</option>
			</select>
		</div>
		<div class="clear"></div>
		<div>
			<div id="dsOrderAddressPhone" class="inputwrapper">
				<input type="text" style="width: 165px" id="dsOrderAddressIptPhone" name="orderAddressIptPhone" value="<c:out value="${order.recipient.phoneNumber}" />" class="phoneNumber" title="Phone number"  maxlength="15"></input>
				<span id="dsPhoneNumberEg">&nbsp; &nbsp; Please include area code.</span>
			</div>
		</div>
		
		<div class="styled-select occasionselect">
		
					<select class="clickable" id="dsOrderSelOcassion" name="orderAddressSelOccasion"  title="Occasion" >
						<option <%if(order == null || order.getOcasionId() == null){ %>selected="selected"<%} %>>Occasion</option>
						<option<% if(order != null && order.getOcasionId() != null && order.getOcasionId() == 7L){ %> selected="selected" <%} %>>Anniversary</option>
						<option<% if(order != null && order.getOcasionId() != null && order.getOcasionId() == 3L){ %> selected="selected" <%} %>>Birthday</option>
						<option<% if(order != null && order.getOcasionId() != null && order.getOcasionId() == 4L){ %> selected="selected" <%} %>>Business Gift</option>
						<option<% if(order != null && order.getOcasionId() != null && order.getOcasionId() == 1L){ %> selected="selected" <%} %>>Funeral/Memorial</option>
						<option<% if(order != null && order.getOcasionId() != null && order.getOcasionId() == 5L){ %> selected="selected" <%} %>>Holiday</option>
						<option<% if(order != null && order.getOcasionId() != null && order.getOcasionId() == 2L){ %> selected="selected" <%} %>>Illness / Get Well</option>
						<option<% if(order != null && order.getOcasionId() != null && order.getOcasionId() == 6L){ %> selected="selected" <%} %>>Maternity / Birth</option>
						<option<% if(order != null && order.getOcasionId() != null && order.getOcasionId() == 8L){ %> selected="selected" <%} %>>Other</option>
					</select>
			
		</div>
	<br/><br/><br/>
		
		<h4 class="green">Products</h4>
		<div id="dsProductTable">
		<%
		Map<String,String> existingChosenProducts = new HashMap<String,String>();
		boolean hasProducts = false;
		if(order != null) {
			for(DescribedProduct prod : order.getDescribedProducts()) {
				if(!prod.isCodified()) {
					hasProducts = true;
					String prodCode = prod.getCode() != null ? prod.getCode() : "";
					String prodName = prod.getDescription() != null ? prod.getDescription() : "";
					String prodPrice = prod.getPrice() != null ? prod.getPrice().toString().substring(1) : "";
					String prodQuant = String.valueOf(prod.getQuantity());
					existingChosenProducts.put(prodCode,prodQuant);
				}
			}
		}
		int ii = 0;
		for(Document d:dropshipProducts){
			String prodCode =d.get("product_code");
			String prodDescription = d.get("product_description").replaceAll("\"","&quot").replaceAll("'","&rsquo;");
			String prodPrice = d.get("price");
			++ii;
			%><hr><%
			if(existingChosenProducts.get(prodCode) != null){
				String quantity = existingChosenProducts.get(prodCode);
				%>
				<br />
				<div id="dsOrderLine<%=ii %>">
						<img src="https://directory.bloomnet.net/bloomnet-images/dropship/<%=prodCode %>.png" class="productImage" />
						<input readonly type="hidden" name="productCode" id="dsProductCode" class="productCode" title="Product Code" value="<%=prodCode %>" />
						<input type="hidden" readonly name="productDescription" id="dsProductDescription" title="Product Description" class="productDescription" value="<%=prodDescription %>" />
						<span class="dsProdDescription"><%=prodDescription %></span>
						<input readonly type="hidden" name="productPrice" id="dsProductPrice" class="productPrice" title="Price" value="<%=String.valueOf(Double.valueOf(prodPrice) + shippingCost) %>" onchange="dsCalcTotal()" onkeypress="return onlyNumbers();" />
						<input onchange="activateProduct('dsOrderLine<%=ii %>','<%=prodCode %>','<%=prodDescription %>','<%=String.valueOf(Double.valueOf(prodPrice) + shippingCost) %>','<%=ii %>')" type="text" name="productQuantity" id="dsProductQuantity" class="dsProductQuantity" value="<%=quantity %>" onchange="dsCalcTotal()" onkeypress="return onlyNumbers();" />
				</div>
				<div class="dsProdCode"><%=prodCode %></div>
				<br />
				<% String totalPrice = String.valueOf(Double.valueOf(prodPrice) + shippingCost); %>
				<div class="dsProdPrice">$<%=prodPrice %> + $<%=shippingCost %> Shipping</div>
				<br />
			<%
			}else{
		%>
			<br />
			<div id="dsOrderLine<%=ii %>">
					<img src="https://directory.bloomnet.net/bloomnet-images/dropship/<%=prodCode %>.png" class="productImage" />
					<input readonly type="hidden" name="dummyCode" id="dsProductCode" class="productCode" title="Product Code" value="<%=prodCode %>" />
					<input type="hidden" readonly name="dummyDescription" id="dsProductDescription" title="Product Description" class="productDescription" value="<%=prodDescription %>" />
					<span class="dsProdDescription"><%=prodDescription %></span>
					<input readonly type="hidden" name="dummyPrice" id="dsProductPrice" class="productPrice" title="Price" value="<%=String.valueOf(Double.valueOf(prodPrice) + shippingCost) %>" onchange="dsCalcTotal()" onkeypress="return onlyNumbers();" />
					<input onchange="activateProduct('dsOrderLine<%=ii %>','<%=prodCode %>','<%=prodDescription %>','<%=String.valueOf(Double.valueOf(prodPrice) + shippingCost) %>','<%=ii %>')" type="text" name="dummyQuantity" id="dsProductQuantity" class="dsProductQuantity" title="QTY" onchange="dsCalcTotal()" onkeypress="return onlyNumbers();" />
			</div>
			<div class="dsProdCode"><%=prodCode %></div>
			<br />
			<div class="dsProdPrice">$<%=prodPrice %> + $<%=shippingCost %> Shipping</div>
			<br />
		<%
			}
			%><hr><%
		}
		%>
		</div>
		<br /><br /><br />
		<hr/>
			<span id="ordertotalWrapper"><span class="green">Order total </span><span style="float: right" id="dsOrderTotal">0.00</span><span style="float: right">$</span></span><br/>
		<hr/><br/>
		
		<div class="textareacontainer">
			<textarea id="dsOrderCardMsgTxt" name="orderCardMsgTxt" class="cardmessage left" title="Card message"><c:out value="${order.cardMessage}" /></textarea>
			<span class="left label"><span id="dsMsgCount">200</span> Characters Remaining</span>
			<div class="clear"></div>
		</div>
		
		<div class="textareacontainer">							
			<textarea id="dsOrderSpecialInstructionsTxt" name="orderSpecialInstructionsTxt" class="specialinstructions left" title="Special Instructions"><c:out value="${order.specialInstruction}" /></textarea>
			<span class="left label"><span id="dsInstCount">200</span> Characters Remaining</span>
			<div class="clear"></div>
		</div>
		<div id="dsErrorMessage" class="container" style="color: red;">
		</div>
		
		<input type="button" name="continue" id="dsContinue" class="continue" value=""/>
		<input type="button" name="clear" id="dsClearallfields" class="clearallfields" value=""/>				
	</form>					
</div>

<div id="dsAddress-verification" title="Address Verification" class="dialog">
	<div id=dsCloseAddress><a class="dialogclose clickable"><img src="images/close.png" /></a></div>
	<p class="graybg">According To The USPS&reg; The Address You Entered Is Invalid </p>

	<strong>Select a Recommended Address:</strong>
	<div id="dsRecommendedaddress" class="recommendedaddresses" ></div>

	<hr/>

	<div class="modal-footer">
		<div class="buttons">
			<div id="dsKeepAddress"><a href="#"><img src="images/keeporiginal.png"/></a></div>
			<div id="dsEditAddress"><a href="#"><img src="images/editthisaddress.png"/></a></div>	
		</div>
		<p><strong>Keep or Edit Original Address:</strong></p><br />
		<div id="dsOriginalAddress"></div>
	</div>
</div>