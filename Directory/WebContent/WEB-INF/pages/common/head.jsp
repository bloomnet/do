<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ page import="java.util.Date" %>

<%@ page session="false" %>
<%long today = new Date().getTime(); %>

<div id="header">
			<div class="left">
				<a  id="logo"><img src="images/logo.png" alt="Bloomnet Logo" title="Bloomnet Logo"/></a>
			</div>
			<div class="right">
				<a class="clickable" id="layouttoggle" >&lt;&nbsp;&nbsp;&nbsp;&nbsp;Start Over&nbsp;&nbsp;&nbsp;&nbsp;</a>
				<a id="signout" href="<%= request.getContextPath() %>/signout.htm?timestamp=<%=today %>" class="clickable" >Sign Out</a>
				<br/>
				<div class="text">					
					<span id="questions">Questions? Call 800-256-6663</span>
				</div>
			</div>
			<div class="clear"></div>
		</div>  <!-- end of header -->       