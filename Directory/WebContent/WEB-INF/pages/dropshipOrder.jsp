<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ page import="java.util.List" %>
<%@ page import="org.apache.lucene.document.Document" %>
<%@ page import="com.flowers.server.entity.Facility" %>
<%@ page import="com.flowers.portal.vo.Order" %>
<%@ page import="com.flowers.portal.vo.DescribedProduct" %>
<%@ page import="java.util.Date" %>
<%@ page session="false" %>

<%
	Order order = (Order)request.getAttribute("order");
	if (order != null){
	String instructions  = order.getSpecialInstruction();
	}
	Facility facility = (Facility)request.getAttribute("facility");
	List<Document> categories = (List<Document>)request.getAttribute("categories");
	long today = new Date().getTime();
	%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>Bloomnet - Directory Online</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<link href="styles/jquery-ui-1.9.0.custom.min.css?timestamp=<%=today %>" rel="stylesheet">	
	<link rel="stylesheet" type="text/css" href="styles/reset.css?timestamp=<%=today %>" media="screen"/>
	<jsp:include page="common/mainCss.jsp" />
<!--[if IE 7]>
        <link rel="stylesheet" type="text/css" href="styles/ie7.css" />
<![endif]-->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-XG7CP6XY9J"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
  		function gtag(){dataLayer.push(arguments);}
  		gtag('js', new Date());
  		gtag('config', 'G-XG7CP6XY9J');
	</script>

	<script type="text/javascript" src="scripts/jquery-1.8.2.min.js?timestamp=<%=today %>"></script>
	<script type="text/javascript" src="scripts/jquery-ui-1.9.0.custom.min.js?timestamp=<%=today %>"></script>
	<script type="text/javascript" src="scripts/jquery.jcarousel.min.js?timestamp=<%=today %>"></script>
	<script type="text/javascript" src="scripts/jquery.validate.js?timestamp=<%=today %>"></script>
	<jsp:include page="common/bloomnetJs.jsp" />	
	<jsp:include page="common/projectJs.jsp" />
	<jsp:include page="common/searchFloristJs.jsp" />	
	
	
</head>
<body id="form">
	<div id="container">
		<jsp:include page="common/head.jsp" />
		
		<%List<String> bannerAds = (List<String>)request.getAttribute("bannerAds"); 
		if(bannerAds != null && bannerAds.size() > 1){
		%>
			<div id="bannerAds" style="width: 930px; height: 150px; margin-bottom:-20px; overflow: hidden;" class="jcarousel-wrapper">
	                <div data-jcarousel="true">
	                    <ul class="jcarousel" style="left: 0px; top: 0px;">
	                    <% 
	                    int limit = 0;
	                    if(bannerAds.size() >= 3)
	                    	limit = 3;
	                    else
	                    	limit = bannerAds.size();
	                    for(int ii=0; ii<limit; ++ii){ %>
	                    	<li><img width="930px" height="150px" alt="" src="https://directory.bloomnet.net/bloomnet-images/ads/<%=bannerAds.get(ii)%>-BANNER.jpg"></li>
	                    <%} %>
	                    </ul>
	                </div>
	            
	            </div>
	    <%}else if(bannerAds != null && bannerAds.size() == 1){ %>
	    
	    	<div id="bannerAds" style="width: 930px; height: 150px; margin-bottom:-20px; overflow: hidden;" >
	                    <ul style="left: 0px; top: 0px;">
	                    	<li><img width="930px" height="150px" alt="" src="https://directory.bloomnet.net/bloomnet-images/ads/<%=bannerAds.get(0)%>-BANNER.jpg"></li>
	                    </ul>
	            </div>
	    
	    <%} %>
				
		<div id="content">
				<div id="accordion">
                <!--Accordion-->
				<h2 style="margin-top:0px;">
					<a style="margin-left:190px;" href="#">Enter a Dropship Order</a> 
				</h2>
				<div id="mainDropshipDiv">
				   <jsp:include page="common/dsOrder.jsp" />
				
				</div>
              <!-- end of Accordion -->
									
			</div>

			<jsp:include page="common/fillerads.jsp" />

			<div class="clear"></div>
		</div>
		
	</div>
	<jsp:include page="common/footerContemp.jsp" />
</body>
</html>