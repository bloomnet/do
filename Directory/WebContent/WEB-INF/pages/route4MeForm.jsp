<%@ page import="java.util.Date" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>Bloomnet - Driver App Registration</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<jsp:include page="common/mainCss.jsp" />
	<%long today = new Date().getTime(); %>
	<div id="header">
			<div class="left" style="margin-top:-6px; max-width: 190px; overflow:hidden;">
				<img src="images/logo.png" alt="Bloomnet Logo" title="Bloomnet Logo" />
			</div>
			<div class="right" style="margin-top:5px;">
				<a id="signout" href="<%= request.getContextPath() %>/signout.htm?timestamp=<%=today %>">Sign Out</a>
				<br/>
				<div class="text" style="margin-top:8px;">					
					<span id="questions">Questions? Call 800-256-6663</span>
				</div>
			</div>
			<div class="clear"></div>
		</div>
<!--[if IE 7]>
        <link rel="stylesheet" type="text/css" href="styles/ie7.css" />
<![endif]-->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-XG7CP6XY9J"></script>
	<script>
  		window.dataLayer = window.dataLayer || [];
  		function gtag(){dataLayer.push(arguments);}
  		gtag('js', new Date());
  		gtag('config', 'G-XG7CP6XY9J');
	</script>
	
	<!-- Hotjar Tracking Code for directory.bloomnet.net -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:1037407,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>
	
</head>
<body id="form">
	<div style="position: relative; margin-top: 10%; height:100%">
		<div style="color:black; width: 450px; margin: 0 auto; vertical-align: middle;">
			<h1 style="color:black; margin-left: 100px;"><b>BloomNet Driver App Registration</b></h1>
			<br /> <br />
			<form  method="post" action="route4MeIntake.htm" enctype="multipart/form-data">
				<table style="vertical-align: middle;">
					<%
						String email = (String)request.getSession(true).getAttribute("formEmail");
						String shopName = (String)request.getSession(true).getAttribute("formShopName");
						String contact = (String)request.getSession(true).getAttribute("formContact");
						String timezone = (String)request.getSession(true).getAttribute("formTimezone");
						if(email == null) email = "";
						if(shopName == null) shopName = "";
						if(contact == null) contact = "";
						if(timezone == null) timezone = "";
						
						%>
						<tr><td>Email:</td><td><input type="text" name="email" title="Email" value="<%=email %>" />&nbsp;*</td></tr>
						<tr><td>Password:</td><td><input type="password" name="password" title="Password" />&nbsp;*</td></tr>
						<tr><td>Repeat Password:</td><td><input type="password" name="passwordRepeated" title="Repeat Password" />&nbsp;*</td></tr>
						<tr><td>Business Name:</td><td><input type="text" name="shopName" title="Business Name" value="<%=shopName%>"/>&nbsp;*</td></tr>
						<tr><td>Contact (First & Last Name):</td><td><input type="text" name="contact" title="Contact (First & Last Name)" value="<%=contact%>"/>&nbsp;*</td></tr>
						<tr><td>Timezone:</td><td>
							<select name="timezone" title="Timezone" class="styled-select" style="width: 272px; color: #828282; -webkit-appearance: none; -moz-appearance: none;">
								<option value=""></option>
								<option value="US/Alaska" <%if(timezone.equals("US/Alaska")){ %> selected="selected" <%} %>>&nbsp;&nbsp;Alaska</option>
								<option value="America/Chicago" <%if(timezone.equals("America/Chicago")){ %> selected="selected" <%} %>>&nbsp;&nbsp;Central</option>
								<option value="America/New_York" <%if(timezone.equals("America/New_York")){ %> selected="selected" <%} %>>&nbsp;&nbsp;Eastern</option>
								<option value="US/Hawaii" <%if(timezone.equals("US/Hawaii")){ %> selected="selected" <%} %>>&nbsp;&nbsp;Hawaii</option>
								<option value="America/Phoenix" <%if(timezone.equals("America/Phoenix")){ %> selected="selected" <%} %>>&nbsp;&nbsp;Mountain</option>
								<option value="America/Los_Angeles" <%if(timezone.equals("America/Los_Angeles")){ %> selected="selected" <%} %>>&nbsp;&nbsp;Pacific</option>	
							</select>&nbsp;*				
						</td></tr>
				</table>
				<br />
				<div style="margin-left: 200px;"><input type="submit" value="Submit" /></div>
			</form>	
			<br />
			<div>
				<%if(request.getSession(true).getAttribute("failMessage") != null){ %>
					<div style="color: red;"> <%= request.getSession(true).getAttribute("failMessage") %></div>
					<% request.getSession(true).setAttribute("failMessage", null); %>
				<%}else if(request.getSession(true).getAttribute("successMessage") != null){%>
					<div style="color: green;"><%=request.getSession(true).getAttribute("successMessage") %> </div>
					<% request.getSession(true).setAttribute("successMessage", null); %>
				<%} %>
			</div>
			<br /> <br /> <br />
		</div>
	</div>
	<div class="footer" style="position:fixed; bottom:0; width: 100%;">
			<jsp:include page="common/footerContemp.jsp" />
	</div>
</body>