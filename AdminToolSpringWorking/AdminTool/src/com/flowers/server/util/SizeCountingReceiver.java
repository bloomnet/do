package com.flowers.server.util;

import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import com.vaadin.ui.Upload.Receiver;

public class SizeCountingReceiver implements Receiver {
    private static final long serialVersionUID = -5392491238827740383L;
	private String fileName;
    private String mtype;
    private int total;
    private String uploadPath = "/opt/data/import";
            
    public OutputStream receiveUpload(String filename, String MIMEType) {
        total = 0;
        fileName = filename;
        mtype = MIMEType;
        try {
        	return new FilterOutputStream(new FileOutputStream(uploadPath+"/"+filename) {
        		public void write(int b) throws IOException {
        			super.write(b);
        			total++;
        		}
        	});               
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    public String getFileName() {
        return fileName;
    }
    public String getMimeType() {
        return mtype;
    }
	public int getTotal() {
		return total;
	}
    
}