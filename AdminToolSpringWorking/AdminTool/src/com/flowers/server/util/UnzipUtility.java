package com.flowers.server.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
 

public class UnzipUtility {

    private static final int BUFFER_SIZE = 4096;
    private ResizeImages resizer = new ResizeImages();

    public void unzip(String zipFileSource, String destination) throws IOException {
    	
        ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(zipFileSource));
        ZipEntry entry = zipInputStream.getNextEntry();

        while (entry != null) {
        	
            String filePath = destination + File.separator + entry.getName();
            if (!entry.isDirectory()) {
                // if the entry is a file, not a directory, extract it
                extractFile(zipInputStream, filePath);
            }
            zipInputStream.closeEntry();
            entry = zipInputStream.getNextEntry();
        }
        
        zipInputStream.close();
        
    }

    private void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
    	
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        
        bos.close();
    	if((filePath.contains("BAN") || filePath.contains("BANNER") || filePath.contains("TOP")) && !filePath.contains("GIF") && !filePath.contains("gif")) {
    		 File file = new File(filePath);
    	     InputStream in = new FileInputStream(file);
    	     boolean isTopAd = false;
    	     if(filePath.contains("BANNER") || filePath.contains("TOP"))
    	    	 isTopAd = true;
    		 byte[] resizedBytes = resizer.createResizedCopy(in,isTopAd);
    		 in.close();
    		 bos = new BufferedOutputStream(new FileOutputStream(filePath));
    		 bos.write(resizedBytes);
    		 bos.close();
    	}
        
    }
    
}