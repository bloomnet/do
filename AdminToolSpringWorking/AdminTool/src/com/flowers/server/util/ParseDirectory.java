package com.flowers.server.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;

public class ParseDirectory {


	public ParseDirectory(File outFile) {
	
		 try {
			new PostXML_StreamResponse();
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		final String myDir = "/opt/data/fsi/";
		
		Date today = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String date = sdf.format(today);

		String myXMLFile = "MemberDirectory_wTLO_"+date+".xml";

		final String myOutFile2 = "listings.txt";
		
		try {
			
			File file = new File(myDir+myXMLFile);
			
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line;
			
			PrintWriter fos2 = new PrintWriter(myDir+myOutFile2, "UTF-8");
			
			while ((line = bufferedReader.readLine()) != null) {
				
				String [] lineTemp = line.split("<shop>");
				
				for(int ii=1; ii<lineTemp.length; ++ii ){
					
					String shopCode = lineTemp[ii].split("<shopCode>")[1].split("</shopCode>")[0];
					
					String [] serviced = lineTemp[ii].split("<servicedZips>");
					
					if(serviced.length > 1){
						String servicedZips = lineTemp[ii].split("<servicedZips>")[1].split("</servicedZips>")[0];
						servicedZips = servicedZips.replaceAll("<zip></zip>", "");
						String[] zipList = servicedZips.split("<zip>");
												
						if(zipList.length > 1){
							for(int xx=1; xx<zipList.length; ++xx){
								fos2.write(shopCode + "\t" + zipList[xx].split("</zip>")[0] + "\r\n");
							}
						}
					}
					
					System.out.println(shopCode + " added.");
				}

			}
			
			fileReader.close();
			fos2.flush();
			fos2.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		new GetListingsNotInDO(outFile);
	}
}


class PostXML_StreamResponse {

	public PostXML_StreamResponse() throws Exception {

		String myDir = "/opt/data/fsi/";
		Date today = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String date = sdf.format(today);
		String myURL = "http://www.bloomlink.net/fsiv2/processor";	//external PROD

		String myFunc = "getMemberDirectory";
		
		String myFile = "fsi_member_directory_SearchAll_wTLO.xml";

		File myXML = new File(myDir + myFile);

		File myOutFile = new File("MemberDirectory_wTLO_"+date+".xml");
		
		StringBuffer buffer = new StringBuffer();
		FileInputStream myXMLStream = new FileInputStream(myXML);
		InputStreamReader ISR = new InputStreamReader(myXMLStream);
		Reader in = new BufferedReader(ISR);
		
		int ch;
		while ((ch = in.read()) > -1) {
			buffer.append((char)ch);
		}
		in.close();
		
		String myXMLString = buffer.toString();
			
		PostMethod myPost = new PostMethod(myURL);
		
		myPost.addParameter("func", myFunc);
		myPost.addParameter("data", myXMLString);

		HttpClient myClient = new HttpClient();
		FileOutputStream outFile = null;
		InputStream myStream = null;
		
		try {
			int result = myClient.executeMethod(myPost);
			
			System.out.println("Response status code: " + result);
			
			System.out.println("Response Body: ");
			System.out.println("Getting response as Stream...");
			myStream = myPost.getResponseBodyAsStream();

			outFile = new FileOutputStream(myDir + myOutFile);
			int c;

            while ((c = myStream.read()) != -1) {
            	outFile.write(c);
            }

        } finally {
            if (in != null) {
                in.close();
            }
            if (outFile != null) {
            	outFile.close();
            }

			
			myPost.releaseConnection();
			System.out.println("Done!");
			
		}
		
	}
}

class GetListingsNotInDO {
	File outFile;
	Writer out;
	SQLData mySQL;
	
	Map<String, List<String>> listingsNotFound = new HashMap<String, List<String>>();
	Map<String, String> allShopListings = new HashMap<String, String>();
	Map<String, String> allCityZips = new HashMap<String, String>();
	
	public GetListingsNotInDO(File outFile){
		try {
			out = new BufferedWriter(new FileWriter(outFile));
			out.write("Shop Code,City,State,Country\n");
			mySQL = new SQLData();
			mySQL.login();
			exportListings();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void exportListings() throws IOException, SQLException{
		
		FileInputStream fstream = new FileInputStream("/opt/data/fsi/listings.txt");
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		
		String shopCode = "";
		String zipCode = "";
		String cityName = "";
		String stateShortName = "";
		
		String strLine;
		
		int lineCount = 0;
		
		String statement = "USE bloomnet;";
		mySQL.executeStatement(statement);
		
		String query = "SELECT zip.zip_code, city.name, state.short_name FROM city_zip_xref " +
			"INNER JOIN city ON city_zip_xref.id_city = city.id " +
			"INNER JOIN zip ON city_zip_xref.id_zip = zip.id " +
			"INNER JOIN state ON city.state_id = state.id " +
			"WHERE city.city_type = \"D\";";

		ResultSet results = mySQL.executeQuery(query);
		
		System.out.println("Setting all zips");
		while(results.next()){
			allCityZips.put(results.getString("zip_code"), results.getString("name")+","+results.getString("short_name"));
			if(results.getString("zip_code").length() > 5) 
				allCityZips.put(results.getString("zip_code").substring(0,3), results.getString("name")+","+results.getString("short_name"));
		}
		System.out.println("Finished setting all zips");
		mySQL.closeStatement();
		
		query = "SELECT shop_code, city.name, state.short_name FROM listing " +
			"INNER JOIN city_zip_xref ON listing.city_id = city_zip_xref.id_city " +
			"INNER JOIN zip ON city_zip_xref.id_zip = zip.id " +
			"INNER JOIN city ON listing.city_id = city.id " +
			"INNER JOIN state ON city.state_id = state.id;";

		results = mySQL.executeQuery(query);
		
		System.out.println("Setting all listings");
		while(results.next()){
			allShopListings.put(results.getString("shop_code")+","+results.getString("name")+","+results.getString("short_name"),"1");
		}
		System.out.println("Finished setting all listings");
		mySQL.closeStatement();
		
		while((strLine = br.readLine()) != null){
			
			lineCount++;
			if(lineCount%100 == 0){
				System.out.println("Finished with "+lineCount+" lines");
			}
			
			String myArray[] = new String[2];
			myArray = strLine.split("\t");
			
			shopCode = myArray[0];
			zipCode = myArray[1].replaceAll(" ", "");
			
			cityName = "";
			stateShortName = "";
			
			if(allCityZips.get(zipCode) != null){
				cityName = allCityZips.get(zipCode).split(",")[0];
				stateShortName = allCityZips.get(zipCode).split(",")[1];
			}
			
			if(!cityName.equals("") && !stateShortName.equals("")){
				
				if(allShopListings.get(shopCode+","+cityName+","+stateShortName) == null){
					
					if(listingsNotFound.get(shopCode) != null){
						
						List<String> cities = listingsNotFound.get(shopCode);
						
						if(!cities.contains(cityName+","+stateShortName)){
							
							cities.add(cityName+","+stateShortName);
							
							listingsNotFound.put(shopCode, cities);
							
							if(zipCode.length() < 5 || zipCode.length() > 5) 
								out.write(shopCode+","+cityName+","+stateShortName+",CAN\n");
							else
								out.write(shopCode+","+cityName+","+stateShortName+",USA\n");
						}
					}else{
						
						List<String> cities = new ArrayList<String>();
						cities.add(cityName+","+stateShortName);
						
						listingsNotFound.put(shopCode, cities);
						
						if(zipCode.length() < 5 || zipCode.length() > 5) 
							out.write(shopCode+","+cityName+","+stateShortName+",CAN\n");
						else
							out.write(shopCode+","+cityName+","+stateShortName+",USA\n");
					}
				}
				mySQL.closeStatement();
				
			}else{
				System.err.println("No city found for zip code: "+zipCode);
			}
			
		}
		br.close();
		out.close();
		
	}
	

}
