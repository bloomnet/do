package com.flowers.server.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.flowers.server.LookupException;
import com.flowers.server.entity.User;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;

public class DOConnectionKeepAlive {
	
	@Autowired CrudService crudService;
	@Autowired SearchService searchService;
	
	public void keepConnectionAlive() {
		
		SingleThread st = new SingleThread();
		
		String pql = "from State s where s.country.id = :id";
		
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("id", 1L);
		
		try {
			st.findResults(pql, crudService, param);
		} catch (LookupException e) {
			e.printStackTrace();
		}
		
		try {
			searchService.find(User.class,2L);
		} catch (LookupException e) {
			e.printStackTrace();
		}
		
	}
}
