package com.flowers.server.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;

import org.apache.commons.compress.utils.IOUtils;

public class ResizeImages {
	
	public ResizeImages() {} //Default Constructor

	byte[] createResizedCopy(InputStream in, boolean isTopAd) throws IOException {
	    	
	    	ImageOutputStream ios = null;
	    	ImageWriter writer = null;
	    	ImageReader reader = null;
	    	ByteArrayOutputStream os = new ByteArrayOutputStream();
	    	ImageInputStream iis = null;
	    	byte[] bytes = null;
	    	
	    	try {
	    		iis = ImageIO.createImageInputStream(in);
	    		Iterator<ImageReader> imageReaders = ImageIO.getImageReaders(iis);
	    		String format = imageReaders.next().getFormatName().toLowerCase();
	    		Iterator<ImageReader> iterator = ImageIO.getImageReaders(iis);
	    		if(iterator.hasNext()) {
			    	reader = iterator.next();
			    	reader.setInput(iis);
			    	IIOMetadata metadata = null;
			    	try {
			    		metadata = reader.getImageMetadata(0);
			    	}catch(Exception ee) {}
			
			    	BufferedImage bi = reader.read(0);
			    	double x = 0.0;
			    	if(isTopAd && (bi.getHeight() > 600 || bi.getWidth() > 3720))
			    		x = 0.25;
			    	else if(isTopAd && (bi.getHeight() > 300 || bi.getWidth() > 1860))
			    		x = 0.50;
			    	else if(isTopAd && (bi.getHeight() > 150 || bi.getWidth() > 930))
			    		x = 0.85;
			    	else if(bi.getHeight() > 2800 || bi.getWidth() > 3200)
						x = 0.25;
					else if(bi.getHeight() > 1400 || bi.getWidth() > 1600)
						x = 0.50;
					else if(bi.getHeight() > 700 || bi.getWidth() > 800)
						x = 0.85;
			    	else
			    		x = 0.99;
			    	AffineTransformOp scaleOp = new AffineTransformOp(AffineTransform.getScaleInstance(x, x), AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
			    	BufferedImage resultImage = scaleOp.createCompatibleDestImage(bi, null);
			    	resultImage = scaleOp.filter(bi, resultImage);
			    	ios = ImageIO.createImageOutputStream(os);
			
			    	Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName(format);
			    	writer = iter.next();
			    	writer.setOutput(ios);
			
			    	ImageWriteParam iwParam = writer.getDefaultWriteParam();
			    	if (iwParam instanceof JPEGImageWriteParam) {
			    	    ((JPEGImageWriteParam) iwParam).setOptimizeHuffmanTables(true);
			    	}
			    	writer.write(null, new IIOImage(resultImage, null, metadata), iwParam);
			    	
			    	iter = null;
			    	scaleOp = null;
			    	resultImage = null;
			    	metadata = null;
			    	imageReaders = null;
			    	format = null;
			    	
			    	bytes = os.toByteArray();
	    		}else {
	    			return IOUtils.toByteArray(in);
	    		}
	    	}catch(Exception ee) {
	    		return IOUtils.toByteArray(in);
	    	}finally {
	    		if(ios != null)
	    			ios.close();
	    		if(reader != null) {
	    			reader.dispose();
	    			reader = null;
	    		}
	    		if(writer != null) {
	    			writer.dispose();
	    			writer = null;
	    		}
	    		if(iis != null)
	    			iis.close();
	    		if(os != null)
	    			os.close();
	    	}
	        return bytes;
	    	 
	    }	
}
