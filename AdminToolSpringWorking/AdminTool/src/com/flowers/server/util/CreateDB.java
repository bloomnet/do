package com.flowers.server.util;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Locale;
import jxl.Cell;
import jxl.CellView;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;


public class CreateDB {

	private WritableCellFormat timesBold;
	private WritableCellFormat times;
	private String outputFile;
	private WorkbookSettings wbSettings;
	private File file;
	private WritableWorkbook workbook;
	SQLData mySQL = new SQLData();
	
	public CreateDB(File providedFile){
		
		
		setOutputFile(providedFile.getPath());
		GetExcelHeaders getHeaders = new GetExcelHeaders();
			LinkedList<String> headers;
			try {
				mySQL.login();
				mySQL.executeStatement("USE bloomnet;");
				headers = getHeaders.getHeaders("/CreateExcelFromDB/data/headers.xls", 0);
				LinkedList<String> headers2 = getHeaders.getHeaders("/CreateExcelFromDB/data/headers.xls", 1);
				writeDIRTAB("DIRTAB", 0, headers);
				writeDIRSORT("DIRSORT", 1, headers2);
				close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (WriteException e) {
				e.printStackTrace();
			}
		System.out.println("Success");				
	}
	
	public void setOutputFile(String outputFile) {
		this.outputFile = outputFile;
	}

	public void writeDIRTAB(String sheetName, int sheetNum, LinkedList<String> headers) throws IOException, WriteException {
		file = new File(outputFile);
		wbSettings = new WorkbookSettings();

		wbSettings.setLocale(new Locale("en", "EN"));

		workbook = Workbook.createWorkbook(file, wbSettings);
		workbook.createSheet(sheetName, sheetNum);
		WritableSheet excelSheet = workbook.getSheet(0);
		createLabel(excelSheet, headers);
		createContentDIRTAB(excelSheet);

	
	}
	
	public void writeDIRSORT(String sheetName, int sheetNum, LinkedList<String> headers)throws IOException, WriteException {
	
		workbook.createSheet(sheetName, sheetNum);
		WritableSheet excelSheet = workbook.getSheet(sheetNum);
		createLabel(excelSheet, headers);
		createContentDIRSORT(excelSheet);
		
		workbook.createSheet("FAC " + sheetName, sheetNum + 1);
		excelSheet = workbook.getSheet(sheetNum + 1);
		createLabel(excelSheet, headers);
		createContentFACDIRSORT(excelSheet);
	
		
	}

	private void createLabel(WritableSheet sheet, LinkedList<String> headers)
			throws WriteException {
		// Create a times font
		WritableFont times10pt = new WritableFont(WritableFont.TIMES, 10);
		// Define the cell format
		times = new WritableCellFormat(times10pt);
		// Automatically wrap the cells
		times.setWrap(true);

		// Create create a bold font for the headers
		WritableFont times10ptBold = new WritableFont(
				WritableFont.TIMES, 10, WritableFont.BOLD, false);
		timesBold = new WritableCellFormat(times10ptBold);
		// Automatically wrap the cells
		timesBold.setWrap(true);

		CellView cv = new CellView();
		cv.setFormat(times);
		cv.setFormat(timesBold);
		cv.setAutosize(true);

		// Write the headers
		for(int ii=0; ii<headers.size(); ++ii){
			
			addCaption(sheet, ii, 0, headers.get(ii));
			
		}

	}
	
	private void createContentDIRTAB(WritableSheet sheet) throws WriteException,
	RowsExceededException {

		int xx = 1;
		String query = "SELECT shop_code,city.name,shop_name,telephone_number,toll_free_number,fax_number,address.street_address1,"	+ 
							 		"contact_person,state.short_name,state.name,country.name,address.postal_code,"	+
									"open_sunday,bloomlink_indicator,"	+
									"new_shop,florist_for_forrests FROM bloomnet.shop INNER JOIN address ON " +
									"shop.address_id = address.id INNER JOIN city ON address.city_id = city.id "	+
							 		"INNER JOIN state ON city.state_id = state.id INNER JOIN country ON " +
									"state.country_idcountry = country.idcountry "	+
									"ORDER BY shop_code ASC;";
		try{
		
			ResultSet results = mySQL.executeQuery(query);
			//System.out.println(results.getString(1));

			while(results.next()){
				for (int ii = 0; ii < 12; ++ii) {
					addLabel(sheet, ii, xx, results.getString(ii+1));	
					}
				String shopCode = results.getString("shop_code");
				
				if(String.valueOf(results.getByte("open_sunday")).equals("1")) addLabel(sheet, 12, xx, "YES");
				if(String.valueOf(results.getByte("bloomlink_indicator")).equals("1")) addLabel(sheet, 13, xx, "YES");
				if(String.valueOf(results.getByte("new_shop")).equals("1")) addLabel(sheet, 23, xx, "YES");
				if(String.valueOf(results.getByte("florist_for_forrests")).equals("1")) addLabel(sheet, 24, xx, "YES");
				
				ResultSet min_prices = getPrices(shopCode);
		
				while(min_prices.next()){
					if(min_prices.getString(1).equals("ARRANGE")) addLabel(sheet, 14, xx, String.valueOf(min_prices.getDouble(2))+getFlux(results.getString("shop_code"),"1"));
					if(min_prices.getString(1).equals("BLOOM")) addLabel(sheet, 15, xx, String.valueOf(min_prices.getDouble(2))+getFlux(results.getString("shop_code"),"2"));
					if(min_prices.getString(1).equals("DOZEN")) addLabel(sheet, 16, xx, String.valueOf(min_prices.getDouble(2))+getFlux(results.getString("shop_code"),"3"));
					if(min_prices.getString(1).equals("FUNERAL")) addLabel(sheet, 17, xx, String.valueOf(min_prices.getDouble(2))+getFlux(results.getString("shop_code"),"4"));
					if(min_prices.getString(1).equals("BALLOON")) addLabel(sheet, 18, xx, String.valueOf(min_prices.getDouble(2))+getFlux(results.getString("shop_code"),"5"));
					if(min_prices.getString(1).equals("CANDY")) addLabel(sheet, 19, xx, String.valueOf(min_prices.getDouble(2))+getFlux(results.getString("shop_code"),"6"));
					if(min_prices.getString(1).equals("DRIED/SILK")) addLabel(sheet, 20, xx, String.valueOf(min_prices.getDouble(2))+getFlux(results.getString("shop_code"),"7"));
					if(min_prices.getString(1).equals("FRUIT")) addLabel(sheet, 21, xx, String.valueOf(min_prices.getDouble(2))+getFlux(results.getString("shop_code"),"8"));
					if(min_prices.getString(1).equals("GOURMET")) addLabel(sheet, 22, xx, String.valueOf(min_prices.getDouble(2))+getFlux(results.getString("shop_code"),"9"));
				}
				
				String numberFH = getNumberOfFacilities(shopCode, "4");
				String numberNH = getNumberOfFacilities(shopCode, "8");
				String numberHosp = getNumberOfFacilities(shopCode, "9");
				
				if(!numberFH.equals("0")) addLabel(sheet, 25, xx, numberFH);
				if(!numberHosp.equals("0")) addLabel(sheet, 26, xx, numberHosp);
				if(!numberNH.equals("0")) addLabel(sheet, 27, xx, numberNH);
				
				
				xx++;
			}
			results.close();
			mySQL.closeStatement();
		}catch(Exception ee){System.out.println(ee.getMessage());}	
	}
	
	private String getNumberOfFacilities(String shopCode, String facType) throws SQLException{

		String query = "SELECT COUNT(*) AS Number FROM shop_facility_xref " +
				"INNER JOIN facility ON shop_facility_xref.facility_id = facility.id " +
				"WHERE facility.facility_type_id = '"+facType+"' " +
				"AND shop_code = '"+shopCode+"';";
		
		ResultSet results = mySQL.executeQuery(query);
		
		String number = "";
		while(results.next()){
			
			number = results.getString("Number");
			
		}
		results.close();
		mySQL.closeStatement();
		
		return number;
	}

	private String getFlux(String shop_code, String prod_code) throws SQLException {
		String query = "SELECT fluctuate_price FROM bloomnet.minimum_price WHERE " +
				"shop_shop_code = '"+shop_code+"' AND product_category_id = '"+prod_code+"';";
		ResultSet results = mySQL.executeQuery(query);
		if(results.next()){
			if(String.valueOf(results.getByte(1)).equals("1")){
				results.close();
				mySQL.closeStatement();
				return "H";
			}
			else{
				results.close();
				mySQL.closeStatement();
				return "";
			}
		}
		else{
			results.close();
			mySQL.closeStatement();
			return "";
		}

	}

	private void createContentDIRSORT(WritableSheet sheet) throws WriteException,
			RowsExceededException {

		int xx = 1;
		String query = "SELECT listing.shop_code,shop.shop_name,city.name,entry_code,listing_type.name,rotate,rev_listing, "+
				 		"custom_listing,state.short_name,state.name,country.name,'adsize', "+
						"rev_ad,min_order, "+
						"delivery,'mac',listing.date,free_ad,block_bloomlink_zips,text_color,highlight_color, "+
						"video_color,image_count,image_count,cl8,ca9,bid,side_ad_entry_number,top_ad_entry_number,"+
						"side_ad_bid,seasonal_images,vday_background,spring_background,mday_background,summer_background,"+
						"fall_background,winter_background FROM bloomnet.listing INNER JOIN shop ON listing.shop_code = shop.shop_code "+
						"INNER JOIN city ON listing.city_id = city.id INNER JOIN listing_type ON listing.listing_type_id = listing_type.id "+
						"INNER JOIN state ON city.state_id = state.id INNER JOIN country ON state.country_idcountry = country.idcountry "+ 
						"WHERE listing_end_date > now() OR listing_end_date IS NULL " +
						"ORDER BY shop_code ASC;";
		try{
		
			ResultSet results = mySQL.executeQuery(query);
			
			while(results.next()){
				for (int ii = 0; ii < 5; ++ii) {
					addLabel(sheet, ii, xx, results.getString(ii+1));	
					}
				for (int ii = 7; ii < 13; ++ii) {
					if(ii == 11)addLabel(sheet, ii, xx, getAdSize(results.getString("shop_code"),results.getString("name")));
					else addLabel(sheet, ii, xx, results.getString(ii+1));
					}
				for (int ii = 15; ii < 22; ++ii) {
					if(ii == 15)addLabel(sheet, ii, xx, getMac(results.getString("shop_code"),results.getString("name")));
					else addLabel(sheet, ii, xx, results.getString(ii+1));
					}
				
				if(String.valueOf(results.getByte("rotate")).equals("1")) addLabel(sheet, 5, xx, "YES");
				if(String.valueOf(results.getByte("rev_listing")).equals("1")) addLabel(sheet, 6, xx, "YES");
				addLabel(sheet,13,xx,String.valueOf(results.getDouble(14)));
				addLabel(sheet,14,xx,String.valueOf(results.getDouble(15)));
				addLabel(sheet,22,xx,String.valueOf(results.getInt(23)));
				if(String.valueOf(results.getByte("cl8")).equals("1")) addLabel(sheet, 23, xx, "YES");
				if(String.valueOf(results.getByte("ca9")).equals("1")) addLabel(sheet, 24, xx, "YES");
				if(results.getString("bid") != null && !results.getString("bid").equals("null")) addLabel(sheet,25,xx,String.valueOf(results.getString("bid")));
				if(results.getString("side_ad_entry_number") != null && !results.getString("side_ad_entry_number").equals("null"))addLabel(sheet,26,xx,String.valueOf(results.getString("side_ad_entry_number")));
				if(results.getString("top_ad_entry_number") != null && !results.getString("top_ad_entry_number").equals("null"))addLabel(sheet,27,xx,String.valueOf(results.getString("top_ad_entry_number")));
				if(results.getString("side_ad_bid") != null && !results.getString("side_ad_bid").equals("null"))addLabel(sheet,28,xx,String.valueOf(results.getString("side_ad_bid")));
				if(String.valueOf(results.getByte("seasonal_images")).equals("1")) addLabel(sheet, 29, xx, "YES");
				if(results.getString("vday_background") != null && !results.getString("vday_background").equals("null"))addLabel(sheet,30,xx,String.valueOf(results.getString("vday_background")));
				if(results.getString("spring_background") != null && !results.getString("spring_background").equals("null"))addLabel(sheet,31,xx,String.valueOf(results.getString("spring_background")));
				if(results.getString("mday_background") != null && !results.getString("mday_background").equals("null"))addLabel(sheet,32,xx,String.valueOf(results.getString("mday_background")));
				if(results.getString("summer_background") != null && !results.getString("summer_background").equals("null"))addLabel(sheet,33,xx,String.valueOf(results.getString("summer_background")));
				if(results.getString("fall_background") != null && !results.getString("fall_background").equals("null"))addLabel(sheet,34,xx,String.valueOf(results.getString("fall_background")));
				if(results.getString("winter_background") != null && !results.getString("winter_background").equals("null"))addLabel(sheet,35,xx,String.valueOf(results.getString("winter_background")));
				
	
				xx++;
			}
			results.close();
			mySQL.closeStatement();
		}catch(Exception ee){System.out.println(ee.getMessage());}	
	}
	
	private void createContentFACDIRSORT(WritableSheet sheet) throws WriteException,
	RowsExceededException {

		int xx = 1;
		String query = "SELECT facility_listing.shop_code,shop.shop_name,city.name,entry_code,listing_type.name,rotate,rev_listing, "+
				 		"custom_listing,state.short_name,state.name,country.name,'adsize', "+
						"rev_ad,min_order, "+
						"delivery,'mac',facility_listing.date,free_ad,block_bloomlink_zips,text_color,highlight_color, "+
						"video_color,image_count,image_count,cl8,ca9,bid,side_ad_entry_number,top_ad_entry_number,"+
						"side_ad_bid,seasonal_images,vday_background,spring_background,mday_background,summer_background,"+
						"fall_background,winter_background FROM bloomnet.facility_listing INNER JOIN shop ON facility_listing.shop_code = shop.shop_code "+
						"INNER JOIN city ON facility_listing.city_id = city.id INNER JOIN listing_type ON facility_listing.listing_type_id = listing_type.id "+
						"INNER JOIN state ON city.state_id = state.id INNER JOIN country ON state.country_idcountry = country.idcountry "+ 
						"WHERE listing_end_date > now() OR listing_end_date IS NULL " +
						"ORDER BY shop_code ASC;";
		try{
		
			ResultSet results = mySQL.executeQuery(query);
			
			while(results.next()){
				for (int ii = 0; ii < 5; ++ii) {
					addLabel(sheet, ii, xx, results.getString(ii+1));	
					}
				for (int ii = 7; ii < 13; ++ii) {
					if(ii == 11)addLabel(sheet, ii, xx, getAdSize(results.getString("shop_code"),results.getString("name")));
					else addLabel(sheet, ii, xx, results.getString(ii+1));
					}
				for (int ii = 15; ii < 22; ++ii) {
					if(ii == 15)addLabel(sheet, ii, xx, getMac(results.getString("shop_code"),results.getString("name")));
					else addLabel(sheet, ii, xx, results.getString(ii+1));
					}
				
				if(String.valueOf(results.getByte("rotate")).equals("1")) addLabel(sheet, 5, xx, "YES");
				if(String.valueOf(results.getByte("rev_listing")).equals("1")) addLabel(sheet, 6, xx, "YES");
				addLabel(sheet,13,xx,String.valueOf(results.getDouble(14)));
				addLabel(sheet,14,xx,String.valueOf(results.getDouble(15)));
				addLabel(sheet,22,xx,String.valueOf(results.getInt(23)));
				if(String.valueOf(results.getByte("cl8")).equals("1")) addLabel(sheet, 23, xx, "YES");
				if(String.valueOf(results.getByte("ca9")).equals("1")) addLabel(sheet, 24, xx, "YES");
				if(results.getString("bid") != null && !results.getString("bid").equals("null")) addLabel(sheet,25,xx,String.valueOf(results.getString("bid")));
				if(results.getString("side_ad_entry_number") != null && !results.getString("side_ad_entry_number").equals("null"))addLabel(sheet,26,xx,String.valueOf(results.getString("side_ad_entry_number")));
				if(results.getString("top_ad_entry_number") != null && !results.getString("top_ad_entry_number").equals("null"))addLabel(sheet,27,xx,String.valueOf(results.getString("top_ad_entry_number")));
				if(results.getString("side_ad_bid") != null && !results.getString("side_ad_bid").equals("null"))addLabel(sheet,28,xx,String.valueOf(results.getString("side_ad_bid")));
				if(String.valueOf(results.getByte("seasonal_images")).equals("1")) addLabel(sheet, 29, xx, "YES");
				if(results.getString("vday_background") != null && !results.getString("vday_background").equals("null"))addLabel(sheet,30,xx,String.valueOf(results.getString("vday_background")));
				if(results.getString("spring_background") != null && !results.getString("spring_background").equals("null"))addLabel(sheet,31,xx,String.valueOf(results.getString("spring_background")));
				if(results.getString("mday_background") != null && !results.getString("mday_background").equals("null"))addLabel(sheet,32,xx,String.valueOf(results.getString("mday_background")));
				if(results.getString("summer_background") != null && !results.getString("summer_background").equals("null"))addLabel(sheet,33,xx,String.valueOf(results.getString("summer_background")));
				if(results.getString("fall_background") != null && !results.getString("fall_background").equals("null"))addLabel(sheet,34,xx,String.valueOf(results.getString("fall_background")));
				if(results.getString("winter_background") != null && !results.getString("winter_background").equals("null"))addLabel(sheet,35,xx,String.valueOf(results.getString("winter_background")));
				
				xx++;
			}
			results.close();
			mySQL.closeStatement();
		}catch(Exception ee){
			System.out.println(ee.getMessage());
		}	
	}
	
	
	
	private String getAdSize(String shop_code, String city_name) throws SQLException{

		String query = "SELECT ad_size.ad_size FROM bloomnet.listing "+
						"INNER JOIN ad_size ON listing.ad_size_id = ad_size.id "+ 
						"INNER JOIN city ON listing.city_id = city.id "+
						"WHERE shop_code = '"+shop_code+"' AND city.name = \""+city_name+"\";";
		
		ResultSet results = mySQL.executeQuery(query);
		if(results.next()){
			String result = results.getString(1);
			results.close();
			mySQL.closeStatement();
			return result;
		}
		else{
			results.close();
			mySQL.closeStatement();
			return "";
		}
		
	}
	
	private String getMac(String shop_code, String city_name) throws SQLException{

		String query = "SELECT mac.mac_name FROM bloomnet.listing "+
						"INNER JOIN mac ON listing.mac_id = mac.id "+ 
						"INNER JOIN city ON listing.city_id = city.id "+
						"WHERE shop_code = '"+shop_code+"' AND city.name = \""+city_name+"\";";
		
		ResultSet results = mySQL.executeQuery(query);
		if(results.next()){
			String result = results.getString(1);
			results.close();
			mySQL.closeStatement();
			return result;
		}
		else{
			results.close();
			mySQL.closeStatement();
			return "";
		}
		
	}
	
	
	private ResultSet getPrices(String shopCode) throws SQLException{
		
		String query = "SELECT product_category.name, minimum_price.minimum_price FROM bloomnet.minimum_price " +
				"INNER JOIN product_category ON " +
				"minimum_price.product_category_id = product_category.id WHERE shop_shop_code = '"+shopCode+"';";
		ResultSet results = mySQL.executeQuery(query);
		return results;
	}
	
	private void close() throws WriteException, IOException{
		workbook.write();
		workbook.close();
	}

	private void addCaption(WritableSheet sheet, int column, int row, String s)
			throws RowsExceededException, WriteException {
		Label label;
		label = new Label(column, row, s, timesBold);
		sheet.addCell(label);
	}

	@SuppressWarnings("unused")
	private void addNumber(WritableSheet sheet, int column, int row,
			Integer integer) throws WriteException, RowsExceededException {
		Number number;
		number = new Number(column, row, integer, times);
		sheet.addCell(number);
	}

	private void addLabel(WritableSheet sheet, int column, int row, String s)
			throws WriteException, RowsExceededException {
		Label label;
		label = new Label(column, row, s, times);
		sheet.addCell(label);
	}

}

class GetExcelHeaders {
	
	public GetExcelHeaders(){
	
	}

	public LinkedList<String> getHeaders(String inputFile, int sheetNum) throws IOException  {
		
		LinkedList<String> headers = new LinkedList<String>();
		File inputWorkbook = new File(inputFile);
		Workbook w;
		try {
			w = Workbook.getWorkbook(inputWorkbook);
			// Get the requested sheet
			Sheet sheet = w.getSheet(sheetNum);
			
			for (int ii = 0; ii < sheet.getColumns(); ++ii) {//get headers
				
				Cell cell = sheet.getCell(ii, 0);
				headers.add(String.valueOf(cell.getContents()));
		
			}
			
		} catch (BiffException e) {
			e.printStackTrace();
		}
		return headers;
	}


}

