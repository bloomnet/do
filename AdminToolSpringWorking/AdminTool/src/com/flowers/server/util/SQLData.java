package com.flowers.server.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;

public class SQLData {

    private Connection con;
    
    private String USERNAME = "";
    private String PASSWORD = "";
    private String ADDRESS = "";
    
    private PreparedStatement ps = null;
    
    private String userName = "";
    private String password = "";
       
    
    public SQLData(){
    	login();
    	String statement = "USE bloomnet;";
    	executeStatement(statement);
    }
    
    public void processData(LinkedList<String> myData) {
        login();
	if (isConnected()) {
            for (int ii = 0; ii < myData.size(); ++ii) {
                executeStatement(myData.get(ii));
               System.out.println(myData.get(ii));
            }
        }
    }

    public void login() {
    	
    	InputStream input = null;
		try {
			input = new FileInputStream("/opt/apps/properties/reports.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
    	Properties props = new Properties();
        try {
			props.load(input);
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    	this.userName = props.getProperty("dbuser");
    	this.password = props.getProperty("dbpass");
        setInfo(userName,password, props.getProperty("jdbc.default.root")+":3306");

    }

    public boolean setInfo(String USERNAME, String PASSWORD, String ADDRESS) {
        this.USERNAME = USERNAME;
        this.PASSWORD = PASSWORD;
        this.ADDRESS = ADDRESS;
        return startConnection();
    }

    public boolean startConnection() {
        try {
            new com.mysql.cj.jdbc.Driver();
        } catch (SQLException e) {
			e.printStackTrace();
		}
        try {
            con = DriverManager.getConnection("jdbc:mysql://" + ADDRESS,
                    USERNAME, PASSWORD);
            if (isConnected()) {
                return true;
            } else {
            	System.out.println("Connection Problem");
                return false;
            }
        } catch (SQLException ex) {
            return false;
        }
    }

    public boolean isConnected() {
        try {
            return !con.isClosed();
        } catch (Exception e) {
            System.err.println("isConnected Exception: " + e.getMessage());
            return false;
        }
    }

    public void executeStatement(String statement) {
        
	try {
		con.setAutoCommit(true);
	} catch (SQLException e1) {
		e1.printStackTrace();
	}
	try {
            Statement s = con.createStatement();
            s.executeUpdate(statement);
            s.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage()+"\n");
        } catch (Exception e) {
            System.err.println("sendUpdate( " + e.getMessage() + " )");
        }
    }
    
    public void executeParameterizedStatement(String statement, int numParams, List<String> params) {
        
    	try {
    		con.setAutoCommit(true);
    	} catch (SQLException e1) {
    		e1.printStackTrace();
    	}
    	try {
                PreparedStatement s = con.prepareStatement(statement);
                for(int ii=0; ii<numParams; ++ii)
                	s.setString(ii+1, params.get(ii));    
                System.out.println(s.toString());
                s.executeUpdate();
                s.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage()+"\n");
            } catch (Exception e) {
                System.err.println("sendUpdate( " + e.getMessage() + " )");
            }
        }
    
    public ResultSet executeParameterizedQuery(String query, int numParams, List<String> params) {
        try {
            ps = null;
            con.setAutoCommit(false);
            ps = con.prepareStatement(query);
            for(int ii=0; ii<numParams; ++ii){
            	ps.setString(ii+1, params.get(ii));
            }
            con.commit();
            ResultSet results = ps.executeQuery();
            return results;
        } catch (Exception e) {
            System.err.println("sendUpdate( " + e.getMessage() + " )");
            return null;
        }
    }

    public ResultSet executeQuery(String query) {
        try {
            ps = null;
            con.setAutoCommit(false);
            ps = con.prepareStatement(query);
            con.commit();
            ResultSet results = ps.executeQuery();
            return results;
        } catch (Exception e) {
            System.err.println("sendUpdate( " + e.getMessage() + " )");
            return null;
        }
    }
    
    public void closeStatement(){
    	try {
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
    
}
