package com.flowers.server;

//==============================================================================
// Import Declarations
//==============================================================================

/**
 * Returned when we couldnt find a domain instance.
 */

public class LookupException extends Exception {
	private static final long serialVersionUID = 7022833905637366753L;

	/**
	 * Constructor for wrapping internal exceptions.
	 *
	 * @param message string for human consumption
	 * @param cause   the original exception which caused this one
	 */

	public LookupException(final String message, final Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor for construction of a new persistence exception.
	 *
	 * @param message string for human consumption
	 */

	public LookupException(final String message) {
		super(message);
	}
}