package com.flowers.server.search;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.SortedDocValuesField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.util.BytesRef;
import org.hibernate.search.bridge.FieldBridge;
import org.hibernate.search.bridge.LuceneOptions;

import com.flowers.server.entity.State;

public class StateFieldBridge implements FieldBridge {

	@Override
	public void set(String name, Object value, Document document,
			LuceneOptions luceneOptions) {
		TextField field = null;
		StringField field2 = null;
		if (value instanceof State) {
			State state = (State) value;
			field = new TextField("tokenizedName", state.getName().toLowerCase(), Field.Store.YES);
			document.add(field);
			document.add(new SortedDocValuesField("name", new BytesRef(state.getName())));
			field2 = new StringField("country_id", String.valueOf(state.getCountry().getId()), Field.Store.YES);
			document.add(field2);
		}
	}

}
