/*
 * Copyright (C) 2011 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.flowers.server.search;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.hibernate.search.bridge.FieldBridge;
import org.hibernate.search.bridge.LuceneOptions;

import com.flowers.server.entity.City;
import com.flowers.server.entity.ConsumerResultsAd;
import com.flowers.server.entity.State;


public class ConsumerResultsAdFieldBridge implements FieldBridge {

	public void set(String name, Object value, Document document, LuceneOptions luceneOptions) {
		StringField field = null;
		if(value instanceof ConsumerResultsAd) {
			ConsumerResultsAd ad = (ConsumerResultsAd)value;
			if(ad.getCity() != null) {
				City city = ad.getCity();
				field = new StringField("city_id", String.valueOf(city.getId()), Field.Store.YES);
				document.add(field);
				field = new StringField("city", String.valueOf(city.getName()), Field.Store.YES);
				document.add(field);
				State state = city.getState();
				if(state != null) {
					field = new StringField("state_id", String.valueOf(state.getId()), Field.Store.YES);
					document.add(field);
					field = new StringField("state", state.getName(), Field.Store.YES);
					document.add(field);
					field = new StringField("state_code", state.getShortName(), Field.Store.YES);
					document.add(field);
				}
			
			}
		}
	}
}