/*
 * Copyright (C) 2011 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.flowers.server.search;

import java.util.Date;
import java.util.Set;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.SortedDocValuesField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.util.BytesRef;
import org.hibernate.search.bridge.FieldBridge;
import org.hibernate.search.bridge.LuceneOptions;

import com.flowers.server.entity.Address;
import com.flowers.server.entity.City;
import com.flowers.server.entity.Facility;
import com.flowers.server.entity.Listing;
import com.flowers.server.entity.LocalProducts;
import com.flowers.server.entity.MinimumPrice;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.ShopFacilityXref;
import com.flowers.server.entity.ShopProductCodificationXref;
import com.flowers.server.entity.SocialMedia;
import com.flowers.server.entity.State;
import com.flowers.server.entity.Zip;
import com.flowers.server.entity.ZipListing;

public class ListingFieldBridge implements FieldBridge {

	public void set(String name, Object value, Document document, LuceneOptions luceneOptions) {
		StringField field = null;
		if(value instanceof Listing) {
			Listing listing = (Listing)value;
			if(listing.getDelivery() != null) {
				field = new StringField("delivery_charge", String.valueOf(listing.getDelivery()), Field.Store.YES);
				document.add(field);
			}
			if(listing.getListingType() != null) {
				field = new StringField("listing_type", listing.getListingType().getName(), Field.Store.YES);
				document.add(field);
			}
			if(listing.getAdSize() != null) {
				field = new StringField("ad_size", listing.getAdSize().getAdSize(), Field.Store.YES);
				document.add(field);
				document.add(new SortedDocValuesField("ad_size", new BytesRef(listing.getAdSize().getAdSize())));
			}
			if(listing.getCustomListing() != null){
				field = new StringField("custom_listing", listing.getCustomListing(), Field.Store.YES);
				document.add(field);
			}
			if(listing.getCity() != null) {
				City city = listing.getCity();
				field = new StringField("city_id", String.valueOf(city.getId()), Field.Store.YES);
				document.add(field);
				field = new StringField("city", String.valueOf(city.getName()), Field.Store.YES);
				document.add(field);
				State state = city.getState();
				if(state != null) {
					field = new StringField("state_id", String.valueOf(state.getId()), Field.Store.YES);
					document.add(field);
					field = new StringField("state", state.getName(), Field.Store.YES);
					document.add(field);
					field = new StringField("state_code", state.getShortName(), Field.Store.YES);
					document.add(field);
				}
				StringBuffer buff = new StringBuffer();
				for(Zip zip : listing.getCity().getZips()) {
					buff.append(zip.getZipCode()+" ");
				}
				if(buff.length() > 1) {
					String finalBuff = buff.toString().trim();
					if(finalBuff.length() > 32000)
						finalBuff = finalBuff.substring(0, 32000);
					field = new StringField("zips", finalBuff, Field.Store.YES);
					document.add(field);
				}
			}
			Shop shop = listing.getShop();
			if(shop != null) {
				if(shop.getLatitude() != null) {
					field = new StringField("latitude", String.valueOf(shop.getLatitude()), Field.Store.YES);
					document.add(field);
				}
				if(shop.getLongitude() != null) {
					field = new StringField("longitude", String.valueOf(shop.getLongitude()), Field.Store.YES);
					document.add(field);
				}
				if(shop.getMinimum() != null) {
					field = new StringField("minimum", String.valueOf(shop.getMinimum()), Field.Store.YES);
					document.add(field);
				}
				field = new StringField("open_sunday", String.valueOf(shop.getOpenSunday()), Field.Store.YES);
				document.add(field);
				field = new StringField("shopcode", shop.getShopCode(), Field.Store.YES);
				document.add(field);
				if(shop.getContactPerson() != null) {
					field = new StringField("contact", shop.getContactPerson(), Field.Store.YES);
					document.add(field);
				}
				if(shop.getTelephoneNumber() != null) {
					field = new StringField("phone", shop.getTelephoneNumber(), Field.Store.YES);
					document.add(field);
				}
				if(shop.getTollFreeNumber() != null) {
					field = new StringField("phone800", shop.getTollFreeNumber(), Field.Store.YES);
					document.add(field);
				}
				if(shop.getFaxNumber() != null) {
					field = new StringField("fax", shop.getFaxNumber(), Field.Store.YES);
					document.add(field);
				}
				if(shop.getAddress() != null) {
					if(shop.getAddress().getCity() != null) {
						field = new StringField("shopcity", shop.getAddress().getCity().getName(), Field.Store.YES);
						document.add(field);
					}
					if(shop.getAddress().getState() != null) {
						field = new StringField("shopstate", shop.getAddress().getState().getShortName(), Field.Store.YES);
						document.add(field);
					}
				}
				if(shop.getShopName() != null) {
					field = new StringField("shopname", shop.getShopName(), Field.Store.YES);
					document.add(field);
				}
				if(shop.getShopProductCodificationXrefs().size() > 0) {
					StringBuffer buff = new StringBuffer();
					for(ShopProductCodificationXref product : shop.getShopProductCodificationXrefs()) {
						buff.append(product.getProductCodification().getId()+" ");
					}
					if(buff.length() > 0)
						field = new StringField("products", buff.toString().trim(), Field.Store.YES);
					else field = new StringField("products", "NA", Field.Store.YES);
					document.add(field);
				} else {
					field = new StringField("products", "NA", Field.Store.YES);
					document.add(field);
				}
				if(shop.getShopFacilityXrefs().size() > 0) {
					StringBuffer buff = new StringBuffer();
					StringBuffer buff2 = new StringBuffer();
					for(ShopFacilityXref facility : shop.getShopFacilityXrefs()) {
						Facility fac = facility.getFacility();
						buff.append(facility.getFacility().getId()+" ");
						buff2.append(fac.getName()+", "+fac.getAddress().getStreetAddress1()+", "+fac.getAddress().getCity().getName()+", "+fac.getAddress().getCity().getState().getShortName()+", "+fac.getAddress().getPostalCode()+ "----"+fac.getId()+"|");
					}
					String finalBuff = buff2.toString().trim();
					if(finalBuff.length() > 32000)
						finalBuff = finalBuff.substring(0, 32000);
					field = new StringField("facilities", buff.toString().trim(), Field.Store.YES);
					document.add(field);
					field = new StringField("facdata", finalBuff.toString().trim(), Field.Store.YES);
					document.add(field);
				}
				/** 9 Product Categories, enter 0 for missing values **/
				MinimumPrice[] prices = new MinimumPrice[10];
				if(shop.getMinimumPrices().size() > 0) {
					for(MinimumPrice price : shop.getMinimumPrices()) {
						Long id = price.getProductCategory().getId();
						if(id != null) {
							prices[id.intValue()] = price;
						}
					}
				}
				StringBuffer buff = new StringBuffer();
				for(int i=1; i < prices.length; i++) {
					if(prices[i] == null) buff.append("0.0 ");
					else if(prices[i].getFluctuatePrice()) buff.append(prices[i].getMinimumPrice()+"H ");
					else buff.append(String.valueOf(prices[i].getMinimumPrice())+" ");
				}
				field = new StringField("minimums", buff.toString().trim(), Field.Store.YES);
				document.add(field);
				
				Address address = shop.getAddress();
				if(address != null) {
					field = new StringField("zip", address.getPostalCode(), Field.Store.YES);
					document.add(field);
					if(address.getStreetAddress1() != null) {
						field = new StringField("address1", address.getStreetAddress1(), Field.Store.YES);
						document.add(field);
					}
					if(address.getStreetAddress2() != null) {
						field = new StringField("address2", address.getStreetAddress2(), Field.Store.YES);
						document.add(field);
					}
				}
				
				Set<SocialMedia> sml = shop.getSocialMedias();
				if(sml.size() > 0){
					SocialMedia sm = (SocialMedia) sml.toArray()[0];
					if(sm.getFacebook() != null && !sm.getFacebook().equals("")){
						field = new StringField("facebook", sm.getFacebook(), Field.Store.YES);
						document.add(field);
					}
					if(sm.getGoogleplus() != null && !sm.getGoogleplus().equals("")){
						field = new StringField("googleplus", sm.getGoogleplus(), Field.Store.YES);
						document.add(field);
					}
					if(sm.getLinkedin() != null && !sm.getLinkedin().equals("")){
						field = new StringField("linkedin", sm.getLinkedin(), Field.Store.YES);
						document.add(field);
					}
					if(sm.getTwitter() != null && !sm.getTwitter().equals("")){
						field = new StringField("twitter", sm.getTwitter(), Field.Store.YES);
						document.add(field);
					}
					if(sm.getInstagram() != null && !sm.getInstagram().equals("")){
						field = new StringField("instagram", sm.getInstagram(), Field.Store.YES);
						document.add(field);
					}
					if(sm.getPinterest() != null && !sm.getPinterest().equals("")){
						field = new StringField("pinterest", sm.getPinterest(), Field.Store.YES);
						document.add(field);
					}
					if(sm.getYelp() != null && !sm.getYelp().equals("")){
						field = new StringField("yelp", sm.getYelp(), Field.Store.YES);
						document.add(field);
					}
				}
				Set<ZipListing> zipListings = shop.getZipListings();
				buff = new StringBuffer();
				if(zipListings != null && zipListings.size() > 0){
					for(ZipListing zipListing : zipListings) {
						if(zipListing.getActive() == 1)
							buff.append(zipListing.getZipCode()+" ");
					}
					if(buff.length() > 1) {
						field = new StringField("zip_listings", buff.toString().trim(), Field.Store.YES);
						document.add(field);
					}
				}
				
				int totalWeight = 0;
				int numberFacListings = 0;
				int numberZipListings = 0;
				if(shop.getShopFacilityXrefs() != null)
					numberFacListings = shop.getShopFacilityXrefs().size();
				if(zipListings != null && zipListings.size() > 0)
					for(ZipListing zl : zipListings){
						if(zl.getActive() == 1)
							numberZipListings += 1;
					}
				Date today = new Date();
				for(Listing l : shop.getListings()){
					if(l.getAdSize() != null && l.getAdSize().getAdSize() != null && 
							(l.getAdStartDate() == null || l.getAdStartDate().equals("") || l.getAdStartDate().getTime() <= today.getTime()) && 
							(l.getAdEndDate() == null || l.getAdEndDate().equals("") || l.getAdEndDate().getTime() >= today.getTime())){
						if(l.getAdSize().getAdSize().equals("G"))
							totalWeight += 300;
						else if(l.getAdSize().getAdSize().equals("F"))
							totalWeight += 200;
						else if(l.getAdSize().getAdSize().equals("E"))
							totalWeight += 110;
						else if(l.getAdSize().getAdSize().equals("D"))
							totalWeight += 80;
						else if(l.getAdSize().getAdSize().equals("C"))
							totalWeight += 65;
					}
					if(l.getListingType() != null && l.getListingType().getName() != null && l.getListingType().getName().equalsIgnoreCase("X"))
						totalWeight += 15;
				}
				if(numberFacListings > 30)
					totalWeight += 150;
				else
					totalWeight += numberFacListings*5;
				
				totalWeight += numberZipListings*5;
				field = new StringField("weightedPlacement", String.valueOf(totalWeight), Field.Store.YES);
				document.add(field);
				
				if(shop.getShopName() != null) {
					field = new StringField("listing_country", listing.getCity().getState().getCountry().getShortName(), Field.Store.YES);
					document.add(field);
				}
				
				if(shop.getLocalProducts() != null) {
					String productsString = "";
					for(LocalProducts localProduct : shop.getLocalProducts()){
						productsString += localProduct.getProductName()+"--"+String.valueOf(localProduct.getPrice())+"--"+localProduct.getPictureFile()+",";
					}
					if(!productsString.equals(""))
						productsString = productsString.substring(0, productsString.length()-1);
					field = new StringField("local_products", productsString, Field.Store.YES);
					document.add(field);
				}
				
				if(shop.getShopWebsite() != null){
					field = new StringField("website", shop.getShopWebsite(), Field.Store.YES);
					document.add(field);
				}
				
				if(shop.getPrimaryWebsite() != null){
					field = new StringField("primary_website", String.valueOf(shop.getPrimaryWebsite()), Field.Store.YES);
					document.add(field);
				}
				
				if(shop.getShopLogo() != null){
					field = new StringField("logo", shop.getShopLogo(), Field.Store.YES);
					document.add(field);
				}
				
				if(shop.getConsumerImage() != null){
					field = new StringField("consumer_image", shop.getConsumerImage(), Field.Store.YES);
					document.add(field);
				}
				
				if(shop.getDontShowCDO() != null){
					field = new StringField("dont_show_cdo", String.valueOf(shop.getDontShowCDO()), Field.Store.YES);
					document.add(field);
				}
			
			}
		}
	}

}