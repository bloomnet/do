/*
 * Copyright (C) 2011 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.flowers.server.search;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.SortedDocValuesField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.util.BytesRef;
import org.hibernate.search.bridge.FieldBridge;
import org.hibernate.search.bridge.LuceneOptions;

import com.flowers.server.entity.Address;
import com.flowers.server.entity.Facility;
import com.flowers.server.entity.ShopFacilityXref;


public class FacilityFieldBridge implements FieldBridge {

	
	public void set(String name, Object value, Document document, LuceneOptions luceneOptions) {
		StringField field = null;
		if(value instanceof Facility) {
			Facility facility = (Facility)value;
			field = new StringField("typeid", String.valueOf(facility.getFacilityType().getId()), Field.Store.YES);
			document.add(field);
			field = new StringField("type", String.valueOf(facility.getFacilityType().getName()), Field.Store.YES);
			document.add(field);
			Address address = facility.getAddress();
			if(address != null) {
				if(address.getStreetAddress1() != null) {
					field = new StringField("address1", address.getStreetAddress1(), Field.Store.YES);
					document.add(field);
				}
				if(address.getStreetAddress2() != null) {
					field = new StringField("address2", address.getStreetAddress2(), Field.Store.YES);
					document.add(field);
				}
				if(address.getCity() != null) {
					field = new StringField("city_id", String.valueOf(address.getCity().getId()), Field.Store.YES);
					document.add(field);
					field = new StringField("city", address.getCity().getName(), Field.Store.YES);
					document.add(field);
					if(address.getCity().getState() != null) {
						field = new StringField("state", address.getCity().getState().getName(), Field.Store.YES);
						document.add(field);
						field = new StringField("state_code", address.getCity().getState().getShortName(), Field.Store.YES);
						document.add(field);
						field = new StringField("state_id", String.valueOf(address.getCity().getState().getId()), Field.Store.YES);
						document.add(field);
						if(address.getCity().getState().getCountry() != null) {
							field = new StringField("country", address.getCity().getState().getCountry().getName(), Field.Store.YES);
							document.add(field);
							field = new StringField("country_id", String.valueOf(address.getCity().getState().getCountry().getId()), Field.Store.YES);
							document.add(field);
						}
					}
				}
				if(address.getPostalCode() != null) {
					field = new StringField("zip", address.getPostalCode(), Field.Store.YES);
					document.add(field);
				}
			}
			if(facility.getShopFacilityXrefs().size() > 0) {
				StringBuffer buff = new StringBuffer();
				for(ShopFacilityXref shop : facility.getShopFacilityXrefs()) {
					buff.append(shop.getShop().getShopCode()+" ");
				}
				field = new StringField("shopcodes", buff.toString().trim(), Field.Store.YES);
				document.add(field);
				field = new StringField("hasshopcodes", "true", Field.Store.YES);
				document.add(field);
			}
			TextField field2 = new TextField("tokenizedName", facility.getName().toLowerCase(), Field.Store.YES);
			document.add(field2);
			document.add(new SortedDocValuesField("name", new BytesRef(facility.getName())));
		}
	}
}
