/*
 * Copyright (C) 2011 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.flowers.server.search;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.SortedDocValuesField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.util.BytesRef;
import org.hibernate.search.bridge.FieldBridge;
import org.hibernate.search.bridge.LuceneOptions;

import com.flowers.server.entity.City;
import com.flowers.server.entity.Zip;

public class CityFieldBridge implements FieldBridge {

	
	public void set(String name, Object value, Document document, LuceneOptions luceneOptions) {
		StringField field = null;
		TextField field2 = null;
		if(value instanceof City) {
			City city = (City)value;
			if(city.getZips().size() > 0) {
				StringBuffer buff = new StringBuffer();
				for(Zip zip : city.getZips()) {
					buff.append(zip.getZipCode()+" ");
				}
				field2 = new TextField("zips", buff.toString().trim(), Field.Store.YES);
				document.add(field2);
			}
			field2 = new TextField("tokenizedCityName", (city.getName() != null)?city.getName().toLowerCase():"", Field.Store.YES);
			document.add(field2);
			field = new StringField("cityName", (city.getName() != null)?city.getName():"", Field.Store.YES);
			document.add(field);
			field = new StringField("state_id", (String.valueOf(city.getState().getId()) != null) ? String.valueOf(city.getState().getId()):"", Field.Store.YES);
			document.add(field);
			field = new StringField("state_name", (city.getState().getName() != null)?city.getState().getName():"", Field.Store.YES);
			document.add(field);
			document.add(new SortedDocValuesField("cityName", new BytesRef(city.getName())));
		}
	}
} 
