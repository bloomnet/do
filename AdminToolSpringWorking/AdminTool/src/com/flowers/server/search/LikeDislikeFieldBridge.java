package com.flowers.server.search;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.hibernate.search.bridge.FieldBridge;
import org.hibernate.search.bridge.LuceneOptions;

import com.flowers.server.entity.LikeDislike;

public class LikeDislikeFieldBridge implements FieldBridge {

	
	public void set(String name, Object value, Document document, LuceneOptions luceneOptions) {
		StringField field = null;
		if(value instanceof LikeDislike) {
			LikeDislike likedislike = (LikeDislike)value;
			if(likedislike.getUsername() != null && likedislike.getShopCode() != null) {
				field = new StringField("id", String.valueOf(likedislike.getId()), Field.Store.YES);
				document.add(field);
				field = new StringField("username", likedislike.getUsername(), Field.Store.YES);
				document.add(field);
				field = new StringField("shop_code", likedislike.getShopCode(), Field.Store.YES);
				document.add(field);
				field = new StringField("liked", String.valueOf(likedislike.getLiked()), Field.Store.YES);
				document.add(field);
			}			
		}
	}
}
