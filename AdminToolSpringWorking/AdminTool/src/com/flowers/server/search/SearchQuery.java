package com.flowers.server.search;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;

import com.flowers.server.entity.MinimumPrice;
import com.flowers.server.entity.ProductCodification;

/**
 * This class represents all criteria that can be used on the searches.<br/>
 * <p>
 * All attributes on this class but type, will be used to store query filter
 * values, according to the query specified by 
 * {@link com.flowers.server.search.SearchQueryType SearchQueryType} value on
 * <i>type</i> attribute.
 * </p>
 * <p>
 * 
 * </p>
 * 
 * 
 * @author Fabio Cabral
 */
public class SearchQuery implements Serializable {
	private static final long serialVersionUID = -5438298102256627623L;
	/**
	 * Type of the search. @see com.flowers.server.search.SearchQueryType
	 */
	private SearchQueryType type;
	private int returnType = 1;
	
	//RECIPIENT
	private String firstName;
	private String lastName;
	//FLORIST_DETAIL
	private String date;
	private boolean openSunday;
	private String zip;
	private String state; //used on CITY, ZIP, FACILITY and FACILITY_TYPE search
	private String stateCode;
	private String city; //used on ZIP, FACILITY and FACILITY_TYPE search
	private String majorCity;
	private String phone;
	private String shopCode;
	private String shopName;
	private String facility;
	private List<ProductCodification> productCodifications = new ArrayList<ProductCodification>();
	private List<MinimumPrice> minimums = new ArrayList<MinimumPrice>();
	private String order; //used to order the results by default or distance options
	//FLORIST_DETAIL
	//VIDEOS
	//PHOTOS
	//FLORIST_PRODUCTS
	private int floristId;
	//FACILITY
	private String facilityTypeId;
	//CODIFIED_PRODUCT_DETAIL
	private long codifiedProductId;
	private String productCode;
	//PRODUCT_CATEGORY_DETAIL
	private long productCategoryId;
	//ZIP
	private String address1;
	private String address2;
	private String listingId;
	
	private int page = 0;
	private int pageSize = 0;
	
	private String destinationZip;
	private Double destinationLatitude;
	private Double destinationLongitude;
	
	private Query luceneQuery;
	private Sort luceneSort;
	
	public static final int RETURN_TYPE_DOCUMENT = 1;
	public static final int RETURN_TYPE_OBJECT = 2;	
	public static final int RETURN_TYPE_MAP = 3;
	
	//public SearchQuery() {}
	public SearchQuery(SearchQueryType type) {
		this.type = type;
	}
	public SearchQuery(SearchQueryType type, int returnType) {
		this.type = type;
		this.returnType = returnType;
	}
	
	public SearchQueryType getType() {
		return type;
	}
	public void setType(SearchQueryType type) {
		this.type = type;
	}
	public int getReturnType() {
		return returnType;
	}
	public void setReturnType(int returnType) {
		this.returnType = returnType;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public void setOpenSunday(boolean openSunday) {
		this.openSunday = openSunday;
	}
	public boolean isOpenSunday() {
		return openSunday;
	}
	public void setMajorCity(String majorCity) {
		this.majorCity = majorCity;
	}
	public String getMajorCity() {
		return majorCity;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPhone() {
		return phone;
	}
	public void setFacility(String facility) {
		this.facility = facility;
	}
	public String getFacility() {
		return facility;
	}
	public void setMinimums(List<MinimumPrice> minimums) {
		this.minimums = minimums;
	}
	public List<MinimumPrice> getMinimums() {
		return minimums;
	}
	public void setProductCodifications(List<ProductCodification> productCodifications) {
		this.productCodifications = productCodifications;
	}
	public List<ProductCodification> getProductCodifications() {
		return productCodifications;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public String getDestinationZip() {
		return destinationZip;
	}
	public void setDestinationZip(String destinationZip) {
		this.destinationZip = destinationZip;
	}
	public Double getDestinationLatitude() {
		return destinationLatitude;
	}
	public void setDestinationLatitude(Double destinationLatitude) {
		this.destinationLatitude = destinationLatitude;
	}
	public Double getDestinationLongitude() {
		return destinationLongitude;
	}
	public void setDestinationLongitude(Double destinationLongitude) {
		this.destinationLongitude = destinationLongitude;
	}
	public Query getLuceneQuery() {
		return luceneQuery;
	}
	public void setLuceneQuery(Query luceneQuery) {
		this.luceneQuery = luceneQuery;
	}
	public Sort getLuceneSort() {
		return luceneSort;
	}
	public void setLuceneSort(Sort luceneSort) {
		this.luceneSort = luceneSort;
	}
	public String getFacilityTypeId() {
		return facilityTypeId;
	}
	public void setFacilityTypeId(String facilityTypeId) {
		this.facilityTypeId = facilityTypeId;
	}
	public long getCodifiedProductId() {
		return codifiedProductId;
	}
	public void setCodifiedProductId(long codifiedProductId) {
		this.codifiedProductId = codifiedProductId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public long getProductCategoryId() {
		return productCategoryId;
	}
	public void setProductCategoryId(long productCategoryId) {
		this.productCategoryId = productCategoryId;
	}
	public String getListingId() {
		return listingId;
	}
	public void setListingId(String listingId) {
		this.listingId = listingId;
	}
	@Override
	public String toString() {
		return "SearchQuery [type=" + type + ", firstName=" + firstName + ", lastName=" + lastName + ", date=" + date + ", openSunday=" + openSunday + ", zip=" + zip + ", state=" + state + ", city=" + city + ", majorCity=" + majorCity + ", phone=" + phone + ", shopCode=" + shopCode + ", shopName="
				+ shopName + ", facility=" + facility + ", productCodifications=" + productCodifications + ", minimums=" + minimums + ", order=" + order + ", floristId=" + floristId + ", facilityTypeId=" + facilityTypeId + ", codifiedProductId=" + codifiedProductId + ", productCategoryId="
				+ productCategoryId + ", address1=" + address1 + ", address2=" + address2 + "]";
	}
}
