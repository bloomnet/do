package com.flowers.server.search;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.hibernate.search.bridge.FieldBridge;
import org.hibernate.search.bridge.LuceneOptions;

import com.flowers.server.entity.Address;
import com.flowers.server.entity.City;
import com.flowers.server.entity.MinimumPrice;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.ShopFacilityXref;
import com.flowers.server.entity.ShopProductCodificationXref;
import com.flowers.server.entity.State;

public class ShopFieldBridge implements FieldBridge {

	public void set(String name, Object value, Document document, LuceneOptions luceneOptions) {
		StringField field = null;
		if(value instanceof Shop) {
			Shop shop = (Shop)value;
			Address address = shop.getAddress();
			if(address != null) {
				if(address.getPostalCode() != null){
					field = new StringField("zip", address.getPostalCode(), Field.Store.YES);
					document.add(field);
				}
				City city = address.getCity();
				if(city != null) {
					field = new StringField("city_id", String.valueOf(city.getId()), Field.Store.YES);
					document.add(field);
					field = new StringField("city", String.valueOf(city.getName()), Field.Store.YES);
					document.add(field);
					State state = city.getState();
					if(state != null) {
						field = new StringField("state_id", String.valueOf(state.getId()), Field.Store.YES);
						document.add(field);
						field = new StringField("state", state.getName(), Field.Store.YES);
						document.add(field);
						field = new StringField("state_code", state.getShortName(), Field.Store.YES);
						document.add(field);
						if(state.getCountry() != null) {
							field = new StringField("country_id", String.valueOf(state.getCountry().getId()), Field.Store.YES);
							document.add(field);
							field = new StringField("country", state.getCountry().getShortName(), Field.Store.YES);
							document.add(field);
						}
					}
				}
				if(address.getStreetAddress1() != null) {
					field = new StringField("address1", address.getStreetAddress1(), Field.Store.YES);
					document.add(field);
				}
				if(address.getStreetAddress2() != null) {
					field = new StringField("address2", address.getStreetAddress2(), Field.Store.YES);
					document.add(field);
				}
			}
			if(shop.getShopProductCodificationXrefs().size() > 0) {
				StringBuffer buff = new StringBuffer();
				for(ShopProductCodificationXref product : shop.getShopProductCodificationXrefs()) {
					buff.append(product.getProductCodification().getId()+" ");
				}
				field = new StringField("products", buff.toString().trim(), Field.Store.YES);
				document.add(field);
			}
			if(shop.getShopFacilityXrefs().size() > 0) {
				StringBuffer buff = new StringBuffer();
				for(ShopFacilityXref facility : shop.getShopFacilityXrefs()) {
					buff.append(facility.getFacility().getId()+" ");
				}
				field = new StringField("facilities", buff.toString().trim(), Field.Store.YES);
				document.add(field);
			}
			if(shop.getMinimumPrices().size() > 0) {
				for(MinimumPrice price : shop.getMinimumPrices()) {
					Long id = price.getProductCategory().getId();
					if(price.getFluctuatePrice()) field = new StringField("price"+id, price.getMinimumPrice()+"H", Field.Store.YES);
					else field = new StringField("price"+id, String.valueOf(price.getMinimumPrice()), Field.Store.YES);
					document.add(field);
				}
			}
			
			if(shop.getShopProductCodificationXrefs().size() > 0) {
				
			}
			field = new StringField("tokenizedShopCode", shop.getShopCode(), Field.Store.YES);
			document.add(field);
		}
	}

}
