package com.flowers.server.search;

import java.util.Comparator;
import java.util.Random;

import org.apache.lucene.document.Document;

public class DistanceSortComparator implements Comparator<Document> {

	
	public int compare(Document doc1, Document doc2) {
		String adSize1 = doc1.get("ad_size");
		String adSize2 = doc2.get("ad_size");
		String distance1 = doc1.get("distance");
		String distance2 = doc2.get("distance");
		
		if(adSize1 == null && adSize2 == null) {
			if(distance1 == null) {
		    	if(distance2 == null) {
		    		return 0;
		        }
		        return 1;
		    } else if(distance2 == null) {
		    	return -1;
		    }
			Integer dist1 = Integer.valueOf(distance1);
			Integer dist2 = Integer.valueOf(distance2);
		    return dist1.compareTo(dist2);
		} else if(adSize1 != null) {
			if(adSize2 != null) {
				if(adSize1.equals(adSize2)) {
					Random rand = new Random();
					Integer i1 = rand.nextInt();
					Integer i2 = rand.nextInt();
					return i1.compareTo(i2);
				}
				return adSize2.compareTo(adSize1);
		    }
		    return -1;
		} else if(adSize2 != null) {
			return 1;
		}
		return 0;
	}

}
