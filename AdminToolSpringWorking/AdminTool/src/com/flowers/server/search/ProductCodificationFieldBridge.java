package com.flowers.server.search;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.SortedDocValuesField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.util.BytesRef;
import org.hibernate.search.bridge.FieldBridge;
import org.hibernate.search.bridge.LuceneOptions;

import com.flowers.server.entity.ProductCodification;

public class ProductCodificationFieldBridge implements FieldBridge {

	@Override
	public void set(String name, Object value, Document document, LuceneOptions luceneOptions) {
		TextField field = null;
		if(value instanceof ProductCodification) {
			ProductCodification productCodification = (ProductCodification)value;
			field = new TextField("tokenizedName", productCodification.getName(), Field.Store.YES);
			document.add(field);
			document.add(new SortedDocValuesField("name", new BytesRef(productCodification.getName())));
		}
	}

}
