package com.flowers.server.search;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.SortedDocValuesField;
import org.apache.lucene.util.BytesRef;
import org.hibernate.search.bridge.FieldBridge;
import org.hibernate.search.bridge.LuceneOptions;

import com.flowers.server.entity.FruitBouquet;

public class FruitBoquetFieldBridge implements FieldBridge {
	
	public void set(String name, Object value, Document document, LuceneOptions luceneOptions) {
		if(value instanceof FruitBouquet) {
			FruitBouquet fbq = (FruitBouquet)value;
			document.add(new SortedDocValuesField("product_code", new BytesRef(fbq.getProductCode())));
		}
	}

}
