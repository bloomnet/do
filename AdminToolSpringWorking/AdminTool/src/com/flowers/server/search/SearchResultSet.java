package com.flowers.server.search;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.Query;
import com.fasterxml.jackson.annotation.JsonIgnore;

import com.flowers.server.entity.Address;


public class SearchResultSet<T> implements Serializable {
	private static final long serialVersionUID = -575570194601296400L;
	//private int resultSize;
	private int pageNumber;
	private String queryExplanation;
	private SearchQuery searchQuery;
	private Query luceneQuery;
	private long time;
	private Address destination;
	private List<T> results = new ArrayList<T>();
	private List<List<Document>> pages = new ArrayList<List<Document>>();
	private List<String> availableShopCodes = new ArrayList<String>();
	private int pagesVisited;
	private String username;
	private String message = "";
	private long orderId;
	private long logId;
	
	public SearchResultSet() {}
	public SearchResultSet(SearchQuery searchQuery) {
		this.searchQuery = searchQuery;
	}
	
	public T getResultById(String id) {
		if(results.size() > 0 && results.get(0) instanceof Document) {
			for(T doc : results) {
				Document d = (Document)doc;
				if(d.get("id") != null && d.get("id").equals(id))
					return doc;
			}
		}
		return null;
	}
	public int getResultSize() {
		return results.size();
	}
	public void setResultSize(int resultSize) {
		
	}
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public int getPageCount() {
		return pages.size();
	}
	public void setPageCount(int pageCount) {
		//no op
	}
	public String getQueryExplanation() {
		return queryExplanation;
	}
	public void setQueryExplanation(String queryExplanation) {
		this.queryExplanation = queryExplanation;
	}
	@JsonIgnore
	public SearchQuery getSearchQuery() {
		return searchQuery;
	}
	public void setSearchQuery(SearchQuery searchQuery) {
		this.searchQuery = searchQuery;
	}
	@JsonIgnore
	public Query getLuceneQuery() {
		return luceneQuery;
	}
	public void setLuceneQuery(Query luceneQuery) {
		this.luceneQuery = luceneQuery;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	public Address getDestination() {
		return destination;
	}
	public void setDestination(Address destination) {
		this.destination = destination;
	}
	public T getResult(int index) {
		return results.get(index);
	}
	public List<T> getResults() {
		return results;
	}
	public void setResults(List<T> results) {
		this.results = results;
	}
	public List<List<Document>> getPages() {
		return pages;
	}
	public void setPages(List<List<Document>> pages) {
		this.pages = pages;
	}
	public List<String> getAvailableShopCodes() {
		return availableShopCodes;
	}
	public void setAvailableShopCodes(List<String> availableShopCodes) {
		this.availableShopCodes = availableShopCodes;
	}
	public int getPagesVisited() {
		return pagesVisited;
	}
	public void setPagesVisited(int pagesVisited) {
		this.pagesVisited = pagesVisited;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public long getOrderId() {
		return orderId;
	}
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	public long getLogId() {
		return logId;
	}
	public void setLogId(long logId) {
		this.logId = logId;
	}
		
}
