package com.flowers.server.search;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.SortedDocValuesField;
import org.apache.lucene.util.BytesRef;
import org.hibernate.search.bridge.FieldBridge;
import org.hibernate.search.bridge.LuceneOptions;

import com.flowers.server.entity.ProductCategory;

public class ProductCategoryFieldBridge implements FieldBridge{
	
	public void set(String name, Object value, Document document, LuceneOptions luceneOptions) {
		if(value instanceof ProductCategory) {
			ProductCategory productCategory = (ProductCategory)value;
			document.add(new SortedDocValuesField("name", new BytesRef(productCategory.getName())));
		}
	}

}
