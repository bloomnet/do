package com.flowers.server.search;

/**
 * 
 * <p>
 * Enum type to define all possible queries that will use {@link com.flowers.server.search.SearchQuery SearchQuery} 
 * object.
 * </p>
 * <p>
 * Queries:
 * </p>
 * <ul>
 * 	<li>STATE: will be used to search all states</li>
 * 	<li>CITY: will be used to search all cities from a specified state</li>
 * 	<li>MAJORCITY: will be used to search all major cities</li>
 * 	<li>SHOP: will be used to search for shops. Query parameters will be filled with
 *      values from <i>Search for a florist</i> screen</li>
 *  <li>SHOP_PRODUCTS: will be used to perform a list of codified products from a specified shop</li>
 *  <li>SHOP_DETAIL: will be used to perform a detailed search from a specified shop</li>
 *  <li>SHOP_MINIMUM_PRICES: will be used to list all product categories and minimum prices from a specified shop</li>
 *  <li>VIDEOS: will be used to list all videos from a specified shop</li>
 *  <li>PHOTOS: will be used to list all photos from a specified shop</li>
 *  <li>SPONSORED_ADS: will be used to list all sponsored ads</li>
 *  <li>FACILITY_TYPE: will be used to list all facility types from a specified location information</li>
 *  <li>FACILITY: will be used to list all facilities from a specified location information and facility type</li>
 *  <li>OCCASION: will be used to list all occasions</li>
 *  <li>CODIFIED_PRODUCT: will be used to list all codified products from a specified shop</li>
 *  <li>CODIFIED_PRODUCT_DETAIL: will be used to get codified product details from a specified codified product id</li>
 *  <li>CODIFIED_PRODUCT_PHOTOS: will be used to get codified product photos from a specified codified product id</li>
 *  <li>RECIPIENT: will be used to get an address information from a specified first/last name</li>
 *  <li>ZIP: will be used to get ZIP information from a specified address information</li>
 *  <li>PRODUCT_CATEGORY: will be used to get a product category list</li>
 *  <li>PRODUCT_CATEGORY_DETAIL: will be used to get a product category from a specified ID</li>
 * </ul>
 * 
 * 
 * @author Fabio Cabral
 *
 */
public enum SearchQueryType {
	STATE, 
	CITY,
	CITIES,
	MAJORCITY, 
	LISTING,
	LISTINGS,
	SHOP_DETAIL,
	SHOP_PRODUCTS,
	SHOP_MINIMUM_PRICES, 
	VIDEOS, 
	PHOTOS, 
	SPONSORED_ADS, 
	FACILITY_TYPE,
	FACILITY, 
	OCCASION, 
	CODIFIED_PRODUCT, 
	CODIFIED_PRODUCT_DETAIL,
	CODIFIED_PRODUCT_PHOTOS,
	DROPSHIP,
	RECIPIENT, 
	ZIP,
	PRODUCT_CATEGORY,
	PRODUCT_CATEGORY_DETAIL,
	CODIFIED_PRODUCT_DETAIL_BY_CODE
}
