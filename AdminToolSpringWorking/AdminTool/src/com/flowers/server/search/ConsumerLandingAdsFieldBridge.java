package com.flowers.server.search;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.SortedDocValuesField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.util.BytesRef;
import org.hibernate.search.bridge.FieldBridge;
import org.hibernate.search.bridge.LuceneOptions;

import com.flowers.server.entity.ConsumerLandingAd;

public class ConsumerLandingAdsFieldBridge implements FieldBridge {

	public void set(String name, Object value, Document document, LuceneOptions luceneOptions) {
		StringField field = null;
		if(value instanceof ConsumerLandingAd) {
			ConsumerLandingAd ad = (ConsumerLandingAd)value;
			if(ad.getFileName() != null) {
				field = new StringField("file_name", ad.getFileName(), Field.Store.YES);
				document.add(field);
				document.add(new SortedDocValuesField("file_name", new BytesRef(ad.getFileName())));
			
			}
		}
	}
}
