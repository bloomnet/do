package com.flowers.server.service;

import java.util.List;
import java.util.Map;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.Query;

import com.flowers.server.LookupException;
import com.flowers.server.search.SearchQuery;
import com.flowers.server.search.SearchResultSet;

/**
 * Bloomnet service for searching. Searching framework is Hibernate Search.
 * @author Fabio Cabral
 */
public interface SearchService extends SearchServiceSupport {
	public static final int BATCH_SIZE = 100;
	/**
	 * Search objects from database/lucene index
	 * @param searchQuery
	 * @return
	 */
	public <T> SearchResultSet<T> search(SearchQuery searchQuery);
	
	/** Helper functions **/
	public Map<String, Document> fetchProducts();
	public Map<String, Document> fetchCategories();
	public List<Document> fetchFacilities(String[] ids);
	
	void index(Class<?> clazz) throws LookupException;
	void index(Class<?> clazz, int page);
	void index(Class<?> clazz, Long id);
	void index(Class<?> clazz, List<Long> ids);
	void index(Object obj);
	void purge(Class<?> clazz);
	void optimize(Class<?> clazz);
	
	Long count(Class<?> clazz);
	
	Document document(Class<?> clazz, String id);
	Document document(Class<?> clazz, Query query);
}
