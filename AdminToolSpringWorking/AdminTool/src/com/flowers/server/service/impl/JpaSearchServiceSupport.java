package com.flowers.server.service.impl;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.SortField;
import org.hibernate.CacheMode;
import org.hibernate.search.MassIndexer;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.springframework.transaction.annotation.Transactional;

import com.flowers.server.LookupException;
import com.flowers.server.entity.MinimumPrice;
import com.flowers.server.search.DistanceSortComparator;
import com.flowers.server.search.SearchQuery;
import com.flowers.server.search.SearchResultSet;
import com.flowers.server.service.SearchServiceSupport;
import com.flowers.server.util.DistanceCalculationUtil;


public class JpaSearchServiceSupport extends JpaServiceSupport implements SearchServiceSupport {
	protected final Log log = LogFactory.getLog(getClass());
	private DistanceCalculationUtil calculator = new DistanceCalculationUtil();
	
	public static final Double MAX_DISTANCE = 5000.00;
	
	private static List<String> fieldExclusions = new ArrayList<String>();
	static {
		fieldExclusions.add("_hibernate_class");
	}
	
	@SuppressWarnings("rawtypes")
	public Document document(Class clazz, String id) {
		FullTextEntityManager searchManager = null;
		try {
			searchManager = Search.getFullTextEntityManager(em);
			MultiFieldQueryParser parser = new MultiFieldQueryParser(new String[]{"id"}, new StandardAnalyzer());
			String query = "id:"+id;
			FullTextQuery hibQuery = searchManager.createFullTextQuery(parser.parse(query), clazz);
			hibQuery.setProjection("DOCUMNET");
			Object[] result = (Object[])hibQuery.getSingleResult();
			return (Document)result[0];
		} catch(Exception e) {
			log.debug("no entity found id:"+id);
		}finally{
			if(searchManager != null)
				searchManager.clear();
		}
		return null;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> documents(Class clazz, Query query, SortField sort) {
		FullTextEntityManager searchManager = null;
		try {
			searchManager = Search.getFullTextEntityManager(em);
			FullTextQuery hibQuery = searchManager.createFullTextQuery(query, clazz);
			hibQuery.setProjection(FullTextQuery.DOCUMENT);			
			List<Object> results1 = hibQuery.getResultList();
			List<Document> results = new ArrayList<Document>();
			for(Object o : results1) {
				if(o instanceof Object[]) {
					results.add((Document)((Object[])o)[0]);
				}
			}
			return results;
		} catch(Exception e) {
			log.debug("no entity found for query");
		}finally{
			if(searchManager != null)
				searchManager.clear();
		}
		return null;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T> SearchResultSet<T> objects(Class clazz, SearchQuery query) {
		FullTextEntityManager searchManager = null;
		try {
			long start = System.currentTimeMillis();
			searchManager = Search.getFullTextEntityManager(em);
			FullTextQuery hibQuery = searchManager.createFullTextQuery(query.getLuceneQuery(), clazz);
			if(query.getLuceneSort() != null) hibQuery.setSort(query.getLuceneSort());
			if(query.getPage() != 0) hibQuery.setFirstResult((query.getPage()*query.getPageSize())-query.getPageSize());
			if(query.getPageSize() != 0 && query.getPage() != 0) hibQuery.setMaxResults(query.getPageSize());
			List<Object> results1 = hibQuery.getResultList();
			SearchResultSet results = new SearchResultSet();
			for(Object o : results1) {
				results.getResults().add((T)o);
			}
			results.setResultSize(hibQuery.getResultSize());
			long end = System.currentTimeMillis();
			long time = (end-start);
			results.setTime(time);
			return results;
		} catch(Exception e) {
			e.printStackTrace();
		}finally{
			if(searchManager != null)
				searchManager.clear();
		}
		return null;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T> SearchResultSet<T> maps(Class clazz, SearchQuery query) {
		FullTextEntityManager searchManager = null;
		try {
			long start = System.currentTimeMillis();
			searchManager = Search.getFullTextEntityManager(em);
			FullTextQuery hibQuery = searchManager.createFullTextQuery(query.getLuceneQuery(), clazz);
			if(query.getLuceneSort() != null) hibQuery.setSort(query.getLuceneSort());
			if(query.getPage() != 0) hibQuery.setFirstResult((query.getPage()*query.getPageSize())-query.getPageSize());
			if(query.getPageSize() != 0 && query.getPage() != 0) hibQuery.setMaxResults(query.getPageSize());
			hibQuery.setProjection(FullTextQuery.DOCUMENT);
			List<Object> results1 = hibQuery.getResultList();
			SearchResultSet results = new SearchResultSet();
			for(Object o : results1) {
				if(o instanceof Object[]) {
					Document doc = (Document)((Object[])o)[0];
					Map<String, Object> map = new HashMap<String, Object>();
					for(Object obj : doc.getFields()) {
						if(obj instanceof Field) {
							Field field = (Field)obj;
							if(!fieldExclusions.contains(field.name())) map.put(field.name(), field.stringValue());
						}
					}
					results.getResults().add(map);
				}
			}
			results.setResultSize(hibQuery.getResultSize());
			results.setPageNumber(query.getPage());
			if(query.getPageSize() == 0 || results.getResultSize() < query.getPageSize()) results.setPageCount(1);
			else {
				double ratio = (double)results.getResultSize() / query.getPageSize();
				results.setPageCount((int)(Math.ceil(ratio)));
			}
			results.setQueryExplanation(query.getLuceneQuery().toString());
			long end = System.currentTimeMillis();
			long time = (end-start);
			results.setTime(time);
			return results;
		} catch(Exception e) {
			e.printStackTrace();
		}finally{
			if(searchManager != null)
				searchManager.clear();
		}
		return null;
	}
	@SuppressWarnings("rawtypes")
	public Document document(Class clazz, Query query) {
		FullTextEntityManager searchManager = null;
		try {
			searchManager = Search.getFullTextEntityManager(em);
			FullTextQuery hibQuery = searchManager.createFullTextQuery(query, clazz);
			hibQuery.setProjection(FullTextQuery.DOCUMENT);
			Object obj = hibQuery.getSingleResult();
			if(obj instanceof Object[]) {
				Document doc = (Document)((Object[])obj)[0];
				return doc;
			}
			return (Document)obj;
		} catch(Exception e) {
			log.debug("no entity found for query");
		}finally{
			if(searchManager != null)
				searchManager.clear();
		}
		return null;
	} 
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T> SearchResultSet<T> documents(Class clazz, SearchQuery query) {
		FullTextEntityManager searchManager = null;
		try {
			long start = System.currentTimeMillis();
			searchManager = Search.getFullTextEntityManager(em);
			FullTextQuery hibQuery = searchManager.createFullTextQuery(query.getLuceneQuery(), clazz);
			if(query.getLuceneSort() != null) hibQuery.setSort(query.getLuceneSort());
			if(query.getPage() != 0) hibQuery.setFirstResult((query.getPage()*query.getPageSize())-query.getPageSize());
			if(query.getPageSize() != 0 && query.getPage() != 0) hibQuery.setMaxResults(query.getPageSize());
			hibQuery.setProjection(FullTextQuery.DOCUMENT);
			List<Object> results1 = hibQuery.getResultList();
			SearchResultSet results = new SearchResultSet();
			for(Object o : results1) {
				if(o instanceof Object[]) {
					Document doc = (Document)((Object[])o)[0];
					results.getResults().add((T)doc);
				}
			}
			results.setResultSize(hibQuery.getResultSize());
			long end = System.currentTimeMillis();
			long time = (end-start);
			results.setTime(time);
			return results;
		} catch(Exception e) {
			e.printStackTrace();
		}finally{
			if(searchManager != null)
				searchManager.clear();
		}
		return null;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T> SearchResultSet<T> listings(Class clazz, SearchQuery query) {
		FullTextEntityManager searchManager = null;
		try {
			long start = System.currentTimeMillis();
			searchManager = Search.getFullTextEntityManager(em);
			FullTextQuery hibQuery = searchManager.createFullTextQuery(query.getLuceneQuery(), clazz);
			if(query.getLuceneSort() != null) hibQuery.setSort(query.getLuceneSort());
			if(query.getPage() != 0) hibQuery.setFirstResult((query.getPage()*query.getPageSize())-query.getPageSize());
			if(query.getPageSize() != 0 && query.getPage() != 0) hibQuery.setMaxResults(query.getPageSize());
			hibQuery.setProjection(FullTextQuery.DOCUMENT);
			List<Object> results1 = hibQuery.getResultList();
			SearchResultSet results = new SearchResultSet(query);
			List<String> shopCodes = new ArrayList<String>();
			for(Object o : results1) {
				if(o instanceof Object[]) {
					Document doc = (Document)((Object[])o)[0];
					
					if(query.getDestinationLatitude() != null && query.getDestinationLongitude() != null && doc.get("latitude") != null && doc.get("longitude") != null) {
						Double latititude = Double.valueOf(doc.get("latitude"));
						Double longitude = Double.valueOf(doc.get("longitude"));
						double distance = calculator.calculate(query.getDestinationLatitude(), query.getDestinationLongitude(), latititude, longitude, DistanceCalculationUtil.CalculationType.MILES);
						if(distance <= MAX_DISTANCE) {
							doc.add(new StringField("distance", String.valueOf((int)distance), Field.Store.YES));
						} else continue;
					}
					/** 
					 * We check if the shopCode is already included, if it's a duplicate then remove it.
					 */
					if(!shopCodes.contains(doc.get("shopcode"))) {						
						if(query.getMinimums().size() > 0) {
							boolean holiday = inHolidayPeriod();					
							String minimums = doc.get("minimums");
							if(minimums != null) {
								String[] mins = minimums.split(" ");
								for(MinimumPrice price : query.getMinimums()) {
									String min = mins[price.getId().intValue()];
									Double minDouble = null;
									if(min != null && !min.equals("")) {
										if(min.endsWith("H")) {
											if(holiday)	
												minDouble = Double.valueOf(min.substring(0, min.length()-1));
										} else
											minDouble = Double.valueOf(min);
										if(minDouble == null || minDouble <= price.getMinimumPrice()) {
											shopCodes.add(doc.get("shopcode"));
											results.getResults().add((T)doc);
											if(doc.get("ad_size") != null) {
												Document clone = cloneDocument(doc);
												results.getResults().add((T)clone);
											}
										}
									}	
								}
							}
						} else {
							shopCodes.add(doc.get("shopcode"));
							results.getResults().add((T)doc);
							if(doc.get("ad_size") != null) {
								Document clone = cloneDocument(doc);
								results.getResults().add((T)clone);
							}
						}
					}
				}
			}
			Collections.sort(results.getResults(), new DistanceSortComparator());
			results.setResultSize(hibQuery.getResultSize());
			long end = System.currentTimeMillis();
			long time = (end-start);
			results.setTime(time);
			return results;
		} catch(Exception e) {
			e.printStackTrace();
		}finally{
			if(searchManager != null)
				searchManager.clear();
		}
		return null;
	}
	@SuppressWarnings("rawtypes")
	public <T> SearchResultSet<T> facilities(Class clazz, SearchQuery query) {
		
		return new SearchResultSet<T>();
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> documents(Class clazz, String[] ids) {
		FullTextEntityManager searchManager = null;
		try {
			searchManager = Search.getFullTextEntityManager(em);
			MultiFieldQueryParser parser = new MultiFieldQueryParser(new String[]{"id"}, new StandardAnalyzer());
			StringBuffer query = new StringBuffer();
			for(int i=0; i < ids.length; i++) {
				if(!ids[i].equals("")) query.append(" id:"+ids[i]);
			}
			FullTextQuery hibQuery = searchManager.createFullTextQuery(parser.parse(query.toString().trim()), clazz);
			hibQuery.setProjection(FullTextQuery.DOCUMENT);
			List<Object> results1 = hibQuery.getResultList();
			List<Document> results = new ArrayList<Document>();
			for(Object o : results1) {
				if(o instanceof Object[]) {
					results.add((Document)((Object[])o)[0]);
				}
			}
			return results;
		} catch(Exception e) {
			e.printStackTrace();
		}finally{
			if(searchManager != null)
				searchManager.clear();
		}
		return null;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Document> documentsByParent(Class clazz, String[] ids) {
		FullTextEntityManager searchManager = null;
		try {
			searchManager = Search.getFullTextEntityManager(em);
			MultiFieldQueryParser parser = new MultiFieldQueryParser(new String[]{"parent_id"}, new StandardAnalyzer());
			StringBuffer query = new StringBuffer();
			for(int i=0; i < ids.length; i++) {
				if(!ids[i].equals("")) query.append(" parent_id:"+ids[i]);
			}
			FullTextQuery hibQuery = searchManager.createFullTextQuery(parser.parse(query.toString().trim()), clazz);
			hibQuery.setProjection(FullTextQuery.DOCUMENT);
			List<Object> results1 = hibQuery.getResultList();
			List<Document> results = new ArrayList<Document>();
			for(Object o : results1) {
				if(o instanceof Object[]) {
					results.add((Document)((Object[])o)[0]);
				}
			}
			return results;
		} catch(Exception e) {
			e.printStackTrace();
		}finally{
			if(searchManager != null)
				searchManager.clear();
		}
		return null;
	}
	@Transactional
	public void index(Class<?> clazz, List<Long> ids) {
		FullTextEntityManager searchManager = null;
		try{
			searchManager = Search.getFullTextEntityManager(em);
			for(Long id : ids) {
				Object obj = find(clazz, id);
				searchManager.index(obj);
			}
			searchManager.flushToIndexes();
			searchManager.clear();
			searchManager.getSearchFactory().optimize(clazz);
		} catch(Exception e) {
			e.printStackTrace();
		}finally{
			if(searchManager != null)
				searchManager.clear();
		}
	}
	@Transactional
	public void index(Class<?> clazz, Long id) {
		FullTextEntityManager searchManager = null;
		try{
			searchManager = Search.getFullTextEntityManager(em);
			Object obj = find(clazz, id);
			searchManager.index(obj);
			searchManager.flushToIndexes();
		} catch(Exception e) {
			e.printStackTrace();
		}finally{
			if(searchManager != null)
				searchManager.clear();
		}
	}
	@Transactional
	public void index(Object obj) {
		FullTextEntityManager searchManager = null;
		try {
			searchManager = Search.getFullTextEntityManager(em);
			searchManager.index(obj);
			searchManager.flushToIndexes();
			searchManager.clear();
		} catch(Exception e) {
			e.printStackTrace();
		}finally{
			if(searchManager != null)
				searchManager.clear();
		}
	}

	@Transactional
	public void index(Class<?> clazz, boolean doBulkIndex){
		FullTextEntityManager searchManager = null;
		searchManager = Search.getFullTextEntityManager(em);
		System.out.println("starting mass indexing of "+clazz.getName());
		MassIndexer mi = searchManager.createIndexer(clazz);
		mi.optimizeOnFinish(true);
		mi.batchSizeToLoadObjects(50);
		mi.threadsToLoadObjects(4);
		mi.purgeAllOnStart(true);
		mi.optimizeAfterPurge(true);
		mi.optimizeOnFinish(true);
		mi.cacheMode(CacheMode.NORMAL);
		try {
			mi.startAndWait();
			System.out.println("No Error!");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}finally{
			System.out.println("Finished indexing...");
		}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public void index(Class<?> clazz) throws LookupException {
		FullTextEntityManager searchManager = null;
		try{
			searchManager = Search.getFullTextEntityManager(em);
			List<Object> objects = findAll(clazz.getName());
			System.out.println("starting indexing "+clazz.getName());
			//purge(clazz);
			for(int i=0; i < objects.size(); i++) {
				if(objects.get(i) != null) {
					searchManager.index(objects.get(i));
				}
				if(i%1000 == 0) {
					searchManager.flushToIndexes();
					System.out.println("indexing "+i+" of "+objects.size());
				}
			}
			System.out.println("flushing index");
			searchManager.flushToIndexes();
			System.out.println("clearing index");
			searchManager.clear();
			System.out.println("optimizing index");
			searchManager.getSearchFactory().optimize(clazz);
			System.out.println("finished indexing "+clazz.getName());
		}catch(Exception ee){
			log.error(ee.getMessage());
		}finally{
			if(searchManager != null)
				searchManager.clear();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public void index(Class<?> clazz, int page) {
		FullTextEntityManager searchManager = null;
		try{
			searchManager = Search.getFullTextEntityManager(em);
			List<Object> objects = findPaged(clazz, page);
			for(int i=0; i < objects.size(); i++) {
				if(objects.get(i) != null) {
					searchManager.index(objects.get(i));
				}
			}
			searchManager.flushToIndexes();
			searchManager.clear();
		}catch(Exception ee){
			log.error(ee.getMessage());
		}finally{
			if(searchManager != null)
				searchManager.clear();
		}
	}
	
	public void purge(Class<?> clazz) {
		FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(em);
		fullTextEntityManager.purgeAll(clazz);
		fullTextEntityManager.getSearchFactory().optimize(clazz);
		fullTextEntityManager.clear();
	}
	public void optimize(Class<?> clazz) {
		FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(em);
		fullTextEntityManager.flushToIndexes();
		fullTextEntityManager.clear();
		fullTextEntityManager.getSearchFactory().optimize(clazz);
		fullTextEntityManager.clear();
	}
	protected boolean inHolidayPeriod() {
		return false;
	}
	protected Document cloneDocument(Document doc) {
		Document document = new Document();
		for(Object fieldObj : doc.getFields()) {
			Field field = (Field)fieldObj;
			if(!field.name().equals("ad_size")) {
				document.add(field);
			}
		}
		return document;
	}
}
