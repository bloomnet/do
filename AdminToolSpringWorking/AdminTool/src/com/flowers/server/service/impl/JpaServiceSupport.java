package com.flowers.server.service.impl;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.transaction.annotation.Transactional;

import com.flowers.server.LookupException;
import com.flowers.server.service.ServiceSupport;


public class JpaServiceSupport implements ServiceSupport{
	protected final Log log = LogFactory.getLog(getClass());
	protected EntityManager em;
	
	
	public void refresh(Object o) {
		em.refresh(o);
	}
	@Transactional(rollbackFor = Exception.class)
	public void persist(Object o) throws PersistenceException {
		try {
			em.persist(o);
		} catch(Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Problem saving object of type:"+o.getClass().getSimpleName());
		}
	}
	@Transactional(rollbackFor = Exception.class)
	public void merge(Object o) throws PersistenceException {
		try {
			em.merge(o);
		} catch(Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Problem saving object of type:"+o.getClass().getSimpleName());
		}
	}
	@Transactional(rollbackFor = Exception.class)
	public void remove(Object o) throws PersistenceException {
		try {
			em.remove(o);
		} catch(Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Problem removing object of type:"+o.getClass().getSimpleName());
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Transactional(rollbackFor = Exception.class)
	public Object find(Class clazz, Object id) throws LookupException {
		try {
			Object obj = em.find(clazz, id);
			return obj;
		} catch(Exception e) {
			e.printStackTrace();
			throw new LookupException("Problem fetching object of id:"+id);
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Transactional(rollbackFor = Exception.class)
	public List<Object> findMultiple(Class clazz, List<Object> findList) throws LookupException {
		List<Object> returnList = new ArrayList<Object>();
		try {
			for(int ii=0; ii<findList.size(); ++ii){
				Object obj = em.find(clazz, findList.get(ii));
				returnList.add(obj);
			}
			return returnList;
		} catch(Exception e) {
			e.printStackTrace();
			throw new LookupException("Problem fetching object array");
		}
	}
	
	@SuppressWarnings("rawtypes")
	@Transactional(rollbackFor = Exception.class)
	public List findAll(String name) throws LookupException {
		try {
			List list = em.createQuery("from "+name).getResultList();
			return list;
		} catch(Exception e) {
			e.printStackTrace();
			throw new LookupException("Problem fetching "+name);
		}
	}
	@Transactional(rollbackFor = Exception.class)
	public Object findSingleResult(String query) {
		try {
			Object obj = em.createQuery(query).getSingleResult();
			return obj;
		} catch(Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Transactional(rollbackFor = Exception.class)
	public List<Object> findResults(String query, int limit) {
		try {
			List<Object> list = (limit > 0) ? em.createQuery(query).setMaxResults(limit).getResultList() : em.createQuery(query).getResultList();
			return list;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	@SuppressWarnings("unchecked")
	@Transactional(rollbackFor = Exception.class)
	public List<Object> findResults(String query, Map<String, Object> params) {
		try {
			Query q = em.createQuery(query);
			if (params != null){
				Set<String> keys = params.keySet();
				for (String key:keys){
					q.setParameter(key, params.get(key));
				}
			}
			List<Object> list = q.getResultList();
			return list;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	@SuppressWarnings("rawtypes")
	@Transactional(rollbackFor = Exception.class)
	public List findPaged(Class clazz, int page) {
		String name = clazz.getSimpleName();
		Query q = em.createQuery("from "+name+" n order by n.id asc");
		q.setFirstResult(page*100).setMaxResults(100);
		List list = q.getResultList();
		return list;
	}
	@Transactional(rollbackFor = Exception.class)
	public Long count(Class<?> clazz) {
		Long count = (Long)em.createQuery("select count(*) from "+clazz.getSimpleName()).getSingleResult();
		return count;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Transactional(rollbackFor = Exception.class)
	public List findAllTradCities(String name) throws LookupException {
		refreshConnection();
		try {
			Query query = em.createQuery("from "+name+" where state.id < 78");
			List list = query.getResultList();
			return list;
		} catch(Exception e) {
			e.printStackTrace();
			throw new LookupException("Problem fetching "+name);
		}
	}
	
	@PersistenceContext(unitName="entityManagerFactory", type=PersistenceContextType.EXTENDED)
    public void setEntityManager(EntityManager em) {
		this.em = em;
    }
	
	public void refreshConnection(){
		em.clear();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Object find(Class clazz, Object id, boolean resfreshEntityManager) throws LookupException {
		
		refreshConnection();
		try {
			Object obj = em.find(clazz, id);
			return obj;
		} catch(Exception e) {
			e.printStackTrace();
			throw new LookupException("Problem fetching object of id:"+id);
		}
	}
	
}
