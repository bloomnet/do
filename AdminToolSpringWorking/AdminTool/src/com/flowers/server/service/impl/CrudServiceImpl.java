package com.flowers.server.service.impl;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.flowers.server.entity.Shop;
import com.flowers.server.service.CrudService;

@Service("CrudService")
public class CrudServiceImpl extends JpaSearchServiceSupport implements CrudService{
	
	@Transactional
	public void geocodeShop(long id) {
		synchronized(CrudServiceImpl.class){
			try {
				Shop shop = (Shop)find(Shop.class, id);
				if(shop.getAddress() != null && shop.getAddress().getCity() != null && shop.getAddress().getStreetAddress1() != null && shop.getAddress().getState().getCountry().getShortName().equals("US")) {
					String address = shop.getAddress().getStreetAddress1()+","+shop.getAddress().getCity().getName()+","+shop.getAddress().getCity().getState().getShortName();
					Double[] coords = fetchCoords(address);
					if(coords != null) {
						shop.setLatitude(coords[0]);
						shop.setLongitude(coords[1]);
						merge(shop);
						index(Shop.class, id);
					} else {
						System.out.println("");
						System.out.println("no coords found for "+shop.getShopCode()+" : "+address);
					}
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	protected Double[] fetchCoords(String address) {
		try {
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("address", address));
			URI uri = URIUtils.createURI("http", "geocoder.us", -1, "service/csv/geocode", URLEncodedUtils.format(params, "UTF-8"), null);
			HttpClient client = new DefaultHttpClient();
			HttpGet get = new HttpGet(uri);
			HttpResponse resp = client.execute(get);
			HttpEntity entity = resp.getEntity();
			InputStreamReader reader = new InputStreamReader(entity.getContent());
			StringBuilder content = new StringBuilder();
			char[] chars = new char[4096];
			int len = -1;
			while ( (len = reader.read(chars, 0, 4096)) != -1 ){
				content.append(chars, 0, len);
			}
			String[] coords = content.toString().split(",");
			if(coords.length >= 2) {
				Double[] vals = new Double[2];
				vals[0] = Double.valueOf(coords[0]);
				vals[1] = Double.valueOf(coords[1]);
				return vals;
			}
		} catch(Exception e) {
			System.out.println("Failed to lookup coodinates - "+e.getMessage());
		}
		return null;
	}
	
}
