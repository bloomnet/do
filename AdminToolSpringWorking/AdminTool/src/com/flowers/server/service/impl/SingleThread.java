package com.flowers.server.service.impl;

import java.util.List;
import java.util.Map;

import com.flowers.server.LookupException;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;

public class SingleThread {
	
	public List<?> findAll(String s, CrudService service) throws LookupException{
		synchronized(SingleThread.class){
			return service.findAll(s);
		}
	}
	
	public List<?> findAllTradCities(String s, CrudService service) throws LookupException{
		synchronized(SingleThread.class){
			return service.findAllTradCities(s);
		}
	}
	
	@SuppressWarnings("rawtypes")
	public Object find(Class clazz, Long l, CrudService service, boolean resfreshEntityManager) throws LookupException{
		synchronized(SingleThread.class){
			return service.find(clazz, l, resfreshEntityManager);
		}
	}
	
	@SuppressWarnings("rawtypes")
	public Object find(Class clazz, Long l, CrudService service) throws LookupException{
		synchronized(SingleThread.class){
			return service.find(clazz, l);
		}
	}
	
	public Object findResults(String pql, CrudService service, Map<String, Object> params) throws LookupException{
		synchronized(SingleThread.class){
			return service.findResults(pql, params);
		}
	}
	
	public Object findResults(String pql, CrudService service, Integer i) throws LookupException{
		synchronized(SingleThread.class){
			return service.findResults(pql, i);
		}
	}
		
	
	public Object findSingleResult(String pql, CrudService service) throws LookupException{
		synchronized(SingleThread.class){
			return service.findSingleResult(pql);
		}
	}
	
	public void persist(Object o, CrudService service){
		synchronized(SingleThread.class){
			service.persist(o);
		}
	}
	
	public void merge(Object o, CrudService service){
		synchronized(SingleThread.class){
			service.merge(o);
		}
	}
	
	public void remove(Object o, CrudService service){
		synchronized(SingleThread.class){
			service.remove(o);
		}
	}
	
	public void index(Class<?> clazz, SearchService service){
		synchronized(SingleThread.class){
			try {
				service.index(clazz);
			} catch (LookupException e) {
				e.printStackTrace();
			} 
		}
	}
	
	public void index(Class<?> clazz, SearchService service, boolean useBulkMethod){
		synchronized(SingleThread.class){
			service.index(clazz,true); 
		}
	}
	
	public void index(Class<?> clazz, int page, SearchService service){
		synchronized(SingleThread.class){
			service.index(clazz, page);
		}
	}
	
	public void index(Class<?> clazz, Long id, SearchService service){
		synchronized(SingleThread.class){
			service.index(clazz, id);
		}
	}
	
	public void index(Class<?> clazz, Long id, CrudService service){
		synchronized(SingleThread.class){
			service.index(clazz, id);
		}
	}
	
	@Deprecated //Reason for Deprecation: Object won't necessarily be part of the active session
	public void index(Object obj, SearchService service) {
		synchronized(SingleThread.class){
			service.index(obj);
		}
	}
	
	public void index(Object obj, CrudService service) {
		synchronized(SingleThread.class){
			service.index(obj);
		}
	}
	
	public void index(Class<?> clazz, List<Long> ids, SearchService service) {
		synchronized(SingleThread.class){
			service.index(clazz, ids);
		}
	}
	
	public void index(Class<?> clazz, List<Long> ids, CrudService service) {
		synchronized(SingleThread.class){
			service.index(clazz, ids);
		}
	}
	
	public void refreshObject(Class<?> clazz, Long id, CrudService service) throws LookupException{
		synchronized(SingleThread.class){
			Object o = service.find(clazz, id);
			service.refresh(o);
		}
	}
	
	public void refreshObject(Class<?> clazz, List<Long> indexList, CrudService service) throws LookupException{
		synchronized(SingleThread.class){
			for(int ii = 0; ii<indexList.size(); ++ii){
				Object o = service.find(clazz, indexList.get(ii));
				service.refresh(o);
			}
		}
	}

	public List<?> findMultiple(Class<?> clazz,
			List<Object> findList, CrudService service) throws LookupException {
		synchronized(SingleThread.class){
			return service.findMultiple(clazz, findList);
		}
	}
	
	public void removeMultiple(List<Object> oList, CrudService service){
		synchronized(SingleThread.class){
			for(Object o : oList){
				service.remove(o);
			}
		}
	}
	
}
