package com.flowers.server.service;

import java.util.List;

public interface CrudService extends ServiceSupport {

	void geocodeShop(long id);
	void index(Object obj);
	void index(Class<?> clazz, Long id);
	void index(Class<?> clazz, List<Long> ids);
	
}
