package com.flowers.server.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;
import org.hibernate.search.annotations.TermVector;


@Entity
@Indexed
@Table(name = "login_log")
public class LoginLog {
	private long id;
	private String shopCode;
	private long timestamp;
	
	public LoginLog() {}
	public LoginLog(String shopCode) {
		this.shopCode = shopCode;
		this.timestamp = System.currentTimeMillis();
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	@Column(name = "shop_code")
	@Field(name="shop_code",store=Store.YES,index=Index.YES,termVector=TermVector.NO)
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	@Column(name = "created_date")
	@Field(name="createdDate",store=Store.YES,index=Index.YES,termVector=TermVector.NO)
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	
}
