package com.flowers.server.entity;

//default package
//Generated Aug 22, 2013 7:06:48 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
* SocialMedia generated by hbm2java
*/
@Entity
@Table(name = "social_media", catalog = "bloomnet", uniqueConstraints = @UniqueConstraint(columnNames = "shop_shop_code"))
public class SocialMedia implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3642484643233479095L;
	private Long id;
	private Shop shop;
	private String facebook;
	private String twitter;
	private String linkedin;
	private String googleplus;
	private String instagram;
	private String pinterest;
	private String yelp;

	public SocialMedia() {
	}

	public SocialMedia(Shop shop) {
		this.shop = shop;
	}

	public SocialMedia(Shop shop, String facebook, String twitter,
			String linkedin, String googleplus) {
		this.shop = shop;
		this.facebook = facebook;
		this.twitter = twitter;
		this.linkedin = linkedin;
		this.googleplus = googleplus;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "shop_shop_code", unique = true, nullable = false, referencedColumnName = "shop_code")
	public Shop getShop() {
		return this.shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	@Column(name = "facebook", length = 200)
	public String getFacebook() {
		return this.facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	@Column(name = "twitter", length = 200)
	public String getTwitter() {
		return this.twitter;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	@Column(name = "linkedin", length = 200)
	public String getLinkedin() {
		return this.linkedin;
	}

	public void setLinkedin(String linkedin) {
		this.linkedin = linkedin;
	}

	@Column(name = "googleplus", length = 200)
	public String getGoogleplus() {
		return this.googleplus;
	}

	public void setGoogleplus(String googleplus) {
		this.googleplus = googleplus;
	}
	
	@Column(name = "instagram", length = 200)
	public String getInstagram() {
		return instagram;
	}

	public void setInstagram(String instagram) {
		this.instagram = instagram;
	}

	@Column(name = "pinterest", length = 200)
	public String getPinterest() {
		return pinterest;
	}

	public void setPinterest(String pinterest) {
		this.pinterest = pinterest;
	}
	
	@Column(name = "yelp", length = 200)
	public String getYelp() {
		return yelp;
	}

	public void setYelp(String yelp) {
		this.yelp = yelp;
	}

}
