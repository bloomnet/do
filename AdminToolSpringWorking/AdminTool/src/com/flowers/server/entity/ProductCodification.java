package com.flowers.server.entity;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.ClassBridge;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;
import org.hibernate.search.annotations.TermVector;

import com.flowers.server.search.ProductCodificationFieldBridge;


@Entity
@Indexed
@ClassBridge(name="productCodificationBridge",index=Index.YES,store=Store.YES,impl =ProductCodificationFieldBridge.class)
@Table(name="product_codification")
public class ProductCodification  implements java.io.Serializable {
     private static final long serialVersionUID = 993337356706813287L;
	private long id;
     private PhysicalFile physicalFile;
     private String productCodification;
     private String name;
     private String description;
     private String color;
     private String material;
     private String unitMeasures;
     private String flowersArrangement;
     private Double unitPrice;
     private String bloomnetUrl;
     
     private Set<ShopProductCodificationXref> shopProductCodificationXrefs = new HashSet<ShopProductCodificationXref>(0);
     
     
    public ProductCodification() {
    }

    public ProductCodification(long id) {
    	this.id = id;
    }
    public ProductCodification(long id, String productCodification) {
    	this.id = id;
    	this.productCodification = productCodification;
    }
    public ProductCodification(long id, PhysicalFile physicalFile) {
        this.id = id;
        this.physicalFile = physicalFile;
    }
    public ProductCodification(long id, PhysicalFile physicalFile, String productCodification, String name, 
    		String description, String color, String material, String unitMeasures, String flowersArrangement, Double unitPrice, String bloomnetUrl) {
       this.id = id;
       this.physicalFile = physicalFile;
       this.productCodification = productCodification;
       this.name = name;
       this.description = description;
       this.color = color;
       this.material = material;
       this.unitMeasures = unitMeasures;
       this.flowersArrangement = flowersArrangement;
       this.unitPrice = unitPrice;
       this.bloomnetUrl = bloomnetUrl;
    }
   
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id", unique=true, nullable=false)
    public long getId() {
        return this.id;
    }
    
    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="physical_file_id", nullable=true)
    public PhysicalFile getPhysicalFile() {
        return this.physicalFile;
    }
    
    public void setPhysicalFile(PhysicalFile physicalFile) {
        this.physicalFile = physicalFile;
    }

    @Field(name="code",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
    @Column(name="product_codification", length=1)
    public String getProductCodification() {
        return this.productCodification;
    }
    
    public void setProductCodification(String productCodification) {
        this.productCodification = productCodification;
    }

    @Field(name="name",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
    @Column(name="name", length=50)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    @Field(name="description",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
    @Column(name="description", length=100)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    @Field(name="color",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
    @Column(name="color", length=30)
    public String getColor() {
        return this.color;
    }
    
    public void setColor(String color) {
        this.color = color;
    }

    @Field(name="material",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
    @Column(name="material", length=50)
    public String getMaterial() {
        return this.material;
    }
    
    public void setMaterial(String material) {
        this.material = material;
    }

    @Field(name="unit_measures",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
    @Column(name="unit_measures", length=100)
    public String getUnitMeasures() {
        return this.unitMeasures;
    }
    
    public void setUnitMeasures(String unitMeasures) {
        this.unitMeasures = unitMeasures;
    }

    @Field(name="flowers_arrangement",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
    @Column(name="flowers_arrangement", length=100)
    public String getFlowersArrangement() {
        return this.flowersArrangement;
    }
    
    public void setFlowersArrangement(String flowersArrangement) {
		this.flowersArrangement = flowersArrangement;
    }

    @Field(name="unit_price",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
    @Column(name="unit_price", precision=22, scale=0)
    public Double getUnitPrice() {
        return this.unitPrice;
    }
    
    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }
    
    @Field(name="bloomnet_url",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
    @Column(name="bloomnet_url", length=200)
    public String getBloomnetUrl() {
		return bloomnetUrl;
	}
    
    public void setBloomnetUrl(String bloomnetUrl) {
		this.bloomnetUrl = bloomnetUrl;
	}
    
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "productCodification")
	public Set<ShopProductCodificationXref> getShopProductCodificationXrefs() {
		return this.shopProductCodificationXrefs;
	}

	public void setShopProductCodificationXrefs(
			Set<ShopProductCodificationXref> shopProductCodificationXrefs) {
		this.shopProductCodificationXrefs = shopProductCodificationXrefs;
	}
}


