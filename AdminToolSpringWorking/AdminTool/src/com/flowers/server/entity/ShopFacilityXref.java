package com.flowers.server.entity;

// Generated Jan 31, 2011 3:27:19 PM by Hibernate Tools 3.3.0.GA

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.Indexed;


/**
 * ShopFacilityXref generated by hbm2java
 */
@Entity
@Indexed
@Table(name = "shop_facility_xref")
public class ShopFacilityXref implements java.io.Serializable {
	private static final long serialVersionUID = 2332124171931670013L;
	private long id;
	private Shop shop;
	private Facility facility;

	public ShopFacilityXref() {
	}

	public ShopFacilityXref(long id, Shop shop, Facility facility) {
		this.id = id;
		this.shop = shop;
		this.facility = facility;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}
	@ContainedIn
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "shop_code", nullable = false, referencedColumnName="shop_code")
	public Shop getShop() {
		return this.shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}
	@ContainedIn
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "facility_id", nullable = false)
	public Facility getFacility() {
		return this.facility;
	}

	public void setFacility(Facility facility) {
		this.facility = facility;
	}

}
