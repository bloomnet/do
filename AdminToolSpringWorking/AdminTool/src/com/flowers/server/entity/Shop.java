package com.flowers.server.entity;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.ClassBridge;
import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.SortableField;
import org.hibernate.search.annotations.Store;
import org.hibernate.search.annotations.TermVector;

import com.flowers.server.search.ShopFieldBridge;


@Entity
@Indexed
@ClassBridge(name="shopBridge",index=Index.YES,store=Store.YES,impl =ShopFieldBridge.class)
@Table(name = "shop")
@JsonIgnoreProperties({"address"})
public class Shop implements java.io.Serializable {
	private static final long serialVersionUID = -3794369771019199299L;
	private long id;
	private String shopCode;
	private User userByUserCreatedId;
	private User userByUserModifiedId;
	private Address address;
	private String shopName;
	private String telephoneNumber;
	private String tollFreeNumber;
	private String faxNumber;
	private String contactPerson;
	private Boolean openSunday;
	private Boolean bloomlinkIndicator;
	private Boolean newShop;
	private Boolean floristForForrests;
	private Date dateCreated;
	private Date dateModified;
	private Double latitude;
	private Double longitude;
	private Double minimum;
	private String shopWebsite;
	private Boolean primaryWebsite;
	private Boolean thirdPartySite;
	private String shopLogo;
	private String consumerImage;
	private Set<MinimumPrice> minimumPrices = new HashSet<MinimumPrice>(0);
	private Set<ShopLocationCoveredXref> shopLocationCoveredXrefs = new HashSet<ShopLocationCoveredXref>(0);
	private Set<ShopFacilityXref> shopFacilityXrefs = new HashSet<ShopFacilityXref>(0);
	private Set<Listing> listings = new HashSet<Listing>(0);
	private Set<FacilityListing> facilityListings = new HashSet<FacilityListing>(0);
	private Set<LocalProducts> localProducts = new HashSet<LocalProducts>(0);
	private Set<ShopProductCodificationXref> shopProductCodificationXrefs = new HashSet<ShopProductCodificationXref>(0);
	private Set<LikeDislike> likeDislikes = new HashSet<LikeDislike>(0);
	private Set<SocialMedia> socialMedias = new HashSet<SocialMedia>(0);
	private Set<ZipListing> zipListings = new HashSet<ZipListing>(0);
	private Boolean dontShowCDO;
	private Double deliveryFee;
	
	public Shop() {}
	
	public Shop(String shopCode, String shopName) {
		this.shopCode = shopCode;
		this.shopName = shopName;
	}
	public Shop(long id, String shopCode, String shopName, String contactPerson) {
		this.id = id;
		this.shopCode = shopCode;
		this.shopName = shopName;
		this.contactPerson = contactPerson;
	}

	public Shop(long id, String shopCode, String shopName) {
		this.id = id;
		this.shopCode = shopCode;
		this.shopName = shopName;
	}
	public Shop(long id, String shopCode, User userByUserCreatedId, Address address) {
		this.id = id;
		this.shopCode = shopCode;
		this.userByUserCreatedId = userByUserCreatedId;
		this.address = address;
	}

	public Shop(long id, String shopCode, User userByUserCreatedId,
			User userByUserModifiedId, Address address, String shopName,
			String telephoneNumber, String tollFreeNumber, String faxNumber,
			String contactPerson, Boolean openSunday,
			Boolean bloomlinkIndicator, Boolean newShop,
			Boolean floristForForrests, Date dateCreated, Date dateModified,
			Set<MinimumPrice> minimumPrices,
			Set<ShopLocationCoveredXref> shopLocationCoveredXrefs,
			Set<ShopFacilityXref> shopFacilityXrefs, Set<Listing> listings,
			Set<ShopProductCodificationXref> shopProductCodificationXrefs,
			Set<SocialMedia> socialMedias) {
		this.id = id;
		this.shopCode = shopCode;
		this.userByUserCreatedId = userByUserCreatedId;
		this.userByUserModifiedId = userByUserModifiedId;
		this.address = address;
		this.shopName = shopName;
		this.telephoneNumber = telephoneNumber;
		this.tollFreeNumber = tollFreeNumber;
		this.faxNumber = faxNumber;
		this.contactPerson = contactPerson;
		this.openSunday = openSunday;
		this.bloomlinkIndicator = bloomlinkIndicator;
		this.newShop = newShop;
		this.floristForForrests = floristForForrests;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
		this.minimumPrices = minimumPrices;
		this.shopLocationCoveredXrefs = shopLocationCoveredXrefs;
		this.shopFacilityXrefs = shopFacilityXrefs;
		this.listings = listings;
		this.shopProductCodificationXrefs = shopProductCodificationXrefs;
		this.socialMedias = socialMedias;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "shop_id", unique = true, nullable = false)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	@Field(name="shop_code",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "shop_code", unique = true, nullable = false, length = 8)
	public String getShopCode() {
		return this.shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_created_id", nullable = false)
	public User getUserByUserCreatedId() {
		return this.userByUserCreatedId;
	}

	public void setUserByUserCreatedId(User userByUserCreatedId) {
		this.userByUserCreatedId = userByUserCreatedId;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_modified_id")
	public User getUserByUserModifiedId() {
		return this.userByUserModifiedId;
	}

	public void setUserByUserModifiedId(User userByUserModifiedId) {
		this.userByUserModifiedId = userByUserModifiedId;
	}
	@ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name = "address_id", nullable = false)
	public Address getAddress() {
		return this.address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	@Field(name="shop_name",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "shop_name", length = 50)
	@SortableField(forField="shop_name")
	public String getShopName() {
		return this.shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	@Field(name="telephone_number",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "telephone_number", length = 25)
	public String getTelephoneNumber() {
		return this.telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}
	@Field(name="toll_free_number",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "toll_free_number", length = 25)
	public String getTollFreeNumber() {
		return this.tollFreeNumber;
	}

	public void setTollFreeNumber(String tollFreeNumber) {
		this.tollFreeNumber = tollFreeNumber;
	}
	@Field(name="fax_number",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "fax_number", length = 25)
	public String getFaxNumber() {
		return this.faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}
	@Field(name="contact_person",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "contact_person", length = 50)
	public String getContactPerson() {
		return this.contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}
	@Field(name="open_sunday",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "open_sunday")
	public Boolean getOpenSunday() {
		if (openSunday == null){
			return Boolean.FALSE;
		}else{
			return this.openSunday;
		}
	}
	public void setOpenSunday(String openSunday) {
		if(openSunday.equals("Y")) this.openSunday = Boolean.TRUE;
		else this.openSunday = Boolean.FALSE;
	}
	
	public void setOpenSunday(Boolean openSunday) {
		this.openSunday = openSunday;
	}
	@Field(name="bloomlink_indicator",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "bloomlink_indicator")
	public Boolean getBloomlinkIndicator() {
		if (bloomlinkIndicator == null){
			return Boolean.FALSE;
		}else{
			return this.bloomlinkIndicator;
		}
	}

	public void setBloomlinkIndicator(String bloomlinkIndicator) {
		if(bloomlinkIndicator.equals("Y"))this.bloomlinkIndicator = Boolean.TRUE;
		else this.bloomlinkIndicator = Boolean.FALSE;
	}
	public void setBloomlinkIndicator(Boolean bloomlinkIndicator) {
		this.bloomlinkIndicator = bloomlinkIndicator;
	}
	
	@Column(name = "new_shop")
	public Boolean getNewShop() {
		if (newShop == null){
			return Boolean.FALSE;
		}else{
			return this.newShop;
		}
	}
	
	public void setNewShop(String newShop) {
		if(newShop.equals("X")) this.newShop = Boolean.TRUE;
		else this.newShop = Boolean.FALSE;
	}
	
	public void setNewShop(Boolean newShop) {
		this.newShop = newShop;
	}
	@Field(name="florist_for_forrests",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "florist_for_forrests")
	public Boolean getFloristForForrests() {
		if (floristForForrests == null){
			return Boolean.FALSE;
		}else{
			return this.floristForForrests;
		}
	}

	public void setFloristForForrests(String floristForForrests) {
		if(floristForForrests.equals("X")) this.floristForForrests = Boolean.TRUE;
		else this.floristForForrests = Boolean.FALSE;
	}
	
	public void setFloristForForrests(Boolean floristForForrests) {
		this.floristForForrests = floristForForrests;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_created", length = 19)
	public Date getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_modified", length = 19)
	public Date getDateModified() {
		return this.dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}
	@Field(name="latitude",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "latitude")
	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	@Field(name="longitude",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "longitude")
	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "shop", cascade=CascadeType.REMOVE, orphanRemoval=true)
	public Set<MinimumPrice> getMinimumPrices() {
		return this.minimumPrices;
	}

	public void setMinimumPrices(Set<MinimumPrice> minimumPrices) {
		this.minimumPrices = minimumPrices;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "shop", cascade=CascadeType.REMOVE, orphanRemoval=true)
	public Set<ShopLocationCoveredXref> getShopLocationCoveredXrefs() {
		return this.shopLocationCoveredXrefs;
	}

	public void setShopLocationCoveredXrefs(
			Set<ShopLocationCoveredXref> shopLocationCoveredXrefs) {
		this.shopLocationCoveredXrefs = shopLocationCoveredXrefs;
	}
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "shop", cascade=CascadeType.REMOVE, orphanRemoval=true)
	public Set<ShopFacilityXref> getShopFacilityXrefs() {
		return this.shopFacilityXrefs;
	}

	public void setShopFacilityXrefs(Set<ShopFacilityXref> shopFacilityXrefs) {
		this.shopFacilityXrefs = shopFacilityXrefs;
	}
	@ContainedIn
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "shop", cascade=CascadeType.REMOVE, orphanRemoval=true)
	public Set<Listing> getListings() {
		return this.listings;
	}

	public void setListings(Set<Listing> listings) {
		this.listings = listings;
	}
	
	@ContainedIn
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "shop", cascade=CascadeType.REMOVE, orphanRemoval=true)
	public Set<FacilityListing> getFacilityListings() {
		return this.facilityListings;
	}

	public void setFacilityListings(Set<FacilityListing> facilityListings) {
		this.facilityListings = facilityListings;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "shop", cascade=CascadeType.REMOVE, orphanRemoval=true)
	public Set<ShopProductCodificationXref> getShopProductCodificationXrefs() {
		return this.shopProductCodificationXrefs;
	}

	public void setShopProductCodificationXrefs(
			Set<ShopProductCodificationXref> shopProductCodificationXrefs) {
		this.shopProductCodificationXrefs = shopProductCodificationXrefs;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "shop", cascade = CascadeType.REMOVE, orphanRemoval = true)
	public Set<LikeDislike> getLikeDislikes() {
		return likeDislikes;
	}
	
	public void setLikeDislikes(Set<LikeDislike> likeDislikes) {
		this.likeDislikes = likeDislikes;
	}

	@Override
	public String toString() {
		return "Shop [shopCode=" + shopCode + "]";
		/*
		 * This was throwing Nulls during indexing of associated objects (Listings, Products) not sure why....Patrick
		return "Shop [shopCode=" + shopCode + ", userByUserCreatedId=" + userByUserCreatedId + ", userByUserModifiedId=" + userByUserModifiedId + ", address=" + address + ", shopName=" + shopName + ", telephoneNumber=" + telephoneNumber + ", tollFreeNumber=" + tollFreeNumber + ", faxNumber="
				+ faxNumber + ", contactPerson=" + contactPerson + ", openSunday=" + openSunday + ", bloomlinkIndicator=" + bloomlinkIndicator + ", newShop=" + newShop + ", floristForForrests=" + floristForForrests + ", dateCreated=" + dateCreated + ", dateModified=" + dateModified
				+ ", minimumPrices=" + minimumPrices + ", shopLocationCoveredXrefs=" + shopLocationCoveredXrefs + ", shopFacilityXrefs=" + shopFacilityXrefs + ", listings=" + listings + ", shopProductCodificationXrefs=" + shopProductCodificationXrefs + "]";
		*/
		
	}

	public void setSocialMedias(Set<SocialMedia> socialMedias) {
		this.socialMedias = socialMedias;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "shop", cascade = CascadeType.REMOVE, orphanRemoval = true)
	public Set<SocialMedia> getSocialMedias() {
		return socialMedias;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "shop")
	public Set<ZipListing> getZipListings() {
		return this.zipListings;
	}

	public void setZipListings(
			Set<ZipListing> zipListings) {
		this.zipListings = zipListings;
	}
	@ContainedIn
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "shop", cascade=CascadeType.REMOVE, orphanRemoval=true)
	public Set<LocalProducts> getLocalProducts() {
		return localProducts;
	}

	public void setLocalProducts(Set<LocalProducts> localProducts) {
		this.localProducts = localProducts;
	}
	
	@Field(name="minimum",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "minimum")
	public Double getMinimum() {
		return minimum;
	}

	public void setMinimum(Double minimum) {
		this.minimum = minimum;
	}
	
	@Field(name="website",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "website")
	public String getShopWebsite() {
		return shopWebsite;
	}

	public void setShopWebsite(String shopWebsite) {
		this.shopWebsite = shopWebsite;
	}

	@Field(name="logo",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "logo")
	public String getShopLogo() {
		return shopLogo;
	}

	public void setShopLogo(String shopLogo) {
		this.shopLogo = shopLogo;
	}

	@Field(name="consumer_image",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "consumer_image")
	public String getConsumerImage() {
		return consumerImage;
	}

	public void setConsumerImage(String consumerImage) {
		this.consumerImage = consumerImage;
	}
	
	@Field(name="primary_website",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "primary_website")
	public Boolean getPrimaryWebsite() {
		return primaryWebsite;
	}
	public void setPrimaryWebsite(Boolean primaryWebsite) {
		this.primaryWebsite = primaryWebsite;
	}
	
	@Field(name="dont_show_cdo",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "dontShowCDO")
	public Boolean getDontShowCDO() {
		return dontShowCDO;
	}
	public void setDontShowCDO(Boolean dontShowCDO) {
		this.dontShowCDO = dontShowCDO;
	}
	
	@Field(name="delivery_fee",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "delivery_fee")
	public Double getDeliveryFee() {
		return deliveryFee;
	}

	public void setDeliveryFee(Double deliveryFee) {
		this.deliveryFee = deliveryFee;
	}
	
	@Field(name="third_party_site",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "third_party_site")
	public Boolean getThirdPartySite() {
		return thirdPartySite;
	}

	public void setThirdPartySite(Boolean thirdPartySite) {
		this.thirdPartySite = thirdPartySite;
	}

}
