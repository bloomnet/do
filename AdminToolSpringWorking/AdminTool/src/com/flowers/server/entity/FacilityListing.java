package com.flowers.server.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.ClassBridge;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;
import org.hibernate.search.annotations.TermVector;

import com.flowers.server.search.FacilityListingFieldBridge;

@Entity
@Indexed
@ClassBridge(name="listingBridge",index=Index.YES,store=Store.YES,impl =FacilityListingFieldBridge.class)
@Table(name = "facility_listing")
public class FacilityListing implements java.io.Serializable {
	private static final long serialVersionUID = 8736294323729072211L;
	private long id;
	private Shop shop;
	private City city;
	private ListingType listingType;
	private User userByUserCreatedId;
	private AdSize adSize;
	private Mac mac;
	private User userByUserModifiedId;
	private String entryCode;
	private Boolean rotate;
	private Boolean revListing;
	private String customListing;
	private String revAd;
	private Double minOrder;
	private Double delivery;
	private String date;
	private String freeAd;
	private String blockBloomlinkZips;
	private String textColor;
	private String highlightColor;
	private String videoColor;
	private Integer imageCount;
	private Boolean cl8;
	private Boolean ca9;
	private Boolean status;
	private Date dateCreated;
	private Date dateModified;
	private Set<ListingPhysicalFileXref> listingPhysicalFileXrefs = new HashSet<ListingPhysicalFileXref>(0);
	private Double bid;
	private Double sideAdBid;
	private String sideAdEntryNumber;
	private String topAdEntryNumber;
	private Date topAdStartDate;
	private Date topAdEndDate;
	private Date adStartDate;
	private Date adEndDate;
	private Date bannerStartDate;
	private Date bannerEndDate;
	private Date listingEndDate;
	private Boolean moreInfo;
	private Boolean seasonalImages;
	private String winterBackground;
	private String vdayBackground;
	private String springBackground;
	private String mdayBackground;
	private String summerBackground;
	private String fallBackground;
	private Boolean featuredListing;
	private Boolean preferredListing;

	public FacilityListing() {
	}

	public FacilityListing(long id, Shop shop, ListingType listingType,
			User userByUserCreatedId) {
		this.id = id;
		this.shop = shop;
		this.listingType = listingType;
		this.userByUserCreatedId = userByUserCreatedId;
	}

	public FacilityListing(long id, Shop shop, City city, ListingType listingType,
			User userByUserCreatedId, AdSize adSize, Mac mac,
			User userByUserModifiedId, String entryCode, Boolean rotate,
			Boolean revListing, String customListing, String revAd,
			Double minOrder, Double delivery, String date, String freeAd,
			String blockBloomlinkZips, String textColor, String highlightColor,
			String videoColor, Integer imageCount, Boolean cl8, Boolean ca9,
			Boolean status, Date dateCreated, Date dateModified,
			Set<ListingPhysicalFileXref> listingPhysicalFileXrefs, Boolean moreInfo) {
		this.id = id;
		this.shop = shop;
		this.city = city;
		this.listingType = listingType;
		this.userByUserCreatedId = userByUserCreatedId;
		this.adSize = adSize;
		this.mac = mac;
		this.userByUserModifiedId = userByUserModifiedId;
		this.entryCode = entryCode;
		this.rotate = rotate;
		this.revListing = revListing;
		this.customListing = customListing;
		this.revAd = revAd;
		this.minOrder = minOrder;
		this.delivery = delivery;
		this.date = date;
		this.freeAd = freeAd;
		this.blockBloomlinkZips = blockBloomlinkZips;
		this.textColor = textColor;
		this.highlightColor = highlightColor;
		this.videoColor = videoColor;
		this.imageCount = imageCount;
		this.cl8 = cl8;
		this.ca9 = ca9;
		this.status = status;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
		this.listingPhysicalFileXrefs = listingPhysicalFileXrefs;
		this.moreInfo = moreInfo;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "shop_code", nullable = false, referencedColumnName="shop_code")
	public Shop getShop() {
		return this.shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "city_id")
	public City getCity() {
		return this.city;
	}

	public void setCity(City city) {
		this.city = city;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "listing_type_id", nullable = false)
	public ListingType getListingType() {
		return this.listingType;
	}

	public void setListingType(ListingType listingType) {
		this.listingType = listingType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_created_id", nullable = true)
	public User getUserByUserCreatedId() {
		return this.userByUserCreatedId;
	}

	public void setUserByUserCreatedId(User userByUserCreatedId) {
		this.userByUserCreatedId = userByUserCreatedId;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ad_size_id")
	public AdSize getAdSize() {
		return this.adSize;
	}

	public void setAdSize(AdSize adSize) {
		this.adSize = adSize;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "mac_id")
	public Mac getMac() {
		return this.mac;
	}

	public void setMac(Mac mac) {
		this.mac = mac;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_modified_id")
	public User getUserByUserModifiedId() {
		return this.userByUserModifiedId;
	}

	public void setUserByUserModifiedId(User userByUserModifiedId) {
		this.userByUserModifiedId = userByUserModifiedId;
	}
	@Field(name="entry_code",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "entry_code", length = 10)
	public String getEntryCode() {
		return this.entryCode;
	}

	public void setEntryCode(String entryCode) {
		this.entryCode = entryCode;
	}
	@Field(name="rotate",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "rotate")
	public Boolean getRotate() {
		if (rotate == null){
			return Boolean.FALSE;
		}else{
			return this.rotate;
		}
	}

	public void setRotate(Boolean rotate) {
		this.rotate = rotate;
	}
	@Field(name="rev_listing",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "rev_listing")
	public Boolean getRevListing() {
		if (revListing == null){
			return Boolean.FALSE;
		}else{
			return this.revListing;
		}
	}

	public void setRevListing(Boolean revListing) {
		this.revListing = revListing;
	}
	@Field(name="custom_listing",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "custom_listing")
	public String getCustomListing() {
		return this.customListing;
	}

	public void setCustomListing(String customListing) {
		this.customListing = customListing;
	}
	@Field(name="rev_ad",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "rev_ad", length = 5)
	public String getRevAd() {
		return this.revAd;
	}

	public void setRevAd(String revAd) {
		this.revAd = revAd;
	}
	@Field(name="min_order",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "min_order", precision = 22, scale = 0)
	public Double getMinOrder() {
		return this.minOrder;
	}

	public void setMinOrder(Double minOrder) {
		this.minOrder = minOrder;
	}
	@Field(name="delivery",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "delivery", precision = 22, scale = 0)
	public Double getDelivery() {
		return this.delivery;
	}

	public void setDelivery(Double delivery) {
		this.delivery = delivery;
	}
	@Field(name="date",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "date", length = 50)
	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	@Column(name = "free_ad", length = 50)
	public String getFreeAd() {
		return this.freeAd;
	}

	public void setFreeAd(String freeAd) {
		this.freeAd = freeAd;
	}
	@Field(name="block_bloomlink_zips",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "block_bloomlink_zips", length = 50)
	public String getBlockBloomlinkZips() {
		return this.blockBloomlinkZips;
	}

	public void setBlockBloomlinkZips(String blockBloomlinkZips) {
		this.blockBloomlinkZips = blockBloomlinkZips;
	}
	@Field(name="text_color",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "text_color", length = 50)
	public String getTextColor() {
		return this.textColor;
	}

	public void setTextColor(String textColor) {
		this.textColor = textColor;
	}
	@Field(name="highlight_color",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "highlight_color", length = 50)
	public String getHighlightColor() {
		return this.highlightColor;
	}

	public void setHighlightColor(String highlightColor) {
		this.highlightColor = highlightColor;
	}
	@Field(name="video_color",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "video_color", length = 50)
	public String getVideoColor() {
		return this.videoColor;
	}

	public void setVideoColor(String videoColor) {
		this.videoColor = videoColor;
	}
	@Field(name="image_count",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "image_count")
	public Integer getImageCount() {
		return this.imageCount;
	}

	public void setImageCount(Integer imageCount) {
		this.imageCount = imageCount;
	}
	@Column(name = "cl8")
	public Boolean getCl8() {
		if (cl8 == null){
			return Boolean.FALSE;
		}else{
			return this.cl8;
		}
	}

	public void setCl8(Boolean cl8) {
		this.cl8 = cl8;
	}
	@Column(name = "ca9")
	public Boolean getCa9() {
		if (ca9 == null){
			return Boolean.FALSE;
		}else{
			return this.ca9;
		}
	}

	public void setCa9(Boolean ca9) {
		this.ca9 = ca9;
	}
	@Field(name="status",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "status")
	public Boolean getStatus() {
		if (status == null){
			return Boolean.FALSE;
		}else{
			return this.status;
		}
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_created", length = 19)
	public Date getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_modified", length = 19)
	public Date getDateModified() {
		return this.dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "listing")
	public Set<ListingPhysicalFileXref> getListingPhysicalFileXrefs() {
		return this.listingPhysicalFileXrefs;
	}

	public void setListingPhysicalFileXrefs(
			Set<ListingPhysicalFileXref> listingPhysicalFileXrefs) {
		this.listingPhysicalFileXrefs = listingPhysicalFileXrefs;
	}

	public void setBid(Double bid) {
		this.bid = bid;
	}

	@Field(name="bid",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "bid")
	public Double getBid() {
		return bid;
	}
	
	public void setSideAdBid(Double sideAdBid) {
		this.sideAdBid = sideAdBid;
	}

	@Field(name="side_ad_bid",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "side_ad_bid")
	public Double getSideAdBid() {
		return sideAdBid;
	}
	
	@Field(name="side_ad_entry_number",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "side_ad_entry_number")
	public String getSideAdEntryNumber() {
		return sideAdEntryNumber;
	}

	public void setSideAdEntryNumber(String sideAdEntryNumber) {
		this.sideAdEntryNumber = sideAdEntryNumber;
	}
	
	@Field(name="ad_start_date",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "ad_start_date")
	public Date getAdStartDate() {
		return adStartDate;
	}

	public void setAdStartDate(Date adStartDate) {
		this.adStartDate = adStartDate;
	}
	
	@Field(name="ad_end_date",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "ad_end_date")
	public Date getAdEndDate() {
		return adEndDate;
	}

	public void setAdEndDate(Date adEndDate) {
		this.adEndDate = adEndDate;
	}
	
	@Field(name="banner_start_date",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "banner_start_date")
	public Date getBannerStartDate() {
		return bannerStartDate;
	}

	public void setBannerStartDate(Date bannerStartDate) {
		this.bannerStartDate = bannerStartDate;
	}
	
	@Field(name="banner_end_date",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "banner_end_date")
	public Date getBannerEndDate() {
		return bannerEndDate;
	}

	public void setBannerEndDate(Date bannerEndDate) {
		this.bannerEndDate = bannerEndDate;
	}

	@Field(name="listing_end_date",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "listing_end_date")
	public Date getListingEndDate() {
		return listingEndDate;
	}
	
	public void setListingEndDate(Date listingEndDate) {
		this.listingEndDate = listingEndDate;
	}
	
	@Field(name="top_ad_entry_number",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "top_ad_entry_number")
	public String getTopAdEntryNumber() {
		return topAdEntryNumber;
	}

	public void setTopAdEntryNumber(String topAdEntryNumber) {
		this.topAdEntryNumber = topAdEntryNumber;
	}
	
	@Field(name="top_ad_start_date",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "top_ad_start_date")
	public Date getTopAdStartDate() {
		return topAdStartDate;
	}

	public void setTopAdStartDate(Date topAdStartDate) {
		this.topAdStartDate = topAdStartDate;
	}
	
	@Field(name="top_ad_end_date",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "top_ad_end_date")
	public Date getTopAdEndDate() {
		return topAdEndDate;
	}

	public void setTopAdEndDate(Date topAdEndDate) {
		this.topAdEndDate = topAdEndDate;
	}

	@Field(name="more_info",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "more_info")
	public Boolean getMoreInfo() {
		if (moreInfo == null){
			return Boolean.FALSE;
		}else{
			return this.moreInfo;
		}
	}

	public void setMoreInfo(Boolean moreInfo) {
		this.moreInfo = moreInfo;
	}
	
	@Field(name="seasonal_images",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "seasonal_images")
	public Boolean getSeasonalImages() {
		if (seasonalImages == null){
			return Boolean.FALSE;
		}else{
			return this.seasonalImages;
		}
	}

	public void setVdayBackground(String vdayBackground) {
		this.vdayBackground = vdayBackground;
	}
	
	@Field(name="vday",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "vday_background")
	public String getVdayBackground() {
		return this.vdayBackground;
	}
	
	public void setSpringBackground(String springBackground) {
		this.springBackground = springBackground;
	}
	
	@Field(name="spring",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "spring_background")
	public String getSpringBackground() {
		return this.springBackground;
	}
	
	public void setMdayBackground(String mdayBackground) {
		this.mdayBackground = mdayBackground;
	}
	
	@Field(name="mday",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "mday_background")
	public String getMdayBackground() {
		return this.mdayBackground;
	}
	
	public void setSummerBackground(String summerBackground) {
		this.summerBackground = summerBackground;
	}
	
	@Field(name="summer",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "summer_background")
	public String getSummerBackground() {
		return this.summerBackground;
	}
	
	public void setFallBackground(String fallBackground) {
		this.fallBackground = fallBackground;
	}
	
	@Field(name="fall",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "fall_background")
	public String getFallBackground() {
		return this.fallBackground;
	}
	
	public void setWinterBackground(String winterBackground) {
		this.winterBackground = winterBackground;
	}
	
	@Field(name="winter",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "winter_background")
	public String getWinterBackground() {
		return this.winterBackground;
	}

	public void setSeasonalImages(Boolean seasonalImages) {
		this.seasonalImages = seasonalImages;
	}
	
	@Field(name="featured_listing",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "featured_listing")
	public Boolean getFeaturedListing() {
		if(featuredListing == null)
			return Boolean.FALSE;
		else
			return this.featuredListing;
	}

	public void setFeaturedListing(Boolean featuredListing) {
		this.featuredListing = featuredListing;
	}
	
	@Field(name="preferred_listing",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "preferred_listing")
	public Boolean getPreferredListing() {
		return preferredListing;
	}

	public void setPreferredListing(Boolean preferredListing) {
		this.preferredListing = preferredListing;
	}

}
