package com.flowers.server.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="zip_listing", catalog = "bloomnet")
public class ZipListing  implements java.io.Serializable {
	private static final long serialVersionUID = -2092668176707596019L;
	private long id;
	private Shop shop;
	private String zipCode;
	private int active;
     
    public ZipListing() {
    }

    public ZipListing(Shop shop) {
    	this.shop = shop;
    }

    public ZipListing(Shop shop, String zipCode, int active) {
       this.shop = shop;
       this.active = active;
       this.zipCode = zipCode;
    }
   
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id", unique=true, nullable=false)
    public long getId() {
        return this.id;
    }
    
    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "shop_code", unique = true, nullable = false, referencedColumnName = "shop_code")
	public Shop getShop() {
		return this.shop;
	}
    
    public void setShop(Shop shop) {
        this.shop = shop;
    }

    @Column(name="zip_code", length=10)
    public String getZipCode() {
        return this.zipCode;
    }
    
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Column(name="active", length=1)
    public int getActive() {
        return this.active;
    }
    
    public void setActive(int active) {
        this.active = active;
    }
}


