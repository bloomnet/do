package com.flowers.server.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.ClassBridge;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;
import org.hibernate.search.annotations.TermVector;

import com.flowers.server.search.SideAdFieldBridge;

@Entity
@Indexed
@ClassBridge(name="sideAdBridge",index=Index.YES,store=Store.YES,impl =SideAdFieldBridge.class)
@Table(name = "side_ad")
public class SideAd implements java.io.Serializable {
	private static final long serialVersionUID = -1174961730852856327L;
	private long id;
	private String shopCode;
	private String fileName;

		
	public SideAd() {
	}

	public SideAd(long id, String shopCode) {
		this.id = id;
		this.shopCode = shopCode;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public void setShopCode(String shopCode){
		this.shopCode = shopCode;
	}

	@Field(name="shop_code",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "shopCode", length = 8)
	public String getShopCode() {
		return this.shopCode;
	}
	
	public void setFileName(String fileName){
		this.fileName = fileName;
	}

	@Field(name="file_name",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "fileName")
	public String getFileName() {
		return this.fileName;
	}
	

}

