package com.flowers.server.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.ClassBridge;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;
import org.hibernate.search.annotations.TermVector;

import com.flowers.server.search.ConsumerLandingAdsFieldBridge;
import com.flowers.server.search.ConsumerResultsAdFieldBridge;

@Entity
@Indexed
@ClassBridge(name="landingAdsBridge",index=Index.YES,store=Store.YES,impl =ConsumerLandingAdsFieldBridge.class)
@Table(name = "consumer_landing_ad")
public class ConsumerLandingAd implements java.io.Serializable {
	private static final long serialVersionUID = -1174961730852856327L;
	private long id;
	private String fileName;
	private String website;
	private String adShape;

		
	public ConsumerLandingAd() {
	}

	public ConsumerLandingAd(long id, String fileName) {
		this.id = id;
		this.fileName = fileName;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public void setFileName(String fileName){
		this.fileName = fileName;
	}

	@Field(name="file_name",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "file_name", length = 255)
	public String getFileName() {
		return this.fileName;
	}

	@Field(name="website",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "website", length = 255)
	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}
	
	@Field(name="adShape",store=Store.YES,index=Index.YES,termVector=TermVector.NO,analyze=Analyze.NO)
	@Column(name = "ad_shape", length = 1)
	public String getAdShape() {
		return adShape;
	}

	public void setAdShape(String adShape) {
		this.adShape = adShape;
	}
	

}

