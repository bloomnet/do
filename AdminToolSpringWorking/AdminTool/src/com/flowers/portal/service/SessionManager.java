package com.flowers.portal.service;

import javax.servlet.http.HttpServletRequest;


//import com.bloomnet.bom.common.entity.Bomorder;
public interface SessionManager {

	//TODO: Fix incoming and outgoing types 
	Object getApplicationUser(HttpServletRequest request);
	void setApplicationUser(HttpServletRequest request, Object applicationUser);

	//TODO: Fix incoming and outgoing types 
	Object getOrder(HttpServletRequest request);
	void setOrder(HttpServletRequest request,Object masterOrder);
	void removeOrder(HttpServletRequest request);
	
	
	
	/*List<? extends Bomorder> getSearchResults( HttpServletRequest request );
	void setSearchResults( HttpServletRequest request , List<? extends Bomorder> searchResults );*/

	/**
	 * Removes all of the object wich the application might have had kept in
	 * session.  This method is usefull for the sign out user action. This method
	 * checks for all methods starting with "remove".
	 *
	 * @param request
	 */
	void deleteAllApplicationObjectsFromSession(HttpServletRequest request);
	
	
	void removeSelectedDate(HttpServletRequest request);
	void setSelectedDate(HttpServletRequest request, Object masterOrder);
	Object getSelectedDate(HttpServletRequest request);
	Object getSelectedEndDate(HttpServletRequest request);
	void setSelectedEndDate(HttpServletRequest request, Object masterOrder);
	void removeSelectedEndDate(HttpServletRequest request);
}