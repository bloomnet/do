/**
 * 
 */
package com.flowers.portal.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.flowers.portal.exception.UserNotFoundException;
import com.flowers.portal.exception.UserWrongPasswordException;
import com.flowers.portal.exception.WorkingShopNotFoundException;
import com.flowers.portal.service.AuthUserDetailsService;
import com.flowers.server.entity.User;
import com.flowers.server.entity.Userrole;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Service("authService")
@Transactional
public class DbUserauthenticator implements AuthUserDetailsService {
	
    // Define a static logger variable
    static Logger logger = Logger.getLogger( DbUserauthenticator.class );
    
    @Autowired private HibernateTemplate hibernateTemplate;
    
    final String salt = "gdB!2D!4G*7vV9";

	public DbUserauthenticator(){}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.AuthUserDetailsService#authenticate(org.springframework.security.core.userdetails.User)
	 */

	/**
	 * Validates a WebAppUser.
	 * 
	 * @param user
	 * @return
	 * @throws Exception
	 */
/*	@SuppressWarnings("unchecked")
	private WebAppUser validateUser( WebAppUserInterface formUser,
			                         UserInterface dbUser ) throws UserWrongPasswordException {
		
		if( formUser.getPassword().equalsIgnoreCase(dbUser.getPassword())){
			
			HashMap<String, String> propMap = new HashMap<String, String>((Map) viewAccess);
		    Set<Map.Entry<String, String>> propSet;
		    propSet = propMap.entrySet();
			return (WebAppUser) populateUserViews(dbUser, propSet);
		}
		else {
			
			throw new UserWrongPasswordException();
		}
	}
	
	private WebAppUserInterface populateUserViews( UserInterface dbUser, 
										           Set<Map.Entry<String, String>> propSet) {
		
		Set<ApplicationView> allowedViews = new HashSet<ApplicationView>();
		Set<Userrole>        userRoles    = dbUser.getUserrolesForUserId();
		
	    for (Map.Entry<String, String> me : propSet) {
	    	
	    	ApplicationView view = new ApplicationView();
	    	view.setUrl(me.getKey());
	    	
	    	String[] roleNames = me.getValue().split(",");
	        
	    	for( int i = 0; i < roleNames.length; i++ ) {
	    		
	    		String definedRoleDescription = roleNames[i];
	    		
	    		for(Userrole userRole : userRoles){
	    			
	    			if(definedRoleDescription.equalsIgnoreCase(userRole.getRole().getDescription())){
	    				allowedViews.add(view);
	    			}
	    		}
	    	}
	    }
	    
	    // Construct the web application user
	    WebAppUser webAppUser = new WebAppUser();
	    webAppUser.initialize( dbUser );
	    webAppUser.setUserViews( allowedViews );
	    
		return webAppUser;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void updateUserDetails( UserInterface user, 
								   Uactivitytype activity ) throws HibernateException {
		
		userDao.persistUserActivity((User) user, activity);
	}
*/
	
	public byte[] getSHA(String input) throws NoSuchAlgorithmException{ 
        MessageDigest md = MessageDigest.getInstance("SHA-256"); 
        return md.digest(input.getBytes(StandardCharsets.UTF_8)); 
	}
    
	public String toHexString(byte[] hash){
        BigInteger number = new BigInteger(1, hash); 
        StringBuilder hexString = new StringBuilder(number.toString(16)); 
        while (hexString.length() < 32) { 
            hexString.insert(0, '0'); 
        } 
        return hexString.toString(); 
	}
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.hibernateTemplate = new HibernateTemplate(sessionFactory);
	}
	public SessionFactory getSessionFactory() {
		return hibernateTemplate.getSessionFactory();
	}

	@SuppressWarnings({ "unchecked"})
	@Transactional
	@Override
	public User authenticate(User formUser) throws HibernateException,
			UserNotFoundException, UserWrongPasswordException,
			WorkingShopNotFoundException, Exception {
		
		String userName = formUser.getUserName();
		String password = toHexString(getSHA(salt+formUser.getPassword()));
		User user = null;
		if(userName != null && userName.length() > 0) {
			Session session = hibernateTemplate.getSessionFactory().getCurrentSession(); 
			List<User> userList = session.createQuery("from User where userName = :userName").setParameter("userName", userName).list();
			if(userList != null && userList.size() > 0)
				user = userList.get(0);
			if(user != null && user.getPassword().equals(password)) {
				List<Userrole> userRoleList = session.createQuery("from Userrole where User_ID = :userID and Role_ID = :roleID").setParameter("roleID", "1").setParameter("userID", user.getId()).list();
				Userrole userRole = null;
				if(userRoleList != null && userRoleList.size() > 0)
					userRole = userRoleList.get(0);
				if(userRole != null){
					return user;
				}else{
					throw new Exception ("You do not have permission to view this content");
				}
			}else{
				throw new UserNotFoundException( userName );
			}
		}
		return user;
	}
}
