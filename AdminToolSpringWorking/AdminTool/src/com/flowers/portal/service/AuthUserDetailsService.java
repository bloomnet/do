package com.flowers.portal.service;

import org.hibernate.HibernateException;

/*import com.bloomnet.bom.common.entity.Uactivitytype;
import com.bloomnet.bom.common.entity.UserInterface;
import com.bloomnet.bom.common.entity.Useractivity;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.businessobjects.WebAppUserInterface;*/
import com.flowers.portal.exception.UserNotFoundException;
import com.flowers.portal.exception.UserWrongPasswordException;
import com.flowers.portal.exception.WorkingShopNotFoundException;
import com.flowers.server.entity.User;


/**
 * A custom service for authenticating users against database.
 * 
 * @author Danil Svirchtchev
 */
public interface AuthUserDetailsService {
    
    /*@SuppressWarnings("rawtypes")
    static final Comparator ACTIVITIES_BY_DATE = new Comparator () {
        
        public int compare ( Object o1, Object o2 ) {
            
            Useractivity v1 = (Useractivity)o1;
            Useractivity v2 = (Useractivity)o2;
            
            return v1.getCreatedDate ().compareTo ( v2.getCreatedDate() );
        }
    };*/

	/**
	 * Retrieves a user record containing the user's credentials and access.
	 *
	 * @param user User entity
	 * @return User if found, null if not
	 * @throws Exception User exception
	 */
	 User authenticate(User formUser) throws HibernateException,
															 UserNotFoundException,
															 UserWrongPasswordException,
															 WorkingShopNotFoundException, Exception;
	
//	void updateUserDetails ( UserInterface user, Uactivitytype activity ) throws HibernateException;
	
//	List<Useractivity> getUserActivities ( Long userId ) throws HibernateException;
}
