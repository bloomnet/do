package com.flowers.portal.validators;

import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.flowers.server.entity.User;

/*import com.bloomnet.bom.common.entity.UserInterface;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;*/

/**
 * Validator for SignInController
 * 
 * @author Danil Svirchtchev
 */
public class SignInValidator extends AbstractValidator {

	// Define a static logger variable
	static Logger logger = Logger.getLogger(SignInValidator.class);

	public boolean supports(Class<?> clazz) {

		return clazz.equals(User.class);
	}

	/**
	 * Validates the ApplicationUser command object. Ensures that a password and
	 * username are specified.
	 * 
	 * @see com.bloomnet.bom.mvc.businessobjects.WebAppUser
	 */
	public void validateForm(Object target, Errors errors) {

		User user = (User) target;

		final String  username = user.getUserName();
		final String  password = user.getPassword();

		if ((username == null) || username.equals("")) {
			errors.rejectValue("userName", "error.login.no.username");
		}

		if ((password == null) || password.equals("")) {
			errors.rejectValue("password", "error.login.no.password");
		}
	}
}
