package com.flowers.portal.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/*import com.bloomnet.bom.mvc.businessobjects.CommandClassInterface;
*/
/**
 * 
 * @author Danil Svirchtchev
 */
public abstract class AbstractValidator implements Validator {

	/**
	 * Method to be overridden by subclasses
	 * 
	 * @param target
	 * @param errors
	 * @throws Exception
	 */
	protected abstract void validateForm( Object target, Errors errors ) throws Exception;

    /**
     * Validate the supplied <code>target</code> object, which must be of a
     * {@link Class}for which the {@link #supports(Class)}method typically has
     * (or would) return <code>true</code>.
     * <p>
     * The supplied {@link Errors errors}instance can be used to report any
     * resulting validation errors.
     * 
     * @param target the object that is to be validated (can be <code>null</code>)
     * @param errors contextual state about the validation process (never <code>null</code>)
     *
     * @see ValidationUtils
     */
	@Override
	public void validate( Object target, Errors errors ) {
		
        if ( target == null ) {
            return;
        } 
        else {

           // CommandClassInterface formData = (CommandClassInterface) target;
            
           // if ( target instanceof  CommandClassInterface ) {

                try {

                    validateForm( target, errors );
                } 
                catch ( Exception ex ) {

                	final String message = "General validation exception";
                    errors.reject( "java.exception.message", new Object[] { message }, null );
                }
          /*  } 
            else {
            	
            	final String message = "Wrong object type is passed to a validator";
            	errors.reject( "java.exception.message", new Object[] { message }, null );
            }*/
        }
	}
}
