package com.flowers.portal.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

//import com.bloomnet.bom.common.entity.Uactivitytype;
//import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.flowers.portal.exception.UserWrongPasswordException;
import com.flowers.portal.exception.WorkingShopNotFoundException;
import com.flowers.portal.exception.UserNotFoundException;

import com.flowers.portal.service.AuthUserDetailsService;
import com.flowers.portal.service.SessionManager;
import com.flowers.portal.validators.SignInValidator;
import com.flowers.server.entity.User;

@Controller
@Transactional
public class SignInController {

    // Define a static logger variable
    static Logger logger = Logger.getLogger( SignInController.class );
    
    @Autowired private SessionManager sessionManager;
    @Autowired private SignInValidator signInValidator;
    @Autowired private AuthUserDetailsService authService;


    @RequestMapping(value = "/signin.htm", method = RequestMethod.POST)
    public String processSubmit( HttpServletRequest request,
    							 @ModelAttribute("command") User webAppUser, 
    		                     BindingResult result, 
    		                     SessionStatus status ) {
    	
    	String nextView = "signin";
    	
    	signInValidator.validateForm(webAppUser, result);
        
        if (result.hasErrors()) {
        	// do nothing
        } else {

        	try {

        		@SuppressWarnings("unused")
				final Date now = new Date();
				 User authenticatedUser = authService.authenticate(webAppUser);    		
				sessionManager.setApplicationUser(request, authenticatedUser);
				if(authenticatedUser != null)
					nextView = "redirect:/Admin";
				else
					nextView = "redirect:signin.htm";
				
			} catch (HibernateException e) {
				result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
				logger.error(e);
				e.printStackTrace();
			} catch (UserNotFoundException e) {
				result.reject("error.login.exception", new Object[] { e.getMessage() }, null );
				logger.info(e);
			} catch (UserWrongPasswordException e) {
				result.reject("error.login.exception", new Object[] { e.getMessage() }, null );
			} catch (WorkingShopNotFoundException e) {
				result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
				logger.error(e);
	        } catch (Exception e) {
	        	result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
				logger.error(e);
			}
        }
        
        return nextView;
    }
    
	@RequestMapping(value = "/signin.htm", method = RequestMethod.GET)
	public String handleRequest( HttpServletRequest request,
			                     Model model ) {
		
		String nextView = "signin";
		
		if( sessionManager.getApplicationUser(request) != null ) {
		    
			nextView = "redirect:/Admin";			
		} 
		else {
		    
			//WebAppUser user = WebAppUser.getInstance();		
			model.addAttribute( "command", new User() );
		}
	
		return nextView;
	}
	
	
	
	@SuppressWarnings("unused")
	private String getClientIpAddr(HttpServletRequest request) {   
        String ip = request.getHeader("X-Forwarded-For");   
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {   
            ip = request.getHeader("Proxy-Client-IP");   
        }   
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {   
            ip = request.getHeader("WL-Proxy-Client-IP");   
        }   
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {   
            ip = request.getHeader("HTTP_CLIENT_IP");   
        }   
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {   
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");   
        }   
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {   
            ip = request.getRemoteAddr();   
        }   
        return ip;   
    }  
	
	
}
