package com.flowers.portal.controller;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import com.flowers.portal.service.SessionManager;
import com.flowers.portal.vo.ApplicationView;
import com.flowers.server.entity.User;
import com.flowers.server.entity.Userrole;


public class HttpRequestInterceptor implements HandlerInterceptor  {
    
    
    // Define a static logger variable
    static final Logger logger = Logger.getLogger( HttpRequestInterceptor.class );
    
    
    private String         signInPage;
    @Autowired private SessionManager sessionManager;
	@Autowired private Properties viewAccess;
    @Autowired private HibernateTemplate hibernateTemplate;


    
    
    /**
     * Uses SessionManager to ensure user is logged in; if not, then
     * user is forwarded to the sign-in page.
     *
     * @see SimpleHttpSessionManagerImpl
     */
    @Override
    public boolean preHandle( HttpServletRequest request, 
                              HttpServletResponse response, 
                              Object handler) throws Exception {
        
        final boolean result;
        
        System.out.println("Pre-handle");
        
        User user = (User) sessionManager.getApplicationUser(request);
        
        if ( user == null ) {
            
            response.sendRedirect( signInPage );
            result = false;
        }
        else {
            
                result = true;
        }
        
        return result;
    }
    

    @SuppressWarnings("unused")
	private boolean isViewAllowed(String requestURI, User user) {


    	@SuppressWarnings({ "rawtypes", "unchecked" })
		HashMap<String, String> propMap = new HashMap<String, String>((Map) viewAccess);
    	Set<Map.Entry<String, String>> propSet;
    	propSet = propMap.entrySet();

    	Set<ApplicationView> allowedViews = new HashSet<ApplicationView>();
    	Set<Userrole>        userRoles    = getUserrolesForUserId(user);
    	Set<ApplicationView> userViews = null;



    	for (Map.Entry<String, String> me : propSet) {

    		ApplicationView view = new ApplicationView();
    		view.setUrl(me.getKey());

    		String[] roleNames = me.getValue().split(",");

    		for( int i = 0; i < roleNames.length; i++ ) {

    			String definedRoleDescription = roleNames[i];

    			for(Userrole userRole : userRoles){

    				if(definedRoleDescription.equalsIgnoreCase(userRole.getRole().getDescription())){
    					allowedViews.add(view);
    				}
    			}
    		}
    	}

    	boolean b = false;

    	if ( userViews != null ) {

    		for ( ApplicationView view : userViews ) {

    			if ( requestURI.indexOf ( view.getUrl () ) != -1 ) {

    				b = true;
    			}
    		}
    	}

    	return b;


    }
    
    @Override    public void postHandle(HttpServletRequest request,HttpServletResponse response,
    		Object handler,ModelAndView modelAndView) throws Exception {        
    	
    	System.out.println("Post-handle");    }         
    
    
    
    
    
    @Override    public void afterCompletion(HttpServletRequest request,  HttpServletResponse response, Object handler, Exception ex) throws Exception 
    {       
    	System.out.println("After completion handle");    }


	


	private Set<Userrole> getUserrolesForUserId(User user) {

		Set<Userrole> userRoles = new HashSet<Userrole>();
		long id = user.getId();
		
		Session session = hibernateTemplate.getSessionFactory().getCurrentSession(); 
		@SuppressWarnings("unchecked")
		List<Userrole> results = (List<Userrole>) session.createQuery("from Userrole u where u.userByUserId = :id").setParameter("id", id).list();

		for (Userrole role: results){
			userRoles.add(role);
		}
		
		return userRoles;
	}


	///////////////////////////////////////////////////////////////////////////
    //
    // START SETTERS AND GETTERS FOR SPRING DEPENDENCY INJECTION
    //
    public String getSignInPage() {        
        return signInPage;
    }
    public void setSignInPage(String signInPage) {
        this.signInPage = signInPage;
    }
    
    public SessionManager getSessionManager() {
        return sessionManager;
    }
    public void setSessionManager(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }
}
