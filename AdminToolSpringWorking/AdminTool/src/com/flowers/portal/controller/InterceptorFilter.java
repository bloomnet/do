package com.flowers.portal.controller;

import java.io.IOException;
 
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import com.flowers.server.entity.User;

@Component
public class InterceptorFilter implements Filter {
 
    public void doFilter(ServletRequest req, ServletResponse res,
            FilterChain chain) throws IOException, ServletException {
 
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        User user = (User) request.getSession(true).getAttribute( "USER" );
        if(user == null){
        	response.sendRedirect("signin.htm");
        	return;
        }
        
        chain.doFilter(req, res);

    }
    public void init(FilterConfig config) throws ServletException {
         
    }
    public void destroy() {
        //add code to release any resource
    }
}
	

