/**
 * 
 */
package com.flowers.portal.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.flowers.portal.service.SessionManager;
import com.flowers.server.entity.User;





/**
 * Controller for "signing out" of the application.
 *
 * @author Danil Svirchtchev
 */
@Controller
@RequestMapping("/signout.htm")
public class SignOutController {
	
    // Define a static logger variable
    static Logger logger = Logger.getLogger( SignOutController.class );
    
    @Autowired private SessionManager sessionManager;
   // @Autowired private Uactivitytype signOutActivity;
    
	@RequestMapping(method = RequestMethod.GET)
	public String handleRequest( HttpServletRequest request,
								 ModelMap model ) {
		
        try {
            
            User user = (User) sessionManager.getApplicationUser(request);
    	
            if( user != null ) {
            	
            	@SuppressWarnings("unused")
				final Date now = new Date();
            	
            	// Store user activity in the db when user is out.
            	//user.setSignOutTime( now );          	
              //  authService.updateUserDetails(user,signOutActivity);
            }
        } 
        catch (HibernateException ex) {
            logger.error( ex );
        }
        finally {
        	
            sessionManager.deleteAllApplicationObjectsFromSession(request);
            request.getSession().invalidate();
        }
		
		return "redirect:signin.htm";
	}
}
