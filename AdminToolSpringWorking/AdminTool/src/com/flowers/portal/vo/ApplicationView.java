package com.flowers.portal.vo;

import java.io.Serializable;

/**
 * Representation of a view.
 *
 * @author Danil Svirchtchev
 */
public class ApplicationView implements Serializable {
    
    //default serial version id, required for serializable classes.
    private static final long serialVersionUID = 1L;
    
    private Integer id;
    private String  actionStr;
    private String  buttonName;
    private String  url;
    private String  viewDesc;
    private String  menuEnable;
    
    public ApplicationView () {
    }
    
    public ApplicationView ( String url, String description ) {
        this();
        this.url = url;
        this.viewDesc = description;
    }
    
    public Integer getId () {
        return this.id;
    }
    public void setId (Integer id) {
        this.id = id;
    }
    
    public String getActionStr () {
        return this.actionStr;
    }
    public void setActionStr (String actionStr) {
        this.actionStr = actionStr;
    }
    
    public String getButtonName () {
        return this.buttonName;
    }
    public void setButtonName (String buttonName) {
        this.buttonName = buttonName;
    }
    
    public String getUrl () {
        return this.url;
    }
    public void setUrl (String url) {
        this.url = url;
    }
    
    public String getViewDesc () {
        return this.viewDesc;
    }
    public void setViewDesc (String viewDesc) {
        this.viewDesc = viewDesc;
    }
    
    public String getMenuEnable () {
        return menuEnable;
    }
    public void setMenuEnable (String menuEnable) {
        this.menuEnable = menuEnable;
    }
    
    @Override
    public boolean equals ( Object other ) {
		if ( other == null ) {
		   return false;
		}
		else if ( other instanceof ApplicationView ) {
			return this.url.equals ( ( ( ApplicationView )other ).url);
		}
    	else return false;
    }
    
    @Override
	public int hashCode() {
    	return url.hashCode();
	}
    
    public String toString () {
        
        StringBuffer theString = new StringBuffer ();
        theString.append ("url=["+getUrl ()+"] :: viewDesc=["+getViewDesc ()+"]");
        return new String ( theString );
    }
}