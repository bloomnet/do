package com.flowers.portal.exception;

/**
 * @author Danil svirchtchev
 *
 */
public class WorkingShopNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;
	
	/**
	 * @param message
	 */
	public WorkingShopNotFoundException( String shopCode ) {
		super("Working shop "+shopCode+" was not found");
	}
}
