package com.flowers.client.address;

import java.util.Arrays;
import java.util.List;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.ui.base.BaseNestedForm;
import com.flowers.server.LookupException;
import com.flowers.server.entity.Address;
import com.flowers.server.entity.City;
import com.flowers.server.entity.Country;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Item;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.FieldEvents.FocusListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;

public class AddressNestedForm extends BaseNestedForm<Address> {
	private static final long serialVersionUID = -4878769106331389421L;
	private static final String COMMON_FIELD_WIDTH = "25em";
	private static final String SMALL_FIELD_WIDTH = "12em";
	private Form form;
	private ComboBox city;
	private FormLayout formLayout;

	public final String[] ADDRESS_FIELD_ORDER = new String[] { "city", "postalCode", "streetAddress1", "streetAddress2" };

	public AddressNestedForm(Address address) {
		super(address);
		formLayout = new FormLayout();
		formLayout.setMargin(false);
		formLayout.setSpacing(false);
		form = new Form(formLayout);
		setCompositionRoot(form);
		setReadOnly(true);
	}

	@Override
	public Address getBlankObject() {
		return new Address();
	}

	@Override
	public void attach() {
		super.attach();
		form.setCaption("Address");
		form.setWriteThrough(false);
		form.setFormFieldFactory(new AddressNestedFormFieldFactory());
		BeanItem<Address> bAddress = new BeanItem<Address>(getObject());
		if (!isNewObject()) {
			bAddress.addItemProperty("country", new ObjectProperty<Country>(
					getObject().getState().getCountry()));
		} else {
			bAddress.addItemProperty("country", new ObjectProperty<Country>(
					new Country()));
		}
		form.setItemDataSource(bAddress, Arrays.asList(ADDRESS_FIELD_ORDER));
		form.setReadOnly(true);
	}

	@Override
	public void commit() throws SourceException, InvalidValueException {
		super.commit();
		form.commit();
	}

	@Override
	public void discard() throws SourceException {
		super.discard();
		form.discard();
	}

	@Override
	public Class<?> getType() {
		return Address.class;
	}

	@Override
	public void setReadOnly(boolean readOnly) {
		super.setReadOnly(readOnly);
		form.setReadOnly(readOnly);
	}

	private class AddressNestedFormFieldFactory extends DefaultFieldFactory {
		/**
		 * 
		 */
		private static final long serialVersionUID = -3357130710580931610L;

		@Override
		public Field createField(Item item, Object propertyId,
				Component uiContext) {
			String pid = (String) propertyId;
			if ("city".equals(pid)) {
				return getCity();
			} else {
				Field field = super.createField(item, propertyId, uiContext);
				if (field instanceof TextField) {
					TextField txt = (TextField) field;
					if ("postalCode".equals(pid)) {
						txt.setWidth(SMALL_FIELD_WIDTH);
						txt.setMaxLength(10);
					} else if ("streetAddress1".equals(pid)) {
						txt.setWidth(COMMON_FIELD_WIDTH);
						txt.setMaxLength(50);
					} else if ("streetAddress2".equals(pid)) {
						txt.setWidth(COMMON_FIELD_WIDTH);
						txt.setMaxLength(50);
					}
					txt.setNullRepresentation("");
				}
				return field;
			}
		}
	}


	public ComboBox getCity() {
		if (city == null){
			city = new ComboBox("City");
			city.setWidth("325px");
			city.setRequired(true);
		}
		city.addListener(new FocusListener(){
			
			private static final long serialVersionUID = 5152784125968646974L;
			
			@Override
			public void focus(FocusEvent event) {
				if(city.size() <= 1){
					BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
					CrudService service = app.getBean("CrudService", CrudService.class);
					try {
						SingleThread st = new SingleThread();
						@SuppressWarnings("unchecked")
						List<City> cities = (List<City>) st.findAll("City", service);
						if (cities != null){
							for(City o:cities){
								Item item = city.addItem(o);
								if(item != null) {
									city.setItemCaption(o, o.getName()+", "+o.getState().getShortName());
								}
							}
						}
					} catch (LookupException e) {
						e.printStackTrace();
					} 
				}
				
			}
		});
		return city;
	}




}
