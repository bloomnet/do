package com.flowers.client.listings;

import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.BooleanQuery.Builder;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.City;
import com.flowers.server.entity.Listing;
import com.flowers.server.service.SearchService;
import com.vaadin.data.Item;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Table;

public class ListingTable extends Table implements TableContainer<Listing> {
	private static final long serialVersionUID = 4363624315568930161L;
	private SearchService searchService;
	
	public ListingTable() {
		addContainerProperty("shopCode", String.class, null);
		addContainerProperty("shopName", String.class, null);
		addContainerProperty("city", String.class, null);
		addContainerProperty("city exists", Boolean.class, null);
		addContainerProperty("seasonalImages", Boolean.class, null);
		setSizeFull();
		setHeight(500, AbstractComponent.UNITS_PIXELS);
		setSelectable(true);
		setImmediate(true);
		setNullSelectionAllowed(false);
	}
	protected void init() {
		if(searchService == null) {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			searchService = app.getBean("SearchService", SearchService.class);
		}
	}
	public Item addItemToContainer(Listing t) {
		return null;
	}

	public Item addDocumentToContainer(Document d) {
		init();
		Long id = Long.valueOf(d.get("id"));
		Item item = this.addItem(id);
		if(item != null) {
			String city = d.get("city");
			String state = d.get("state");
			String seasonalImages = d.get("seasonal_images");
			Builder query = new BooleanQuery.Builder();
			query.add(new BooleanClause(new TermQuery(new Term("name", city)), BooleanClause.Occur.MUST));
			query.add(new BooleanClause(new TermQuery(new Term("state.name", state)), BooleanClause.Occur.MUST));
			List<Document> cities = searchService.documents(City.class, query.build(), new SortField("name", SortField.Type.STRING));
			item.getItemProperty("shopCode").setValue(d.get("shopcode"));
			item.getItemProperty("shopName").setValue(d.get("shopname"));
			item.getItemProperty("city").setValue(city);
			item.getItemProperty("city exists").setValue(cities != null && cities.size() > 0);
			item.getItemProperty("seasonalImages").setValue(seasonalImages != null && seasonalImages.equalsIgnoreCase("True"));
		}
		return item;
	}

	public void selectRow(Listing t) {
	}

}
