package com.flowers.client.listings;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.server.entity.City;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.Zip;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.terminal.FileResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.ProgressIndicator;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FinishedEvent;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class ListingZipCodeRemovalWindow extends Window {
	private static final long serialVersionUID = 4832691384085398012L;
	private SizeCountingReceiver counter;
	private Upload upload;
	private Label state = new Label();
    private Label result = new Label();
    private Label fileName = new Label();
    private Label textualProgress = new Label();
    
    private ProgressIndicator pi = new ProgressIndicator();

    private String uploadPath = "/opt/data/listing_zip_export";
	
	public ListingZipCodeRemovalWindow() {
		center();
		VerticalLayout layout = (VerticalLayout) getContent();
        layout.setMargin(true);
        layout.setSpacing(true); 
        layout.setWidth("350px");
        layout.setHeight("300px");
        
        counter = new SizeCountingReceiver();
        upload = new Upload("", counter);
        addComponent(new Label("Select a file to export zip codes from."));
        
        upload.setImmediate(true);
        upload.setButtonCaption("Select Source File");
        addComponent(upload);

        final Button cancelProcessing = new Button("Cancel");
        cancelProcessing.addListener(new Button.ClickListener() {
        	private static final long serialVersionUID = 1L;

			public void buttonClick(ClickEvent event) {
        		upload.interruptUpload();
        	}
        });
        cancelProcessing.setVisible(false);
        cancelProcessing.setStyleName("small");

        Panel p = new Panel("Status");
        p.setSizeUndefined();
        p.setWidth(300, HorizontalLayout.UNITS_PIXELS);
        FormLayout l = new FormLayout();
        l.setMargin(true);
        p.setContent(l);
        HorizontalLayout stateLayout = new HorizontalLayout();
        stateLayout.setSpacing(true);
        stateLayout.addComponent(state);
        stateLayout.addComponent(cancelProcessing);
        stateLayout.setCaption("Current state");
        state.setValue("Idle");
        l.addComponent(stateLayout);
        fileName.setCaption("File name");
        l.addComponent(fileName);
        result.setCaption("bytes");
        l.addComponent(result);
        pi.setCaption("Progress");
        pi.setVisible(false);
        l.addComponent(pi);
        textualProgress.setVisible(false);
        l.addComponent(textualProgress);

        addComponent(p);

        upload.addListener(new Upload.StartedListener() {
        	private static final long serialVersionUID = 1084163386895143768L;

			public void uploadStarted(StartedEvent event) {
        		// this method gets called immediatedly after upload is
        		// started
        		pi.setValue(0f);
        		pi.setVisible(true);
        		pi.setPollingInterval(500); // hit server frequently to get updates to client
        		textualProgress.setVisible(true);
        		state.setValue("Exporting");
        		fileName.setValue(event.getFilename());

        		cancelProcessing.setVisible(true);
        	}
        });

        upload.addListener(new Upload.ProgressListener() {
        	private static final long serialVersionUID = 3318393640870913379L;

			public void updateProgress(long readBytes, long contentLength) {
        		// this method gets called several times during the update
        		pi.setValue(readBytes / (float) contentLength);
        		textualProgress.setValue("Processed " + readBytes + " bytes of " + contentLength);
        		result.setValue(counter.getTotal());
        	}

        });
        upload.addListener(new Upload.SucceededListener() {
        	private static final long serialVersionUID = -4135230597799242206L;

			public void uploadSucceeded(SucceededEvent event) {
        		result.setValue(counter.getTotal() + " (total)");
        	}
        });
        upload.addListener(new Upload.FailedListener() {
        	private static final long serialVersionUID = -8754773108937834980L;

			public void uploadFailed(FailedEvent event) {
        		result.setValue(counter.getTotal()
        				+ " (counting interrupted at "
        				+ Math.round(100 * (Float) pi.getValue()) + "%)");
        	}
        });
        upload.addListener(new Upload.FinishedListener() {
        	private static final long serialVersionUID = -2421647485010631615L;

			public void uploadFinished(FinishedEvent event) {
        		state.setValue("Idle");
        		pi.setVisible(false);
        		textualProgress.setVisible(false);
        		cancelProcessing.setVisible(false);
        		doImport(event.getFilename());
        	}
        });
	}
	protected void doImport(String filename) {
		try {
			
			HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(uploadPath+"/"+filename));
			HSSFSheet sheet = workbook.getSheet("Sheet1");
			
			Iterator<Row> sheetIterator = sheet.rowIterator();
			
			boolean isFirst = true;
			int lineNumber = 0;
			int totalLines = sheet.getPhysicalNumberOfRows()-1;
			System.out.println("Processing "+(totalLines)+" Shops");
			
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			
			File outFile = new File("/opt/data/listing_zip_export/"+filename.replaceAll(".xls","")+"_Results.csv");
			Writer out = new BufferedWriter(new FileWriter(outFile));
			
			while(sheetIterator.hasNext()){
				lineNumber++;
				HSSFRow row = (HSSFRow)sheetIterator.next();
				String shopCode = getCellValue(row, 0);
				String city = getCellValue(row, 1);
				String state = getCellValue(row, 3);
				String country = getCellValue(row, 4);
				
				String pql = "from City c where c.name='"+city+"' and c.state.name='"+state+"' and c.state.country.shortName='"+country+"' and c.cityType='D'";
				String pql2 = "from City c where c.name='"+city+"' and c.state.name='"+state+"' and c.state.country.shortName='"+country+"' and c.cityType='A'";
				String zipPql = "from Shop s where s.shopCode='"+shopCode+"'";
				
				if (isFirst){
					isFirst = false;
					continue;
				}
				if (!isRowValid(row)){
					System.out.println("InvalidRow "+lineNumber);
					continue;
				}
				try {
					
					String line = shopCode+",";
					City cityObj = (City) st.findSingleResult(pql, service);
					City cityObj2 = (City) st.findSingleResult(pql2, service);
					
					if(cityObj != null || cityObj2 != null ){
						
						Set<Zip> zips = null;
						
						if(cityObj != null){
							zips = cityObj.getZips();
							if(zips.size() == 0 && cityObj2 != null){
								zips = cityObj2.getZips();
							}
						}else if(cityObj2 != null){
							zips = cityObj2.getZips();
						}
						
						List<Zip> zipArray = new ArrayList<Zip>(zips);
						
						Shop shop = (Shop) st.findSingleResult(zipPql, service);
						boolean ownZip = false;
						
						for(int ii=0; ii<zipArray.size(); ++ii){
							if(shop == null){
								if(ii == zipArray.size() - 1) line += zipArray.get(ii).getZipCode();
								else line += zipArray.get(ii).getZipCode() + ","; 
							}else if(!zipArray.get(ii).getZipCode().equals(shop.getAddress().getPostalCode())){
								if(ii == zipArray.size() - 1) line += zipArray.get(ii).getZipCode();
								else line += zipArray.get(ii).getZipCode() + ","; 
							}else if(zipArray.size() == 1){
								ownZip = true;
							}
						}
						
						if(!ownZip && zipArray.size() != 0) out.write(line+"\n");
						
						if(lineNumber % 50 == 0) System.out.println(lineNumber+" processed");
						
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			out.flush();
			out.close();
			FileResource fileResource = new FileResource(outFile, (BloomNetAdminApplication)getApplication());
			getWindow().getParent().open(fileResource);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	protected boolean isRowValid(HSSFRow row){
		Iterator<Cell> cellIterator = row.cellIterator();
		List<Boolean> validators = new ArrayList<Boolean>();
		while (cellIterator.hasNext()){
			Cell cell = cellIterator.next();
			validators.add(!isCellContentNull(cell));
		}
		for (Boolean b:validators){
			if (b.booleanValue()){
				return true;
			}
		}
		return false;
	}
	protected boolean isCellContentNull(Cell cell){
		if (cell == null){
			return true;
		}
		switch ( cell.getCellType() ) {
        case HSSFCell.CELL_TYPE_BLANK:
        	return true;
        case HSSFCell.CELL_TYPE_STRING:
        	return (cell.getStringCellValue() ==null) || (cell.getStringCellValue().trim().length() == 0);
        default:
        	return true;
		}
	}
	public void attach() {
		
	}
	public class SizeCountingReceiver implements Receiver {
        private static final long serialVersionUID = -5392491238827740383L;
		private String fileName;
        private String mtype;
        private int total;
                
        public OutputStream receiveUpload(String filename, String MIMEType) {
            total = 0;
            fileName = filename;
            mtype = MIMEType;
            try {
            	return new FilterOutputStream(new FileOutputStream(uploadPath+"/"+filename) {
            		public void write(int b) throws IOException {
            			super.write(b);
            			total++;
            		}
            	});               
            } catch(Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        public String getFileName() {
            return fileName;
        }
        public String getMimeType() {
            return mtype;
        }
		public int getTotal() {
			return total;
		}
        
    }
	protected String getCellValue(HSSFRow row, int index) {
		Cell cell = row.getCell(index);
		if (cell == null) return null;
		cell.setCellType(HSSFCell.CELL_TYPE_STRING);
		String value = cell.getStringCellValue().trim();
		if(value == null || value.length() == 0) return null;
		return value;
	}
}
