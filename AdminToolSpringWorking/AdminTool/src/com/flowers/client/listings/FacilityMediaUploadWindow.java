package com.flowers.client.listings;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.PersistenceException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.transaction.annotation.Transactional;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.server.LookupException;
import com.flowers.server.entity.City;
import com.flowers.server.entity.FacilityListing;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.ZipListing;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.impl.SingleThread;
import com.flowers.server.util.SQLData;
import com.flowers.server.util.SizeCountingReceiver;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.ProgressIndicator;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FinishedEvent;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;


public class FacilityMediaUploadWindow extends Window {
	private static final long serialVersionUID = -359167762053227360L;
	private SizeCountingReceiver counter;
	private Upload upload;
	private Label state = new Label();
    private Label result = new Label();
    private Label fileName = new Label();
    private Label textualProgress = new Label();
    
    private SQLData mySQL = null;
    
    private List<Long> shopsToIndex;
    private List<Long> listingsToIndex;
    private List<Long> citiesToIndex;
    
    private ProgressIndicator pi = new ProgressIndicator();

    private String uploadPath = "/opt/data/import";
	
	public FacilityMediaUploadWindow() {
		center();
		VerticalLayout layout = (VerticalLayout) getContent();
        layout.setMargin(true);
        layout.setSpacing(true); 
        layout.setWidth("350px");
        layout.setHeight("300px");
        
        counter = new SizeCountingReceiver();
        upload = new Upload("", counter);
        addComponent(new Label("Upload a file to your collection."));
        
        upload.setImmediate(true);
        upload.setButtonCaption("Upload File");
        addComponent(upload);

        final Button cancelProcessing = new Button("Cancel");
        cancelProcessing.addListener(new Button.ClickListener() {
        	private static final long serialVersionUID = 1L;

			public void buttonClick(ClickEvent event) {
        		upload.interruptUpload();
        	}
        });
        cancelProcessing.setVisible(false);
        cancelProcessing.setStyleName("small");

        Panel p = new Panel("Status");
        p.setSizeUndefined();
        p.setWidth(300, HorizontalLayout.UNITS_PIXELS);
        FormLayout l = new FormLayout();
        l.setMargin(true);
        p.setContent(l);
        HorizontalLayout stateLayout = new HorizontalLayout();
        stateLayout.setSpacing(true);
        stateLayout.addComponent(state);
        stateLayout.addComponent(cancelProcessing);
        stateLayout.setCaption("Current state");
        state.setValue("Idle");
        l.addComponent(stateLayout);
        fileName.setCaption("File name");
        l.addComponent(fileName);
        result.setCaption("bytes");
        l.addComponent(result);
        pi.setCaption("Progress");
        pi.setVisible(false);
        l.addComponent(pi);
        textualProgress.setVisible(false);
        l.addComponent(textualProgress);

        addComponent(p);

        upload.addListener(new Upload.StartedListener() {
        	private static final long serialVersionUID = 1084163386895143768L;

			public void uploadStarted(StartedEvent event) {
        		// this method gets called immediatedly after upload is
        		// started
        		pi.setValue(0f);
        		pi.setVisible(true);
        		pi.setPollingInterval(500); // hit server frequently to get
        		textualProgress.setVisible(true);
        		// updates to client
        		state.setValue("Uploading");
        		fileName.setValue(event.getFilename());

        		cancelProcessing.setVisible(true);
        	}
        });

        upload.addListener(new Upload.ProgressListener() {
        	private static final long serialVersionUID = 3318393640870913379L;

			public void updateProgress(long readBytes, long contentLength) {
        		// this method gets called several times during the update
        		pi.setValue(readBytes / (float) contentLength);
        		textualProgress.setValue("Processed " + readBytes + " bytes of " + contentLength);
        		result.setValue(counter.getTotal());
        	}

        });
        upload.addListener(new Upload.SucceededListener() {
        	private static final long serialVersionUID = -4135230597799242206L;

			public void uploadSucceeded(SucceededEvent event) {
        		result.setValue(counter.getTotal() + " (total)");
        	}
        });
        upload.addListener(new Upload.FailedListener() {
        	private static final long serialVersionUID = -8754773108937834980L;

			public void uploadFailed(FailedEvent event) {
        		result.setValue(counter.getTotal()
        				+ " (counting interrupted at "
        				+ Math.round(100 * (Float) pi.getValue()) + "%)");
        	}
        });
        upload.addListener(new Upload.FinishedListener() {
        	private static final long serialVersionUID = -2421647485010631615L;

			public void uploadFinished(FinishedEvent event) {
        		state.setValue("Idle");
        		pi.setVisible(false);
        		textualProgress.setVisible(false);
        		cancelProcessing.setVisible(false);
        		doImport(event.getFilename());
        	}
        });
	}
	protected void doImport(String filename) {
		try {
			
			mySQL = new SQLData();
			shopsToIndex = new ArrayList<Long>();
			listingsToIndex = new ArrayList<Long>();
			citiesToIndex = new ArrayList<Long>();
			
			HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(uploadPath+"/"+filename));
			HSSFSheet dirTabSheet = workbook.getSheet("DIRTAB");
			Iterator<Row> dirTabRowIterator = dirTabSheet.rowIterator();
			boolean isFirst = true;
			int lineNumber = 0;
			System.out.println("Processing "+(dirTabSheet.getPhysicalNumberOfRows()-1)+" Shops");
			while(dirTabRowIterator.hasNext()){
				lineNumber++;
				HSSFRow row = (HSSFRow)dirTabRowIterator.next();
				if (isFirst){
					isFirst = false;
					continue;
				}
				if (!isRowValid(row)){
					System.out.println("InvalidRow "+lineNumber);
					continue;
				}
				try {
					addOrUpdateShop(row);
					if(lineNumber % 50 == 0) System.out.println(lineNumber+" processed");					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			
			HSSFSheet dirSortSheet = workbook.getSheet("DIRSORT");
			Iterator<Row> dirSortRowIterator = dirSortSheet.rowIterator();
			isFirst = true;
			lineNumber = 0;
			System.out.println("Processing "+(dirSortSheet.getPhysicalNumberOfRows()-1)+" Listings");
			while(dirSortRowIterator.hasNext()){
				lineNumber++;
				HSSFRow row = (HSSFRow)dirSortRowIterator.next();
				if (isFirst){
					isFirst = false;
					continue;
				}
				if (!isRowValid(row)){
					System.out.println("InvalidRow "+lineNumber);
					continue;
				}
				try {
					addOrUpdateListing(row);
					if(lineNumber % 50 == 0) System.out.println(lineNumber+" processed");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			st.index(City.class,citiesToIndex,service);
			
			st.index(Shop.class,shopsToIndex,service);
			
			st.index(FacilityListing.class, listingsToIndex, service);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	protected boolean isRowValid(HSSFRow row){
		Iterator<Cell> cellIterator = row.cellIterator();
		List<Boolean> validators = new ArrayList<Boolean>();
		while (cellIterator.hasNext()){
			Cell cell = cellIterator.next();
			validators.add(!isCellContentNull(cell));
		}
		for (Boolean b:validators){
			if (b.booleanValue()){
				return true;
			}
		}
		return false;
	}
	protected boolean isCellContentNull(Cell cell){
		if (cell == null){
			return true;
		}
		switch ( cell.getCellType() ) {
        case HSSFCell.CELL_TYPE_BLANK:
        	return true;
        case HSSFCell.CELL_TYPE_STRING:
        	return (cell.getStringCellValue() ==null) || (cell.getStringCellValue().trim().length() == 0);
        default:
        	return true;
		}
	}
	public void attach() {
		
	}
	
	public void addOrUpdateShop(HSSFRow row) throws PersistenceException, LookupException, SQLException {
		
		String shopcode = getCellValue(row, 0);
		String cityname = getCellValue(row, 1);
		String shopname = getCellValue(row, 2);
		String statecode = getCellValue(row, 8);
		String zipcode = getCellValue(row, 11);
		String address1 = getCellValue(row, 6);
		String openSunday = getCellValue(row, 12);
		String bloomlinkIndicator = getCellValue(row, 13);
		String newShop = getCellValue(row, 23);
		String floristForForrests = getCellValue(row, 24);	
		String price1 = getCellValue(row, 14);
		String price2 = getCellValue(row, 15);
		String price3 = getCellValue(row, 16);
		String price4 = getCellValue(row, 17);
		String price5 = getCellValue(row, 18);
		String price6 = getCellValue(row, 19);
		String price7 = getCellValue(row, 20);
		String price8 = getCellValue(row, 21);
		String price9 = getCellValue(row, 22);
		//String zipListings = getCellValue(row, 25);
		String shopMinimumPrice = getCellValue(row, 25);
		String shopWebsite = getCellValue(row, 26);
		String shopLogo = getCellValue(row, 27);
		boolean isNewShop = false;
		if(shopcode != null) {
			
			if(shopname.length() > 50){
				shopname = shopname.substring(0,50);
			}
			
			String shopId = "";
			String cityId = "";
			String stateId = "";
			
			String query = "SELECT shop_id from shop s where s.shop_code=\""+shopcode+"\";";
			ResultSet results = mySQL.executeQuery(query);
			if(results.next()){
				shopId = results.getString("shop_id");
			}
			mySQL.closeStatement();
			
			query = "SELECT id FROM state WHERE short_name = \""+statecode+"\";";
			results = mySQL.executeQuery(query);
			if(results.next()){
				stateId = results.getString("id");
			}
			mySQL.closeStatement();
			if(!stateId.equals("")){
				query = "SELECT id from city c where c.name=\""+cityname+"\" and c.state_id = "+stateId+" and c.city_type = \"D\";";
				results = mySQL.executeQuery(query);
				if(results.next()){
					cityId = results.getString("id");
				}
				mySQL.closeStatement();
				
				if(cityId.equals("") && cityname != null && cityname.length() > 0) {
					String statement = "INSERT INTO city(name,state_id,city_type) VALUES(\""+cityname.toUpperCase()+"\","+stateId+",\"D\");";
					mySQL.executeStatement(statement);
					cityId = getId();
					citiesToIndex.add(Long.valueOf(getId()));
				}
				if(shopId.equals("")) {
					isNewShop = true;
				} else {
					String statement = "DELETE FROM minimum_price WHERE shop_shop_code = \""+shopcode+"\";";
					mySQL.executeStatement(statement);
				}
				
				String statement = "INSERT INTO address(street_address1,city_id,postal_code) VALUES(\""+address1+"\","+cityId+",\""+zipcode+"\")";
				mySQL.executeStatement(statement);
				String addressId = getId();
				
				if(openSunday == null) openSunday = "0";
				else if (openSunday.equalsIgnoreCase("Y")) openSunday = "1";
				else openSunday = "0";
				if(bloomlinkIndicator == null) bloomlinkIndicator = "0";
				else if (bloomlinkIndicator.equalsIgnoreCase("Y")) bloomlinkIndicator = "1";
				else bloomlinkIndicator = "0";
				if(newShop == null) newShop = "0";
				else if (newShop.equalsIgnoreCase("Y")) newShop = "1";
				else newShop = "0";
				if(floristForForrests == null) floristForForrests = "0";
				else if (floristForForrests.equalsIgnoreCase("Y")) floristForForrests = "1";
				else floristForForrests = "0";
	
				if(!isNewShop){
					statement = "UPDATE shop SET address_id = "+addressId+", shop_code = \""+shopcode+"\", shop_name = \""+shopname+"\","
							+ " telephone_number = \""+getCellValue(row, 3)+"\", toll_free_number = \""+getCellValue(row, 4)+"\","
							+ " fax_number = \""+getCellValue(row, 5)+"\", contact_person = \""+getCellValue(row, 7)+"\","
							+ " open_sunday = "+openSunday+", bloomlink_indicator = "+bloomlinkIndicator+", "
							+ " new_shop = "+newShop+", florist_for_forrests = "+floristForForrests+", "
							+ " minimum = "+shopMinimumPrice+", logo = "+shopLogo+", "
							+ " website = "+shopWebsite+", "
							+ " user_modified_id = 1, date_modified = now() WHERE shop_id = "+shopId+";";
				}else{
					statement = "INSERT INTO shop(address_id,shop_code,shop_name,telephone_number,toll_free_number,fax_number,contact_person,open_sunday,bloomlink_indicator,new_shop,florist_for_forrests,user_created_id,date_created) "
							+ "VALUES ("+addressId+",\""+shopcode+"\",\""+shopname+"\",\""+getCellValue(row, 3)+"\",\""+getCellValue(row, 4)+"\","
							+ "\""+getCellValue(row, 5)+"\",\""+getCellValue(row, 7)+"\","+openSunday+","+bloomlinkIndicator+","+newShop+","+floristForForrests+",1,now());";
				}
				statement = statement.replaceAll("\"\"", "NULL");
				mySQL.executeStatement(statement);
				if(price1 != null && !price1.equals(""))
					buildMinimumPrice(1, price1, shopcode);
				if(price2 != null && !price2.equals(""))
					buildMinimumPrice(2, price2, shopcode);
				if(price3 != null && !price3.equals(""))
					buildMinimumPrice(3, price3, shopcode);
				if(price4 != null && !price4.equals(""))
					buildMinimumPrice(4, price4, shopcode);
				if(price5 != null && !price5.equals(""))
					buildMinimumPrice(5, price5, shopcode);
				if(price6 != null && !price6.equals(""))
					buildMinimumPrice(6, price6, shopcode);
				if(price7 != null && !price7.equals(""))
					buildMinimumPrice(7, price7, shopcode);
				if(price8 != null && !price8.equals(""))
					buildMinimumPrice(8, price8, shopcode);
				if(price9 != null && !price9.equals(""))
					buildMinimumPrice(9, price9, shopcode);
				if(shopId.equals(""))
					shopId = getId();
				//addOrUpdateZipListings(zipListings,shop);
				shopsToIndex.add(Long.valueOf(shopId));
				System.out.print(".");
			}
		}
		String query = "SELECT id FROM listing WHERE shop_code = \""+shopcode+"\";";
		ResultSet results = mySQL.executeQuery(query);
		while(results.next()){
			listingsToIndex.add(Long.valueOf(results.getString("id")));
		}
		mySQL.closeStatement();
	}
	@Transactional
	public void addOrUpdateListing(HSSFRow row) throws PersistenceException, LookupException, ParseException, SQLException {
			
		String shopcode = getCellValue(row, 0);
		String cityname = getCellValue(row, 2);
		String statecode = getCellValue(row, 8);
		String listingtype = getCellValue(row, 4);
		String macname = getCellValue(row, 15);
		String adsize = getCellValue(row, 11);
		String entrycode = getCellValue(row, 3);
		String rotate = getCellValue(row, 5);
		String revListing = getCellValue(row, 6);
		String custom = getCellValue(row, 7);
		String revAd = getCellValue(row, 12);
		String minOrder = getCellValue(row, 13);
		String delivery = getCellValue(row, 14);
		String date = getCellValue(row, 16);
		String freeAd = getCellValue(row, 17);
		if(freeAd != null) freeAd = freeAd.replaceAll("\"","");
		String blockZips = getCellValue(row, 18);
		String textColor = getCellValue(row, 19);
		String highlightColor = getCellValue(row, 20);
		String videoColor = getCellValue(row, 21);
		String imageCount = getCellValue(row, 22);
		String cl8 = getCellValue(row, 23);
		String ca9 = getCellValue(row, 24);
		String sideAdEntryNumber = getCellValue(row, 25);
		String adStartDate = getCellValue(row, 26);
		String adEndDate = getCellValue(row, 27);
		String bannerStartDate = getCellValue(row, 28);
		String bannerEndDate = getCellValue(row, 29);
		String listingEndDate = getCellValue(row, 30);
		String seasonalImages = getCellValue(row, 31);
		if(seasonalImages != null){
			if(seasonalImages.equalsIgnoreCase("Y"))
				seasonalImages = "1";
			else
				seasonalImages = "";
		}
		String vdayBackground = getCellValue(row, 32);
		String springBackground = getCellValue(row, 33);
		String mdayBackground = getCellValue(row, 34);
		String summerBackground = getCellValue(row, 35);
		String fallBackground = getCellValue(row, 36);
		String winterBackground = getCellValue(row, 37);
		String featuredListing = getCellValue(row, 38);
		String macId = "";
		String listingTypeId = "2";
		String adSizeId = "";
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		if(date != null && !date.equals(""))
			date = sdf.format(date);
		
		boolean isNewListing = false;
		
		String stateId = "";
		String cityId= "";
		String listingId = "";
		
		String query = "SELECT id FROM state WHERE short_name = \""+statecode+"\";";
		ResultSet results = mySQL.executeQuery(query);
		if(results.next()){
			stateId = results.getString("id");
		}
		mySQL.closeStatement();
		
		if(!stateId.equals("")){
		
			query = "SELECT id FROM city WHERE name = \""+cityname+"\" AND state_id = "+stateId+" AND city_type = \"D\";";
			results = mySQL.executeQuery(query);
			if(results.next()){
				cityId = results.getString("id");
			}
			mySQL.closeStatement();
			
			if(cityId.equals("") && cityname != null && cityname.length() > 0) {
				String statement = "INSERT INTO city(name,state_id,city_type) VALUES(\""+cityname.toUpperCase()+"\","+stateId+",\"D\");";
				mySQL.executeStatement(statement);
				cityId = getId();
				citiesToIndex.add(Long.valueOf(cityId));
			}
			
			if(shopcode != null && cityId != null) {
				query = "SELECT id FROM facility_listing WHERE shop_code = \""+shopcode+"\" AND city_id = "+cityId+";";
				results = mySQL.executeQuery(query);
				if(results.next()){
					listingId = results.getString("id");
				}
				mySQL.closeStatement();
				if(listingId.equals("")) {
					isNewListing = true;
				}
				query = "SELECT id FROM mac WHERE mac_name = \""+macname+"\";";
				results = mySQL.executeQuery(query);
				if(results.next()){
					macId = results.getString("id");
				}
				if(listingtype != null) {
					query = "SELECT id FROM listing_type WHERE name = \""+listingtype+"\";";
					results = mySQL.executeQuery(query);
					if(results.next()){
						listingTypeId = results.getString("id");
					}
					mySQL.closeStatement();
				}
				if(adsize != null) {
					query = "SELECT id FROM ad_size WHERE ad_size = \""+adsize+"\";";
					results = mySQL.executeQuery(query);
					if(results.next()){
						adSizeId = results.getString("id");
					}
					mySQL.closeStatement();
				}
				if(freeAd.length() > 50)
					freeAd = freeAd.substring(0,50);
				if(isNewListing){
					String statement = "INSERT INTO facility_listing(shop_code,entry_code,listing_type_id,rotate,rev_listing,ad_size_id,rev_ad,min_order,delivery,mac_id,date,free_ad,text_color,highlight_color,video_color,image_count,cl8,ca9,city_id,date_created,user_created_id,side_ad_entry_number,ad_start_date,ad_end_date,banner_start_date,banner_end_date,listing_end_date,custom_listing,block_bloomlink_zips,seasonal_images,vday_background,spring_background,mday_background,summer_background,fall_background,winter_background,featured_listing)"
							+ " VALUES(\""+shopcode+"\",\""+entrycode+"\",\""+listingTypeId+"\",\""+rotate+"\",\""+revListing+"\",\""+adSizeId+"\",\""+revAd+"\",\""+minOrder+"\",\""+delivery+"\",\""+macId+"\",\""+date+"\",\""+freeAd+"\",\""+textColor+"\",\""+highlightColor+"\",\""+videoColor+"\",\""+imageCount+"\",\""+cl8+"\",\""+ca9+"\",\""+cityId+"\",now(),1,\""+sideAdEntryNumber+"\",\""+adStartDate+"\",\""+adEndDate+"\",\""+bannerStartDate+"\",\""+bannerEndDate+"\",\""+listingEndDate+"\",\""+custom+"\",\""+blockZips+"\",\""+seasonalImages+"\",\""+vdayBackground+"\",\""+springBackground+"\",\""+mdayBackground+"\",\""+summerBackground+"\",\""+fallBackground+"\",\""+winterBackground+"\",\""+featuredListing+"\");";
					statement = statement.replaceAll("\"\"", "NULL");
					mySQL.executeStatement(statement);
					listingsToIndex.add(Long.valueOf(getId()));
					
				}else{
					String statement = "UPDATE facility_listing SET entry_code = \""+entrycode+"\", listing_type_id = \""+listingTypeId+"\", rotate = \""+rotate+"\", rev_listing = \""+revListing+"\", "
							+ "ad_size_id = \""+adSizeId+"\", rev_ad = \""+revAd+"\", min_order = \""+minOrder+"\", delivery = \""+delivery+"\", mac_id = \""+macId+"\", date = \""+date+"\", free_ad = \""+freeAd+"\", text_color = \""+textColor+"\", highlight_color = \""+highlightColor+"\", image_count = \""+imageCount+"\", "
									+ "cl8 = \""+cl8+"\", ca9 = \""+ca9+"\", city_id = \""+cityId+"\", date_modified = now(), user_modified_id = 1, side_ad_entry_number = \""+sideAdEntryNumber+"\", ad_start_date = \""+adStartDate+"\", ad_end_date = \""+adEndDate+"\", banner_start_date = \""+bannerStartDate+"\", banner_end_date = \""+bannerEndDate+"\", listing_end_date = \""+listingEndDate+"\", custom_listing = \""+custom+"\", block_bloomlink_zips = \""+blockZips+"\", seasonal_images = \""+seasonalImages+"\","
									+ "vday_background = \""+vdayBackground+"\", spring_background = \""+springBackground+"\", mday_background = \""+mdayBackground+"\", summer_background = \""+summerBackground+"\", fall_background = \""+fallBackground+"\", winter_background = \""+winterBackground+"\", featured_listing = \""+featuredListing+"\""		
									+ " WHERE id = "+listingId+";";
					statement = statement.replaceAll("\"\"","NULL");
					mySQL.executeStatement(statement);
					listingsToIndex.add(Long.valueOf(listingId));
				}
				
			}
		}
		query = "SELECT id FROM facility_listing WHERE shop_code = \""+shopcode+"\";";
		results = mySQL.executeQuery(query);
		while(results.next()){
			if(!listingsToIndex.contains(Long.valueOf(results.getString("id"))))
				listingsToIndex.add(Long.valueOf(results.getString("id")));
		}
		mySQL.closeStatement();
	}
	
	@Transactional
	protected void buildMinimumPrice(int category, String price, String shopcode) {
		boolean fluctuate = false;
		if(price.equalsIgnoreCase("H"))
			return;
		if(price.endsWith("H") || price.endsWith("h")){
			fluctuate = true;
			price = price.substring(0, price.length()-1);
		}else
			fluctuate = false;
		String statement = "INSERT INTO minimum_price(product_category_id,shop_shop_code,minimum_price,fluctuate_price,date_created,user_created_id,date_modified,user_modified_id) "
				+ "VALUES("+category+",\""+shopcode+"\",\""+price+"\",";
		if(fluctuate)
			statement += "1,";
		else
			statement += "0,";
		statement+= "now(),1,now(),1);";
		mySQL.executeStatement(statement);
	}
	@Transactional
	public void geocodeShop(long id) {

		try {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			Shop shop = (Shop)st.find(Shop.class, id, service);
			if(shop.getAddress() != null && shop.getAddress().getCity() != null && shop.getAddress().getStreetAddress1() != null && shop.getAddress().getState().getCountry().getShortName().equals("US")) {
				String address = shop.getAddress().getStreetAddress1()+","+shop.getAddress().getCity().getName()+","+shop.getAddress().getCity().getState().getShortName();
				Double[] coords = fetchCoords(address);
				if(coords != null) {
					shop.setLatitude(coords[0]);
					shop.setLongitude(coords[1]);
					st.merge(shop, service);
					st.index(Shop.class, id, service);
				} else {
					System.out.println("");
					System.out.println("no coords found for "+shop.getShopCode()+" : "+address);
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	protected Double[] fetchCoords(String address) {
		try {
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("address", address));
			URI uri = URIUtils.createURI("http", "geocoder.us", -1, "service/csv/geocode", URLEncodedUtils.format(params, "UTF-8"), null);
			HttpClient client = new DefaultHttpClient();
			HttpGet get = new HttpGet(uri);
			HttpResponse resp = client.execute(get);
			HttpEntity entity = resp.getEntity();
			InputStreamReader reader = new InputStreamReader(entity.getContent());
			StringBuilder content = new StringBuilder();
			char[] chars = new char[4096];
			int len = -1;
			while ( (len = reader.read(chars, 0, 4096)) != -1 ){
				content.append(chars, 0, len);
			}
			String[] coords = content.toString().split(",");
			if(coords.length >= 2) {
				Double[] vals = new Double[2];
				vals[0] = Double.valueOf(coords[0]);
				vals[1] = Double.valueOf(coords[1]);
				return vals;
			}
		} catch(Exception e) {
			System.out.println("Failed to lookup coodinates - "+e.getMessage());
		}
		return null;
	}
	protected String getCellValue(HSSFRow row, int index) {
		Cell cell = row.getCell(index);
		if (cell == null) return "";
		cell.setCellType(HSSFCell.CELL_TYPE_STRING);
		String value = cell.getStringCellValue().trim();
		if(value == null || value.length() == 0) return "";
		return value;
	}
	
	@Transactional
	protected void addOrUpdateZipListings(String zipListings, Shop shop) throws LookupException{
		
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SingleThread st = new SingleThread();
		String shopCode = shop.getShopCode();
		if(zipListings != null && ! zipListings.equals("")){
			String[] zips = zipListings.split(",");
			if(zips == null || zips.length == 0)
				zips = new String[]{zipListings};
			for(int ii=0; ii<zips.length; ++ii){
				String zip = zips[ii];
				ZipListing zl = (ZipListing)st.findSingleResult("from ZipListing l where l.shop.shopCode=\""+shopCode+"\" and l.zipCode=\""+zip+"\"", service);
				if(zl != null && zl.getActive() == 0){
					zl.setActive(1);
					st.merge(zl, service);
				}else if(zl == null){
					zl = new ZipListing();
					zl.setActive(1);
					zl.setZipCode(zip);
					zl.setShop(shop);
					st.persist(zl, service);
				}
			}
		}
		
	}
	
	protected String getId(){
		String query = "SELECT LAST_INSERT_ID()";
		ResultSet results = mySQL.executeQuery(query);
		String id = "";
		try {
			results.next();
			id = results.getString("LAST_INSERT_ID()");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		mySQL.closeStatement();
		return id;
	}
}
