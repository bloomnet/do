package com.flowers.client.listings;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.server.entity.FacilityListing;
import com.flowers.server.entity.Listing;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class ListingPanel extends VerticalLayout {
	private static final long serialVersionUID = 8310272037766774853L;
	private TextField shopCode;
	private Button searchButton;
	private ListingTable listingTable;
	private Label searchMessage;
	private MediaUploadWindow mediaUploadWindow;
	private FacilityMediaUploadWindow facilityMediaUploadWindow;
	private ImagesMediaUploadWindow imagesMediaUploadWindow;
	private ListingZipCodeRemovalWindow listingZipCodeRemovalWindow;
	
	public HorizontalLayout getListingSearch() {
		HorizontalLayout shopSearchLayout = new HorizontalLayout();
		shopSearchLayout.setSpacing(true);
		shopSearchLayout.setHeight("40px");
		
		shopCode = new TextField("City Name");
		shopSearchLayout.addComponent(shopCode);
		shopSearchLayout.setComponentAlignment(shopCode, Alignment.MIDDLE_LEFT);
		
		searchButton = new Button("Search");
		shopSearchLayout.addComponent(searchButton);
		shopSearchLayout.setComponentAlignment(searchButton, Alignment.BOTTOM_LEFT);
		searchButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				search();
			}
		});
		
		final Button indexListingsButton = new Button("Index Listings");
		shopSearchLayout.addComponent(indexListingsButton);
		shopSearchLayout.setComponentAlignment(indexListingsButton, Alignment.BOTTOM_LEFT);
		indexListingsButton.addListener(new ClickListener() {
			private static final long serialVersionUID = -1177941627607832172L;
			public void buttonClick(ClickEvent event) {
				BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
				SearchService searchService = app.getBean("SearchService", SearchService.class);
				SingleThread st = new SingleThread();
				try {
					st.index(Listing.class, searchService, true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		final Button indexFacilityListingsButton = new Button("Index Fac Listings");
		shopSearchLayout.addComponent(indexFacilityListingsButton);
		shopSearchLayout.setComponentAlignment(indexFacilityListingsButton, Alignment.BOTTOM_LEFT);
		indexFacilityListingsButton.addListener(new ClickListener() {
			private static final long serialVersionUID = -1177941627707832172L;
			public void buttonClick(ClickEvent event) {
				BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
				SearchService searchService = app.getBean("SearchService", SearchService.class);
				SingleThread st = new SingleThread();
				try {
					st.index(FacilityListing.class, searchService, true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		final Button importListingsButton = new Button("Import Listings");
		shopSearchLayout.addComponent(importListingsButton);
		shopSearchLayout.setComponentAlignment(importListingsButton, Alignment.BOTTOM_LEFT);
		importListingsButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				importListingsButton.setEnabled(false);
				try {
					mediaUploadWindow = new MediaUploadWindow();
					getApplication().getMainWindow().addWindow(mediaUploadWindow);
					importListingsButton.setEnabled(true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		final Button importFacilityListingsButton = new Button("Import Fac Listings");
		shopSearchLayout.addComponent(importFacilityListingsButton);
		shopSearchLayout.setComponentAlignment(importFacilityListingsButton, Alignment.BOTTOM_LEFT);
		importFacilityListingsButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108049082840091062L;
			public void buttonClick(ClickEvent event) {
				importFacilityListingsButton.setEnabled(false);
				try {
					facilityMediaUploadWindow = new FacilityMediaUploadWindow();
					getApplication().getMainWindow().addWindow(facilityMediaUploadWindow);
					importFacilityListingsButton.setEnabled(true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		final Button importImagesButton = new Button("Import Images");
		shopSearchLayout.addComponent(importImagesButton);
		shopSearchLayout.setComponentAlignment(importImagesButton, Alignment.BOTTOM_LEFT);
		importImagesButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108049082840091062L;
			public void buttonClick(ClickEvent event) {
				importImagesButton.setEnabled(false);
				try {
					imagesMediaUploadWindow = new ImagesMediaUploadWindow();
					getApplication().getMainWindow().addWindow(imagesMediaUploadWindow);
					importImagesButton.setEnabled(true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		final Button exportListingZipsButton = new Button("Export Listing Zips");
		shopSearchLayout.addComponent(exportListingZipsButton);
		shopSearchLayout.setComponentAlignment(exportListingZipsButton, Alignment.BOTTOM_LEFT);
		exportListingZipsButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				exportListingZipsButton.setEnabled(false);
				try {
					listingZipCodeRemovalWindow = new ListingZipCodeRemovalWindow();
					getApplication().getMainWindow().addWindow(listingZipCodeRemovalWindow);
					exportListingZipsButton.setEnabled(true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		searchMessage = new Label();
		shopSearchLayout.addComponent(searchMessage);
		shopSearchLayout.setComponentAlignment(searchMessage, Alignment.MIDDLE_LEFT);
		return shopSearchLayout;
	}

	protected void search(){
		try {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			SearchService searchService = app.getBean("SearchService", SearchService.class);
			List<Document> shops = null;
			SortField sortField = new SortField("city", SortField.Type.STRING);
			if(shopCode.getValue() != null && shopCode.getValue().toString().length() >0) {
				TermQuery query = new TermQuery(new Term("city", shopCode.getValue().toString()));
				 shops = searchService.documents(Listing.class, query, sortField);
			} else{
				shops = searchService.documents(Listing.class, new MatchAllDocsQuery(), sortField);
			}
			if(shops != null) {
				listingTable.removeAllItems();
				for(Document d : shops) {
					listingTable.addDocumentToContainer(d);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public ListingPanel() {
		setSizeFull();
		setSpacing(true);
		addComponent(getListingSearch());
		
		listingTable = new ListingTable();
		addComponent(listingTable);
	}
}
