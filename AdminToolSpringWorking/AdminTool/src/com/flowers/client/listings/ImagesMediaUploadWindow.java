package com.flowers.client.listings;
import java.io.File;
import com.flowers.server.util.SizeCountingReceiver;
import com.flowers.server.util.UnzipUtility;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.ProgressIndicator;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FinishedEvent;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;


public class ImagesMediaUploadWindow extends Window {
	private static final long serialVersionUID = -359167762053227360L;
	private SizeCountingReceiver counter;
	private Upload upload;
	private Label state = new Label();
    private Label result = new Label();
    private Label fileName = new Label();
    private Label textualProgress = new Label();
    
    
    private ProgressIndicator pi = new ProgressIndicator();

    private String uploadPath = "/opt/data/import";
	
	public ImagesMediaUploadWindow() {
		center();
		VerticalLayout layout = (VerticalLayout) getContent();
        layout.setMargin(true);
        layout.setSpacing(true); 
        layout.setWidth("350px");
        layout.setHeight("300px");
        
        counter = new SizeCountingReceiver();
        upload = new Upload("", counter);
        addComponent(new Label("Upload a file to your collection."));
        
        upload.setImmediate(true);
        upload.setButtonCaption("Upload File");
        addComponent(upload);

        final Button cancelProcessing = new Button("Cancel");
        cancelProcessing.addListener(new Button.ClickListener() {
        	private static final long serialVersionUID = 1L;

			public void buttonClick(ClickEvent event) {
        		upload.interruptUpload();
        	}
        });
        cancelProcessing.setVisible(false);
        cancelProcessing.setStyleName("small");

        Panel p = new Panel("Status");
        p.setSizeUndefined();
        p.setWidth(300, HorizontalLayout.UNITS_PIXELS);
        FormLayout l = new FormLayout();
        l.setMargin(true);
        p.setContent(l);
        HorizontalLayout stateLayout = new HorizontalLayout();
        stateLayout.setSpacing(true);
        stateLayout.addComponent(state);
        stateLayout.addComponent(cancelProcessing);
        stateLayout.setCaption("Current state");
        state.setValue("Idle");
        l.addComponent(stateLayout);
        fileName.setCaption("File name");
        l.addComponent(fileName);
        result.setCaption("bytes");
        l.addComponent(result);
        pi.setCaption("Progress");
        pi.setVisible(false);
        l.addComponent(pi);
        textualProgress.setVisible(false);
        l.addComponent(textualProgress);

        addComponent(p);

        upload.addListener(new Upload.StartedListener() {
        	private static final long serialVersionUID = 1084163386895143768L;

			public void uploadStarted(StartedEvent event) {
        		// this method gets called immediatedly after upload is
        		// started
        		pi.setValue(0f);
        		pi.setVisible(true);
        		pi.setPollingInterval(500); // hit server frequently to get
        		textualProgress.setVisible(true);
        		// updates to client
        		state.setValue("Uploading");
        		fileName.setValue(event.getFilename());

        		cancelProcessing.setVisible(true);
        	}
        });

        upload.addListener(new Upload.ProgressListener() {
        	private static final long serialVersionUID = 3318393640870913379L;

			public void updateProgress(long readBytes, long contentLength) {
        		// this method gets called several times during the update
        		pi.setValue(readBytes / (float) contentLength);
        		textualProgress.setValue("Processed " + readBytes + " bytes of " + contentLength);
        		result.setValue(counter.getTotal());
        	}

        });
        upload.addListener(new Upload.SucceededListener() {
        	private static final long serialVersionUID = -4135230597799242206L;

			public void uploadSucceeded(SucceededEvent event) {
        		result.setValue(counter.getTotal() + " (total)");
        	}
        });
        upload.addListener(new Upload.FailedListener() {
        	private static final long serialVersionUID = -8754773108937834980L;

			public void uploadFailed(FailedEvent event) {
        		result.setValue(counter.getTotal()
        				+ " (counting interrupted at "
        				+ Math.round(100 * (Float) pi.getValue()) + "%)");
        	}
        });
        upload.addListener(new Upload.FinishedListener() {
        	private static final long serialVersionUID = -2421647485010631615L;

			public void uploadFinished(FinishedEvent event) {
        		state.setValue("Idle");
        		pi.setVisible(false);
        		textualProgress.setVisible(false);
        		cancelProcessing.setVisible(false);
        		doImport(event.getFilename());
      
        	}
        });
	}
	protected void doImport(String filename) {
		try {
			
			UnzipUtility unzipper = new UnzipUtility();
			unzipper.unzip(uploadPath+"/"+filename,"/var/lib/tomcat/webapps/bloomnet-images/ads");
			File file = new File(uploadPath + "/" + filename);
    		file.delete();
			
		}catch(Exception ee){}
	}
	
}


