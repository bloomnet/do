package com.flowers.client.logs;

import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.server.entity.SearchQuery;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class LogViewerPanel extends VerticalLayout {
	private static final long serialVersionUID = -2902873589065914106L;
	private TextField shopCode;
	private Button searchButton;	
	private LogViewerTable listingTable;
	private Label searchMessage;
	
	public HorizontalLayout getListingSearch() {
		HorizontalLayout shopSearchLayout = new HorizontalLayout();
		shopSearchLayout.setSpacing(true);
		shopSearchLayout.setHeight("40px");
		
		shopCode = new TextField("City Name");
		shopSearchLayout.addComponent(shopCode);
		shopSearchLayout.setComponentAlignment(shopCode, Alignment.MIDDLE_LEFT);
		
		searchButton = new Button("Search");
		shopSearchLayout.addComponent(searchButton);
		shopSearchLayout.setComponentAlignment(searchButton, Alignment.BOTTOM_LEFT);
		searchButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				search();
			}
		});
		
		final Button indexListingsButton = new Button("Index Logs");
		shopSearchLayout.addComponent(indexListingsButton);
		shopSearchLayout.setComponentAlignment(indexListingsButton, Alignment.BOTTOM_LEFT);
		indexListingsButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
				SearchService searchService = app.getBean("SearchService", SearchService.class);
				indexListingsButton.setEnabled(false);
				try {
					searchService = app.getBean("SearchService", SearchService.class);
					SingleThread st = new SingleThread();
					st.index(SearchQuery.class, searchService,true);
					indexListingsButton.setEnabled(true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		searchMessage = new Label();
		shopSearchLayout.addComponent(searchMessage);
		shopSearchLayout.setComponentAlignment(searchMessage, Alignment.MIDDLE_LEFT);
		return shopSearchLayout;
	}

	protected void search(){
		try {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			SearchService searchService = app.getBean("SearchService", SearchService.class);
			List<Document> shops = null;
			SortField sortField = new SortField("timestamp", SortField.Type.LONG, true);
			if(shopCode.getValue() != null && shopCode.getValue().toString().length() >0) {
				TermQuery query = new TermQuery(new Term("city", shopCode.getValue().toString()));
				 shops = searchService.documents(SearchQuery.class, query, sortField);
			} else{
				shops = searchService.documents(SearchQuery.class, new MatchAllDocsQuery(), sortField);
			}
			if(shops != null) {
				searchMessage.setCaption("Found "+shops.size()+" queries");
				listingTable.removeAllItems();
				for(Document d : shops) {
					listingTable.addDocumentToContainer(d);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public LogViewerPanel() {
		setSizeFull();
		setSpacing(true);
		addComponent(getListingSearch());
		
		listingTable = new LogViewerTable();
		addComponent(listingTable);
	}
}
