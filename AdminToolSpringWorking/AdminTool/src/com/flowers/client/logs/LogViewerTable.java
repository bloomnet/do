package com.flowers.client.logs;

import java.util.Date;

import org.apache.lucene.document.Document;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.SearchQuery;
import com.flowers.server.service.SearchService;
import com.vaadin.data.Item;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Table;

public class LogViewerTable extends Table implements TableContainer<SearchQuery> {
	private static final long serialVersionUID = 4363624315568930161L;
	private SearchService searchService;
	
	public LogViewerTable() {
		addContainerProperty("date", String.class, null);
		addContainerProperty("city", String.class, null);
		addContainerProperty("delivery", String.class, null);
		addContainerProperty("facility", String.class, null);
		addContainerProperty("minimums", String.class, null);
		addContainerProperty("orderId", String.class, null);
		addContainerProperty("phone", String.class, null);
		addContainerProperty("products", String.class, null);
		addContainerProperty("shopCode", String.class, null);
		addContainerProperty("shopName", String.class, null);
		addContainerProperty("count", String.class, null);
		addContainerProperty("state", String.class, null);
		
		setSizeFull();
		setHeight(500, AbstractComponent.UNITS_PIXELS);
		setSelectable(true);
		setImmediate(true);
		setNullSelectionAllowed(false);
	}
	protected void init() {
		if(searchService == null) {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			searchService = app.getBean("SearchService", SearchService.class);
		}
	}
	public Item addItemToContainer(SearchQuery t) {
		return null;
	}

	public Item addDocumentToContainer(Document d) {
		init();
		Long id = Long.valueOf(d.get("id"));
		Item item = this.addItem(id);
		if(item != null) {
			String city = d.get("city");
			String state = d.get("state");
			String timestamp = d.get("timestamp");
			if(timestamp != null) {
				Date date = new Date(Long.valueOf(timestamp));
				item.getItemProperty("date").setValue(date.toString());
			}
			item.getItemProperty("city").setValue(city);			
			item.getItemProperty("delivery").setValue(d.get("delivery"));
			item.getItemProperty("facility").setValue(d.get("facility"));
			item.getItemProperty("minimums").setValue(d.get("minimum"));
			item.getItemProperty("orderId").setValue(d.get("orderId"));
			item.getItemProperty("phone").setValue(d.get("phone"));
			item.getItemProperty("products").setValue(d.get("products"));
			item.getItemProperty("shopCode").setValue(d.get("shopCode"));
			item.getItemProperty("shopName").setValue(d.get("shopName"));
			item.getItemProperty("count").setValue(d.get("count"));
			item.getItemProperty("state").setValue(state);
		}
		return item;
	}

	public void selectRow(SearchQuery t) {
	}

}
