package com.flowers.client.dropship;

import org.apache.lucene.document.Document;

import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.Dropship;
import com.vaadin.data.Item;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Table;

public class DropshipTable extends Table implements TableContainer<Dropship> {
	private static final long serialVersionUID = -7514027152429177475L;

	public DropshipTable() {
		addContainerProperty("code", String.class, null);
		addContainerProperty("description", String.class, null);		
		addContainerProperty("price", String.class, null);		
		setSizeFull();
		setHeight(200, AbstractComponent.UNITS_PIXELS);
		setSelectable(true);
		setImmediate(true);
		setNullSelectionAllowed(false);
	}
	
	@Override
	public Item addItemToContainer(Dropship t) {
		Item item = this.addItem(t.getId());
		if(item != null) {
			item.getItemProperty("code").setValue(t.getProductCode());
			item.getItemProperty("description").setValue(t.getProductDescription());
			item.getItemProperty("price").setValue(t.getPrice());
		}
		return item;
	}

	@Override
	public Item addDocumentToContainer(Document d) {
		Long id = Long.valueOf(d.get("id"));
		Item item = this.addItem(id);
		if(item != null) {
			item.getItemProperty("code").setValue(d.get("product_code"));
			item.getItemProperty("description").setValue(d.get("product_description"));
			item.getItemProperty("price").setValue(d.get("price"));
		}
		return item;
	}

	@Override
	public void selectRow(Dropship t) {
		this.setValue(t.getId());
	}

}
