package com.flowers.client.dropship;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.server.entity.Dropship;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.flowers.server.util.SQLData;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.terminal.FileResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class DropshipPanel extends VerticalLayout {
	private static final long serialVersionUID = -6123729574360668207L;
	private DropshipTable dropshipTable;
	private DropshipForm dropshipForm;
	private TextField productCode;
	private TextField startDate;
	private TextField endDate;
	private Button searchButton;
	private Button reportButton;
	
	public HorizontalLayout getDropshipSearch() {
		
		Date today = new Date();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String date = sdf.format(today);
		
		HorizontalLayout dropshipLayout = new HorizontalLayout();
		dropshipLayout.setSpacing(true);
		dropshipLayout.setHeight("40px");
		
		productCode = new TextField("Product Code");
		dropshipLayout.addComponent(productCode);
		dropshipLayout.setComponentAlignment(productCode, Alignment.MIDDLE_LEFT);
		
		startDate = new TextField("Start Date");
		endDate = new TextField("End Date");
		startDate.setValue(date);
		endDate.setValue(date);
		
		searchButton = new Button("Search");
		dropshipLayout.addComponent(searchButton);
		dropshipLayout.setComponentAlignment(searchButton, Alignment.BOTTOM_LEFT);
		searchButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				Dropship dropship = new Dropship();
				dropshipForm.setObjectToForm(dropship);
				dropshipForm.editButton.setVisible(false);
				dropshipForm.removeButton.setVisible(false);
				search();
			}
		});
		final Button indexButton = new Button("Index");
		dropshipLayout.addComponent(indexButton);
		dropshipLayout.setComponentAlignment(indexButton, Alignment.BOTTOM_LEFT);
		indexButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
				SearchService searchService = app.getBean("SearchService", SearchService.class);
				indexButton.setEnabled(false);
				try {
					SingleThread st = new SingleThread();
					st.index(Dropship.class, searchService,true);
					indexButton.setEnabled(true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		dropshipLayout.addComponent(startDate);
		dropshipLayout.setComponentAlignment(startDate, Alignment.BOTTOM_LEFT);
		dropshipLayout.addComponent(endDate);
		dropshipLayout.setComponentAlignment(endDate, Alignment.BOTTOM_CENTER);
		reportButton = new Button("Generate Report");
		dropshipLayout.addComponent(reportButton);
		dropshipLayout.setComponentAlignment(reportButton, Alignment.BOTTOM_RIGHT);
		reportButton.addListener(new ClickListener() {
			private static final long serialVersionUID = -6074662345875852514L;

			public void buttonClick(ClickEvent event) {
				try {
					String date = String.valueOf(new Date().getTime());
					File outFile = new File("/var/lib/tomcat/webapps/fruitBouquetData/fruitBouquetOrders"+date+".csv");
					Writer out = new BufferedWriter(new FileWriter(outFile));
					String start = startDate.getValue().toString();
					String end = endDate.getValue().toString();
				
					out.write("OrderNumber,ProductCode,Quantity,Description,Price\n");
				
					SQLData mySQL = new SQLData();
					String query = "SELECT op.tracking_number, op.product_code, op.quantity, op.description, op.price "
							+ "FROM processed_order po INNER JOIN order_products op ON po.tracking_number = op.tracking_number "
							+ "WHERE op.tracking_number != 0 AND op.product_code IN (SELECT product_code FROM dropship)"
							+ " AND po.created_date >= '" +start+"' AND po.created_date <= '" +end+"';";
					ResultSet results = mySQL.executeQuery(query);
					System.out.println(query);
					System.out.println(results);
					while(results.next()){
						out.write(results.getString("tracking_number")+","+results.getString("product_code")+","+results.getString("quantity")+","+results.getString("description")+","+results.getString("price")+"\n");
					}
					
					mySQL.closeStatement();
					
					out.flush();
					out.close();
					
					FileResource fileResource = new FileResource(outFile, (BloomNetAdminApplication)getApplication());						
					getWindow().open(fileResource);
				
				} catch (IOException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
		});
		
		return dropshipLayout;
	}

	protected void search(){
		try {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			SearchService searchService = app.getBean("SearchService", SearchService.class);
			List<Document> dropshipProducts = null;
			SortField sortField = new SortField("product_code", SortField.Type.STRING);
			if(!"".equals(productCode.getValue())) {
				
				String searchTerm = productCode.getValue().toString().toUpperCase();
				
				TermQuery query = new TermQuery(new Term("product_code", searchTerm));
				
				dropshipProducts = searchService.documents(Dropship.class, query,
						sortField);
			} else{
				dropshipProducts = searchService.documents(Dropship.class, new MatchAllDocsQuery(), sortField);
			}
			if(dropshipProducts != null) {
				dropshipTable.removeAllItems();
				for(Document d : dropshipProducts) {
					dropshipTable.addDocumentToContainer(d);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public DropshipPanel() {
		setSizeFull();
		setSpacing(true);
		addComponent(getDropshipSearch());

		dropshipTable = new DropshipTable();
		addComponent(dropshipTable);
		dropshipTable.addListener(new DropshipTable.ValueChangeListener() {
            private static final long serialVersionUID = 2537403500201812771L;
			public void valueChange(ValueChangeEvent event) {
				Long id = (Long)dropshipTable.getValue();
				try {
					if (id != null){
						BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
						CrudService service = app.getBean("CrudService", CrudService.class);
						SingleThread st = new SingleThread();
						Dropship obj = (Dropship)st.find(Dropship.class, id, service);
						dropshipForm.setObjectToForm(obj);
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
            }
        });
		
		dropshipForm = new DropshipForm(dropshipTable);
		addComponent(dropshipForm);
	}
}
