package com.flowers.client.sidead;

import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.server.entity.SideAd;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class SideAdPanel extends VerticalLayout {
	private static final long serialVersionUID = -6449339436955571727L;
	private SideAdTable sideAdTable;
	private SideAdForm sideAdForm;
	private TextField shopCode;
	private Button searchButton;
	
	public HorizontalLayout getSideAdSearch() {
		
		HorizontalLayout sideAdLayout = new HorizontalLayout();
		sideAdLayout.setSpacing(true);
		sideAdLayout.setHeight("40px");
		
		shopCode = new TextField("Shop Code");
		
		sideAdLayout.addComponent(shopCode);
		sideAdLayout.setComponentAlignment(shopCode, Alignment.MIDDLE_LEFT);
		
		searchButton = new Button("Search");
		sideAdLayout.addComponent(searchButton);
		sideAdLayout.setComponentAlignment(searchButton, Alignment.BOTTOM_LEFT);
		searchButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				SideAd sideAd = new SideAd();
				sideAdForm.setObjectToForm(sideAd);
				sideAdForm.removeButton.setVisible(true);
				search();
			}
		});
		final Button indexButton = new Button("Index");
		sideAdLayout.addComponent(indexButton);
		sideAdLayout.setComponentAlignment(indexButton, Alignment.BOTTOM_LEFT);
		indexButton.addListener(new ClickListener() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -2876914040646907172L;

			public void buttonClick(ClickEvent event) {
				BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
				SearchService searchService = app.getBean("SearchService", SearchService.class);
				indexButton.setEnabled(false);
				try {
					SingleThread st = new SingleThread();
					st.index(SideAd.class, searchService,true);
					indexButton.setEnabled(true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
			
		return sideAdLayout;
	}

	protected void search(){
		try {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			SearchService searchService = app.getBean("SearchService", SearchService.class);
			List<Document> sideAds = null;
			SortField sortField = new SortField("shop_code", SortField.Type.STRING);
			if(!"".equals(shopCode.getValue())) {
				
				String searchTerm = shopCode.getValue().toString().toUpperCase();
				
				TermQuery query = new TermQuery(new Term("shop_code", searchTerm));
				
				sideAds = searchService.documents(SideAd.class, query,
						sortField);
			} else{
				sideAds = searchService.documents(SideAd.class, new MatchAllDocsQuery(), sortField);
			}
			if(sideAds != null) {
				sideAdTable.removeAllItems();
				for(Document d : sideAds) {
					sideAdTable.addDocumentToContainer(d);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public SideAdPanel() {
		setSizeFull();
		setSpacing(true);
		addComponent(getSideAdSearch());

		sideAdTable = new SideAdTable();
		addComponent(sideAdTable);
		sideAdTable.addListener(new SideAdTable.ValueChangeListener() {
			private static final long serialVersionUID = -3922216275089187949L;

			public void valueChange(ValueChangeEvent event) {
				Long id = (Long)sideAdTable.getValue();
				try {
					if (id != null){
						BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
						CrudService service = app.getBean("CrudService", CrudService.class);
						SingleThread st = new SingleThread();
						SideAd obj = (SideAd)st.find(SideAd.class, id, service);
						sideAdForm.setObjectToForm(obj);
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
            }
        });
		
		sideAdForm = new SideAdForm(sideAdTable);
		addComponent(sideAdForm);
	}
}
