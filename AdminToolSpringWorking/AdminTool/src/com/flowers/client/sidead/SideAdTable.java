package com.flowers.client.sidead;

import org.apache.lucene.document.Document;

import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.SideAd;
import com.vaadin.data.Item;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Table;

public class SideAdTable extends Table implements TableContainer<SideAd> {
	private static final long serialVersionUID = 6659565392894085290L;

	public SideAdTable() {
		addContainerProperty("shopCode", String.class, null);	
		addContainerProperty("fileName", String.class, null);	
		setSizeFull();
		setHeight(200, AbstractComponent.UNITS_PIXELS);
		setSelectable(true);
		setImmediate(true);
		setNullSelectionAllowed(false);
	}
	
	@Override
	public Item addItemToContainer(SideAd t) {
		Item item = this.addItem(t.getId());
		if(item != null) {
			item.getItemProperty("shopCode").setValue(t.getShopCode());
			item.getItemProperty("fileName").setValue(t.getFileName());
		}
		return item;
	}

	@Override
	public Item addDocumentToContainer(Document d) {
		Long id = Long.valueOf(d.get("id"));
		Item item = this.addItem(id);
		if(item != null) {
			item.getItemProperty("shopCode").setValue(d.get("shop_code"));
			item.getItemProperty("fileName").setValue(d.get("file_name"));
		}
		return item;
	}

	@Override
	public void selectRow(SideAd t) {
		this.setValue(t.getId());
	}

}
