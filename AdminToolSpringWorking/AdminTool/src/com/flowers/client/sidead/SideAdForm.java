package com.flowers.client.sidead;

import javax.persistence.PersistenceException;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.ui.base.BaseForm;
import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.SideAd;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window.Notification;

public class SideAdForm extends BaseForm<SideAd> {

	private static final long serialVersionUID = -1609034181956013444L;
	private static final String COMMON_FIELD_WIDTH = "25em";
	public static final String[] VISIBLE_FIELDS_ORDER = new String[]{"shopCode","fileName"};
	
	public SideAdForm(TableContainer<SideAd> tableContainer) {
		super(tableContainer, VISIBLE_FIELDS_ORDER);
	}
	
	
	@Override
	public SideAd getBlankObject() {
		return new SideAd();
	}

	@Override
	public SideAd insert(SideAd t) throws PersistenceException {
		try{
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			st.persist(t, service);
			st.index(SideAd.class, t.getId(), service);
		}catch(Exception ee){
		}
		return t;
	}

	@Override
	public SideAd update(SideAd t) throws PersistenceException {
		try{
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			st.merge(t, service);
			st.index(SideAd.class, t.getId(), service);
		}catch(Exception ee){
		}
		return t;
	}

	@Override
	public Object remove(SideAd t) throws PersistenceException {
		try{
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			st.remove(t, service);
		}catch(Exception ee){
		}
		return t;
	}

	@Override
	public FormFieldFactory getFormFieldFactory() {
		return new SideAdFormFieldFactory();
	}
	
	public void saveButtonClickHandler(){
		if(getField("shopCode").getValue() == null || getField("fileName").getValue() == null){
			getApplication().getMainWindow().showNotification("Please Fill Out All Necessary Fields.", Notification.POSITION_CENTERED);
		}else{
			super.saveButtonClickHandler();
		}
	}

	private class SideAdFormFieldFactory extends DefaultFieldFactory{

		private static final long serialVersionUID = -3596248659949602235L;

		@Override
		public Field createField(Item item, Object propertyId,	Component uiContext) {
			String pid = (String)propertyId;
			Field field = super.createField(item, propertyId, uiContext);
			if(field instanceof TextField) {
				TextField txt = (TextField)field;
				if ("shopCode".equals(pid)){
					txt.setWidth(COMMON_FIELD_WIDTH);
					txt.setMaxLength(10);
				}
				if ("fileName".equals(pid)){
					txt.setWidth(COMMON_FIELD_WIDTH);
					txt.setMaxLength(100);
				}
				txt.setNullRepresentation("");
			}
			return field;
		}
	}
}
