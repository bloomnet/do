package com.flowers.client.facilities;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.flowers.portal.util.WebServiceUtil;
import com.flowers.server.util.SQLData;

public class WriteFacilities {

	HashMap<String, String> existingStates = new HashMap<String, String>();
	SQLData mySQL;
	String databaseName;
	Map<String, String> cityZipXref = new HashMap<String, String>();

	public WriteFacilities(String shopToUse, File outfile) {

		mySQL = new SQLData();
		mySQL.login();

		try {
			writeFacilityData(shopToUse, outfile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void setXrefData(String zipsString) throws SQLException {

		String statement = "USE bloomnet;";
		mySQL.executeStatement(statement);

		String query = "SELECT zip_code, city.name FROM zip " + "INNER JOIN city_zip_xref xref ON zip.id = xref.id_zip "
				+ "INNER JOIN city ON xref.id_city = city.id " + "WHERE city.city_type = \"D\" AND zip_code IN ("
					+ zipsString + ");";

		ResultSet results = mySQL.executeQuery(query);

		while (results.next()) {
			cityZipXref.put(results.getString(1), results.getString(2));
		}
	}

	@SuppressWarnings("unchecked")
	private void writeFacilityData(String shopToUse, File outputfile) throws IOException {

		BufferedWriter out = new BufferedWriter(new FileWriter(outputfile));

		String shopName = "";
		String writeLine = "";
		out.write(writeLine);
		out.flush();
		String writeLine2 = "";
		String zipsString = "";

		try {

			writeLine = "";
			writeLine2 = "";

			Properties props = new Properties();
			FileInputStream fos = new FileInputStream("/opt/apps/properties/directory.properties");
			props.load(fos);

			String webServiceUrl = props.getProperty("address");
			String username = props.getProperty("username");
			String password = props.getProperty("password");
			String shopcode = props.getProperty("shopcode");
			
			StringBuilder params = new StringBuilder();
			params.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			params.append("<memberDirectoryInterface>");
			params.append("<searchShopRequest>");
			params.append("<security>");
			params.append("<username>"+username+"</username>");
			params.append("<password>"+password+"</password>");
			params.append("<shopCode>"+shopcode+"</shopCode>");
			params.append("</security>");
			params.append("<memberDirectorySearchOptions>");
			params.append("<searchByShopCode>");
			params.append("<shopCode>" + shopToUse + "</shopCode>");
			params.append("</searchByShopCode>");
			params.append("</memberDirectorySearchOptions>");
			params.append("</searchShopRequest>");
			params.append("</memberDirectoryInterface>");
			String webServiceData = "func=getMemberDirectory&data=" + URLEncoder.encode(params.toString(), "UTF-8");
			
			String result = WebServiceUtil.sendGetRequest(webServiceUrl,webServiceData);
			
			if(result != null) {
				SAXBuilder builder = new SAXBuilder();
				Document document = builder.build(new ByteArrayInputStream(result.getBytes()));
				
				Element rootElement = document.getRootElement();
				Element responseElement = rootElement.getChild("searchShopResponse");
				Element shopsElement = responseElement.getChild("shops");
				if(shopsElement != null){
					Element shopElement = shopsElement.getChild("shop");
					Element name = shopElement.getChild("name");
					shopName = name.getValue();
					Element servicedZips = shopElement.getChild("servicedZips");
					List<Element> allServicedZips = servicedZips.getChildren("zip");
					
					for(Element element : allServicedZips){
						if(element != null && element.getValue() != null && !element.getValue().equals("")){
							if(element.getValue().length() == 3)
								zipsString += "00" + element.getValue() + ",";
							if(element.getValue().length() == 4)
								zipsString += "0" + element.getValue() + ",";
							else
								zipsString += element.getValue() + ",";
						}
					}
					
				}
			}
			
			fos.close();
			
			if(zipsString.length() > 0)
				zipsString = zipsString.substring(0, zipsString.length() -1);
			
			setXrefData(zipsString);

			String query = "SELECT f.name, t.name, telephone_number, street_address1, c.name, s.short_name, postal_code"
					+ " FROM facility f" + " INNER JOIN address a ON f.address_id = a.id"
					+ " INNER JOIN city c ON a.city_id = c.id" + " INNER JOIN state s ON c.state_id = s.id"
					+ " INNER JOIN facility_type t ON f.facility_type_id = t.id" + " WHERE a.postal_code IN ("
					+ zipsString + ") ORDER BY f.name ASC;";

			ResultSet results = mySQL.executeQuery(query);
			Map<String, String> facilityData = new HashMap<String, String>();
			Map<String,String> facilitiesSeen = new HashMap<String,String>();
			facilityData.put("FUNERAL HOME","");
			facilityData.put("HOSPITALS AND NURSING HOMES","");
			while (results.next()) {
				if(facilitiesSeen.get(results.getString(1)+results.getString(4)+results.getString(5)+results.getString(6)+results.getString(7)) != null)
						continue;
				String facType = results.getString("t.name");
				if (facType.equalsIgnoreCase("FUNERAL HOME")) {
					String existingData = facilityData.get(facType.toUpperCase());
					if (existingData == null)
						existingData = "";
					facilityData.put(facType.toUpperCase(),
							existingData + "\t\t<tr height=\"20\">\n"
									+ "\t\t\t<td height=\"20\" style=\"height: 20px;\">" + results.getString(1)
									+ " located at " + results.getString(4) + ", " + results.getString(5) + ", "
									+ results.getString(6) + ", " + results.getString(7) + "</td>\n" + "\t\t</tr>\n");
				} else {
					String existingData = facilityData.get("HOSPITALS AND NURSING HOMES");
					if (existingData == null)
						existingData = "";
					facilityData.put("HOSPITALS AND NURSING HOMES",
							existingData + "\t\t<tr height=\"20\">\n"
									+ "\t\t\t<td height=\"20\" style=\"height: 20px;\">" + results.getString(1)
									+ " located at " + results.getString(4) + ", " + results.getString(5) + ", "
									+ results.getString(6) + ", " + results.getString(7) + "</td>\n" + "\t\t</tr>\n");
				}
				facilitiesSeen.put(results.getString(1)+results.getString(4)+results.getString(5)+results.getString(6)+results.getString(7), "1");
			}
			writeLine += "HOSPITALS AND NURSING HOMES:\n";
			writeLine += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 765px;\" width=\"765\"> \n"
					+ " \t <colgroup>\n" + " \t\t<col />\n" + "\t</colgroup>\n" + "\t<tbody>\n";
			writeLine += facilityData.get("HOSPITALS AND NURSING HOMES");
			writeLine += "\t</tbody>\n" + "</table>\n";
			writeLine += "FUNERAL HOMES:\n";
			writeLine += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 765px;\" width=\"765\"> \n"
					+ " \t <colgroup>\n" + " \t\t<col />\n" + "\t</colgroup>\n" + "\t<tbody>\n";
			writeLine += facilityData.get("FUNERAL HOME");
			writeLine += "\t</tbody>\n" + "</table>\n";
			results.close();
			mySQL.closeStatement();

			writeLine2 += shopToUse + "\t" + shopName + " serves the following cities: ";
			List<String> citiesGotten = new ArrayList<String>();
			String[] zips;
			if(zipsString.contains(","))
				zips = zipsString.split(",");
			else
				zips = new String[]{zipsString};
			for (int ii = 0; ii < zips.length; ii++) {
				String zip = zips[ii];
				String city = cityZipXref.get(zip);
				if (!citiesGotten.contains(city) && city != null && !city.equals("")) {
					writeLine2 += city + ",";
					citiesGotten.add(city);
				}
			}
			writeLine2 = writeLine2.substring(0, writeLine2.length() - 1);
			writeLine2 += " and zip codes: " + zipsString + "\n";

			out.write(writeLine);
			out.flush();
			out.write("Coverage:\n" + writeLine2.replaceAll(",", ", "));
			out.flush();
		} catch (Exception ee) {
			ee.printStackTrace();
		}
		out.flush();
		out.close();
	}

}
