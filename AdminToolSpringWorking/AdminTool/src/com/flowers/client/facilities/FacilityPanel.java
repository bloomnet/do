package com.flowers.client.facilities;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.BooleanQuery.Builder;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.server.entity.Facility;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class FacilityPanel extends VerticalLayout {
	private static final long serialVersionUID = 1109959475307724212L;
	private FacilityTable facilityTable;
	private FacilityForm facilityForm;
	private TextField facilityName;
	private TextField facilityCityName;
	private TextField facilityPhone;
	private Button searchButton;
	private Button uploadFacilities;
	private Label buildLabel;
	private MediaUploadWindow mediaUploadWindow;
	private MediaUploadFacilityWindow mediaUploadFacilityWindow;

	public HorizontalLayout getFacilitySearch() {
		HorizontalLayout facilitySearchLayout = new HorizontalLayout();
		facilitySearchLayout.setSpacing(true);
		facilitySearchLayout.setHeight("40px");

		facilityName = new TextField("Facility Name");
		facilitySearchLayout.addComponent(facilityName);
		facilitySearchLayout.setComponentAlignment(facilityName,
				Alignment.MIDDLE_LEFT);

		facilityCityName = new TextField("City Name");
		facilitySearchLayout.addComponent(facilityCityName);
		facilitySearchLayout.setComponentAlignment(facilityCityName,
				Alignment.MIDDLE_LEFT);

		facilityPhone = new TextField("Phone");
		facilitySearchLayout.addComponent(facilityPhone);
		facilitySearchLayout.setComponentAlignment(facilityPhone,
				Alignment.MIDDLE_LEFT);

		searchButton = new Button("Search");
		facilitySearchLayout.addComponent(searchButton);
		facilitySearchLayout.setComponentAlignment(searchButton,
				Alignment.BOTTOM_LEFT);
		searchButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046084848991062L;

			public void buttonClick(ClickEvent event) {
				Facility facility = new Facility();
				facilityForm.setObjectToForm(facility);
				facilityForm.editButton.setVisible(false);
				facilityForm.removeButton.setVisible(false);
				if (!"".equals(facilityName.getValue()))
					search();
				else if (!"".equals(facilityCityName.getValue()))
					searchCity();
				else
					searchPhone();
			}
		});
		Button indexButton = new Button("Index");
		facilitySearchLayout.addComponent(indexButton);
		facilitySearchLayout.setComponentAlignment(indexButton,
				Alignment.BOTTOM_LEFT);
		indexButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;

			public void buttonClick(ClickEvent event) {
				BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
				SearchService searchService = app.getBean("SearchService", SearchService.class);
				SingleThread st = new SingleThread();
				try {
					st.index(Facility.class, searchService,true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		final Button importFacilityReferencesButton = new Button("Import Facility Refs");
		facilitySearchLayout.addComponent(importFacilityReferencesButton);
		facilitySearchLayout.setComponentAlignment(importFacilityReferencesButton, Alignment.BOTTOM_LEFT);
		importFacilityReferencesButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3261115526952442776L;
			public void buttonClick(ClickEvent event) {
				importFacilityReferencesButton.setEnabled(false);
				try {
					mediaUploadWindow = new MediaUploadWindow();
					getApplication().getMainWindow().addWindow(mediaUploadWindow);
					importFacilityReferencesButton.setEnabled(true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		uploadFacilities = new Button("Upload Facilities");
		facilitySearchLayout.addComponent(uploadFacilities);
		facilitySearchLayout.setComponentAlignment(uploadFacilities, Alignment.BOTTOM_LEFT);
		uploadFacilities.addListener(new ClickListener() {
			private static final long serialVersionUID = 3261115526952442776L;
			public void buttonClick(ClickEvent event) {
				importFacilityReferencesButton.setEnabled(false);
				try {
						mediaUploadFacilityWindow = new MediaUploadFacilityWindow();
						getApplication().getMainWindow().addWindow(mediaUploadFacilityWindow);
						importFacilityReferencesButton.setEnabled(true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		buildLabel = new Label();
		facilitySearchLayout.addComponent(buildLabel);
		buildLabel.setImmediate(true);
		facilitySearchLayout.setComponentAlignment(buildLabel,
				Alignment.BOTTOM_LEFT);
		
		return facilitySearchLayout;
	}


	protected void search() {
		try {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			SearchService searchService = app.getBean("SearchService", SearchService.class);
			List<Document> facilities = null;
			SortField sortField = new SortField("name", SortField.Type.STRING);
			if (!"".equals(facilityName.getValue())) {
				org.apache.lucene.search.PhraseQuery.Builder query = new PhraseQuery.Builder();
				
				String[] searchTerm = facilityName.getValue().toString().toLowerCase().split(" ");
				
				for(String word : searchTerm){
					query.add(new Term("tokenizedName",word));
				}
				facilities = searchService.documents(Facility.class, query.build(),
						sortField);
			} else {
				facilities = searchService.documents(Facility.class,
						new MatchAllDocsQuery(), sortField);
			}
			if (facilities != null) {
				facilityTable.removeAllItems();
				for (Document d : facilities) {
					facilityTable.addDocumentToContainer(d);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected void searchCity() {
		try {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			SearchService searchService = app.getBean("SearchService", SearchService.class);
			List<Document> facilities = null;
			SortField sortField = new SortField("city", SortField.Type.STRING);
			if (!"".equals(facilityCityName.getValue())) {
				Builder bq = new BooleanQuery.Builder();
				bq.add(new TermQuery(new Term("city", (facilityCityName.getValue().toString().toUpperCase()))), BooleanClause.Occur.MUST);
				facilities = searchService.documents(Facility.class, bq.build(),
						sortField);
			} else {
				facilities = searchService.documents(Facility.class,
						new MatchAllDocsQuery(), sortField);
			}
			if (facilities != null) {
				facilityTable.removeAllItems();
				for (Document d : facilities) {
					facilityTable.addDocumentToContainer(d);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected void searchPhone() {
		try {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			SearchService searchService = app.getBean("SearchService", SearchService.class);
			List<Document> facilities = null;
			SortField sortField = new SortField("phone", SortField.Type.STRING);
			if (!"".equals(facilityPhone.getValue())) {
				Builder bq = new BooleanQuery.Builder();
				org.apache.lucene.search.PhraseQuery.Builder query = new PhraseQuery.Builder();
				
				String searchTerm = facilityPhone.getValue().toString();
				
				query.add(new Term("phone",searchTerm));
				
				bq.add(query.build(), BooleanClause.Occur.MUST);
				facilities = searchService.documents(Facility.class, query.build(),
						sortField);
			} else {
				facilities = searchService.documents(Facility.class,
						new MatchAllDocsQuery(), sortField);
			}
			if (facilities != null) {
				facilityTable.removeAllItems();
				for (Document d : facilities) {
					facilityTable.addDocumentToContainer(d);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	protected void searchOld() {
		try {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			List<Object> facilitys = null;
			if (!"".equals(facilityName.getValue())) {
				StringBuilder pql = new StringBuilder(
						"from Facility s where s.name like :facilityName");
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("facilityName", "%" + facilityName.getValue() + "%");
				facilitys = (List<Object>) st.findResults(pql.toString(), service, params);
			} else {
				facilitys = (List<Object>) st.findResults("from Facility", service, new HashMap<String,Object>());
			}
			if (facilitys != null) {
				facilityTable.removeAllItems();
				for (Object facilityObj : facilitys) {
					Facility facility = (Facility) facilityObj;
					facilityTable.addItemToContainer(facility);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public FacilityPanel() {
		setSizeFull();
		setSpacing(true);
		addComponent(getFacilitySearch());

		facilityTable = new FacilityTable();
		addComponent(facilityTable);
		facilityTable.addListener(new FacilityTable.ValueChangeListener() {
			private static final long serialVersionUID = 2537403500201812771L;

			public void valueChange(ValueChangeEvent event) {
				Long id = (Long) facilityTable.getValue();
				try {
					BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
					CrudService service = app.getBean("CrudService", CrudService.class);
					SingleThread st = new SingleThread();
					if (id != null) {
						Facility obj = (Facility) st.find(Facility.class, id,
								service);
						facilityForm.setObjectToForm(obj);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		facilityForm = new FacilityForm(facilityTable);
		addComponent(facilityForm);

	}
}
