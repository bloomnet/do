package com.flowers.client.facilities;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.ShopFacilityXref;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.flowers.server.util.SQLData;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.ProgressIndicator;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FinishedEvent;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.SucceededEvent;

public class MediaUploadWindow extends Window {
	private static final long serialVersionUID = 4832691384085398012L;
	private SizeCountingReceiver counter;
	private Upload upload;
	private Label state = new Label();
    private Label result = new Label();
    private Label fileName = new Label();
    private Label textualProgress = new Label();
    
    private SQLData mySQL = null;
    
    private List<Long> shopsToIndex;
    
    private ProgressIndicator pi = new ProgressIndicator();

    private String uploadPath = "/opt/data/listing_zip_export";
	
	public MediaUploadWindow() {
		center();
		VerticalLayout layout = (VerticalLayout) getContent();
        layout.setMargin(true);
        layout.setSpacing(true); 
        layout.setWidth("350px");
        layout.setHeight("300px");
        
        counter = new SizeCountingReceiver();
        upload = new Upload("", counter);
        addComponent(new Label("Select a file to import shop to facility references from."));
        
        upload.setImmediate(true);
        upload.setButtonCaption("Select Source File");
        addComponent(upload);

        final Button cancelProcessing = new Button("Cancel");
        cancelProcessing.addListener(new Button.ClickListener() {
        	private static final long serialVersionUID = 1L;

			public void buttonClick(ClickEvent event) {
        		upload.interruptUpload();
        	}
        });
        cancelProcessing.setVisible(false);
        cancelProcessing.setStyleName("small");

        Panel p = new Panel("Status");
        p.setSizeUndefined();
        p.setWidth(300, HorizontalLayout.UNITS_PIXELS);
        FormLayout l = new FormLayout();
        l.setMargin(true);
        p.setContent(l);
        HorizontalLayout stateLayout = new HorizontalLayout();
        stateLayout.setSpacing(true);
        stateLayout.addComponent(state);
        stateLayout.addComponent(cancelProcessing);
        stateLayout.setCaption("Current state");
        state.setValue("Idle");
        l.addComponent(stateLayout);
        fileName.setCaption("File name");
        l.addComponent(fileName);
        result.setCaption("bytes");
        l.addComponent(result);
        pi.setCaption("Progress");
        pi.setVisible(false);
        l.addComponent(pi);
        textualProgress.setVisible(false);
        l.addComponent(textualProgress);

        addComponent(p);

        upload.addListener(new Upload.StartedListener() {
        	private static final long serialVersionUID = 1084163386895143768L;

			public void uploadStarted(StartedEvent event) {
        		// this method gets called immediatedly after upload is
        		// started
        		pi.setValue(0f);
        		pi.setVisible(true);
        		pi.setPollingInterval(500); // hit server frequently to get updates to client
        		textualProgress.setVisible(true);
        		state.setValue("Importing");
        		fileName.setValue(event.getFilename());

        		cancelProcessing.setVisible(true);
        	}
        });

        upload.addListener(new Upload.ProgressListener() {
        	private static final long serialVersionUID = 3318393640870913379L;

			public void updateProgress(long readBytes, long contentLength) {
        		// this method gets called several times during the update
        		pi.setValue(readBytes / (float) contentLength);
        		textualProgress.setValue("Processed " + readBytes + " bytes of " + contentLength);
        		result.setValue(counter.getTotal());
        	}

        });
        upload.addListener(new Upload.SucceededListener() {
        	private static final long serialVersionUID = -4135230597799242206L;

			public void uploadSucceeded(SucceededEvent event) {
        		result.setValue(counter.getTotal() + " (total)");
        	}
        });
        upload.addListener(new Upload.FailedListener() {
        	private static final long serialVersionUID = -8754773108937834980L;

			public void uploadFailed(FailedEvent event) {
        		result.setValue(counter.getTotal()
        				+ " (counting interrupted at "
        				+ Math.round(100 * (Float) pi.getValue()) + "%)");
        	}
        });
        upload.addListener(new Upload.FinishedListener() {
        	private static final long serialVersionUID = -2421647485010631615L;

			public void uploadFinished(FinishedEvent event) {
        		state.setValue("Idle");
        		pi.setVisible(false);
        		textualProgress.setVisible(false);
        		cancelProcessing.setVisible(false);
        		doImport(event.getFilename());
        	}
        });
	}
	protected void doImport(String filename) {
		try {
			mySQL = new SQLData();
			shopsToIndex = new ArrayList<Long>();
			
			HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(uploadPath+"/"+filename));
			HSSFSheet sheet = workbook.getSheet("Sheet1");
			
			Iterator<Row> sheetIterator = sheet.rowIterator();
			
			int lineNumber = 0;
			int totalLines = sheet.getPhysicalNumberOfRows();
			System.out.println("Processing "+(totalLines)+" Shops");
			
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			SingleThread st = new SingleThread();
			SearchService searchService = app.getBean("SearchService", SearchService.class);
			CrudService service = app.getBean("CrudService", CrudService.class);
			
			while(sheetIterator.hasNext()){
				lineNumber++;
				if(lineNumber % 50 == 0) System.out.println(lineNumber+" processed");
				HSSFRow row = (HSSFRow)sheetIterator.next();
				String shopCode = getCellValue(row, 0);
				String phone = formatPhone(getCellValue(row, 1));
				
				String facilityId = "";
				String shopId = "";
				
				String query1 = "SELECT shop_id FROM shop WHERE shop_code = \""+shopCode+"\";";
				String query2 = "SELECT id FROM facility WHERE telephone_number = \""+phone+"\";";
				
				if (!isRowValid(row)){
					System.out.println("InvalidRow "+lineNumber);
					continue;
				}
				try {
					ResultSet results = mySQL.executeQuery(query1);
					if(results.next()){
						shopId = results.getString("shop_id");
						if(!shopId.equals(""))
							if(!shopsToIndex.contains(Long.valueOf(shopId))) 
								shopsToIndex.add(Long.valueOf(shopId));
						ResultSet results2 = mySQL.executeQuery(query2);
						if(results2.next()){
							facilityId = results2.getString("id");
							String query3 = "SELECT id FROM shop_facility_xref WHERE shop_code = \""+shopCode+"\" AND facility_id = \""+facilityId+"\"";
							ResultSet results3 = mySQL.executeQuery(query3);
							if(results3.next()){
								results3.close();
								mySQL.closeStatement();
								continue;
							}
							results3.close();
							mySQL.closeStatement();
							String statement = "INSERT INTO shop_facility_xref(shop_code,facility_id) VALUES(\""+shopCode+"\",\""+facilityId+"\")";
							mySQL.executeStatement(statement);
							results2.close();
							mySQL.closeStatement();
						}else{
							results2.close();
							mySQL.closeStatement();
							continue;
						}
						results.close();
						mySQL.closeStatement();
					}else{
						results.close();
						mySQL.closeStatement();
						continue;
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			st.index(ShopFacilityXref.class, searchService, true);
			st.refreshObject(Shop.class, shopsToIndex, service);
			st.index(Shop.class, shopsToIndex, searchService);
			System.out.println("Done!!!!!!!!!!!!!!");
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	protected String getId(){
		String query = "SELECT LAST_INSERT_ID()";
		ResultSet results = mySQL.executeQuery(query);
		String id = "";
		try {
			results.next();
			id = results.getString("LAST_INSERT_ID()");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		mySQL.closeStatement();
		return id;
	}
	
	protected boolean isRowValid(HSSFRow row){
		Iterator<Cell> cellIterator = row.cellIterator();
		List<Boolean> validators = new ArrayList<Boolean>();
		while (cellIterator.hasNext()){
			Cell cell = cellIterator.next();
			validators.add(!isCellContentNull(cell));
		}
		for (Boolean b:validators){
			if (b.booleanValue()){
				return true;
			}
		}
		return false;
	}
	
	protected boolean isCellContentNull(Cell cell){
		if (cell == null){
			return true;
		}
		switch ( cell.getCellType() ) {
        case HSSFCell.CELL_TYPE_BLANK:
        	return true;
        case HSSFCell.CELL_TYPE_STRING:
        	return (cell.getStringCellValue() ==null) || (cell.getStringCellValue().trim().length() == 0);
        default:
        	return true;
		}
	}
	public void attach() {
		
	}
	public class SizeCountingReceiver implements Receiver {
        private static final long serialVersionUID = -5392491238827740383L;
		private String fileName;
        private String mtype;
        private int total;
                
        public OutputStream receiveUpload(String filename, String MIMEType) {
            total = 0;
            fileName = filename;
            mtype = MIMEType;
            try {
            	return new FilterOutputStream(new FileOutputStream(uploadPath+"/"+filename) {
            		public void write(int b) throws IOException {
            			super.write(b);
            			total++;
            		}
            	});               
            } catch(Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        public String getFileName() {
            return fileName;
        }
        public String getMimeType() {
            return mtype;
        }
		public int getTotal() {
			return total;
		}
        
    }
	protected String getCellValue(HSSFRow row, int index) {
		Cell cell = row.getCell(index);
		if (cell == null) return null;
		cell.setCellType(HSSFCell.CELL_TYPE_STRING);
		String value = cell.getStringCellValue().trim();
		if(value == null || value.length() == 0) return null;
		return value;
	}
	
	protected String formatPhone(String phone){
		
		if(phone == null || phone.equals("")) return "";
		final StringBuilder result = new StringBuilder();
	    final StringCharacterIterator iterator = new StringCharacterIterator(phone);
	    
	    int charNum = 0;
	    
	    char character =  iterator.current();
	    
	    while (character != CharacterIterator.DONE ){
	      
	        charNum++;
	      
	        if (character == '(' || character == ')' || character == ' ') {
	        	charNum--;
	        }else if(charNum == 4 && character != '-'){
	    	    result.append('-');
	            result.append(character);
	            charNum++;
	        }else if(charNum == 8 && character != '-'){
		        result.append('-');
		        result.append(character);
		        charNum++;
		    }else{
			    result.append(character);
		    }
	        character = iterator.next();
	    }
	    return result.toString();
	}
	
}

