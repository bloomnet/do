package com.flowers.client.facilities;
import java.util.Date;
import java.util.List;
import javax.persistence.PersistenceException;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.address.AddressNestedForm;
import com.flowers.client.ui.base.BaseForm;
import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.Address;
import com.flowers.server.entity.Facility;
import com.flowers.server.entity.User;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window.Notification;

public class FacilityForm extends BaseForm<Facility> {
	private static final long serialVersionUID = -4021058109431784984L;
	public static final String[] VISIBLE_FIELD_ORDER = {"facilityType", "name", "telephoneNumber", "faxNumber", "email", "url", "latitude", "longitude", "address"};
	private static final String ADDRESS_NESTED_FORM = "addressNestedForm";
	private ComboBox facilityType;
	private GridLayout layout;
	
	public FacilityForm(TableContainer<Facility> tableContainer) {
		super(tableContainer, VISIBLE_FIELD_ORDER);
		layout = new GridLayout(2, 8);
        layout.setMargin(true, false, false, true);
        layout.setSpacing(true);
        setLayout(layout);
	}
	

	@Override
	protected void attachField(Object propertyId, Field field) {
		if (propertyId.equals("facilityType")) 
			layout.addComponent(field, 0, 0);
		else if (propertyId.equals("name")) 
			layout.addComponent(field, 1, 0);
		else if (propertyId.equals("telephoneNumber")) 
			layout.addComponent(field, 0, 1);
		else if (propertyId.equals("faxNumber")) 
			layout.addComponent(field, 1, 1);
		else if (propertyId.equals("email")) 
			layout.addComponent(field, 0, 2);
		else if (propertyId.equals("url")) 
			layout.addComponent(field, 1, 2);
		else if (propertyId.equals("latitude")) 
			layout.addComponent(field, 0, 3);
		else if (propertyId.equals("longitude")) 
			layout.addComponent(field, 1, 3);
		else if (propertyId.equals("address")) 
			layout.addComponent(field, 0, 4, 1, 7);
	}
	
	@Override
	public Facility getBlankObject() {
		return new Facility();
	}

	@Override
	public Facility insert(Facility t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SearchService searchService = app.getBean("SearchService", SearchService.class);
		SingleThread st = new SingleThread();
		try {
			User u = (User)searchService.find(User.class, 2L);
			t.setUserByUserCreatedId(u);
			t.setDateCreated(new Date());
			AddressNestedForm nestedForm = (AddressNestedForm)getNestedForm(ADDRESS_NESTED_FORM);
			t.setAddress(nestedForm.getObject());

		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(t.getAddress().getCity().getId());
		st.persist(t, service);
		return t;
	}

	@Override
	public Facility update(Facility t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SearchService searchService = app.getBean("SearchService", SearchService.class);
		SingleThread st = new SingleThread();
		try {
			User u = (User)searchService.find(User.class, 2L);
			t.setUserByUserModifiedId(u);
			t.setDateModified(new Date());
		} catch (Exception e) {
			e.printStackTrace();
		}
		st.merge(t, service);
		return t;
	}

	@Override
	public Object remove(Facility t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SingleThread st = new SingleThread();
		st.remove(t, service);
		return t.getId();
	}
	
	@Override
	public FormFieldFactory getFormFieldFactory() {
		return new FacilityFormFieldFactory();
	}
	
	public void saveButtonClickHandler(){
		AddressNestedForm nestedForm = (AddressNestedForm)getNestedForm(ADDRESS_NESTED_FORM);
		if(nestedForm.getCity().getValue() == null ||
			facilityType.getValue() == null){
				getApplication().getMainWindow().showNotification("Please Fill Out All Necessary Fields.", Notification.POSITION_CENTERED);
		}else super.saveButtonClickHandler();
	}
	
	private static final String COMMON_FIELD_WIDTH = "25em";
	public class FacilityFormFieldFactory extends DefaultFieldFactory {
		private static final long serialVersionUID = -5199119368168782293L;
		
		@SuppressWarnings("rawtypes")
		public Field createField(Item item, Object propertyId, Component uiContext) {
			String pid = (String)propertyId;
			if ("address".equals(pid)){
				Address address = ((Facility)((BeanItem)item).getBean()).getAddress();
				AddressNestedForm field = new AddressNestedForm(address);
				field.setPropertyDataSource(item.getItemProperty(propertyId));
				addNestedForm(ADDRESS_NESTED_FORM, field);
				return field;
			}else if ("facilityType".equals(pid)){
				return getFacilityType();
			}else{
				Field field = super.createField(item, propertyId, uiContext);
				if(field instanceof TextField){
					TextField txt = (TextField)field;
					if ("name".equals(pid)){
						txt.setMaxLength(50);
					}else if ("telephoneNumber".equals(pid)){
						txt.setMaxLength(25);
					}else if ("faxNumber".equals(pid)){
						txt.setMaxLength(25);
					}else if ("email".equals(pid)){
						txt.setMaxLength(55);
					}else if ("url".equals(pid)){
						txt.setMaxLength(100);
					}
					txt.setWidth(COMMON_FIELD_WIDTH);
					txt.setNullRepresentation("");
				}
				return field;
			}
		}
	}
	
	public ComboBox getFacilityType() {
		if (facilityType == null){
			facilityType = new ComboBox();
			facilityType.setWidth(COMMON_FIELD_WIDTH);
			facilityType.setCaption("Facility Type");
			facilityType.setNewItemsAllowed(true);
			facilityType.setNullSelectionAllowed(false);
			try {
				BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
				CrudService service = app.getBean("CrudService", CrudService.class);
				SingleThread st = new SingleThread();
				@SuppressWarnings("unchecked")
				List<Object> types = (List<Object>) st.findAll("FacilityType", service);
				if (types != null){
					for (Object o:types){
						facilityType.addItem(o);
					}
				} 
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return facilityType;
	}
}
