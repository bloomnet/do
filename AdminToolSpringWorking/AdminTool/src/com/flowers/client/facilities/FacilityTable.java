package com.flowers.client.facilities;

import org.apache.lucene.document.Document;

import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.Facility;
import com.vaadin.data.Item;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Table;

public class FacilityTable extends Table implements TableContainer<Facility>{
	private static final long serialVersionUID = 4777403686832370347L;
	
	
		public FacilityTable() {
			addContainerProperty("name", String.class, null);
			addContainerProperty("city", String.class, null);
			addContainerProperty("state", String.class, null);
			addContainerProperty("facility type", String.class, null);
			addContainerProperty("telephone number", String.class, null);		
			setSizeFull();
			setHeight(200, AbstractComponent.UNITS_PIXELS);
			setSelectable(true);
			setImmediate(true);
			setNullSelectionAllowed(false);
		}
		
		@Override
		public Item addItemToContainer(Facility t) {
			Item item = this.addItem(t.getId());
			if(item != null) {
				item.getItemProperty("name").setValue(t.getName());
				item.getItemProperty("city").setValue(t.getAddress().getCity().getName());
				item.getItemProperty("state").setValue(t.getAddress().getCity().getState().getName());
				item.getItemProperty("facility type").setValue(t.getFacilityType().getName());
				item.getItemProperty("telephone number").setValue(t.getTelephoneNumber());
			}
			return item;
		}
		
		@Override
		public void selectRow(Facility t) {
			this.setValue(t.getId());
		}
		
		@Override
		public Item addDocumentToContainer(Document d) {
			Long id = Long.valueOf(d.get("id"));
			Item item = this.addItem(id);
			if(item != null) {
				item.getItemProperty("name").setValue(d.get("name"));
				item.getItemProperty("city").setValue(d.get("city"));
				item.getItemProperty("state").setValue(d.get("state"));
				item.getItemProperty("facility type").setValue(d.get("type"));
				item.getItemProperty("telephone number").setValue(d.get("phone"));
			}
			return item;
		}
}
