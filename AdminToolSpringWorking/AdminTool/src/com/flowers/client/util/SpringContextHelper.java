package com.flowers.client.util;

import java.io.Serializable;

import javax.servlet.ServletContext;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.vaadin.Application;
import com.vaadin.terminal.gwt.server.WebApplicationContext;

public class SpringContextHelper implements Serializable{

	private static final long serialVersionUID = 3685608289906923157L;
	private ApplicationContext context;

    public SpringContextHelper(Application application) {
        if(application.getContext() instanceof WebApplicationContext) {
        	ServletContext servletContext = ((WebApplicationContext) application.getContext()).getHttpSession().getServletContext();
        	context = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
        }
    }

    @SuppressWarnings("unchecked")
	public <T> T getBean(final String beanRef, Class<T> classOfService) {
        return (T)context.getBean(beanRef);
    }    
}
