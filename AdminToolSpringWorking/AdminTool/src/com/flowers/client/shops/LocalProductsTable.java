package com.flowers.client.shops;

import org.apache.lucene.document.Document;

import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.LocalProducts;
import com.vaadin.data.Item;
import com.vaadin.ui.Table;

public class LocalProductsTable extends Table implements TableContainer<LocalProducts> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7651572011709922236L;

	public LocalProductsTable() {
		addContainerProperty("product name", String.class, null);
		addContainerProperty("price", Double.class, null);
		addContainerProperty("picture file", String.class, null);
		setSizeFull();
		setSelectable(true);
		setMultiSelect(true);
		setImmediate(true);
		setNullSelectionAllowed(false);
	}
	
	@Override
	public Item addItemToContainer(LocalProducts t) {
		Item item = this.addItem(t.getId());
		if(item != null) {
			item.getItemProperty("product name").setValue(t.getProductName());
			item.getItemProperty("price").setValue(t.getPrice());
			item.getItemProperty("picture file").setValue(t.getPictureFile());

				
		}
		return item;
	}
	
	@Override
	public void selectRow(LocalProducts t) {
		this.setValue(t.getId());
	}
	
	@Override
	public Item addDocumentToContainer(Document d) {
		Long id = Long.parseLong(d.get("id"));
		Item item = this.addItem(id);
		return item;
	}
}
