package com.flowers.client.shops;

import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class FacilityListingSubWindow extends Window {
	private static final long serialVersionUID = 5874934201132175214L;

	public FacilityListingSubWindow() {
		super("Listing", new VerticalLayout());
		VerticalLayout layout = (VerticalLayout) getContent();
		layout.setWidth("100%");
		layout.setHeight("100%");
	}
	
	@Override
	public void close() {
		super.close();
	}
	
}
