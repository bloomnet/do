package com.flowers.client.shops;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import com.flowers.server.util.SQLData;

public class WriteShopProductCounts {

	SQLData mySQL;
	String databaseName;

	public WriteShopProductCounts(File outfile) {

		mySQL = new SQLData();
		mySQL.login();
		
		String statement = "USE bloomnet;";
		mySQL.executeStatement(statement);

		try {
			writeFacilityData(outfile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void writeFacilityData(File outputfile) throws IOException {

		BufferedWriter out = new BufferedWriter(new FileWriter(outputfile));

		String writeLine = "residential_listings,extra_listings,display_ads,side_ads,top_ads,seasonal_background_images,display_ad_bids,side_ad_bids,facility_residential_listings,facility_extra_listings,facility_display_ads,facility_side_ads,facility_top_ads,facility_seasonal_background_images,facility_display_ad_bids,facility_side_ad_bids\n";
		out.write(writeLine);
		out.flush();

		try {

			writeLine = "";

			String query = "SELECT " +
					"(SELECT COUNT(*) FROM listing WHERE listing_type_id = 2 AND (listing_end_date > now() OR listing_end_date IS NULL)) AS residential_listings, " +
					"(SELECT COUNT(*) FROM listing WHERE listing_type_id = 1 AND (listing_end_date > now() OR listing_end_date IS NULL)) AS extra_listings, " +
					"(SELECT COUNT(*) FROM listing WHERE ad_size_id IS NOT NULL AND (ad_end_date > now() OR ad_end_date IS NULL) AND (listing_end_date > now() OR listing_end_date IS NULL)) AS display_ads, " +
					"(SELECT COUNT(*) FROM listing WHERE side_ad_entry_number IS NOT NULL AND side_ad_entry_number != \"\" AND (banner_end_date > now() OR banner_end_date IS NULL) AND (listing_end_date > now() OR listing_end_date IS NULL)) AS side_ads, " +
					"(SELECT COUNT(*) FROM listing WHERE top_ad_entry_number IS NOT NULL AND top_ad_entry_number != \"\" AND (top_ad_end_date > now() OR top_ad_end_date IS NULL) AND (listing_end_date > now() OR listing_end_date IS NULL)) AS top_ads, " +
					"(SELECT COUNT(*) FROM listing WHERE seasonal_images = 1 AND (listing_end_date > now() OR listing_end_date IS NULL)) AS seasonal_background_images, " +
					"(SELECT COUNT(*) FROM listing WHERE bid > 0 AND (listing_end_date > now() OR listing_end_date IS NULL)) AS display_ad_bids, " +
					"(SELECT COUNT(*) FROM listing WHERE side_ad_bid > 0 AND (listing_end_date > now() OR listing_end_date IS NULL)) AS side_ad_bids, " +
					"(SELECT COUNT(*) FROM facility_listing WHERE listing_type_id = 2 AND (listing_end_date > now() OR listing_end_date IS NULL)) AS facility_residential_listings, " +
					"(SELECT COUNT(*) FROM facility_listing WHERE listing_type_id = 1 AND (listing_end_date > now() OR listing_end_date IS NULL)) AS facility_extra_listings, " +
					"(SELECT COUNT(*) FROM facility_listing WHERE ad_size_id IS NOT NULL AND (ad_end_date > now() OR ad_end_date IS NULL) AND (listing_end_date > now() OR listing_end_date IS NULL)) AS facility_display_ads, " +
					"(SELECT COUNT(*) FROM facility_listing WHERE side_ad_entry_number IS NOT NULL AND side_ad_entry_number != \"\" AND (banner_end_date > now() OR banner_end_date IS NULL) AND (listing_end_date > now() OR listing_end_date IS NULL)) AS facility_side_ads, " +
					"(SELECT COUNT(*) FROM facility_listing WHERE top_ad_entry_number IS NOT NULL AND top_ad_entry_number != \"\" AND (top_ad_end_date > now() OR top_ad_end_date IS NULL) AND (listing_end_date > now() OR listing_end_date IS NULL)) AS facility_top_ads, " +
					"(SELECT COUNT(*) FROM facility_listing WHERE seasonal_images = 1 AND (listing_end_date > now() OR listing_end_date IS NULL)) AS facility_seasonal_background_images, " +
					"(SELECT COUNT(*) FROM facility_listing WHERE bid > 0 AND (listing_end_date > now() OR listing_end_date IS NULL)) AS facility_display_ad_bids, " +
					"(SELECT COUNT(*) FROM facility_listing WHERE side_ad_bid > 0 AND (listing_end_date > now() OR listing_end_date IS NULL)) AS facility_side_ad_bids;";

			ResultSet results = mySQL.executeQuery(query);
			
			while (results.next()) {
				
				writeLine = results.getString(1)+","+results.getString(2)+","+results.getString(3)+","+results.getString(4)+","+results.getString(5)+","+results.getString(6)+","+results.getString(7)+","+results.getString(8)+","+results.getString(9)+","+results.getString(10)+","+results.getString(11)+","+results.getString(12)+","+results.getString(13)+","+results.getString(14)+","+results.getString(15)+","+results.getString(16)+"\n";				

				out.write(writeLine);
				out.flush();
			}
			
			results.close();
			mySQL.closeStatement();

		} catch (Exception ee) {
			ee.printStackTrace();
		}
		out.flush();
		out.close();
	}

}
