package com.flowers.client.shops;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.PersistenceException;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.address.AddressNestedForm;
import com.flowers.client.ui.base.BaseForm;
import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.Address;
import com.flowers.server.entity.Listing;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.User;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Window.Notification;

public class ShopForm extends BaseForm<Shop> {	
	private static final long serialVersionUID = -5012593619017655102L;
	private GridLayout layout;
	
	public static final String[] SHOP_FIELD_ORDER = {"shopCode","shopName","telephoneNumber","tollFreeNumber","faxNumber","contactPerson",
			"openSunday","bloomlinkIndicator","newShop","floristForForrests","latitude","longitude", "address", "minimum", "shopWebsite", "primaryWebsite", "shopLogo", "consumerImage", "dontShowCDO", "deliveryFee", "thirdPartySite"};
	private static final String ADDRESS_NESTED_FORM = "addressNestedForm";
	
	public ShopForm(TableContainer<Shop> tableContainer) {
		super(tableContainer, SHOP_FIELD_ORDER);
		layout = new GridLayout(3, 10);
		
        layout.setMargin(true, false, false, true);
        layout.setSpacing(true);
        setLayout(layout);
        
        Button geocodeButton = new Button("Geocode");
		getFooter().addComponent(geocodeButton);
		//layout.setComponentAlignment(geocodeButton, Alignment.BOTTOM_LEFT);
		geocodeButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				try {
					long id = (Long)((ShopTable)getTableContainer()).getValue();
					BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
					CrudService service = app.getBean("CrudService", CrudService.class);
					SingleThread st = new SingleThread();
					service.geocodeShop(id);
					Shop shop = (Shop)st.find(Shop.class, id, service);
					setObjectToForm(shop);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	@Override
	public Shop getBlankObject() {
		return new Shop();
	}

	@Override
	public Shop insert(Shop t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		//ShopCrudService service = app.getSpringContextHelper().getBean("CrudService", ShopCrudService.class);
		CrudService service = app.getBean("CrudService", CrudService.class);
		SearchService searchService = app.getBean("SearchService", SearchService.class);
		SingleThread st = new SingleThread();
		try {
			User u = (User)searchService.find(User.class,2L);
			t.setUserByUserCreatedId(u);
			t.setDateCreated(new Date());
			AddressNestedForm nestedForm = (AddressNestedForm)getNestedForm(ADDRESS_NESTED_FORM);
			t.setAddress(nestedForm.getObject());
		} catch (Exception e) {
			e.printStackTrace();
		}
		st.persist(t, service);
		if(t.getDeliveryFee() != null && t.getListings().size() > 0) {
			ArrayList<Long> listingsToUpdate = new ArrayList<Long>();
			for(Listing l : t.getListings()) {
				l.setDelivery(t.getDeliveryFee());
				listingsToUpdate.add(l.getId());
				st.persist(l, service);
			}
			st.index(Listing.class, listingsToUpdate, service);
		}
		return t;
	}

	@Override
	public Shop update(Shop t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SearchService searchService = app.getBean("SearchService", SearchService.class);
		SingleThread st = new SingleThread();
		try {
			User u = (User)searchService.find(User.class, 2L);
			t.setUserByUserModifiedId(u);
			t.setDateModified(new Date());
		} catch (Exception e) {
			e.printStackTrace();
		}
		st.merge(t, service);
		if(t.getDeliveryFee() != null && t.getListings().size() > 0) {
			for(Listing l : t.getListings()) {
				l.setDelivery(t.getDeliveryFee());
				st.merge(l, service);
			}
		}
		Set<Listing> listings = t.getListings();
		List<Long> indexList = new ArrayList<Long>();
		for(Listing listing : listings) {
			indexList.add(listing.getId());
		}
		st.index(Listing.class, indexList, searchService);
		return t;
	}
	
	@Override
	public Object remove(Shop t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SingleThread st = new SingleThread();
		st.remove(t, service);
		return t.getId();
	}
	
	public void attachField(Object propertyId, Field field) {
		if (propertyId.equals("shopCode")) 
			layout.addComponent(field, 0, 0);
		else if (propertyId.equals("shopName")) 
			layout.addComponent(field, 1, 0);
		else if (propertyId.equals("telephoneNumber")) 
			layout.addComponent(field, 0, 1);
		else if (propertyId.equals("tollFreeNumber")) 
			layout.addComponent(field, 1, 1);
		else if (propertyId.equals("faxNumber")) 
			layout.addComponent(field, 0, 2);
		else if (propertyId.equals("contactPerson")) 
			layout.addComponent(field, 1, 2);
		else if (propertyId.equals("openSunday")) 
			layout.addComponent(field, 0, 3);
		else if (propertyId.equals("bloomlinkIndicator")) 
			layout.addComponent(field, 1, 3);
		else if (propertyId.equals("newShop")) 
			layout.addComponent(field, 0, 4);
		else if (propertyId.equals("floristForForrests")) 
			layout.addComponent(field, 1, 4);
		else if (propertyId.equals("latitude")) 
			layout.addComponent(field, 0, 5);
		else if (propertyId.equals("longitude")) 
			layout.addComponent(field, 1, 5);
		else if (propertyId.equals("address")) 
			layout.addComponent(field, 0, 9, 1, 9);
		else if (propertyId.equals("minimum")) 
			layout.addComponent(field, 0, 6);
		else if (propertyId.equals("shopWebsite")) 
			layout.addComponent(field, 1, 6);
		else if (propertyId.equals("primaryWebsite")) 
			layout.addComponent(field, 0, 7);
		else if (propertyId.equals("shopLogo")) 
			layout.addComponent(field, 1, 7);
		else if (propertyId.equals("consumerImage")) 
			layout.addComponent(field, 0, 8);
		else if (propertyId.equals("dontShowCDO")) 
			layout.addComponent(field, 1, 8);
		else if (propertyId.equals("deliveryFee")) 
			layout.addComponent(field, 2, 0);
		else if (propertyId.equals("thirdPartySite")) 
			layout.addComponent(field, 2, 1);
	}
	@Override
	public FormFieldFactory getFormFieldFactory() {
		return new ShopFormFieldFactory();
	}
	
	public void saveButtonClickHandler(){
		AddressNestedForm nestedForm = (AddressNestedForm)getNestedForm(ADDRESS_NESTED_FORM);
		if(nestedForm.getCity().getValue() == null ||
			getField("shopCode").getValue() == null ||
			getField("shopName").getValue() == null ||
			getField("telephoneNumber").getValue() == null){
				getApplication().getMainWindow().showNotification("Please Fill Out All Necessary Fields.", Notification.POSITION_CENTERED);
		}else super.saveButtonClickHandler();
	}
	
	private static final String COMMON_FIELD_WIDTH = "25em";
	public class ShopFormFieldFactory extends DefaultFieldFactory {
		private static final long serialVersionUID = -5199119368168782293L;
		
		@SuppressWarnings("rawtypes")
		public Field createField(Item item, Object propertyId, Component uiContext) {
			String pid = (String)propertyId;
			if ("address".equals(pid)){
				Address address = ((Shop)((BeanItem)item).getBean()).getAddress();
				AddressNestedForm field = new AddressNestedForm(address);
				field.setPropertyDataSource(item.getItemProperty(propertyId));
				addNestedForm(ADDRESS_NESTED_FORM, field);
				return field;
			}else{
				Field field = super.createField(item, propertyId, uiContext);
				if(field instanceof TextField){
					TextField txt = (TextField)field;
					if ("shopCode".equals(pid)){
						txt.setMaxLength(8);
					}else if ("shopName".equals(pid)){
						txt.setMaxLength(50);
					}else if ("telephoneNumber".equals(pid)){
						txt.setMaxLength(25);
					}else if ("tollFreeNumber".equals(pid)){
						txt.setMaxLength(25);
					}else if ("faxNumber".equals(pid)){
						txt.setMaxLength(25);
					}else if ("contactPerson".equals(pid)){
						txt.setMaxLength(50);
					}
					txt.setWidth(COMMON_FIELD_WIDTH);
					txt.setNullRepresentation("");
				}
				
				return field;
			}
		}
	}
}
