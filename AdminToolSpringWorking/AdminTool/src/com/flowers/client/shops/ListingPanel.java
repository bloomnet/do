package com.flowers.client.shops;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.server.LookupException;
import com.flowers.server.entity.Listing;
import com.flowers.server.entity.Shop;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.Notification;

public class ListingPanel extends VerticalLayout {
	private static final long serialVersionUID = 8310272037766774853L;
	private ListingTable listingTable;
	private Shop parentShop;
	private Button add;
	private Button edit;
	private Button remove;
	private Button showAd;
	
	public ListingPanel() {
		listingTable = new ListingTable();
		addComponent(listingTable);
		add = new Button("Add");
		add.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = -4094808531574784520L;
			@Override
			public void buttonClick(Button.ClickEvent event) {
				if(parentShop == null || parentShop.getShopCode() == null) getApplication().getMainWindow().showNotification("No Shop Selected.", Notification.POSITION_CENTERED);
				else showSubWindow(null, parentShop);
			}
		});
		edit = new Button("Edit");
		edit.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = -1177941627607832172L;
			@SuppressWarnings("unchecked")
			public void buttonClick(Button.ClickEvent event) {
				if (((Set<Listing>)listingTable.getValue()).toArray().length > 0){
					Listing l = null;
					try {
						Set<Listing> selectedListings = (Set<Listing>) listingTable.getValue();
						Long id = (Long)selectedListings.toArray()[0];
						CrudService service = ((BloomNetAdminApplication)getApplication()).getBean("CrudService", CrudService.class);
						SingleThread st = new SingleThread();
						l = (Listing)st.find(Listing.class, id, service);
						showSubWindow(l, l.getShop());						
						
					} catch (LookupException e) {
						e.printStackTrace();
					}
				}else{
					getApplication().getMainWindow().showNotification("No Listing selected to edit.", Notification.POSITION_CENTERED);
				}
			}
		});
		remove = new Button("Remove");
		remove.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = -2772357972276733911L;
			@SuppressWarnings("unchecked")
			public void buttonClick(Button.ClickEvent event) {
				if (((Set<Listing>)listingTable.getValue()).toArray().length > 0 ) {
					try {
						Set<Listing> selectedListings = (Set<Listing>) listingTable.getValue();
						SingleThread st = new SingleThread();
						CrudService service = ((BloomNetAdminApplication)getApplication()).getBean("CrudService", CrudService.class);
						SearchService searchService = ((BloomNetAdminApplication)getApplication()).getBean("SearchService", SearchService.class);
						List<Object> findList = new ArrayList<Object>();
						Object[] listingArray = (Object[])selectedListings.toArray();
						for(int ii=0; ii<selectedListings.toArray().length; ++ii){
							findList.add(listingArray[ii]);
						}
						List<Object> resultsList = (List<Object>) st.findMultiple(Listing.class, findList, service);
						st.removeMultiple(resultsList, service);
						for(int ii=0; ii<resultsList.size(); ++ii){
							Listing listing = (Listing)resultsList.get(ii);
							listingTable.removeItem(listing.getId());
							parentShop.getListings().remove(listing);
						}
						st.index(Shop.class, parentShop.getId(), searchService);
					
					} catch (LookupException e) {
						e.printStackTrace();
					}
				}else{
					getApplication().getMainWindow().showNotification("No Listing selected to remove.", Notification.POSITION_CENTERED);
				}
			}
		});
		showAd = new Button("Show Ad");
		showAd.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = -1177941627607832172L;
			@SuppressWarnings("unchecked")
			public void buttonClick(Button.ClickEvent event) {
				if (((Set<Listing>)listingTable.getValue()).toArray().length > 0){
					try {
						Set<Listing> selectedListings = (Set<Listing>) listingTable.getValue();
						Long id = (Long)selectedListings.toArray()[0];
						CrudService service = ((BloomNetAdminApplication)getApplication()).getBean("CrudService", CrudService.class);
						SingleThread st = new SingleThread();
						Listing listing = (Listing)st.find(Listing.class, id, service);
						if(listing.getAdSize() != null)
							showSubWindow(listing, listing.getShop(), listing.getShop().getShopCode());
						else
							getApplication().getMainWindow().showNotification("There is no ad to display for this listing", Notification.POSITION_CENTERED);
					} catch (LookupException e) {
						e.printStackTrace();
					}
				}else{
					getApplication().getMainWindow().showNotification("No Listing selected to show ad.", Notification.POSITION_CENTERED);
				}
			}
		});
		HorizontalLayout footer = new HorizontalLayout();
		footer.addComponent(add);
		footer.addComponent(edit);
		footer.addComponent(remove);
		footer.addComponent(showAd);
		addComponent(footer);
		
	}
	public void attach() {
		super.attach();
	}
	protected void showSubWindow(Listing listing, Shop shop){
		ListingSubWindow subWindow = new ListingSubWindow();
		subWindow.setModal(true);
		subWindow.setWidth("600px");
		subWindow.setHeight("800px");
		ListingForm listingForm = new ListingForm(listingTable, (BloomNetAdminApplication)getApplication(), shop);
		listingForm.setParentSubWindow(subWindow);
		subWindow.addComponent(listingForm);
		getApplication().getMainWindow().addWindow(subWindow);
		if (listing == null){
			listingForm.addButtonClickHandler();
		}else{
			listingForm.setObjectToForm(listing);
			listingForm.setReadOnly(false);
			listingForm.handleButtons(false);
		}
		if(listing != null && listing.getCity() != null) listingForm.getCity().setValue(listing.getCity().getName() + ", " + listing.getCity().getState().getShortName());
	}
	
	protected void showSubWindow(Listing listing, Shop shop, String shopCode){
		ListingSubWindow subWindow = new ListingSubWindow();
		subWindow.setModal(true);
		subWindow.setWidth("300px");
		subWindow.setHeight("500px");
		URL url = null;
		try {
			url = new URL("http://directory.bloomnet.net/bloomnet-images/ads/"+shopCode+"-"+listing.getEntryCode()+listing.getAdSize()+".png");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		Embedded image = new Embedded("", new ExternalResource(url));
		image.setType(Embedded.TYPE_IMAGE);
		subWindow.addComponent(image);
		image.setSizeFull();
		getApplication().getMainWindow().addWindow(subWindow);
	}
	
	public void setShop(Shop shop) {
		parentShop = shop;
		listingTable.removeAllItems();
		for(Listing listing : shop.getListings()) {
			listingTable.addItemToContainer(listing);
		}
	}
}
