package com.flowers.client.shops;

import org.apache.lucene.document.Document;

import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.Shop;
import com.vaadin.data.Item;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Table;

public class ShopTable extends Table implements TableContainer<Shop>{
	private static final long serialVersionUID = -4833773405613863032L;
	
	public ShopTable() {
		addContainerProperty("shopCode", String.class, null);
		addContainerProperty("shopName", String.class, null);
		addContainerProperty("contactPerson", String.class, null);		
		setSizeFull();
		setHeight(200, AbstractComponent.UNITS_PIXELS);
		setSelectable(true);
		setImmediate(true);
		setNullSelectionAllowed(false);
	}

	@Override
	public void selectRow(Shop t) {
		this.setValue(t.getId());
	}
	
	@Override
	public Item addItemToContainer(Shop t) {
		Item item = this.addItem(t.getId());
		if(item != null) {
			item.getItemProperty("shopCode").setValue(t.getShopCode());
			item.getItemProperty("shopName").setValue(t.getShopName());
			item.getItemProperty("contactPerson").setValue(t.getContactPerson());
		}
		return item;
	}
	
	@Override
	public Item addDocumentToContainer(Document d) {
		Long id = Long.valueOf(d.get("id"));
		Item item = this.addItem(id);
		if(item != null) {
			item.getItemProperty("shopCode").setValue(d.get("shop_code"));
			item.getItemProperty("shopName").setValue(d.get("shop_name"));
			item.getItemProperty("contactPerson").setValue(d.get("contact_person"));
		}
		return item;
	}
}
