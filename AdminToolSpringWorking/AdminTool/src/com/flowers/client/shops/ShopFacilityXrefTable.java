package com.flowers.client.shops;

import org.apache.lucene.document.Document;

import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.ShopFacilityXref;
import com.vaadin.data.Item;
import com.vaadin.ui.Table;

public class ShopFacilityXrefTable extends Table implements TableContainer<ShopFacilityXref>{
	private static final long serialVersionUID = 4777403686832370347L;
	
	
		public ShopFacilityXrefTable() {
			addContainerProperty("name", String.class, null);
			addContainerProperty("city", String.class, null);
			addContainerProperty("state", String.class, null);
			addContainerProperty("facilityType", String.class, null);
			addContainerProperty("telephoneNumber", String.class, null);		
			//setVisibleColumns(ShopContainer.NATURAL_COL_ORDER);
			//setColumnHeaders(ShopContainer.COL_HEADERS_ENGLISH);
			//setShopContainer(null);
			setSizeFull();
			setSelectable(true);
			setImmediate(true);
			setMultiSelect(true);
			setNullSelectionAllowed(false);
		}
		
		@Override
		public Item addItemToContainer(ShopFacilityXref t) {
			Item item = this.addItem(t.getId());
			if(item != null) {
				item.getItemProperty("name").setValue(t.getFacility().getName());
				item.getItemProperty("city").setValue(t.getFacility().getAddress().getCity().getName());
				item.getItemProperty("state").setValue(t.getFacility().getAddress().getCity().getState().getName());
				item.getItemProperty("facilityType").setValue(t.getFacility().getFacilityType().getName());
				item.getItemProperty("telephoneNumber").setValue(t.getFacility().getTelephoneNumber());
			}
			return item;
		}
		
		@Override
		public void selectRow(ShopFacilityXref t) {
			this.setValue(t.getId());
		}
		
		@Override
		public Item addDocumentToContainer(Document d) {
			throw new RuntimeException("Not implemented on " + this.getClass().getName());
		}
}
