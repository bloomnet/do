package com.flowers.client.shops;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.PersistenceException;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.ui.base.BaseForm;
import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.LookupException;
import com.flowers.server.entity.City;
import com.flowers.server.entity.FacilityListing;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.State;
import com.flowers.server.entity.User;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Item;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.Notification;

public class FacilityListingForm extends BaseForm<FacilityListing> {
	private static final long serialVersionUID = 6751659598235142499L;
	private Shop parentShop;
	private Window parentSubWindow;
	private GridLayout layout;
	private TextField city;
	private ComboBox listingType;
	private ComboBox adSize;
	private ComboBox mac;

	
	public static final String[] visibleFieldOrder = new String[]{"entryCode", "revAd", "customListing", "minOrder", 
																			 "delivery", "date", "city", "listingType", "adSize", "mac", "bid", "sideAdEntryNumber",
																			 "adStartDate", "adEndDate", "bannerStartDate", "listingEndDate", "bannerEndDate","topAdEntryNumber",
																			 "topAdStartDate", "topAdEndDate", "moreInfo", "sideAdBid", "seasonalImages","vdayBackground","springBackground",
																			 "mdayBackground","summerBackground","fallBackground","winterBackground","featuredListing","preferredListing"};
	public FacilityListingForm(TableContainer<FacilityListing> tableContainer, BloomNetAdminApplication bloomNetApp, Shop parentShop) {
		super(tableContainer, visibleFieldOrder);
		layout = new GridLayout(3, 15);
		layout.setMargin(true, false, false, true);
        layout.setSpacing(true);
        setLayout(layout);
		this.parentShop = parentShop;
	}
		
	public void attachField(Object propertyId, Field field) {
		if (propertyId.equals("entryCode")) 
			layout.addComponent(field, 0, 0);
		else if (propertyId.equals("customListing")) 
			layout.addComponent(field, 0, 1, 1, 1);
		else if (propertyId.equals("city")) 
			layout.addComponent(field, 0, 2, 1, 2);
		else if (propertyId.equals("listingType")) 
			layout.addComponent(field, 0, 3);
		else if (propertyId.equals("mac")) 
			layout.addComponent(field, 1, 3);
		else if (propertyId.equals("delivery")) 
			layout.addComponent(field, 0, 4);
		else if (propertyId.equals("minOrder")) 
			layout.addComponent(field, 1, 4);
		else if (propertyId.equals("sideAdEntryNumber")) 
			layout.addComponent(field, 0, 5);
		else if (propertyId.equals("adSize")) 
			layout.addComponent(field, 1, 5);
		else if (propertyId.equals("bannerStartDate")) 
			layout.addComponent(field, 0, 6);		
		else if (propertyId.equals("adStartDate")) 
			layout.addComponent(field, 1, 6);
		else if (propertyId.equals("bannerEndDate")) 
			layout.addComponent(field, 0, 7);
		else if (propertyId.equals("adEndDate")) 
			layout.addComponent(field, 1, 7);
		else if (propertyId.equals("listingEndDate")) 
			layout.addComponent(field, 0, 8);
		else if (propertyId.equals("bid")) 
			layout.addComponent(field, 1, 8);
		else if (propertyId.equals("topAdEntryNumber")) 
			layout.addComponent(field, 0, 9);
		else if (propertyId.equals("topAdStartDate")) 
			layout.addComponent(field, 1, 9);
		else if (propertyId.equals("topAdEndDate")) 
			layout.addComponent(field, 0, 10);
		else if (propertyId.equals("moreInfo")) 
			layout.addComponent(field, 1, 10);
		else if (propertyId.equals("sideAdBid")) 
			layout.addComponent(field, 0, 11);
		else if (propertyId.equals("featuredListing")) 
			layout.addComponent(field, 1, 11);
		else if (propertyId.equals("preferredListing")) 
			layout.addComponent(field, 2, 11);
		else if (propertyId.equals("seasonalImages")) 
			layout.addComponent(field, 2, 3);
		else if (propertyId.equals("vdayBackground")) 
			layout.addComponent(field, 2, 4);
		else if (propertyId.equals("springBackground")) 
			layout.addComponent(field, 2, 5);
		else if (propertyId.equals("mdayBackground")) 
			layout.addComponent(field, 2, 6);
		else if (propertyId.equals("summerBackground")) 
			layout.addComponent(field, 2, 7);
		else if (propertyId.equals("fallBackground")) 
			layout.addComponent(field, 2, 8);
		else if (propertyId.equals("winterBackground")) 
			layout.addComponent(field, 2, 9);
	}
	
	public void setParentSubWindow(Window parentSubWindow) {
		this.parentSubWindow = parentSubWindow;
	}
	
	@Override
	public void cancelButtonClickHandler() {
		discard();
		if (parentSubWindow != null && (parentSubWindow instanceof ListingSubWindow)){
			((ListingSubWindow)parentSubWindow).close();
		}
	}
	
	@Override
	public void saveButtonClickHandler() {
		if(parentShop == null){
			getApplication().getMainWindow().showNotification("No Shop Selected.", Notification.POSITION_CENTERED);
		}else if(city.getValue() != null && listingType.getValue() != null){
			city.setReadOnly(true);
			super.saveButtonClickHandler();
			if (parentSubWindow != null && (parentSubWindow instanceof ListingSubWindow)){
				((ListingSubWindow)parentSubWindow).close();
			}
	    }else{
	    	getApplication().getMainWindow().showNotification("Please fill in all necessary fields.", Notification.POSITION_CENTERED);
	    	}
	}
	
	@Override
	public FacilityListing getBlankObject() {
		return new FacilityListing();
	}

	@Override
	public FacilityListing insert(FacilityListing t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SearchService searchService =app.getBean("SearchService", SearchService.class);
		SingleThread st = new SingleThread();
		try {
			User u = (User)st.find(User.class, 2L, service);
			t.setUserByUserCreatedId(u);
			t.setDateCreated(new Date());
			t.setShop(parentShop);
			String state = city.getValue().toString().split(",")[1].trim();
			String cityName = city.getValue().toString().split(",")[0].trim();
			State selectedState = (State)st.findSingleResult("from State s where s.shortName = '"+state+"'", service);
			if(selectedState == null){
				getApplication().getMainWindow().showNotification("Invalid City/State.", Notification.POSITION_CENTERED);
				city.setReadOnly(false);
				return t;
			}
			City selectedCity = (City) st.findSingleResult("from City c where c.name = '"+cityName+"' and state.id = "+selectedState.getId()+" and c.cityType = 'D'", service);
			if(selectedCity == null){
				getApplication().getMainWindow().showNotification("Invalid City/State.", Notification.POSITION_CENTERED);
				city.setReadOnly(false);
				return t;
			}
			t.setCity(selectedCity);
		} catch (Exception e) {
			e.printStackTrace();
		}
		st.persist(t, service);
		parentShop.getFacilityListings().add(t);
		try{
			st.index(t.getShop().getClass(), t.getShop().getId(), searchService);
			List<Long> indexList = new ArrayList<Long>();
			for(FacilityListing facilityListing : t.getShop().getFacilityListings()) {
				indexList.add(facilityListing.getId());
			}
			st.index(FacilityListing.class, indexList, searchService);
		}catch(Exception ee){
			ee.printStackTrace();
		}
		return t;
	}

	@Override
	public FacilityListing update(FacilityListing t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SearchService searchService =app.getBean("SearchService", SearchService.class);
		SingleThread st = new SingleThread();
		try {
			User u = (User)searchService.find(User.class, 2L);
			t.setUserByUserModifiedId(u);
			t.setDateModified(new Date());
			String state = city.getValue().toString().split(",")[1].trim();
			String cityName = city.getValue().toString().split(",")[0].trim();
			State selectedState = (State)st.findSingleResult("from State s where s.shortName = '"+state+"'", service);
			if(selectedState == null){
				getApplication().getMainWindow().showNotification("Invalid City/State.", Notification.POSITION_CENTERED);
				city.setReadOnly(false);
				return t;
			}
			City selectedCity = (City) st.findSingleResult("from City c where c.name = '"+cityName+"' and state.id = "+selectedState.getId()+" and c.cityType = 'D'", service);
			if(selectedCity == null){
				getApplication().getMainWindow().showNotification("Invalid City/State.", Notification.POSITION_CENTERED);
				city.setReadOnly(false);
				return t;
			}
			t.setCity(selectedCity);
		} catch (Exception e) {
			e.printStackTrace();
		}
		st.merge(t, service);
		try{
			st.index(t.getShop().getClass(), t.getShop().getId(), searchService);
			List<Long> indexList = new ArrayList<Long>();
			for(FacilityListing listing : t.getShop().getFacilityListings()) {
				indexList.add(listing.getId());
			}
			st.index(FacilityListing.class, indexList, searchService);
		}catch(Exception ee){
			ee.printStackTrace();
		}
		return t;
	}

	@Override
	public Object remove(FacilityListing t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SearchService searchService = app.getBean("SearchService", SearchService.class);
		SingleThread st = new SingleThread();
		st.remove(t, service);
		Set<FacilityListing> facilityListings = t.getShop().getFacilityListings();
		List<Long> indexList = new ArrayList<Long>();
		for(FacilityListing facilityListing : facilityListings) {
			indexList.add(facilityListing.getId());
		}
		st.index(FacilityListing.class, indexList, searchService);
		st.index(Shop.class, t.getShop().getId(), searchService);
		return t.getId();
	}
	
	@Override
	public FormFieldFactory getFormFieldFactory() {
		return new ListingFieldFactory();
	}
	
	public class ListingFieldFactory extends DefaultFieldFactory{
		private static final long serialVersionUID = -2037407585689870276L;
		public Field createField(Item item, Object propertyId, Component uiContext) {
			String pid = (String)propertyId;
			if ("customListing".equals(pid)){
				TextArea field = new TextArea("Custom Listing");
				field.setWordwrap(true);
				field.setRows(4);
				field.setColumns(25);
				field.setNullRepresentation("");
				return field;
			}else if ("city".equals(pid)){
				return getCity();
			}else if ("listingType".equals(pid)){
				return getListingType();
			}else if ("adSize".equals(pid)){
				return getAdSize();
			}else if ("mac".equals(pid)){
				return getMac();
			}else{
				Field field = super.createField(item, propertyId, uiContext);
				if(field instanceof TextField){
					TextField tf = (TextField)field;
					tf.setNullRepresentation("");
					if("entryCode".equals(pid)){
						tf.setMaxLength(2);
					}
				}
				
				return field;
			}
		}
	}

	public TextField getCity() {
		if (city == null){
			city = new TextField("City");
			city.setValue("");
			city.setNullRepresentation("");
			city.setWidth("325px");
			city.setRequired(false);
		}
		return city;
	}
	
	public ComboBox getListingType() {
		if (listingType == null){
			listingType = new ComboBox("Listing Type");
			listingType.setRequired(true);
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			try {
				SingleThread st = new SingleThread();
				@SuppressWarnings("unchecked")
				List<Object> listingTypes = (List<Object>) st.findAll("ListingType", service);
				if (listingTypes != null){
					for (Object o:listingTypes){
						listingType.addItem(o);
					}
				}
			} catch (LookupException e) {
				e.printStackTrace();
			} 

		}
		return listingType;
	}
	
	public ComboBox getAdSize() {
		if (adSize == null){
			adSize = new ComboBox("Ad Size");
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			try {
				
				SingleThread st = new SingleThread();
				@SuppressWarnings("unchecked")
				List<Object> adSizes = (List<Object>) st.findAll("AdSize", service);
				if (adSizes != null){
					for (Object o:adSizes){
						adSize.addItem(o);
					}
				}
			} catch (LookupException e) {
				e.printStackTrace();
			} 

		}
		return adSize;
	}
	
	public ComboBox getMac() {
		if (mac == null){
			mac = new ComboBox("Mac");
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			try {
				SingleThread st = new SingleThread();
				@SuppressWarnings("unchecked")
				List<Object> macs = (List<Object>) st.findAll("Mac", service);
				if (macs != null){
					for (Object o:macs){
						mac.addItem(o);
					}
				}
			} catch (LookupException e) {
				e.printStackTrace();
			} 

		}
		return mac;
	}
	
}
