package com.flowers.client.shops;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.server.LookupException;
import com.flowers.server.entity.LocalProducts;
import com.flowers.server.entity.Shop;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Window.Notification;

public class LocalProductsPanel extends VerticalLayout {
	private static final long serialVersionUID = 8310272037766774853L;
	private LocalProductsTable productsTable;
	private Shop parentShop;
	private Button add;
	private Button edit;
	private Button remove;
	private Button uploadLP;
	
	public LocalProductsPanel() {
		productsTable = new LocalProductsTable();
		addComponent(productsTable);
		add = new Button("Add");
		add.addListener(new Button.ClickListener() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -955064449217826361L;

			@Override
			public void buttonClick(Button.ClickEvent event) {
				if(parentShop == null || parentShop.getShopCode() == null) getApplication().getMainWindow().showNotification("No Shop Selected.", Notification.POSITION_CENTERED);
				else showSubWindow(null, parentShop);
			}
		});
		edit = new Button("Edit");
		edit.addListener(new Button.ClickListener() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -5988244174264459324L;

			@SuppressWarnings("unchecked")
			public void buttonClick(Button.ClickEvent event) {
				if (((Set<LocalProducts>)productsTable.getValue()).toArray().length > 0){
					LocalProducts lp = null;
					try {
						Set<LocalProducts> selectedProducts = (Set<LocalProducts>) productsTable.getValue();
						Long id = (Long)selectedProducts.toArray()[0];
						CrudService service = ((BloomNetAdminApplication)getApplication()).getBean("CrudService", CrudService.class);
						SingleThread st = new SingleThread();
						lp = (LocalProducts)st.find(LocalProducts.class, id, service);
						showSubWindow(lp, lp.getShop());
						
						SearchService searchService = ((BloomNetAdminApplication)getApplication()).getBean("SearchService", SearchService.class);
						
						Set<LocalProducts> localProducts = lp.getShop().getLocalProducts();
						for(LocalProducts localProduct : localProducts) {
							st.index(LocalProducts.class, localProduct.getId(), searchService);
						}
						st.index(Shop.class, lp.getShop().getId(), searchService);
						
					} catch (LookupException e) {
						e.printStackTrace();
					}
				}else{
					getApplication().getMainWindow().showNotification("No products selected to edit.", Notification.POSITION_CENTERED);
				}
			}
		});
		remove = new Button("Remove");
		remove.addListener(new Button.ClickListener() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -9007073653372295611L;

			@SuppressWarnings("unchecked")
			public void buttonClick(Button.ClickEvent event) {
				if (((Set<LocalProducts>)productsTable.getValue()).toArray().length > 0 ) {
					try {
						Set<LocalProducts> selectedProducts = (Set<LocalProducts>) productsTable.getValue();
						SingleThread st = new SingleThread();
						CrudService service = ((BloomNetAdminApplication)getApplication()).getBean("CrudService", CrudService.class);
						SearchService searchService = ((BloomNetAdminApplication)getApplication()).getBean("SearchService", SearchService.class);
						List<Object> findList = new ArrayList<Object>();
						Object[] productArray = (Object[])selectedProducts.toArray();
						for(int ii=0; ii<selectedProducts.toArray().length; ++ii){
							findList.add(productArray[ii]);
						}
						List<Object> resultsList = (List<Object>) st.findMultiple(LocalProducts.class, findList, service);
						st.removeMultiple(resultsList, service);
						for(int ii=0; ii<resultsList.size(); ++ii){
							LocalProducts localProduct = (LocalProducts)resultsList.get(ii);
							productsTable.removeItem(localProduct.getId());
							parentShop.getLocalProducts().remove(localProduct);
						}
						st.index(Shop.class, parentShop.getId(), searchService);
					
					} catch (LookupException e) {
						e.printStackTrace();
					}
				}else{
					getApplication().getMainWindow().showNotification("No products selected to remove.", Notification.POSITION_CENTERED);
				}
			}
		});
		
		uploadLP = new Button("Upload Local Products");
		uploadLP.addListener(new Button.ClickListener() {

			private static final long serialVersionUID = -7154151668521563177L;

			public void buttonClick(ClickEvent event) {
				uploadLP.setEnabled(false);
				try {
					MediaUploadWindowLocalProducts mediaUploadWindow = new MediaUploadWindowLocalProducts();
					getApplication().getMainWindow().addWindow(mediaUploadWindow);
					uploadLP.setEnabled(true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		HorizontalLayout footer = new HorizontalLayout();
		footer.addComponent(add);
		footer.addComponent(edit);
		footer.addComponent(remove);
		footer.addComponent(uploadLP);
		addComponent(footer);
		
	}
	public void attach() {
		super.attach();
	}
	protected void showSubWindow(LocalProducts localProduct, Shop shop){
		LocalProductSubWindow subWindow = new LocalProductSubWindow();
		subWindow.setModal(true);
		subWindow.setWidth("400px");
		subWindow.setHeight("750px");
		LocalProductsForm productsForm = new LocalProductsForm(productsTable, (BloomNetAdminApplication)getApplication(), shop);
		productsForm.setParentSubWindow(subWindow);
		subWindow.addComponent(productsForm);
		getApplication().getMainWindow().addWindow(subWindow);
		if (localProduct == null){
			productsForm.addButtonClickHandler();
		}else{
			productsForm.setObjectToForm(localProduct);
			productsForm.setReadOnly(false);
			productsForm.handleButtons(false);
		}	}
	
	
	public void setShop(Shop shop) {
		parentShop = shop;
		productsTable.removeAllItems();
		for(LocalProducts localProduct : shop.getLocalProducts()) {
			productsTable.addItemToContainer(localProduct);
		}
	}
}
