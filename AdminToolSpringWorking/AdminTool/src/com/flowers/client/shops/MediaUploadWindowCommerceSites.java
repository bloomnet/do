package com.flowers.client.shops;

import java.io.FileInputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.PersistenceException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import com.flowers.client.BloomNetAdminApplication;
import com.flowers.server.LookupException;
import com.flowers.server.entity.FacilityListing;
import com.flowers.server.entity.Listing;
import com.flowers.server.entity.Shop;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.impl.SingleThread;
import com.flowers.server.util.SQLData;
import com.flowers.server.util.SizeCountingReceiver;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.ProgressIndicator;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FinishedEvent;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;


public class MediaUploadWindowCommerceSites extends Window {
	private static final long serialVersionUID = -8842699564789612163L;
	private SizeCountingReceiver counter;
	private Upload upload;
	private Label state = new Label();
    private Label result = new Label();
    private Label fileName = new Label();
    private Label textualProgress = new Label();
    
    private SQLData mySQL = null;
    
    private List<Long> shopsToIndex;
    private List<Long> listingsToIndex;
    private List<Long> facilityListingsToIndex;
    
    private ProgressIndicator pi = new ProgressIndicator();

    private String uploadPath = "/opt/data/import";
	
	public MediaUploadWindowCommerceSites() {
		center();
		VerticalLayout layout = (VerticalLayout) getContent();
        layout.setMargin(true);
        layout.setSpacing(true); 
        layout.setWidth("350px");
        layout.setHeight("300px");
        
        counter = new SizeCountingReceiver();
        upload = new Upload("", counter);
        addComponent(new Label("Upload a file to your collection."));
        
        upload.setImmediate(true);
        upload.setButtonCaption("Upload File");
        addComponent(upload);

        final Button cancelProcessing = new Button("Cancel");
        cancelProcessing.addListener(new Button.ClickListener() {
        	private static final long serialVersionUID = 1L;

			public void buttonClick(ClickEvent event) {
        		upload.interruptUpload();
        	}
        });
        cancelProcessing.setVisible(false);
        cancelProcessing.setStyleName("small");

        Panel p = new Panel("Status");
        p.setSizeUndefined();
        p.setWidth(300, HorizontalLayout.UNITS_PIXELS);
        FormLayout l = new FormLayout();
        l.setMargin(true);
        p.setContent(l);
        HorizontalLayout stateLayout = new HorizontalLayout();
        stateLayout.setSpacing(true);
        stateLayout.addComponent(state);
        stateLayout.addComponent(cancelProcessing);
        stateLayout.setCaption("Current state");
        state.setValue("Idle");
        l.addComponent(stateLayout);
        fileName.setCaption("File name");
        l.addComponent(fileName);
        result.setCaption("bytes");
        l.addComponent(result);
        pi.setCaption("Progress");
        pi.setVisible(false);
        l.addComponent(pi);
        textualProgress.setVisible(false);
        l.addComponent(textualProgress);

        addComponent(p);

        upload.addListener(new Upload.StartedListener() {
        	private static final long serialVersionUID = 1084163386895143768L;

			public void uploadStarted(StartedEvent event) {
        		// this method gets called immediatedly after upload is
        		// started
        		pi.setValue(0f);
        		pi.setVisible(true);
        		pi.setPollingInterval(500); // hit server frequently to get
        		textualProgress.setVisible(true);
        		// updates to client
        		state.setValue("Uploading");
        		fileName.setValue(event.getFilename());

        		cancelProcessing.setVisible(true);
        	}
        });

        upload.addListener(new Upload.ProgressListener() {
        	private static final long serialVersionUID = 3318393640870913379L;

			public void updateProgress(long readBytes, long contentLength) {
        		// this method gets called several times during the update
        		pi.setValue(readBytes / (float) contentLength);
        		textualProgress.setValue("Processed " + readBytes + " bytes of " + contentLength);
        		result.setValue(counter.getTotal());
        	}

        });
        upload.addListener(new Upload.SucceededListener() {
        	private static final long serialVersionUID = -4135230597799242206L;

			public void uploadSucceeded(SucceededEvent event) {
        		result.setValue(counter.getTotal() + " (total)");
        	}
        });
        upload.addListener(new Upload.FailedListener() {
        	private static final long serialVersionUID = -8754773108937834980L;

			public void uploadFailed(FailedEvent event) {
        		result.setValue(counter.getTotal()
        				+ " (counting interrupted at "
        				+ Math.round(100 * (Float) pi.getValue()) + "%)");
        	}
        });
        upload.addListener(new Upload.FinishedListener() {
        	private static final long serialVersionUID = -2421647485010631615L;

			public void uploadFinished(FinishedEvent event) {
        		state.setValue("Idle");
        		pi.setVisible(false);
        		textualProgress.setVisible(false);
        		cancelProcessing.setVisible(false);
        		doImport(event.getFilename());
        	}
        });
	}
	protected void doImport(String filename) {
		try {
			
			mySQL = new SQLData();
			shopsToIndex = new ArrayList<Long>();
			listingsToIndex = new ArrayList<Long>();
			facilityListingsToIndex = new ArrayList<Long>();
			
			String query = "SELECT shop_id, shop_code FROM shop WHERE website IS NOT NULL;";
			ResultSet results = mySQL.executeQuery(query);
			String shopId = "";
			String shopCode = "";
			while(results.next()) {
				shopId = results.getString("shop_id");
				shopCode = results.getString("shop_code");
				shopsToIndex.add(Long.valueOf(shopId));
				query = "SELECT id FROM listing WHERE shop_code=\"" + shopCode + "\";";
				ResultSet results2 = mySQL.executeQuery(query);
				String listingId = "";
				while(results2.next()) {
					listingId = results2.getString("id");
					listingsToIndex.add(Long.valueOf(listingId));
				}
				results2.close();
				mySQL.closeStatement();
				
				query = "SELECT id FROM facility_listing WHERE shop_code=\"" + shopCode + "\";";
				results2 = mySQL.executeQuery(query);
				listingId = "";
				while(results2.next()) {
					listingId = results2.getString("id");
					facilityListingsToIndex.add(Long.valueOf(listingId));
				}
				results2.close();
				mySQL.closeStatement();
				
			}
			mySQL.closeStatement();
			
			String statement = "UPDATE shop SET website = NULL WHERE website IS NOT NULL AND third_party_site != 1;";
			mySQL.executeStatement(statement);
			mySQL.closeStatement();
			
			HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(uploadPath+"/"+filename));
			HSSFSheet sheet = workbook.getSheet("Sheet1");
			Iterator<Row> tabRowIterator = sheet.rowIterator();
			boolean isFirst = true;
			int lineNumber = 0;
			System.out.println("Processing "+(sheet.getPhysicalNumberOfRows()-1)+" Shops");
			while(tabRowIterator.hasNext()){
				lineNumber++;
				HSSFRow row = (HSSFRow)tabRowIterator.next();
				if (isFirst){
					isFirst = false;
					continue;
				}
				if (!isRowValid(row)){
					System.out.println("InvalidRow "+lineNumber);
					continue;
				}
				try {
					updateShop(row);
					if(lineNumber % 50 == 0) System.out.println(lineNumber+" processed");					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			
			st.index(Shop.class,shopsToIndex,service);
			st.index(Listing.class, listingsToIndex, service);
			st.index(FacilityListing.class, facilityListingsToIndex, service);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	protected boolean isRowValid(HSSFRow row){
		Iterator<Cell> cellIterator = row.cellIterator();
		List<Boolean> validators = new ArrayList<Boolean>();
		while (cellIterator.hasNext()){
			Cell cell = cellIterator.next();
			validators.add(!isCellContentNull(cell));
		}
		for (Boolean b:validators){
			if (b.booleanValue()){
				return true;
			}
		}
		return false;
	}
	protected boolean isCellContentNull(Cell cell){
		if (cell == null){
			return true;
		}
		switch ( cell.getCellType() ) {
        case HSSFCell.CELL_TYPE_BLANK:
        	return true;
        case HSSFCell.CELL_TYPE_STRING:
        	return (cell.getStringCellValue() ==null) || (cell.getStringCellValue().trim().length() == 0);
        default:
        	return true;
		}
	}
	public void attach() {
		
	}
	
	public void updateShop(HSSFRow row) throws PersistenceException, LookupException, SQLException {
		
		String shopCode = getCellValue(row, 0);
		String shopWebsite = getCellValue(row, 1);
		
		if(shopCode != null) {
						
			String shopId = "";
			
			String query = "SELECT shop_id from shop s where s.shop_code=\""+shopCode+"\";";
			ResultSet results = mySQL.executeQuery(query);
			if(results.next()){
				shopId = results.getString("shop_id");
			}
			mySQL.closeStatement();
			
			if(!shopId.equals("")) {
				shopsToIndex.add(Long.valueOf(shopId));
				query = "SELECT id FROM listing WHERE shop_code=\"" + shopCode + "\";";
				ResultSet results2 = mySQL.executeQuery(query);
				String listingId = "";
				while(results2.next()) {
					listingId = results2.getString("id");
					listingsToIndex.add(Long.valueOf(listingId));
				}
				results2.close();
				mySQL.closeStatement();
				
				String statement = "UPDATE shop SET website = \""+shopWebsite+"\", third_party_site = 0 WHERE shop_id = "+shopId+";";
				mySQL.executeStatement(statement);
				mySQL.closeStatement();
			}
		
			
		}
	
	}
	
	protected String getCellValue(HSSFRow row, int index) {
		Cell cell = row.getCell(index);
		if (cell == null) return "";
		cell.setCellType(HSSFCell.CELL_TYPE_STRING);
		String value = cell.getStringCellValue().trim();
		if(value == null || value.length() == 0) return "";
		return value;
	}
	
	
	protected String getId(){
		String query = "SELECT LAST_INSERT_ID()";
		ResultSet results = mySQL.executeQuery(query);
		String id = "";
		try {
			results.next();
			id = results.getString("LAST_INSERT_ID()");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		mySQL.closeStatement();
		return id;
	}
}

