package com.flowers.client.shops;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.server.LookupException;
import com.flowers.server.entity.FacilityListing;
import com.flowers.server.entity.Shop;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.Notification;

public class FacilityListingPanel extends VerticalLayout {
	private static final long serialVersionUID = 8310272037766774853L;
	private FacilityListingTable facilityListingTable;
	private Shop parentShop;
	private Button add;
	private Button edit;
	private Button remove;
	private Button showAd;
	
	public FacilityListingPanel() {
		facilityListingTable = new FacilityListingTable();
		addComponent(facilityListingTable);
		add = new Button("Add");
		add.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = -4094808531574784520L;
			@Override
			public void buttonClick(Button.ClickEvent event) {
				if(parentShop == null || parentShop.getShopCode() == null) getApplication().getMainWindow().showNotification("No Shop Selected.", Notification.POSITION_CENTERED);
				else showSubWindow(null, parentShop);
			}
		});
		edit = new Button("Edit");
		edit.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = -1177941627607832172L;
			@SuppressWarnings("unchecked")
			public void buttonClick(Button.ClickEvent event) {
				if (((Set<FacilityListing>)facilityListingTable.getValue()).toArray().length > 0){
					FacilityListing l = null;
					try {
						Set<FacilityListing> selectedListings = (Set<FacilityListing>) facilityListingTable.getValue();
						Long id = (Long)selectedListings.toArray()[0];
						CrudService service = ((BloomNetAdminApplication)getApplication()).getBean("CrudService", CrudService.class);
						SingleThread st = new SingleThread();
						l = (FacilityListing)st.find(FacilityListing.class, id, service);
						showSubWindow(l, l.getShop());
												
					} catch (LookupException e) {
						e.printStackTrace();
					}
				}else{
					getApplication().getMainWindow().showNotification("No Listing selected to edit.", Notification.POSITION_CENTERED);
				}
			}
		});
		remove = new Button("Remove");
		remove.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = -2772357972276733911L;
			@SuppressWarnings("unchecked")
			public void buttonClick(Button.ClickEvent event) {
				if (((Set<FacilityListing>)facilityListingTable.getValue()).toArray().length > 0 ) {
					try {
						Set<FacilityListing> selectedListings = (Set<FacilityListing>) facilityListingTable.getValue();
						SingleThread st = new SingleThread();
						CrudService service = ((BloomNetAdminApplication)getApplication()).getBean("CrudService", CrudService.class);
						SearchService searchService = ((BloomNetAdminApplication)getApplication()).getBean("SearchService", SearchService.class);
						List<Object> findList = new ArrayList<Object>();
						Object[] listingArray = (Object[])selectedListings.toArray();
						for(int ii=0; ii<selectedListings.toArray().length; ++ii){
							findList.add(listingArray[ii]);
						}
						List<Object> resultsList = (List<Object>) st.findMultiple(FacilityListing.class, findList, service);
						st.removeMultiple(resultsList, service);
						for(int ii=0; ii<resultsList.size(); ++ii){
							FacilityListing listing = (FacilityListing)resultsList.get(ii);
							facilityListingTable.removeItem(listing.getId());
							parentShop.getListings().remove(listing);
						}
						st.index(Shop.class, parentShop.getId(), searchService);
					
					} catch (LookupException e) {
						e.printStackTrace();
					}
				}else{
					getApplication().getMainWindow().showNotification("No Listing selected to remove.", Notification.POSITION_CENTERED);
				}
			}
		});
		showAd = new Button("Show Ad");
		showAd.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = -1177941627607832172L;
			@SuppressWarnings("unchecked")
			public void buttonClick(Button.ClickEvent event) {
				if (((Set<FacilityListing>)facilityListingTable.getValue()).toArray().length > 0){
					try {
						Set<FacilityListing> selectedListings = (Set<FacilityListing>) facilityListingTable.getValue();
						Long id = (Long)selectedListings.toArray()[0];
						CrudService service = ((BloomNetAdminApplication)getApplication()).getBean("CrudService", CrudService.class);
						SingleThread st = new SingleThread();
						FacilityListing facilityListing = (FacilityListing)st.find(FacilityListing.class, id, service);
						if(facilityListing.getAdSize() != null)
							showSubWindow(facilityListing, facilityListing.getShop(), facilityListing.getShop().getShopCode());
						else
							getApplication().getMainWindow().showNotification("There is no ad to display for this listing", Notification.POSITION_CENTERED);
					} catch (LookupException e) {
						e.printStackTrace();
					}
				}else{
					getApplication().getMainWindow().showNotification("No Listing selected to show ad.", Notification.POSITION_CENTERED);
				}
			}
		});
		HorizontalLayout footer = new HorizontalLayout();
		footer.addComponent(add);
		footer.addComponent(edit);
		footer.addComponent(remove);
		footer.addComponent(showAd);
		addComponent(footer);
		
	}
	public void attach() {
		super.attach();
	}
	protected void showSubWindow(FacilityListing facilityListing, Shop shop){
		ListingSubWindow subWindow = new ListingSubWindow();
		subWindow.setModal(true);
		subWindow.setWidth("600px");
		subWindow.setHeight("800px");
		FacilityListingForm facilityListingForm = new FacilityListingForm(facilityListingTable, (BloomNetAdminApplication)getApplication(), shop);
		facilityListingForm.setParentSubWindow(subWindow);
		subWindow.addComponent(facilityListingForm);
		getApplication().getMainWindow().addWindow(subWindow);
		if (facilityListing == null){
			facilityListingForm.addButtonClickHandler();
		}else{
			facilityListingForm.setObjectToForm(facilityListing);
			facilityListingForm.setReadOnly(false);
			facilityListingForm.handleButtons(false);
		}
		if(facilityListing != null && facilityListing.getCity() != null) facilityListingForm.getCity().setValue(facilityListing.getCity().getName() + ", " + facilityListing.getCity().getState().getShortName());
	}
	
	protected void showSubWindow(FacilityListing listing, Shop shop, String shopCode){
		FacilityListingSubWindow subWindow = new FacilityListingSubWindow();
		subWindow.setModal(true);
		subWindow.setWidth("300px");
		subWindow.setHeight("500px");
		URL url = null;
		try {
			url = new URL("http://directory.bloomnet.net/bloomnet-images/ads/"+shopCode+"-"+listing.getEntryCode()+listing.getAdSize()+".png");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		Embedded image = new Embedded("", new ExternalResource(url));
		image.setType(Embedded.TYPE_IMAGE);
		subWindow.addComponent(image);
		image.setSizeFull();
		getApplication().getMainWindow().addWindow(subWindow);
	}
	
	public void setShop(Shop shop) {
		parentShop = shop;
		facilityListingTable.removeAllItems();
		for(FacilityListing facilityListing : shop.getFacilityListings()) {
			facilityListingTable.addItemToContainer(facilityListing);
		}
	}
}
