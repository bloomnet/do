package com.flowers.client.shops;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.PersistenceException;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.ui.base.BaseForm;
import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.LocalProducts;
import com.flowers.server.entity.Shop;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.Notification;

public class LocalProductsForm extends BaseForm<LocalProducts> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5444323477505659386L;
	private Shop parentShop;
	private Window parentSubWindow;
	private GridLayout layout;

	
	public static final String[] visibleFieldOrder = new String[]{"productName", "price", "pictureFile"};
	public LocalProductsForm(TableContainer<LocalProducts> tableContainer, BloomNetAdminApplication bloomNetApp, Shop parentShop) {
		super(tableContainer, visibleFieldOrder);
		layout = new GridLayout(2, 4);
		layout.setMargin(true, false, false, true);
        layout.setSpacing(true);
        setLayout(layout);
		this.parentShop = parentShop;
	}
		
	public void attachField(Object propertyId, Field field) {
		if (propertyId.equals("productName")) 
			layout.addComponent(field, 0, 0);
		else if (propertyId.equals("price")) 
			layout.addComponent(field, 0, 1);
		else if (propertyId.equals("pictureFile")) 
			layout.addComponent(field, 0, 2);
	}
	
	public void setParentSubWindow(Window parentSubWindow) {
		this.parentSubWindow = parentSubWindow;
	}
	
	@Override
	public void cancelButtonClickHandler() {
		discard();
		if (parentSubWindow != null && (parentSubWindow instanceof LocalProductSubWindow)){
			((LocalProductSubWindow)parentSubWindow).close();
		}
	}
	
	@Override
	public void saveButtonClickHandler() {
		if(parentShop == null){
			getApplication().getMainWindow().showNotification("No Shop Selected.", Notification.POSITION_CENTERED);
	    }else{
	    	super.saveButtonClickHandler();
			if (parentSubWindow != null && (parentSubWindow instanceof LocalProductSubWindow)){
				((LocalProductSubWindow)parentSubWindow).close();
			}	    }
	}
	
	@Override
	public LocalProducts getBlankObject() {
		return new LocalProducts();
	}

	@Override
	public LocalProducts insert(LocalProducts t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SearchService searchService =app.getBean("SearchService", SearchService.class);
		SingleThread st = new SingleThread();
		try {
			t.setShop(parentShop);
		} catch (Exception e) {
			e.printStackTrace();
		}
		st.persist(t, service);
		parentShop.getLocalProducts().add(t);
		try{
			st.index(t.getShop().getClass(), t.getShop().getId(), searchService);
			List<Long> indexList = new ArrayList<Long>();
			for(LocalProducts product : t.getShop().getLocalProducts()) {
				indexList.add(product.getId());
			}
			st.index(LocalProducts.class, indexList, searchService);
		}catch(Exception ee){
			ee.printStackTrace();
		}
		return t;
	}

	@Override
	public LocalProducts update(LocalProducts t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SearchService searchService =app.getBean("SearchService", SearchService.class);
		SingleThread st = new SingleThread();
		st.merge(t, service);
		try{
			st.index(t.getShop().getClass(), t.getShop().getId(), searchService);
			List<Long> indexList = new ArrayList<Long>();
			for(LocalProducts product : t.getShop().getLocalProducts()) {
				indexList.add(product.getId());
			}
			st.index(LocalProducts.class, indexList, searchService);
		}catch(Exception ee){
			ee.printStackTrace();
		}
		return t;
	}

	@Override
	public Object remove(LocalProducts t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SearchService searchService = app.getBean("SearchService", SearchService.class);
		SingleThread st = new SingleThread();
		st.remove(t, service);
		Set<LocalProducts> products = t.getShop().getLocalProducts();
		List<Long> indexList = new ArrayList<Long>();
		for(LocalProducts product : products) {
			indexList.add(product.getId());
		}
		st.index(LocalProducts.class, indexList, searchService);
		st.index(Shop.class, t.getShop().getId(), searchService);
		return t.getId();
	}
	
	@Override
	public FormFieldFactory getFormFieldFactory() {
		return new LocalProductsFieldFactory();
	}
	
	public class LocalProductsFieldFactory extends DefaultFieldFactory{
		/**
		 * 
		 */
		private static final long serialVersionUID = -1648979460435041355L;

		public Field createField(Item item, Object propertyId, Component uiContext) {
			Field field = super.createField(item, propertyId, uiContext);
			if(field instanceof TextField){
				TextField tf = (TextField)field;
				tf.setNullRepresentation("");
			}
				
			return field;
		}
	}
	
}
