package com.flowers.client.shops;
import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;
import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.minimumprice.MinimumPricePanel;
import com.flowers.client.products.CodifiedProductPanel;
import com.flowers.client.socialmedia.SocialMediaPanel;
import com.flowers.client.ziplistings.ZipListingPanel;
import com.flowers.server.entity.Shop;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.flowers.server.util.CreateDB;
import com.flowers.server.util.ParseDirectory;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.terminal.FileResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Window.Notification;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.flowers.client.address.AddressNestedForm;


public class ShopPanel extends VerticalLayout {
	private static final long serialVersionUID = 1109959475307724212L;
	private ShopTable shopTable;
	private ShopForm shopForm;
	private ListingPanel listings;
	private FacilityListingPanel facilityListings;
	private FacilityPanel facilities;
	private MinimumPricePanel minimumPrice;
	private TextField shopCode;
	private Button searchButton;
	private Button exportButton;
	private Button sendDBDumpButton;
	private Button uploadDisableShopsButton;
	private Button uploadCommerceSitesButton;
	private Button sendNotInDOButton;
	private CodifiedProductPanel codifiedProduct;
	private SocialMediaPanel socialMedia;
	private ZipListingPanel zipListings;
	private LocalProductsPanel localProducts;
	
	public HorizontalLayout getShopSearch() {
		final HorizontalLayout shopSearchLayout = new HorizontalLayout();
		shopSearchLayout.setSpacing(true);
		shopSearchLayout.setHeight("40px");
		
		shopCode = new TextField("Shop Code");
		shopSearchLayout.addComponent(shopCode);
		shopSearchLayout.setComponentAlignment(shopCode, Alignment.MIDDLE_LEFT);
		
		searchButton = new Button("Search");
		shopSearchLayout.addComponent(searchButton);
		shopSearchLayout.setComponentAlignment(searchButton, Alignment.BOTTOM_LEFT);
		searchButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				Shop shop = new Shop();
				shopForm.setObjectToForm(shop);
				shopForm.editButton.setVisible(false);
				shopForm.removeButton.setVisible(false);
				socialMedia.setShop(shop);
				zipListings.setShop(shop);
				localProducts.setShop(shop);
				listings.setShop(shop);
				facilities.setShop(shop);
				minimumPrice.setShop(shop);
				codifiedProduct.setShop(shop);
				search();
			}
		});
				
		uploadDisableShopsButton = new Button("Disable Shops");
		uploadDisableShopsButton.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = 1L;

			public void buttonClick(ClickEvent event) {
				uploadDisableShopsButton.setEnabled(false);
				try {
					MediaUploadWindowDisableShops mediaUploadWindow = new MediaUploadWindowDisableShops();
					getApplication().getMainWindow().addWindow(mediaUploadWindow);
					uploadDisableShopsButton.setEnabled(true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		shopSearchLayout.addComponent(uploadDisableShopsButton);
		shopSearchLayout.setComponentAlignment(uploadDisableShopsButton, Alignment.BOTTOM_LEFT);
		
		uploadCommerceSitesButton = new Button("Upload Sites");
		uploadCommerceSitesButton.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = 1L;

			public void buttonClick(ClickEvent event) {
				uploadCommerceSitesButton.setEnabled(false);
				try {
					MediaUploadWindowCommerceSites mediaUploadWindow = new MediaUploadWindowCommerceSites();
					getApplication().getMainWindow().addWindow(mediaUploadWindow);
					uploadCommerceSitesButton.setEnabled(true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		shopSearchLayout.addComponent(uploadCommerceSitesButton);
		shopSearchLayout.setComponentAlignment(uploadCommerceSitesButton, Alignment.BOTTOM_LEFT);
			
				
		Button indexButton = new Button("Index Shops");
		shopSearchLayout.addComponent(indexButton);
		shopSearchLayout.setComponentAlignment(indexButton, Alignment.BOTTOM_LEFT);
		indexButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
				SearchService searchService = app.getBean("SearchService", SearchService.class);
				try {
					SingleThread st = new SingleThread();
					st.index(Shop.class, searchService, true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		exportButton = new Button("Export Product Counts");
		exportButton.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = 4601820294909274594L;

			@Override
			public void buttonClick(ClickEvent event) {
				
				String date = String.valueOf(new Date().getTime());
				File file = new File("/var/lib/tomcat/webapps/shopData/"+date+".csv");
				
				new WriteShopProductCounts(file);
	
				FileResource fileResource = new FileResource(file, (BloomNetAdminApplication)getApplication());						
				getWindow().open(fileResource);
				
			}

		});
		
		shopSearchLayout.addComponent(exportButton);
		shopSearchLayout.setComponentAlignment(exportButton, Alignment.BOTTOM_LEFT);
	
		
		sendDBDumpButton = new Button("Get DB Dump");
		shopSearchLayout.addComponent(sendDBDumpButton);
		shopSearchLayout.setComponentAlignment(sendDBDumpButton, Alignment.BOTTOM_LEFT);
		sendDBDumpButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {		
				File file = new File("/CreateExcelFromDB/data/directory-onlineDB.xls");
				new CreateDB(file);
				FileResource fileResource = new FileResource(file, (BloomNetAdminApplication)getApplication());						
				getWindow().open(fileResource);			
				
			}
			
		});
		
		sendNotInDOButton = new Button("Get Listings Not In DO");
		shopSearchLayout.addComponent(sendNotInDOButton);
		shopSearchLayout.setComponentAlignment(sendNotInDOButton, Alignment.BOTTOM_LEFT);
		sendNotInDOButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				File file = new File("/GetListingsNotInDO/data/results.csv");
				new ParseDirectory(file);
				FileResource fileResource = new FileResource(file, (BloomNetAdminApplication)getApplication());						
				getWindow().open(fileResource);	
			}
			
		});
		
		return shopSearchLayout;
	}

	@SuppressWarnings("unchecked")
	protected void searchOld(){
		try {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			List<Object> shops = null;
			if(shopCode.getValue() != null && shopCode.getValue().toString().length() >0) {
				StringBuilder pql = new StringBuilder("from Shop s where s.shopCode=:shopCode");
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("shopCode", shopCode.getValue());
				shops = (List<Object>) st.findResults(pql.toString(), service, params);
			} else{
				shops = (List<Object>) st.findResults("from Shop", service, new HashMap<String, Object>());
			}
			if(shops != null) {
				shopTable.removeAllItems();
				for(Object shopObj : shops) {
					Shop shop = (Shop)shopObj;
					shopTable.addItemToContainer(shop);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected void search(){
		try {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			SearchService searchService = app.getBean("SearchService", SearchService.class);
			List<Document> shops = null;
			SortField sortField = new SortField("shop_name", SortField.Type.STRING);
			if(shopCode.getValue() != null && shopCode.getValue().toString().length() >0) {
				TermQuery query = new TermQuery(new Term("tokenizedShopCode", shopCode.getValue().toString()));
				 shops = searchService.documents(Shop.class, query, sortField);
			} else{
				shops = searchService.documents(Shop.class, new MatchAllDocsQuery(), sortField);
			}
			if(shops != null) {
				shopTable.removeAllItems();
				for(Document d : shops) {
					shopTable.addDocumentToContainer(d);
				}
			}else {
				getApplication().getMainWindow().showNotification("No Shops Were Found.", Notification.POSITION_CENTERED);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public ShopPanel() {
		setSizeFull();
		setSpacing(true);
		addComponent(getShopSearch());
				
		shopTable = new ShopTable();
		addComponent(shopTable);
		shopTable.addListener(new ValueChangeListener() {
            private static final long serialVersionUID = 2537403500201812771L;
			public void valueChange(ValueChangeEvent event) {
				Long id = (Long)shopTable.getValue();
				try {
					BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
					CrudService service = app.getBean("CrudService", CrudService.class);
					SingleThread st = new SingleThread();
					if (id != null){
						
						boolean refreshEntityManager = true;
						Shop obj = (Shop)st.find(Shop.class, id, service, refreshEntityManager);
						shopForm.setObjectToForm(obj);
						socialMedia.setShop(obj);
						zipListings.setShop(obj);
						localProducts.setShop(obj);
						listings.setShop(obj);
						facilityListings.setShop(obj);
						facilities.setShop(obj);
						minimumPrice.setShop(obj);
						codifiedProduct.setShop(obj);
						if(obj != null && obj.getAddress().getCity() != null) {
							AddressNestedForm nestedForm = (AddressNestedForm)shopForm.getNestedForm("addressNestedForm"); 
							com.flowers.server.entity.City c = obj.getAddress().getCity();
							nestedForm.getCity().addItem(c);
							nestedForm.getCity().setItemCaption(c, c.getName() + ", " + obj.getAddress().getState().getShortName());
						}
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
            }
        });
		TabSheet tabs = new TabSheet();
		addComponent(tabs);
		tabs.setSizeFull();
		shopForm = new ShopForm(shopTable);
		tabs.addTab(shopForm, "Detail", null);
		shopForm.setSizeFull();
		listings = new ListingPanel();
		tabs.addTab(listings, "Listings", null);
		facilityListings = new FacilityListingPanel();
		tabs.addTab(facilityListings, "Facility Listings", null);
		facilities = new FacilityPanel();
		tabs.addTab(facilities, "Facilities", null);
		minimumPrice = new MinimumPricePanel();
		tabs.addTab(minimumPrice, "Minimum Price", null);
		codifiedProduct = new CodifiedProductPanel();
		tabs.addTab(codifiedProduct, "Codified Products", null);
		socialMedia = new SocialMediaPanel();
		tabs.addTab(socialMedia, "Social Media", null);
		zipListings = new ZipListingPanel();
		tabs.addTab(zipListings, "Zip Listings", null);
		localProducts = new LocalProductsPanel();
		tabs.addTab(localProducts, "Local Products", null);
	}
}
