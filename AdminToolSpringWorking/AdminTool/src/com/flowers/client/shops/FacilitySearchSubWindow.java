package com.flowers.client.shops;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.facilities.FacilityTable;
import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.LookupException;
import com.flowers.server.entity.Facility;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.ShopFacilityXref;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class FacilitySearchSubWindow extends Window {
	private static final long serialVersionUID = 880068930494756183L;
	private HorizontalLayout header;
	private TextField searchField;
	private TextField searchFieldCity;
	private Button searchButton;
	private Button searchButtonCity;
	private Button addButton;
	private FacilityTable facilityTable;
	private HorizontalLayout footer;
	private TableContainer<ShopFacilityXref> parentFacilityTable;
	private Shop parentShop;
	
	public FacilitySearchSubWindow(TableContainer<ShopFacilityXref> parentFacilityTable, Shop parentShop) {
		super("Facility Search", new VerticalLayout());
		this.parentFacilityTable = parentFacilityTable;
		this.parentShop = parentShop;
		this.addComponent(getHeader());
		facilityTable = new FacilityTable();
		facilityTable.setMultiSelect(true);
		this.addComponent(facilityTable);
		this.addComponent(getFooter());
		this.setModal(true);
		VerticalLayout layout = (VerticalLayout) getContent();
		layout.setWidth("685px");
		layout.setHeight("300px");
		super.setWidth(700, UNITS_PIXELS);
	}
	
	public HorizontalLayout getFooter() {
		if (footer == null){
			footer = new HorizontalLayout();
			addButton = new Button("Add Facility");
			addButton.addListener(new Button.ClickListener() {
				private static final long serialVersionUID = -6028966999160490414L;

				@SuppressWarnings("unchecked")
				@Override
				public void buttonClick(ClickEvent event) {
					if (((Set<Facility>)facilityTable.getValue()).toArray().length > 0){
						
						BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
						CrudService service = app.getBean("CrudService", CrudService.class);
						SearchService searchService = app.getBean("SearchService", SearchService.class);
						
						Set<Facility> selectedFacilities = (Set<Facility>) facilityTable.getValue();
						for(int ii=0; ii<selectedFacilities.toArray().length; ++ii){
							Long id = (Long)selectedFacilities.toArray()[ii];
							if (!isDuplicated(id)){
								addFacility(id, service);
							}else{
								getApplication().getMainWindow().showNotification("Facility already related to shop.", Notification.POSITION_CENTERED);
							}
						}
						SingleThread st = new SingleThread();
						st.index(Shop.class, parentShop.getId(), searchService);
						closeWindow();
					}else{
						getApplication().getMainWindow().showNotification("No Facility selected to add.", Notification.POSITION_CENTERED);
					}
				}
			});
			footer.addComponent(addButton);
		}
		return footer;
	}
	
	protected void closeWindow() {
		this.close();
		
	}
	
	protected boolean isDuplicated(Long id){
		if (parentShop.getShopFacilityXrefs() != null){
			for (ShopFacilityXref xref : parentShop.getShopFacilityXrefs()){
				if (xref.getFacility().getId() == id.longValue()){
					return true;
				}
			}
		}
		return false;
	}
	
	protected void addFacility(Long id, CrudService service){
		try {
			SingleThread st = new SingleThread();
			Facility facility = (Facility)st.find(Facility.class, id, service);
			ShopFacilityXref xref = new ShopFacilityXref();
			xref.setFacility(facility);
			xref.setShop(parentShop);
			st.persist(xref, service);
			parentShop.getShopFacilityXrefs().add(xref);
			facility.getShopFacilityXrefs().add(xref);
			parentFacilityTable.addItemToContainer(xref);
			parentFacilityTable.selectRow(xref);
		} catch (LookupException e) {
			e.printStackTrace();
		}
	}
	
	public HorizontalLayout getHeader() {
		if (header == null){
			header = new HorizontalLayout();
			searchField = new TextField("Facility Name");
			searchButton = new Button("Search");
			searchButton.addListener(new Button.ClickListener() {

				private static final long serialVersionUID = 2563424845481437362L;

				@Override
				public void buttonClick(ClickEvent event) {
					search((String)searchField.getValue());
				}
			});
			searchFieldCity = new TextField("City Name");
			searchButtonCity = new Button("Search");
			searchButtonCity.addListener(new Button.ClickListener() {

				private static final long serialVersionUID = 7997116351686736469L;

				@Override
				public void buttonClick(ClickEvent event) {
					searchCity((String)searchFieldCity.getValue());
				}
			});
			header.setWidth(500, HorizontalLayout.UNITS_PIXELS);
			header.addComponent(searchField);
			header.addComponent(searchButton);
			header.addComponent(searchFieldCity);
			header.addComponent(searchButtonCity);
			header.setComponentAlignment(searchField, Alignment.BOTTOM_LEFT);
			header.setComponentAlignment(searchButton, Alignment.BOTTOM_LEFT);
			header.setComponentAlignment(searchFieldCity, Alignment.BOTTOM_LEFT);
			header.setComponentAlignment(searchButtonCity, Alignment.BOTTOM_LEFT);
		}
		return header;
	}
	
	protected void search(String facilityName){
		try {
			CrudService service = ((BloomNetAdminApplication)getApplication()).getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			String pql = "from Facility f where f.name like :name";
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("name", "%"+facilityName+"%");
			@SuppressWarnings("unchecked")
			List<Object> facilities = (List<Object>) st.findResults(pql, service, params);
			facilityTable.removeAllItems();
			if (facilities != null){
				for (Object o:facilities){
					facilityTable.addItemToContainer((Facility)o);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected void searchCity(String cityName){
		try {
			CrudService service = ((BloomNetAdminApplication)getApplication()).getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			String pql = "from Facility f where f.address.city.name like :name";
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("name", "%"+cityName+"%");
			@SuppressWarnings("unchecked")
			List<Object> facilities = (List<Object>) st.findResults(pql, service, params);
			facilityTable.removeAllItems();
			if (facilities != null){
				for (Object o:facilities){
					facilityTable.addItemToContainer((Facility)o);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
