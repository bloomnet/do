package com.flowers.client.shops;


import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.facilities.WriteFacilities;
import com.flowers.server.entity.Facility;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.ShopFacilityXref;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.terminal.FileResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.Notification;

public class FacilityPanel extends VerticalLayout {
	private static final long serialVersionUID = -5047954150611512065L;
	private Button addButton;
	private Button removeButton;
	private Button exportButton;
	private ShopFacilityXrefTable facilityTable ;
	private Shop parentShop;
	
	@SuppressWarnings("serial")
	public FacilityPanel() {
		setSizeFull();
		setSpacing(true);
		facilityTable = new ShopFacilityXrefTable(); 
		addComponent(facilityTable);
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		addButton = new Button("Add");
		addButton.addListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				if(parentShop == null || parentShop.getShopCode() == null){
					getApplication().getMainWindow().showNotification("No Shop Selected.", Notification.POSITION_CENTERED);
				}else{
					FacilitySearchSubWindow subWindow = new FacilitySearchSubWindow(facilityTable, parentShop);
					getApplication().getMainWindow().addWindow(subWindow);
					}
				}
			});
		removeButton = new Button("Remove");
		removeButton.addListener(new Button.ClickListener() {
			@SuppressWarnings("unchecked")
			@Override
			public void buttonClick(ClickEvent event) {
				if (((Set<Facility>)facilityTable.getValue()).toArray().length > 0){
					Set<Facility> selectedFacilities = (Set<Facility>) facilityTable.getValue();
					BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
					SearchService searchService = app.getBean("SearchService", SearchService.class);
					SingleThread st = new SingleThread();
					CrudService service = ((BloomNetAdminApplication)getApplication()).getBean("CrudService", CrudService.class);
					try {
						List<Object> findList = new ArrayList<Object>();
						Object[] facilityArray = (Object[])selectedFacilities.toArray();
						for(int ii=0; ii<selectedFacilities.toArray().length; ++ii){
							findList.add(facilityArray[ii]);
						}
						List<Object> resultsList = (List<Object>) st.findMultiple(ShopFacilityXref.class, findList, service);
						st.removeMultiple(resultsList, service);
						for(int ii=0; ii<resultsList.size(); ++ii){
							ShopFacilityXref xref = (ShopFacilityXref)resultsList.get(ii);
							facilityTable.removeItem(xref.getId());
							parentShop.getShopFacilityXrefs().remove(xref);
						}
						st.index(Shop.class, parentShop.getId(), searchService);
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}else{
					getApplication().getMainWindow().showNotification("No Facility selected to remove.", Notification.POSITION_CENTERED);
				}
			}
		});
		
		exportButton = new Button("Export Facilities");
		exportButton.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = 4601820294909274594L;

			@Override
			public void buttonClick(ClickEvent event) {
				
				if(parentShop == null || parentShop.getShopCode() == null){
					getApplication().getMainWindow().showNotification("No Shop Selected.", Notification.POSITION_CENTERED);
				}else{
					String date = String.valueOf(new Date().getTime());
					File file = new File("/var/lib/tomcat/webapps/facilityData/"+date+".txt");
					
					new WriteFacilities(parentShop.getShopCode(), file);

					FileResource fileResource = new FileResource(file, (BloomNetAdminApplication)getApplication());						
					getWindow().open(fileResource);
				}
				
			}

		});
		
		horizontalLayout.addComponent(addButton);
		horizontalLayout.addComponent(removeButton);
		horizontalLayout.addComponent(exportButton);
		addComponent(horizontalLayout);
		
	}

	
	public void setShop(Shop shop) {
		this.parentShop = shop;
		facilityTable.removeAllItems();
		for(ShopFacilityXref ref : shop.getShopFacilityXrefs()) {
			facilityTable.addItemToContainer(ref);
		}
	}
}
