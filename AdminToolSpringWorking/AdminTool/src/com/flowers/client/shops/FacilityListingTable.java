package com.flowers.client.shops;

import org.apache.lucene.document.Document;

import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.FacilityListing;
import com.vaadin.data.Item;
import com.vaadin.ui.Table;

public class FacilityListingTable extends Table implements TableContainer<FacilityListing> {
	private static final long serialVersionUID = 3352729349030827257L;

	
	public FacilityListingTable() {
		addContainerProperty("city", String.class, null);
		addContainerProperty("state", String.class, null);
		addContainerProperty("delivery fee", Double.class, null);
		addContainerProperty("minimum price", Double.class, null);
		addContainerProperty("listing entry", String.class, null);
		addContainerProperty("display ad", String.class, null);
		addContainerProperty("custom ad", String.class, null);
		addContainerProperty("bid", Double.class, null);
		addContainerProperty("side ad entry", String.class, null);
		addContainerProperty("ad start date", String.class, null);
		addContainerProperty("ad end date", String.class, null);
		addContainerProperty("banner start date", String.class, null);
		addContainerProperty("banner end date", String.class, null);
		addContainerProperty("listing end date", String.class, null);
		addContainerProperty("top ad entry number", String.class, null);
		addContainerProperty("top ad start date", String.class, null);
		addContainerProperty("top ad end date", String.class, null);
		addContainerProperty("more info", String.class, null);
		addContainerProperty("seasonal images", String.class, null);
		addContainerProperty("featured listing", String.class, null);
		addContainerProperty("preferred listing", String.class, null);
		setSizeFull();
		setSelectable(true);
		setMultiSelect(true);
		setImmediate(true);
		setNullSelectionAllowed(false);
		Object [] properties={"city"};
		boolean [] ordering={true};
		sort(properties, ordering);
	}
	
	@Override
	public Item addItemToContainer(FacilityListing t) {
		Item item = this.addItem(t.getId());
		if(item != null) {
			if(t.getCity() != null) {
				item.getItemProperty("city").setValue(t.getCity().getName());
				item.getItemProperty("state").setValue(t.getCity().getState().getName());
				item.getItemProperty("delivery fee").setValue(t.getDelivery());
				item.getItemProperty("minimum price").setValue(t.getMinOrder());
				item.getItemProperty("listing entry").setValue(t.getEntryCode());
				item.getItemProperty("display ad").setValue(t.getAdSize());
				item.getItemProperty("custom ad").setValue(t.getCustomListing());
				item.getItemProperty("bid").setValue(t.getBid());
				item.getItemProperty("side ad entry").setValue(t.getSideAdEntryNumber());
				item.getItemProperty("ad start date").setValue(t.getAdStartDate());
				item.getItemProperty("ad end date").setValue(t.getAdEndDate());
				item.getItemProperty("banner start date").setValue(t.getBannerStartDate());
				item.getItemProperty("banner end date").setValue(t.getBannerEndDate());
				item.getItemProperty("listing end date").setValue(t.getListingEndDate());
				item.getItemProperty("top ad entry number").setValue(t.getTopAdEntryNumber());
				item.getItemProperty("top ad start date").setValue(t.getTopAdStartDate());
				item.getItemProperty("top ad end date").setValue(t.getTopAdEndDate());
				item.getItemProperty("more info").setValue(t.getMoreInfo());
				item.getItemProperty("seasonal images").setValue(t.getSeasonalImages());
				item.getItemProperty("featured listing").setValue(t.getFeaturedListing());
				item.getItemProperty("preferred listing").setValue(t.getPreferredListing());

			}
		}
		return item;
	}
	
	@Override
	public void selectRow(FacilityListing t) {
		this.setValue(t.getId());
	}
	
	@Override
	public Item addDocumentToContainer(Document d) {
		Long id = Long.parseLong(d.get("id"));
		Item item = this.addItem(id);
		if(item != null) {
			item.getItemProperty("city").setValue(d.get("city"));
		}
		return item;
	}
}
