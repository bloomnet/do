package com.flowers.client.minimumprice;

import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class MinimumPriceSubWindow extends Window {
	private static final long serialVersionUID = 5682553948512489973L;

	public MinimumPriceSubWindow() {
		super("Minimum Price");
		VerticalLayout layout = (VerticalLayout) getContent();
        layout.setMargin(true);
        layout.setSpacing(true); 
        layout.setWidth("350px");
        layout.setHeight("200px");
		setModal(true);
	}
	
	@Override
	public void close() {
		super.close();
	}
}
