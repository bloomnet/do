package com.flowers.client.minimumprice;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.server.LookupException;
import com.flowers.server.entity.Listing;
import com.flowers.server.entity.MinimumPrice;
import com.flowers.server.entity.Shop;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.Notification;

public class MinimumPricePanel extends VerticalLayout {
	private static final long serialVersionUID = -6337298689195361915L;
	private MinimumPriceTable minimumPriceTable;
	private Shop parentShop;
	private Button add;
	private Button edit;
	private Button remove;
	private TextField minimumPrice;
	private Button updateAll;
	public MinimumPricePanel() {
		minimumPriceTable = new MinimumPriceTable();
		addComponent(minimumPriceTable);
		add = new Button("Add");
		add.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = 8744032684657430261L;

			@Override
			public void buttonClick(Button.ClickEvent event) {
				if(parentShop == null || parentShop.getShopCode() == null) getApplication().getMainWindow().showNotification("No Shop Selected.", Notification.POSITION_CENTERED);
				else showSubWindow(null, parentShop);
			}
		});
		edit = new Button("Edit");
		edit.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = -2359692724000378336L;

			@SuppressWarnings("unchecked")
			public void buttonClick(Button.ClickEvent event) {
				if (((Set<MinimumPrice>)minimumPriceTable.getValue()).toArray().length > 0){
					try {
						SingleThread st = new SingleThread();
						Set<MinimumPrice> selectedMinimums = (Set<MinimumPrice>) minimumPriceTable.getValue();
						Long id = (Long) selectedMinimums.toArray()[0];
						CrudService service = ((BloomNetAdminApplication)getApplication()).getBean("CrudService", CrudService.class);
						MinimumPrice minimumPrice = (MinimumPrice)st.find(MinimumPrice.class, id, service);
						showSubWindow(minimumPrice, minimumPrice.getShop());
					} catch (LookupException e) {
						e.printStackTrace();
					}
				}else{
					getApplication().getMainWindow().showNotification("No Minimum Price selected to edit.", Notification.POSITION_CENTERED);
				}
			}
		});
		remove = new Button("Remove");
		remove.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = -6253478101876538892L;
			@SuppressWarnings("unchecked")
			public void buttonClick(Button.ClickEvent event) {
				if (((Set<MinimumPrice>)minimumPriceTable.getValue()).toArray().length > 0){
					try {
						Set<MinimumPrice> selectedMinimums = (Set<MinimumPrice>) minimumPriceTable.getValue();
						SingleThread st = new SingleThread();
						CrudService service = ((BloomNetAdminApplication)getApplication()).getBean("CrudService", CrudService.class);
						SearchService searchService = ((BloomNetAdminApplication)getApplication()).getBean("SearchService", SearchService.class);
						List<Object> findList = new ArrayList<Object>();
						Object[] minPricesArray = (Object[])selectedMinimums.toArray();
						for(int ii=0; ii<selectedMinimums.toArray().length; ++ii){
							findList.add(minPricesArray[ii]);
						}
						List<Object> resultsList = (List<Object>) st.findMultiple(MinimumPrice.class, findList, service);
						st.removeMultiple(resultsList, service);
						for(int ii=0; ii<resultsList.size(); ++ii){
							MinimumPrice mp = (MinimumPrice)resultsList.get(ii);
							minimumPriceTable.removeItem(mp.getId());
							parentShop.getListings().remove(mp);
						}
						st.index(Shop.class, parentShop.getId(), searchService);
						
					} catch (LookupException e) {
						e.printStackTrace();
					}
				}else{
					getApplication().getMainWindow().showNotification("No Minimum Price selected to remove.", Notification.POSITION_CENTERED);
				}
			}
		});
		updateAll = new Button("Update All");
		updateAll.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = 8744032684757430261L;

			@Override
			public void buttonClick(Button.ClickEvent event) {
				if(parentShop == null || parentShop.getShopCode() == null) getApplication().getMainWindow().showNotification("No Shop Selected.", Notification.POSITION_CENTERED);
				else updateAll(parentShop, minimumPrice);
			}
		});
		minimumPrice = new TextField("Minimum Prices");
		HorizontalLayout footer = new HorizontalLayout();
		footer.addComponent(add);
		footer.addComponent(edit);
		footer.addComponent(remove);
		footer.addComponent(minimumPrice);
		footer.addComponent(updateAll);
		addComponent(footer);
		
	}
	public void attach() {
		super.attach();
	}
	protected void showSubWindow(MinimumPrice minimumPrice, Shop shop) {
		MinimumPriceSubWindow subWindow = new MinimumPriceSubWindow();
		subWindow.setModal(true);
		MinimumPriceForm minimumPriceForm = new MinimumPriceForm(minimumPriceTable, (BloomNetAdminApplication)getApplication(), shop);
		minimumPriceForm.setParentSubWindow(subWindow);
		subWindow.addComponent(minimumPriceForm);
		getApplication().getMainWindow().addWindow(subWindow);
		if (minimumPrice == null){
			minimumPriceForm.addButtonClickHandler();
		}else{
			minimumPriceForm.setObjectToForm(minimumPrice);
			minimumPriceForm.setReadOnly(false);
		}
	}
	public void setShop(Shop shop) {
		parentShop = shop;
		minimumPriceTable.removeAllItems();
		for(MinimumPrice listing : shop.getMinimumPrices()) {
			minimumPriceTable.addItemToContainer(listing);
		}
	}
	
	public void updateAll(Shop shop, TextField text){
		
		String amount = text.getValue().toString();
		Boolean fluctuate = false;
		SingleThread st = new SingleThread();
		List<Long> indexList = new ArrayList<Long>();
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		SearchService searchService = app.getBean("SearchService", SearchService.class);
		CrudService service = app.getBean("CrudService", CrudService.class);
		
		if(amount.endsWith("H")){
			amount = amount.substring(0, amount.length() - 1);
			fluctuate = true;
		}
		for(MinimumPrice m : shop.getMinimumPrices()){
			m.setMinimumPrice(Double.valueOf(amount));
			m.setFluctuatePrice(fluctuate);
			st.persist(m, service);
		}
		for(Listing listing : parentShop.getListings()) {
			indexList.add(listing.getId());
			System.out.println(listing.getId());
		}
		st.index(Listing.class, indexList, searchService);
	}
}
