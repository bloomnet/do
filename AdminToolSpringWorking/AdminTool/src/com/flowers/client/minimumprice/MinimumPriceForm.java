package com.flowers.client.minimumprice;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.PersistenceException;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.ui.base.BaseForm;
import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.LookupException;
import com.flowers.server.entity.Listing;
import com.flowers.server.entity.MinimumPrice;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.User;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Item;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.Notification;

public class MinimumPriceForm extends BaseForm<MinimumPrice>{
	private static final long serialVersionUID = -7563025920823002110L;
	private Shop parentShop;
	private CheckBox fluctuatePrice;
	private ComboBox productCategory;
	private Window parentSubWindow;
	
	public static final String[] visibleFieldOrder = new String[]{"productCategory", "minimumPrice", "fluctuatePrice"};
	
	public MinimumPriceForm(TableContainer<MinimumPrice> tableContainer, BloomNetAdminApplication bloomNetApp, Shop parentShop) {
		super(tableContainer, visibleFieldOrder);
		this.parentShop = parentShop;
	}
	
	public void setParentSubWindow(Window parentSubWindow) {
		this.parentSubWindow = parentSubWindow;
	}
	
	@Override
	public void cancelButtonClickHandler() {
		discard();
		if (parentSubWindow != null && (parentSubWindow instanceof MinimumPriceSubWindow)){
			((MinimumPriceSubWindow)parentSubWindow).close();
		}
	}
	
	@Override
	public void saveButtonClickHandler() {
		if(productCategory.getValue() != null){
			super.saveButtonClickHandler();
			if (parentSubWindow != null && (parentSubWindow instanceof MinimumPriceSubWindow)){
				((MinimumPriceSubWindow)parentSubWindow).close();
			}
		}else{
			getApplication().getMainWindow().showNotification("No Product cateory selected.", Notification.POSITION_CENTERED);
			return;
		}
	}
	
	@Override
	public MinimumPrice getBlankObject() {
		return new MinimumPrice();
	}

	@Override
	public MinimumPrice insert(MinimumPrice t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SearchService searchService = app.getBean("SearchService", SearchService.class);
		SingleThread st = new SingleThread();
		try {
			User u = (User)searchService.find(User.class, 2L);
			t.setUserByUserCreatedId(u);
			t.setDateCreated(new Date());
			t.setShop(parentShop);
		} catch (Exception e) {
			e.printStackTrace();
		}
		st.persist(t, service);
		parentShop.getMinimumPrices().add(t);
		return t;
	}

	@Override
	public MinimumPrice update(MinimumPrice t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SearchService searchService = app.getBean("SearchService", SearchService.class);
		SingleThread st = new SingleThread();
		try {
			User u = (User)searchService.find(User.class, 2L);
			t.setUserByUserModifiedId(u);
			t.setDateModified(new Date());
		} catch (Exception e) {
			e.printStackTrace();
		}
		st.merge(t, service);
		List<Long> indexList = new ArrayList<Long>();
		for(Listing listing : t.getShop().getListings()) {
			indexList.add(listing.getId());
		}
		st.index(Listing.class, indexList, searchService);
		st.index(t.getShop().getClass(), t.getShop().getId(), searchService);
		return t;
	}

	@Override
	public Object remove(MinimumPrice t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SearchService searchService = app.getBean("SearchService", SearchService.class);
		SingleThread st = new SingleThread();
		Set<Listing> listings = t.getShop().getListings();
		st.remove(t, service);
		List<Long> indexList = new ArrayList<Long>();
		for(Listing listing : listings) {
			indexList.add(listing.getId());
		}
		st.index(Listing.class, indexList, searchService);
		st.index(Shop.class, t.getShop().getId(), searchService);
		return t.getId();
	}
	
	@Override
	public void setItemDataSource(Item newDataSource) {
		super.setItemDataSource(newDataSource);
		setReadOnly(false);
		handleButtons(false);
	}
	
	@Override
	public FormFieldFactory getFormFieldFactory() {
		return new MinimumPriceFieldFactory();
	}

	public class MinimumPriceFieldFactory extends DefaultFieldFactory{
		private static final long serialVersionUID = -1738565607364079114L;

		public Field createField(Item item, Object propertyId, Component uiContext) {
			String pid = (String)propertyId;
			if ("fluctuatePrice".equals(pid)){
				fluctuatePrice = new CheckBox("Fluctuate Price");
				return fluctuatePrice;
			}else if ("productCategory".equals(pid)){
				return getProductCategory();
			}else{
				Field field = super.createField(item, propertyId, uiContext);
				if(field instanceof TextField){
					((TextField)field).setNullRepresentation("");
				}
				return field;
			}
		}
	}

	@SuppressWarnings("unchecked")
	public ComboBox getProductCategory() {
		if (productCategory == null){
			productCategory = new ComboBox("Product Category");
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			try {
				List<Object> cities = (List<Object>) st.findAll("ProductCategory", service);
				if (cities != null){
					for (Object o:cities){
						productCategory.addItem(o);
					}
				}
			} catch (LookupException e) {
				e.printStackTrace();
			} 
		}
		return productCategory;
	}
	
}
