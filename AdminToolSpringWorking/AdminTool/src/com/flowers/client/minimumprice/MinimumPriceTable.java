package com.flowers.client.minimumprice;

import org.apache.lucene.document.Document;

import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.MinimumPrice;
import com.vaadin.data.Item;
import com.vaadin.ui.Table;

public class MinimumPriceTable extends Table implements TableContainer<MinimumPrice> {
	private static final long serialVersionUID = 1536974881471590928L;

	public MinimumPriceTable() {
		addContainerProperty("category", String.class, null);
		addContainerProperty("price", Double.class, null);
		addContainerProperty("fluctuate", String.class, null);
		setSizeFull();
		setSelectable(true);
		setImmediate(true);
		setMultiSelect(true);
		setNullSelectionAllowed(false);
	}
	
	@Override
	public Item addItemToContainer(MinimumPrice t) {
		Item item = this.addItem(t.getId());
		if(item != null) {
			item.getItemProperty("category").setValue(t.getProductCategory().getName());
			item.getItemProperty("price").setValue(t.getMinimumPrice());
			item.getItemProperty("fluctuate").setValue(t.getFluctuatePrice() ? "Y" : "N");
		}
		return item;
	}

	@Override
	public void selectRow(MinimumPrice t) {
		this.setValue(t.getId());
	}

	@Override
	public Item addDocumentToContainer(Document d) {
		throw new RuntimeException("Not implemented on " + this.getClass().getName());
	}
}
