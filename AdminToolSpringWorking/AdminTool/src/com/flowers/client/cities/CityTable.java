package com.flowers.client.cities;

import org.apache.lucene.document.Document;

import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.City;
import com.vaadin.data.Item;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Table;

public class CityTable extends Table implements TableContainer<City> {
	private static final long serialVersionUID = 5727522761057999855L;

	public CityTable() {
		addContainerProperty("type", String.class, null);
		addContainerProperty("name", String.class, null);
		addContainerProperty("state", String.class, null);
		addContainerProperty("shortName", String.class, null);		
		setSizeFull();
		setHeight(200, AbstractComponent.UNITS_PIXELS);
		setSelectable(true);
		setImmediate(true);
		setNullSelectionAllowed(false);
	}
	
	@Override
	public Item addItemToContainer(City t) {
		Item item = this.addItem(t.getId());
		if(item != null) {
			item.getItemProperty("type").setValue(t.getCityType());
			item.getItemProperty("name").setValue(t.getName());
			item.getItemProperty("state").setValue(t.getState().getName());
			item.getItemProperty("shortName").setValue(t.getState().getShortName());
		}
		return item;
	}

	@Override
	public void selectRow(City t) {
		this.setValue(t.getId());
	}

	@Override
	public Item addDocumentToContainer(Document d) {
		Long id = Long.valueOf(d.get("id"));
		Item item = this.addItem(id);
		if(item != null) {
			item.getItemProperty("type").setValue(d.get("city_type"));
			item.getItemProperty("name").setValue(d.get("name"));
			item.getItemProperty("state").setValue(d.get("state.name"));
			item.getItemProperty("shortName").setValue(d.get("state.short_name"));
		}
		return item;
	}
}
