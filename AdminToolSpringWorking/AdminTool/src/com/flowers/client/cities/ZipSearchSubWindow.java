package com.flowers.client.cities;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.ui.base.TableContainer;
import com.flowers.client.zip.ZipTable;
import com.flowers.server.LookupException;
import com.flowers.server.entity.City;
import com.flowers.server.entity.Zip;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class ZipSearchSubWindow extends Window {
	private static final long serialVersionUID = 5043710447377456882L;
	private HorizontalLayout header;
	private TextField searchField;
	private Button searchButton;
	private Button addButton;
	private ZipTable zipTable;
	private HorizontalLayout footer;
	private TableContainer<Zip> parentFacilityTable;
	private City parentCity;
	
	public ZipSearchSubWindow(TableContainer<Zip> parentZipTable, City parentCity) {
		super("ZIP Search", new VerticalLayout());
		this.setSizeFull();
		this.parentFacilityTable = parentZipTable;
		this.parentCity = parentCity;
		this.addComponent(getHeader());
		zipTable = new ZipTable();
		this.addComponent(zipTable);
		this.addComponent(getFooter());
		this.setModal(true);
		setWidth("450px");
		setHeight("450px");
	}
	
	public HorizontalLayout getFooter() {
		if (footer == null){
			footer = new HorizontalLayout();
			addButton = new Button("Add Zip");
			addButton.addListener(new Button.ClickListener() {
				private static final long serialVersionUID = -7327918019388615644L;

				@Override
				public void buttonClick(ClickEvent event) {
					if (zipTable.getValue() != null){
						Long id = (Long)zipTable.getValue();
						if (!isDuplicated(id)){
							addZip(id);
						}else{
							getApplication().getMainWindow().showNotification("ZIP already related to shop.", Notification.POSITION_CENTERED);
						}
					}else{
						getApplication().getMainWindow().showNotification("No ZIP selected to add.", Notification.POSITION_CENTERED);
					}
				}
			});
			footer.addComponent(addButton);
		}
		return footer;
	}

	protected boolean isDuplicated(Long id){
		if (parentCity.getZips() != null){
			for (Zip xref : parentCity.getZips()){
				if (xref.getId() == id.longValue()){
					return true;
				}
			}
		}
		return false;
	}
	
	protected void addZip(Long id){
		try {
			CrudService service = ((BloomNetAdminApplication)getApplication()).getBean("CrudService", CrudService.class);
			SearchService searchService = ((BloomNetAdminApplication)getApplication()).getBean("SearchService", SearchService.class);
			SingleThread st = new SingleThread();
			Zip zip = (Zip)st.find(Zip.class, id, service);
			parentCity.getZips().add(zip);
			st.merge(parentCity, service);
			st.index(City.class, parentCity.getId(), searchService);
			parentFacilityTable.addItemToContainer(zip);
			parentFacilityTable.selectRow(zip);
			this.close();
		} catch (LookupException e) {
			e.printStackTrace();
		}
	}
	
	public HorizontalLayout getHeader() {
		if (header == null){
			header = new HorizontalLayout();
			searchField = new TextField("ZIP Code");
			searchButton = new Button("Search ZIP");
			searchButton.addListener(new Button.ClickListener() {
				private static final long serialVersionUID = 9083699328951648564L;

				@Override
				public void buttonClick(ClickEvent event) {
					search((String)searchField.getValue());
				}
			});
			header.addComponent(searchField);
			header.addComponent(searchButton);
		}
		return header;
	}
	
	protected void search(String zipCode){
		try {
			CrudService service = ((BloomNetAdminApplication)getApplication()).getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			String pql = "from Zip f where f.zipCode like :zipCode";
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("zipCode", "%"+zipCode+"%");
			@SuppressWarnings("unchecked")
			List<Object> facilities = (List<Object>) st.findResults(pql, service, params);
			zipTable.removeAllItems();
			if (facilities != null){
				for (Object o:facilities){
					zipTable.addItemToContainer((Zip)o);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}