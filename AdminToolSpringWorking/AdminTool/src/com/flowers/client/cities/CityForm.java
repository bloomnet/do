package com.flowers.client.cities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.ui.base.BaseForm;
import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.City;
import com.flowers.server.entity.Country;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window.Notification;

public class CityForm extends BaseForm<City> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6250803794125604441L;
	private static final String COMMON_FIELD_WIDTH = "25em";
	private static final String SMALL_FIELD_WIDTH = "12em";
	public static final String[] VISIBLE_FIELDS_ORDER = new String[]{"country", "state", "name", "cityType", "majorCity"};
	
	public CityForm(TableContainer<City> tableContainer) {
		super(tableContainer, VISIBLE_FIELDS_ORDER);
	}
	
	@Override
	public City getBlankObject() {
		return new City();
	}

	@Override
	public void addButtonClickHandler() {
		super.addButtonClickHandler();
	}
	
	@Override
	public City insert(City t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		SearchService searchService =app.getBean("SearchService", SearchService.class);
		CrudService service = app.getBean("CrudService", CrudService.class);
		SingleThread st = new SingleThread();
		st.persist(t, service);
		st.index(City.class, t.getId(), searchService);
		return t;
	}

	@Override
	public City update(City t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SearchService searchService =app.getBean("SearchService", SearchService.class);
		SingleThread st = new SingleThread();
		st.merge(t, service);
		st.index(City.class, t.getId(), searchService);
		return t;
	}

	@Override
	public Object remove(City t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SearchService searchService =app.getBean("SearchService", SearchService.class);
		SingleThread st = new SingleThread();
		st.remove(t, service);
		st.index(City.class, t.getId(), searchService);
		return t.getId();
	}
	
	@Override
	public FormFieldFactory getFormFieldFactory() {
		return new CityFormFieldFactory();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setItemDataSource(Item newDataSource, Collection<?> propertyIds) {
		BeanItem<City> item = (BeanItem<City>)newDataSource;
		if (item.getBean() != null && item.getBean().getState() != null){
			newDataSource.addItemProperty("country", new ObjectProperty<Country>(item.getBean().getState().getCountry()));
		}else{
			newDataSource.addItemProperty("country", new ObjectProperty<Country>(new Country()));
		}
		super.setItemDataSource(newDataSource, propertyIds);
	}
	
	public void saveButtonClickHandler(){
		if(getField("state").getValue() == null ||
				getField("country").getValue() == null ||
				getField("name").getValue() == null ||
				getField("cityType").getValue() == null){
			getApplication().getMainWindow().showNotification("Please Fill Out All Necessary Fields.", Notification.POSITION_CENTERED);
		}else{
			super.saveButtonClickHandler();
		}
	}
	
	private class CityFormFieldFactory extends DefaultFieldFactory{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1568521812739811677L;
		private ComboBox country;
		private ComboBox state;
		@Override
		public Field createField(Item item, Object propertyId,	Component uiContext) {
			String pid = (String)propertyId;
			if("country".equals(pid)){
				return getCountry();
			} else if ("state".equals(pid)){
				return getState();
			} else if ("majorCity".equals(pid)){
				return new CheckBox("Major City");
			} else if ("cityType".equals(pid)){
				ComboBox l = new ComboBox("Type");
				l.setWidth("5em");
				l.addItem("D");
				l.addItem("A");
				l.addItem("N");
				return l;
			} else {
				Field field = super.createField(item, propertyId, uiContext);
				if(field instanceof TextField){
					field.setWidth(COMMON_FIELD_WIDTH);
					((TextField)field).setMaxLength(50);
					((TextField)field).setNullRepresentation("");
				}
				return field;
			}
		}
		
		public ComboBox getCountry() {
			country = new ComboBox();
			country.setWidth(SMALL_FIELD_WIDTH);
			country.setCaption("Country");
			country.setNewItemsAllowed(true);
			country.setNullSelectionAllowed(false);
			List<Object> countries = getCountries();
			if (countries != null){
				for (Object o:countries){
					country.addItem(o);
				}
			}
			country.addListener(new Property.ValueChangeListener() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 7492894717160925656L;

				public void valueChange(Property.ValueChangeEvent event) {
					Country c = (Country)country.getValue();
					if (c != null){
						populateState(c.getId());
					}
				}
			});
			return country;
		}
		
		public void populateState(Long countryId){
			List<Object> states = getStates(countryId);
			boolean stateReadOnly = getState().isReadOnly();
			getState().setReadOnly(false);
			getState().removeAllItems();
			if (states != null){
				for (Object o:states){
					getState().addItem(o);
				}
			}
			getState().setReadOnly(stateReadOnly);
		}
		
		public ComboBox getState() {
			if (state == null){
				state = new ComboBox();
				state.setWidth(SMALL_FIELD_WIDTH);
				state.setCaption("State");
				state.setNewItemsAllowed(true);
				state.setNullSelectionAllowed(false);
			}
			return state;
		}
		
		@SuppressWarnings("unchecked")
		protected List<Object> getCountries(){
			try{
				BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
				CrudService service = app.getBean("CrudService", CrudService.class);
				SingleThread st = new SingleThread();
				return (List<Object>) st.findResults("from Country", service, 0);
			}catch(Exception ee){
				return new ArrayList<Object>();
			}
		}
		
		@SuppressWarnings("unchecked")
		protected List<Object> getStates(Long countryId){
			try{
				String pql = "from State s where s.country.id = :id";
				Map<String, Object> param = new HashMap<String, Object>();
				param.put("id", countryId);
				BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
				CrudService service = app.getBean("CrudService", CrudService.class);
				SingleThread st = new SingleThread();
				return (List<Object>) st.findResults(pql, service, param);
			}catch(Exception ee){
				return new ArrayList<Object>();
			}
		}
	}
	
}
