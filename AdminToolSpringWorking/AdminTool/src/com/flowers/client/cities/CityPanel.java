package com.flowers.client.cities;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.SortField;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.facilities.FacilityTable;
import com.flowers.server.entity.City;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class CityPanel extends VerticalLayout {
	private static final long serialVersionUID = 8003043832145548005L;
	private CityTable cityTable;
	private CityForm cityForm;
	private ZipPanel zipPanel;
	private TextField cityName;
	private Button searchButton;
	
	public HorizontalLayout getCitySearch() {
		HorizontalLayout citySearchLayout = new HorizontalLayout();
		citySearchLayout.setSpacing(true);
		citySearchLayout.setHeight("40px");
		
		cityName = new TextField("City Name");
		citySearchLayout.addComponent(cityName);
		citySearchLayout.setComponentAlignment(cityName, Alignment.MIDDLE_LEFT);
		
		searchButton = new Button("Search");
		citySearchLayout.addComponent(searchButton);
		citySearchLayout.setComponentAlignment(searchButton, Alignment.BOTTOM_LEFT);
		searchButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				City city = new City();
				cityForm.setObjectToForm(city);
				cityForm.editButton.setVisible(false);
				cityForm.removeButton.setVisible(false);
				search();
			}
		});
		Button indexButton = new Button("Index");
		citySearchLayout.addComponent(indexButton);
		citySearchLayout.setComponentAlignment(indexButton, Alignment.BOTTOM_LEFT);
		indexButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
				SearchService searchService = app.getBean("SearchService", SearchService.class);
				SingleThread st = new SingleThread();
				try {
					st.index(City.class, searchService, true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		return citySearchLayout;
	}
	
	@SuppressWarnings("unchecked")
	protected void searchOld(){
		try {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			List<Object> cities = null;
			if(!"".equals(cityName.getValue())) {
				StringBuilder pql = new StringBuilder("from City s where s.name like :cityName");
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("cityName", "%"+cityName.getValue()+"%");
				cities = (List<Object>) st.findResults(pql.toString(), service, params);
			} else{
				cities = (List<Object>) st.findResults("from City", service, new HashMap<String, Object>());
			}
			if(cities != null) {
				cityTable.removeAllItems();
				for(Object facilityObj : cities) {
					City city = (City)facilityObj;
					cityTable.addItemToContainer(city);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	protected void search(){
		try {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			SearchService searchService = app.getBean("SearchService", SearchService.class);
			List<Document> cities = null;
			SortField sortField = new SortField("name", SortField.Type.STRING);
			if(!"".equals(cityName.getValue())) {
				org.apache.lucene.search.PhraseQuery.Builder query = new PhraseQuery.Builder();
				
				String[] searchTerm = cityName.getValue().toString().toLowerCase().split(" ");
				
				for(String word : searchTerm){
					query.add(new Term("tokenizedCityName",word));
				}
				cities = searchService.documents(City.class, query.build(),
						sortField);
			} else{
				cities = searchService.documents(City.class, new MatchAllDocsQuery(), sortField);
			}
			if(cities != null) {
				cityTable.removeAllItems();
				for(Document d : cities) {
					cityTable.addDocumentToContainer(d);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public CityPanel() {
		setSizeFull();
		setSpacing(true);
		addComponent(getCitySearch());

		cityTable = new CityTable();
		addComponent(cityTable);
		cityTable.addListener(new FacilityTable.ValueChangeListener() {
            private static final long serialVersionUID = 2537403500201812771L;
			public void valueChange(ValueChangeEvent event) {
				Long id = (Long)cityTable.getValue();
				try {
					BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
					CrudService service = app.getBean("CrudService", CrudService.class);
					SingleThread st = new SingleThread();
					if (id != null){
						City obj = (City)st.find(City.class, id, service);
						cityForm.setObjectToForm(obj);
						cityForm.removeButton.setVisible(false);
						//cityForm.editButton.setVisible(false);
						zipPanel.setCity(obj);
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
            }
        });
		
		TabSheet tabs = new TabSheet();
		addComponent(tabs);
		tabs.setSizeFull();
		cityForm = new CityForm(cityTable);
		tabs.addTab(cityForm, "City", null);
		zipPanel = new ZipPanel();
		tabs.addTab(zipPanel, "Zips", null);
		
	}
	
}
