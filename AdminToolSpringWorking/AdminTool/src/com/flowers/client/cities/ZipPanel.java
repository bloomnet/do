package com.flowers.client.cities;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.Date;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.BooleanQuery.Builder;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.SortField;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.zip.ZipTable;
import com.flowers.server.entity.City;
import com.flowers.server.entity.Zip;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.terminal.FileResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.Notification;

public class ZipPanel  extends VerticalLayout {
	private static final long serialVersionUID = 8818913251224316620L;
	private Button addButton;
	private Button removeButton;
	private Button exportZips;
	private ZipTable zipTable ;
	private City parentCity;
	
	public ZipPanel() {
		setSizeFull();
		setSpacing(true);
		zipTable = new ZipTable(); 
		addComponent(zipTable);
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		addButton = new Button("Add");
		addButton.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = -1852307706156351495L;

			@Override
			public void buttonClick(ClickEvent event) {
				if(parentCity == null){
					getApplication().getMainWindow().showNotification("No City selected.", Notification.POSITION_CENTERED);
				}else{
					ZipSearchSubWindow subWindow = new ZipSearchSubWindow(zipTable, parentCity);
					getApplication().getMainWindow().addWindow(subWindow);
				}
			}
		});
		removeButton = new Button("Remove");
		removeButton.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = 4601820294909274594L;

			@Override
			public void buttonClick(ClickEvent event) {
				if (zipTable.getValue() != null){
					try {
						Long id = (Long)zipTable.getValue();
						CrudService service = ((BloomNetAdminApplication)getApplication()).getBean("CrudService", CrudService.class);
						SearchService searchService = ((BloomNetAdminApplication)getApplication()).getBean("SearchService", SearchService.class);
						SingleThread st = new SingleThread();
						Zip zip = (Zip)st.find(Zip.class, id, service);
						parentCity.getZips().remove(zip);
						st.merge(parentCity, service);
						st.index(City.class, parentCity.getId(), searchService);
						zipTable.removeItem(id);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}else{
					getApplication().getMainWindow().showNotification("No Zip selected to remove.", Notification.POSITION_CENTERED);
				}
			}
		});
		exportZips = new Button("Export Zips");
		exportZips.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = 4601820294909274594L;

			@Override
			public void buttonClick(ClickEvent event) {
				if (zipTable.getItemIds().toArray().length > 0){
					try {
				
						String date = String.valueOf(new Date().getTime());
						File outFile = new File("/var/lib/tomcat/webapps/zipData/"+date+".doc");
						Writer out = new BufferedWriter(new FileWriter(outFile));
						
						String idsString = "";
						
						BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
						SearchService searchService = app.getBean("SearchService", SearchService.class);
						SortField sortField = new SortField("id", SortField.Type.STRING);
						
						List<Document> cities = null;
						
						Builder bq = new BooleanQuery.Builder();
						org.apache.lucene.search.PhraseQuery.Builder query = new PhraseQuery.Builder();
						
						String searchTerm = String.valueOf(parentCity.getId());
						
						query.add(new Term("id",searchTerm));
						bq.add(query.build(), BooleanClause.Occur.MUST);
						
						cities = searchService.documents(City.class, bq.build(), sortField);

						idsString += cities.get(0).getField("zips").toString().split("zips:")[1].split(">")[0].replaceAll(" ", ",");
							
						out.write(idsString);
						out.close();
	
						FileResource fileResource = new FileResource(outFile, (BloomNetAdminApplication)getApplication());						
						getWindow().open(fileResource);

					} catch (Exception e) {
						e.printStackTrace();
					}
				}else{
					getApplication().getMainWindow().showNotification("No Zip Codes to export.", Notification.POSITION_CENTERED);
				}
			}
		});
		horizontalLayout.addComponent(addButton);
		horizontalLayout.addComponent(removeButton);
		horizontalLayout.addComponent(exportZips);
		addComponent(horizontalLayout);
	}
	
	public void setCity(City city) {
		this.parentCity = city;
		zipTable.removeAllItems();
		for(Zip ref : city.getZips()) {
			zipTable.addItemToContainer(ref);
		}
	}
}
