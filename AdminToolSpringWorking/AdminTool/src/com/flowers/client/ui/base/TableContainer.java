package com.flowers.client.ui.base;

import org.apache.lucene.document.Document;

import com.vaadin.data.Item;

public interface TableContainer<T> {
	public Item addItemToContainer(T t);
	public Item addDocumentToContainer(Document d); 
	public void selectRow(T t);
	public boolean removeItem(Object itemId);
}
