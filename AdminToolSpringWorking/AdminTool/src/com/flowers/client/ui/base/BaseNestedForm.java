package com.flowers.client.ui.base;

import com.vaadin.ui.CustomField;

public abstract class BaseNestedForm<T> extends CustomField {
	private static final long serialVersionUID = -2541104915074143798L;
	private T newObject;
	private boolean isNewObject;
	public BaseNestedForm(T t) {
		isNewObject = (t==null);
		if (isNewObject){
			this.newObject = getBlankObject(); 
		}else{
			this.newObject = t;
		}
	}
	
	public abstract T getBlankObject();
	
	public T getObject() {
		return newObject;
	}
	public boolean isNewObject() {
		return isNewObject;
	}
	
}
