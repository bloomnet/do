package com.flowers.client.ui.base;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;

import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.Form;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.HorizontalLayout;

public abstract class BaseForm<T> extends Form{
	private static final long serialVersionUID = -5736583283876181475L;
	private Button addButton = null;
	public Button removeButton = null;
	private Button saveButton = null;
	private Button cancelButton = null;
	public Button editButton = null;
	private boolean isNewObject = false;
	private T newObject = null;
	private T oldObject = null;
	protected boolean isBlank = true;
	private boolean hasFieldFactory = false;;
	
	private String[] fieldOrder = null;
	private TableContainer<T> tableContainer;
	private Map<String, BaseNestedForm<?>> nestedForms = new HashMap<String, BaseNestedForm<?>>();
	
	public BaseForm(TableContainer<T> tableContainer, String[] visibleFieldOrder) {
		this.fieldOrder = visibleFieldOrder;
		this.tableContainer = tableContainer;
		addButton = new Button("Add");
		addButton.addListener(new ClickListener() {

			private static final long serialVersionUID = -1836571483886148122L;

			public void buttonClick(ClickEvent event) {
				addButtonClickHandler();
			}
		});
		saveButton = new Button("Save");
		saveButton.addListener(new ClickListener() {

			private static final long serialVersionUID = -8498301468616050993L;

			public void buttonClick(ClickEvent event) {
				saveButtonClickHandler();
			}
		});
		cancelButton = new Button("Cancel");
		cancelButton.addListener(new ClickListener() {
	
			private static final long serialVersionUID = 3580470955336111629L;

			public void buttonClick(ClickEvent event) {
				cancelButtonClickHandler();
			}
		});
		editButton = new Button("Edit");
		editButton.addListener(new ClickListener() {

			private static final long serialVersionUID = 6251622137693048936L;

			public void buttonClick(ClickEvent event) {
				editButtonClickHandler();
			}
		});
		removeButton = new Button("Remove");
		removeButton.addListener(new ClickListener() {

			private static final long serialVersionUID = 6810154109495111048L;

			public void buttonClick(ClickEvent event) {
				removeButtonClickHandler();
			}
		});
		setWriteThrough(false);
		setInvalidCommitted(false);
		HorizontalLayout footerLayout = new HorizontalLayout();
		footerLayout.setSpacing(false);
		footerLayout.addComponent(addButton);
		footerLayout.addComponent(saveButton);
		footerLayout.addComponent(cancelButton);
		footerLayout.addComponent(editButton);
		footerLayout.addComponent(removeButton);
		footerLayout.setVisible(false);
		setFooter(footerLayout);
	}

	@Override
	public void attach() {
		super.attach();
		setBaseFormFieldFactory();
		if (isBlank){
			setObjectToForm(getBlankObject(), true);
			setReadOnly(true);
			handleButtons(true);
		}
	}
	
	protected void setBaseFormFieldFactory(){
		this.setFormFieldFactory(getFormFieldFactory());
		this.hasFieldFactory = true;
	}
	
	public abstract T getBlankObject();
	
	public abstract T insert(T t) throws PersistenceException;
	
	public abstract T update(T t) throws PersistenceException;

	public abstract Object remove(T t) throws PersistenceException;
	
	public abstract FormFieldFactory getFormFieldFactory();
	
	public void addNestedForm(String key, BaseNestedForm<?> nestedForm){
		nestedForms.put(key, nestedForm);
	}
	
	public BaseNestedForm<?> getNestedForm(String key){
		return nestedForms.get(key);
	}
	
	public void saveButtonClickHandler(){
		/* If the given input is not valid there is no point in continuing */
		if (!isValid()) {
			return;
		}
		if (isNewObject) {
			//first commit when new object, 
			commit();
			//and after perform DB/JPA tasks
			newObject = insert(newObject);
			/* We need to add the new person to the container */
			tableContainer.addItemToContainer(newObject);
			/*
			 * We must update the form to use the Item from our datasource
			 * as we are now in edit mode
			 */
			setObjectToForm(newObject, false);
			isNewObject = false;
			//select row on Table object
			tableContainer.selectRow(newObject);
		}else{
			commit();
			update(oldObject);
		}
		setReadOnly(true);
		handleButtons(true);
		isBlank = false;
	}
	
	public void addButtonClickHandler(){
		// Create a temporary item for the form
		newObject = getBlankObject();
		setReadOnly(false);
		setItemDataSource(new BeanItem<T>(newObject));
		setReadOnly(false);
		isNewObject = true;
		handleButtons(false);
	}
	
	public void cancelButtonClickHandler(){
		if (isNewObject) {
			isNewObject = false;
			setObjectToForm(oldObject, this.isBlank);
		} else {
			discard();
		}
		setReadOnly(true);
		handleButtons(true);
	}
	
	public void removeButtonClickHandler(){
		Object itemId = remove(oldObject);
		tableContainer.removeItem(itemId);
		setObjectToForm(getBlankObject(), true);
		setReadOnly(true);
		handleButtons(true);
	}
	
	public void editButtonClickHandler(){
		setReadOnly(false);
		handleButtons(false);
	}
	
	protected void setObjectToForm(T data, boolean isBlank) {
		if (!hasFieldFactory){
			setBaseFormFieldFactory();
		}
		this.isBlank = isBlank;
		BeanItem<T> item = new BeanItem<T>(data);
		this.setItemDataSource(item);
		oldObject = data;
	}
	
	public void setObjectToForm(T data) {
		setObjectToForm(data, false);
	}
	
	@Override
	public void setItemDataSource(Item newDataSource) {
		if (!hasFieldFactory){
			setBaseFormFieldFactory();
		}
		if (newDataSource != null) {
			List<String> orderedProperties = Arrays.asList(fieldOrder);
			setItemDataSource(newDataSource, orderedProperties);
			setReadOnly(true);
			handleButtons(true);
			getFooter().setVisible(true);
		} else {
			super.setItemDataSource(null);
			getFooter().setVisible(false);
		}
		isNewObject = false;
	}

	@Override
	public void setReadOnly(boolean readOnly) {
		super.setReadOnly(readOnly);
		for (CustomField f:nestedForms.values()){
			f.setReadOnly(readOnly);
		}
	}
	
	public void handleButtons(boolean readOnly){
		saveButton.setVisible(!readOnly);
		cancelButton.setVisible(!readOnly);
		if (isBlank){
			editButton.setVisible(false);
			removeButton.setVisible(false);
		}else{
			editButton.setVisible(readOnly);
			removeButton.setVisible(readOnly);
		}
		addButton.setVisible(readOnly);
	}
	
	@Override
	public void commit() throws SourceException {
		super.commit();
		for (CustomField f:nestedForms.values()){
			f.commit();
		}
	}
	
	@Override
	public void discard() throws SourceException {
		super.discard();
		for (CustomField f:nestedForms.values()){
			f.discard();
		}
	}
	
	//access methods
	
	public Button getAddButton() {
		return addButton;
	}
	
	public Button getRemoveButton() {
		return removeButton;
	}
	
	public Button getSaveButton() {
		return saveButton;
	}
	
	public Button getCancelButton() {
		return cancelButton;
	}
	
	public Button getEditButton() {
		return editButton;
	}
	
	public boolean isNewObject() {
		return isNewObject;
	}
	
	public boolean isBlank() {
		return isBlank;
	}
	
	public T getNewObject() {
		return newObject;
	}
	
	public T getOldObject() {
		return oldObject;
	}
	
	public String[] getFieldOrder() {
		return fieldOrder;
	}
	
	public TableContainer<T> getTableContainer() {
		return tableContainer;
	}

	public void setNewObject(boolean isNewObject) {
		this.isNewObject = isNewObject;
	}

	public boolean isHasFieldFactory() {
		return hasFieldFactory;
	}
	
}
