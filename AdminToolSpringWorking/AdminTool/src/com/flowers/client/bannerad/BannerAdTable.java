package com.flowers.client.bannerad;

import org.apache.lucene.document.Document;

import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.BannerAd;
import com.vaadin.data.Item;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Table;

public class BannerAdTable extends Table implements TableContainer<BannerAd> {
	private static final long serialVersionUID = 6659565392894085290L;

	public BannerAdTable() {
		addContainerProperty("shopCode", String.class, null);
		addContainerProperty("website", String.class, null);
		setSizeFull();
		setHeight(200, AbstractComponent.UNITS_PIXELS);
		setSelectable(true);
		setImmediate(true);
		setNullSelectionAllowed(false);
	}
	
	@Override
	public Item addItemToContainer(BannerAd t) {
		Item item = this.addItem(t.getId());
		if(item != null) {
			item.getItemProperty("shopCode").setValue(t.getShopCode());
			item.getItemProperty("website").setValue(t.getWebsite());
		}
		return item;
	}

	@Override
	public Item addDocumentToContainer(Document d) {
		Long id = Long.valueOf(d.get("id"));
		Item item = this.addItem(id);
		if(item != null) {
			item.getItemProperty("shopCode").setValue(d.get("shop_code"));
			item.getItemProperty("website").setValue(d.get("website"));
		}
		return item;
	}

	@Override
	public void selectRow(BannerAd t) {
		this.setValue(t.getId());
	}

}
