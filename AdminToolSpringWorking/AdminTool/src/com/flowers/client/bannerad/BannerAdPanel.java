package com.flowers.client.bannerad;

import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.server.entity.BannerAd;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class BannerAdPanel extends VerticalLayout {
	private static final long serialVersionUID = -6449339436955571727L;
	private BannerAdTable bannerAdTable;
	private BannerAdForm bannerAdForm;
	private TextField shopCode;
	private Button searchButton;
	
	public HorizontalLayout getBannerAdSearch() {
		
		HorizontalLayout bannerAdLayout = new HorizontalLayout();
		bannerAdLayout.setSpacing(true);
		bannerAdLayout.setHeight("40px");
		
		shopCode = new TextField("Shop Code");
		
		bannerAdLayout.addComponent(shopCode);
		bannerAdLayout.setComponentAlignment(shopCode, Alignment.MIDDLE_LEFT);
		
		searchButton = new Button("Search");
		bannerAdLayout.addComponent(searchButton);
		bannerAdLayout.setComponentAlignment(searchButton, Alignment.BOTTOM_LEFT);
		searchButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				BannerAd bannerAd = new BannerAd();
				bannerAdForm.setObjectToForm(bannerAd);
				bannerAdForm.removeButton.setVisible(true);
				search();
			}
		});
		final Button indexButton = new Button("Index");
		bannerAdLayout.addComponent(indexButton);
		bannerAdLayout.setComponentAlignment(indexButton, Alignment.BOTTOM_LEFT);
		indexButton.addListener(new ClickListener() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -2876914040646907172L;

			public void buttonClick(ClickEvent event) {
				BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
				SearchService searchService = app.getBean("SearchService", SearchService.class);
				indexButton.setEnabled(false);
				try {
					SingleThread st = new SingleThread();
					st.index(BannerAd.class, searchService,true);
					indexButton.setEnabled(true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
			
		return bannerAdLayout;
	}

	protected void search(){
		try {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			SearchService searchService = app.getBean("SearchService", SearchService.class);
			List<Document> bannerAds = null;
			SortField sortField = new SortField("shop_code", SortField.Type.STRING);
			if(!"".equals(shopCode.getValue())) {
				
				String searchTerm = shopCode.getValue().toString().toUpperCase();
				
				TermQuery query = new TermQuery(new Term("shop_code", searchTerm));
				
				bannerAds = searchService.documents(BannerAd.class, query,
						sortField);
			} else{
				bannerAds = searchService.documents(BannerAd.class, new MatchAllDocsQuery(), sortField);
			}
			if(bannerAds != null) {
				bannerAdTable.removeAllItems();
				for(Document d : bannerAds) {
					bannerAdTable.addDocumentToContainer(d);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public BannerAdPanel() {
		setSizeFull();
		setSpacing(true);
		addComponent(getBannerAdSearch());

		bannerAdTable = new BannerAdTable();
		addComponent(bannerAdTable);
		bannerAdTable.addListener(new BannerAdTable.ValueChangeListener() {
			private static final long serialVersionUID = -3922216275089187949L;

			public void valueChange(ValueChangeEvent event) {
				Long id = (Long)bannerAdTable.getValue();
				try {
					if (id != null){
						BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
						CrudService service = app.getBean("CrudService", CrudService.class);
						SingleThread st = new SingleThread();
						BannerAd obj = (BannerAd)st.find(BannerAd.class, id, service);
						bannerAdForm.setObjectToForm(obj);
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
            }
        });
		
		bannerAdForm = new BannerAdForm(bannerAdTable);
		addComponent(bannerAdForm);
	}
}
