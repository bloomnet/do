package com.flowers.client.socialmedia;

import org.apache.lucene.document.Document;

import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.SocialMedia;
import com.vaadin.data.Item;
import com.vaadin.ui.Table;

public class SocialMediaTable extends Table implements TableContainer<SocialMedia> {
	private static final long serialVersionUID = 27498564362347896L;

	public SocialMediaTable() {
		addContainerProperty("facebook", String.class, null);
		addContainerProperty("googleplus", String.class, null);
		addContainerProperty("twitter", String.class, null);
		addContainerProperty("linkedin", String.class, null);
		addContainerProperty("instagram", String.class, null);
		addContainerProperty("pinterest", String.class, null);
		addContainerProperty("yelp", String.class, null);
		setSizeFull();
		setSelectable(true);
		setImmediate(true);
		setMultiSelect(true);
		setNullSelectionAllowed(false);
	}
	
	@Override
	public Item addItemToContainer(SocialMedia t) {
		Item item = this.addItem(t.getId());
		if(item != null) {
			item.getItemProperty("facebook").setValue(t.getFacebook());
			item.getItemProperty("googleplus").setValue(t.getGoogleplus());
			item.getItemProperty("twitter").setValue(t.getTwitter());
			item.getItemProperty("linkedin").setValue(t.getLinkedin());
			item.getItemProperty("instagram").setValue(t.getInstagram());
			item.getItemProperty("pinterest").setValue(t.getPinterest());
			item.getItemProperty("yelp").setValue(t.getYelp());
		}
		return item;
	}

	@Override
	public void selectRow(SocialMedia t) {
		this.setValue(t.getId());
	}

	@Override
	public Item addDocumentToContainer(Document d) {
		throw new RuntimeException("Not implemented on " + this.getClass().getName());
	}


}
