package com.flowers.client.socialmedia;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.server.LookupException;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.SocialMedia;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.Notification;

public class SocialMediaPanel extends VerticalLayout {
	private static final long serialVersionUID = -6337298689195361915L;
	private SocialMediaTable socialMediaTable;
	private Shop parentShop;
	private Button add;
	private Button edit;
	private Button remove;
	public SocialMediaPanel() {
		socialMediaTable = new SocialMediaTable();
		addComponent(socialMediaTable);
		add = new Button("Add");
		add.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = 42378592347986421L;

			@Override
			public void buttonClick(Button.ClickEvent event) {
				if(parentShop == null || parentShop.getShopCode() == null) getApplication().getMainWindow().showNotification("No Shop Selected.", Notification.POSITION_CENTERED);
				else if(socialMediaTable.getVisibleItemIds().size() > 0)getApplication().getMainWindow().showNotification("Shop Already Has Social Media Data. Please Edit the Existing Data.", Notification.POSITION_CENTERED);
				else showSubWindow(null, parentShop);
			}
		});
		edit = new Button("Edit");
		edit.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = -2435782657267264235L;

			@SuppressWarnings("unchecked")
			public void buttonClick(Button.ClickEvent event) {
				if (((Set<SocialMedia>)socialMediaTable.getValue()).toArray().length > 0){
					try {
						SingleThread st = new SingleThread();
						Set<SocialMedia> selectedSocialMedia = (Set<SocialMedia>) socialMediaTable.getValue();
						Long id = (Long) selectedSocialMedia.toArray()[0];
						CrudService service = ((BloomNetAdminApplication)getApplication()).getBean("CrudService", CrudService.class);
						SocialMedia sm = (SocialMedia)st.find(SocialMedia.class, id, service);
						showSubWindow(sm, sm.getShop());
					} catch (LookupException e) {
						e.printStackTrace();
					}
				}else{
					getApplication().getMainWindow().showNotification("No Social Networks selected to edit.", Notification.POSITION_CENTERED);
				}
			}
		});
		remove = new Button("Remove");
		remove.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = -6253478101876538892L;
			@SuppressWarnings("unchecked")
			public void buttonClick(Button.ClickEvent event) {
				if (((Set<SocialMedia>)socialMediaTable.getValue()).toArray().length > 0){
					try {
						Set<SocialMedia> selectedSocialMedia = (Set<SocialMedia>) socialMediaTable.getValue();
						SingleThread st = new SingleThread();
						CrudService service = ((BloomNetAdminApplication)getApplication()).getBean("CrudService", CrudService.class);
						SearchService searchService = ((BloomNetAdminApplication)getApplication()).getBean("SearchService", SearchService.class);
						List<Object> findList = new ArrayList<Object>();
						Object[] socialMediaArray = (Object[])selectedSocialMedia.toArray();
						for(int ii=0; ii<selectedSocialMedia.toArray().length; ++ii){
							findList.add(socialMediaArray[ii]);
						}
						List<Object> resultsList = (List<Object>) st.findMultiple(SocialMedia.class, findList, service);
						st.removeMultiple(resultsList, service);
						for(int ii=0; ii<resultsList.size(); ++ii){
							SocialMedia sm = (SocialMedia)resultsList.get(ii);
							socialMediaTable.removeItem(sm.getId());
							parentShop.getListings().remove(sm);
						}
						st.index(Shop.class, parentShop.getId(), searchService);
						
					} catch (LookupException e) {
						e.printStackTrace();
					}
				}else{
					getApplication().getMainWindow().showNotification("No Social Networks selected to remove.", Notification.POSITION_CENTERED);
				}
			}
		});
		HorizontalLayout footer = new HorizontalLayout();
		footer.addComponent(add);
		footer.addComponent(edit);
		footer.addComponent(remove);
		addComponent(footer);
		
	}
	public void attach() {
		super.attach();
	}
	protected void showSubWindow(SocialMedia sm, Shop shop) {
		SocialMediaSubWindow subWindow = new SocialMediaSubWindow();
		subWindow.setModal(true);
		SocialMediaForm smf = new SocialMediaForm(socialMediaTable, (BloomNetAdminApplication)getApplication(), shop);
		smf.setParentSubWindow(subWindow);
		subWindow.addComponent(smf);
		getApplication().getMainWindow().addWindow(subWindow);
		if (sm == null){
			smf.addButtonClickHandler();
		}else{
			smf.setObjectToForm(sm);
			smf.setReadOnly(false);
		}
	}
	public void setShop(Shop shop) {
		parentShop = shop;
		socialMediaTable.removeAllItems();
		for(SocialMedia sm : shop.getSocialMedias()) {
			socialMediaTable.addItemToContainer(sm);
		}
	}
}
