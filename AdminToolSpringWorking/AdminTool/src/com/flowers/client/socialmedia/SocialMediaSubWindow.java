package com.flowers.client.socialmedia;

import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class SocialMediaSubWindow extends Window {
	private static final long serialVersionUID = 5682553948512489973L;

	public SocialMediaSubWindow() {
		super("Social Media");
		VerticalLayout layout = (VerticalLayout) getContent();
        layout.setMargin(true);
        layout.setSpacing(true); 
        layout.setWidth("400px");
        layout.setHeight("400px");
		setModal(true);
	}
	
	@Override
	public void close() {
		super.close();
	}
}
