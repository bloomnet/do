package com.flowers.client.socialmedia;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.PersistenceException;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.ui.base.BaseForm;
import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.Listing;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.SocialMedia;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;

public class SocialMediaForm extends BaseForm<SocialMedia>{
	private static final long serialVersionUID = -7563025920823002110L;
	private Shop parentShop;
	private Window parentSubWindow;
	
	public static final String[] visibleFieldOrder = new String[]{"facebook", "googleplus", "twitter", "linkedin", "instagram", "pinterest", "yelp"};
	
	public SocialMediaForm(TableContainer<SocialMedia> tableContainer, BloomNetAdminApplication bloomNetApp, Shop parentShop) {
		super(tableContainer, visibleFieldOrder);
		this.parentShop = parentShop;
	}
	
	public void setParentSubWindow(Window parentSubWindow) {
		this.parentSubWindow = parentSubWindow;
	}
	
	@Override
	public void cancelButtonClickHandler() {
		discard();
		if (parentSubWindow != null && (parentSubWindow instanceof SocialMediaSubWindow)){
			((SocialMediaSubWindow)parentSubWindow).close();
		}
	}
	
	@Override
	public void saveButtonClickHandler(){
		super.saveButtonClickHandler();
		if (parentSubWindow != null && (parentSubWindow instanceof SocialMediaSubWindow)){
			((SocialMediaSubWindow)parentSubWindow).close();
		}
	}

	
	@Override
	public SocialMedia getBlankObject() {
		return new SocialMedia();
	}

	@Override
	public SocialMedia insert(SocialMedia t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SingleThread st = new SingleThread();
		try {
			t.setShop(parentShop);
		} catch (Exception e) {
			e.printStackTrace();
		}
		st.persist(t, service);
		parentShop.getSocialMedias().add(t);
		SearchService searchService = app.getBean("SearchService", SearchService.class);
		st.index(t.getShop().getClass(), t.getShop().getId(), searchService);
		List<Long> indexList = new ArrayList<Long>();
		for(Listing listing : t.getShop().getListings()) {
			indexList.add(listing.getId());
		}
		st.index(Listing.class, indexList, searchService);
		st.index(Shop.class, t.getShop().getId(), searchService);
		return t;
	}

	@Override
	public SocialMedia update(SocialMedia t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SearchService searchService = app.getBean("SearchService", SearchService.class);
		SingleThread st = new SingleThread();
		st.merge(t, service);
		st.index(t.getShop().getClass(), t.getShop().getId(), searchService);
		List<Long> indexList = new ArrayList<Long>();
		for(Listing listing : t.getShop().getListings()) {
			indexList.add(listing.getId());
		}
		st.index(Listing.class, indexList, searchService);
		st.index(Shop.class, t.getShop().getId(), searchService);
		return t;
	}

	@Override
	public Object remove(SocialMedia t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SearchService searchService = app.getBean("SearchService", SearchService.class);
		SingleThread st = new SingleThread();
		Set<Listing> listings = t.getShop().getListings();
		st.remove(t, service);
		st.index(Shop.class, t.getShop().getId(), searchService);
		List<Long> indexList = new ArrayList<Long>();
		for(Listing listing : listings) {
			indexList.add(listing.getId());
		}
		st.index(Listing.class, indexList, searchService);
		st.index(Shop.class, t.getShop().getId(), searchService);
		return t.getId();
	}
	
	@Override
	public void setItemDataSource(Item newDataSource) {
		super.setItemDataSource(newDataSource);
		setReadOnly(false);
		handleButtons(false);
	}
	
	@Override
	public FormFieldFactory getFormFieldFactory() {
		return new SocialMediaFieldFactory();
	}

	private static final String COMMON_FIELD_WIDTH = "25em";
	public class SocialMediaFieldFactory extends DefaultFieldFactory {
		private static final long serialVersionUID = -432098532723496243L;
		
		public Field createField(Item item, Object propertyId, Component uiContext) {
			String pid = (String)propertyId;
			Field field = super.createField(item, propertyId, uiContext);
			if(field instanceof TextField){
				TextField txt = (TextField)field;
				if ("facebook".equals(pid)){
					txt.setMaxLength(200);
				}else if ("googleplus".equals(pid)){
					txt.setMaxLength(200);
				}else if ("twitter".equals(pid)){
					txt.setMaxLength(200);
				}else if ("linkedin".equals(pid)){
					txt.setMaxLength(200);
				}else if ("instagram".equals(pid)){
					txt.setMaxLength(200);
				}else if ("pinterest".equals(pid)){
					txt.setMaxLength(200);
				}else if ("yelp".equals(pid)){
					txt.setMaxLength(200);
				}
				txt.setWidth(COMMON_FIELD_WIDTH);
				txt.setNullRepresentation("");
			}
			return field;
		}
	}
}

