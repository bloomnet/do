package com.flowers.client;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.flowers.client.bannerad.BannerAdPanel;
import com.flowers.client.cities.CityPanel;
import com.flowers.client.consumerlandingad.ConsumerLandingAdPanel;
import com.flowers.client.consumerresultsad.ConsumerResultsAdPanel;
import com.flowers.client.dropship.DropshipPanel;
import com.flowers.client.facilities.FacilityPanel;
import com.flowers.client.fruitbouquet.FruitBouquetPanel;
import com.flowers.client.listings.ListingPanel;
import com.flowers.client.products.ProductPanel;
import com.flowers.client.shops.ShopPanel;
import com.flowers.client.sidead.SideAdPanel;
import com.flowers.client.states.StatePanel;
import com.flowers.client.zip.ZipPanel;
import com.vaadin.Application;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.terminal.Sizeable;
import com.vaadin.terminal.gwt.server.WebApplicationContext;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Window;

public class BloomNetAdminApplication extends Application {
	
	private static final long serialVersionUID = 3539905928125263349L;	
	private Window root;
	private ApplicationContext springContext;
	private BloomNetAdminNavTree tree;
	private ListingPanel listingPanel;
	private ShopPanel shopPanel;
	private FacilityPanel facilityPanel;
	private CityPanel cityPanel;
	private StatePanel statePanel;
	private ProductPanel productPanel;
	private DropshipPanel dropshipPanel;
	private FruitBouquetPanel fruitBouquetPanel;
	private BannerAdPanel bannerAdPanel;
	private SideAdPanel sideAdPanel;
	private ConsumerLandingAdPanel consumerLandingAdPanel;
	private ConsumerResultsAdPanel consumerResultsAdPanel;
	private ZipPanel zipPanel;
	//private ProcessedOrderPanel orderPanel;
	//private LogViewerPanel logPanel;
	
	private TreeClickListener listener = new TreeClickListener();
	
	public void init() {
		if(getContext() instanceof WebApplicationContext) {
			ServletContext servletContext = ((WebApplicationContext) getContext()).getHttpSession().getServletContext();
        	springContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
		}

		HorizontalLayout rootLayout = new HorizontalLayout();
        root = new Window("BloomNet Adminstration", rootLayout);
        setMainWindow(root);
        rootLayout.setMargin(false);
        rootLayout.setHeight("850px");
        rootLayout.setWidth(Sizeable.SIZE_UNDEFINED, 0);
        rootLayout.setSpacing(false);
        
        
        Panel sidebar = new Panel();
        sidebar.setSizeFull();
        
        tree = new BloomNetAdminNavTree("BloomNet Administration");
        sidebar.addComponent(tree);
        tree.addListener(listener);
        
        Panel tabs = new Panel();
        tabs.setSizeFull();
        sidebar.setWidth(200, HorizontalSplitPanel.UNITS_PIXELS);
        tabs.setWidth(1024, HorizontalSplitPanel.UNITS_PIXELS);
        root.addComponent(sidebar);
        root.addComponent(tabs);
        
        shopPanel = new ShopPanel();
        tabs.addComponent(shopPanel);
        shopPanel.setVisible(true);
        
        listingPanel = new ListingPanel();
        tabs.addComponent(listingPanel);
        listingPanel.setVisible(false);
        
        facilityPanel = new FacilityPanel();
        tabs.addComponent(facilityPanel);
        facilityPanel.setVisible(false);
        
        cityPanel = new CityPanel();
        tabs.addComponent(cityPanel);
        cityPanel.setVisible(false);
        
        statePanel = new StatePanel();
        tabs.addComponent(statePanel);
        statePanel.setVisible(false);
        
        productPanel = new ProductPanel();
        tabs.addComponent(productPanel);
        productPanel.setVisible(false);
        
        dropshipPanel = new DropshipPanel();
        tabs.addComponent(dropshipPanel);
        dropshipPanel.setVisible(false);
        
        fruitBouquetPanel = new FruitBouquetPanel();
        tabs.addComponent(fruitBouquetPanel);
        fruitBouquetPanel.setVisible(false);
        
        bannerAdPanel = new BannerAdPanel();
        tabs.addComponent(bannerAdPanel);
        bannerAdPanel.setVisible(false);
        
        sideAdPanel = new SideAdPanel();
        tabs.addComponent(sideAdPanel);
        sideAdPanel.setVisible(false);
        
        consumerLandingAdPanel = new ConsumerLandingAdPanel();
        tabs.addComponent(consumerLandingAdPanel);
        consumerLandingAdPanel.setVisible(false);
        
        consumerResultsAdPanel = new ConsumerResultsAdPanel();
        tabs.addComponent(consumerResultsAdPanel);
        consumerResultsAdPanel.setVisible(false);
        
        zipPanel = new ZipPanel();
        tabs.addComponent(zipPanel);
        zipPanel.setVisible(false);
        
        /*orderPanel = new ProcessedOrderPanel();
        tabs.addComponent(orderPanel);
        orderPanel.setVisible(false);
        
        logPanel = new LogViewerPanel();
        tabs.addComponent(logPanel);
        logPanel.setVisible(false);*/
        
	}
	public void onRequestStart(HttpServletRequest request, HttpServletResponse response) {
		if(springContext == null) {
			
			ServletContext servletContext = ((WebApplicationContext) getContext()).getHttpSession().getServletContext();
			springContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
		}
	}
	
	public class TreeClickListener implements ItemClickListener {
		private static final long serialVersionUID = 8153657076750372005L;

		public void itemClick(ItemClickEvent event) {
			if(event.getButton() == ItemClickEvent.BUTTON_LEFT) {
				shopPanel.setVisible(false);
				facilityPanel.setVisible(false);
				cityPanel.setVisible(false);
				statePanel.setVisible(false);
				productPanel.setVisible(false);
				zipPanel.setVisible(false);
				//orderPanel.setVisible(false);
				listingPanel.setVisible(false);
				//logPanel.setVisible(false);
				dropshipPanel.setVisible(false);
				fruitBouquetPanel.setVisible(false);
				bannerAdPanel.setVisible(false);
				sideAdPanel.setVisible(false);
				consumerLandingAdPanel.setVisible(false);
				consumerResultsAdPanel.setVisible(false);
				
				Long itemId = (Long)event.getItemId();
				if(itemId.equals(1L)) shopPanel.setVisible(true);
				if(itemId.equals(2L)) facilityPanel.setVisible(true);
				if(itemId.equals(3L)) cityPanel.setVisible(true);
				if(itemId.equals(4L)) statePanel.setVisible(true);
				if(itemId.equals(5L)) productPanel.setVisible(true);
				if(itemId.equals(6L)) zipPanel.setVisible(true);
				//if(itemId.equals(7L)) orderPanel.setVisible(true);
				if(itemId.equals(8L)) listingPanel.setVisible(true);
				//if(itemId.equals(9L)) logPanel.setVisible(true);
				if(itemId.equals(14L)) dropshipPanel.setVisible(true);
				if(itemId.equals(15L)) fruitBouquetPanel.setVisible(true);
				if(itemId.equals(16L)) bannerAdPanel.setVisible(true);
				if(itemId.equals(17L)) sideAdPanel.setVisible(true);
				if(itemId.equals(18L)) consumerLandingAdPanel.setVisible(true);
				if(itemId.equals(19L)) consumerResultsAdPanel.setVisible(true);
				if(itemId.equals(10L)){
					String relativePath = ((WebApplicationContext)root.getApplication().getContext()).getHttpSession().getServletContext().getContextPath();
					root.getApplication().setLogoutURL(relativePath+"/signout.htm");
					root.getApplication().close();
				}
					
			}		
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getBean(final String beanRef, Class<T> classOfService) {
        return (T)springContext.getBean(beanRef);
    }
	public void onRequestEnd(HttpServletRequest request, HttpServletResponse response) {} 	
}
