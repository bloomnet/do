package com.flowers.client;

import com.vaadin.data.Item;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Tree;

public class BloomNetAdminNavTree extends Tree {
	private static final long serialVersionUID = 8465557391410634015L;
	private HierarchicalContainer container = new HierarchicalContainer();
	
	public BloomNetAdminNavTree(String caption) {
		super(caption);
		setItemCaptionPropertyId("name");
        setItemCaptionMode(AbstractSelect.ITEM_CAPTION_MODE_PROPERTY);
	}
	public void attach() {
		super.attach();
		container.addContainerProperty("name", String.class, null);
		Item item = container.addItem(1L);
		container.setChildrenAllowed(1L, false);
		item.getItemProperty("name").setValue("Shops");
		
		item = container.addItem(8L);
		container.setChildrenAllowed(8L, false);
		item.getItemProperty("name").setValue("Listings");
		
		item = container.addItem(2L);
		container.setChildrenAllowed(2L, false);
		item.getItemProperty("name").setValue("Facilities");
		
		item = container.addItem(3L);
		container.setChildrenAllowed(3L, false);
		item.getItemProperty("name").setValue("Cities");
		
		item = container.addItem(4L);
		container.setChildrenAllowed(4L, false);
		item.getItemProperty("name").setValue("States");
		
		item = container.addItem(5L);
		container.setChildrenAllowed(5L, false);
		item.getItemProperty("name").setValue("Products");
		
		item = container.addItem(14L);
		container.setChildrenAllowed(14L, false);
		item.getItemProperty("name").setValue("Dropship Products");
		
		item = container.addItem(15L);
		container.setChildrenAllowed(15L, false);
		item.getItemProperty("name").setValue("Fruit Bouquet Products");
		
		item = container.addItem(16L);
		container.setChildrenAllowed(16L, false);
		item.getItemProperty("name").setValue("Banner Ads");
		
		item = container.addItem(17L);
		container.setChildrenAllowed(17L, false);
		item.getItemProperty("name").setValue("Side Ads");
		
		item = container.addItem(18L);
		container.setChildrenAllowed(18L, false);
		item.getItemProperty("name").setValue("Consumer Landing Ads");
		
		item = container.addItem(19L);
		container.setChildrenAllowed(19L, false);
		item.getItemProperty("name").setValue("Consumer Results Ads");
		
		item = container.addItem(6L);
		container.setChildrenAllowed(6L, false);
		item.getItemProperty("name").setValue("ZIP");
		
		item = container.addItem(7L);
		container.setChildrenAllowed(7L, false);
		item.getItemProperty("name").setValue("");
		
		item = container.addItem(9L);
		container.setChildrenAllowed(9L, false);
		item.getItemProperty("name").setValue("");
		
		item = container.addItem(11L);
		container.setChildrenAllowed(11L, false);
		item.getItemProperty("name").setValue("");
		
		item = container.addItem(12L);
		container.setChildrenAllowed(12L, false);
		item.getItemProperty("name").setValue("");
		
		item = container.addItem(13L);
		container.setChildrenAllowed(13L, false);
		item.getItemProperty("name").setValue("");
		
		item = container.addItem(10L);
		container.setChildrenAllowed(10L, false);
		item.getItemProperty("name").setValue("Sign Out");
		item.getItemPropertyIds();
		
		setContainerDataSource(container);
	}
}
