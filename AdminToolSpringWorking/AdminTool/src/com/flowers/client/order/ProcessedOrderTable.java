/*
 * Copyright (C) 2011 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package com.flowers.client.order;

import java.util.Date;

import org.apache.lucene.document.Document;

import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.ProcessedOrder;
import com.vaadin.data.Item;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Table;

public class ProcessedOrderTable extends Table implements TableContainer<ProcessedOrder> {
	private static final long serialVersionUID = -1757087490840900886L;

	public ProcessedOrderTable() {
		addContainerProperty("date", String.class, null);
		addContainerProperty("order id", String.class, null);		
		addContainerProperty("sending shop", String.class, null);
		addContainerProperty("receiving shop", String.class, null);
		addContainerProperty("recipient", String.class, null);
		addContainerProperty("recipient address", String.class, null);
		addContainerProperty("message", String.class, null);
		setSizeFull();
		setHeight(500, AbstractComponent.UNITS_PIXELS);
		setSelectable(true);
		setImmediate(true);
		setNullSelectionAllowed(false);
	}
	public Item addItemToContainer(ProcessedOrder t) {
		Item item = this.addItem(t.getId());
		if(item != null) {
			item.getItemProperty("date").setValue(new Date(t.getTimestamp()).toString());
			item.getItemProperty("order id").setValue(t.getTrackingNumber());
			item.getItemProperty("sending shop").setValue(t.getSendingShop());
			item.getItemProperty("receiving shop").setValue(t.getReceivingShop());
		}
		return item;
	}

	public Item addDocumentToContainer(Document d) {
		Long id = Long.valueOf(d.get("id"));
		Item item = this.addItem(id);
		if(item != null) {
			item.getItemProperty("date").setValue(new Date(Long.valueOf(d.get("timestamp"))).toString());
			item.getItemProperty("order id").setValue(d.get("tracking_number"));
			item.getItemProperty("sending shop").setValue(d.get("sending_shop"));
			item.getItemProperty("receiving shop").setValue(d.get("receiving_shop"));
			item.getItemProperty("recipient").setValue(d.get("recipient_name"));
			item.getItemProperty("recipient address").setValue(d.get("recipient_address"));
			item.getItemProperty("message").setValue(d.get("message"));
		}
		return item;
	}

	public void selectRow(ProcessedOrder t) {
	}

}
