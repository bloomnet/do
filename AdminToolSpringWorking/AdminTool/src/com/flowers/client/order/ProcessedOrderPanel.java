package com.flowers.client.order;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.products.ProductTable;
import com.flowers.server.entity.ProcessedOrder;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class ProcessedOrderPanel extends VerticalLayout {
	private static final long serialVersionUID = 8945410887102864690L;
	private ProcessedOrderTable orderTable;
	private TextField zipCodeSearch;
	private Button searchButton;
	
	
	public ProcessedOrderPanel() {
		setSizeFull();
		setSpacing(true);
		
		HorizontalLayout zipSearchLayout = new HorizontalLayout();
		zipSearchLayout.setSpacing(true);
		zipSearchLayout.setHeight("40px");
		
		zipCodeSearch = new TextField("Order ID");
		zipSearchLayout.addComponent(zipCodeSearch);
		zipSearchLayout.setComponentAlignment(zipCodeSearch, Alignment.MIDDLE_LEFT);
		
		searchButton = new Button("Search");
		zipSearchLayout.addComponent(searchButton);
		zipSearchLayout.setComponentAlignment(searchButton, Alignment.BOTTOM_LEFT);
		searchButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				search();
			}
		});
		Button indexButton = new Button("Index");
		zipSearchLayout.addComponent(indexButton);
		zipSearchLayout.setComponentAlignment(indexButton, Alignment.BOTTOM_LEFT);
		indexButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
				SearchService searchService = app.getBean("SearchService", SearchService.class);
				SingleThread st = new SingleThread();
				st.index(ProcessedOrder.class, searchService, true);
			}
		});
		addComponent(zipSearchLayout);
		
		orderTable = new ProcessedOrderTable();
		addComponent(orderTable);
		orderTable.addListener(new ProductTable.ValueChangeListener() {
            private static final long serialVersionUID = 2537403500201812771L;
			public void valueChange(ValueChangeEvent event) {
				Long id = (Long)orderTable.getValue();
				try {
					BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
					CrudService service = app.getBean("CrudService", CrudService.class);
					SingleThread st = new SingleThread();
					if (id != null){
						@SuppressWarnings("unused")
						ProcessedOrder obj = (ProcessedOrder)st.find(ProcessedOrder.class, id, service);
						
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
            }
        });
	}
	
	protected void search(){
		try {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			SearchService searchService = app.getBean("SearchService", SearchService.class);
			
			List<Document> states = null;
			SortField sortField = new SortField("timestamp", SortField.Type.STRING, true);
			if(!"".equals(zipCodeSearch.getValue())) {
				TermQuery query = new TermQuery(new Term("tracking_number", zipCodeSearch.getValue().toString().toLowerCase()));
				 states = searchService.documents(ProcessedOrder.class, query, sortField);
			} else{
				states = searchService.documents(ProcessedOrder.class, new MatchAllDocsQuery(), sortField);
			}
			if(states != null) {
				orderTable.removeAllItems();
				for(Document d : states) {
					orderTable.addDocumentToContainer(d);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
