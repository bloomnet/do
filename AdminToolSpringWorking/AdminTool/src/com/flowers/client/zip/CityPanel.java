package com.flowers.client.zip;

import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.City;
import com.flowers.server.entity.Listing;
import com.flowers.server.entity.Zip;
import com.flowers.server.service.SearchService;
import com.vaadin.data.Item;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

public class CityPanel extends VerticalLayout {
	private static final long serialVersionUID = 450350090464546269L;
	private CityTable cityTable;
	
	public CityPanel() {
		cityTable = new CityTable();
		addComponent(cityTable);
	}

	public void setZip(Zip zip) {
		cityTable.removeAllItems();
		for(City city : zip.getCities()) {
			cityTable.addItemToContainer(city);
		}		
	}
	
	public class CityTable extends Table implements TableContainer<City> {
		private static final long serialVersionUID = -6462320757448354385L;

		public CityTable() {
			addContainerProperty("state", String.class, null);
			addContainerProperty("name", String.class, null);
			addContainerProperty("listings", String.class, null);
			setSizeFull();
			setSelectable(true);
			setImmediate(true);
			setNullSelectionAllowed(false);
		}
		public Item addItemToContainer(City t) {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			SearchService searchService = app.getBean("SearchService", SearchService.class);
			Item item = this.addItem(t.getId());
			if(item != null) {
				TermQuery query = new TermQuery(new Term("city", t.getName()));
				List<Document> listings = searchService.documents(Listing.class, query, new SortField("city", SortField.Type.STRING));
				item.getItemProperty("state").setValue(t.getName());
				item.getItemProperty("name").setValue(t.getState().getName());
				item.getItemProperty("listings").setValue(listings.size() > 0);
			}
			return item;
		}

		public Item addDocumentToContainer(Document d) {
			return null;
		}

		public void selectRow(City t) {
		}
		
	}
}
