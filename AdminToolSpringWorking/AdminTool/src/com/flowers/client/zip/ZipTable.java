package com.flowers.client.zip;

import org.apache.lucene.document.Document;

import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.Zip;
import com.vaadin.data.Item;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Table;

public class ZipTable extends Table implements TableContainer<Zip> {

	private static final long serialVersionUID = -832635756826655722L;

	public ZipTable() {
		addContainerProperty("zip", String.class, null);
		addContainerProperty("latitude", String.class, null);		
		addContainerProperty("longitude", String.class, null);		
		setSizeFull();
		setHeight(200, AbstractComponent.UNITS_PIXELS);
		setSelectable(true);
		setImmediate(true);
		setNullSelectionAllowed(false);
	}
	
	@Override
	public Item addItemToContainer(Zip t) {
		Item item = this.addItem(t.getId());
		if(item != null) {
			item.getItemProperty("zip").setValue(t.getZipCode());
			item.getItemProperty("latitude").setValue(t.getLatitude());
			item.getItemProperty("longitude").setValue(t.getLongitude());
		}
		return item;
	}

	@Override
	public Item addDocumentToContainer(Document d) {
		Long id = Long.valueOf(d.get("id"));
		Item item = this.addItem(id);
		if(item != null) {
			item.getItemProperty("zip").setValue(d.get("value"));
			item.getItemProperty("latitude").setValue(d.get("latitude"));
			item.getItemProperty("longitude").setValue(d.get("longitude"));
		}
		return item;
	}

	@Override
	public void selectRow(Zip t) {
		this.setValue(t.getId());
	}

}
