package com.flowers.client.zip;

import javax.persistence.PersistenceException;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.ui.base.BaseForm;
import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.Zip;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window.Notification;

public class ZipForm extends BaseForm<Zip> {

	private static final long serialVersionUID = -4653515654339424349L;
	private static final String COMMON_FIELD_WIDTH = "25em";
	public static final String[] VISIBLE_FIELDS_ORDER = new String[]{"zipCode","latitude","longitude"};
	
	public ZipForm(TableContainer<Zip> tableContainer) {
		super(tableContainer, VISIBLE_FIELDS_ORDER);
	}
	

	public Zip getBlankObject() {
		return new Zip();
	}

	public Zip insert(Zip t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SingleThread st = new SingleThread();
		st.persist(t, service);
		return t;
	}

	public Zip update(Zip t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SingleThread st = new SingleThread();
		st.merge(t, service);
		return t;
	}

	public Object remove(Zip t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SingleThread st = new SingleThread();
		st.remove(t, service);
		return t.getId();
	}

	public FormFieldFactory getFormFieldFactory() {
		return new ZipFormFieldFactory();
	}
	
	public void saveButtonClickHandler(){
		if(getField("zipCode").getValue() == null ||
				getField("latitude").getValue() == null ||
				getField("longitude").getValue() == null){
			getApplication().getMainWindow().showNotification("Please Fill Out All Necessary Fields.", Notification.POSITION_CENTERED);
		}else{
			super.saveButtonClickHandler();
		}
	}
	
	private class ZipFormFieldFactory extends DefaultFieldFactory{

		private static final long serialVersionUID = 6923289372532479468L;

		@Override
		public Field createField(Item item, Object propertyId,	Component uiContext) {
			String pid = (String)propertyId;
			Field field = super.createField(item, propertyId, uiContext);
			if(field instanceof TextField) {
				TextField txt = (TextField)field;
				if ("zipCode".equals(pid)){
					txt.setWidth(COMMON_FIELD_WIDTH);
					txt.setMaxLength(10);
				}else if ("latitude".equals(pid)){
					txt.setWidth(COMMON_FIELD_WIDTH);
				}else if ("longitude".equals(pid)){
					txt.setWidth(COMMON_FIELD_WIDTH);
				}
				txt.setNullRepresentation("");
			}
			return field;
		}
	}
}