package com.flowers.client.zip;

import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.products.ProductTable;
import com.flowers.server.entity.Zip;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class ZipPanel extends VerticalLayout {
	
	private static final long serialVersionUID = 3376883560546015784L;
	private ZipTable zipTable;
	private ZipForm zipForm;
	private CityPanel cityPanel;
	private TextField zipCodeSearch;
	private Button searchButton;
	
	public HorizontalLayout getZipSearch() {
		HorizontalLayout zipSearchLayout = new HorizontalLayout();
		zipSearchLayout.setSpacing(true);
		zipSearchLayout.setHeight("40px");
		
		zipCodeSearch = new TextField("Zip Code");
		zipSearchLayout.addComponent(zipCodeSearch);
		zipSearchLayout.setComponentAlignment(zipCodeSearch, Alignment.MIDDLE_LEFT);
		
		searchButton = new Button("Search");
		zipSearchLayout.addComponent(searchButton);
		zipSearchLayout.setComponentAlignment(searchButton, Alignment.BOTTOM_LEFT);
		searchButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				Zip zip = new Zip();
				zipForm.setObjectToForm(zip);
				zipForm.editButton.setVisible(false);
				zipForm.removeButton.setVisible(false);
				search();
			}
		});
		Button indexButton = new Button("Index");
		zipSearchLayout.addComponent(indexButton);
		zipSearchLayout.setComponentAlignment(indexButton, Alignment.BOTTOM_LEFT);
		indexButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
				SearchService searchService = app.getBean("SearchService", SearchService.class);
				try {
					SingleThread st = new SingleThread();
					st.index(Zip.class, searchService,true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		return zipSearchLayout;
	}
	
	protected void search(){
		try {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			SearchService searchService = app.getBean("SearchService", SearchService.class);
			List<Document> states = null;
			SortField sortField = new SortField("value", SortField.Type.STRING);
			if(!"".equals(zipCodeSearch.getValue())) {
				TermQuery query = new TermQuery(new Term("value", zipCodeSearch.getValue().toString().toLowerCase()));
				 states = searchService.documents(Zip.class, query, sortField);
			} else{
				states = searchService.documents(Zip.class, new MatchAllDocsQuery(), sortField);
			}
			if(states != null) {
				zipTable.removeAllItems();
				for(Document d : states) {
					zipTable.addDocumentToContainer(d);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public ZipPanel() {
		setSizeFull();
		setSpacing(true);
		addComponent(getZipSearch());

		zipTable = new ZipTable();
		addComponent(zipTable);
		zipTable.addListener(new ProductTable.ValueChangeListener() {
            private static final long serialVersionUID = 2537403500201812771L;
			public void valueChange(ValueChangeEvent event) {
				Long id = (Long)zipTable.getValue();
				try {
					BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
					CrudService service = app.getBean("CrudService", CrudService.class);
					SingleThread st = new SingleThread();
					if (id != null){
						Zip obj = (Zip)st.find(Zip.class, id, service);
						zipForm.setObjectToForm(obj);
						cityPanel.setZip(obj);
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
            }
        });
		
		TabSheet tabs = new TabSheet();
		addComponent(tabs);
		tabs.setSizeFull();
		zipForm = new ZipForm(zipTable);
		tabs.addTab(zipForm, "Zip", null);
		cityPanel = new CityPanel();
		tabs.addTab(cityPanel, "Cities", null);
		
		
	}
	
}
