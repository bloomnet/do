package com.flowers.client.products;

import java.util.List;
import java.util.Set;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.server.LookupException;
import com.flowers.server.entity.Listing;
import com.flowers.server.entity.ProductCodification;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.ShopProductCodificationXref;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.Notification;

public class CodifiedProductPanel extends VerticalLayout {
	private static final long serialVersionUID = -5843785074589482357L;
	private CodifiedProductTable table;
	private Shop parentShop;
	private Button add;
	private Button edit;
	private ProductCodificationSubWindow subWindow;
	
	public CodifiedProductPanel() {
		setSizeFull();
		setSpacing(true);
		
		table = new CodifiedProductTable();
		addComponent(table);
		add = new Button("Add");
		add.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = 8744032684657430261L;

			@Override
			public void buttonClick(Button.ClickEvent event) {
			if(parentShop == null || parentShop.getShopCode() == null) getApplication().getMainWindow().showNotification("No Shop Selected.", Notification.POSITION_CENTERED);
			else showSubWindow();
			}
		});
		edit = new Button("Remove");
		edit.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = -2359692724000378336L;

			@SuppressWarnings("unchecked")
			public void buttonClick(Button.ClickEvent event) {
				if (((Set<ShopProductCodificationXref>)table.getValue()).toArray().length > 0){
					try {
						Set<ShopProductCodificationXref> selectedProducts = (Set<ShopProductCodificationXref>) table.getValue();
						SingleThread st = new SingleThread();
						
						for(int ii=0; ii<selectedProducts.toArray().length; ++ii){
							Long id = (Long)selectedProducts.toArray()[ii];
							CrudService service = ((BloomNetAdminApplication)getApplication()).getBean("CrudService", CrudService.class);
							SearchService searchService = ((BloomNetAdminApplication)getApplication()).getBean("SearchService", SearchService.class);
							ShopProductCodificationXref product = (ShopProductCodificationXref)st.find(ShopProductCodificationXref.class, id, service);
							product.getShop().getShopProductCodificationXrefs().remove(product);
							if(product != null) {
								st.remove(product, service);
								table.removeItem(id);
							}
							for(Listing listing : product.getShop().getListings()) {
								st.index(Listing.class, listing.getId(), searchService);
							}
							st.index(product.getShop().getClass(), product.getShop().getId(), searchService);
						}
						
					} catch (LookupException e) {
						e.printStackTrace();
					}
				}else{
					getApplication().getMainWindow().showNotification("No Product selected to remove.", Notification.POSITION_CENTERED);
				}
			}
		});
		HorizontalLayout footer = new HorizontalLayout();
		footer.addComponent(add);
		footer.addComponent(edit);
		addComponent(footer);
		subWindow = new ProductCodificationSubWindow(this);
	}
	public void attach() {
		super.attach();
	}
	@SuppressWarnings("unchecked")
	protected void showSubWindow(){
		try {
			CrudService service = ((BloomNetAdminApplication)getApplication()).getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			List<ProductCodification> products = (List<ProductCodification>)st.findAll("ProductCodification", service);
			for(ShopProductCodificationXref ref : parentShop.getShopProductCodificationXrefs()) {
				products.remove(ref.getProductCodification());
			}
			subWindow.setShop(parentShop);
			subWindow.setProducts(products);
		} catch (LookupException e) {
			e.printStackTrace();
		}
		getApplication().getMainWindow().addWindow(subWindow);		
	}
	public void setShop(Shop shop) {
		parentShop = shop;
		table.removeAllItems();
		for(ShopProductCodificationXref ref : shop.getShopProductCodificationXrefs()) {
			table.addItemToContainer(ref);
		}
	}
	public void addShopProductCodificationXref(ShopProductCodificationXref ref) {
		table.addItemToContainer(ref);
	}
}
