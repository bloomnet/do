package com.flowers.client.products;

import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.BooleanQuery.Builder;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.SortField;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.server.entity.ProductCategory;
import com.flowers.server.entity.ProductCodification;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class ProductPanel extends VerticalLayout {
	private static final long serialVersionUID = -6123729574360668207L;
	private ProductTable productTable;
	private ProductForm productForm;
	private TextField productName;
	private Button searchButton;
	
	public HorizontalLayout getProductSearch() {
		HorizontalLayout productSearchLayout = new HorizontalLayout();
		productSearchLayout.setSpacing(true);
		productSearchLayout.setHeight("40px");
		
		productName = new TextField("Product Name");
		productSearchLayout.addComponent(productName);
		productSearchLayout.setComponentAlignment(productName, Alignment.MIDDLE_LEFT);
		
		searchButton = new Button("Search");
		productSearchLayout.addComponent(searchButton);
		productSearchLayout.setComponentAlignment(searchButton, Alignment.BOTTOM_LEFT);
		searchButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				ProductCodification product = new ProductCodification();
				productForm.setObjectToForm(product);
				productForm.editButton.setVisible(false);
				productForm.removeButton.setVisible(false);
				search();
			}
		});
		final Button indexButton = new Button("Index");
		productSearchLayout.addComponent(indexButton);
		productSearchLayout.setComponentAlignment(indexButton, Alignment.BOTTOM_LEFT);
		indexButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
				SearchService searchService = app.getBean("SearchService", SearchService.class);
				indexButton.setEnabled(false);
				try {
					SingleThread st = new SingleThread();
					st.index(ProductCodification.class, searchService, true);
				} catch(Exception e) {
					e.printStackTrace();
				}
				try {
					SingleThread st = new SingleThread();
					st.index(ProductCategory.class, searchService,true);
					indexButton.setEnabled(true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		return productSearchLayout;
	}

	protected void search(){
		try {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			SearchService searchService = app.getBean("SearchService", SearchService.class);
			List<Document> products = null;
			SortField sortField = new SortField("name", SortField.Type.STRING);
			if(!"".equals(productName.getValue())) {
				Builder bq = new BooleanQuery.Builder();
				org.apache.lucene.search.PhraseQuery.Builder query = new PhraseQuery.Builder();
				
				String[] searchTerm = productName.getValue().toString().toLowerCase().split(" ");
				
				for(String word : searchTerm){
					query.add(new Term("tokenizedName",word));
				}
				bq.add(query.build(), BooleanClause.Occur.MUST);
				products = searchService.documents(ProductCodification.class, query.build(),
						sortField);
			} else{
				products = searchService.documents(ProductCodification.class, new MatchAllDocsQuery(), sortField);
			}
			if(products != null) {
				productTable.removeAllItems();
				for(Document d : products) {
					productTable.addDocumentToContainer(d);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public ProductPanel() {
		setSizeFull();
		setSpacing(true);
		addComponent(getProductSearch());

		productTable = new ProductTable();
		addComponent(productTable);
		productTable.addListener(new ProductTable.ValueChangeListener() {
            private static final long serialVersionUID = 2537403500201812771L;
			public void valueChange(ValueChangeEvent event) {
				Long id = (Long)productTable.getValue();
				try {
					if (id != null){
						BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
						CrudService service = app.getBean("CrudService", CrudService.class);
						SingleThread st = new SingleThread();
						ProductCodification obj = (ProductCodification)st.find(ProductCodification.class, id, service);
						productForm.setObjectToForm(obj);
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
            }
        });
		
		productForm = new ProductForm(productTable);
		addComponent(productForm);
	}
}
