package com.flowers.client.products;

import javax.persistence.PersistenceException;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.ui.base.BaseForm;
import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.ProductCodification;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window.Notification;

public class ProductForm extends BaseForm<ProductCodification> {
	private static final long serialVersionUID = -8318266858824205894L;
	private static final String COMMON_FIELD_WIDTH = "25em";
	public static final String[] VISIBLE_FIELDS_ORDER = new String[]{"productCodification","name","description","color","material","unitMeasures","flowersArrangement","unitPrice","bloomnetUrl"};
	
	public ProductForm(TableContainer<ProductCodification> tableContainer) {
		super(tableContainer, VISIBLE_FIELDS_ORDER);
	}
	
	
	@Override
	public ProductCodification getBlankObject() {
		return new ProductCodification();
	}

	@Override
	public ProductCodification insert(ProductCodification t) throws PersistenceException {
		try{
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			st.persist(t, service);
		}catch(Exception ee){
		}
		return t;
	}

	@Override
	public ProductCodification update(ProductCodification t) throws PersistenceException {
		try{
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			st.merge(t, service);
		}catch(Exception ee){
		}
		return t;
	}

	@Override
	public Object remove(ProductCodification t) throws PersistenceException {
		try{
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			st.remove(t, service);
		}catch(Exception ee){
		}
		return t;
	}

	@Override
	public FormFieldFactory getFormFieldFactory() {
		return new ProductCodificationFormFieldFactory();
	}
	
	public void saveButtonClickHandler(){
		if(getField("productCodification").getValue() == null ||
				getField("name").getValue() == null ||
				getField("description").getValue() == null ||
				getField("color").getValue() == null ||
				getField("material").getValue() == null ||
				getField("unitMeasures").getValue() == null ||
				getField("flowersArrangement").getValue() == null ||
				getField("unitPrice").getValue() == null ||
				getField("bloomnetUrl").getValue() == null){
			getApplication().getMainWindow().showNotification("Please Fill Out All Necessary Fields.", Notification.POSITION_CENTERED);
		}else{
			super.saveButtonClickHandler();
		}
	}

	private class ProductCodificationFormFieldFactory extends DefaultFieldFactory{
		private static final long serialVersionUID = 6090837022898959344L;

		@Override
		public Field createField(Item item, Object propertyId,	Component uiContext) {
			String pid = (String)propertyId;
			Field field = super.createField(item, propertyId, uiContext);
			if(field instanceof TextField) {
				TextField txt = (TextField)field;
				if ("productCodification".equals(pid)){
					txt.setWidth("3em");
					txt.setMaxLength(1);
				}else if ("name".equals(pid)){
					txt.setMaxLength(50);
					txt.setWidth(COMMON_FIELD_WIDTH);
				}else if ("description".equals(pid)){
					txt.setMaxLength(100);
					txt.setWidth(COMMON_FIELD_WIDTH);
				}else if ("color".equals(pid)){
					txt.setMaxLength(30);
					txt.setWidth(COMMON_FIELD_WIDTH);
				}else if ("material".equals(pid)){
					txt.setMaxLength(50);
					txt.setWidth(COMMON_FIELD_WIDTH);
				}else if ("unitMeasures".equals(pid)){
					txt.setMaxLength(100);
					txt.setWidth(COMMON_FIELD_WIDTH);
				}else if ("flowersArrangement".equals(pid)){
					txt.setMaxLength(100);
					txt.setWidth(COMMON_FIELD_WIDTH);
				}else if ("unitPrice".equals(pid)){
					txt.setWidth(COMMON_FIELD_WIDTH);
				}else if ("bloomnetUrl".equals(pid)){
					txt.setMaxLength(200);
					txt.setWidth(COMMON_FIELD_WIDTH);
				}
				txt.setNullRepresentation("");
			}
			return field;
		}
	}
}
