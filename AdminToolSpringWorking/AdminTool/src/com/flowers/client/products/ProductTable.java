package com.flowers.client.products;

import org.apache.lucene.document.Document;

import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.ProductCodification;
import com.vaadin.data.Item;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Table;

public class ProductTable extends Table implements TableContainer<ProductCodification> {
	private static final long serialVersionUID = -7514027152422177475L;

	public ProductTable() {
		addContainerProperty("code", String.class, null);
		addContainerProperty("name", String.class, null);		
		addContainerProperty("url", String.class, null);		
		setSizeFull();
		setHeight(200, AbstractComponent.UNITS_PIXELS);
		setSelectable(true);
		setImmediate(true);
		setNullSelectionAllowed(false);
	}
	
	@Override
	public Item addItemToContainer(ProductCodification t) {
		Item item = this.addItem(t.getId());
		if(item != null) {
			item.getItemProperty("code").setValue(t.getProductCodification());
			item.getItemProperty("name").setValue(t.getName());
			item.getItemProperty("url").setValue(t.getBloomnetUrl());
		}
		return item;
	}

	@Override
	public Item addDocumentToContainer(Document d) {
		Long id = Long.valueOf(d.get("id"));
		Item item = this.addItem(id);
		if(item != null) {
			item.getItemProperty("code").setValue(d.get("code"));
			item.getItemProperty("name").setValue(d.get("name"));
			item.getItemProperty("url").setValue(d.get("bloomnet_url"));
		}
		return item;
	}

	@Override
	public void selectRow(ProductCodification t) {
		this.setValue(t.getId());
	}

}
