package com.flowers.client.products;

import org.apache.lucene.document.Document;

import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.ProductCodification;
import com.flowers.server.entity.ShopProductCodificationXref;
import com.vaadin.data.Item;
import com.vaadin.ui.Table;

public class CodifiedProductTable extends Table implements TableContainer<ShopProductCodificationXref> {
	private static final long serialVersionUID = 1822489577089007961L;

	public CodifiedProductTable() {
		addContainerProperty("name", String.class, null);
		setSizeFull();
		setSelectable(true);
		setImmediate(true);
		setMultiSelect(true);
		setNullSelectionAllowed(false);
	}
	@Override
	public Item addItemToContainer(ShopProductCodificationXref t) {
		Item item = this.addItem(t.getId());
		if(item != null) {
			item.getItemProperty("name").setValue(t.getProductCodification().getName());
		}
		return item;
	}
	public Item addItemToContainer(ProductCodification t) {
		Item item = this.addItem(t.getId());
		if(item != null) {
			item.getItemProperty("name").setValue(t.getName());
		}
		return item;
	}
	@Override
	public void selectRow(ShopProductCodificationXref t) {
		this.setValue(t.getId());
	}

	@Override
	public Item addDocumentToContainer(Document d) {
		throw new RuntimeException("Not implemented on " + this.getClass().getName());
	}
}
