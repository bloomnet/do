package com.flowers.client.products;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.server.LookupException;
import com.flowers.server.entity.Listing;
import com.flowers.server.entity.ProductCodification;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.ShopProductCodificationXref;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Item;
import com.vaadin.ui.Button;
import com.vaadin.ui.Window;

public class ProductCodificationSubWindow extends Window {
	private static final long serialVersionUID = -8502250579656487437L;
	private CodifiedProductTable table2;
	private Shop shop;
	private CodifiedProductPanel panel;
	
	public ProductCodificationSubWindow(CodifiedProductPanel panel) {
		super("Product Codification");
		this.panel = panel;
		getContent().setWidth("400px");
		getContent().setHeight("300px");
		setModal(true);
		table2 = new CodifiedProductTable();
		addComponent(table2);
		table2.setHeight("240px");
		Button add = new Button("Add");
		addComponent(add);
		add.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = 8744032684657430261L;

			@Override
			public void buttonClick(Button.ClickEvent event) {
				@SuppressWarnings("unchecked")
				Set<ShopProductCodificationXref> selectedProducts = (Set<ShopProductCodificationXref>) table2.getValue();
				
				if(selectedProducts.toArray().length < 1) {	
					
					getApplication().getMainWindow().showNotification("No Codified Product Selected.", Notification.POSITION_CENTERED);
				
				}else{
					
					for(int ii=0; ii<selectedProducts.toArray().length; ++ii){
						Long id = (Long)selectedProducts.toArray()[ii];
						addProductToShop(id);
						table2.removeItem(id);
					}
				}
			}
		});
	}
	public void addProductToShop(long id) {
		try {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			ProductCodification product = (ProductCodification)st.find(ProductCodification.class, id, service);
			ShopProductCodificationXref ref = new  ShopProductCodificationXref();
			ref.setProductCodification(product);
			ref.setShop(shop);
			st.persist(ref, service);
			shop.getShopProductCodificationXrefs().add(ref);
			panel.addShopProductCodificationXref(ref);
			try{
				SearchService searchService =app.getBean("SearchService", SearchService.class);
				st.index(shop.getClass(), shop.getId(), searchService);
				List<Long> indexList = new ArrayList<Long>();
				for(Listing listing : shop.getListings()) {
					indexList.add(listing.getId());
				}
				st.index(Listing.class, indexList, searchService);
			}catch(Exception ee){
				ee.printStackTrace();
			}
		} catch (LookupException e) {
			e.printStackTrace();
		}
	}
	public void setShop(Shop shop) {
		this.shop = shop;
	}
	public void setProducts(List<ProductCodification> products) {
		for(ProductCodification product : products) {
			Item item = table2.addItem(product.getId());
			if(item != null) {
				item.getItemProperty("name").setValue(product.getName());
			}
		}
	}

	@Override
	public void close() {
		super.close();
	}
}
