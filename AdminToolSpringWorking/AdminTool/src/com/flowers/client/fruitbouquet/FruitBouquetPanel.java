package com.flowers.client.fruitbouquet;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.server.entity.FruitBouquet;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.flowers.server.util.SQLData;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.terminal.FileResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class FruitBouquetPanel extends VerticalLayout {
	private static final long serialVersionUID = -6123729574360668207L;
	private FruitBouquetTable fruitBouquetTable;
	private FruitBouquetForm fruitBouquetForm;
	private TextField productCode;
	private TextField startDate;
	private TextField endDate;
	private Button searchButton;
	private Button reportButton;
	
	public HorizontalLayout getFruitBouquetSearch() {
		
		Date today = new Date();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String date = sdf.format(today);
		
		HorizontalLayout fruitBouquetLayout = new HorizontalLayout();
		fruitBouquetLayout.setSpacing(true);
		fruitBouquetLayout.setHeight("40px");
		
		productCode = new TextField("Product Code");
		startDate = new TextField("Start Date");
		endDate = new TextField("End Date");
		startDate.setValue(date);
		endDate.setValue(date);
		
		fruitBouquetLayout.addComponent(productCode);
		fruitBouquetLayout.setComponentAlignment(productCode, Alignment.MIDDLE_LEFT);
		
		searchButton = new Button("Search");
		fruitBouquetLayout.addComponent(searchButton);
		fruitBouquetLayout.setComponentAlignment(searchButton, Alignment.BOTTOM_LEFT);
		searchButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				FruitBouquet fruitBouquet = new FruitBouquet();
				fruitBouquetForm.setObjectToForm(fruitBouquet);
				fruitBouquetForm.editButton.setVisible(false);
				fruitBouquetForm.removeButton.setVisible(false);
				search();
			}
		});
		final Button indexButton = new Button("Index");
		fruitBouquetLayout.addComponent(indexButton);
		fruitBouquetLayout.setComponentAlignment(indexButton, Alignment.BOTTOM_LEFT);
		indexButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
				SearchService searchService = app.getBean("SearchService", SearchService.class);
				indexButton.setEnabled(false);
				try {
					SingleThread st = new SingleThread();
					st.index(FruitBouquet.class, searchService,true);
					indexButton.setEnabled(true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		fruitBouquetLayout.addComponent(startDate);
		fruitBouquetLayout.setComponentAlignment(startDate, Alignment.BOTTOM_LEFT);
		fruitBouquetLayout.addComponent(endDate);
		fruitBouquetLayout.setComponentAlignment(endDate, Alignment.BOTTOM_CENTER);
		reportButton = new Button("Generate Report");
		fruitBouquetLayout.addComponent(reportButton);
		fruitBouquetLayout.setComponentAlignment(reportButton, Alignment.BOTTOM_RIGHT);
		reportButton.addListener(new ClickListener() {
			private static final long serialVersionUID = -6074662345875852514L;

			public void buttonClick(ClickEvent event) {
				try {
					String date = String.valueOf(new Date().getTime());
					File outFile = new File("/var/lib/tomcat/webapps/fruitBouquetData/fruitBouquetOrders"+date+".csv");
					Writer out = new BufferedWriter(new FileWriter(outFile));
					String start = startDate.getValue().toString();
					String end = endDate.getValue().toString();
				
					out.write("OrderNumber,ProductCode,Quantity,Description,Price\n");
				
					SQLData mySQL = new SQLData();
					String query = "SELECT op.tracking_number, op.product_code, op.quantity, op.description, op.price "
							+ "FROM processed_order po INNER JOIN order_products op ON po.tracking_number = op.tracking_number "
							+ "WHERE op.tracking_number != 0 AND op.product_code IN (SELECT product_code FROM fruitbouquet)"
							+ " AND po.created_date >= '" +start+"' AND po.created_date <= '" +end+"';";
					ResultSet results = mySQL.executeQuery(query);
					System.out.println(query);
					System.out.println(results);
					while(results.next()){
						out.write(results.getString("tracking_number")+","+results.getString("product_code")+","+results.getString("quantity")+","+results.getString("description")+","+results.getString("price")+"\n");
					}
					
					mySQL.closeStatement();
					
					out.flush();
					out.close();
					
					FileResource fileResource = new FileResource(outFile, (BloomNetAdminApplication)getApplication());						
					getWindow().open(fileResource);
				
				} catch (IOException e) {
					e.printStackTrace();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
		});
		
		return fruitBouquetLayout;
	}

	protected void search(){
		try {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			SearchService searchService = app.getBean("SearchService", SearchService.class);
			List<Document> fruitBouquetProducts = null;
			SortField sortField = new SortField("product_code", SortField.Type.STRING);
			if(!"".equals(productCode.getValue())) {
				
				String searchTerm = productCode.getValue().toString().toUpperCase();
				
				TermQuery query = new TermQuery(new Term("product_code", searchTerm));
				
				fruitBouquetProducts = searchService.documents(FruitBouquet.class, query,
						sortField);
			} else{
				fruitBouquetProducts = searchService.documents(FruitBouquet.class, new MatchAllDocsQuery(), sortField);
			}
			if(fruitBouquetProducts != null) {
				fruitBouquetTable.removeAllItems();
				for(Document d : fruitBouquetProducts) {
					fruitBouquetTable.addDocumentToContainer(d);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public FruitBouquetPanel() {
		setSizeFull();
		setSpacing(true);
		addComponent(getFruitBouquetSearch());

		fruitBouquetTable = new FruitBouquetTable();
		addComponent(fruitBouquetTable);
		fruitBouquetTable.addListener(new FruitBouquetTable.ValueChangeListener() {
            private static final long serialVersionUID = 2537403500201812771L;
			public void valueChange(ValueChangeEvent event) {
				Long id = (Long)fruitBouquetTable.getValue();
				try {
					if (id != null){
						BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
						CrudService service = app.getBean("CrudService", CrudService.class);
						SingleThread st = new SingleThread();
						FruitBouquet obj = (FruitBouquet)st.find(FruitBouquet.class, id, service);
						fruitBouquetForm.setObjectToForm(obj);
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
            }
        });
		
		fruitBouquetForm = new FruitBouquetForm(fruitBouquetTable);
		addComponent(fruitBouquetForm);
	}
}
