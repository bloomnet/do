package com.flowers.client.fruitbouquet;

import javax.persistence.PersistenceException;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.ui.base.BaseForm;
import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.FruitBouquet;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window.Notification;

public class FruitBouquetForm extends BaseForm<FruitBouquet> {
	private static final long serialVersionUID = -8318266858874205894L;
	private static final String COMMON_FIELD_WIDTH = "25em";
	public static final String[] VISIBLE_FIELDS_ORDER = new String[]{"productCode","productDescription","price"};
	
	public FruitBouquetForm(TableContainer<FruitBouquet> tableContainer) {
		super(tableContainer, VISIBLE_FIELDS_ORDER);
	}
	
	
	@Override
	public FruitBouquet getBlankObject() {
		return new FruitBouquet();
	}

	@Override
	public FruitBouquet insert(FruitBouquet t) throws PersistenceException {
		try{
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			st.persist(t, service);
			st.index(FruitBouquet.class, t.getId(), service);
		}catch(Exception ee){
		}
		return t;
	}

	@Override
	public FruitBouquet update(FruitBouquet t) throws PersistenceException {
		try{
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			st.merge(t, service);
			st.index(FruitBouquet.class, t.getId(), service);
		}catch(Exception ee){
		}
		return t;
	}

	@Override
	public Object remove(FruitBouquet t) throws PersistenceException {
		try{
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			st.remove(t, service);
		}catch(Exception ee){
		}
		return t;
	}

	@Override
	public FormFieldFactory getFormFieldFactory() {
		return new FruitBouquetFormFieldFactory();
	}
	
	public void saveButtonClickHandler(){
		if(getField("productCode").getValue() == null ||
				getField("productDescription").getValue() == null ||
				getField("price").getValue() == null){
			getApplication().getMainWindow().showNotification("Please Fill Out All Necessary Fields.", Notification.POSITION_CENTERED);
		}else{
			getField("productCode").setValue(getField("productCode").getValue().toString().toUpperCase());
			super.saveButtonClickHandler();
		}
	}

	private class FruitBouquetFormFieldFactory extends DefaultFieldFactory{
		private static final long serialVersionUID = 6090837022898959344L;

		@Override
		public Field createField(Item item, Object propertyId,	Component uiContext) {
			String pid = (String)propertyId;
			Field field = super.createField(item, propertyId, uiContext);
			if(field instanceof TextField) {
				TextField txt = (TextField)field;
				if ("productCode".equals(pid)){
					txt.setWidth(COMMON_FIELD_WIDTH);
					txt.setMaxLength(10);
				}else if ("productDescription".equals(pid)){
					txt.setMaxLength(2000);
					txt.setWidth(COMMON_FIELD_WIDTH);
				}else if ("price".equals(pid)){
					txt.setMaxLength(10);
					txt.setWidth(COMMON_FIELD_WIDTH);
				}
				txt.setNullRepresentation("");
			}
			return field;
		}
	}
}
