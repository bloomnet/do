package com.flowers.client.states;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.ui.base.BaseForm;
import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.State;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Item;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window.Notification;

public class StateForm extends BaseForm<State>{
	private static final long serialVersionUID = -5771074228250157806L;
	private static final String COMMON_FIELD_WIDTH = "25em";
	private static final String SMALL_FIELD_WIDTH = "12em";
	public static final String[] VISIBLE_FIELDS_ORDER = new String[]{"country", "name", "shortName"};
	private ComboBox country;
	
	public StateForm(TableContainer<State> tableContainer) {
		super(tableContainer, VISIBLE_FIELDS_ORDER);
	}
	
	@Override
	public State getBlankObject() {
		return new State();
	}

	@Override
	public State insert(State t) throws PersistenceException {
		try{
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			st.persist(t, service);
		}catch(Exception ee){}
		return t;
	}

	@Override
	public State update(State t) throws PersistenceException {
		try{
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			st.merge(t, service);
		}catch(Exception ee){}
		return t;
	}

	@Override
	public Object remove(State t) throws PersistenceException {
		try{
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			st.remove(t, service);
		}catch(Exception ee){}
		return t.getId();
	}
	
	@Override
	public FormFieldFactory getFormFieldFactory() {
		return new StateFormFieldFactory();
	}
	
	public void saveButtonClickHandler(){
		if(getField("country").getValue() == null ||
				getField("name").getValue() == null ||
				getField("shortName").getValue() == null){
			getApplication().getMainWindow().showNotification("Please Fill Out All Necessary Fields.", Notification.POSITION_CENTERED);
		}else{
			super.saveButtonClickHandler();
		}
	}

	private class StateFormFieldFactory extends DefaultFieldFactory{
		private static final long serialVersionUID = -7007101992383077814L;

		@Override
		public Field createField(Item item, Object propertyId,	Component uiContext) {
			String pid = (String)propertyId;
			if ("country".equals(pid)){
				return getCountry();
			}else{
				Field field = super.createField(item, propertyId, uiContext);
				if(field instanceof TextField){
					TextField txt = (TextField)field;
					if ("shortName".equals(pid)){
						txt.setMaxLength(2);
						txt.setWidth(SMALL_FIELD_WIDTH);
					}else if ("name".equals(pid)){
						txt.setMaxLength(50);
						txt.setWidth(COMMON_FIELD_WIDTH);
					}
					txt.setNullRepresentation("");
				}
				return field;
			}
		}
	}	
	
	public ComboBox getCountry() {
		country = new ComboBox();
		country.setWidth(SMALL_FIELD_WIDTH);
		country.setCaption("Country");
		country.setNewItemsAllowed(true);
		country.setNullSelectionAllowed(false);
		List<Object> countries = getCountries();
		if (countries != null){
			for (Object o:countries){
				country.addItem(o);
			}
		}
		return country;
	}
	
	@SuppressWarnings("unchecked")
	protected List<Object> getCountries(){
		try{
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			return (List<Object>) st.findResults("from Country", service, 0);
		}catch(Exception ee){}
		return new ArrayList<Object>();
	}
	
}
