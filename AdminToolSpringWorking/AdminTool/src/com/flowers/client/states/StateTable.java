package com.flowers.client.states;

import org.apache.lucene.document.Document;

import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.State;
import com.vaadin.data.Item;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Table;

public class StateTable extends Table implements TableContainer<State> {
	private static final long serialVersionUID = -3805372188477465701L;

	public StateTable() {
		addContainerProperty("name", String.class, null);
		addContainerProperty("country", String.class, null);		
		setSizeFull();
		setHeight(200, AbstractComponent.UNITS_PIXELS);
		setSelectable(true);
		setImmediate(true);
		setNullSelectionAllowed(false);
	}
	
	@Override
	public Item addItemToContainer(State t) {
		Item item = this.addItem(t.getId());
		if(item != null) {
			item.getItemProperty("name").setValue(t.getName());
			item.getItemProperty("country").setValue(t.getCountry().getName());
		}
		return item;
	}

	@Override
	public void selectRow(State t) {
		this.setValue(t.getId());
	}

	@Override
	public Item addDocumentToContainer(Document d) {
		Long id = Long.valueOf(d.get("id"));
		Item item = this.addItem(id);
		if(item != null) {
			item.getItemProperty("name").setValue(d.get("name"));
			item.getItemProperty("country").setValue(d.get("country.name"));
		}
		return item;
	}
}
