package com.flowers.client.states;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.SortField;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.facilities.FacilityTable;
import com.flowers.server.entity.State;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class StatePanel extends VerticalLayout {
	private static final long serialVersionUID = 5201287159136748395L;
	private StateTable stateTable;
	private StateForm stateForm;
	private TextField stateName;
	private Button searchButton;
	
	public HorizontalLayout getStateSearch() {
		HorizontalLayout stateSearchLayout = new HorizontalLayout();
		stateSearchLayout.setSpacing(true);
		stateSearchLayout.setHeight("40px");
		
		stateName = new TextField("State Name");
		stateSearchLayout.addComponent(stateName);
		stateSearchLayout.setComponentAlignment(stateName, Alignment.MIDDLE_LEFT);
		
		searchButton = new Button("Search");
		stateSearchLayout.addComponent(searchButton);
		stateSearchLayout.setComponentAlignment(searchButton, Alignment.BOTTOM_LEFT);
		searchButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				State state = new State();
				stateForm.setObjectToForm(state);
				stateForm.editButton.setVisible(false);
				stateForm.removeButton.setVisible(false);
				search();
			}
		});
		Button indexButton = new Button("Index");
		stateSearchLayout.addComponent(indexButton);
		stateSearchLayout.setComponentAlignment(indexButton, Alignment.BOTTOM_LEFT);
		indexButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
				SearchService searchService = app.getBean("SearchService", SearchService.class);
				SingleThread st = new SingleThread();
				try {
					st.index(State.class, searchService,true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
		return stateSearchLayout;
	}
	
	@SuppressWarnings("unchecked")
	protected void searchOld(){
		try {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			List<Object> cities = null;
			if(!"".equals(stateName.getValue())) {
				StringBuilder pql = new StringBuilder("from State s where s.name like :stateName");
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("stateName", "%"+stateName.getValue()+"%");
				cities = (List<Object>) st.findResults(pql.toString(), service, params);
			} else{
				cities = (List<Object>) st.findResults("from State", service, new HashMap<String,Object>());
			}
			if(cities != null) {
				stateTable.removeAllItems();
				for(Object facilityObj : cities) {
					State state = (State)facilityObj;
					stateTable.addItemToContainer(state);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void search(){
		try {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			SearchService searchService = app.getBean("SearchService", SearchService.class);
			List<Document> states = null;
			SortField sortField = new SortField("name", SortField.Type.STRING);
			if(!"".equals(stateName.getValue())) {
				org.apache.lucene.search.PhraseQuery.Builder query = new PhraseQuery.Builder();
				
				String[] searchTerm = stateName.getValue().toString().toLowerCase().split(" ");
				
				for(String word : searchTerm){
					query.add(new Term("tokenizedName",word));
				}
				states = searchService.documents(State.class, query.build(),
						sortField);
			} else{
				states = searchService.documents(State.class, new MatchAllDocsQuery(), sortField);
			}
			if(states != null) {
				stateTable.removeAllItems();
				for(Document d : states) {
					stateTable.addDocumentToContainer(d);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public StatePanel() {
		setSizeFull();
		setSpacing(true);
		addComponent(getStateSearch());

		stateTable = new StateTable();
		addComponent(stateTable);
		stateTable.addListener(new FacilityTable.ValueChangeListener() {
            private static final long serialVersionUID = 2537403500201812771L;
			public void valueChange(ValueChangeEvent event) {
				Long id = (Long)stateTable.getValue();
				try {
					if (id != null){
						BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
						CrudService service = app.getBean("CrudService", CrudService.class);
						SingleThread st = new SingleThread();
						State obj = (State)st.find(State.class, id, service);
						stateForm.setObjectToForm(obj);
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
            }
        });
		
		stateForm = new StateForm(stateTable);
		addComponent(stateForm);
		
	}
}
