package com.flowers.client.consumerlandingad;

import javax.persistence.PersistenceException;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.ui.base.BaseForm;
import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.ConsumerLandingAd;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window.Notification;

public class ConsumerLandingAdForm extends BaseForm<ConsumerLandingAd> {

	private static final long serialVersionUID = -1609034181956013244L;
	private static final String COMMON_FIELD_WIDTH = "25em";
	public static final String[] VISIBLE_FIELDS_ORDER = new String[]{"fileName", "website", "adShape"};
	
	public ConsumerLandingAdForm(TableContainer<ConsumerLandingAd> tableContainer) {
		super(tableContainer, VISIBLE_FIELDS_ORDER);
	}
	
	
	@Override
	public ConsumerLandingAd getBlankObject() {
		return new ConsumerLandingAd();
	}

	@Override
	public ConsumerLandingAd insert(ConsumerLandingAd t) throws PersistenceException {
		try{
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			st.persist(t, service);
			st.index(ConsumerLandingAd.class, t.getId(), service);
		}catch(Exception ee){
		}
		return t;
	}

	@Override
	public ConsumerLandingAd update(ConsumerLandingAd t) throws PersistenceException {
		try{
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			st.merge(t, service);
			st.index(ConsumerLandingAd.class, t.getId(), service);
		}catch(Exception ee){
		}
		return t;
	}

	@Override
	public Object remove(ConsumerLandingAd t) throws PersistenceException {
		try{
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			st.remove(t, service);
		}catch(Exception ee){
		}
		return t;
	}

	@Override
	public FormFieldFactory getFormFieldFactory() {
		return new ConsumerLandingAdFieldFactory();
	}
	
	public void saveButtonClickHandler(){
		if(getField("fileName").getValue() == null || getField("website").getValue() == null || getField("adShape").getValue() == null){
			getApplication().getMainWindow().showNotification("Please Fill Out All Necessary Fields.", Notification.POSITION_CENTERED);
		}else{
			super.saveButtonClickHandler();
		}
	}

	private class ConsumerLandingAdFieldFactory extends DefaultFieldFactory{

		private static final long serialVersionUID = -3596248659949602235L;

		@Override
		public Field createField(Item item, Object propertyId,	Component uiContext) {
			String pid = (String)propertyId;
			Field field = super.createField(item, propertyId, uiContext);
			if(field instanceof TextField) {
				TextField txt = (TextField)field;
				if ("fileName".equals(pid)){
					txt.setWidth(COMMON_FIELD_WIDTH);
					txt.setMaxLength(255);
				}
				if ("website".equals(pid)){
					txt.setWidth(COMMON_FIELD_WIDTH);
					txt.setMaxLength(255);
				}
				if ("adShape".equals(pid)){
					txt.setWidth(COMMON_FIELD_WIDTH);
					txt.setMaxLength(1);
				}
				txt.setNullRepresentation("");
			}
			return field;
		}
	}
}
