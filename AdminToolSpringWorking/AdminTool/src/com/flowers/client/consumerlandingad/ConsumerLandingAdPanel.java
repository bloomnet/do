package com.flowers.client.consumerlandingad;

import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.server.entity.ConsumerLandingAd;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class ConsumerLandingAdPanel extends VerticalLayout {
	private static final long serialVersionUID = -6449339436955571727L;
	private ConsumerLandingAdTable consumerLandingAdTable;
	private ConsumerLandingAdForm consumerLandingAdForm;
	private TextField fileName;
	private Button searchButton;
	
	public HorizontalLayout getBannerAdSearch() {
		
		HorizontalLayout consumerLandingAdLayout = new HorizontalLayout();
		consumerLandingAdLayout.setSpacing(true);
		consumerLandingAdLayout.setHeight("40px");
		
		fileName = new TextField("File Name");
		
		consumerLandingAdLayout.addComponent(fileName);
		consumerLandingAdLayout.setComponentAlignment(fileName, Alignment.MIDDLE_LEFT);
		
		searchButton = new Button("Search");
		consumerLandingAdLayout.addComponent(searchButton);
		consumerLandingAdLayout.setComponentAlignment(searchButton, Alignment.BOTTOM_LEFT);
		searchButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				ConsumerLandingAd landingAd = new ConsumerLandingAd();
				consumerLandingAdForm.setObjectToForm(landingAd);
				consumerLandingAdForm.removeButton.setVisible(true);
				search();
			}
		});
		final Button indexButton = new Button("Index");
		consumerLandingAdLayout.addComponent(indexButton);
		consumerLandingAdLayout.setComponentAlignment(indexButton, Alignment.BOTTOM_LEFT);
		indexButton.addListener(new ClickListener() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -2876914040646907172L;

			public void buttonClick(ClickEvent event) {
				BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
				SearchService searchService = app.getBean("SearchService", SearchService.class);
				indexButton.setEnabled(false);
				try {
					SingleThread st = new SingleThread();
					st.index(ConsumerLandingAd.class, searchService,true);
					indexButton.setEnabled(true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
			
		return consumerLandingAdLayout;
	}

	protected void search(){
		try {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			SearchService searchService = app.getBean("SearchService", SearchService.class);
			List<Document> consumerLandingAds = null;
			SortField sortField = new SortField("file_name", SortField.Type.STRING);
			if(!"".equals(fileName.getValue())) {
				
				String searchTerm = fileName.getValue().toString().toUpperCase();
				
				TermQuery query = new TermQuery(new Term("file_name", searchTerm));
				
				consumerLandingAds = searchService.documents(ConsumerLandingAd.class, query,
						sortField);
			} else{
				consumerLandingAds = searchService.documents(ConsumerLandingAd.class, new MatchAllDocsQuery(), sortField);
			}
			if(consumerLandingAds != null) {
				consumerLandingAdTable.removeAllItems();
				for(Document d : consumerLandingAds) {
					consumerLandingAdTable.addDocumentToContainer(d);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public ConsumerLandingAdPanel() {
		setSizeFull();
		setSpacing(true);
		addComponent(getBannerAdSearch());

		consumerLandingAdTable = new ConsumerLandingAdTable();
		addComponent(consumerLandingAdTable);
		consumerLandingAdTable.addListener(new ConsumerLandingAdTable.ValueChangeListener() {
			private static final long serialVersionUID = -3922216275089187949L;

			public void valueChange(ValueChangeEvent event) {
				Long id = (Long)consumerLandingAdTable.getValue();
				try {
					if (id != null){
						BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
						CrudService service = app.getBean("CrudService", CrudService.class);
						SingleThread st = new SingleThread();
						ConsumerLandingAd obj = (ConsumerLandingAd)st.find(ConsumerLandingAd.class, id, service);
						consumerLandingAdForm.setObjectToForm(obj);
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
            }
        });
		
		consumerLandingAdForm = new ConsumerLandingAdForm(consumerLandingAdTable);
		addComponent(consumerLandingAdForm);
	}
}
