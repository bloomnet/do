package com.flowers.client.consumerlandingad;

import org.apache.lucene.document.Document;

import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.ConsumerLandingAd;
import com.vaadin.data.Item;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Table;

public class ConsumerLandingAdTable extends Table implements TableContainer<ConsumerLandingAd> {
	private static final long serialVersionUID = 6659565392894082290L;

	public ConsumerLandingAdTable() {
		addContainerProperty("fileName", String.class, null);
		addContainerProperty("website", String.class, null);
		addContainerProperty("adShape", String.class, null);
		setSizeFull();
		setHeight(200, AbstractComponent.UNITS_PIXELS);
		setSelectable(true);
		setImmediate(true);
		setNullSelectionAllowed(false);
	}
	
	@Override
	public Item addItemToContainer(ConsumerLandingAd t) {
		Item item = this.addItem(t.getId());
		if(item != null) {
			item.getItemProperty("fileName").setValue(t.getFileName());
			item.getItemProperty("website").setValue(t.getWebsite());
			item.getItemProperty("adShape").setValue(t.getAdShape());
		}
		return item;
	}

	@Override
	public Item addDocumentToContainer(Document d) {
		Long id = Long.valueOf(d.get("id"));
		Item item = this.addItem(id);
		if(item != null) {
			item.getItemProperty("fileName").setValue(d.get("file_name"));
			item.getItemProperty("website").setValue(d.get("website"));
			item.getItemProperty("adShape").setValue(d.get("adShape"));
		}
		return item;
	}

	@Override
	public void selectRow(ConsumerLandingAd t) {
		this.setValue(t.getId());
	}

}
