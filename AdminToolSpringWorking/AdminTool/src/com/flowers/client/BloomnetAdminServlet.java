package com.flowers.client;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import com.vaadin.Application;
import com.vaadin.terminal.gwt.server.AbstractApplicationServlet;

public class BloomnetAdminServlet extends AbstractApplicationServlet {
	private static final long serialVersionUID = 6657081328357686591L;

	@Override
	protected Application getNewApplication(HttpServletRequest request) 	throws ServletException {
		return new BloomNetAdminApplication();
	}

	@Override
	protected Class<? extends Application> getApplicationClass() throws ClassNotFoundException {
		return BloomNetAdminApplication.class;
	}

}
