package com.flowers.client.ziplistings;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.server.LookupException;
import com.flowers.server.entity.Listing;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.ZipListing;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.Notification;

public class ZipListingPanel extends VerticalLayout {
	private static final long serialVersionUID = 8998010082722235604L;
	private ZipListingTable zipListingTable;
	private Shop parentShop;
	private Button add;
	private Button edit;
	public ZipListingPanel() {
		zipListingTable = new ZipListingTable();
		addComponent(zipListingTable);
		add = new Button("Add");
		add.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = 42378592347986421L;

			@Override
			public void buttonClick(Button.ClickEvent event) {
				if(parentShop == null || parentShop.getShopCode() == null) getApplication().getMainWindow().showNotification("No Shop Selected.", Notification.POSITION_CENTERED);
				else showSubWindow(null, parentShop);
			}
		});
		
		edit = new Button("Activate/Deactivate");
		edit.addListener(new Button.ClickListener() {
			private static final long serialVersionUID = -2435782657267264235L;

			@SuppressWarnings("unchecked")
			public void buttonClick(Button.ClickEvent event) {
				if (((Set<ZipListing>)zipListingTable.getValue()).toArray().length > 0){
					try {
						SingleThread st = new SingleThread();
						Set<ZipListing> selectedZipListings = (Set<ZipListing>) zipListingTable.getValue();
						ZipListing zl = null;
						BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
						SearchService searchService = app.getBean("SearchService", SearchService.class);
						CrudService service = ((BloomNetAdminApplication)getApplication()).getBean("CrudService", CrudService.class);
						for(int ii=0; ii<selectedZipListings.size(); ++ii){
							Long id = (Long) selectedZipListings.toArray()[ii];
							zl = (ZipListing)st.find(ZipListing.class, id, service);
							if(zl.getActive() == 1)
								zl.setActive(0);
							else if(zl.getActive() == 0)
								zl.setActive(1);

							st.merge(zl, service);
							zipListingTable.getItem(id).getItemProperty("active").setValue(String.valueOf(zl.getActive()).replaceAll("0", "FALSE").replaceAll("1", "TRUE"));
						}
						List<Long> indexList = new ArrayList<Long>();
						for(Listing listing : zl.getShop().getListings()) {
							indexList.add(listing.getId());
						}
						st.index(Listing.class, indexList, searchService);
						st.index(Shop.class, zl.getShop().getId(), searchService);
					} catch (LookupException e) {
						e.printStackTrace();
					}
				}else{
					getApplication().getMainWindow().showNotification("No Zip Listing selected to Activate/Deactivate.", Notification.POSITION_CENTERED);
				}
			}
		});
		HorizontalLayout footer = new HorizontalLayout();
		footer.addComponent(add);
		footer.addComponent(edit);
		addComponent(footer);
		
	}
	public void attach() {
		super.attach();
	}
	protected void showSubWindow(ZipListing zl, Shop shop) {
		ZipListingSubWindow subWindow = new ZipListingSubWindow();
		subWindow.setModal(true);
		ZipListingForm zlf = new ZipListingForm(zipListingTable, (BloomNetAdminApplication)getApplication(), shop);
		zlf.setParentSubWindow(subWindow);
		subWindow.addComponent(zlf);
		getApplication().getMainWindow().addWindow(subWindow);
		if (zl == null){
			zlf.addButtonClickHandler();
		}else{
			zlf.setObjectToForm(zl);
			zlf.setReadOnly(false);
		}
	}
	public void setShop(Shop shop) {
		parentShop = shop;
		zipListingTable.removeAllItems();
		for(ZipListing zl : shop.getZipListings()) {
			zipListingTable.addItemToContainer(zl);
		}
	}
}
