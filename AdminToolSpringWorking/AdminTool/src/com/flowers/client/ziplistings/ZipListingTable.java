package com.flowers.client.ziplistings;

import org.apache.lucene.document.Document;

import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.ZipListing;
import com.vaadin.data.Item;
import com.vaadin.ui.Table;

public class ZipListingTable extends Table implements TableContainer<ZipListing> {
	private static final long serialVersionUID = -5287531644326627578L;

	public ZipListingTable() {
		addContainerProperty("zipCode", String.class, null);
		addContainerProperty("active", String.class, null);
		setSizeFull();
		setSelectable(true);
		setImmediate(true);
		setMultiSelect(true);
		setNullSelectionAllowed(false);
	}
	
	@Override
	public Item addItemToContainer(ZipListing t) {
		Item item = this.addItem(t.getId());
		if(item != null) {
			item.getItemProperty("zipCode").setValue(t.getZipCode());
			item.getItemProperty("active").setValue(String.valueOf(t.getActive()).replaceAll("0", "FALSE").replaceAll("1", "TRUE"));
		}
		return item;
	}

	@Override
	public void selectRow(ZipListing t) {
		this.setValue(t.getId());
	}

	@Override
	public Item addDocumentToContainer(Document d) {
		throw new RuntimeException("Not implemented on " + this.getClass().getName());
	}


}
