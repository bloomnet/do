package com.flowers.client.ziplistings;

import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class ZipListingSubWindow extends Window {
	private static final long serialVersionUID = 4202912110080784311L;

	public ZipListingSubWindow() {
		super("Zip Listings");
		VerticalLayout layout = (VerticalLayout) getContent();
        layout.setMargin(true);
        layout.setSpacing(true); 
        layout.setWidth("400px");
        layout.setHeight("400px");
		setModal(true);
	}
	
	@Override
	public void close() {
		super.close();
	}
}
