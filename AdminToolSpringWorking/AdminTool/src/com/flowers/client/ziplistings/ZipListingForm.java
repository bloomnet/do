package com.flowers.client.ziplistings;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.PersistenceException;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.ui.base.BaseForm;
import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.Listing;
import com.flowers.server.entity.Shop;
import com.flowers.server.entity.ZipListing;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;

public class ZipListingForm extends BaseForm<ZipListing>{
	private static final long serialVersionUID = -5107156580600837821L;
	private Shop parentShop;
	private Window parentSubWindow;
	
	public static final String[] visibleFieldOrder = new String[]{"zipCode", "active"};
	
	public ZipListingForm(TableContainer<ZipListing> tableContainer, BloomNetAdminApplication bloomNetApp, Shop parentShop) {
		super(tableContainer, visibleFieldOrder);
		this.parentShop = parentShop;
	}
	
	public void setParentSubWindow(Window parentSubWindow) {
		this.parentSubWindow = parentSubWindow;
	}
	
	@Override
	public void cancelButtonClickHandler() {
		discard();
		if (parentSubWindow != null && (parentSubWindow instanceof ZipListingSubWindow)){
			((ZipListingSubWindow)parentSubWindow).close();
		}
	}
	
	@Override
	public void saveButtonClickHandler(){
		super.saveButtonClickHandler();
		if (parentSubWindow != null && (parentSubWindow instanceof ZipListingSubWindow)){
			((ZipListingSubWindow)parentSubWindow).close();
		}
	}

	
	@Override
	public ZipListing getBlankObject() {
		return new ZipListing();
	}

	@Override
	public ZipListing insert(ZipListing t) throws PersistenceException {
		t.setActive(1);
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SingleThread st = new SingleThread();
		try {
			t.setShop(parentShop);
		} catch (Exception e) {
			e.printStackTrace();
		}
		st.persist(t, service);
		parentShop.getZipListings().add(t);
		SearchService searchService = app.getBean("SearchService", SearchService.class);
		st.index(t.getShop().getClass(), t.getShop().getId(), searchService);
		List<Long> indexList = new ArrayList<Long>();
		for(Listing listing : t.getShop().getListings()) {
			indexList.add(listing.getId());
		}
		st.index(Listing.class, indexList, searchService);
		st.index(Shop.class, t.getShop().getId(), searchService);
		return t;
	}

	@Override
	public ZipListing update(ZipListing t) throws PersistenceException {
		if(t.getActive() == 1)
			t.setActive(0);
		else if(t.getActive() == 0)
			t.setActive(1);
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SearchService searchService = app.getBean("SearchService", SearchService.class);
		SingleThread st = new SingleThread();
		st.merge(t, service);
		st.index(t.getShop().getClass(), t.getShop().getId(), searchService);
		List<Long> indexList = new ArrayList<Long>();
		for(Listing listing : t.getShop().getListings()) {
			indexList.add(listing.getId());
		}
		st.index(Listing.class, indexList, searchService);
		st.index(Shop.class, t.getShop().getId(), searchService);
		return t;
	}

	@Override
	public Object remove(ZipListing t) throws PersistenceException {
		BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
		CrudService service = app.getBean("CrudService", CrudService.class);
		SearchService searchService = app.getBean("SearchService", SearchService.class);
		SingleThread st = new SingleThread();
		Set<Listing> listings = t.getShop().getListings();
		st.remove(t, service);
		st.index(Shop.class, t.getShop().getId(), searchService);
		List<Long> indexList = new ArrayList<Long>();
		for(Listing listing : listings) {
			indexList.add(listing.getId());
		}
		st.index(Listing.class, indexList, searchService);
		st.index(Shop.class, t.getShop().getId(), searchService);
		return t.getId();
	}
	
	@Override
	public void setItemDataSource(Item newDataSource) {
		super.setItemDataSource(newDataSource);
		setReadOnly(false);
		handleButtons(false);
	}
	
	@Override
	public FormFieldFactory getFormFieldFactory() {
		return new ZipListingFieldFactory();
	}

	private static final String COMMON_FIELD_WIDTH = "25em";
	public class ZipListingFieldFactory extends DefaultFieldFactory {
		private static final long serialVersionUID = -432098532723496243L;
		
		public Field createField(Item item, Object propertyId, Component uiContext) {
			String pid = (String)propertyId;
			Field field = super.createField(item, propertyId, uiContext);
			if(field instanceof TextField){
				TextField txt = (TextField)field;
				if ("zipCode".equals(pid)){
					txt.setMaxLength(10);
				}else if ("active".equals(pid)){
					txt.setMaxLength(1);
					txt.setVisible(false);
				}
				txt.setWidth(COMMON_FIELD_WIDTH);
				txt.setNullRepresentation("");
			}
			return field;
		}
	}
}

