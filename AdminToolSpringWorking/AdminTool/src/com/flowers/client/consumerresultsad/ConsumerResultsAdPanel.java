package com.flowers.client.consumerresultsad;

import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.BooleanQuery.Builder;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.server.entity.ConsumerResultsAd;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class ConsumerResultsAdPanel extends VerticalLayout {
	private static final long serialVersionUID = -6449339434955571727L;
	private ConsumerResultsAdTable consumerResultsAdTable;
	private ConsumerResultsAdForm consumerResultsAdForm;
	private TextField city;
	private Button searchButton;
	
	public HorizontalLayout getBannerAdSearch() {
		
		HorizontalLayout consumerLandingAdLayout = new HorizontalLayout();
		consumerLandingAdLayout.setSpacing(true);
		consumerLandingAdLayout.setHeight("40px");
		
		city = new TextField("City");
		
		consumerLandingAdLayout.addComponent(city);
		consumerLandingAdLayout.setComponentAlignment(city, Alignment.MIDDLE_LEFT);
		
		searchButton = new Button("Search");
		consumerLandingAdLayout.addComponent(searchButton);
		consumerLandingAdLayout.setComponentAlignment(searchButton, Alignment.BOTTOM_LEFT);
		searchButton.addListener(new ClickListener() {
			private static final long serialVersionUID = 3108046082840091062L;
			public void buttonClick(ClickEvent event) {
				ConsumerResultsAd resultsAd = new ConsumerResultsAd();
				consumerResultsAdForm.setObjectToForm(resultsAd);
				consumerResultsAdForm.removeButton.setVisible(true);
				search();
			}
		});
		final Button indexButton = new Button("Index");
		consumerLandingAdLayout.addComponent(indexButton);
		consumerLandingAdLayout.setComponentAlignment(indexButton, Alignment.BOTTOM_LEFT);
		indexButton.addListener(new ClickListener() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -2876914040646907172L;

			public void buttonClick(ClickEvent event) {
				BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
				SearchService searchService = app.getBean("SearchService", SearchService.class);
				indexButton.setEnabled(false);
				try {
					SingleThread st = new SingleThread();
					st.index(ConsumerResultsAd.class, searchService,true);
					indexButton.setEnabled(true);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
			
		return consumerLandingAdLayout;
	}

	protected void search(){
		try {
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			SearchService searchService = app.getBean("SearchService", SearchService.class);
			List<Document> consumerLandingAds = null;
			SortField sortField = new SortField("city", SortField.Type.STRING);
			if(!"".equals(city.getValue())) {
				if(city.getValue().toString().split(",").length > 1){
					String cityName = city.getValue().toString().split(",")[0].trim().toUpperCase();
					String stateName = city.getValue().toString().split(",")[1].trim().toUpperCase();
					Builder query = new BooleanQuery.Builder();
					TermQuery q1 = new TermQuery(new Term("city", cityName));
					TermQuery q2 = new TermQuery(new Term("state_code", stateName));
					query.add(new BooleanClause(q1, BooleanClause.Occur.MUST));
					query.add(new BooleanClause(q2, BooleanClause.Occur.MUST));
					consumerLandingAds = searchService.documents(ConsumerResultsAd.class, query.build(), sortField);
				}
					
				else{
					
					String searchTerm = city.getValue().toString().toUpperCase();
					TermQuery query = new TermQuery(new Term("city", searchTerm));
					consumerLandingAds = searchService.documents(ConsumerResultsAd.class, query, sortField);
				}

			} else{
				consumerLandingAds = searchService.documents(ConsumerResultsAd.class, new MatchAllDocsQuery(), sortField);
			}
			if(consumerLandingAds != null) {
				consumerResultsAdTable.removeAllItems();
				for(Document d : consumerLandingAds) {
					consumerResultsAdTable.addDocumentToContainer(d);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public ConsumerResultsAdPanel() {
		setSizeFull();
		setSpacing(true);
		addComponent(getBannerAdSearch());

		consumerResultsAdTable = new ConsumerResultsAdTable();
		addComponent(consumerResultsAdTable);
		consumerResultsAdTable.addListener(new ConsumerResultsAdTable.ValueChangeListener() {
			private static final long serialVersionUID = -3922216275089187949L;

			public void valueChange(ValueChangeEvent event) {
				Long id = (Long)consumerResultsAdTable.getValue();
				try {
					if (id != null){
						BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
						CrudService service = app.getBean("CrudService", CrudService.class);
						SingleThread st = new SingleThread();
						ConsumerResultsAd obj = (ConsumerResultsAd)st.find(ConsumerResultsAd.class, id, service);
						consumerResultsAdForm.setObjectToForm(obj);
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
            }
        });
		
		consumerResultsAdForm = new ConsumerResultsAdForm(consumerResultsAdTable);
		addComponent(consumerResultsAdForm);
	}
}
