
package com.flowers.client.consumerresultsad;

import javax.persistence.PersistenceException;

import com.flowers.client.BloomNetAdminApplication;
import com.flowers.client.ui.base.BaseForm;
import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.City;
import com.flowers.server.entity.ConsumerResultsAd;
import com.flowers.server.entity.State;
import com.flowers.server.service.CrudService;
import com.flowers.server.service.impl.SingleThread;
import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window.Notification;

public class ConsumerResultsAdForm extends BaseForm<ConsumerResultsAd> {

	private static final long serialVersionUID = -1609034181956013244L;
	private TextField city;
	private static final String COMMON_FIELD_WIDTH = "25em";
	public static final String[] VISIBLE_FIELDS_ORDER = new String[]{"city", "fileName", "website"};
	
	public ConsumerResultsAdForm(TableContainer<ConsumerResultsAd> tableContainer) {
		super(tableContainer, VISIBLE_FIELDS_ORDER);
	}
	
	
	@Override
	public ConsumerResultsAd getBlankObject() {
		return new ConsumerResultsAd();
	}

	@Override
	public ConsumerResultsAd insert(ConsumerResultsAd t) throws PersistenceException {
		try{
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			SingleThread st = new SingleThread();
			CrudService service = app.getBean("CrudService", CrudService.class);
			String state = city.getValue().toString().split(",")[1].trim();
			String cityName = city.getValue().toString().split(",")[0].trim();
			State selectedState = (State)st.findSingleResult("from State s where s.shortName = '"+state+"'", service);
			if(selectedState == null){
				getApplication().getMainWindow().showNotification("Invalid City/State.", Notification.POSITION_CENTERED);
				city.setReadOnly(false);
				return t;
			}
			City selectedCity = (City) st.findSingleResult("from City c where c.name = '"+cityName+"' and state.id = "+selectedState.getId()+"", service);
			if(selectedCity == null || !selectedCity.getCityType().equals("D")){
				getApplication().getMainWindow().showNotification("Invalid City/State.", Notification.POSITION_CENTERED);
				city.setReadOnly(false);
				return t;
			}
			t.setCity(selectedCity);
			st.persist(t, service);
			st.index(ConsumerResultsAd.class, t.getId(), service);
		}catch(Exception ee){
		}
		return t;
	}

	@Override
	public ConsumerResultsAd update(ConsumerResultsAd t) throws PersistenceException {
		try{
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			String state = city.getValue().toString().split(",")[1].trim();
			String cityName = city.getValue().toString().split(",")[0].trim();
			State selectedState = (State)st.findSingleResult("from State s where s.shortName = '"+state+"'", service);
			if(selectedState == null){
				getApplication().getMainWindow().showNotification("Invalid City/State.", Notification.POSITION_CENTERED);
				city.setReadOnly(false);
				return t;
			}
			City selectedCity = (City) st.findSingleResult("from City c where c.name = '"+cityName+"' and state.id = "+selectedState.getId()+"", service);
			if(selectedCity == null || !selectedCity.getCityType().equals("D")){
				getApplication().getMainWindow().showNotification("Invalid City/State.", Notification.POSITION_CENTERED);
				city.setReadOnly(false);
				return t;
			}
			t.setCity(selectedCity);
			st.merge(t, service);
			st.index(ConsumerResultsAd.class, t.getId(), service);
		}catch(Exception ee){
		}
		return t;
	}

	@Override
	public Object remove(ConsumerResultsAd t) throws PersistenceException {
		try{
			BloomNetAdminApplication app = (BloomNetAdminApplication)getApplication();
			CrudService service = app.getBean("CrudService", CrudService.class);
			SingleThread st = new SingleThread();
			st.remove(t, service);
		}catch(Exception ee){
		}
		return t;
	}

	@Override
	public FormFieldFactory getFormFieldFactory() {
		return new ConsumerLandingAdFieldFactory();
	}
	
	public void saveButtonClickHandler(){
		if(city.getValue() == null || getField("fileName").getValue() == null || getField("website").getValue() == null){
			getApplication().getMainWindow().showNotification("Please Fill Out All Necessary Fields.", Notification.POSITION_CENTERED);
		}else{
			city.setReadOnly(true);
			super.saveButtonClickHandler();
		}
	}
	
	public TextField getCity() {
		if (city == null){
			city = new TextField("City");
			city.setValue("");
			city.setNullRepresentation("");
			city.setWidth("325px");
			city.setRequired(false);
		}
		return city;
		
	}

	private class ConsumerLandingAdFieldFactory extends DefaultFieldFactory{

		private static final long serialVersionUID = -3596248659949602235L;

		@Override
		public Field createField(Item item, Object propertyId,	Component uiContext) {
			String pid = (String)propertyId;
			Field field = super.createField(item, propertyId, uiContext);
			if(field instanceof TextField) {
				TextField txt = (TextField)field;
				if ("city".equals(pid)){
					return getCity();
				}
				if ("fileName".equals(pid)){
					txt.setWidth(COMMON_FIELD_WIDTH);
					txt.setMaxLength(255);
				}
				if ("website".equals(pid)){
					txt.setWidth(COMMON_FIELD_WIDTH);
					txt.setMaxLength(255);
				}
				txt.setNullRepresentation("");
			}
			return field;
		}
	}
}
