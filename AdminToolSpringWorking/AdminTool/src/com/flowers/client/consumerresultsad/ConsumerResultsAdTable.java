package com.flowers.client.consumerresultsad;

import org.apache.lucene.document.Document;

import com.flowers.client.ui.base.TableContainer;
import com.flowers.server.entity.ConsumerResultsAd;
import com.vaadin.data.Item;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Table;

public class ConsumerResultsAdTable extends Table implements TableContainer<ConsumerResultsAd> {
	private static final long serialVersionUID = 6659565399894082290L;

	public ConsumerResultsAdTable() {
		addContainerProperty("city", String.class, null);
		addContainerProperty("state", String.class, null);
		addContainerProperty("fileName", String.class, null);
		addContainerProperty("website", String.class, null);
		setSizeFull();
		setHeight(200, AbstractComponent.UNITS_PIXELS);
		setSelectable(true);
		setImmediate(true);
		Object [] properties={"city"};
		boolean [] ordering={true};
		sort(properties, ordering);
		setNullSelectionAllowed(false);
	}
	
	@Override
	public Item addItemToContainer(ConsumerResultsAd t) {
		Item item = this.addItem(t.getId());
		if(t.getCity() != null) {
			item.getItemProperty("city").setValue(t.getCity().getName());
			item.getItemProperty("state").setValue(t.getCity().getState().getName());
		}
		if(item != null) {
			item.getItemProperty("fileName").setValue(t.getFileName());
			item.getItemProperty("website").setValue(t.getWebsite());
		}
		return item;
	}

	@Override
	public Item addDocumentToContainer(Document d) {
		Long id = Long.valueOf(d.get("id"));
		Item item = this.addItem(id);
		if(item != null) {
			item.getItemProperty("city").setValue(d.get("city"));
			item.getItemProperty("state").setValue(d.get("state"));
			item.getItemProperty("fileName").setValue(d.get("file_name"));
			item.getItemProperty("website").setValue(d.get("website"));
		}
		return item;
	}

	@Override
	public void selectRow(ConsumerResultsAd t) {
		this.setValue(t.getId());
	}

}
