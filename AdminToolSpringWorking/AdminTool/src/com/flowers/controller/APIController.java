package com.flowers.controller;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.flowers.server.entity.Shop;
import com.flowers.server.service.SearchService;
import com.flowers.server.service.impl.SingleThread;
import com.flowers.server.util.SQLData;

@Controller
public class APIController{
	@Autowired SearchService searchService;
	protected final Log logger = LogFactory.getLog(getClass());
    String encryptedAPIKey = "77a2d7b6b069bc91d5053b551220b66d60c5d5ee446c397dd403749cc415c79d0312f39c13fd2a23760d8d182ac97a9959c4b7d9d7b2d1a6d62daf8d4cee6632";

    public static class ResultsXML {
        private String resultsXML;
        public void setResultsXML(String resultsXML){
        	this.resultsXML = resultsXML;
        }
        public String getResultsXML(){
        	return this.resultsXML;
        }
    }
	
	private static List<String> fieldExclusions = new ArrayList<String>();
	static {
		fieldExclusions.add("_hibernate_class");
		fieldExclusions.add("zips");
	}
	
			

	@RequestMapping(value="/v1/InsertShopWebsite")
	@Scope("request")
	public @ResponseBody String insertShopWebsite(HttpServletRequest request,
			@RequestParam(value="apiKey", required=false) String apiKey,
			@RequestParam(value="shopCode", required=false) String shopCode,
			@RequestParam(value="website", required=false) String website,
			@RequestParam(value="primaryWebsite", required=false) String primaryWebsite) throws SQLException{
		
		if(authorize(apiKey)){
		
			SQLData mySQL = new SQLData();
			List<Long> shopsToIndex = new ArrayList<Long>();
			
			
			String query = "SELECT shop_id FROM shop WHERE shop_code = ?;";
			String shopID = "";
			
			List<String> params = new ArrayList<String>();
			params.add(shopCode);
			
			String primaryWebsiteBoolean = "0";
			
			if(primaryWebsite != null && primaryWebsite.equalsIgnoreCase("Y"))
				primaryWebsiteBoolean = "1";
			
			ResultSet results = mySQL.executeParameterizedQuery(query, 1, params);
			
			if(results.next()){
				shopID = results.getString("shop_id");
				String statement = "UPDATE shop SET website = ?, primary_website =  ? WHERE shop_id = ?;";
				params = new ArrayList<String>();
				params.add(website);
				params.add(primaryWebsiteBoolean);
				params.add(shopID);
				mySQL.executeParameterizedStatement(statement, 3, params);
				
				shopsToIndex.add(Long.valueOf(shopID));
				
				SingleThread st = new SingleThread();
				
				st.index(Shop.class, shopsToIndex, searchService);
				 
			}else{
				return "Shop code not found in Directory Online.";
			}
			mySQL.closeStatement();
		}else{
			return "Unauthorized attempt.";
		}
		return "Request completed sucessfully.";
	}
	
	private boolean authorize(String apiKey){

    	MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-512");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		String salt = "h7M).hL9B6hjlScXz!a0Av$()77ScXzZ"; //hard coded here to prevent discovery in database or files system
		
		
		if(apiKey == null){
    		return false;
    	}else{
    		StringBuffer sb = new StringBuffer();
			md.update((apiKey+salt).getBytes());
			for (int xx=0; xx<10000; ++xx){
				 md.update(sb.toString().getBytes());
			     byte byteData[] = md.digest();
		        //convert the byte to hex format
		        sb = new StringBuffer();
		        for (int i = 0; i < byteData.length; i++) {
		        	sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		        }
			}
			
			if(sb.toString().equals(encryptedAPIKey)){
				return true;
			}else{
				return false;
			}
    	}
	}
	
}
