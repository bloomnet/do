<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>Welcome - Directory Online</title>
		<meta name="description" content="" />
		<link rel="stylesheet" type="text/css" href="css/login.css" /> 
	<style>
		.error {
			color: red;
		}
	</style>
 		
	</head>
   
   <body class=" controls-visible guest-community signed-out public-page"> 
   <div id="login">
   
<form:form method="post"> 
  <div class="aui-fieldset-content "> <input class="aui-field-input aui-field-input-hidden"  id="_58_username" name="_58_username"    type="hidden" value=""   /> 
  <div id="container"> 
  <div id="login_container"> 
  <table id="login_fields"> 
  <tr> 
  <td><div class="login_field"> <span class="aui-field aui-field-text"> 
  <span class="aui-field-content"> <label class="aui-field-label" for="_58_login"> Username	</label> 
  <span class='aui-field-element '> <form:input path="userName" type="text" name="username" value="" class="aui-field-input aui-field-input-text" onfocus="if (this.value=='username'){this.value='';this.style.color='#000'}" onblur="if (this.value==''){this.value='username';this.style.color='#c7c6c6'}" /> </span> </span> </span> 
  </div>
  </td> 
  <td><div class="login_field"> <span class="aui-field aui-field-text"> 
  <span class="aui-field-content"> <label class="aui-field-label" for="_58_password"> Password </label> 
  <span class='aui-field-element '><form:input path="password" type="password" name="password" value=""  class="aui-field-input aui-field-input-text" onfocus="if (this.value=='password'){this.value='';this.style.color='#000'}" onblur="if (this.value==''){this.value='password';this.style.color='#c7c6c6'}" /> </span> </span> </span> 
  </div>
  </td> 
  <td><span id="_58_passwordCapsLockSpan" style="display: none;">Caps Lock is on.</span></td> <!-- --> 
  </tr> 
  <tr>
  <td><div class="login_field"><input class="login_signin" type="submit" value="" onclick="populateLoginValue();" /></div></td></tr> <tr> <td colspan="3"> </td> 
  </tr> 
  </table> 
  </div> </div>
   </div>  
   </form:form> 
   </div>
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<br />
	<div align="center">
		<table align="center">
	            <tr> 
	              <td height="40" colspan="2" align="center">
	                <font face="Arial" size="2" color="red">
				<spring:bind path="command.*">
				  <c:if test="${status.errors.errorCount > 0}">
				    <c:forEach var="error" items="${status.errors.allErrors}">
				      <c:out value="Invalid User Name or Password"></c:out> <br>
				    </c:forEach>
				  </c:if>
				</spring:bind>
		      </font>
	              </td>
	            </tr> 
	          </table>
	</div>
  </body>
</html>