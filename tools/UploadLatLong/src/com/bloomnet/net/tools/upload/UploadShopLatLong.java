package com.bloomnet.net.tools.upload;

import java.io.File;
import java.io.IOException;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class UploadShopLatLong {
	
	SQLData mySQL;
	
	public static void main(String[] args){
		new UploadShopLatLong();
	}
	public UploadShopLatLong(){
		mySQL = new SQLData();
		mySQL.login();
		uploadShopLatLong("data/shopLatLong.xls");
	}
	private void uploadShopLatLong(String inputFile){
		File inputWorkbook = new File(inputFile);
		Workbook w;
		try {
			w = Workbook.getWorkbook(inputWorkbook);
			Sheet sheet = w.getSheet(0);
			for (int ii = 1; ii < sheet.getRows(); ++ii){
				Cell florist = sheet.getCell(0, ii);
				Cell lat = sheet.getCell(1, ii);
				Cell longi = sheet.getCell(2, ii);
				String shopCode = florist.getContents().toString();
				String latitude = lat.getContents().toString();
				String longitude = longi.getContents().toString();
				System.out.println(shopCode);
				String statement = "UPDATE bloomnet.shop " +
						"SET latitude = "+Double.valueOf(latitude)+", " +
						"longitude = "+Double.valueOf(longitude)+" " +
						"WHERE shop_code = '"+shopCode+"';";
				mySQL.executeStatement(statement);
			}	
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
