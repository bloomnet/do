package net.bloomnet.copyads;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;


public class CopyAds{
	
	public static void main(String[] args){
		new CopyAds();
	}
	
	public CopyAds(){
		getFilesToCopy();
	}
	
	private void getFilesToCopy(){
		try{
		    FileInputStream fis = new FileInputStream("adSizeChanges.txt");
		    BufferedReader br = new BufferedReader(new InputStreamReader(fis));
		    String strLine;
		    String shopCode;
		    String entryCode;
		    while ((strLine = br.readLine()) != null){
		    	shopCode = strLine.split("	")[0];
		    	entryCode = strLine.split("	")[1];
		    	String srFileName = shopCode+"-"+entryCode+"B.png";
		    	String dtFileName = shopCode+"-"+entryCode+"C.png";
		    	String dt2FileName = "ads_BKUPS/"+shopCode+"-"+entryCode+"B.png";
		    	copyFile(srFileName, dtFileName, dt2FileName);
		    }
		    fis.close();
		    }catch (Exception ee){
		      System.err.println(ee.getMessage());
		    }

	}
	
	private void copyFile(String srFile, String dtFile, String dt2File){
  
		try{
			File f1 = new File(srFile);
		  	File f2 = new File(dtFile);
		  	File f3 = new File(dt2File);
		  
		  	InputStream in = new FileInputStream(f1);
		  	OutputStream out = new FileOutputStream(f2);
		  	OutputStream out2 = new FileOutputStream(f3);
		  
		  	byte[] buf = new byte[1024];
		  	int len;
	 
		  	while ((len = in.read(buf)) > 0){
		  		out.write(buf, 0, len);
		  		out2.write(buf, 0, len);
		  	}
		  	in.close();
		  	out.close();
		  	out2.close();
	 
		  	System.out.println("File copied.");
	  	}catch(FileNotFoundException ex){
	  		System.out.println(ex.getMessage());
	  	}catch(IOException ee){
	  		System.out.println(ee.getMessage());  
	  	}
	  }
	}
	

