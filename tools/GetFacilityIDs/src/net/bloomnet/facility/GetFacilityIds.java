package net.bloomnet.facility;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.HashMap;
import java.util.Map;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

class GetFacilityIds {
	private Map<String,String> facilityPhone = new HashMap<String,String>();
	private Map<String,String> facilityAddress = new HashMap<String,String>();
	SQLData mySQL;
	FileWriter fstream;
	BufferedWriter out;
	
	public static void main(String[] args){
		new GetFacilityIds();
	}
	
	public GetFacilityIds(){
		mySQL = new SQLData();
		mySQL.login();
		try {
			fstream = new FileWriter("facility_ids.txt");
		} catch (IOException e) {
			e.printStackTrace();
		}
		out = new BufferedWriter(fstream);
		setFacilityPhone();
		getFacilityIds("badfile.xls");
	}
	
	public void setFacilityPhone(){
		String query = "SELECT id,telephone_number FROM bloomnet.facility;";
		ResultSet results = mySQL.executeQuery(query);
		try {
			while(results.next()){
				facilityPhone.put(formatPhone(results.getString("telephone_number")),results.getString("id"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void setFacilityAddresses() throws SQLException{
		String query = "SELECT facility.id, address.street_address1, city.name, state.name " +
							"FROM bloomnet.facility " +
							"INNER JOIN bloomnet.address ON facility.address_id = address.id " +
							"INNER JOIN bloomnet.city ON address.city_id = city.id " +
							"INNER JOIN bloomnet.state ON city.state_id = state.id ";
		ResultSet results = mySQL.executeQuery(query);
		
		try{
			while(results.next()){
				facilityAddress.put(results.getString(2).toUpperCase()+","+results.getString(3).toUpperCase()+","+results.getString(4).toUpperCase(),results.getString(1));
			}
		}catch(Exception ee){}
	}
	
	public void getFacilityIds(String inputFile){
		File inputWorkbook = new File(inputFile);
		Workbook w;
		try {
			w = Workbook.getWorkbook(inputWorkbook);
			Sheet sheet = w.getSheet(0);
			for (int ii = 0; ii < sheet.getRows(); ++ii){
				Cell phone;
				try{
					phone = sheet.getCell(20, ii);
				}catch(Exception ee){
					phone = sheet.getCell(6, ii);
				}
				String phoneString = "";
				if(phone.getContents()!= null && !phone.getContents().equals("")) phoneString = formatPhone(phone.getContents().toString());
				if(phoneString != null && !phoneString.equals("")){
					try{
						if(facilityPhone.get(phoneString) != null){
							String facilityId = facilityPhone.get(phoneString);
							System.out.println(facilityId);
							out.write("\n"+facilityId);
						}else{
							System.out.println("No facility found for: "+phoneString);
						}
					}catch(Exception ee){
						System.out.println("No facility found for: "+phoneString);
					}
				}else{
					try {
						if(facilityAddress.size() == 0) setFacilityAddresses();
					} catch (SQLException e) {
						e.printStackTrace();
					}
					Cell address1 = sheet.getCell(3,ii);
					Cell city = sheet.getCell(5,ii);
					Cell state = sheet.getCell(6,ii);
					
					String addressString = address1.getContents().toString().toUpperCase() + "," + city.getContents().toString().toUpperCase() + "," +
											state.getContents().toString().toUpperCase();
						
					try{
						if(facilityAddress.get(addressString) != null){
							String facilityId = facilityAddress.get(addressString);
							out.write("\n"+facilityId);
						}else{
							System.out.println("PROBLEM: No facility found for: "+addressString);
						}
					}catch(Exception ee){
						System.out.println("CATCH: No facility found for: "+addressString);
					}
				}
			}	
		} catch (BiffException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
private String formatPhone(String phone){
		
		final StringBuilder result = new StringBuilder();
	    final StringCharacterIterator iterator = new StringCharacterIterator(phone);
	    
	    char character =  iterator.current();
	    
	    while (character != CharacterIterator.DONE ){
	      if (character == '(' || character == ')' || character == '-' || character == ' ') {
	      }else if(character == '0' || character == '1' || character == '2' || character == '3' ||
	    		  character == '4' || character == '5' || character == '6' || character == '7' || character == '8' ||
	    		  character == '9'){
	        result.append(character);
	      }
	      character = iterator.next();
	    }

	    return result.toString();
	}
}
