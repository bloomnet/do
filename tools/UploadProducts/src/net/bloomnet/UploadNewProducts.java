package net.bloomnet;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class UploadNewProducts{
	
  private SQLData mySQL = new SQLData();
  private HashMap<String, String> productCodification = new HashMap<String,String>();
  
  public UploadNewProducts(){
	  
    mySQL.login();
    uploadProducts("Codification.xls");
    try{
      setProductCodification();
    }catch (SQLException e){
      e.printStackTrace();
    }
    uploadProductShopXref("xref.xls");
    
  }
  
  private void setProductCodification() throws SQLException{
	  
    String query = "SELECT id,product_codification FROM bloomnet.product_codification;";
    ResultSet results = mySQL.executeQuery(query);
    
    while (results.next()) {
      productCodification.put(results.getString("product_codification"), results.getString("id"));
    }
    
    System.out.println(productCodification);
    
  }
  
  private void uploadProducts(String inputFile){
	  
    String delete = "DELETE FROM bloomnet.shop_product_codification_xref WHERE product_codification_id NOT IN (1144,1145);";
    mySQL.executeStatement(delete);
    delete = "DELETE FROM bloomnet.product_codification WHERE id NOT IN (1144,1145);";
    mySQL.executeStatement(delete);
    File inputWorkbook = new File(inputFile);
    
    try
    {
      Workbook w = Workbook.getWorkbook(inputWorkbook);
      Sheet sheet = w.getSheet(0);
      
      for (int ii = 0; ii < sheet.getRows(); ii++){
    	  
        Cell description = sheet.getCell(1, ii);
        Cell symbol = sheet.getCell(2, ii);
        
        if ((symbol.getContents().toString() != null) && (!symbol.getContents().toString().equals(""))){
          String query = "INSERT INTO bloomnet.product_codification (product_codification,name) VALUES (\"" + symbol.getContents().toString() + "\",\"" + description.getContents().toString().toUpperCase() + "\");";
          mySQL.executeStatement(query);
        }
        
      }
    }catch (BiffException e){
      e.printStackTrace();
    }catch (IOException e){
      e.printStackTrace();
    }
    
  }
  
  private void uploadProductShopXref(String inputFile){
	  
    File inputWorkbook = new File(inputFile);
    try{
      Workbook w = Workbook.getWorkbook(inputWorkbook);
      Sheet sheet = w.getSheet(0);
      
      for (int ii = 1; ii < sheet.getRows(); ii++){
        Cell florist = sheet.getCell(0, ii);
        Cell symbol = sheet.getCell(1, ii);
        String shopCode = florist.getContents().toString();
        System.out.println(shopCode);
        String symbolId = productCodification.get(symbol.getContents().toString());
        String query = "INSERT INTO bloomnet.shop_product_codification_xref (shop_code,product_codification_id) VALUES ('" + shopCode + "'," + Integer.valueOf(symbolId) + ");";
        mySQL.executeStatement(query);
      }
      
    }catch (BiffException e){
      e.printStackTrace();
    }catch (IOException e){
      e.printStackTrace();
    }
    
  }
  
  public static void main(String[] args){
	  
    new UploadNewProducts();
    
  }
  
}
