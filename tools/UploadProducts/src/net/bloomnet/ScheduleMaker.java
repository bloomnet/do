package net.bloomnet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;


public class ScheduleMaker {
	List<String> employees = new ArrayList<String>();
	Map<String,Integer> chosenEmployeeCount = new HashMap<String,Integer>();
	
	Random rn = new Random();
	int random = 999;
	String personHomeThree = "";
	
	public ScheduleMaker(){
		employees.add("Mark");
		employees.add("Jay");
		employees.add("Eric");
		employees.add("Anthony");
		
		String[] choices = new String[employees.size()];
		for(int i=0; i<choices.length; i++){
			choices[i] = employees.get(i);
		}
		JPanel panel = new JPanel();  
		personHomeThree = choices[JOptionPane.showOptionDialog(null, panel,  
		    "Who is remote three times this week?", JOptionPane.OK_OPTION, -1, null, choices, 1)];  

		employees.remove(personHomeThree);
		
		random = getNextEmployee(random);
		String monday = employees.get(random)+", ";
		random = getNextEmployee(random);
		monday += employees.get(random);
		random = getNextEmployee(random);
		
		String tuesday = employees.get(random)+", ";
		random = getNextEmployee(random);
		tuesday += employees.get(random);
		random = getNextEmployee(random);
		
		String wednesday = "all";
		
		String thursday = personHomeThree+", ";
		thursday += employees.get(random);
		random = getNextEmployee(random);
		
		String friday = employees.get(random);
		
		String schedule = "Monday: "+ monday + "\n" + "Tuesday: "+ tuesday + "\n" + "Wednesday: "+ wednesday + "\n" + "Thursday: "+ thursday + "\n" + "Friday: "+ friday + "\n" ;
		
		JTextArea textarea= new JTextArea(schedule);
		textarea.setEditable(false);
		JOptionPane.showMessageDialog(null, textarea, "Schedule for the week", -1);

	}
	
	private int getNextEmployee(int random){
		if(random != 999){
			if(chosenEmployeeCount.get(employees.get(random)) != null){
				chosenEmployeeCount.put(employees.get(random), chosenEmployeeCount.get(employees.get(random)) + 1);
				employees.remove(random);
			}else
				chosenEmployeeCount.put(employees.get(random), 1);
		}
		if(employees.size() > 1)
			while(random == (random = rn.nextInt(employees.size())));
		else
			random = rn.nextInt(employees.size());
		return random;
	}
	
	public static void main(String[] args){
		new ScheduleMaker();
	}
	
}
