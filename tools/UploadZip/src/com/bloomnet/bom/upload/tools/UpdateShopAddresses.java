package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.qas.EncodingUtil;
import com.qas.newmedia.internet.ondemand.product.proweb.AddressLine;
import com.qas.newmedia.internet.ondemand.product.proweb.PicklistItem;
import com.qas.newmedia.internet.ondemand.product.proweb.PromptSet;
import com.qas.newmedia.internet.ondemand.product.proweb.QasException;
import com.qas.newmedia.internet.ondemand.product.proweb.QuickAddress;
import com.qas.newmedia.internet.ondemand.product.proweb.SearchResult;
import com.qas.ondemand_2011_03.QAAuthentication;

public class UpdateShopAddresses {
	
	SQLData mySQL = null;
	
	Map<String,String> existingCities = new HashMap<String,String>();
	Map<String, String> existingZips = new HashMap<String,String>();
	Map<String, String> existingShops = new HashMap<String,String>();
	
	private FileOutputStream fos;
	private OutputStreamWriter out;

	public UpdateShopAddresses(){
		mySQL = new SQLData();
		mySQL.login();
		try {
			//updateAddresses();  //Use this to get a file of all verified addresses along side their original addresses for each shop in the database.
			//setExistingCities();
			//setExistingZips();
			//setExistingShops();
			//writeNewAddressesToDB();    //Use this to get the results from the results file and actually push those changes to the database
			mergeMatchedShops();    //use this to actually consolidate shops that are newly matched by address.
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (QasException e) {
			e.printStackTrace();
		}
	}
	
	private void setExistingCities() throws SQLException{
		
		String query = "SELECT city.City_ID,city.Name,state.Short_Name FROM shopdata.city " +
					   "INNER JOIN shopdata.state ON city.State_ID = state.State_ID;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			try{
				existingCities.put(results.getString("Name").toUpperCase()+","+results.getString("Short_Name").toUpperCase(), results.getString("City_ID"));
			}catch(Exception ee){
				System.err.println(results.getString("Name"));
			}
		}
		results.close();
		mySQL.closeStatement();
	}
	
	private void setExistingZips() throws SQLException{
		
		String query = "SELECT zip.Zip_ID,zip.Zip_Code FROM shopdata.zip;";

		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			existingZips.put(results.getString("Zip_Code"), results.getString("Zip_ID"));
		}
		results.close();
		mySQL.closeStatement();
	}
	
	private void setExistingShops() throws SQLException{
		
		String query = "SELECT shop.Shop_ID,shop.ShopPhone FROM shopdata.shop;";

		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			existingShops.put(results.getString("ShopPhone"), results.getString("Shop_ID"));
		}
		results.close();
		mySQL.closeStatement();
	}
	
	private void updateAddresses() throws SQLException, IOException, QasException{
		
		fos = new FileOutputStream("data/shopAddresses.txt");
		out = new OutputStreamWriter(fos);
		
		InputStream input = null;
		try {
			input = new FileInputStream("/var/bloomnet/cfg/reports.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
    	Properties props = new Properties();
        try {
			props.load(input);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String URL = "https://ws2.ondemand.qas.com/ProOnDemand/V3/ProOnDemandService.asmx?WSDL";
	    String Username = props.getProperty("qasuser");
	    String Password = props.getProperty("qaspassword");
	    String action = EncodingUtil.decodeURIComponent("search");
	    String addLayout = EncodingUtil.decodeURIComponent("Database layout");
	    
	    QAAuthentication authenticationInfo = new QAAuthentication();
		authenticationInfo.setUsername(Username);
		authenticationInfo.setPassword(Password);
		
		QuickAddress qAddress = new QuickAddress(URL, authenticationInfo);
	    qAddress.setEngineType(QuickAddress.VERIFICATION);
	    qAddress.setFlatten(true);
	    qAddress.setEngineIntensity(QuickAddress.CLOSE);
	    qAddress.setPromptSet("Default");
		
		out.write("ShopName,ShopPhone,OriginalAddress1,OriginalAddress2,OriginalCity,OriginalState,OriginalZip,NewAddress1,NewAddress2,NewCity,NewState,NewZip\n");
		
		String statement = "USE shopdata;";
		mySQL.executeStatement(statement);
		
		String query = "SELECT ShopName, ShopPhone, ShopAddress1, ShopAddress2, city.Name, state.Short_Name, zip.Zip_Code "+
							"FROM shop "+
							"INNER JOIN city ON shop.City_ID = city.City_ID "+
							"INNER JOIN state ON city.State_ID = state.State_ID "+
							"INNER JOIN zip ON shop.Zip_ID = zip.Zip_ID "+
							"LIMIT 30000;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		int count=0;
		
		while(results.next()){
			count++;
			if(count%100==0)
				System.out.println(count);
			String shopName = results.getString("ShopName");
			String shopPhone = results.getString("ShopPhone");
			String address1 = results.getString("ShopAddress1");
			String address2 = results.getString("ShopAddress2");
			String cityName = results.getString("Name");
			String stateName = results.getString("Short_Name");
			String zip = results.getString("Zip_Code");
			
			String searchstring = "";
		    
		    if(address2 != null && !address2.equals(""))
		    	searchstring = EncodingUtil.decodeURIComponent(address1+" "+address2+"|||@+"+cityName+"|"+stateName+"|"+zip);
		    else
		    	searchstring = EncodingUtil.decodeURIComponent(address1+"|||@+"+cityName+"|"+stateName+"|"+zip);
			
		    //search for address
		    if (action.equals("search"))
		    {
		        //send address to QAS
		        SearchResult result = qAddress.search("USA", searchstring, PromptSet.DEFAULT, addLayout);
		        
		        if ((result.getVerifyLevel() == "Verified") || (result.getVerifyLevel() == "InteractionRequired") ){
		        //for Verified and InteractionRequired addresses get the result
			        if(result.getPicklist() != null)
			        {
			            for( PicklistItem item : result.getPicklist().getItems())
			            {
			            	String addressCorrected = item.getPartialAddress().replaceAll(item.getPostcode(), "").trim();
			            	String[] addressCorrectedItems = addressCorrected.split(",");
			            	String correctedAddress1 = addressCorrectedItems[0];
			            	String[] correctedCityState = addressCorrectedItems[1].split(" ");
			            	String correctedCity = "";
			            	String correctedState = "";
			            	for(int ii=0; ii<correctedCityState.length; ++ii){
			            		if(ii == correctedCityState.length - 1)
			            			correctedState = correctedCityState[ii];
			            		else if(ii == 0)
			            			correctedCity = correctedCityState[ii];
			            		else
			            			correctedCity += " "+correctedCityState[ii];
			        
			            	}
			            	String correctedZip = item.getPostcode();
			            	if(!correctedAddress1.equalsIgnoreCase(address1) || !correctedCity.equalsIgnoreCase(cityName) || !correctedZip.equals(zip)){
			            		out.write(shopName+","+shopPhone+","+address1+","+address2+","+cityName+","+stateName+","+zip+","+correctedAddress1+",,"+correctedCity+","+stateName+","+correctedZip+"\n");
			            		out.flush();
			            		break;
			            	}
			            }
			        }else if(result.getAddress() != null && result.getAddress().getAddressLines() != null){
			        	String correctedAddress = "";
			        	 for( AddressLine line : result.getAddress().getAddressLines())
				            {
				            	correctedAddress += line.getLine() + ",";
				            }
			        	 correctedAddress = correctedAddress.substring(0,correctedAddress.length() - 1).trim();
			        	 String[] correctedArray = correctedAddress.split(",");
			        	 String correctedAddress1 = correctedArray[0];
			        	 String correctedAddress2 = correctedArray[1];
			        	 String correctedCity = correctedArray[3];
			        	 String correctedZip = correctedArray[5];
			        	 if(correctedZip.contains("-"))
			        		 correctedZip = correctedZip.split("-")[0];
			        	 if(!correctedAddress1.equalsIgnoreCase(address1) || !correctedAddress2.equalsIgnoreCase(address2) || !correctedCity.equalsIgnoreCase(cityName) || !correctedZip.equals(zip)){
			        		 out.write(shopName+","+shopPhone+","+address1+","+address2+","+cityName+","+stateName+","+zip+","+correctedAddress1+","+correctedAddress2+","+correctedCity+","+stateName+","+correctedZip+"\n");
			        		 out.flush();
			        	 }
			        }
			    }
		    }
		}
		results.close();
		mySQL.closeStatement();
		out.flush();
		out.close();
	}
	
	private void writeNewAddressesToDB() throws SQLException, IOException, QasException{
		BufferedReader br = new BufferedReader(new FileReader("data/newAddresses.csv"));
		String line = "";
		while ((line = br.readLine()) != null) {
		   String[] lineItems = line.split(",");
		   String shopPhone = lineItems[1];
		   String newAddress1 = lineItems[7];
		   String newAddress2 = lineItems[8];
		   String newCity = lineItems[9].toUpperCase();
		   String newState = lineItems[10].toUpperCase();
		   String newZip = lineItems[11];
		   if(newZip.length() == 3) newZip = "00"+newZip;
		   if(newZip.length() == 4) newZip = "0"+newZip;
		   
		   String cityId = existingCities.get(newCity+","+newState);
		   String zipId = existingZips.get(newZip);
		   String shopId = existingShops.get(shopPhone);
		   
		   if(cityId == null || cityId.equals("") || zipId == null || zipId.equals("") || shopId == null || shopId.equals("")){
			   System.out.println("Something is null: CityId: "+cityId+" ZipId: "+zipId+" ShopId: "+shopId);
			   continue;
		   }else{
			   if(newAddress2 != null && !newAddress2.equals("")){
				   String statement = "UPDATE shopdata.shop SET ShopAddress1 = '"+newAddress1+"', ShopAddress2 = '"+newAddress2+"', City_ID = "+cityId+", Zip_ID = "+zipId+" WHERE Shop_ID = "+shopId;
				   mySQL.executeStatement(statement);
			   }else{
				   String statement = "UPDATE shopdata.shop SET ShopAddress1 = '"+newAddress1+"', City_ID = "+cityId+", Zip_ID = "+zipId+" WHERE Shop_ID = "+shopId;
				   mySQL.executeStatement(statement);
			   }
		   }
		}
		br.close();
	}
	
	private void mergeMatchedShops() throws SQLException, IOException, QasException{
		
		String initialStatement = "USE shopdata;";
		mySQL.executeStatement(initialStatement);
		
		String query = "SELECT ShopAddress1, ShopAddress2, City_ID, Zip_ID FROM shop GROUP BY City_ID, Zip_ID, ShopAddress2, ShopAddress1 HAVING COUNT(ShopAddress1) > 1;";
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			
			String address1 = results.getString("ShopAddress1");
			String address2 = results.getString("ShopAddress2");
			String cityId = results.getString("City_ID");
			String zipId = results.getString("Zip_ID");
			String shopId1 = "";
			String shopId2 = "";
			
			String query2 = "SELECT Shop_ID FROM shop WHERE ShopAddress1 = \""+address1+"\" AND ShopAddress2 = \""+address2+"\" AND City_ID = \""+cityId+"\" AND Zip_ID = \""+zipId+"\";";
			System.out.println(query2);
			ResultSet results2 = mySQL.executeQuery(query2);
			results2.next();
			shopId1 = results2.getString("Shop_ID");
			results2.next();
			shopId2 = results2.getString("Shop_ID");
			results2.close();
			mySQL.closeStatement();
			
			String statement = "UPDATE shopnetwork SET Shop_ID = "+shopId1+" WHERE Shop_ID = "+shopId2+";";
			mySQL.executeStatement(statement);
			statement = "DELETE FROM shop WHERE Shop_ID = "+shopId2+";";
			mySQL.executeStatement(statement);
			
				
		}
		results.close();
		mySQL.closeStatement();
	}
	
	public static void main(String[] args){
		new UpdateShopAddresses();
	}
}
