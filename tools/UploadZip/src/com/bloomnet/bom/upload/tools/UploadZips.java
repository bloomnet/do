package com.bloomnet.bom.upload.tools;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class UploadZips {
	
	Map<String,String> existingStates = new HashMap<String,String>();
	Map<String,String> existingCities = new HashMap<String,String>();
	Map<String, String> existingZips = new HashMap<String,String>();
	Map<String, String> existingCityZips = new HashMap<String,String>();
	
	Map<String,String> fileStates = new HashMap<String,String>();
	Map<String,String> fileCities = new HashMap<String,String>();
	Map<String,String> fileZips = new HashMap<String,String>();
	
	private FileOutputStream fos;
	private OutputStreamWriter out;

	SQLData mySQL;
	
	public static void main(String[] args){
		new UploadZips();
	}
	
	public UploadZips(){
		mySQL = new SQLData();
		mySQL.login();
		try {
			setExistingStates();
			setExistingCities();
			setExistingZips();
			setExistingCityZips();
			uploadZips("data/usa.csv", false);
			uploadZips("data/canada.csv", true);
			outputExistingToRemove();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void setExistingStates() throws SQLException{
		
		String query = "SELECT state.id,state.short_name FROM bloomnet.state;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			existingStates.put(results.getString("short_name"), results.getString("id"));
		}
		results.close();
		mySQL.closeStatement();
	}
	
	private void setExistingCities() throws SQLException{
		
		String query = "SELECT city.id,city.name,state.short_name,city.city_type FROM bloomnet.city " +
					   "INNER JOIN bloomnet.state ON city.state_id = state.id;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			try{
				existingCities.put(results.getString("name").toUpperCase()+","+results.getString("short_name").toUpperCase()+","+results.getString("city_type").toUpperCase(), results.getString("id"));
			}catch(Exception ee){
				System.err.println(results.getString("name"));
			}
		}
		results.close();
		mySQL.closeStatement();
	}
	
	private void setExistingZips() throws SQLException{
		
		String query = "SELECT zip.id,zip.zip_code FROM bloomnet.zip;";

		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			existingZips.put(results.getString("zip_code"), results.getString("id"));
		}
		results.close();
		mySQL.closeStatement();
	}
	
	private void setExistingCityZips() throws SQLException{
		
		String query = "SELECT zip.zip_code, city.name, state.short_name FROM bloomnet.city_zip_xref " +
					   "INNER JOIN bloomnet.city ON city_zip_xref.id_city = city.id " +
					   "INNER JOIN bloomnet.zip ON city_zip_xref.id_zip = zip.id " +
					   "INNER JOIN bloomnet.state ON city.state_id = state.id;";

		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			existingCityZips.put(results.getString("name").toUpperCase()+","+results.getString("short_name").toUpperCase()+","+results.getString("zip_code"), "1");
		}
		results.close();
		mySQL.closeStatement();
	}

	private String getId() throws SQLException{
		String query = "SELECT LAST_INSERT_ID()";
		ResultSet results = mySQL.executeQuery(query);
		results.next();
		String id = results.getString("LAST_INSERT_ID()");
		results.close();
		mySQL.closeStatement();
		return id;
	}
	
	private void outputExistingToRemove(){
		try {
			fos = new FileOutputStream("data/existingToRemove"+new Date().getTime()+".txt");
			out = new OutputStreamWriter(fos);
			
			Set<String> keys = existingStates.keySet();
			Iterator<String> itr = keys.iterator();
			String key;
			
			while(itr.hasNext()){
				key = itr.next().toString();
				System.out.println(key);
				if(fileStates.get(key) == null){
					out.write("State: " + key + "\n");
				}
			}
			keys = existingCities.keySet();
			itr = keys.iterator();
			
			while(itr.hasNext()){
				key = itr.next().toString();
				System.out.println(key);;
				if(fileCities.get(key) == null){
					out.write("City: " + key + "\n");
				}
			}
			
			keys = existingZips.keySet();
			itr = keys.iterator();
			while(itr.hasNext()){
				key = itr.next().toString();
				System.out.println(key);
				if(fileZips.get(key) == null){
					out.write("Zip: " + key + "\n");
				}
			}
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void uploadZips(String fileName, boolean canadaCheck){

		try {
			
		  BigFile file = new BigFile(fileName);
		  
		  int ii=0;
		  
		  String existingStateId = "";
		  int country=0;
		  String existingCityId="";
		  String existingZipId="";
		  
		  for (String strLine : file){
			  if(ii!=0){
				  String[] zipArray = strLine.split(",");
				  String zipCode;
				  String cityName;
				  String cityType;
				  String stateName;
				  String stateShortName;
				  String lat;
				  String longi;

				  if(canadaCheck){
					  zipCode = zipArray[0].replaceAll(" ", "");
					  cityName = zipArray[1];
					  cityType = zipArray[2].replaceAll(" ", "");
					  stateName = zipArray[3];
					  stateShortName = zipArray[4].replaceAll(" ", "");
					  lat = zipArray[8].replaceAll(" ", "");
					  longi = zipArray[9].replaceAll(" ", "");
				  }else{
					  zipCode = zipArray[0].replaceAll(" ", "");
					  cityName = zipArray[2];
					  cityType = zipArray[3].replaceAll(" ", "");
					  stateName = zipArray[6];
					  stateShortName = zipArray[7].replaceAll(" ", "");
					  lat = zipArray[14].replaceAll(" ", "");
					  longi = zipArray[15].replaceAll(" ", "");
				  }
				  if(cityType != null && cityType.equalsIgnoreCase("D")){
					  fileStates.put(stateShortName.toUpperCase(),"1");
					  fileCities.put(cityName.toUpperCase()+","+stateShortName.toUpperCase(),"1");
					  fileZips.put(zipCode,"1");
					  
					  if(existingStates.get(stateShortName) != null){
						  
						  existingStateId = existingStates.get(stateShortName);
						  
						  if(existingCities.get(cityName.toUpperCase()+","+stateShortName.toUpperCase()+","+cityType.toUpperCase()) != null){
							  
							  existingCityId = existingCities.get(cityName.toUpperCase()+","+stateShortName.toUpperCase()+","+cityType.toUpperCase());
							  
							  String statement = "UPDATE bloomnet.city SET " +
												 "city.city_type = '"+cityType+"' " +
												 "WHERE city.id = "+Long.valueOf(existingCityId)+";";
							  
							  mySQL.executeStatement(statement);
							  
							  if(existingZips.get(zipCode) != null){
								  
								  existingZipId = existingZips.get(zipCode);
								  statement = "UPDATE bloomnet.zip SET " +
											   "zip_code = '"+zipCode+"'" +
											   ",latitude = "+Double.valueOf(lat)+"" +
											   ",longitude = "+Double.valueOf(longi)+"" +
											   "WHERE zip.id = "+Long.valueOf(existingZipId)+";";
								  
								  mySQL.executeStatement(statement);
							  
							  }else{
								  statement = "INSERT INTO bloomnet.zip ( " +
												  "zip_code" +
												  ",latitude" +
												  ",longitude" +
												  ") VALUES (" +
												  "'"+zipCode+"'" +
												  ","+Double.valueOf(lat)+"" +
												  ","+Double.valueOf(longi)+"" +
												  ");";
								  
								  mySQL.executeStatement(statement);
								  
								  existingZipId = getId();
								  existingZips.put(zipCode,existingZipId);
							  }
							  
						  }else{
							  String statement = "INSERT INTO bloomnet.city (" +
												   "name" +
												  ",state_id" +
												  ",major_city" +
												  ",city_type" +
												") VALUES (" +
												   "\""+cityName.toUpperCase()+"\"" +
												  ","+Long.valueOf(existingStateId)+"" +
												  ",0" +
												  ",\""+cityType.toUpperCase()+"\"" +
												");";
							  
							  mySQL.executeStatement(statement);
							  
							  existingCityId = getId();
							  existingCities.put(cityName.toUpperCase()+","+stateShortName.toUpperCase()+","+cityType.toUpperCase(),existingCityId);
							  
							if(existingZips.get(zipCode) != null){
								  
								  existingZipId = existingZips.get(zipCode);
								  statement = "UPDATE bloomnet.zip SET " +
											   "zip_code = '"+zipCode+"'" +
											   ",latitude = "+Double.valueOf(lat)+"" +
											   ",longitude = "+Double.valueOf(longi)+"" +
											   "WHERE zip.id = "+Long.valueOf(existingZipId)+";";
								  
								  mySQL.executeStatement(statement);
							  
							  }else{
								  statement = "INSERT INTO bloomnet.zip ( " +
												  "zip_code" +
												  ",latitude" +
												  ",longitude" +
												  ") VALUES (" +
												  "'"+zipCode+"'" +
												  ","+Double.valueOf(lat)+"" +
												  ","+Double.valueOf(longi)+"" +
												  ");";
								  
								  mySQL.executeStatement(statement);
								  
								  existingZipId = getId();
								  existingZips.put(zipCode,existingZipId);
							  }
						  }
					  }else{
						  if(stateShortName.toUpperCase().equals("PR")) country = 3;
						  else if(canadaCheck) country = 2;
						  else country = 1;
						  String statement = "INSERT INTO bloomnet.state ( " +
										       "name" +
											  ",country_idcountry" +
											  ",short_name" +
											") VALUES (" +
											   "\""+stateName.toUpperCase()+"\"" +
											  ","+country+"" +
											  ",\""+stateShortName.toUpperCase()+"\"" +
											  ");";
						  
						  mySQL.executeStatement(statement);
						  
						  existingStateId = getId();
						  existingStates.put(stateShortName, existingStateId);
						
						  statement = "INSERT INTO bloomnet.city (" +
						  	"name" +
						  	",state_id" +
						  	",major_city" +
							  ",city_type" +
							") VALUES (" +
							   "\""+cityName.toUpperCase()+"\"" +
							  ","+Long.valueOf(existingStateId)+"" +
							  ",0" +
							  ",\""+cityType.toUpperCase()+"\"" +
							");";
	
						  mySQL.executeStatement(statement);
							
						  existingCityId = getId();
						  existingCities.put(cityName.toUpperCase()+","+stateShortName.toUpperCase()+","+cityType.toUpperCase(),existingCityId);
						  
						  if(existingZips.get(zipCode) != null){
							  
							  existingZipId = existingZips.get(zipCode);
							  statement = "UPDATE bloomnet.zip SET " +
										   "zip_code = '"+zipCode+"'" +
										   ",latitude = "+Double.valueOf(lat)+"" +
										   ",longitude = "+Double.valueOf(longi)+"" +
										   "WHERE zip.id = "+Long.valueOf(existingZipId)+";";
							  
							  mySQL.executeStatement(statement);
						  
						  }else{
							  statement = "INSERT INTO bloomnet.zip ( " +
											  "zip_code" +
											  ",latitude" +
											  ",longitude" +
											  ") VALUES (" +
											  "'"+zipCode+"'" +
											  ","+Double.valueOf(lat)+"" +
											  ","+Double.valueOf(longi)+"" +
											  ");";
							  
							  mySQL.executeStatement(statement);
							  
							  existingZipId = getId();
							  existingZips.put(zipCode,existingZipId);
						  }
					  }
					  
					  if(existingCityZips.get(cityName.toUpperCase()+","+stateShortName.toUpperCase()+","+zipCode) == null){
						  String statement = "INSERT INTO bloomnet.city_zip_xref ( " +
											  "id_city" +
											  ",id_zip" +
											  ") VALUES (" +
											  ""+existingCityId+"" +
											  ","+existingZipId+"" +
											  ");";
						  
						  mySQL.executeStatement(statement);
						  
						  existingCityZips.put(cityName.toUpperCase()+","+stateShortName.toUpperCase() + "," + zipCode, "1");
					  }
				  }
			  }
			  ++ii;
			  if(ii%100 == 0)System.out.println("Finished with: " + ii + " lines.");
		}
		  file.Close();
		}catch(Exception ee){
			ee.printStackTrace();
		}
	}
	
}
