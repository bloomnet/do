package net.bloomnet;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class GetZipsForCities {
	
	private SQLData mySQL = new SQLData();
	
	public GetZipsForCities(){
		mySQL.login();
		try {
			createZipsFromCitiesResults("input.xls");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	
	private void createZipsFromCitiesResults(String inputFile) throws SQLException{
		File inputWorkbook = new File(inputFile);
		FileWriter fw = null;
		try {
			 fw = new FileWriter("output.csv");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		Workbook w;
		try {
			w = Workbook.getWorkbook(inputWorkbook);
			Sheet sheet = w.getSheet(0);
			fw.append("BMT#,Shop Name,CITY,Stateabv,Country,Zips\n");
			for (int ii = 1; ii < sheet.getRows(); ++ii){
				Cell florist = sheet.getCell(0, ii);
				Cell floristName = sheet.getCell(1, ii);
				Cell city = sheet.getCell(2, ii);
				Cell state = sheet.getCell(3, ii);
				Cell country = sheet.getCell(4, ii);
				
				String shopCode = florist.getContents().toString();
				String shopName = floristName.getContents().toString();
				String cityName = city.getContents().toString();
				String stateAbv = state.getContents().toString();
				String countryName = country.getContents().toString();
				
				String statement = "USE bloomnetordermanagement;";
				
				mySQL.executeStatement(statement);
				
				String query = "SELECT Zip_Code FROM city_zip_xref xref " +
						"INNER JOIN city ON xref.City_ID = city.City_ID " +
						"INNER JOIN state ON city.State_ID = state.State_ID " +
						"INNER JOIN zip ON xref.Zip_ID = zip.Zip_ID " +
						"WHERE city.Name = '"+cityName.replaceAll("'","''")+"' " +
						"AND state.Short_Name = '"+stateAbv+"'; ";
				
				ResultSet results = mySQL.executeQuery(query);
				
				String zipString = "";
				if(results != null){
					while(results.next()){
						zipString += results.getString("Zip_Code")+",";
					}
					if(zipString.length() > 1) zipString = zipString.substring(0, zipString.length()-1);
					mySQL.closeStatement();
				}
				
				fw.write(shopCode+","+shopName+","+cityName+","+stateAbv+","+countryName+","+zipString+"\n");
			}	
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args){
		new GetZipsForCities();
	}
}

