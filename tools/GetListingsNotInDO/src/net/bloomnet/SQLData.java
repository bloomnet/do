package net.bloomnet;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.Properties;

public class SQLData {
    //You can use a LinkedList<String> of mysql commands with processData
    //So you don't have to reinvent the wheel

    private Connection con;
    
    private String USERNAME = "";
    private String PASSWORD = "";
    private String ADDRESS = "";
    
    private PreparedStatement ps = null;
    
    private String userName = "";
    private String password = "";
    
    private FileOutputStream fos;
	private OutputStreamWriter out;
    
    public SQLData(){
 	
    	try {
			fos = new FileOutputStream("logs/SQLError.txt");
			out = new OutputStreamWriter(fos);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
    }
    
    public void processData(LinkedList<String> myData) {
        login();
	if (isConnected()) {
            for (int ii = 0; ii < myData.size(); ++ii) {
                executeStatement(myData.get(ii));
               System.out.println(myData.get(ii));
            }
        }
    }

    public void login() {
    	
    	InputStream input = null;
		try {
			input = new FileInputStream("/var/bloomnet/cfg/reports.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
    	Properties props = new Properties();
        try {
			props.load(input);
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
    	this.userName = props.getProperty("dbuser");
    	this.password = props.getProperty("dbpass");
    	
        setInfo(userName,password, "localhost");
    }

    public boolean setInfo(String USERNAME, String PASSWORD, String ADDRESS) {
        this.USERNAME = USERNAME;
        this.PASSWORD = PASSWORD;
        this.ADDRESS = ADDRESS;
        return startConnection();
    }

    public boolean startConnection() {
        try {
            new com.mysql.jdbc.Driver();
        } catch (SQLException e) {
			e.printStackTrace();
		}
        try {
            con = DriverManager.getConnection("jdbc:mysql://" + ADDRESS,
                    USERNAME, PASSWORD);
            if (isConnected()) {
                return true;
            } else {
            	System.out.println("Connection Problem");
                return false;
            }
        } catch (SQLException ex) {
            //Logger.getLogger(ReadFiles.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean isConnected() {
        try {
            return !con.isClosed();
        } catch (Exception e) {
            System.err.println("isConnected Exception: " + e.getMessage());
            try {
            	System.err.println(e.getMessage()+"\n");
				out.write(e.getMessage()+"\n");
			} catch (IOException ex) {
				e.printStackTrace();
			}
            return false;
        }
    }

    public void executeStatement(String statement) {
        
	try {
		con.setAutoCommit(true);
	} catch (SQLException e1) {
		e1.printStackTrace();
	}
	try {
            Statement s = con.createStatement();
            s.executeUpdate(statement);
            s.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage()+"\n");
            try {
            	System.err.println(e.getMessage()+"\n");
				out.write(e.getMessage()+"\n");
			} catch (IOException ex) {
				e.printStackTrace();
			}
        } catch (Exception e) {
            System.err.println("sendUpdate( " + e.getMessage() + " )");
            try {
            	System.err.println(e.getMessage()+"\n");
				out.write(e.getMessage()+"\n");
			} catch (IOException e1) {
				e.printStackTrace();
			}
        }
    }

    public ResultSet executeQuery(String query) {
        try {
            ps = null;
            con.setAutoCommit(false);
            ps = con.prepareStatement(query);
            con.commit();
            ResultSet results = ps.executeQuery();
            return results;
        } catch (Exception e) {
            System.err.println("sendUpdate( " + e.getMessage() + " )");
            try {
            	System.err.println(e.getMessage()+"\n");
				out.write(e.getMessage()+"\n");
			} catch (IOException e1) {
				e.printStackTrace();
			}
            return null;
        }
    }
    
    public void closeStatement(){
    	try {
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				System.err.println(e.getMessage()+"\n");
				out.write(e.getMessage()+"\n");
			} catch (IOException e1) {
				e.printStackTrace();
			}
		}
    }
    
    public void closeFile(){
    	try {
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
}
