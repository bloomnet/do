package net.bloomnet;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.Properties;

public class SQLData {
    //You can use a LinkedList<String> of mysql commands with processData
    //So you don't have to reinvent the wheel

    private Connection con;
    private String USERNAME = "";
    private String PASSWORD = "";
    private String ADDRESS = "";
    
    public void processData(LinkedList<String> myData) {
        login();
	if (isConnected()) {
            for (int ii = 0; ii < myData.size(); ++ii) {
                executeStatement(myData.get(ii));
               System.out.println(myData.get(ii));
            }
        }
    }

    public void login() {
    	InputStream input = null;
		try {
			input = new FileInputStream("/var/bloomnet/cfg/reports.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
    	Properties props = new Properties();
        try {
			props.load(input);
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
        setInfo(props.getProperty("dbuser"),props.getProperty("dbpassword"), "localhost");
        
    }

    public boolean setInfo(String USERNAME, String PASSWORD, String ADDRESS) {
    	System.out.println("set-info");
        this.USERNAME = USERNAME;
        this.PASSWORD = PASSWORD;
        this.ADDRESS = ADDRESS;
        return startConnection();
    }

    public boolean startConnection() {
    	System.out.println("start");
        try {
        	 new com.mysql.jdbc.Driver();
        } catch (SQLException e) {
			e.printStackTrace();
		}
        try {
            con = DriverManager.getConnection("jdbc:mysql://" + ADDRESS,
                    USERNAME, PASSWORD);
            if (isConnected()) {
                System.out.println(">Connected to MySQL server.");
                return true;
            } else {
            	System.out.println("Connection Problem");
                return false;
            }
        } catch (SQLException ex) {
            //Logger.getLogger(ReadFiles.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean isConnected() {
        try {
            return !con.isClosed();
        } catch (Exception e) {
            System.err.println("isConnected Exception: " + e.getMessage());
            return false;
        }
    }

    public void executeStatement(String statement) {
        
	try {
		con.setAutoCommit(true);
	} catch (SQLException e1) {
		e1.printStackTrace();
	}
	try {
            Statement s = con.createStatement();
            s.executeUpdate(statement);
            s.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.err.println("sendUpdate( " + e.getMessage() + " )");
        }
    }

    public ResultSet executeQuery(String query) {
        try {
            PreparedStatement ps = null;
            con.setAutoCommit(false);
            ps = con.prepareStatement(query);
            con.commit();
            return ps.executeQuery();
        } catch (Exception e) {
            System.err.println("sendUpdate( " + e.getMessage() + " )");
            return null;
        }
    }
}
