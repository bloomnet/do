package net.bloomnet;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.HashMap;
import java.util.Map;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class UploadFacilityReferences {
	
	private SQLData mySQL = new SQLData();
	private HashMap<String,Integer> shopFacilityXref = new HashMap<String,Integer>();
	private Map<String,String> facilityPhone = new HashMap<String,String>();
	
	public UploadFacilityReferences(){
		mySQL.login();
		try {
			setFacilityPhone();
			setShopFacilityXref();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		uploadFacilityReferences("xref.xls");
		
	}
	
	public void setShopFacilityXref() throws SQLException{
		String query = "SELECT shop_code,facility_id FROM bloomnet.shop_facility_xref;";
		ResultSet results = mySQL.executeQuery(query);
		while(results.next()){
			shopFacilityXref.put(results.getString("shop_code")+","+results.getString("facility_id"), 1);
		}
	}
	
	public void setFacilityPhone(){
		String query = "SELECT id,telephone_number FROM bloomnet.facility;";
		ResultSet results = mySQL.executeQuery(query);
		try {
			while(results.next()){
				facilityPhone.put(formatPhone(results.getString("telephone_number")),results.getString("id"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void uploadFacilityReferences(String inputFile){
		File inputWorkbook = new File(inputFile);
		Workbook w;
		try {
			w = Workbook.getWorkbook(inputWorkbook);
			Sheet sheet = w.getSheet(0);
			for (int ii = 1; ii < sheet.getRows(); ++ii){
				Cell florist = sheet.getCell(0, ii);
				Cell facility = sheet.getCell(1, ii);
				
				String shopCode = florist.getContents().toString();
				String facilityNumber = facility.getContents().toString();
				facilityNumber = formatPhone(facilityNumber);
				
				String facilityId = facilityPhone.get(facilityNumber);
				
				if(shopFacilityXref.get(shopCode+","+facilityId)== null){
					if(facilityId != null && !facilityId.equals("")){
						System.out.println(shopCode);
						String query = "INSERT INTO bloomnet.shop_facility_xref ("+
											"shop_code"+
											",facility_id"+
											") VALUES ("+
											"\""+shopCode+"\""+
											","+Integer.valueOf(facilityId)+""+
											");";
						mySQL.executeStatement(query);
						shopFacilityXref.put(shopCode+","+facilityId, 1);
					}
				}
			}	
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private String formatPhone(String phone){
		
		if(phone == null || phone.equals("")) return "";
		final StringBuilder result = new StringBuilder();
	    final StringCharacterIterator iterator = new StringCharacterIterator(phone);
	    
	    char character =  iterator.current();
	    
	    while (character != CharacterIterator.DONE ){
	      if (character == '(' || character == ')' || character == '-' || character == ' ') {
	      }else if(character == '0' || character == '1' || character == '2' || character == '3' ||
	    		  character == '4' || character == '5' || character == '6' || character == '7' || character == '8' ||
	    		  character == '9'){
	        result.append(character);
	      }
	      character = iterator.next();
	    }

	    return result.toString();
	}
	
	public static void main(String[] args){
		new UploadFacilityReferences();
	}
}

