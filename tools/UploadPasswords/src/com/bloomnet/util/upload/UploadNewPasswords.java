package com.bloomnet.util.upload;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

public class UploadNewPasswords {
	
	static final String baseTable = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"; 
	public static MessageDigest md;
	
	private SQLData mySQL;
	
	public static void main(String[] args){
		new UploadNewPasswords();
	}
	
	public UploadNewPasswords(){
		mySQL = new SQLData();
		mySQL.login();
		UploadPasswords("data/passwords.xls");
	}
	
	public static String hash(String pass) {
	            String sPassword = pass; 
	            byte[] unicodeValue;
				try {
					unicodeValue = sPassword.getBytes( "iso-8859-1" );
		            md = MessageDigest.getInstance( "SHA1" ); 
		            md.update( unicodeValue ); 
		            byte[] encodedPassword = md.digest(); 
		            String encodedString = encode( encodedPassword ); 
		            return encodedString;
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} 
				return "";
	  
	}
	 public static String encode( byte[] bytes ) 
	    { 
	        StringBuffer tmp = new StringBuffer(); 
	        int i = 0; 
	        byte pos; 
	        for (i = 0; i < bytes.length - bytes.length % 3; i += 3) { 
	            pos = (byte) (bytes[i] >> 2 & 63); 
	            tmp.append( baseTable.charAt( pos ) ); 
	            pos = (byte) (((bytes[i] & 3) << 4) + (bytes[i + 1] >> 4 & 15)); 
	            tmp.append( baseTable.charAt( pos ) ); 
	            pos = (byte) (((bytes[i + 1] & 15) << 2) + (bytes[i + 2] >> 6 & 3)); 
	            tmp.append( baseTable.charAt( pos ) ); 
	            pos = (byte) (bytes[i + 2] & 63); 
	            tmp.append( baseTable.charAt( pos ) ); 
	            if ((i + 2) % 56 == 0) { 
	                tmp.append( "\r\n" ); 
	            } 
	        } 
	        if (bytes.length % 3 != 0) { 
	            if (bytes.length % 3 == 2) { 
	                pos = (byte) (bytes[i] >> 2 & 63); 
	                tmp.append( baseTable.charAt( pos ) ); 
	                pos = (byte) (((bytes[i] & 3) << 4) + (bytes[i + 1] >> 4 & 15)); 
	                tmp.append( baseTable.charAt( pos ) ); 
	                pos = (byte) ((bytes[i + 1] & 15) << 2); 
	                tmp.append( baseTable.charAt( pos ) ); 
	                tmp.append( "=" ); 
	            } else if (bytes.length % 3 == 1) { 
	                pos = (byte) (bytes[i] >> 2 & 63); 
	                tmp.append( baseTable.charAt( pos ) ); 
	                pos = (byte) ((bytes[i] & 3) << 4); 
	                tmp.append( baseTable.charAt( pos ) ); 
	                tmp.append( "==" ); 
	            } 
	        } 
	        return tmp.toString(); 
	    }
	 
	 public void UploadPasswords(String inputFile){
		 
	 
		 File inputWorkbook = new File(inputFile);
		 
		 Workbook w;
		 
			try {
				w = Workbook.getWorkbook(inputWorkbook);
				Sheet sheet = w.getSheet(0);
				for (int ii = 1; ii < sheet.getRows(); ++ii){
					
					Cell shopCode = sheet.getCell(0,ii);
					Cell user = sheet.getCell(1,ii);
					Cell pass = sheet.getCell(2,ii);
					
					String shopString = shopCode.getContents().toString().substring(0,4).toLowerCase();
					String userString = user.getContents().toString().toLowerCase();
					String passHash = hash(pass.getContents().toString());
					
					if(shopString.equals(userString)){
						String statement = "UPDATE lportal2.User_ SET " +
							  				"password_ = \""+passHash+"\" " +
							  				"WHERE User_.screenName = \""+userString+"\"";
						mySQL.executeStatement(statement);
					}
				}
			}catch(Exception ee){}
	 }

}
